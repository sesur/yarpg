// Platform independent game code.

#include <def.h>
#include <game.h>
#include <game_internal.h>

#include <opengl/shader.h>
#include <opengl/renderer.h>
#include <renderer/renderer.h>
#include <renderer/opengl_renderer.h>

#include <util/layout.h>
#include <compiler.h>

#include <util/image.h>
#include <util/random.h>
#include <util/utf8.h>
#include <util/math.h>
#include <util/vector.h>
#include <util/string.h>
#include <util/format.h>
#include <util/colors.h>
#include <util/tokenizer.h>
#include <util/id_map.h>
#include <util/noise.h>
#include <util/hash.h>
#include <util/delaunay.h>

#include <font.h>
#include <game/thread.h>

#include <debug/debug_state.h>

// #pragma GCC poison memset

////////////////////////////////////////////////////////////////////////////////
/// GLOBALS ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static game *Game;
static transient_state *TState;
static platform_layer *PlatformLayer;


/* GetDebugStr(void) */
/* { */
/* 	debug_output *Output = &DebugState->Output; */
/* 	str *Result = &Output->Buffers[Output->CurrentBuffer++]; */

/* 	if (Output->CurrentBuffer == Output->NumBuffers) */
/* 	{ */
/* 		Output->CurrentBuffer = 0; */
/* 	} */

/* 	FormatOut("{str}\n", ViewOf(*Result)); */
/* 	Reset(Result); */

/* 	return Result; */
/* } */

//#define Dbg(Format, ...) FormatInto(GetDebugStr(), VIEWC(Format) __VA_OPT__(,) __VA_ARGS__)
#define ConsoleOut(Console, Format, ...) FormatInto(Reset(&(Console)->Output), VIEWC(Format) __VA_OPT__(,) __VA_ARGS__)

GAMEFN playing_sound *
EnqueueSound(sound_id Id, f32 Volume, f32 Speed)
{
	playing_sound *Sound = NULL;
	if (TState->FreeSound)
	{
		Sound = TState->FreeSound;
		TState->FreeSound = Sound->Next;
		FormatOut("[ENQUEUE SOUND] Getting free sound: {ptr} (new free: {ptr})\n",
			  Sound,
			  TState->FreeSound);
	}
	else
	{
		Sound = PushStruct(&TState->Arena, *Sound);
		FormatOut("[ENQUEUE SOUND] Getting new sound: {ptr}\n", Sound);

	}
	YASSERT(Sound);
	FetchSound(&TState->Assets, Id);

	Sound->Speed          = Speed;
	Sound->SecondsElapsed = 0.0;
	Sound->Volume         = Volume;
	Sound->Id             = Id;
	Sound->Next           = TState->Sounds;
	TState->Sounds        = Sound;

	return Sound;
}

PRIVATE idx
PlaySamples(playing_sound *Sound, idx NumSamples, idx TargetSampleRate, f32 *FSamples)
{
	idx SampleCount = NumSamples;
	INSTRUMENT(PlaySamples)
	{
		real_sound_buffer Music;
		sound_id NextPart;
		if (TryLoadSound(&TState->Assets, Sound->Id, &Music, &NextPart))
		{
			if (ValidID(NextPart))
			{
				FetchSound(&TState->Assets, NextPart);
			}
			SampleCount = 0;
			f32 *MusicData = (f32 *) Music.Data;

			f32 SampleRate = (f32) Music.SampleRate;

			f32 SampleRateAdjustment = (f32) TargetSampleRate / SampleRate;
			f32 SampleStep = Sound->Speed * SampleRateAdjustment;
			f32 SampleTime = SampleStep / SampleRate;

			f32 StartTime   = Sound->SecondsElapsed;
			f32 StartSample = StartTime * SampleRate;

			__m128 StartSample4x = _mm_set1_ps(StartSample);
			__m128 Offsets;
			{
				__m128 Temp = _mm_setr_ps(0.0, 1.0, 2.0, 3.0);
				__m128 SampleStep4x = _mm_set1_ps(SampleStep);
				Offsets = _mm_mul_ps(Temp, SampleStep4x);
			}
			StartSample4x = _mm_add_ps(StartSample4x, Offsets);
			__m128 MSamples = _mm_set1_ps((f32)Music.NumSamples);
			__m128 One      = _mm_set1_ps(1.0f);

			i32 const AllValid = 0xFFFF; /* AllValid = 0b00000000000000001111111111111111 */

			b32 Finished = YARPG_FALSE;

			for (;
			     (SampleCount + 4 <= NumSamples);
			     SampleCount += 4)
			{
				__m128 SampleCount4x = _mm_set1_ps(SampleStep * (f32)SampleCount);
				__m128 CurrentSample = _mm_add_ps(StartSample4x, SampleCount4x);

				__m128 ValidIdx      = _mm_cmple_ps(CurrentSample, MSamples);
				CurrentSample        = _mm_and_ps(CurrentSample, ValidIdx);
				__m128i SourceIdx    = _mm_cvttps_epi32(CurrentSample);
				__m128  FracSample   = _mm_sub_ps(CurrentSample,
								  _mm_cvtepi32_ps(SourceIdx));

				__m128 NFracSample   = _mm_sub_ps(One, FracSample);

				i32 Sources[4];
				_mm_storeu_si128((__m128i *) Sources, SourceIdx);

				f32 LowValues[4] __attribute__((aligned (16)));
				f32 HighValues[4] __attribute__((aligned (16)));
				for (idx I = 0;
				     I < 4;
				     ++I)
				{
					i32 SI = Sources[I];

					f32 LI, HI;

					if (SI < Music.NumSamples - 1)
					{
						LI  = MusicData[SI];
						HI  = MusicData[SI + 1];
					} else {
						LI = HI = MusicData[SI];
					}


					LowValues[I] = LI;
					HighValues[I] = HI;
				}
				__m128 LowValue  = _mm_load_ps(LowValues);
				__m128 HighValue = _mm_load_ps(HighValues);

				__m128 Value     = _mm_add_ps(_mm_mul_ps(FracSample, LowValue),
							      _mm_mul_ps(NFracSample, HighValue));

				// in the recursive call FSamples may not be aligned
				__m128 OldVal = _mm_loadu_ps(FSamples + SampleCount);
				Value = _mm_add_ps(OldVal, Value);
				_mm_maskmoveu_si128(_mm_castps_si128(Value),
						    _mm_castps_si128(ValidIdx),
						    (char *) (FSamples + SampleCount));

				i32 Valids = _mm_movemask_epi8(_mm_castps_si128(ValidIdx));

				if (Valids != AllValid)
				{
					// AllValid = 0x0FFF | 0x00FF | 0x000F | 0x0000

					Finished = YARPG_TRUE;

					switch (Valids)
					{
						break;case 0x0FFF:
						{
							SampleCount += 3;
						}
						break;case 0x00FF:
						{
							SampleCount += 2;
						}
						break;case 0x000F:
						{
							SampleCount += 1;
						}
						break;case 0x0000:
						{
							SampleCount += 0;
						}
						INVALIDDEFAULT();
					}

					break;
				}
			}


			for (;
			     SampleCount < NumSamples;
			     ++SampleCount)
			{
				f32 CurrentSample = StartSample + (f32) SampleCount * SampleStep;
				idx SourceIdx     = Round(CurrentSample);
				f32 FracSample    = CurrentSample - (f32)SourceIdx;

				f32 Value;
				if (SourceIdx >= Music.NumSamples)
				{
					Finished = YARPG_TRUE;
					break;
				} else if (SourceIdx == Music.NumSamples - 1) {
					// Should we even have this case ?
					// might be just better to throw away one
					// sample and fuse this with the above case
					Value = MusicData[SourceIdx];
				} else {
					f32 ValueLow  = MusicData[SourceIdx];
					f32 ValueHigh = MusicData[SourceIdx + 1];

					Value = Lerp(ValueLow, FracSample, ValueHigh);
				}
				Value = Sound->Volume * Value;

				FSamples[SampleCount] += Value;
			}

			Sound->SecondsElapsed += (f32)SampleCount * SampleTime;

			if (Finished && ValidID(NextPart))
			{
				Sound->Id             = NextPart;
				Sound->SecondsElapsed = 0.0;

				SampleCount += PlaySamples(Sound, NumSamples - SampleCount,
							   TargetSampleRate, FSamples + SampleCount);
			}
		}
	}

	return SampleCount;
}

GAMEFN i32 CONSTFN
KeyStateNumReleased(key_state Current)
{
	i32 HalfTransitions = Current.HalfTransitions;

	i32 NumReleased = HalfTransitions / 2;

	if (!Current.IsDown) NumReleased += (HalfTransitions % 2);

        return NumReleased;
}

GAMEFN i32 CONSTFN
KeyStateNumPressed(key_state Current)
{
	i32 HalfTransitions = Current.HalfTransitions;

	i32 NumPressed = HalfTransitions / 2;

	if (Current.IsDown) NumPressed += (HalfTransitions % 2);

        return NumPressed;
}

/// GLOBALS END ////////////////////////////////////////////////////////////////

#include "def.c"

#include "util/image.c"
#include "util/memory.c"
#include "util/utf8.c"
#include "util/random.c"
#include "util/vector.c"
#include "util/arena_allocator.c"
#include "util/list_allocator.c"
#include "util/math.c"
#include "util/asset_builder.c"
#include "util/asset.c"
#include "util/format.c"
#include "util/string.c"
#include "util/tokenizer.c"
#include "util/id_map.c"
#include "util/color.c"
#include "util/rect.c"
#include "util/hash.c"
#include "util/layout.c"
#include "util/delaunay.c"

#include "opengl/util.c"
#include "opengl/renderer.c"
#include "opengl/imgui.c"
#include "opengl/shader.c"
#include "opengl/string.c"

#include "renderer/renderer.c"
#include "renderer/opengl_renderer.c"

#include "game/world.c"
#include "game/battlefield.c"
#include "game/unit.c"
#include "game_ui.c"
#include "game/input.c"
#include "game/bf_input.c"
#include "game/debug_info.c"

////////////////////////////////////////////////////////////////////////////////
/// DYNAMIC FUNCTIONS //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

PUSH_TASK(PushTask)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.PushTask);
	return PlatformLayer->API.PushTask(Fun, Size, Alignment, Arg);
}

b32
PushLowPriorityTask(taskfn Fun, void *Arg)
{
	b32 Result;
	Result = PushTask(Fun, sizeof(Arg), alignof(Arg), &Arg);
	return Result;
}

b32
PushHighPriorityTask(taskfn Fun, void *Arg)
{
	b32 Result;
	Result = PushTask(Fun, sizeof(Arg), alignof(Arg), &Arg);
	return Result;
}

WAIT(Wait)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.Wait);
	return PlatformLayer->API.Wait();
}

RENDER_GLYPH(RenderGlyph)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.RenderGlyph);
	return PlatformLayer->API.RenderGlyph(Char);
}

REQUEST_FONT(RequestFont)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.RequestFont);
	return PlatformLayer->API.RequestFont(FontId, PtSize);
}

OPEN_ASSET_ITERATOR(OpenAssetIterator)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.OpenAssetIterator);
	return PlatformLayer->API.OpenAssetIterator();
}

START_ITERATOR(StartIterator)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.StartIterator);
	return PlatformLayer->API.StartIterator(Iter);
}

NEXT_FILE(NextFile)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.NextFile);
	return PlatformLayer->API.NextFile(Iter);
}

READ_FILE(ReadFile)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.ReadFile);
	return PlatformLayer->API.ReadFile(Handle, Start, Length, Buffer);
}

CLOSE_FILE(CloseFile)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.CloseFile);
	return PlatformLayer->API.CloseFile(Handle);
}

CLOSE_ITERATOR(CloseIterator)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.CloseIterator);
	return PlatformLayer->API.CloseIterator(Iter);
}

WRITE_OUT(WriteOut)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.WriteOut);
	return PlatformLayer->API.WriteOut(Target, NumBytes, Bytes);
}

PUSH_PERF_EVENT(PushPerfEvent)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.PushPerfEvent);
	return PlatformLayer->API.PushPerfEvent(HitCount, Loc, Type);
}

CURRENT_TIME_STAMP(CurrentTimeStamp)
{
	YASSERT(PlatformLayer);
	YASSERT(PlatformLayer->API.CurrentTimeStamp);
	return PlatformLayer->API.CurrentTimeStamp();
}

/// DYNAMIC FUNCTIONS END //////////////////////////////////////////////////////

#include "entity_test.c"
#include "util/noise.c"

PRIVATE void
MakeCamera(
	f32 CameraMatrix[4][4], // should be all 0
        v2  Middle, // in Meter, z assumed to be 0
	v3  Radius // in Meter, xrad, yrad, zrad
	)
{
	for (i32 Row = 0; Row < 4; ++Row)
	{
		for (i32 Column = 0; Column < 4; ++Column)
		{
			CameraMatrix[Row][Column] = 0.0f;
		}
		CameraMatrix[Row][Row] = 1.0f;
	}

	CameraMatrix[0][0] = 1/Radius.X; // = 2/(right-left)
	CameraMatrix[1][1] = 1/Radius.Y;
	CameraMatrix[2][2] = -1/Radius.Z;
	CameraMatrix[3][3] = 1;
	CameraMatrix[0][3] = -Middle.X/Radius.X;
	CameraMatrix[1][3] = -Middle.Y/Radius.Y;
	CameraMatrix[2][3] = 0;
}

PRIVATE void
InvertCamera(f32 Inverse[4][4],
	     f32 CameraMatrix[4][4])
{
	// NOTE: sizeof(*Inverse) == sizeof(f32[4]) == 16!
	ZeroArray(16, ((f32 *) Inverse));
	Inverse[0][0] = 1/CameraMatrix[0][0];
	Inverse[1][1] = 1/CameraMatrix[1][1];
	Inverse[2][2] = 1/CameraMatrix[2][2];
	Inverse[3][3] = 1;
	Inverse[0][3] = -CameraMatrix[0][3] / CameraMatrix[0][0];
	Inverse[1][3] = -CameraMatrix[1][3] / CameraMatrix[1][1];
	Inverse[2][3] = -CameraMatrix[2][3] / CameraMatrix[2][2];
}

#if 0
PRIVATE void
TestRandom(void)
{
	random_series Test;
	RandomSeries(&Test, 0);
	u32 NumTries = 10000000;

	u32 Bits[32] = {0};

	FormatOut("Begin random u32 test:\n");

	u64 Min, Max;

	YSASSERT(sizeof(u64) == 8);

	Min = 1UL << 63;
	Max = 0;

	for (u32 I = 0;
	     I < NumTries;
	     ++I)
	{
		u32 R = NextU32(&Test);

		if (R < Min) Min = R;
		if (R > Max) Max = R;

		for (u32 B = 0;
		     B < 32;
		     ++B)
		{
			Bits[B] += (R >> B) & 0x1;
		}
	}

	for (u32 B = 0;
	     B < 32;
	     ++B)
	{
		printf("Bit %u: %2.5lf (%u / %u)\n",
		       B, (f64)Bits[B] / NumTries,
		       Bits[B], NumTries);
	}

	printf("Min: %lu, Max: %lu\n\n\n",
	       Min, Max);

	FormatOut("Begin random normal test:\n");

	u32 Boxes[50] = {0};



	for (u32 I = 0;
	     I < NumTries;
	     ++I)
	{
		f32 Normal = NextNormal(&Test);

                u32 K = (u32) (Normal * ARRAYCOUNT(Boxes));

                YASSERT(K <= ARRAYCOUNT(Boxes));
		if (K == ARRAYCOUNT(Boxes)) K -= 1;

		Boxes[K] += 1;
	}



	for (u32 I = 0;
	     I < ARRAYCOUNT(Boxes);
	     ++I)
	{
		printf("Box [%f %f): %2.5lf (%u / %u)\n", (f32) I / ARRAYCOUNT(Boxes),
		       (f32) (I+1) / ARRAYCOUNT(Boxes),
		       (f64) Boxes[I] / NumTries,
		       Boxes[I], NumTries);
	}

}

#endif

PRIVATE char const *
LoadShaderText(asset_store *Assets, text_id Shader,
	       memory_arena *Arena, str_view Preamble)
{
	text_info *ShaderInfo = GetTextInfo(Assets, Shader);

	idx ShaderSize = ShaderInfo->NumBytes;

	idx CompleteSize = Preamble.Size // size of the preamble
		+ ShaderSize             // size of the actual shader
		+ 1;                     // for the null terminator

	char *ShaderSource = PushArray(Arena, CompleteSize, char);

	// write shader source
	LoadTextDataInto(Assets, Shader, (byte *) ShaderSource + Preamble.Size);

	// move #version to top
	// it should always be the first line

	char *FirstLineEnd = &ShaderSource[Preamble.Size];
	while (*FirstLineEnd++ != '\n');

	char *PreambleStart = ShaderSource;
	char *FirstLineStart  = &ShaderSource[Preamble.Size];
	while (FirstLineStart != FirstLineEnd)
	{
		*PreambleStart++ = *FirstLineStart++;
	}

	// write preamble
	for (u64 I = 0;
	     I < Preamble.Size;
	     ++I)
	{
		PreambleStart[I] = Preamble.Data[I];
	}
	// write null terminator
	ShaderSource[CompleteSize - 1] = '\0';

	return ShaderSource;
}

PRIVATE render_shader
LoadRenderShader(opengl_backend *Backend, memory_arena *Scratch,
		 asset_store *Assets,
		 text_id VertexAsset, text_id FragmentAsset)
{
	render_shader RenderShader;

	GLenum Types[2] = { GL_VERTEX_SHADER, GL_FRAGMENT_SHADER };


	char const *RendererFragmentSource = LoadShaderText(Assets,
							    FragmentAsset,
							    Scratch,
							    Backend->ShaderPreamble);

	char const *RendererVertexSource   = LoadShaderText(Assets,
							    VertexAsset,
							    Scratch,
							    Backend->ShaderPreamble);

	char const *RendSources[2] = { RendererVertexSource, RendererFragmentSource };

	char const *RenderAttributes[5] = {
		"Position", "Color", "TexturePos", "Transform", "TextureId"
	};

	char const *RenderUniforms[2] = {
		"Projection", "Textures",
	};


	YASSERT(BuildProgram(2, RendSources, Types,
			     &RenderShader.Program));

	LookupProgramLocations(RenderShader.Program,
			       5, RenderAttributes, RenderShader.Attributes,
			       2, RenderUniforms, RenderShader.Uniforms);

	return RenderShader;
}

PRIVATE void
InitTransientState(game *Game,
		   memory_arena Arena,
		   memory_arena *Debug,
		   transient_state *Trans)
{
	Trans->Arena = Arena;

	Trans->Scratch = SplitArena(&Trans->Arena, SCRATCH_ARENA_SIZE);

	memory_arena *Scratch = &Trans->Scratch;

	// potentially bad
	TState = Trans;

	COMPILERBARRIER();

	LoadYAFFFiles(&Trans->Assets, &Trans->Arena, Scratch);
	FreeAll(Scratch);

	text_id VertexShader = FirstTextOf(&Trans->Assets, Category_VERTEXSHADER);
	text_id FragmentShader = FirstTextOf(&Trans->Assets, Category_FRAGSHADER);

	YASSERT(ValidID(VertexShader));
	YASSERT(ValidID(FragmentShader));

	render_shader RenderShader = LoadRenderShader(&Game->Backend, Scratch,
						      &Trans->Assets,
						      VertexShader, FragmentShader);

	GL_QUICK_CHECK();

	RendererCreate(&Trans->Renderer, &Game->Backend, RenderShader);

	GL_QUICK_CHECK();

	FreeAll(Scratch);


	Game->Battle.Head = FirstSpriteOf(&Trans->Assets, Category_HEAD);
	Game->Battle.Body = FirstSpriteOf(&Trans->Assets, Category_TORSO);

	Game->Battle.Bow  = FirstSpriteOf(&Trans->Assets, Category_BOW);
	Game->Battle.Fist = FirstSpriteOf(&Trans->Assets, Category_FIST);

	Game->Battle.Arrow = FirstSpriteOf(&Trans->Assets, Category_ARROW);

	InitGui(&Trans->Gui, &Game->Table);

	FreeAll(&Trans->Scratch);

	Trans->Sounds = NULL;
	Trans->FreeSound = NULL;

	Trans->DebugInfo = DebugInfo_OfArena(Debug, &PlatformLayer->Info);
}

PRIVATE void
InitGameBattlefield(game_battlefield *Battle, memory_arena *Arena,
		    memory_arena *Scratch)
{
	i32 Width = 10;
	i32 Height = 10;
	i32 MaxEntities = Width * Height;

	CreateBattlefield(&Battle->Field, Width, Height,
			  MaxEntities, Arena, Scratch);

	Battle->TurnOrder     = PushArray(Arena, MaxEntities, *Battle->TurnOrder);
	Battle->TurnCount     = 0;
	Battle->CurrentEntity = 0;
	Battle->Actions       = PushArray(Arena, Width * Height, *Battle->Actions);
}

PRIVATE void
InitGameWorld(game_world *Map, memory_arena *Arena)
{
	InitWorld(&Map->World, Arena);

	world_position Origin = {IV3(0, 0, 0), V2(0, 0)};
	world_position HeroPosition;
	HeroPosition.Chunk = Origin.Chunk;
	HeroPosition.Offset = Diff(Origin.Offset, V2(0.5f, 0.5f));
	world_position EnemyPosition;
	EnemyPosition.Chunk = Origin.Chunk;
	EnemyPosition.Offset = Add(Origin.Offset, V2(1.0f, 0.5f));
	world_position EnemyPosition2 = EnemyPosition;
	EnemyPosition2.Offset = Add(EnemyPosition2.Offset, V2(-2, -2));

	f32 HeroColor[3]  = {F32C(0.1), F32C(0.7), F32C(0.2)};
	f32 EnemyColor[3] = {F32C(1.0), F32C(0.2), F32C(0.3)};
	Map->HeroIndex = AddArmy(&Map->World, Origin, HeroColor, WorldAllegiance_OWNED);
	AddArmy(&Map->World, EnemyPosition, EnemyColor, WorldAllegiance_ENEMY);
	AddArmy(&Map->World, EnemyPosition2, EnemyColor, WorldAllegiance_ENEMY);

	Reload(&Map->World);

	Map->InputState.CurrentAction = ActionState_NOACTION;
	Map->PlayerGold  = 100;
	Map->MenuOpen    = YARPG_FALSE;
	Map->ShouldQuit  = YARPG_FALSE;
	Map->ShowDetails = YARPG_FALSE;
}

PRIVATE void
InitGame(game *Game, memory_arena Arena, memory_arena Debug, memory_arena *Scratch)
{
	Game->Arena = Arena;
	Game->Debug = Debug;
	Game->TranStateInitialized    = YARPG_TRUE;
	Game->Clock                   = 0.0f;

	Game->ZoomFactor              = 1.0f;
	Game->ShowMemoryWindow        = YARPG_FALSE;
	Game->MemoryWindowBounds      = (irect) {.TopRight = IV(950, 880), .BottomLeft = IV(650, 650)};
	Game->ShowCameraPosition      = YARPG_FALSE;
	Game->CameraWindowBounds      = (irect) {.TopRight = IV(950, 280), .BottomLeft = IV(650, 50)};
	Game->CameraVel               = V(0.0f, 0.0f);
	Game->ShowPerformance         = YARPG_TRUE;
	Game->PerformanceWindowBounds = (irect) {.TopRight = IV(950, 280), .BottomLeft = IV(350, 50)};

	Game->FontViewerBounds        = (irect) {.TopRight = IV(950, 280), .BottomLeft = IV(650, 50)};
	Game->ShowFontViewer          = YARPG_FALSE;
	Game->FontViewerInputCount    = 0;
	Game->FontViewerInput[0]      = '\0';

	Game->ShowFPS                 = YARPG_FALSE;
	Game->FPSBounds               = (irect) {.TopRight = IV(200, 150), .BottomLeft = IV(50, 50)};

	Game->ShowTest                = YARPG_FALSE;
	Game->TestBounds              = (irect) {.TopRight = IV(50+300, 550+330), .BottomLeft = IV(50, 550)};

	Game->ShowRenderGroupInfo     = YARPG_FALSE;
	Game->RenderGroupInfoBounds   = (irect) {.TopRight = IV(50+800, 550+80), .BottomLeft = IV(50, 550)};
	Game->ShowRendererInfo        = YARPG_FALSE;
	Game->RendererInfoBounds      = (irect) {.TopRight = IV(50+800, 550+80), .BottomLeft = IV(50, 550)};

	Game->ShowMusic               = YARPG_FALSE;
	Game->MusicWindowBounds       = (irect) {.TopRight = IV(50+600, 550+450), .BottomLeft = IV(50, 550)};

	Game->ShowUiDebug             = YARPG_FALSE;
	Game->ShowMapEdit             = YARPG_FALSE;
	Game->ShowDbgMsg              = YARPG_FALSE;
	Game->DbgMsgBounds            = (irect) {.TopRight = IV(50+600, 550+450), .BottomLeft = IV(50, 550)};

	Game->DPS = 0.0f;

	Game->ShowBattle              = YARPG_FALSE;

	for (i32 y = 0; y < 4; y++)
	{
		for (i32 x = 0; x < 4; x++)
		{
			Game->CameraMatrix[y][x] = 0.0f;
		}
	}

	Game->WindowBounds = (irect) {
		.TopRight = IV(50+300, 650+230),
		.BottomLeft = IV(50, 650)
	};

	Game->FrameCounter = 0;
	for (u32 Cursor = 0;
	     Cursor < NUM_PERFORMANCE_DATA;
	     ++Cursor)
	{
		Game->PQueueFillGraph[Cursor] = 0.0;
		Game->BQueueFillGraph[Cursor] = 0.0;
	}

	GL_QUICK_CHECK();

	CreateBackend(&Game->Backend, &Game->Arena);

	GL_QUICK_CHECK();

	InitGlyphTable(&Game->Table, FONT_ROBOTTO, 48, Scratch);

	InitGameWorld(&Game->Map, &Game->Arena);
	InitGameBattlefield(&Game->Battle, &Game->Arena, Scratch);

	GL_QUICK_CHECK();

	Game->LastRenderGroupSize = 0;
}

/* PRIVATE void */
/* InitConsole(debug_console *Console, memory_arena *Arena) */
/* { */
/* 	Console->Visible        = YARPG_FALSE; */
/* 	Console->PercentShowing = 0.0f; */
/* 	Console->NumChars       = 0; */
/* 	Console->Output         = MakeStr(1024, Arena); */
/* 	Console->Input          = MakeStr(1024, Arena); */
/* } */

/* PRIVATE void */
/* InitDebugOutput(debug_output *Output, */
/* 		idx NumBuffers, */
/* 		idx BufferSize, */
/* 		memory_arena *Arena) */
/* { */
/* 	Output->NumBuffers       = NumBuffers; */
/* 	byte (*Data)[BufferSize] = PushArray(Arena, NumBuffers, *Data); */
/* 	Output->Buffers          = PushArray(Arena, NumBuffers, *Output->Buffers); */
/* 	for (idx Buffer = 0; */
/* 	     Buffer < NumBuffers; */
/* 	     ++Buffer) */
/* 	{ */
/* 		Output->Buffers[Buffer] = MakeStrFromBuffer(BufferSize, (char *)Data[Buffer]); */
/* 	} */
/* } */

/* PRIVATE void */
/* InitDebugState(platform_layer *Platform, udx Size, void *Memory) */
/* { */
/* 	YASSERTF(sizeof(debug_state) < Size, */
/* 		 "%lu < %lu", sizeof(debug_state), Size); */
/* 	/\* global *\/ DebugState = GET_DEBUG_STATE(Memory); */

/* 	DebugState->CurrentData        = 0; */

/* 	DebugState->CurrentStorage     = 0; */
/* 	DebugState->NextEvent          = &DebugState->EventStorage[0][0]; */
/* 	DebugState->InternalBlockId    = 0; */
/* 	DebugState->AcceptData         = 1; */
/* 	DebugState->CurrentId          = INVALID_PERF_ID; */
/* 	DebugState->FirstFrameToShow   = 0; */
/* 	DebugState->NumFramesToShow    = 3; */
/* 	DebugState->ByFunction         = 1; */

/* 	DebugState->NumInstrumentations = NumInstrumentations; */

/* 	void *ArenaStart = DebugState + 1; */
/* 	udx ArenaSize    = Size - ((ptr) ArenaStart - (ptr) Memory); */

/* 	InitializeArena(&DebugState->Arena, ArenaSize, ArenaStart); */

/* 	DebugState->NumFrames = NUM_GATHERED_DATA; */
/* 	DebugState->Frames    = PushZeroArray(&DebugState->Arena, */
/* 					      DebugState->NumFrames, */
/* 					      *DebugState->Frames); */
/* 	DebugState->FreeBlock = NULL; */
/* 	DebugState->NumThreads = Platform->Info.NumWorkerThreads + 1; */
/* 	DebugState->Threads = PushArray(&DebugState->Arena, */
/* 				       DebugState->NumThreads, */
/* 				       *DebugState->Threads); */

/* 	for (idx I = 0; */
/* 	     I < DebugState->NumThreads; */
/* 	     ++I) */
/* 	{ */
/* 		DebugState->Threads[I].Closed = NULL; */
/* 		DebugState->Threads[I].Open   = NULL; */
/* 	} */

/* 	InitConsole(&DebugState->Console, &DebugState->Arena); */
/* 	InitDebugOutput(&DebugState->Output, */
/* 			256, */
/* 			1024, */
/* 			&DebugState->Arena); */
/* } */

typedef enum
{
//	Biome_OCEAN,
	Biome_SNOW,
	Biome_TUNDRA,
//	Biome_BARE,
//	Biome_SCORCHED,
//	Biome_TAIGA,
//	Biome_SHRUBLAND,
//	Biome_TEMPERATE_DESERT,
//	Biome_TEMPERATE_RAIN_FORREST,
//	Biome_TEMPERATE_DECIDUOUS_FORREST,
	Biome_DECIDUOUS_FORREST,
	Biome_GRASSLAND,
	Biome_RAIN_FORREST,
	Biome_DESERT,
//	Biome_TROPICAL_RAIN_FOREST,
//	Biome_TROPICAL_SEASONAL_FOREST,
//	Biome_SUBTROPICAL_DESERT,

	Biome_COUNT,
} biome;

PRIVATE biome
SelectLandBiome(f32 Values[MapParam_COUNT])
{
	f32 Elevation = Values[MapParam_ELEVATION]; // -1 to 1
	f32 Moisture  = Values[MapParam_MOISTURE]; // -1 to 1
	biome Biome = Biome_GRASSLAND;

	i32 ElevationStep = (i32) RoundNearest(ReScale(Elevation, -1, 1, 0, 3));
	i32 MoistureStep  = (i32) RoundNearest(ReScale(Moisture, -1, 1, 0, 5));
#define BIOME(Type) Biome = Biome_ ## Type
	switch (ElevationStep)
	{
		break;case 0:
		{
			switch (MoistureStep)
			{
				break;case 0: case 1: case 2:
				{
					BIOME(TUNDRA);
				}
				break;case 3: case 4: case 5:
				{
					BIOME(SNOW);
				}
				INVALIDDEFAULT();
			}
		}
		break;case 1:
		{
			switch (MoistureStep)
			{
				break;case 0: case 1:
				{
					BIOME(DESERT);
				}
				break;case 2: case 3:
				{
					BIOME(GRASSLAND);
				}
				break;case 4: case 5:
				{
					BIOME(TUNDRA);
				}
				INVALIDDEFAULT();
			}
		}
		break;case 2:
		{
			switch (MoistureStep)
			{
				break;case 0:
				{
					BIOME(DESERT);
				}
				break;case 1: case 2:
				{
					BIOME(GRASSLAND);
				}
				break;case 3: case 4:
				{
					BIOME(DECIDUOUS_FORREST);
				}
				break;case 5:
				{
					BIOME(RAIN_FORREST);
				}

				INVALIDDEFAULT();
			}
		}
		break;case 3:
		{
			switch (MoistureStep)
			{
				break;case 0:
				{
					BIOME(DESERT);
				}
				break;case 1:
				{
					BIOME(GRASSLAND);
				}
				break;case 2: case 3:
				{
					BIOME(DECIDUOUS_FORREST);
				}
				break;case 4: case 5:
				{
					BIOME(RAIN_FORREST);
				}
				INVALIDDEFAULT();
			}
		}
		INVALIDDEFAULT();
	}

#undef BIOME

	return Biome;
}

PRIVATE void
AddRiver(entity_test *Test, f32 Radius, iv2 Max, iv2 A, i32 Id)
{
	v2 Offset[] =
	{
		V(-1, 0),
		V(-0.5, Sqrt3Halfs),
		V(0.5, Sqrt3Halfs),
		V(1, 0),
		V(0.5, -Sqrt3Halfs),
		V(-0.5, -Sqrt3Halfs),

	};

	idx Corners[2] = { Id, (Id + 1) % 6 };

	idx NumSteps = 100;

	for(idx Step = 0;
	    Step < NumSteps;
	    ++Step)
	{
		entity_id River = NextEntity(Test);
		v2 OrigPos = HexToLinear(ToFloat(A));
		v2 Dim = V(Radius / (f32)Max.X, Radius / (f32)Max.Y);
		v2 Pos = Hadamard(Dim, OrigPos);
		v2 Delta = Lerp(Offset[Corners[0]], (f32) Step / (f32) (NumSteps - 1), Offset[Corners[1]]);
		AddPosition(Test, River, Add(Delta, Pos));
		AddRect(Test, River, (rect_data)
			{
				.Color  = V(0.3, 0.2, 0.8, 1),
				.Dim    = V(0.05, 0.5),
				.Offset = V(0, 0),
			});
	}
}

PRIVATE void
PopulateWeirdSnake(entity_test *Test, asset_store *Assets)
{
	rand64 S = RandomSeries(5);

	sprite_id Swamp = FirstSpriteOf(Assets, Category_SWAMP);

	entity_id LastEntity = { -1 };
	entity_id FirstEntity = { -1 };
	for (idx Idx = 0;
	     Idx < (1 << 14) - 10;
	     ++Idx)
	{
		entity_id Entity = NextEntity(Test);

		YASSERT(AddRect(Test, Entity,
				(rect_data)
				{
					.Color  = V(1, 0, 1, 1),
					.Offset = V(NextBetween(&S, -8, 8), NextBetween(&S, -8, 8)),
					.Dim    = V(0.5, 0.5),
				}));

		YASSERT(AddTexture(Test, Entity,
				   (text_data)
				   {
					   .Offset = V(NextBetween(&S, -8, 8), NextBetween(&S, -8, 8)),
					   .Dim    = V(0.5, 0.5),
					   .Sprite = Swamp,
				   }));

		YASSERT(AddPosition(Test, Entity,
				    V(NextBetween(&S, -8, 8), NextBetween(&S, -8, 8))));

		if (LastEntity.ToIdx != -1)
		{
			YASSERT(AddFollow(Test, Entity, LastEntity));
		}
		if (FirstEntity.ToIdx == -1)
		{
			FirstEntity = Entity;
		}
		LastEntity = Entity;
	}

	YASSERT(AddFollow(Test, FirstEntity, LastEntity));
}

PRIVATE void
AddSwamp(entity_test *Test, v2 Pos, v2 Dim)
{
	v2 HalfDim = Scale(0.5, Dim);
	sprite_id Sprite = FirstSpriteOf(&TState->Assets, Category_SWAMP);
	entity_id Swamp = NextEntity(Test);
	AddPosition(Test, Swamp, Diff(Pos, HalfDim));
	AddTexture(Test, Swamp,
		   (text_data)
		   {
			   .Dim    = Dim,
			   .Sprite = Sprite,
		   });
}

PRIVATE void
AddTree(entity_test *Test, v2 Pos, v2 Dim)
{
	v2 HalfDim = V(0.5 * Dim.X, 0);//Scale(0.5, Dim);
	sprite_id Sprite = FirstSpriteOf(&TState->Assets, Category_TREE);
	entity_id Tree = NextEntity(Test);
	AddPosition(Test, Tree, Diff(Pos, HalfDim));
	AddTexture(Test, Tree,
		   (text_data)
		   {
			   .Dim    = Dim,
			   .Sprite = Sprite,
		   });
}

FWDDECLARE(base_map);
struct base_map
{
	v2 Middle;

	idx      NumPoints;
	id_map   Indices;
	iv2      *HexPos;

	idx      NumEdges;
	id_map   Edges;
	iv2      *EdgePos;
	iv2      (*EdgeHex)[2];
};

#define ID(X, Y)						\
	({							\
		YASSERT(I16_MIN <= X && X <= I16_MAX);		\
		YASSERT(I16_MIN <= Y && Y <= I16_MAX);		\
		u64 NewX = (u16) X;				\
		u64 NewY = (u16) Y;				\
		entity_id Id = (entity_id) { (NewX << 32)	\
			| NewY };				\
		YASSERT(Id.ToIdx >= 0);				\
		Id;						\
	})

PRIVATE void
MakeEdges(idx NumPoints, iv2 Points[static NumPoints],
	  id_map *Edges, idx NumEdges, iv2 EdgePos[static NumEdges],
	  iv2 Hexs[static NumEdges][2])
{
	iv2 Sides[] =
		{
			IV(1, 0),
			IV(0, 1),
			IV(1, -1),
			IV(-1, 0),
			IV(0, -1),
			IV(-1, 1),
		};

	YASSERT(NumEdges <= MAXOF((id_index) 0));
	idx LastE = 0;
	for (idx P = 0;
	     P < NumPoints;
	     ++P)
	{
		iv2 HexPos = Points[P];
		for (idx Edge = 0;
		     Edge < (idx)ARRAYCOUNT(Sides);
		     ++Edge)
		{
			iv2 Pos = Add(Sides[Edge], Scale(2, HexPos));
			id_index *Slot = CreateIdSlot(Edges, ID(Pos.X, Pos.Y));
			if (Slot)
			{
				iv2 HexPos2 = Add(HexPos, Sides[Edge]);
				id_index Idx = (id_index) LastE++;
				EdgePos[Idx] = Pos;
				*Slot = Idx;
				Hexs[Idx][0] = HexPos;
				Hexs[Idx][1] = HexPos2;
			}
		}
	}
	YASSERTF(LastE == NumEdges,
		 "%ld == %ld", LastE, NumEdges);
}

PRIVATE base_map
MakeRoundBaseMap(memory_arena *Scratch, idx Radius)
{
	idx NumPoints = 1 + 3 * Radius * (Radius + 1);
	id_map Indices = IdMapOfArena(NumPoints, Scratch);
	iv2 *IPos      = PushArray(Scratch, NumPoints, *IPos);

	YASSERTF(NumPoints <= MAXOF((id_index) 0),
		 "NumPoints: %ld, Max: %ld",
		 NumPoints, (idx) MAXOF((id_index) 0));

	idx LastP = 0;
	for (idx Y = -Radius;
	     Y <= Radius;
	     ++Y)
	{
		for (idx X = Max(-Radius, -Y - Radius);
		     X <= Min(Radius, -Y + Radius);
		     ++X)
		{
			iv2 Pos = IV(X, Y);
			id_index *Slot = CreateIdSlot(&Indices, ID(X, Y));
			YASSERT(Slot);
			id_index Idx = (id_index) LastP++;
			*Slot = Idx;
			IPos[Idx] = Pos;
		}
	}

	idx NumEdges    = 3 * NumPoints + 3 * (2 * Radius + 1);
	id_map Edges    = IdMapOfArena(NumEdges, Scratch);
	iv2    *EdgePos = PushArray(Scratch, NumEdges, *EdgePos);
	iv2    (*Hexs)[2]  = PushArray(Scratch, NumEdges, *Hexs);
#if 0
	iv2 Sides[] =
		{
			IV(1, 0),
			IV(0, 1),
			IV(1, -1),
			IV(-1, 0),
			IV(0, -1),
			IV(-1, 1),
		};

	YASSERT(NumEdges <= MAXOF((id_index) 0));
	idx LastE = 0;
	for (idx P = 0;
	     P < NumPoints;
	     ++P)
	{
		iv2 HexPos = IPos[P];
		for (idx Edge = 0;
		     Edge < (idx)ARRAYCOUNT(Sides);
		     ++Edge)
		{
			iv2 Pos = Add(Sides[Edge], Scale(2, HexPos));
			id_index *Slot = CreateIdSlot(&Edges, ID(Pos.X, Pos.Y));
			if (Slot)
			{
				iv2 HexPos2 = Add(HexPos, Sides[Edge]);
				id_index Idx = LastE++;
				EdgePos[Idx] = Pos;
				*Slot = Idx;
				Hexs[Idx][0] = HexPos;
				Hexs[Idx][1] = HexPos2;
			}
		}
	}

	YASSERT(LastE == NumEdges);
#else
	MakeEdges(NumPoints, IPos,
		  &Edges, NumEdges, EdgePos,
		  Hexs);
#endif

	base_map Result;
	Result.Indices      = Indices;
	Result.HexPos       = IPos;
	Result.NumPoints    = NumPoints;
	Result.NumEdges     = NumEdges;
	Result.Edges        = Edges;
	Result.EdgePos      = EdgePos;
	Result.EdgeHex      = Hexs;

	return Result;
}

PRIVATE base_map
MakeRectangleBaseMap(memory_arena *Scratch, iv2 Dims)
{
	i32 Width  = Dims.Width;
	i32 Height = Dims.Height;

	idx NumPoints = Width * Height;

	id_map Indices = IdMapOfArena(NumPoints, Scratch);

	iv2 *IPos      = PushArray(Scratch, NumPoints, *IPos);

	idx LastP = 0;
	for (idx Y = 0;
	     Y < Height;
	     ++Y)
	{
		for (idx X = 0;
		     X < Width;
		     ++X)
		{
			id_index *Slot = CreateIdSlot(&Indices, ID(X, Y));
			YASSERT(Slot);
			id_index Idx = (id_index) LastP++;
			*Slot = Idx;

			IPos[Idx] = IV(X, Y);
		}
	}

	YASSERT(LastP == NumPoints);

	idx NumEdges = Width * Height * 3 + (Width + Height) * 2 - 1;

	id_map Edges    = IdMapOfArena(NumEdges, Scratch);
	iv2    *EdgePos = PushArray(Scratch, NumEdges, *EdgePos);
	iv2    (*Hexs)[2]  = PushArray(Scratch, NumEdges, *Hexs);

	MakeEdges(NumPoints, IPos,
		  &Edges, NumEdges, EdgePos,
		  Hexs);

	base_map Result;
	Result.Indices      = Indices;
	Result.HexPos       = IPos;
	Result.NumPoints    = NumPoints;
	Result.NumEdges     = NumEdges;
	Result.Edges        = Edges;
	Result.EdgePos      = EdgePos;
	Result.EdgeHex      = Hexs;

	return Result;
}

PRIVATE v2 *
SpawnTiles(memory_arena *Arena, idx NumPoints, iv2 HexPos[static NumPoints],
	   v2 HexDim, v2 Offset)
{
	v2 *Location = PushArray(Arena, NumPoints, *Location);
	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		v2 GridPos = HexToLinear(ToFloat(HexPos[Point]));
		Location[Point] = Hadamard(HexDim, Diff(GridPos, Offset));
	}
	return Location;
}

PRIVATE void
AddEdges(entity_test *Test, idx NumPoints, iv2 HexPos[static NumPoints],
	 v2 HexDim, v2 Offset)
{
	v2 Sides[] =
	{
		V(0.5, 0),
		V(-0.5, 0),
		V(0, 0.5),
		V(0, -0.5),
		V(-0.5, 0.5),
		V(0.5, -0.5),
	};

	for (idx Side = 0;
	     Side < (idx)ARRAYCOUNT(Sides);
	     ++Side)
	{
		v2 SideOffset = Sides[Side];
		for (idx Point = 0;
		     Point < NumPoints;
		     ++Point)
		{
			entity_id Edge = NextEntity(Test);
			v2 GridPos = HexToLinear(Add(ToFloat(HexPos[Point]), SideOffset));
			v2 Location = Hadamard(HexDim, Diff(GridPos, Offset));

			AddPosition(Test, Edge, Location);
			AddHex(Test, Edge, (hex_data)
			       {
				       .Color  = COLOR.White,
				       .Dim    = Scale(F32C(0.3), HexDim),
				       .Offset = V(0, 0),
			       });
		}
	}

}

FWDDECLARE(neighborhood);

struct neighborhood
{
	id_index *NeighborIds;
	idx      *NeighborsEnd;
};

PRIVATE neighborhood
MakeNeighbors(memory_arena *Arena, idx NumPoints, iv2 *HexPos, id_map *Indices)
{
	id_index *NeighborIds  = PushArray(Arena, NumPoints * 6, *NeighborIds);
	idx      *NeighborsEnd = PushArray(Arena, NumPoints, *NeighborsEnd);

	idx LastNeighbor = 0;
	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		iv2 Pos = HexPos[Point];
		idx X = Pos.X;
		idx Y = Pos.Y;
		entity_id Id = ID(X, Y);
		id_index *Slot = GetIdSlot(Indices, Id);
		YASSERT(Slot);
		id_index Idx = *Slot;

		id_index *Candidates[6] =
			{
				GetIdSlot(Indices, ID((X-1), (Y))),
				GetIdSlot(Indices, ID((X+1), (Y))),
				GetIdSlot(Indices, ID((X), (Y-1))),
				GetIdSlot(Indices, ID((X), (Y+1))),
				GetIdSlot(Indices, ID((X+1), (Y-1))),
				GetIdSlot(Indices, ID((X-1), (Y+1))),
			};

		for (idx I = 0;
		     I < 6;
		     ++I)
		{
			if (Candidates[I])
			{
				NeighborIds[LastNeighbor++] = *Candidates[I];
			}
		}

		NeighborsEnd[Idx] = LastNeighbor;
	}

	neighborhood Result;

	Result.NeighborIds  = NeighborIds;
	Result.NeighborsEnd = NeighborsEnd;

	return Result;
}

PRIVATE f32 *
MakeHeights(memory_arena *Arena, idx NumPoints, rand64 *Rand)
{
	f32 *Heights = PushArray(Arena, NumPoints, *Heights);
	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		Heights[Point] = NextBinormal(Rand);
	}
	return Heights;
}

PRIVATE f32 *
SmoothHeights(memory_arena *Arena, idx NumPoints, f32 *Input, idx NumSmoothings,
	      neighborhood Hood)
{
	id_index *NeighborIds  = Hood.NeighborIds;
	idx      *NeighborsEnd = Hood.NeighborsEnd;

	idx Current = 0;
	f32 *Heights[2] =
		{
			Input,
			PushArray(Arena, NumPoints, *Heights[1]),
		};

	for (idx SmoothStage = 0;
	     SmoothStage < NumSmoothings;
	     ++SmoothStage)
	{
		id_index *Neighbor = &NeighborIds[0];
		for (idx Point = 0;
		     Point < NumPoints;
		     ++Point)
		{
			f32 Avg = Heights[Current][Point];
			f32 Num = 1;
			for (;
			     Neighbor != &NeighborIds[NeighborsEnd[Point]];
			     ++Neighbor)
			{
				Avg += Heights[Current][*Neighbor];
				Num += 1;
			}
			Heights[1-Current][Point] = Avg/Num;
		}
		Current = 1 - Current;
	}

	return Heights[Current];
}

PRIVATE f32
AverageHeight(idx NumPoints, f32 Heights[static NumPoints])
{
	f32 AvgHeight = 0;

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		AvgHeight += Heights[Point];
	}
	AvgHeight /= (f32) NumPoints;

	return AvgHeight;

}

PRIVATE iv2
EdgeLo(iv2 Edge)
{
	iv2 Half  = IV(Edge.X / 2, Edge.Y / 2);
	iv2 Delta = Diff(Edge, Half);
	i32 Sum   = Delta.X + Delta.Y;
	iv2 Lo;

	if (Sum == 2)
	{
		Lo = Add(Half, IV(1, 0));
	}
	else if (Sum == -2)
	{
		Lo = Diff(Half, IV(0, -1));
	}
	else
	{
		Lo = Half;
	}

	return Lo;
}

PRIVATE iv2
EdgeHi(iv2 Edge)
{
	iv2 Lo = EdgeLo(Edge);
	iv2 Delta = Diff(Edge, IV(Lo.X * 2, Lo.Y * 2));
	iv2 Hi = Add(Lo, Delta);
	return Hi;
}



PRIVATE void
MakeForrest(entity_test *Test, memory_arena *Scratch, map_gen *Options)
{
	rand64 Rand = RandomSeries(Options->Seed);
	//RandomSeries(2141424);
	//RandomSeries(123);

	f32 HexRad = Options->HexRad;
	v2 HexDim;
	base_map Map;
	v2 Offset;
	switch (Options->Type)
	{
		break;case BaseMapType_RECT:
		{
			idx Width = Options->Dim.Width;
			idx Height = Options->Dim.Height;

			Map = MakeRectangleBaseMap(Scratch, Options->Dim);
			Offset = HexToLinear(V((f32)Width/F32C(2.0), (f32)Height/F32C(2.0)));

			HexDim = V(HexRad, HexRad*F32C(0.5));// Scale(HexRad, V(2.0/Width, 2.0/Height));
		}
		break;case BaseMapType_CIRCLE:
		{
			idx Radius = Options->Radius;
			Map = MakeRoundBaseMap(Scratch, Radius);
			Offset = V(0, 0);
			HexDim = V(HexRad, HexRad*0.5);
		}
		INVALIDDEFAULT();
	}

	v2 Dim = V(HexRad, HexRad);

	idx NumPoints   = Map.NumPoints;
	iv2 *HexPos     = Map.HexPos;
	id_map *Indices = &Map.Indices;

	v2 *Location      = SpawnTiles(Scratch, NumPoints, HexPos, HexDim, Offset);
	neighborhood Hood = MakeNeighbors(Scratch, NumPoints, HexPos, Indices);

	f32 *Heights = MakeHeights(Scratch, NumPoints, &Rand);

	f32 *Smoothed = SmoothHeights(Scratch, NumPoints, Heights, 3, Hood);

	f32 Average   = AverageHeight(NumPoints, Smoothed);

	typedef enum
	{
		PreTileType_FORREST,
		PreTileType_GRASSLAND,
		PreTileType_SWAMP,
	} pre_tile_type;

	pre_tile_type *PreTileTypes = PushArray(Scratch, NumPoints, *PreTileTypes);
	f32 *Noise = PushArray(Scratch, NumPoints, *Noise);

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		Noise[Point] = FractalV2(Options->Params[0].Octaves,
					 F32C(1.0) / (f32) Options->Params[0].OneOverFreq,
					 Options->Params[0].Ampl,
					 Options->Params[0].Lacu,
					 Options->Params[0].Pers,
					 Location[Point]);
		if (Smoothed[Point] > Average)
		{
			PreTileTypes[Point] = PreTileType_FORREST;
		}
		else
		{
			PreTileTypes[Point] = PreTileType_GRASSLAND;
		}
	}

#if 1
	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		switch (PreTileTypes[Point])
		{
			break;case PreTileType_FORREST:
			{
				if (Noise[Point] > 0.0)
				{
					AddTree(Test, Location[Point], Dim);
				}
				else
				{
					PreTileTypes[Point] = PreTileType_GRASSLAND;
				}
			}
			break;case PreTileType_GRASSLAND:
			{
				if (Noise[Point] < -0.2)
				{
					AddSwamp(Test, Location[Point], HexDim);
					PreTileTypes[Point] = PreTileType_SWAMP;
				}
			}
			break;case PreTileType_SWAMP:
			{
				AddSwamp(Test, Location[Point], Dim);
			}
			INVALIDDEFAULT();
		}
	}
#endif

#if 1 // create map

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		entity_id Tile = NextEntity(Test);

		AddPosition(Test, Tile, Location[Point]);
		v4 Color;
#if 0 // height map
		f32 Height = Normalize(-1, Smoothed[Point], 1);
		f32 Value = Height; //1 - Square(1-Height);
		Color = V(Value, Value, Value, 1.0);

#else // color
		switch (PreTileTypes[Point])
		{
			break;case PreTileType_GRASSLAND:
			{
				Color = V(0.50f, 0.73f, 0.40f, 1.0f);
			}
			break;case PreTileType_FORREST:
			{
				Color = V(0.07f, 0.51f, 0.01f, 1.0f);
			}
			break;case PreTileType_SWAMP:
			{
				Color = V(0.00f, 0.24f, 0.04f, 1.0f);
			}
			INVALIDDEFAULT();
		}
#endif
		AddHex(Test, Tile, (hex_data)
		       {
			       .Color = Color,
			       .Dim   = HexDim,
			       .Offset = V(0, 0),
		       });

	}

#else // test neighbors
	idx P = 101;
	{
		entity_id Tile = NextEntity(Test);

		AddPosition(Test, Tile, Location[P]);
		AddRect(Test, Tile, (rect_data)
			{
				.Color = V(0, 1, 1, 1),
				.Dim   = Dim,
				.Offset = V(0, 0),
			});
	}

	for (id_index *Point = &NeighborIds[NeighborsEnd[P - 1]];
	     Point != &NeighborIds[NeighborsEnd[P]];
	     ++Point)
	{
		entity_id Tile = NextEntity(Test);
		AddPosition(Test, Tile, Location[*Point]);
		AddRect(Test, Tile, (rect_data)
			{
				.Color = V(1, 1, 0, 1),
				.Dim   = Dim,
				.Offset = V(0, 0),
			});
	}

#endif
	// AddEdges(Test, NumPoints, HexPos, HexDim, Offset);

	idx NumEdges = Map.NumEdges;
	iv2 *EdgePos = Map.EdgePos;
	iv2 (*EdgeHex)[2] = Map.EdgeHex;
	for (idx Edge = 0;
	     Edge < NumEdges;
	     ++Edge)
	{
		iv2 Pos = EdgePos[Edge];

		/* iv2 Lo = EdgeLo(Pos); */
		/* iv2 Hi = EdgeHi(Pos); */

		iv2 Lo = EdgeHex[Edge][0];
		iv2 Hi = EdgeHex[Edge][1];

		id_index *LoIdx = GetIdSlot(Indices, ID(Lo.X, Lo.Y));
		id_index *HiIdx = GetIdSlot(Indices, ID(Hi.X, Hi.Y));

		if (LoIdx && HiIdx)
		{
			entity_id Edge = NextEntity(Test);
			v2 RealPos = Hadamard(HexDim,
					      HexToLinear(Scale(0.5, ToFloat(Pos))));

			AddLine(Test, Edge, (line_data)
				{
					.Start = Location[*LoIdx],
					.End   = Location[*HiIdx],
					.Color  = V(1, 0, 1, 0.5),
					.Thickness = F32C(0.005),
				});
		}
		else
		{
			id_index *Single = LoIdx ? LoIdx : HiIdx;
			YASSERT(Single);
			id_index Idx = *Single;

			entity_id BorderTile = NextEntity(Test);
			AddPosition(Test, BorderTile, Location[Idx]);
			AddHex(Test, BorderTile, (hex_data)
			       {
				       .Offset = V(0, 0),
				       .Color  = V(1, 0, 0, 1),
				       .Dim    = Scale(F32C(0.3), HexDim),
			       });

			entity_id Edge = NextEntity(Test);
			v2 RealPos = Hadamard(HexDim,
					      Diff(HexToLinear(Scale(0.5, ToFloat(Pos))),
						   Offset));

			AddPosition(Test, Edge, RealPos);
			AddHex(Test, Edge, (hex_data)
			       {
				       .Offset = V(0, 0),
				       .Color  = V(1, 1, 0, 0.5),
				       .Dim    = Scale(F32C(0.3), HexDim),
			       });
		}
	}
}

PRIVATE base_map
MakeBaseMap
(
	memory_arena *Scratch,
	map_gen *Options
)
{
	base_map Result;
	switch (Options->Type)
	{
		break;case BaseMapType_RECT:
		{
			idx Width = Options->Dim.Width;
			idx Height = Options->Dim.Height;

			Result = MakeRectangleBaseMap(Scratch, Options->Dim);
			Result.Middle = HexToLinear(V((f32)Width/F32C(2.0), (f32)Height/F32C(2.0)));
		}
		break;case BaseMapType_CIRCLE:
		{
			idx Radius = Options->Radius;
			Result = MakeRoundBaseMap(Scratch, Radius);
			Result.Middle = V(0, 0);
		}
		INVALIDDEFAULT();
	}
	return Result;
}

PRIVATE void
GenerateMap
(
	entity_test *Test,
	memory_arena *Scratch,
	map_gen *Options
)
{
	rand64 Rand = RandomSeries(Options->Seed);

	f32 HexRad = Options->HexRad;
	v2 HexDim = V(HexRad, HexRad * F32C(0.5));

	base_map Map = MakeBaseMap(Scratch, Options);
	v2 Offset = Map.Middle;

	v2 Dim = V(HexRad, HexRad);

	idx NumPoints   = Map.NumPoints;
	iv2 *HexPos     = Map.HexPos;
	id_map *Indices = &Map.Indices;

	v2 *Location      = SpawnTiles(Scratch, NumPoints, HexPos, HexDim, Offset);
	neighborhood Hood = MakeNeighbors(Scratch, NumPoints, HexPos, Indices);

	f32 *ProtoHeight = PushArray(Scratch, NumPoints, *ProtoHeight);

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		ProtoHeight[Point] = FractalV2(Options->Params[0].Octaves,
					       F32C(0.01) * (f32) Options->Params[0].OneOverFreq,
					       Options->Params[0].Ampl,
					       Options->Params[0].Lacu,
					       Options->Params[0].Pers,
					       Location[Point]);
	}

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		entity_id Tile = NextEntity(Test);

		AddPosition(Test, Tile, Location[Point]);
		f32 Height = Normalize(-Options->Params[0].Ampl,
				       ProtoHeight[Point],
				       Options->Params[0].Ampl);
		v4 Color = V(Height, Height, Height, 1.0); //ColorOfF32(Height, -1, 1);
		AddHex(Test, Tile, (hex_data)
		       {
			       .Color = Color,
			       .Dim   = HexDim,
			       .Offset = V(0, 0),
		       });

	}
}

PRIVATE void
AddBump
(
	edge_record *Record,
	f32 *Values,
	f32 BumpVal
)
{
	idx Origin = Record->Origin;
	Values[Origin] = BumpVal;

	if (BumpVal > 0.1)
	{
		edge_record *Current = Record;

		do
		{
			AddBump(ReverseEdge(Current), Values, BumpVal / 2);
			Current = Current->Next;
		} while (Record != Current);
	}
}

PRIVATE void
RandomPoints
(
	entity_test *Test,
	memory_arena *Scratch,
	map_gen *Options
)
{
	rand64 Rand = RandomSeries(Options->Seed);
	v2 HexDim = V(0.0008, 0.0008);
	rect Bounds = RectOfBounds(-0.8, 0.8, -0.8, 0.8);
	//v2 Points[8];

	idx NumPoints = 1 << 12;
	v2 *PointArrays[2];
	idx Current = 0;

#define SwitchArrays() do { Current = !Current; } while (0)
#define CurrentArray PointArrays[Current]
#define OtherArray PointArrays[!Current]

	CurrentArray = PushArray(Scratch, NumPoints, *CurrentArray);
	OtherArray   = PushArray(Scratch, NumPoints, *OtherArray);

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		CurrentArray[Point] = V(NextBetween(&Rand, Bounds.MinX, Bounds.MaxX),
					NextBetween(&Rand, Bounds.MinY, Bounds.MaxY));
	}


	bool ShowStart = 0;
	bool ShowDelaunay = 0;
	bool ShowVoronoi = 0;
	bool ShowEnd = 1;

	if (ShowStart)
	{
		for (idx Point = 0;
		     Point < NumPoints;
		     ++Point)
		{
			entity_id Middle = NextEntity(Test);
			AddPosition(Test, Middle, CurrentArray[Point]);
			AddHex(Test, Middle, (hex_data)
			       {
				       .Color = V(1, 1, 0, 1),
				       .Dim   = HexDim,
				       .Offset = V(0, 0),
			       });
		}
	}

	INSTRUMENT(CompleteLloydStep)
	{
		memory_arena Temp = SplitRest(Scratch);
		idx NumSteps = 10;
		for (idx Step = 0;
		     Step < NumSteps;
		     ++Step)
		{
			edge_allocator Alloc;
			INSTRUMENT(Delaunay)
			{
				Alloc = ComputeDelaunayTriangulation(&Temp,
								     NumPoints, CurrentArray);
			}

			INSTRUMENT(Lloyd)
			{
				ComputeLloydStep(&Temp, Bounds,
						 NumPoints, CurrentArray, OtherArray,
						 Alloc.FirstUnallocated,
						 Alloc.Base);
			}

			FreeAll(&Temp);
			SwitchArrays();
		}

		AbsorbEmptyArena(Scratch, Temp);
	}

	memory_arena Alloc = SplitRest(Scratch);
	edge_allocator Delaunay = ComputeDelaunayTriangulation(&Alloc,
							       NumPoints,
							       CurrentArray);

	// this should stay between 0 and 1
	f32 *Value = PushArray(&Alloc, NumPoints, *Value);

	for (idx I = 0;
	     I < NumPoints;
	     ++I)
	{
		Value[I] = F32C(0.5);
	}

	idx NumQuadEdges = Delaunay.FirstUnallocated;
	REPEAT(5)
	{
		idx RandomQuadEdge = NextInRange(&Rand, NumQuadEdges);
		quad_edge *QEdge = &Delaunay.Base[RandomQuadEdge];
		edge_record *Edge = &QEdge->Edges[0];
		if (!UsedEdge(Edge))
		{
			RandomQuadEdge = NextInRange(&Rand, NumQuadEdges);
			QEdge = &Delaunay.Base[RandomQuadEdge];
			Edge = &QEdge->Edges[0];
		}
		AddBump(Edge, Value, 0.3);
	}

	WITHTEMP(Temp, &Alloc)
	{
		DrawVoronoiRegions(&Temp, Bounds, NumPoints,
				   CurrentArray, Value,
				   Delaunay.FirstUnallocated,
				   Delaunay.Base,
				   Test);
	}

	for (idx I = 0;
	     I < NumPoints;
	     ++I)
	{
		Value[I] = Clamp(0, Value[I], 1);
	}


	if (ShowEnd)
	{
		for (idx Point = 0;
		     Point < NumPoints;
		     ++Point)
		{
			entity_id Middle = NextEntity(Test);
			AddPosition(Test, Middle, CurrentArray[Point]);
			AddHex(Test, Middle, (hex_data)
			       {
				       .Color = V(1, 1, 1, 1),
				       .Dim   = HexDim,
				       .Offset = V(0, 0),
			       });
		}
	}
#undef SwitchArrays
#undef CurrentArray
#undef OtherArray
	AbsorbEmptyArena(Scratch, Alloc);
}

#undef ID

#if 0
static v4 const BiomeColors[Biome_COUNT] =
{
	[Biome_TUNDRA]    = V(0.83f, 0.76f, 0.28f, 1.0f),
	[Biome_DESERT]    = V(0.95f, 0.79f, 0.55f, 1.0f),
	[Biome_GRASSLAND] = V(0.50f, 0.73f, 0.40f, 1.0f),
	[Biome_DECIDUOUS_FORREST] = V(0.07f, 0.51f, 0.01f, 1.0f),
	[Biome_RAIN_FORREST] = V(0.05f, 0.16f, 0.05f, 1.0f),
	[Biome_SNOW] = V(0.8, 0.8, 0.8, 1.0),
};

#endif

PRIVATE void
RebuildTest(game *Game)
{
	entity_test *Test = &Game->T;
	ClearTest(Test);
#if 0
	MakeForrest(Test,
		    &TState->Scratch,
		    &Game->Options);
#else
	/* GenerateMap(Test, */
	/* 	    &TState->Scratch, */
	/* 	    &Game->Options); */

	RandomPoints(Test,
		     &TState->Scratch,
		     &Game->Options);

#endif

//	entity_id Entity = NextEntity(Test);
//	AddPosition(Test, Entity, V(0, 0));
//	AddHex(Test, Entity, (hex_data) { .Offset = V(0, 0), .Color = V(1, 1, 1, 1), .Dim = V(.05, .05) });
}

GAME_INIT(GameInit)
{
	game_memory *Memory = &Platform->Memory;
	PlatformLayer       = Platform;
	byte *Persistent    = (byte *) Memory->PersistentMemory;

	memory_arena DebugArena = MemoryArena_FromBuffer(Memory->DebugSize,
							 Memory->DebugMemory);
	// InitDebugState(Platform, Memory->DebugSize, Memory->DebugMemory);

	memory_arena PersistentArena;

	InitializeArena(&PersistentArena, Memory->PersistentSize, Persistent);

	game *CurrentGame = PushStruct(&PersistentArena, *CurrentGame);

	/*
	 * Technically the alignment of Persistent could make CurrentGame not
	 * be positioned at the start of the Peristent buffer.  Since
	 * some code relies on that, we have to make sure that this holds.
	 * We could probably prevent this by filling the "padding" with some
	 * sequence, for example repeating 0xBE, and giving the game struct
	 * a header which is not equal to the padding, in this example
	 * we could let the game struct have a header filled with 0xAB.
	 */
	YASSERT((ptr) CurrentGame == (ptr) Persistent);

	byte *Transient = (byte *) Memory->TransientMemory;
	memory_arena TransientArena;
	InitializeArena(&TransientArena, Memory->TransientSize, Transient);

	InitGame(CurrentGame, PersistentArena, DebugArena, &TransientArena);

	FreeAll(&TransientArena);

	transient_state *TranState = PushStruct(&TransientArena, *TranState);
	InitTransientState(CurrentGame, TransientArena, &CurrentGame->Debug, TranState);
	InitTransientWorldState(&CurrentGame->Map.World, TranState,
				&CurrentGame->Table);

	YASSERT((ptr) TranState == (ptr) Transient);

	InitTest(&CurrentGame->T, &CurrentGame->Arena, 1 << 15);

	for (idx I = 0;
	     I < MapParam_COUNT;
	     ++I)
	{
		CurrentGame->Options.Params[I].Octaves = 1;
		CurrentGame->Options.Params[I].OneOverFreq = 10;
		CurrentGame->Options.Params[I].Ampl = 1.0;
		CurrentGame->Options.Params[I].Lacu = 0.5;
		CurrentGame->Options.Params[I].Pers = 0.5;
	}
	CurrentGame->Options.HeatMap = 0;
	CurrentGame->Options.Selected = 0;

	CurrentGame->Options.Type   = BaseMapType_CIRCLE;
	CurrentGame->Options.Radius = 4;
	CurrentGame->Options.Seed   = 1424;
	CurrentGame->Options.HexRad = 0.05f;

	RebuildTest(CurrentGame);
//	MakeForrest(&CurrentGame->T, &TranState->Scratch, &CurrentGame->Options);

	return YARPG_SUCCESS;
}

PRIVATE b32
DoShowRendererInfo(ui_state *Ui, i32 Id, irect *Bounds,
		   renderer_debug_info *Info)
{
	b32 Result;
	if ((Result = BeginWindow(Ui, Id, "Renderer Info", Bounds,
				   WindowOption_RESIZE
				   | WindowOption_TITLE
				   | WindowOption_MOVABLE
				   | WindowOption_CLOSABLE)))
	{
		str Buffer = TEMPSTR(500);

		irect Bounds = Ui->DrawableBounds;

		str_view L1 = VIEWC("Draw Calls");
		str_view L2 = VIEWC("Texture Rate");
		str_view L3 = VIEWC("Index Rate");
		str_view L4 = VIEWC("MB Uploaded");

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(0, 0), IV(1, 1)),
		      TextPosition_LEFT,
		      (u32) GlyphCount(L1), L1.Data,
		      COLOR.Turquoise, 1.0);

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(0, 1), IV(1, 2)),
		      TextPosition_LEFT,
		      (u32) GlyphCount(L2), L2.Data,
		      COLOR.Turquoise, 1.0);

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(0, 2), IV(1, 3)),
		      TextPosition_LEFT,
		      (u32) GlyphCount(L3), L3.Data,
		      COLOR.Turquoise, 1.0);

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(0, 3), IV(1, 4)),
		      TextPosition_LEFT,
		      (u32) GlyphCount(L4), L4.Data,
		      COLOR.Turquoise, 1.0);

		PrintI64Into(Reset(&Buffer), Info->DrawCalls);

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(1, 0), IV(2, 1)),
		      TextPosition_CENTERED,
                      (u32) GlyphCount(ViewOf(Buffer)),
		      Buffer.Data,
		      COLOR.Turquoise, 1.0);

		f32 TextureRate = (f32) (100 * Info->TexturesUsed)
			/ (f32) (Info->IndexCount * Info->DrawCalls);

		PrintF32Into(Reset(&Buffer), TextureRate, (frac_format_spec) { .Precision = 2 });

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(1, 1), IV(2, 2)),
		      TextPosition_CENTERED,
                      (u32) GlyphCount(ViewOf(Buffer)),
                      Buffer.Data,
		      COLOR.Turquoise, 1.0);

		f32 IndexRate = (f32) (100 * Info->IndicesUsed)
			/ (f32) (Info->IndexCount * Info->DrawCalls);

		PrintF32Into(Reset(&Buffer), IndexRate, (frac_format_spec) { .Precision = 2 });

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(1, 2), IV(2, 3)),
		      TextPosition_CENTERED,
                      (u32) GlyphCount(ViewOf(Buffer)),
                      Buffer.Data,
		      COLOR.Turquoise, 1.0);

		f32 MBUploaded = (f32) ((f64) Info->BytesUploaded / ((f64) 1024 * 1024));

		PrintF32Into(Reset(&Buffer), MBUploaded, (frac_format_spec) { .Precision = 2 });

		Label(Ui, GridRegion(Bounds, IV(2, 4), IV(1, 3), IV(2, 4)),
		      TextPosition_CENTERED,
                      (u32) GlyphCount(ViewOf(Buffer)),
                      Buffer.Data,
		      COLOR.Turquoise, 1.0);
	}
	EndWindow(Ui);

	return Result;
}

PRIVATE b32
DoShowMusicWindow(ui_state *Ui, i32 Id, irect *Bounds,
		  f32 Clock,
		  u32 NumCapturedSamples, f32 SecondsToCapture)
{

	b32 Result;

	if ((Result = BeginWindow(Ui, Id, "Music", Bounds,
				  WindowOption_RESIZE
				  | WindowOption_TITLE
				  | WindowOption_MOVABLE
				  | WindowOption_CLOSABLE)))
	{
		memory_arena *Scratch      = &TState->Scratch;

		u32 const SampleRate = Round((f32) NumCapturedSamples / SecondsToCapture);
		u32 const NumSamples = Round((f32) SampleRate * SecondsToCapture);

		f32 *FSamples = PushAlignedArray(Scratch, NumSamples, *FSamples,
						 alignof(__m128));
		ZeroArray(NumSamples, FSamples);

		real_sound_buffer Music;
		Music.NumSamples  = NumSamples;
		Music.SampleRate  = SampleRate;
		Music.NumChannels = 1;
		Music.Data        = FSamples;

		INSTRUMENT(GatherSamples)
		{
			for (playing_sound *Sound = TState->Sounds;
			     Sound;
			     Sound = Sound->Next)
			{

				playing_sound SoundCopy = *Sound;
				idx SamplesPlayed = PlaySamples(&SoundCopy, NumSamples,
								SampleRate,
								FSamples);
			}
		}

		enum WIDGET_ID
		{
			INVALID_ID,
			TEST_ID,
			SINE_ID,
			CLICK_ID,
			STOP_ID,
		};

		INSTRUMENT_COUNTED(DisplaySamples, Music.NumSamples)
		{
			v4 Green = V(0, 1, 0, 1);
			irect Bounds = HorizontalRegion(Ui->DrawableBounds, 5, 0, 4);
			BlockGraph(Ui, Bounds,
				   Music.NumSamples, Music.Data,
				   0, NumSamples,
				   -1, 0, 1, Green);
		}

		irect ButtonBounds = HorizontalRegion(Ui->DrawableBounds, 5, 4, 5);

		f32 Weights[SoundTag_COUNT] = { 0.0 };
		Weights[SoundTag_ID] = 1.0;
		f32 Desired[SoundTag_COUNT] = { 0.0 };
		f32 Volume = 1.0;
		f32 Speed  = 1.0;

		i32 NumButtons = 4;
		i32 CurrentButton = 0;

		if (Button(Ui, TEST_ID, VerticalRegion(ButtonBounds, 5 * NumButtons,
						       CurrentButton * 5 + 1,
						       CurrentButton * 5 + 4), 0, ""))
		{
			Desired[SoundTag_ID] = 0.0;

			sound_id Id = ClosestSoundMatchOf(&TState->Assets,
							  Category_MUSIC,
							  Weights, Desired);

			EnqueueSound(Id, Volume, Speed);
		}
		CurrentButton += 1;

		if (Button(Ui, SINE_ID, VerticalRegion(ButtonBounds, 5 * NumButtons,
						       CurrentButton * 5 + 1,
						       CurrentButton * 5 + 4), 0, ""))
		{
			Desired[SoundTag_ID] = 2.0;

			sound_id Id = ClosestSoundMatchOf(&TState->Assets,
							  Category_MUSIC,
							  Weights, Desired);

			EnqueueSound(Id, Volume, Speed);
		}
		CurrentButton += 1;

		if (Button(Ui, CLICK_ID, VerticalRegion(ButtonBounds, 5 * NumButtons,
						       CurrentButton * 5 + 1,
						       CurrentButton * 5 + 4), 0, ""))
		{
			Desired[SoundTag_ID] = 1.0;

			sound_id Id = ClosestSoundMatchOf(&TState->Assets,
							  Category_MUSIC,
							  Weights, Desired);

			EnqueueSound(Id, Volume, Speed);
		}
		CurrentButton += 1;

		if (Button(Ui, STOP_ID, VerticalRegion(ButtonBounds, 5 * NumButtons,
						       CurrentButton * 5 + 1,
						       CurrentButton * 5 + 4), 4, "Stop"))
		{
			// TODO: BUG: this sometimes creates loops!?
			playing_sound *Current = TState->Sounds;
			TState->Sounds = NULL;

                        playing_sound **LastFreeSound = &TState->FreeSound;
			while (*LastFreeSound)
			{
				LastFreeSound = &(*LastFreeSound)->Next;
			}
			*LastFreeSound = Current;
		}
	}
	EndWindow(Ui);
	return Result;
}

PRIVATE b32
DoShowTestWindow(ui_state *Ui, i32 Id, irect *Bounds,
		 f32 *SliderVal,
		 b32 *CheckBoxVal[2],
		 iv3 Chunk,
		 world *World,
		 f32 Camera[4][4],
		 f32 MouseX,
		 f32 MouseY)
{
	b32 Result;
	if ((Result = BeginWindow(Ui, Id, "Test Window", Bounds,
				  WindowOption_RESIZE
				  | WindowOption_TITLE
				  | WindowOption_CLOSABLE)))
	{
		enum TEST_WINDOW_ID
		{
			INVALID_ID,
			TEST_SLIDER_ID,
			TEST_CHECKBOX_1_ID,
			TEST_CHECKBOX_2_ID,
			SPAWN_BUTTON_ID,
		};
		irect DrawableBounds = Ui->DrawableBounds;

		irect SliderBounds   = VerticalRegion(DrawableBounds, 52, 2, 10);
		irect CheckBoxBounds = VerticalRegion(DrawableBounds, 52, 10, 30);
		irect SpawnBounds    = VerticalRegion(DrawableBounds, 52, 30, 40);
		irect GraphBounds    = VerticalRegion(DrawableBounds, 52, 42, 52);

		Slider(Ui, TEST_SLIDER_ID, HorizontalRegion(SliderBounds, 5, 1, 4),
		       SliderVal, F32C(0.1), F32C(8.0));

		i32 DrawableWidth = DrawableBounds.MaxX
			- DrawableBounds.MinX;
		f32 ButtonSize = F32C(50.0) / (f32) DrawableWidth;
		if (ButtonSize > F32C(1.0)) ButtonSize = F32C(1.0);

		i32 ButtonColumns = (i32) (F32C(1000.0) * ButtonSize);
		i32 Begin         = (1000 - ButtonColumns) / 2;

		irect RowBounds = VerticalRegion(CheckBoxBounds, 13, 1, 6);


		CheckBox(Ui, TEST_CHECKBOX_1_ID,
			 HorizontalRegion(RowBounds, 1000, Begin, 1000 - Begin),
			 CheckBoxVal[0]);

		RowBounds = VerticalRegion(CheckBoxBounds, 13, 7, 12);

		CheckBox(Ui, TEST_CHECKBOX_2_ID,
			 HorizontalRegion(RowBounds, 1000, Begin, 1000 - Begin),
			 CheckBoxVal[1]);

		if (Button(Ui, SPAWN_BUTTON_ID, SpawnBounds, 5, "Spawn"))
		{
			for (i32 i = -3; i <= 3; ++i)
			{
				for (i32 j = -3; j <= 3; ++j)
				{
					if (((i == -3 || i == 3) && (j != 0)) ||
					    ((j == -3 || j == 3) && (i != 0)))
					{
						world_position Pos;
						Pos.Chunk = Chunk;
						Pos.Offset =  V(i, j);
						AddWall(World, Pos);
					}
				}
			}
			Reload(World);
		}

		f32 Inverse[4][4];
		InvertCamera(Inverse, Camera);

                v2 Result = V(0, 0);

		Result.X = Inverse[0][0] * MouseX + Inverse[0][1] * MouseY + Inverse[0][3];
		Result.Y = Inverse[1][0] * MouseX + Inverse[1][1] * MouseY + Inverse[1][3];

		str Buffer = TEMPSTR(128);

		str_view InputStr = VIEWC("Input");
		Reset(&Buffer);
		PrintCharInto(&Buffer, '(');
		PrintF32Into(&Buffer, MouseX, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintViewInto(&Buffer, VIEWC(", "));
		PrintF32Into(&Buffer, MouseY, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintCharInto(&Buffer, ')');

		Label(Ui, GridRegion(GraphBounds, IV(2, 2), IV(0, 0), IV(1, 1)),
		      TextPosition_LEFT,
		      (u32) InputStr.Size, InputStr.Data, COLOR.Turquoise, 1.0);

		Label(Ui, GridRegion(GraphBounds, IV(2, 2), IV(0, 1), IV(1, 2)),
		      TextPosition_LEFT,
		      (u32) GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);

		str_view WorldStr = VIEWC("World");
		Reset(&Buffer);
		PrintCharInto(&Buffer, '(');
		PrintF32Into(&Buffer, MouseX, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintViewInto(&Buffer, VIEWC(", "));
		PrintF32Into(&Buffer, MouseY, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintCharInto(&Buffer, ')');
		Label(Ui, GridRegion(GraphBounds, IV(2, 2), IV(1, 0), IV(2, 1)),
		      TextPosition_LEFT,
		      (u32) WorldStr.Size, WorldStr.Data, COLOR.Turquoise, 1.0);
		Label(Ui, GridRegion(GraphBounds, IV(2, 2), IV(1, 1), IV(2, 2)),
		      TextPosition_LEFT,
		      (u32) GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
	}
	EndWindow(Ui);

	return Result;
}

PRIVATE b32
DoShowMemoryViewer(ui_state *Ui, i32 Id, irect *Bounds,
		   memory_arena *Persistent,
		   memory_arena *Transient,
		   memory_arena *Scratch)
{
	b32 Result = 0;
#if defined(DEBUG)
	if (BeginWindow(Ui, Id, "Memory", Bounds,
			WindowOption_TRANSPARENT | WindowOption_MOVABLE
			| WindowOption_TITLE | WindowOption_CLOSABLE
			| WindowOption_RESIZE))
	{
		Result = 1;
		i32 NumRows = 40;
		irect DrawableBounds = Ui->DrawableBounds;

		str Buffer = TEMPSTR(128);

		irect Bounds = VerticalRegion(DrawableBounds, NumRows, 1, 8);

		str_view TransientStr  = VIEWC("Transient:");
		str_view PersistentStr = VIEWC("Persistent:");
		str_view ScratchStr    = VIEWC("Scratch:");
		str_view PeakStr       = VIEWC("Peak:");

		Label(Ui, HorizontalRegion(Bounds, 20, 0, 11),
		      TextPosition_LEFT, (u32) TransientStr.Size, TransientStr.Data, COLOR.Turquoise, 1.0);

		Reset(&Buffer);
		PrintF32Into(&Buffer, (f32) 100 * Transient->Peak / (f32) Transient->Size,
			     (frac_format_spec) { .Min = 5, .Precision = 2 });
		Label(Ui, HorizontalRegion(Bounds, 20, 12, 19),
		      TextPosition_LEFT, GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
		Label(Ui, HorizontalRegion(Bounds, 20, 19, 20),
		      TextPosition_RIGHT, 1, "%", COLOR.Turquoise, 1.0);

		Bounds = VerticalRegion(DrawableBounds, NumRows, 11, 18);
		Label(Ui, HorizontalRegion(Bounds, 20, 0, 11),
		      TextPosition_LEFT, PersistentStr.Size, PersistentStr.Data, COLOR.Turquoise, 1.0);
		Reset(&Buffer);
		PrintF32Into(&Buffer, (f32) 100 * Persistent->Peak / (f32) Persistent->Size,
			     (frac_format_spec) { .Min = 5, .Precision = 2 });
		Label(Ui, HorizontalRegion(Bounds, 20, 12, 19),
		      TextPosition_LEFT, GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
		Label(Ui, HorizontalRegion(Bounds, 20, 19, 20),
		      TextPosition_RIGHT, 1, "%", COLOR.Turquoise, 1.0);

		Bounds = VerticalRegion(DrawableBounds, NumRows, 21, 28);
		Label(Ui, HorizontalRegion(Bounds, 20, 0, 11),
		      TextPosition_LEFT, ScratchStr.Size, ScratchStr.Data, COLOR.Turquoise, 1.0);
		Reset(&Buffer);
		PrintF32Into(&Buffer, (f32) 100 * Scratch->Peak / (f32) Scratch->Size,
			     (frac_format_spec) { .Min = 5, .Precision = 2 });
		Label(Ui, HorizontalRegion(Bounds, 20, 12, 19),
		      TextPosition_LEFT, GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
		Label(Ui, HorizontalRegion(Bounds, 20, 19, 20),
		      TextPosition_RIGHT, 1, "%", COLOR.Turquoise, 1.0);

		Bounds = VerticalRegion(DrawableBounds, NumRows, 31, 38);
		Label(Ui, HorizontalRegion(Bounds, 20, 0, 11),
		      TextPosition_LEFT, PeakStr.Size, PeakStr.Data, COLOR.Turquoise, 1.0);
		Reset(&Buffer);
		PrintU64Into(&Buffer,
			     (Transient->Peak - Scratch->Size + Persistent->Peak + Scratch->Peak) >> 10);
		Label(Ui, HorizontalRegion(Bounds, 20, 12, 19),
		      TextPosition_LEFT, GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
		Label(Ui, HorizontalRegion(Bounds, 20, 19, 20),
		      TextPosition_RIGHT, 2, "KB", COLOR.Turquoise, 1.0);
	}

	EndWindow(Ui);
#endif

	return Result;
}

PRIVATE b32
DoShowCameraPosition(ui_state *Ui, i32 Id, irect *Bounds,
		     f32 CameraMatrix[4][4],
		     world_position Camera)
{

	v2 CameraVel = V(0, 0);
	v2 CameraAccel = V(0, 0);

	b32 Result = 0;
	if (BeginWindow(Ui, Id, "Position", Bounds,
		    WindowOption_RESIZE | WindowOption_CLOSABLE | WindowOption_TITLE | WindowOption_MOVABLE))
	{
                i32 NumRows = 4;
		Result = 1;
		str Buffer = TEMPSTR(128);
		str_view ChunkStr = VIEWC("Chunk:");
		str_view OffsetStr = VIEWC("Offset:");
		str_view VelStr = VIEWC("Velocity:");
		str_view AccStr = VIEWC("Accel:");

		irect DrawableBounds = Ui->DrawableBounds;

		irect StuffBounds = VerticalRegion(DrawableBounds, 2, 0, 1);
		irect MatrixBounds = VerticalRegion(DrawableBounds, 2, 1, 2);

		irect Bounds = VerticalRegion(StuffBounds, NumRows, 0, 1);

		Label(Ui, HorizontalRegion(Bounds, 2, 0, 1), TextPosition_LEFT,
		      ChunkStr.Size, ChunkStr.Data, COLOR.Turquoise, 1.0);
		FormatInto(Reset(&Buffer), VIEWC("{i32}, {i32}, {i32}"),
			   Camera.Chunk.X, Camera.Chunk.Y, Camera.Chunk.Z);
		Label(Ui, HorizontalRegion(Bounds, 2, 1, 2), TextPosition_RIGHT,
		      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);

		Bounds = VerticalRegion(StuffBounds, NumRows, 1, 2);
		Label(Ui, HorizontalRegion(Bounds, 2, 0, 1), TextPosition_LEFT,
		      OffsetStr.Size, OffsetStr.Data, COLOR.Turquoise, 1.0);


		Reset(&Buffer);
		PrintF32Into(&Buffer, Camera.Offset.X, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintCharInto(&Buffer, ' ');
		PrintF32Into(&Buffer, Camera.Offset.Y, (frac_format_spec) { .Min = 2, .Precision = 2 });

		Label(Ui, HorizontalRegion(Bounds, 2, 1, 2), TextPosition_RIGHT,
		      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);

		Bounds = VerticalRegion(StuffBounds, NumRows, 2, 3);
		Label(Ui, HorizontalRegion(Bounds, 2, 0, 1), TextPosition_LEFT,
		      9, "Velocity:", COLOR.Turquoise, 1.0);
		Reset(&Buffer);
		PrintF32Into(&Buffer, CameraVel.X, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintCharInto(&Buffer, ' ');
		PrintF32Into(&Buffer, CameraVel.Y, (frac_format_spec) { .Min = 2, .Precision = 2 });
		Label(Ui, HorizontalRegion(Bounds, 2, 1, 2), TextPosition_RIGHT,
		      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);

		Bounds = VerticalRegion(StuffBounds, NumRows, 3, 4);
		Label(Ui, HorizontalRegion(Bounds, 2, 0, 1), TextPosition_LEFT,
		      6, "Accel:", COLOR.Turquoise, 1.0);
		Reset(&Buffer);
		PrintF32Into(&Buffer, CameraAccel.X, (frac_format_spec) { .Min = 2, .Precision = 2 });
		PrintCharInto(&Buffer, ' ');
		PrintF32Into(&Buffer, CameraAccel.Y, (frac_format_spec) { .Min = 2, .Precision = 2 });
		Label(Ui, HorizontalRegion(Bounds, 2, 1, 2), TextPosition_RIGHT,
		      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);

		for (i32 Y = 0;
		     Y < 4;
		     ++Y)
		{
			for (i32 X = 0;
			     X < 4;
			     ++X)
			{
				Bounds = GridRegion(MatrixBounds, IV(8, 4), IV(X, Y), IV(X+1, Y+1));
				PrintF32Into(Reset(&Buffer), CameraMatrix[Y][X],
					     (frac_format_spec) { .Min = 2, .Precision = 2 });
				Label(Ui, Bounds, TextPosition_CENTERED,
				      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
			}
		}

		f32 Inverse[4][4];

		InvertCamera(Inverse, CameraMatrix);

		for (i32 Y = 0;
		     Y < 4;
		     ++Y)
		{
			for (i32 X = 0;
			     X < 4;
			     ++X)
			{
				Bounds = GridRegion(MatrixBounds, IV(8, 4), IV(4+X, Y), IV(4+X+1, Y+1));
				PrintF32Into(Reset(&Buffer), Inverse[Y][X],
					     (frac_format_spec) { .Min = 2, .Precision = 2 });
				Label(Ui, Bounds, TextPosition_CENTERED,
				      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
			}
		}
	}

	EndWindow(Ui);
	return Result;
}

PRIVATE b32
DoShowFPS(ui_state *Ui, i32 Id, irect *Bounds,
	  f64 FrameTime)
{
	double FPS = 1.0 / FrameTime;

        b32 Result;
	if ((Result = BeginWindow(Ui, Id, "FPS", Bounds,
				  WindowOption_CLOSABLE
				  | WindowOption_TITLE
				  | WindowOption_MOVABLE)))
	{
		str Buffer = TEMPSTR(8);

		PrintF32Into(&Buffer, FPS, (frac_format_spec) { .Min = 3, .Precision = 3 });

                Label(Ui, Ui->DrawableBounds, TextPosition_CENTERED,
		      GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
	}
	EndWindow(Ui);
	return Result;
}

/* PRIVATE u32 */
/* DebugTimingBlockParentId(debug_timing_block *Block) */
/* { */
/* 	u32 Id = INVALID_PERF_ID; */
/* 	if (Block->Parent) */
/* 	{ */
/* 		Id = Block->Parent->Id; */
/* 	} */
/* 	return Id; */
/* } */

/* PRIVATE debug_frame * */
/* PreviousFrame(debug_state *Debug, debug_frame *Frame) */
/* { */
/* 	debug_frame *PrevFrame; */

/* 	if (Frame == &Debug->Frames[0]) */
/* 	{ */
/* 		PrevFrame = &Debug->Frames[Debug->NumFrames - 1]; */
/* 	} */
/* 	else */
/* 	{ */
/* 		PrevFrame = Frame - 1; */
/* 	} */

/* 	return PrevFrame; */
/* } */

/* PRIVATE i32 */
/* DrawRegion(debug_state *Debug, ui_state *Ui, */
/* 	   debug_timing_block *Region, v4 Color, */
/* 	   irect Bounds, u64 FrameStart, u64 FrameEnd) */
/* { */
/* 	b32 Result = 0; */
/* 	b32 Open = (Region->EndTime == 0); */
/* 	if (!Open && (Region->EndTime < FrameStart)) return Result; */

/* 	f32 StartP, EndP; */

/* 	f64 FrameTime = (FrameEnd - FrameStart); */

/* 	if (Region->StartTime < FrameStart) */
/* 	{ */
/* 		StartP = 0.0f; */
/* 	} */
/* 	else */
/* 	{ */
/* 		StartP = (Region->StartTime - FrameStart) / FrameTime; */
/* 	} */

/* 	if (Open) */
/* 	{ */
/* 		EndP = 1.0; */
/* 	} */
/* 	else if (Region->EndTime > FrameEnd) */
/* 	{ */
/* 		EndP = 1.0f; */
/* 	} */
/* 	else */
/* 	{ */
/* 		EndP = (Region->EndTime - FrameStart) / FrameTime; */
/* 	} */

/* 	/\* YASSERT(StartP <= 1.0); *\/ */
/* 	/\* YASSERT(EndP <= 1.0); *\/ */

/* 	/\* StartP = Clamp(0.0, StartP, 1.0); *\/ */
/* 	/\* EndP   = Clamp(0.0, EndP, 1.0); *\/ */

/* 	f32 StartX = Lerp(Bounds.MinX, StartP, Bounds.MaxX); */
/* 	f32 EndX   = Lerp(Bounds.MinX, EndP, Bounds.MaxX); */

/* 	irect Block = Bounds; */
/* 	Block.MinX = StartX; */
/* 	Block.MaxX = EndX; */


/* 	if (TestAndSetHotWidget(Ui, Region->BlockId, Block)) */
/* 	{ */
/* 		Result |= (1 << 0); */
/* 	} */

/* 	b32 Active */
/* 		= TestAndSetActiveWidget(Ui, */
/* 					 Region->BlockId, */
/* 					 Block); */
/* 	b32 WontBeActive */
/* 		= WillNotBeActiveWidget(Ui, Region->BlockId); */
/* 	b32 WasClicked = Active && WontBeActive; */

/* 	if (WasClicked) */
/* 	{ */
/* 		Result |= (1 << 1); */
/* 	} */
/* 	DrawIRect(Ui, Block, Color); */

/* 	return Result; */
/* } */

PRIVATE idx
PartitionPerfIds(idx Start, idx End, idx *Ids, u64 *Vals)
{
	u64 Pivot = Vals[Ids[End - 1]];
	idx PivotIdx = Start - 1;

	for (idx Current = Start;
	     Current < End - 1;
	     ++Current)
	{
		if (Vals[Ids[Current]] >= Pivot)
		{
			PivotIdx += 1;
			idx Tmp = Ids[PivotIdx];
			Ids[PivotIdx] = Ids[Current];
			Ids[Current] = Tmp;
		}
	}

	PivotIdx += 1;
	idx Tmp = Ids[PivotIdx];
	Ids[PivotIdx] = Ids[End - 1];
	Ids[End - 1] = Tmp;

	return PivotIdx;
}

PRIVATE void
SortPerfIds(idx Start, idx End, idx *Ids, u64 *Vals)
{
	if (Start >= End || Start < 0) return;

	idx Pivot = PartitionPerfIds(Start, End, Ids, Vals);

	SortPerfIds(Start, Pivot, Ids, Vals);
	SortPerfIds(Pivot + 1, End, Ids, Vals);
}

typedef enum
{
	PerformanceFunctionViewType_CLOCK,
	PerformanceFunctionViewType_CYCLES,
	PerformanceFunctionViewType_WORKCLOCK,
	PerformanceFunctionViewType_WORKCYCLES,
} performance_function_view_type;

PRIVATE void
ShowPerformanceFunctions
(
	ui_state *Ui,
	irect DrawableBounds,
	debug_info *Debug,
	debug_timeline *ToView,
	idx FreeId
)
{
	debug_timeline_view *View = &ToView->View;
	performance_function_view_type ViewType;
	if (View->SortByCycles)
	{
		if (View->ShowComplete)
		{
			ViewType = PerformanceFunctionViewType_CYCLES;
		}
		else
		{
			ViewType = PerformanceFunctionViewType_WORKCYCLES;
		}
	}
	else
	{
		if (View->ShowComplete)
		{
			ViewType = PerformanceFunctionViewType_CLOCK;
		}
		else
		{
			ViewType = PerformanceFunctionViewType_WORKCLOCK;
		}
	}

	idx ThreadId = 0;

	memory_arena Tmp = SplitRest(Debug->Arena);

	u64 *NumEvents  = PushZeroArray(&Tmp, Debug->NumIds, *NumEvents);
	u64 *Hits       = PushZeroArray(&Tmp, Debug->NumIds, *Hits);
	u64 *Time       = PushZeroArray(&Tmp, Debug->NumIds, *Time);
	u64 *WorkTime   = PushZeroArray(&Tmp, Debug->NumIds, *WorkTime);
	u64 *Cycles     = PushZeroArray(&Tmp, Debug->NumIds, *Cycles);
	u64 *WorkCycles = PushZeroArray(&Tmp, Debug->NumIds, *Cycles);
	debug_location const **Location = PushZeroArray(&Tmp, Debug->NumIds, *Location);

	debug_thread *Thread = &ToView->Threads[ThreadId];
	debug_timing_block *Sentinel = &Thread->Sentinel;

	perf_ts Start = ToView->Start;
	perf_ts End   = ToView->End;
	u64 MaxTime   = End.Clock - Start.Clock;
	i32 Level = 0;
	debug_timing_block *Last = Sentinel;
	bool InsideZoomed = ToView->View.Zoomed == Sentinel->Loc;
	i32 ZoomLevel = 0;
	idx *SortedIds = PushArray(&Tmp, Debug->NumIds, *SortedIds);
	idx FoundIds = 0;

	INSTRUMENT(FunThread)
	{
		for (debug_timing_block *Current = Sentinel->Next;
		     Current != Sentinel;
		     Current = Current->Next)
		{
			if (Last == Current->Parent)
			{
				Level += 1;
			}
			else
			{
				while (Last->Parent != Current->Parent)
				{
					YASSERT(Level > 0);
					Level -= 1;
					Last = Last->Parent;
				}
			}
			Last = Current;
			YASSERT(Level >= 0);
			if (Current->Loc == ToView->View.Zoomed)
			{
				InsideZoomed = 1;
				ZoomLevel = Level;
			}
			else if (Level <= ZoomLevel)
			{
				InsideZoomed = 0;
			}

			if (!InsideZoomed) continue;

			id_index Idx = *GetIdSlot(&Debug->LocToId, (entity_id) {(ptr)(Current->Loc)});

			u64 HitCount = Current->HitCount;
			u64 EventTime, EventCycles;

			if (!IsBlockOpen(Current))
			{
				EventTime = Current->End.Clock - Current->Start.Clock;
				EventCycles = Current->End.Cycles - Current->Start.Cycles;
			}
			else
			{
				EventTime   = End.Clock - Current->Start.Clock;
				EventCycles = End.Cycles - Current->Start.Cycles;
			}

			NumEvents[Idx] += 1;
			Hits[Idx] += HitCount;
			Time[Idx] += EventTime;
			WorkTime[Idx] += EventTime;
			Cycles[Idx] += EventCycles;
			WorkCycles[Idx] += EventCycles;
			Location[Idx] = Current->Loc;

			if (NumEvents[Idx] == 1)
			{
				SortedIds[FoundIds++] = Idx;
			}

			debug_timing_block *Parent = Current->Parent;
			if (Parent != Sentinel)
			{
				id_index ParentIdx = *GetIdSlot(&Debug->LocToId,
								(entity_id) {(ptr)(Parent->Loc)});
				WorkTime[ParentIdx] -= EventTime;
				WorkCycles[ParentIdx] -= EventCycles;
			}
		}
	}

	if (FoundIds > 0)
	{
		idx MaxEventsShown = 10;
		idx NumEventsShown = Min(MaxEventsShown, FoundIds);

		u64 *SelectedCriterion;
		u64 *SelectedTime;
		u64 *SelectedCycles;

		idx TimeSpan;

		switch (ViewType)
		{
			break;case PerformanceFunctionViewType_CLOCK:
			{
				SelectedCriterion = Time;
				SelectedTime = Time;
				SelectedCycles = Cycles;
				TimeSpan = End.Clock - Start.Clock;
			}
			break;case PerformanceFunctionViewType_CYCLES:
			{
				SelectedCriterion = Cycles;
				SelectedTime = Time;
				SelectedCycles = Cycles;
				TimeSpan = End.Cycles - Start.Cycles;
			}
			break;case PerformanceFunctionViewType_WORKCLOCK:
			{
				SelectedCriterion = WorkTime;
				SelectedTime = WorkTime;
				SelectedCycles = WorkCycles;
				TimeSpan = End.Clock - Start.Clock;
			}
			break;case PerformanceFunctionViewType_WORKCYCLES:
			{
				SelectedCriterion = WorkCycles;
				SelectedTime = WorkTime;
				SelectedCycles = WorkCycles;
				TimeSpan = End.Cycles - Start.Cycles;
			}
			INVALIDDEFAULT();

		}
		if (ToView->View.Zoomed != &SentinelLocation)
		{
			id_index ZoomedId = *GetIdSlot(&Debug->LocToId, (entity_id) {(ptr)(ToView->View.Zoomed)});
			if (View->SortByCycles)
			{
				TimeSpan = Cycles[ZoomedId];
			}
			else
			{
				TimeSpan = Time[ZoomedId];
			}
		}

		SortPerfIds(0, FoundIds, SortedIds, SelectedCriterion);

		INSTRUMENT(DrawFns)
		{
			u64 Max = TimeSpan;//SelectedCriterion[SortedIds[0]];

			for (idx I = 0;
			     I < NumEventsShown;
			     ++I)
			{
				idx Id = SortedIds[I];
				if ((f64)SelectedCriterion[Id] <= 0.001 * Max)
				{
					NumEventsShown = I;
					break;
				}
			}

			irect HeaderBounds = VerticalRegion(DrawableBounds, 10, 0, 1);
			irect DataBounds = VerticalRegion(DrawableBounds, 10, 1, 10);

			irect GraphBounds = VerticalRegion(DataBounds, 10, 0, 8);
			irect TextBounds  = VerticalRegion(DataBounds, 10, 8, 10);

			irect NameBounds  = HorizontalRegion(TextBounds, 10, 0, 6);
			irect TotalBounds = HorizontalRegion(TextBounds, 10, 6, 10);

			idx SelectedId = -1;

			enum WIDGET_ID
			{
				CYCLE_SWITCH,
				CLOCK_SWITCH,
				DELTA_SWITCH,
				FREE_ID,
			};

			str_view ButtonText[] =
				{
					VIEWC("Complete"),
					VIEWC("Delta"),
				};

			str_view BText = ButtonText[View->ShowComplete];

			if (Button(Ui, FreeId + DELTA_SWITCH,
				   HorizontalRegion(HeaderBounds, 3, 0, 1),
				   GlyphCount(BText), BText.Data))
			{
				View->ShowComplete ^= 1;
			}

			if (View->SortByCycles)
			{
				if (Button(Ui, FreeId + CYCLE_SWITCH, HorizontalRegion(HeaderBounds, 3, 1, 2),
					   5, "Clock"))
				{
					View->SortByCycles = 0;
				}
				Label(Ui, HorizontalRegion(HeaderBounds, 3, 2, 3), TextPosition_CENTERED,
				      6, "Cycles", COLOR.White, 1.0);
			}
			else
			{
				Label(Ui, HorizontalRegion(HeaderBounds, 3, 1, 2), TextPosition_CENTERED,
				      5, "Clock", COLOR.White, 1.0);
				if (Button(Ui, FreeId + CLOCK_SWITCH, HorizontalRegion(HeaderBounds, 3, 2, 3),
					   6, "Cycles"))
				{
					View->SortByCycles = 1;
				}
			}

			for (idx I = 0;
			     I < NumEventsShown;
			     ++I)
			{
				irect LineBounds = VerticalRegion(GraphBounds,
								  MaxEventsShown, I, I + 1);
				irect FunBounds = HorizontalRegion(LineBounds,
								   3, 0, 1);
				irect TBounds   = HorizontalRegion(LineBounds,
								   3, 1, 2);

				irect CBounds   = HorizontalRegion(LineBounds,
								   3, 2, 3);

				idx Id = SortedIds[I];

				irect BarBounds = FunBounds;

				BarBounds.MaxX = (FunBounds.MaxX - FunBounds.MinX)
					* ((f32)SelectedCriterion[Id] / (f32) Max)
					+ FunBounds.MinX;

				if (TestAndSetHotWidget(Ui, FreeId + FREE_ID + Id, LineBounds))
				{
					YASSERT(SelectedId == -1);
					SelectedId = Id;
				}
				DrawIRect(Ui, BarBounds, ColorOfId(Id));

				bool Active = TestAndSetActiveWidget(Ui, FreeId + FREE_ID + Id, LineBounds);
				bool WontBeActive = WillNotBeActiveWidget(Ui, FreeId + FREE_ID + Id);
				bool WasClicked = Active && WontBeActive;

				if (WasClicked)
				{
					ToView->View.Zoomed = Location[Id];
				}

				str Buffer = TEMPSTR(20);

				PrintF32Into(&Buffer, (f32) (SelectedTime[Id] / 1e6),
					     (frac_format_spec) { .Precision = 2 });

				PrintViewInto(&Buffer, VIEWC("ms"));

				Text(Ui, TBounds,
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)),
				     Buffer.Data,
				     COLOR.White, 1.0);

				Reset(&Buffer);

				PrintF32Into(&Buffer, (f32) (SelectedCycles[Id] / 1e6),
					     (frac_format_spec) { .Precision = 2 });

				PrintViewInto(&Buffer, VIEWC("MC"));

				Text(Ui, CBounds,
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)),
				     Buffer.Data,
				     COLOR.White, 1.0);

				Reset(&Buffer);

				PrintF32Into(&Buffer, (f32) ((f64) (100 * SelectedCriterion[Id]) / (f64) Max),
					     (frac_format_spec) { .Precision = 2 });

				PrintViewInto(&Buffer, VIEWC("%"));

				Text(Ui, FunBounds,
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)),
				     Buffer.Data,
				     COLOR.White, 1.0);
			}

			if (SelectedId != -1)
			{
				debug_location const *SelectedSource = Location[SelectedId];
				Text(Ui, NameBounds,
				     TextPosition_CENTERED,
				     CStringLength(SelectedSource->Name),
				     SelectedSource->Name, COLOR.White, 1.0);
			}

			str Buffer = TEMPSTR(20);

			switch (ViewType)
			{
				break;case PerformanceFunctionViewType_CLOCK:
				{
					PrintF32Into(&Buffer, (f32) (TimeSpan / 1000000.0),
						     (frac_format_spec) { .Precision = 2 });

					PrintViewInto(&Buffer, VIEWC("ms"));
				}
				break;case PerformanceFunctionViewType_CYCLES:
				{
					PrintF32Into(&Buffer, (f32) (TimeSpan / 1000000.0),
						     (frac_format_spec) { .Precision = 2 });

					PrintViewInto(&Buffer, VIEWC("MC"));
				}
				break;case PerformanceFunctionViewType_WORKCLOCK:
				{
					PrintF32Into(&Buffer, (f32) (TimeSpan / 1000000.0),
						     (frac_format_spec) { .Precision = 2 });

					PrintViewInto(&Buffer, VIEWC("ms"));
				}
				break;case PerformanceFunctionViewType_WORKCYCLES:
				{
					PrintF32Into(&Buffer, (f32) (TimeSpan / 1000000.0),
						     (frac_format_spec) { .Precision = 2 });

					PrintViewInto(&Buffer, VIEWC("MC"));
				}

			}

			Text(Ui, TotalBounds,
			     TextPosition_CENTERED,
			     GlyphCount(ViewOf(Buffer)),
			     Buffer.Data,
			     COLOR.White, 1.0);
		}
	}
	AbsorbEmptyArena(Debug->Arena, Tmp);
}

PRIVATE i32
DrawRegion(debug_timeline *Viewing, ui_state *Ui,
	   debug_timing_block *Region, v4 Color,
	   irect Bounds, range View, idx Id)
{
	i32 HotClicked = 0;
	idx StartTime = Region->Start.Clock;
	idx EndTime   = Region->End.Clock;
	b32 Open = IsBlockOpen(Region);
	if (!Open && (EndTime < View.Begin)) return HotClicked;
	if (StartTime > View.End) return HotClicked;

	f32 StartP, EndP;

	idx Current = Viewing->End.Clock;

	f64 ViewTime = (View.End - View.Begin);

	if (StartTime < View.Begin)
	{
		StartP = 0.0f;
	}
	else
	{
		StartP = (StartTime - View.Begin) / ViewTime;
	}

	if (Open)
	{
		EndTime = Current;
	}

	if (EndTime > View.End)
	{
		EndP = 1.0f;
	}
	else
	{
		EndP = (EndTime - View.Begin) / ViewTime;
	}

	/* YASSERT(StartP <= 1.0); */
	/* YASSERT(EndP <= 1.0); */

	/* StartP = Clamp(0.0, StartP, 1.0); */
	/* EndP   = Clamp(0.0, EndP, 1.0); */

	f32 StartX = Lerp(Bounds.MinX, StartP, Bounds.MaxX);
	f32 EndX   = Lerp(Bounds.MinX, EndP, Bounds.MaxX);

	irect Block = Bounds;
	Block.MinX = StartX;
	Block.MaxX = EndX;

	if (TestAndSetHotWidget(Ui, Id, Block))
	{
		HotClicked |= (1 << 0);
	}

	bool Active = TestAndSetActiveWidget(Ui, Id, Block);
	bool WontBeActive = WillNotBeActiveWidget(Ui, Id);
	bool WasClicked = Active && WontBeActive;

	if (WasClicked)
	{
		HotClicked |= (1 << 1);
	}
	DrawIRect(Ui, Block, Color);

	return HotClicked;
}

PRIVATE void
ShowThreadTimeline
(
	ui_state *Ui,
	irect DrawableBounds,
	debug_info *Debug,
	debug_timeline *ToView,
	idx FreeId
)
{
	idx NumThreads = Debug->NumThreads;

	debug_timeline_view *View = &ToView->View;

	debug_timing_block *Selected = NULL;

	irect ViewBounds  = VerticalRegion(DrawableBounds, 12, 0, 8);
	irect TextBounds  = VerticalRegion(DrawableBounds, 12, 8, 11);
	irect FocusBounds = VerticalRegion(DrawableBounds, 12, 11, 12);

	enum WIDGET_ID
	{
		MIN_FOCUS_ID,
		MAX_FOCUS_ID,
		MOVE_FOCUS_ID,
		FREE_ID
	};

	for (idx ThreadId = 0;
	     ThreadId < NumThreads;
	     ++ThreadId)
	{
		debug_thread *Thread = &ToView->Threads[ThreadId];
		irect ThreadBounds = VerticalRegion(ViewBounds, NumThreads,
						    ThreadId, ThreadId + 1);

		for (debug_timing_block *Current = Thread->Sentinel.Next;
		     Current != &Thread->Sentinel;
		     Current = Current->Next)
		{
			if (Current->Parent->Loc == View->Zoomed)
			{
				v4 BlockColor = ColorOfId(Current->Loc->Id);
				idx BlockId = (idx) (ptr) Current;
				i32 Interaction = DrawRegion(ToView, Ui, Current,
							     BlockColor,
							     ThreadBounds,
							     View->Focus,
							     BlockId + FreeId + FREE_ID);
				if (Interaction & (1 << 0))
				{
					Selected = Current;
				}

				if (Interaction & (1 << 1))
				{
					View->Zoomed = Current->Loc;
				}
			}
			else if (Current->Loc == View->Zoomed)
			{
				v4 BlockColor = V(0.5, 0.5, 0.5, 0.8);
				idx BlockId = (idx) (ptr) Current;
				i32 Interaction = DrawRegion(ToView, Ui, Current,
							     BlockColor,
							     ThreadBounds,
							     View->Focus,
							     BlockId + FreeId + FREE_ID);
				/* if (Interaction & (1 << 0)) */
				/* { */
				/* 	Selected = Current; */
				/* } */

				/* if (Interaction & (1 << 1)) */
				/* { */
				/* 	View->Zoomed = Current->Parent->Loc;; */
				/* } */
			}
		}
	}

	{
		range Focus = View->Focus;
		range Viewable = ToView->CapturedTime;
		idx FocusStart = Focus.Begin - Viewable.Begin;
		idx FocusEnd   = Focus.End   - Viewable.Begin;
		idx Range      = Viewable.End - Viewable.Begin;

		f64 StartPercent = (f64) FocusStart / (f64) Range;
		f64 EndPercent = (f64) FocusEnd / (f64) Range;

		idx BoxWidth = FocusBounds.MaxX - FocusBounds.MinX;

		idx StartX = FocusBounds.MinX + StartPercent * BoxWidth;
		idx EndX   = FocusBounds.MinX + EndPercent * BoxWidth;

		irect ActiveRect;
		ActiveRect.MinX = StartX + 5;
		ActiveRect.MaxX = EndX - 5;
		ActiveRect.MinY = FocusBounds.MinY;
		ActiveRect.MaxY = FocusBounds.MaxY;

		irect MinRect;
		MinRect.MinX = StartX;
		MinRect.MaxX = StartX + 5;
		MinRect.MinY = FocusBounds.MinY;
		MinRect.MaxY = FocusBounds.MaxY;

		irect MaxRect;
		MaxRect.MinX = EndX - 5;
		MaxRect.MaxX = EndX;
		MaxRect.MinY = FocusBounds.MinY;
		MaxRect.MaxY = FocusBounds.MaxY;

		DrawIRect(Ui, ActiveRect, COLOR.White);
		DrawIRect(Ui, MinRect, COLOR.Yellow);
		DrawIRect(Ui, MaxRect, COLOR.Yellow);

		{
			bool Hot = TestAndSetHotWidget(Ui, FreeId + MIN_FOCUS_ID, MinRect);
			bool Active = TestAndSetActiveWidget(Ui,
							    FreeId + MIN_FOCUS_ID,
							    MinRect);

			if (Hot)
			{
				YASSERT(Selected == NULL);
				str Buffer = TEMPSTR(255);

				FormatInto(&Buffer, VIEWC("{time} - {time}"),
					   Focus.Begin, Focus.End);

				Text(Ui, VerticalRegion(TextBounds, 2, 1, 2),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0);
			}

			if (Active)
			{
				f32 Movement = (f32) (Ui->Next.Mouse.Pos.X - Ui->Current.Mouse.Pos.X);
				f32 Delta    = (Movement * Range) / BoxWidth;
				idx NewFocusBegin = Delta + Focus.Begin;

				if (NewFocusBegin < Viewable.Begin)
				{
					NewFocusBegin = Viewable.Begin;
				}

				if (NewFocusBegin > Focus.End - 10)
				{
					NewFocusBegin = Focus.End - 10;
				}

				View->Focus.Begin = NewFocusBegin;
			}
		}

		{
			bool Hot = TestAndSetHotWidget(Ui, FreeId + MAX_FOCUS_ID, MaxRect);
			bool Active = TestAndSetActiveWidget(Ui,
							    FreeId + MAX_FOCUS_ID,
							    MaxRect);

			if (Hot)
			{
				YASSERT(Selected == NULL);
				str Buffer = TEMPSTR(255);

				FormatInto(&Buffer, VIEWC("{time} - {time}"),
					   Focus.Begin, Focus.End);

				Text(Ui, VerticalRegion(TextBounds, 2, 1, 2),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data,
				     COLOR.White, 1.0);
			}

			if (Active)
			{
				f32 Movement = (f32) (Ui->Next.Mouse.Pos.X - Ui->Current.Mouse.Pos.X);
				f32 Delta    = (Movement * Range) / BoxWidth;
				idx NewFocusEnd = Delta + Focus.End;

				if (NewFocusEnd < Focus.Begin + 10)
				{
					NewFocusEnd = Focus.Begin + 10;
				}

				if (NewFocusEnd > Viewable.End)
				{
					NewFocusEnd = Viewable.End;
				}

				View->Focus.End = NewFocusEnd;
			}
		}

		{
			bool Hot = TestAndSetHotWidget(Ui, FreeId + MOVE_FOCUS_ID, ActiveRect);
			bool Active = TestAndSetActiveWidget(Ui,
							    FreeId + MOVE_FOCUS_ID,
							    ActiveRect);

			if (Hot)
			{
				YASSERT(Selected == NULL);
				str Buffer = TEMPSTR(255);

				FormatInto(&Buffer, VIEWC("{time} - {time}"), Focus.Begin, Focus.End);

				Text(Ui, VerticalRegion(TextBounds, 2, 1, 2),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0);
			}

			if (Active)
			{
				f32 Movement = (f32) (Ui->Next.Mouse.Pos.X - Ui->Current.Mouse.Pos.X);
				f32 Delta    = (Movement * Range) / BoxWidth;
				idx NewFocusBegin = Delta + Focus.Begin;
				idx NewFocusEnd   = (NewFocusBegin - Focus.Begin) + Focus.End;
				idx FocusRange    = Focus.End - Focus.Begin;

				if (NewFocusEnd < Viewable.Begin + FocusRange)
				{
					idx Diff = (Viewable.Begin + FocusRange) - NewFocusEnd;
					NewFocusBegin += Diff;
					NewFocusEnd   += Diff;
				}

				if (NewFocusBegin > Viewable.End - FocusRange)
				{
					idx Diff = NewFocusBegin - (Viewable.End - FocusRange);
					NewFocusBegin -= Diff;
					NewFocusEnd   -= Diff;
				}

				View->Focus.Begin = NewFocusBegin;
				View->Focus.End   = NewFocusEnd;
			}
		}
	}

	if (Selected)
	{
		str Buffer = TEMPSTR(255);

		debug_location const *Source = Selected->Loc;

		u64 BlockTime;
		if (IsBlockOpen(Selected))
		{
			BlockTime = ToView->View.Focus.End - Selected->Start.Clock;
		}
		else
		{
			BlockTime = Selected->End.Clock - Selected->Start.Clock;
		}

		if (Selected->HitCount != 1)
		{
			FormatInto(&Buffer, VIEWC("{u32} * "),
				   Selected->HitCount);
		}

		FormatInto(&Buffer, VIEWC("{char[*]} ({f32}us)"),
			   Source->Name,
			   BlockTime / 1000.0);

		Text(Ui, VerticalRegion(TextBounds, 2, 0, 1),
		     TextPosition_CENTERED,
		     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0);

		FormatInto(Reset(&Buffer), VIEWC("{char[*]}:{u64}"),
			   Source->File, Source->Line);

		Text(Ui, VerticalRegion(TextBounds, 2, 1, 2),
		     TextPosition_CENTERED,
		     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0);
	}
}

PRIVATE void
ShowTimeline
(
	ui_state *Ui,
	irect DrawableBounds,
	debug_info *Debug,
	debug_timeline *ToView,
	idx FreeId,
	irect HeaderBounds
)
{
	irect TitleBounds = HorizontalRegion(HeaderBounds, 5, 0, 4);
	irect ButtonBounds = HorizontalRegion(HeaderBounds, 5, 4, 5);
	irect SwitchBounds = HorizontalRegion(ButtonBounds, 3, 1, 3);
	irect ResetBounds = HorizontalRegion(ButtonBounds, 3, 0, 1);

	enum WIDGET_ID
	{
		SWITCH_ID,
		RESET_ID,
		FREE_ID
	};

	str_view Switcher = VIEWC("Switch");
	if (Button(Ui, FreeId + SWITCH_ID, SwitchBounds,
		   Switcher.Size, Switcher.Data))
	{
		ToView->View.ShowTimeline ^= 1;
	}
	str_view Backup = VIEWC("Reset");
	if (Button(Ui, FreeId + RESET_ID, ResetBounds,
		   Backup.Size, Backup.Data))
	{
		ToView->View.Zoomed = &SentinelLocation;
	}

	if (ToView->View.ShowTimeline)
	{
		str_view Title = VIEWC("Timeline");
		Label(Ui, TitleBounds, TextPosition_CENTERED,
		      Title.Size, Title.Data, COLOR.White, 1.0);
		ShowThreadTimeline(Ui, DrawableBounds, Debug,
				   ToView, FreeId + FREE_ID);
	}
	else
	{
		str_view Title = VIEWC("Time spent inside");
		Label(Ui, TitleBounds, TextPosition_CENTERED,
		      Title.Size, Title.Data, COLOR.White, 1.0);
		ShowPerformanceFunctions(Ui, DrawableBounds, Debug, ToView, FreeId + FREE_ID);
	}
}

/* PRIVATE void */
/* ShowPerformanceFrames(ui_state *Ui, irect DrawableBounds, debug_state *Debug) */
/* { */
/* 	irect GraphBounds = VerticalRegion(DrawableBounds, 100, 0, 80); */
/* 	irect TextBounds = VerticalRegion(DrawableBounds, 100, 80, 100); */
/* 	i32 Width     = 5; */

/* 	irect GraphDescBounds = HorizontalRegion(GraphBounds, 10, 0, 2); */
/* 	GraphBounds = HorizontalRegion(GraphBounds, 10, */
/* 				       2, 10); */

/* 	v4 TaskColors[] = */
/* 		{ */
/* 			V(0, 0, 1, 1), */
/* 			V(0, 1, 0, 1), */
/* 			V(0, 1, 1, 1), */
/* 			V(1, 0, 0, 1), */
/* 			V(1, 0, 1, 1), */
/* 			V(1, 1, 0, 1), */
/* 			V(1, 0.5, 1, 1), */
/* 			V(1, 1, 0.5, 1), */
/* 		}; */

/* 	debug_timing_block *Selected = NULL; */
/* 	debug_timing_block *Clicked  = NULL; */

/* 	u32 NumSlices = Debug->NumThreads + 1; */

/* 	idx StartFrame = (idx)Debug->LastFrame - 1 - Debug->FirstFrameToShow; */

/* 	while (StartFrame < 0) */
/* 	{ */
/* 		StartFrame += Debug->NumFrames; */
/* 	} */


/* 	idx NumFramesToShow = Debug->NumFramesToShow; */
/* 	debug_frame *Frame = &Debug->Frames[StartFrame]; */

/* 	for (idx NumShown = 0; */
/* 	     NumShown < NumFramesToShow; */
/* 	     ++NumShown) */
/* 	{ */
/* 		debug_frame *Prev = PreviousFrame(Debug, Frame); */
/* 		str Buffer = TEMPSTR(20); */
/* 		u64 FrameTime = Frame->StartTime - Prev->StartTime; */

/* 		irect TextBounds = VerticalRegion(GraphDescBounds, */
/* 						  NumSlices * NumFramesToShow, */
/* 						  NumSlices * (NumFramesToShow - NumShown - 1), */
/* 						  NumSlices * (NumFramesToShow - NumShown - 1) */
/* 						  + Debug->NumThreads); */

/* 		PrintU64Into(&Buffer, Frame->Num); */

/* 		Text(Ui, VerticalRegion(TextBounds, 2, 0, 1), */
/* 		     TextPosition_CENTERED, */
/* 		     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0); */

/* 		Reset(&Buffer); */

/* 		PrintF32Into(&Buffer, FrameTime / 1000000.0, (frac_format_spec) { .Precision = 2 }); */

/* 		PrintViewInto(&Buffer, VIEWC("ms")); */

/* 		Text(Ui, VerticalRegion(TextBounds, 2, 1, 2), */
/* 		     TextPosition_CENTERED, */
/* 		     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0); */

/* 		Frame = Prev; */
/* 	} */

/* 	Frame = &Debug->Frames[StartFrame]; */

/* 	idx NumShown = 0; */
/* 	for (idx ThreadId = 0; */
/* 	     ThreadId < Debug->NumThreads; */
/* 	     ++ThreadId) */
/* 	{ */
/* 		INSTRUMENT(FrameThread) */
/* 		{ */
/* 			debug_thread *Thread = &Debug->Threads[ThreadId]; */

/* 			debug_frame *Prev = PreviousFrame(Debug, Frame); */

/* 			u64 FrameStart = Prev->StartTime; */
/* 			u64 FrameEnd   = Frame->StartTime; */
/* 			YASSERT(FrameStart <= FrameEnd); */
/* 			u64 FrameTime = FrameEnd - FrameStart; */
/* 			irect FrameBounds = VerticalRegion(GraphBounds, */
/* 							   NumSlices * NumFramesToShow, */
/* 							   NumSlices * (NumFramesToShow - NumShown - 1), */
/* 							   NumSlices * (NumFramesToShow - NumShown - 1) */
/* 							   + Debug->NumThreads); */

/* 			for (debug_timing_block *Current = Thread->Open; */
/* 			     Current; */
/* 			     Current = NextBlock(Current)) */
/* 			{ */
/* 				v4 Color = ColorOfId(Current->Id); */
/* 				if (Current->StartTime > FrameEnd) continue; */
/* 				if (DebugTimingBlockParentId(Current) != Debug->CurrentId) */
/* 				{ */
/* 					if (Current->Id == Debug->CurrentId) */
/* 					{ */
/* 						Color = COLOR.White; */
/* 						Color.A = 0.5; */
/* 					} */
/* 					else */
/* 					{ */
/* 						continue; */
/* 					} */
/* 				} */
/* 				i32 Interaction = 0; */

/* 				while (Current->StartTime < FrameStart) */
/* 				{ */
/* 					if (NumShown == NumFramesToShow - 1) */
/* 					{ */
/* 						break; */
/* 					} */

/* 					irect ThreadBounds = VerticalRegion(FrameBounds, Debug->NumThreads, */
/* 									    ThreadId, ThreadId + 1); */

/* 					Interaction |= DrawRegion(Debug, Ui, */
/* 								  Current, Color, */
/* 								  ThreadBounds, */
/* 								  FrameStart, FrameEnd); */
/* 					Frame = Prev; */
/* 					Prev = PreviousFrame(Debug, Frame); */
/* 					NumShown += 1; */

/* 					FrameStart  = Prev->StartTime; */
/* 					FrameEnd    = Frame->StartTime; */
/* 					FrameTime   = FrameEnd - FrameStart; */
/* 					FrameBounds = VerticalRegion(GraphBounds, */
/* 								     NumSlices * NumFramesToShow, */
/* 								     NumSlices * (NumFramesToShow - NumShown - 1), */
/* 								     NumSlices * (NumFramesToShow - NumShown - 1) */
/* 								     + Debug->NumThreads); */
/* 				} */

/* 				irect ThreadBounds = VerticalRegion(FrameBounds, Debug->NumThreads, */
/* 								    ThreadId, ThreadId + 1); */

/* 				Interaction |= DrawRegion(Debug, Ui, */
/* 							  Current, Color, */
/* 							  ThreadBounds, */
/* 							  FrameStart, FrameEnd); */

/* 				if (Interaction & (1 << 0)) */
/* 				{ */
/* 					YASSERT(Selected == NULL); */
/* 					Selected = Current; */
/* 				} */

/* 				if (Interaction & (1 << 1)) */
/* 				{ */
/* 					YASSERT(Clicked == NULL); */
/* 					Clicked = Current; */
/* 				} */
/* 			} */

/* 			Frame = &Debug->Frames[StartFrame]; */
/* 			NumShown = 0; */
/* 		} */
/* 	} */

/* 	if (Selected != NULL) */
/* 	{ */
/* 		str Buffer = TEMPSTR(255); */

/* 		debug_perf_source *Source = Debug->Source[Selected->Id]; */

/* 		u64 BlockTime = Selected->EndTime - Selected->StartTime; */

/* 		if (Selected->HitCount != 1) */
/* 		{ */
/* 			FormatInto(&Buffer, VIEWC("{u32} * "), */
/* 				   Selected->HitCount); */
/* 		} */

/* 		FormatInto(&Buffer, VIEWC("{char[*]} ({f32}us)"), */
/* 			   Source->Name, */
/* 			   BlockTime / 1000.0); */

/* 		Text(Ui, VerticalRegion(TextBounds, 2, 0, 1), */
/* 		     TextPosition_CENTERED, */
/* 		     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0); */

/* 		FormatInto(Reset(&Buffer), VIEWC("{char[*]}:{u64}"), */
/* 			   Source->File, Source->Line); */

/* 		Text(Ui, VerticalRegion(TextBounds, 2, 1, 2), */
/* 		     TextPosition_CENTERED, */
/* 		     GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.White, 1.0); */
/* 	} */

/* 	if (Clicked != NULL) */
/* 	{ */
/* 		Debug->CurrentId = Clicked->Id; */
/* 	} */
/* } */

PRIVATE b32
DoShowPerformance(ui_state *Ui, i32 Id, irect *Bounds,
		  debug_info *Debug)
{
	char const *Name = "Performance";
	/* if (Debug->CurrentId != INVALID_PERF_ID) */
	/* { */
	/* 	Name = Debug->Source[Debug->CurrentId]->Name; */
	/* } */
	b32 Result = 0;
	if ((Result = BeginWindow(Ui, Id, Name, Bounds,
				  WindowOption_RESIZE | WindowOption_CLOSABLE | WindowOption_TITLE | WindowOption_MOVABLE)))
	{
		irect DrawableBounds = Ui->DrawableBounds;

		irect HeaderBounds = VerticalRegion(DrawableBounds, 10, 0, 1);
		irect ContentBounds = VerticalRegion(DrawableBounds, 10, 1, 10);

		irect InteractionBounds = HorizontalRegion(HeaderBounds, 10, 0, 1);
		irect TitleBounds       = HorizontalRegion(HeaderBounds, 10, 1, 10);

		enum WIDGET_ID
		{
			INVALID_ID,
			INTERACTION_ID,
			FREE_ID
		};

		if (Debug->VisibleTimeline)
		{
			ShowTimeline(Ui, ContentBounds,
				     Debug,
				     Debug->VisibleTimeline,
				     FREE_ID,
				     TitleBounds);

			str_view BackButton = VIEWC("<");
			if (Button(Ui, INTERACTION_ID, InteractionBounds,
				  BackButton.Size, BackButton.Data))
			{
				Debug->VisibleTimeline = NULL;
			}
		}
		else
		{
			str_view Title = VIEWC("Select snapshot");

			Label(Ui, TitleBounds, TextPosition_CENTERED,
			      Title.Size, Title.Data, COLOR.White, 1.0);

			str_view SnapshotButton = VIEWC("+");
			if (Button(Ui, INTERACTION_ID, InteractionBounds,
				   SnapshotButton.Size, SnapshotButton.Data))
			{
				perf_ts Now = CurrentTimeStamp();
				range TimeRange = (range) {
					.Begin = Now.Clock - 1 SEC,
					.End = Now.Clock
				};
				CreateSnapshot(Debug, TimeRange);
			}

			idx NumTimelines = 0;

			for (debug_timeline *Current = Debug->TimelineSentinel.Next;
			     Current != &Debug->TimelineSentinel;
			     Current = Current->Next)
			{
				NumTimelines += 1;
			}

			str Buffer = TEMPSTR(50);

			idx CurrentIdx = 0;
			for (debug_timeline *Current = Debug->TimelineSentinel.Next;
			     Current != &Debug->TimelineSentinel;
			     Current = Current->Next)
			{
				YASSERT(CurrentIdx < NumTimelines);
				irect RowBounds = VerticalRegion(ContentBounds, NumTimelines,
								 CurrentIdx, CurrentIdx + 1);

				irect SnapshotBounds = HorizontalRegion(RowBounds, 6, 0, 5);
				irect RemoveBounds   = HorizontalRegion(RowBounds, 6, 5, 6);

				FormatInto(Reset(&Buffer), VIEWC("{time} - {time}"),
					   Current->CapturedTime.Begin,
					   Current->CapturedTime.End);

				str_view SnapshotTitle = ViewOf(Buffer);
				str_view Remove   = VIEWC("-");

				if (Button(Ui, FREE_ID + 2 * CurrentIdx, SnapshotBounds,
					   SnapshotTitle.Size, SnapshotTitle.Data))
				{
					Debug->VisibleTimeline = Current;
				}

				if (Button(Ui, FREE_ID + 2 * CurrentIdx + 1, RemoveBounds,
					   Remove.Size, Remove.Data))
				{
					debug_timeline *Last = Current->Prev;
					Debug_DeleteTimeline(Debug, Current);
					Current = Last;
				}

				CurrentIdx += 1;
			}
		}
	}
	EndWindow(Ui);
	return Result;
}

PRIVATE b32
DoShowRenderGroupInfo(ui_state *Ui,
		      i32 Id,
		      irect *Bounds,
                      i32 Capacity,
		      i32 LastSize)
{
	b32 Result = 0;
	if (BeginWindow(Ui, Id, "Render Group Info", Bounds,
		    WindowOption_RESIZE | WindowOption_CLOSABLE | WindowOption_TITLE | WindowOption_MOVABLE))
	{
		Result = 1;
		irect DrawableBounds = Ui->DrawableBounds;

		str_view MemStr = VIEWC("Memory Used:");
		str Buffer = TEMPSTR(50);

		f32 PercentageUsed = (f64) LastSize / (f64) Capacity;

		FormatInto(&Buffer, VIEWC("{i32}/{i32} ({f32}%)"),
			   (i32) LastSize,
			   (i32) Capacity,
			   (f32) PercentageUsed);

		Label(Ui, HorizontalRegion(DrawableBounds, 50, 1, 24),
		      TextPosition_CENTERED, MemStr.Size, MemStr.Data, COLOR.Turquoise, 1.0);
		Label(Ui, HorizontalRegion(DrawableBounds, 50, 26, 49),
		      TextPosition_CENTERED, GlyphCount(ViewOf(Buffer)), Buffer.Data, COLOR.Turquoise, 1.0);
	}
	EndWindow(Ui);
	return Result;
}

PRIVATE b32
DoShowFontViewer(ui_state *Ui, i32 Id, irect *Bounds,
		 char *CurrentInput, i32 *Cursor, i32 InputSize,
		 glyph_table *Table, input_device *KeyboardDevice)
{
	b32 Result = 0;

	if (BeginWindow(Ui, Id, "Font Viewer", Bounds,
		    WindowOption_RESIZE | WindowOption_CLOSABLE | WindowOption_TITLE | WindowOption_MOVABLE))
	{
		YSASSERT(FONT_COUNT == 3);
                enum FONT_VIEWER_ID
		{
			INVALID_ID,
			LOAD_FONT_BUTTON_ID_0, /* 3 IDs for three fonts */
			LOAD_FONT_BUTTON_ID_1,
			LOAD_FONT_BUTTON_ID_2,
			LOAD_BUTTON_ID,

		};
		Result = 1;

		if (KeyboardDevice->Method == InputMethod_KEYBOARD
		    && KeyboardDevice->Data.Keyboard)
		{
			keyboard_data *Keyboard = KeyboardDevice->Data.Keyboard;
			if (Keyboard->GlyphCount > 0)
			{
				for (u32 Idx = 0;
				     Idx < Keyboard->GlyphCount;
				     ++Idx)
				{
					if (*Cursor >= InputSize) break;

					u32 Digit = Keyboard->Text[Idx];
					if ('0' <= Digit && Digit <= '9')
					{
						CurrentInput[*Cursor] = Digit;
						*Cursor += 1;
						CurrentInput[*Cursor] = '\0';
					}
				}
			}

			if (KeyStateNumPressed(Keyboard->Key[KeyId_BACKSPACE]))
			{
				i32 NumDelete = KeyStateNumPressed(Keyboard->Key[KeyId_BACKSPACE]);

				while (--NumDelete >= 0)
				{
					if (*Cursor == 0) break;

					CurrentInput[*Cursor] = '\0';
					*Cursor -= 1;
				}
			}

			KeyboardDevice->Data.Keyboard = NULL;
		}


		irect DrawableBounds = Ui->DrawableBounds;

		irect InputBounds = VerticalRegion(DrawableBounds, 50, 5, 13);

		irect FontBounds  = VerticalRegion(DrawableBounds, 50, 18, 45);

		i32 NumRows = 3 * FONT_COUNT - 1;

		irect ButtonBounds = HorizontalRegion(InputBounds, 20, 1, 3);

		for (i32 Font = 0;
		     Font < FONT_COUNT;
		     ++Font)
		{
			if (Button(Ui, LOAD_FONT_BUTTON_ID_0 + Font,
				   VerticalRegion(ButtonBounds, NumRows, Font * 3,
						  Font * 3 + 2),
				   0, NULL))
			{
				ReleaseGlyphTable(Table);
				InitGlyphTable(Table, (font_id) Font, 48, &TState->Scratch);
			}
		}

		Label(Ui, HorizontalRegion(InputBounds, 20, 4, 14), TextPosition_LEFT, *Cursor, CurrentInput,
		      COLOR.Turquoise, 1.0);
                if(Button(Ui, LOAD_BUTTON_ID, HorizontalRegion(InputBounds, 20, 15, 19), 4, "Load"))
		{

			if (*Cursor != 0)
			{
				parse32 P = ParseU32(ViewOf(CurrentInput));
				if (P.Valid)
				{
					EnsureLoaded(Table, P.U32);
				}
			}

			for (i32 I = 0;
			     I < *Cursor;
			     ++I)
			{
				CurrentInput[I] = '\0';
			}
			*Cursor = 0;
		}

		texture Atlas;
#if defined(NO_GL)
		Atlas.ID = Table->Atlas.Id;
#else
		Atlas.ID = Table->AtlasTexture;
#endif

		// NOTE: glyphs are drawn upside down
		Atlas.Origin = V(0, 1);
		Atlas.Dimensions = V(1, -1);
		Atlas.IsMono = 1;
                Texture(Ui, FontBounds, Atlas);
	}
	EndWindow(Ui);

	return Result;
}

PRIVATE b32
ShouldOpenConsole(game_input Input)
{
	b32 Result = 0;
	for (i32 I = 0;
	     I < Input.NumDevices;
	     ++I)
	{
		input_device *Device = Input.Devices + I;
		keyboard_data *Keyboard;
		if (Device->Method == InputMethod_KEYBOARD
		    && (Keyboard = Device->Data.Keyboard))
		{
			key_state OpenKey = Keyboard->Key[KeyId_ENTER];
			Result = OpenKey.IsDown && (OpenKey.HalfTransitions & 1);
			break;
		}
	}
	return Result;
}

PRIVATE b32
ShouldCloseConsole(game_input Input)
{
	b32 Result = 0;
	for (i32 I = 0;
	     I < Input.NumDevices;
	     ++I)
	{
		input_device *Device = Input.Devices + I;
		keyboard_data *Keyboard;
		if (Device->Method == InputMethod_KEYBOARD
		    && (Keyboard = Device->Data.Keyboard))
		{
			key_state CloseKey = Keyboard->Key[KeyId_ESCAPE];
			Result = CloseKey.IsDown && (CloseKey.HalfTransitions != 0);

			if (Result)
			{
				Input.Devices[I].Data.Keyboard = NULL;
			}
			break;
		}
	}
	return Result;
}

PRIVATE b32
ShouldAcceptConsole(game_input Input)
{
	b32 Result = 0;
	for (i32 I = 0;
	     I < Input.NumDevices;
	     ++I)
	{
		input_device *Device = Input.Devices + I;
		keyboard_data *Keyboard;
		if (Device->Method == InputMethod_KEYBOARD
		    && (Keyboard = Device->Data.Keyboard))
		{
			key_state AcceptKey = Keyboard->Key[KeyId_ENTER];
			Result = AcceptKey.IsDown && (AcceptKey.HalfTransitions & 1);
			break;
		}
	}
	return Result;
}

PRIVATE i32
ShouldDeleteConsole(game_input Input)
{
	i32 Result = 0;
	for (i32 I = 0;
	     I < Input.NumDevices;
	     ++I)
	{
		input_device *Device = Input.Devices + I;
		keyboard_data *Keyboard;
		if (Device->Method == InputMethod_KEYBOARD
		    && (Keyboard = Device->Data.Keyboard))
		{
			key_state DeleteKey = Keyboard->Key[KeyId_BACKSPACE];
			if (DeleteKey.IsDown && (DeleteKey.HalfTransitions & 1))
			{
				Result = KeyStateNumPressed(DeleteKey);
				break;
			}
		}
	}
	return Result;
}

PRIVATE void
PrepareBattlefield(game_battlefield *Battle)
{
	ResetBattlefield(&Battle->Field);

	Battle->TurnCount = 0;
	Battle->SelectedEntity = -1;
	Battle->Closed = YARPG_FALSE;
	Battle->CurrentEntity = 0;
}

PRIVATE b32 *
DebugSwitchLocationOfName(game *Game, str_view Name)
{
	b32 *Result = NULL;

	if (ViewEq(Name, VIEWC("memory"))) {
		Result = &Game->ShowMemoryWindow;
	} else if (ViewEq(Name, VIEWC("performance"))) {
		Result = &Game->ShowPerformance;
	} else if (ViewEq(Name, VIEWC("camera"))) {
		Result = &Game->ShowCameraPosition;
	} else if (ViewEq(Name, VIEWC("font"))) {
		Result = &Game->ShowFontViewer;
	} else if (ViewEq(Name, VIEWC("fps"))) {
		Result = &Game->ShowFPS;
	} else if (ViewEq(Name, VIEWC("test"))) {
		Result = &Game->ShowTest;
	} else if (ViewEq(Name, VIEWC("rginfo"))) {
		Result = &Game->ShowRenderGroupInfo;
	} else if (ViewEq(Name, VIEWC("paths"))) {
		Result = &Game->Map.World.ShowPaths;
	} else if (ViewEq(Name, VIEWC("uidbg"))) {
		Result = &Game->ShowUiDebug;
	} else if (ViewEq(Name, VIEWC("music"))) {
		Result = &Game->ShowMusic;
	} else if (ViewEq(Name, VIEWC("rndrinfo"))) {
		Result = &Game->ShowRendererInfo;
	} else if (ViewEq(Name, VIEWC("mapedit"))) {
		Result = &Game->ShowMapEdit;
	} else if (ViewEq(Name, VIEWC("dbgmsg"))) {
		Result = &Game->ShowDbgMsg;
	}

	return Result;
}

PRIVATE b32
UpdateAndRenderDebugUi(ui_state *Ui,
		       game *Game, f32 Dt, game_input Input,
		       transient_state *Transient)
{
	b32 ShouldContinue = YARPG_TRUE;
	/* INSTRUMENT(Console) */
/* 	{ */
/* 		f32 ConsoleTimeToOpen = 0.3; */
/* 		debug_console *DebugConsole = &DebugState->Console; */

/* 		if (DebugConsole->Visible) */
/* 		{ */
/* 			DebugConsole->PercentShowing += Dt / ConsoleTimeToOpen; */
/* 			if (DebugConsole->PercentShowing > 1.0f) */
/* 				DebugConsole->PercentShowing = 1.0f; */
/* 		} else { */
/* 			DebugConsole->PercentShowing -= Dt / ConsoleTimeToOpen; */
/* 			if (DebugConsole->PercentShowing < 0.0f) */
/* 				DebugConsole->PercentShowing = 0.0f; */
/* 		} */

/* 		irect ConsoleBounds; */
/* 		ConsoleBounds.MinX = 0; */
/* 		ConsoleBounds.MaxX = PlatformLayer->Info.WindowWidth; */

/* 		f32 Height = (PlatformLayer->Info.WindowHeight / 2) * DebugConsole->PercentShowing; */
/* 		ConsoleBounds.MaxY = PlatformLayer->Info.WindowHeight; */
/* 		ConsoleBounds.MinY = PlatformLayer->Info.WindowHeight - Height; */


/* 		if (ShouldOpenConsole(Input)) */
/* 		{ */
/* 			DebugConsole->Visible = YARPG_TRUE; */
/* 		} */

/* 		if (DebugConsole->PercentShowing > 0.05 ) */
/* 		{ */
/* 			irect InputBounds; */
/* 			InputBounds.MinX = ConsoleBounds.MinX; */
/* 			InputBounds.MaxX = ConsoleBounds.MaxX; */

/* 			InputBounds.MinY = 0; */
/* 			InputBounds.MaxY = 50; */
/* 			irect OutputBounds = InputBounds; */
/* 			OutputBounds.MinY = 60; */
/* 			OutputBounds.MaxY = ConsoleBounds.MaxY - 100; */

/* 			BeginWindow(Ui, CONSOLE_WINDOW_ID, */
/* 				    "Console", &ConsoleBounds, 0); */

/* 			f32 InputColor[] = {1.0, 1.0, 1.0}; */

/* 			textinput Desc; */
/* 			Desc.Buffer   = (byte *) DebugConsole->Input.Data; */
/* 			Desc.Head     = DebugConsole->Input.Size; */
/* 			Desc.NumChars = DebugConsole->NumChars; */
/* 			Desc.Capacity = DebugConsole->Input.Capacity; */

/* 			// TODO: there has to be a better way! */
/* 			if (Ui->Next.Interaction.Active.Window == 0) */
/* 			{ */
/* 				Ui->Next.Interaction.Active.Window = CONSOLE_WINDOW_ID; */
/* 				Ui->Next.Interaction.Active.Widget = 0; */
/* 			} */
/* 			if ((Ui->Next.Interaction.Active.Window == CONSOLE_WINDOW_ID) && */
/* 			    (Ui->Next.Interaction.Active.Widget == 0)) */
/* 			{ */
/* 				Ui->Next.Interaction.Active.Widget = 1; */
/* 			} */
/* 			Label(Ui, OutputBounds, TextPosition_LEFT, */
/* 			      GlyphCount(ViewOf(DebugConsole->Output)), */
/* 			      DebugConsole->Output.Data, */
/* 			      COLOR.White, 1.0); */

/* 			textinput_result R = Textinput(Ui, 1, InputBounds, &Desc); */
/* 			if (R.BufferChanged) */
/* 			{ */
/* 				DebugConsole->Input.Size = Desc.Head; */
/* 				DebugConsole->NumChars   = Desc.NumChars; */
/* 			} */

/* 			if (R.EnterPressed) */
/* 			{ */
/* 				tokenizer Tokenizer = TokenizerStart(ViewOf(DebugConsole->Input)); */

/* 				str_view Verb = NextToken(&Tokenizer); */

/* 				if (ViewEq(Verb, VIEWC("break"))) */
/* 				{ */
/* 					YASSERT(0, "manual break triggered"); */
/* 				} */
/* 				else if (ViewEq(Verb, VIEWC("exit"))) */
/* 				{ */
/* 					ShouldContinue = YARPG_FALSE; */
/* 				} */
/* 				else if (ViewEq(Verb, VIEWC("reload"))) */
/* 				{ */
/* 					Game->TranStateInitialized = YARPG_FALSE; */
/* 				} */
/* 				else if (ViewEq(Verb, VIEWC("close"))) */
/* 				{ */
/* 					DebugConsole->Visible = YARPG_FALSE; */
/* 				} */
/* 				else if (ViewEq(Verb, VIEWC("set"))) */
/* 				{ */
/* 					str_view Object = NextToken(&Tokenizer); */
/* 					if (ViewEq(Object, VIEWC("MapSeed"))) */
/* 					{ */
/* 						str_view SeedStr = NextToken(&Tokenizer); */
/* 						parse64 Result = ParseU64(SeedStr); */
/* 						if (Result.Valid) */
/* 						{ */
/* 							u64 OldSeed = Game->Options.Seed; */

/* 							if (OldSeed == Result.U64) */
/* 							{ */
/* 								ConsoleOut(DebugConsole, */
/* 									   "Left MapSeed at {u64}\n", */
/* 									   OldSeed); */
/* 							} */
/* 							else */
/* 							{ */
/* 								Game->Options.Seed = Result.U64; */
/* 								RebuildTest(Game); */
/* 								ConsoleOut(DebugConsole, */
/* 									   "Changed MapSeed: {u64} -> {u64}\n", */
/* 									   OldSeed, */
/* 									   Game->Options.Seed); */
/* 							} */
/* 						} */
/* 						else */
/* 						{ */
/* 							ConsoleOut(DebugConsole, */
/* 								   "'{str}' is not a valid Number.\n", */
/* 								   SeedStr); */
/* 						} */
/* 					} */
/* 					else if (ViewEq(Object, VIEWC("MapBase"))) */
/* 					{ */
/* 						str_view Type = NextToken(&Tokenizer); */

/* 						if (ViewEq(Type, VIEWC("circle"))) */
/* 						{ */
/* 							str_view RadiusStr = NextToken(&Tokenizer); */
/* 							parse64 Radius = ParseU64(RadiusStr); */
/* 							if (!Radius.Valid) */
/* 							{ */
/* 								ConsoleOut(DebugConsole, */
/* 									   "'{str}' is not a valid Number.\n", */
/* 									   RadiusStr); */
/* 							} */
/* 							else */
/* 							{ */
/* //								iv2 OldMax = Game->Options.Max; */
/* 								i32 NewRadius = Radius.U64; */
/* 								/\* if (Eq(OldMax, NewMax)) *\/ */
/* 								/\* { *\/ */
/* 								/\* 	ConsoleOut(DebugConsole, *\/ */
/* 								/\* 		   "Left MaxMax at {iv2}\n", *\/ */
/* 								/\* 		   OldMax); *\/ */
/* 								/\* } *\/ */
/* 								/\* else *\/ */
/* 								switch (Game->Options.Type) */
/* 								{ */
/* 									break;case BaseMapType_RECT: */
/* 									{ */
/* 										ConsoleOut(DebugConsole, */
/* 											   "Changed MapBase:\nWas: RECT({iv2})\nNOW: CIRCLE({i32})\n", */
/* 											   Game->Options.Dim, */
/* 											   NewRadius); */
/* 										Game->Options.Type   = BaseMapType_CIRCLE; */
/* 										Game->Options.Radius = NewRadius; */
/* 										RebuildTest(Game); */
/* 									} */
/* 									break;case BaseMapType_CIRCLE: */
/* 									{ */
/* 										if (NewRadius == Game->Options.Radius) */
/* 										{ */
/* 											ConsoleOut(DebugConsole, */
/* 												   "MapBase already is Circle({i32})\n", */
/* 												   Game->Options.Radius); */
/* 										} */
/* 										else */
/* 										{ */
/* 											ConsoleOut(DebugConsole, */
/* 												   "Changed MapBase:\nWas: CIRCLE({i64})\nNOW: CIRCLE({i32})\n", */
/* 												   Game->Options.Radius, */
/* 												   NewRadius); */
/* 											Game->Options.Type   = BaseMapType_CIRCLE; */
/* 											Game->Options.Radius = NewRadius; */
/* 											RebuildTest(Game); */
/* 										} */

/* 									} */
/* 									INVALIDDEFAULT(); */
/* 								} */
/* 							} */
/* 						} */
/* 						else if (ViewEq(Type, VIEWC("rect"))) */
/* 						{ */
/* 							str_view XStr = NextToken(&Tokenizer); */
/* 							str_view YStr = NextToken(&Tokenizer); */
/* 							parse64 X = ParseU64(XStr); */
/* 							parse64 Y = ParseU64(YStr); */
/* 							if (!X.Valid) */
/* 							{ */
/* 								ConsoleOut(DebugConsole, */
/* 									   "'{str}' is not a valid Number.\n", */
/* 									   XStr); */
/* 							} */
/* 							else if (!Y.Valid) */
/* 							{ */
/* 								ConsoleOut(DebugConsole, */
/* 									   "'{str}' is not a valid Number.\n", */
/* 									   YStr); */
/* 							} */
/* 							else */
/* 							{ */
/* 								iv2 NewDim = IV(X.U64, Y.U64); */

/* 								switch (Game->Options.Type) */
/* 								{ */
/* 									break;case BaseMapType_RECT: */
/* 									{ */
/* 										if (Eq(NewDim, Game->Options.Dim)) */
/* 										{ */
/* 											ConsoleOut(DebugConsole, */
/* 												   "MapBase already is RECT({iv2})\n", */
/* 												   Game->Options.Dim); */
/* 										} */
/* 										else */
/* 										{ */
/* 											ConsoleOut(DebugConsole, */
/* 												   "Changed MapBase:\nWas: RECT({iv2})\nNOW: RECT({iv2})\n", */
/* 												   Game->Options.Dim, */
/* 												   NewDim); */
/* 											Game->Options.Type = BaseMapType_RECT; */
/* 											Game->Options.Dim = NewDim; */
/* 											RebuildTest(Game); */
/* 										} */
/* 									} */
/* 									break;case BaseMapType_CIRCLE: */
/* 									{ */
/* 										ConsoleOut(DebugConsole, */
/* 											   "Changed MapBase:\nWas: CIRCLE({i64})\nNOW: RECT({iv2})\n", */
/* 											   Game->Options.Radius, */
/* 											   NewDim); */
/* 										Game->Options.Type = BaseMapType_RECT; */
/* 										Game->Options.Dim = NewDim; */
/* 										RebuildTest(Game); */
/* 									} */
/* 									INVALIDDEFAULT(); */
/* 								} */
/* 							} */
/* 						} */
/* 						else */
/* 						{ */
/* 							ConsoleOut(DebugConsole, */
/* 								   "'{str}' not a map base type.\n", */
/* 								   Type); */
/* 						} */


/* 					} */
/* 					else if (ViewEq(Object, VIEWC("HexRad"))) */
/* 					{ */
/* #if 0 */
/* 						str_view RadString = NextToken(&Tokenizer); */
/* 						TODO(Implement ParseF32); */
/* 						parse32 Radius = ParseF32(RadString); */
/* 						if (Radius.Valid && Radius.F32 > 0) */
/* 						{ */
/* 							Game->Options.HexRad = Radius.F32; */
/* 						} */
/* 						else */
/* 						{ */
/* 							ConsoleOut(DebugConsole, */
/* 								   "'{str}' is not a valid radius.\n", */
/* 								   RadString); */
/* 						} */
/* #else */
/* 						str_view NumStr = NextToken(&Tokenizer); */
/* 						str_view DenStr = NextToken(&Tokenizer); */

/* 						parse32 Num = ParseI32(NumStr); */
/* 						parse32 Den = ParseI32(DenStr); */
/* 						if (Num.Valid && Den.Valid && Den.I32 > 0 && Num.I32 > 0) */
/* 						{ */
/* 							Game->Options.HexRad = (f32) Num.I32  / (f32) Den.I32; */
/* 							ConsoleOut(DebugConsole, "HexRad = {f32}", */
/* 								Game->Options.HexRad); */
/* 							RebuildTest(Game); */
/* 						} */
/* 						else */
/* 						{ */
/* 							ConsoleOut(DebugConsole, "Bad options.\n"); */
/* 						} */
/* #endif */
/* 					} */
/* 					else */
/* 					{ */
/* 						ConsoleOut(DebugConsole, */
/* 							   "'{str}' not recognized.\n", */
/* 							   Object); */
/* 					} */
/* 				} */
/* 				else if (ViewEq(Verb, VIEWC("print"))) */
/* 				{ */
/* 					str_view Object = NextToken(&Tokenizer); */
/* 					// TODO: write to console output! */
/* 					if (ViewEq(Object, VIEWC("MapSeed"))) */
/* 					{ */

/* 						ConsoleOut(DebugConsole, */
/* 							   "Current Seed: {u64}.\n", */
/* 							   Game->Options.Seed); */
/* 					} */
/* 					else */
/* 					{ */
/* 						ConsoleOut(DebugConsole, */
/* 							   "'{str}' not recognized.\n", */
/* 							   Object); */
/* 					} */
/* 				} */
/* 				else */
/* 				{ */
/* 					str_view Object = NextToken(&Tokenizer); */
/* 					b32 *Switch = DebugSwitchLocationOfName(Game, Object); */

/* 					if (Switch) */
/* 					{ */
/* 						if (ViewEq(Verb, VIEWC("show"))) */
/* 						{ */
/* 							*Switch = YARPG_TRUE; */
/* 						} */
/* 						else if (ViewEq(Verb, VIEWC("toggle"))) */
/* 						{ */
/* 							*Switch ^= YARPG_TRUE; */
/* 						} */
/* 						else if (ViewEq(Verb, VIEWC("hide"))) */
/* 						{ */
/* 							*Switch = YARPG_FALSE; */
/* 						} */
/* 					} */
/* 				} */

/* 				Reset(&DebugConsole->Input); */
/* 				DebugConsole->NumChars = 0; */
/* 			} */

/* 			EndWindow(Ui); */
/* 		} */

/* 	} */


	if (Game->ShowUiDebug)
	{
		INSTRUMENT(UiDebug)
		{
			irect XBounds;
			XBounds.MinX = 0;
			XBounds.MaxX = PlatformLayer->Info.WindowWidth;

			XBounds.MaxY = PlatformLayer->Info.WindowHeight;
			XBounds.MinY = 0;

			if (BeginWindow(Ui, UI_DEBUG_ID,
					"A", &XBounds, WindowOption_TRANSPARENT | WindowOption_NONINTERACTIVE))
			{
				irect DrawableBounds = Ui->DrawableBounds;
				irect Bounds         = GridRegion(DrawableBounds, IV(4, 4), IV(0, 0), IV(2, 2));

				str Buffer = TEMPSTR(50);

				str_view NextStr    = VIEWC("Next");
				str_view CurrentStr = VIEWC("Current");
				str_view HotStr     = VIEWC("Hot");
				str_view ActiveStr  = VIEWC("Active");

				//
				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(1, 0), IV(2, 1)),
				     TextPosition_CENTERED,
				     HotStr.Size, HotStr.Data,
				     COLOR.Turquoise, 1.0);

				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(2, 0), IV(3, 1)),
				     TextPosition_CENTERED,
				     ActiveStr.Size, ActiveStr.Data,
				     COLOR.Turquoise, 1.0);

				FormatInto(Reset(&Buffer), VIEWC("[{i32} {i32} {i32}]"),
					   (i32) Ui->Next.Interaction.Hot.Window,
					   (i32) Ui->Next.Interaction.Hot.Widget,
					   (i32) Ui->Next.Interaction.Hot.Interaction);

				//
				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(0, 1), IV(1, 2)),
				     TextPosition_CENTERED,
				     NextStr.Size, NextStr.Data,
				     COLOR.Turquoise, 1.0);

				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(1, 1), IV(2, 2)),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data,
				     COLOR.Turquoise, 1.0);

				FormatInto(Reset(&Buffer), VIEWC("[{i32} {i32} {i32}]"),
					   (i32) Ui->Next.Interaction.Active.Window,
					   (i32) Ui->Next.Interaction.Active.Widget,
					   (i32) Ui->Next.Interaction.Active.Interaction);

				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(2, 1), IV(3, 2)),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data,
				     COLOR.Turquoise, 1.0);


				//
				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(0, 2), IV(1, 3)),
				     TextPosition_CENTERED,
				     CurrentStr.Size, CurrentStr.Data,
				     COLOR.Turquoise, 1.0);

				FormatInto(Reset(&Buffer), VIEWC("[{i32} {i32} {i32}]"),
					   (i32) Ui->Current.Interaction.Hot.Window,
					   (i32) Ui->Current.Interaction.Hot.Widget,
					   (i32) Ui->Current.Interaction.Hot.Interaction);

				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(1, 2), IV(2, 3)),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data,
				     COLOR.Turquoise, 1.0);

				FormatInto(Reset(&Buffer), VIEWC("[{i32} {i32} {i32}]"),
					   (i32) Ui->Current.Interaction.Active.Window,
					   (i32) Ui->Current.Interaction.Active.Widget,
					   (i32) Ui->Current.Interaction.Active.Interaction);

				Text(Ui, GridRegion(Bounds, IV(3, 3), IV(2, 2), IV(3, 3)),
				     TextPosition_CENTERED,
				     GlyphCount(ViewOf(Buffer)), Buffer.Data,
				     COLOR.Turquoise, 1.0);
			}

			EndWindow(Ui);
		}
	}


	if (Game->ShowMemoryWindow)
	{
		INSTRUMENT(MemoryViewer)
		{
			Game->ShowMemoryWindow = DoShowMemoryViewer(Ui,
								   MEMORY_WINDOW_ID,
								   &Game->MemoryWindowBounds,
								   &Game->Arena,
								   &Transient->Arena,
								   &Transient->Scratch);
		}
	}

	if (Game->ShowCameraPosition)
	{
		INSTRUMENT(CameraViewer)
		{
			Game->ShowCameraPosition =
				DoShowCameraPosition(Ui, CAMERA_WINDOW_ID,
						     &Game->CameraWindowBounds,
						     Game->CameraMatrix,
						     Game->Map.World.CameraPos);
		}

	}

	if (Game->ShowRenderGroupInfo)
	{
#if 0
		TODO(fix this);
		INSTRUMENT(RenderGroupViewer)
		{
			Game->ShowRenderGroupInfo =
				DoShowRenderGroupInfo(Ui, RENDER_GROUP_INFO_WINDOW_ID,
						      &Game->RenderGroupInfoBounds,
						      Game->RenderGroup.Capacity,
						      Game->LastRenderGroupSize);
		}
#endif
	}

	if (Game->ShowTest)
	{
		INSTRUMENT(TestViewer)
		{
			b32 *CheckBoxVals[2];
			CheckBoxVals[0] = 0;// &DebugState->Console.Visible;
			CheckBoxVals[1] = &Game->ShowCameraPosition;

			Game->ShowTest = DoShowTestWindow(
				Ui,
				TEST_WINDOW_ID,
				&Game->TestBounds,
				&Game->ZoomFactor,
				CheckBoxVals,
				Game->Map.World.ActiveChunk,
				&Game->Map.World,
				Game->CameraMatrix,
				0, 0
				/* 2 * ((f32) Input.Input->MouseX / PlatformLayer->Info.WindowWidth) - 1,
				   1 - 2 * ((f32) Input.Input->MouseY / PlatformLayer->Info.WindowHeight) */
				);
		}

	}

	if (Game->ShowMusic)
	{
		INSTRUMENT(MusicViewer)
		{
			Game->ShowMusic
				= DoShowMusicWindow(Ui, MUSIC_WINDOW_ID,
						    &Game->MusicWindowBounds,
						    Game->Clock,
						    24000, 0.5);
		}
	}

	if (Game->ShowPerformance)
	{
		INSTRUMENT(PerformanceViewer)
		{
			Game->ShowPerformance =
				DoShowPerformance(Ui, PERFORMANCE_WINDOW_ID,
						  &Game->PerformanceWindowBounds,
						  TState->DebugInfo);
		}
	}

	if (Game->ShowRendererInfo)
	{
		INSTRUMENT(RendererViewer)
		{
			Game->ShowRendererInfo =
				DoShowRendererInfo(Ui, RENDERER_INFO_WINDOW_ID,
						   &Game->RendererInfoBounds,
						   &Transient->Renderer.DebugInfo);
		}
	}

	if(Game->ShowFontViewer)
	{
		INSTRUMENT(FontViewer)
		{
			Game->ShowFontViewer = DoShowFontViewer(Ui,
							       FONT_WINDOW_ID,
							       &Game->FontViewerBounds,
							       Game->FontViewerInput,
							       &Game->FontViewerInputCount,
							       (i32) ARRAYCOUNT(Game->FontViewerInput),
							       &Game->Table,
							       &Input.Devices[1]);
		}
	}

	if (Game->ShowFPS)
	{
		INSTRUMENT(FPSViewer)
		{
			Game->ShowFPS = DoShowFPS(Ui,
						 FPS_WINDOW_ID,
						 &Game->FPSBounds,
						 Dt);
		}
	}

	irect Bounds;
	Bounds.MinX = 5;
	Bounds.MinY = 5;
	Bounds.MaxX = 300;
	Bounds.MaxY = 300;

	INSTRUMENT(MapEdit)
	{
		if (Game->ShowMapEdit)
		{
			if (BeginWindow(Ui, MAP_EDIT_WINDOW_ID, MapParamName[Game->Options.Selected],
					&Bounds, WindowOption_TITLE))
			{
				irect DrawableBounds = Ui->DrawableBounds;

				enum WIDGET_ID
					{
						INVALID_WIDGET,
						SLIDER_FREQ,
						SLIDER_AMPL,
						SLIDER_LACU,
						SLIDER_PERS,
						SLIDER_OCT,
						TOGGLE_HEAT,
						SELECT_PARAM0,
					};

				u32 NumLines = 7;

				map_gen_param *Param = &Game->Options.Params[Game->Options.Selected];

				b32 Changed = YARPG_FALSE;
				f32 OneOverFreq = Param->OneOverFreq;
				if (Slider(Ui, SLIDER_FREQ, VerticalRegion(DrawableBounds, NumLines, 0, 1),
					   &OneOverFreq,
					   1,
					   100))
				{
					Param->OneOverFreq = RoundNearest(OneOverFreq);
					Changed = YARPG_TRUE;

				}
				Text(Ui,
				     VerticalRegion(DrawableBounds, NumLines, 0, 1),
				     TextPosition_CENTERED,
				     4, "FREQ", COLOR.White, 1.0);
				if (Slider(Ui, SLIDER_AMPL, VerticalRegion(DrawableBounds, NumLines, 1, 2),
					   &Param->Ampl,
					   0.1,
					   1.0))
				{
					Changed = YARPG_TRUE;
				}
				Text(Ui,
				     VerticalRegion(DrawableBounds, NumLines, 1, 2),
				     TextPosition_CENTERED,
				     4, "AMPL", COLOR.White, 1.0);
				if (Slider(Ui, SLIDER_LACU, VerticalRegion(DrawableBounds, NumLines, 2, 3),
					   &Param->Lacu,
					   0.1,
					   1.0))
				{
					Changed = YARPG_TRUE;
				}
				Text(Ui,
				     VerticalRegion(DrawableBounds, NumLines, 2, 3),
				     TextPosition_CENTERED,
				     4, "LACU", COLOR.White, 1.0);
				if (Slider(Ui, SLIDER_PERS, VerticalRegion(DrawableBounds, NumLines, 3, 4),
					   &Param->Pers,
					   0.0,
					   2.0))
				{
					Changed = YARPG_TRUE;
				}
				Text(Ui,
				     VerticalRegion(DrawableBounds, NumLines, 3, 4),
				     TextPosition_CENTERED,
				     4, "PERS", COLOR.White, 1.0);

				f32 OctF32 = (f32) Param->Octaves;
				if (Slider(Ui, SLIDER_OCT, VerticalRegion(DrawableBounds, NumLines, 4, 5),
					   &OctF32,
					   1,
					   10))
				{
					Param->Octaves = RoundNearest(OctF32);
					Changed = YARPG_TRUE;
				}
				Text(Ui,
				     VerticalRegion(DrawableBounds, NumLines, 4, 5),
				     TextPosition_CENTERED,
				     4, "OCTA", COLOR.White, 1.0);

				if (CheckBox(Ui, TOGGLE_HEAT, VerticalRegion(DrawableBounds, NumLines, 5, 6),
					     &Game->Options.HeatMap))
				{
					Changed = YARPG_TRUE;
				}

				irect Bounds = VerticalRegion(DrawableBounds, NumLines, 6, 7);

				for (idx Param = 0;
				     Param < MapParam_COUNT;
				     ++Param)
				{
					b32 Selected = Param == Game->Options.Selected;
					if (CheckBox(Ui, SELECT_PARAM0 + Param,
						     HorizontalRegion(Bounds, MapParam_COUNT,
								      Param, Param + 1),
						     &Selected))
					{
						Changed = YARPG_TRUE;
						Game->Options.Selected = Param;
					}
				}

				if (Changed)
				{
					INSTRUMENT(GenMap)
					{
						RebuildTest(Game);
					}
				}
			}
			EndWindow(Ui);
		}
	}

	/* if (Game->ShowDbgMsg) */
	/* { */
	/* 	if ((Game->ShowDbgMsg = BeginWindow(Ui, MAP_DBG_MSG_ID, "Messages", */
	/* 			&Game->DbgMsgBounds, */
	/* 			WindowOption_TITLE */
	/* 			| WindowOption_CLOSABLE */
	/* 			| WindowOption_MOVABLE))) */
	/* 	{ */
	/* 		irect DrawableBounds = Ui->DrawableBounds; */

	/* 		enum WIDGET_ID */
	/* 			{ */
	/* 				INVALID_WIDGET, */
	/* 			}; */

	/* 		irect ScrollBounds = HorizontalRegion(Ui->DrawableBounds, 10, 9, 10); */
	/* 		irect MessageBounds = HorizontalRegion(Ui->DrawableBounds, 10, 0, 9); */
	/* 		idx CurrentIdx = DebugState->Output.CurrentBuffer - 1; */
	/* 		for (idx Item = 0; */
	/* 		     Item < 10; */
	/* 		     ++Item) */
	/* 		{ */
	/* 			idx BufferIdx = CurrentIdx - Item; */
	/* 			if (BufferIdx < 0) */
	/* 			{ */
	/* 				BufferIdx += DebugState->Output.NumBuffers; */
	/* 			} */
	/* 			irect ItemBounds = VerticalRegion(MessageBounds, 100, */
	/* 							  Item, Item + 1); */
	/* 			str *ItemStr = &DebugState->Output.Buffers[BufferIdx]; */
	/* 			Label(Ui, ItemBounds, TextPosition_LEFT, */
	/* 			      GlyphCount(ViewOf(*ItemStr)), ItemStr->Data, */
	/* 			      COLOR.White, 1.0); */
	/* 		} */

	/* 	} */
	/* 	EndWindow(Ui); */
	/* } */

	return ShouldContinue;
}

/* PRIVATE debug_frame * */
/* AddFrame(debug_state *Debug, u64 TimeStamp, u64 FrameNum) */
/* { */

/* 	idx FrameIdx = Debug->LastFrame++; */

/* 	if (Debug->LastFrame == Debug->NumFrames) */
/* 	{ */
/* 		Debug->LastFrame = 0; */
/* 	} */

/* 	debug_frame *Frame = &Debug->Frames[FrameIdx]; */

/* 	Frame->StartTime  = TimeStamp; */
/* 	Frame->Num        = FrameNum; */

/* 	return Frame; */
/* } */

/* PRIVATE void */
/* ResetDebug(debug_state *Debug) */
/* { */
/* 	Debug->LastFrame = 0; */
/* 	Debug->CurrentId = INVALID_PERF_ID; */
/* 	ZeroArray(Debug->NumFrames, Debug->Frames); */

/* 	for (idx ThreadId = 0; */
/* 	     ThreadId < Debug->NumThreads; */
/* 	     ++ThreadId) */
/* 	{ */
/* 		debug_thread *Thread = &Debug->Threads[ThreadId]; */
/* 		debug_timing_block *FirstOpen   = Thread->Open; */
/* 		debug_timing_block *FirstClosed = Thread->Closed; */
/* 		Debug->Threads[ThreadId].Open    = NULL; */
/* 		Debug->Threads[ThreadId].Closed  = NULL; */

/* 		for (debug_timing_block *Current = FirstOpen; */
/* 		     Current;) */
/* 		{ */
/* 			debug_timing_block *Next = NextBlock(Current); */

/* 			Current->LastChild = Debug->FreeBlock; */
/* 			Debug->FreeBlock = Current; */

/* 			Current = Next; */
/* 		} */
/* 	} */
/* } */

/* PRIVATE void */
/* CleanupBlocks(debug_state *Debug) */
/* { */
/* 	udx StartTime = Debug->Frames[Debug->LastFrame].StartTime; */

/* 	for (idx ThreadId = 0; */
/* 	     ThreadId < Debug->NumThreads; */
/* 	     ++ThreadId) */
/* 	{ */
/* 		debug_thread *Thread = &Debug->Threads[ThreadId]; */
/* #if 1 */
/* 		if (Thread->Closed) */
/* 		{ */
/* 			debug_timing_block *Prev  = NULL; */
/* 			debug_timing_block *Block = Thread->Closed; */
/* 			while (Block->Parent) */
/* 			{ */
/* 				Block = Block->Parent; */
/* 			} */

/* 			while (Block && Block->EndTime >= StartTime) */
/* 			{ */
/* 				YASSERT(!Block->Parent); */
/* 				Prev = Block; */
/* 				Block = Block->Prev; */
/* 			} */

/* 			if (Block) */
/* 			{ */
/* 				// block is now the first parentless block */
/* 				// that is not interesting anymore. */
/* 				// every block that is behind this block */
/* 				// can be freed */

/* 				if (Prev) */
/* 				{ */
/* 					YASSERT(Prev->Prev == Block); */
/* 					Prev->Prev = NULL; */

/* 					debug_timing_block *Current = Block; */

/* 					while (Current) */
/* 					{ */
/* 						debug_timing_block *Next = NextBlock(Current); */
/* 						Current->NextFree  = Debug->FreeBlock; */
/* 						Debug->FreeBlock   = Current; */
/* 						Current = Next; */
/* 					} */
/* 				} */
/* 			} */
/* 		} */
/* #else */
/* 		for (debug_timing_block **BlockPtr = &Thread->Closed; */
/* 		     *BlockPtr;) */
/* 		{ */
/* 			debug_timing_block *Block = *BlockPtr; */
/* 			if (Block->EndTime < StartTime) */
/* 			{ */
/* 				debug_timing_block *Next = NextBlock(Block); */
/* 				Block->NextFree = Debug->FreeBlock; */
/* 				Block->Parent    = NULL; */
/* 				Block->LastChild = NULL; */
/* 				Block->Prev      = NULL; */
/* 				Debug->FreeBlock = Block; */
/* 				*BlockPtr = Next; */
/* 			} */
/* 			else */
/* 			{ */
/* 				if ((*BlockPtr)->LastChild) */
/* 				{ */
/* 					BlockPtr = &(*BlockPtr)->LastChild; */
/* 				} */
/* 				else if ((*BlockPtr)->Prev) */
/* 				{ */
/* 					BlockPtr = &(*BlockPtr)->Prev; */
/* 				} */
/* 				else */
/* 				{ */
/* 					while ((*BlockPtr) && !(*BlockPtr)->Prev) */
/* 					{ */
/* 						BlockPtr = &(*BlockPtr)->Parent; */
/* 					} */
/* 					if (*BlockPtr) BlockPtr = &(*BlockPtr)->Prev; */
/* 				} */
/* 			} */
/* 		} */
/* #endif */
/* 	} */
/* } */

PRIVATE void
BattlefieldAI(game_battlefield *Battle, rand64 R)
{
	battlefield *Field = &Battle->Field;
	i32 Entity = Battle->CurrentEntity;
	battle_movement *Movement = &Field->Movement[Entity];

	if (!Field->HasAttacked[Entity])
	{
		unit_stats *Stats = &Field->UnitStats[Entity];
		combat_stat CStat = CalculateCombatStat(Stats);
		i32 Min = CStat.MinRange;
		i32 Max = CStat.MaxRange;

		iv2 BoardPosition = Field->BoardPosition[Entity];

		i32 Enemy = -1;

		for (i32 X = -Max;
		     X <= Max;
		     ++X)
		{
			for (i32 Y = MaxI32(-Max, -Max-X);
			     Y <= MinI32(Max, Max-X);
			     ++Y)
			{
				if (X == 0 && Y == 0) continue;

				iv2 IHexPos = Add(BoardPosition, IV(X, Y));

				if (!ValidHex(Field, IHexPos)) continue;

				if (IsEmpty(Field, IHexPos)) continue;

				i32 EntityIdx = GetIndexOfPos(Field, IHexPos);

				YASSERT(EntityIdx >= 0);

				if (Field->Allegiance[EntityIdx] != 0) continue;

				if (Field->UnitStats[EntityIdx].Flags.IsDead) continue;

				if (HexDistance(IHexPos, BoardPosition) < Min) continue;

				Enemy = EntityIdx;
				if (!NextInRange(&R, 15))
				{
					goto END_ATTACK;
				}
			}

		}
	END_ATTACK:
		if (Enemy >= 0)
		{
			Field->HasAttacked[Entity] = Fight(Field, Entity, Enemy);
		}

	}

	if (!IsBusy(Field, Battle->CurrentEntity) && (Movement->Used != Movement->Range))
	{
		// MOVE!!
		i32 Range = Movement->Range - Movement->Used;
		YASSERT(Range >= 0);

		iv2 OldPosition = Field->BoardPosition[Entity];

		iv2 NewPosition = IV(-1, -1);

		for (i32 X = -Range;
		     X <= Range;
		     ++X)
		{
			for (i32 Y = MaxI32(-Range, -Range-X);
			     Y <= MinI32(Range, Range-X);
			     ++Y)
			{
				if (X == 0 && Y == 0) continue;

				iv2 IHexPos = Add(OldPosition, IV(X, Y));

				if (!ValidHex(Field, IHexPos)) continue;

				if (!IsEmpty(Field, IHexPos)) continue;

				NewPosition = IHexPos;
				if (!NextInRange(&R, 15))
				{
					goto END_MOVE;
				}
			}
		}
	END_MOVE:
		if (!Eq(NewPosition, IV(-1, -1)))
		{
			if (BFMoveEntity(Field, Entity, NewPosition))
			{
				Movement->Used += HexDistance(NewPosition,
							      OldPosition);
			}
		}

	}
	if (!IsBusy(Field, Entity))
	{
		Next(Battle);
	}
}

GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
	game_update_and_render_report Report = {};
	Report.ShouldContinue = YARPG_TRUE;

	if (PlatformLayer != Platform)
	{
		// this might not look like it but this is
		// a way to prevent a "data race" between
		// currently running threads trying to call
		// platform functions and the main thread.

		// since no thread can run directly after a reload
		// (this is (should at least be) the only case where PlatformLayer != Platform)
		// its safe to set it here.

		PlatformLayer = Platform;
	}

	game_memory *Memory    = &Platform->Memory;
	game_input Input       = Platform->Input;
	game *CurrentGame      = (game *) Memory->PersistentMemory;

	transient_state *TranState = NULL;

	CurrentGame->Clock += DeltaTime;

	if (!CurrentGame->TranStateInitialized)
	{
		/* DebugState                     = GET_DEBUG_STATE(Memory->DebugMemory); */
		/* YASSERT(NumInstrumentations < MAX_NUM_INSTRUMENTATIONS); */
		/* DebugState->NumInstrumentations = NumInstrumentations; */
		/* ResetDebug(DebugState); */

		byte *Transient = (byte *) Memory->TransientMemory;
		memory_arena TransientArena;
		InitializeArena(&TransientArena, Memory->TransientSize, Transient);

		TranState = PushStruct(&TransientArena, *TranState);

		FreeAll(&CurrentGame->Debug);
		InitTransientState(CurrentGame, TransientArena, &CurrentGame->Debug,
				   TranState);
		InitTransientWorldState(&CurrentGame->Map.World, TranState,
					&CurrentGame->Table);

		YASSERT((ptr) TranState == (ptr) Transient);

		CurrentGame->TranStateInitialized = YARPG_TRUE;

		RebuildTest(CurrentGame);

		RebuildMap(&CurrentGame->Battle.Field, 10, 10, &TranState->Scratch);
	}
	else
	{
		TranState = (transient_state *) Memory->TransientMemory;
	}

	Game = CurrentGame;
	TState = TranState;
	COMPILERBARRIER();
	byte *Buffer = PushArray(&TranState->Scratch, RENDER_GROUP_SIZE, byte);
	render_commands Commands = CreateRenderCommands(RENDER_GROUP_SIZE,
							Buffer);
	basis_system DefaultBasis;
	DefaultBasis.Origin = V(0, 0, 0);
	DefaultBasis.XAxis  = V(1, 0);
	DefaultBasis.YAxis  = V(0, 1);
	render_group RenderGroup = CreateRenderGroup(&Game->Backend,
						     &Commands,
						     &TranState->Assets,
						     DefaultBasis);

	INSTRUMENT(UpdateAndRender)
	{
		GL_QUICK_CHECK();

		game_world *Map = &CurrentGame->Map;
		world *World = &Map->World;
		world_position NewCameraPos = World->CameraPos;

		game_battlefield *Battle = &CurrentGame->Battle;
		battlefield *Field = &Battle->Field;

		world_entity *Entity = GetEntityByIndex(&Map->World,
							Map->HeroIndex);

		YASSERT(Entity);

		v2 Middle;
		if (Game->ShowBattle)
		{
			Middle = Field->MiddlePos;
		} else
		{
			Middle = World->CameraPos.Offset;
		}

		v3 Radius = V(8.0f/CurrentGame->ZoomFactor, 8.0f/CurrentGame->ZoomFactor, 1.0f);

		basis_system ScreenBasis;
		ScreenBasis.Origin = V(-1, -1, 0);
		ScreenBasis.XAxis  = V(2.0f/Platform->Info.WindowWidth, 0);
		ScreenBasis.YAxis  = V(0, 2.0f/Platform->Info.WindowHeight);

		basis_system GameBasis;
		GameBasis.Origin = Scale(-CurrentGame->ZoomFactor/8.0f,
					 V(Middle.X, Middle.Y, 0));
		GameBasis.XAxis = V(CurrentGame->ZoomFactor/8.0f, 0);
		GameBasis.YAxis = V(0, CurrentGame->ZoomFactor/8.0f);

		render_group GameRenderGroup = SubRenderGroup(&RenderGroup, GameBasis);
		render_group AddRenderGroup  = SubRenderGroup(&RenderGroup, GameBasis);
		render_group UiRenderGroup   = SubRenderGroup(&RenderGroup, ScreenBasis);

		INSTRUMENT(Update)
		{
			render_group *ScreenLike = &UiRenderGroup;

			basis_system Screen;
			Screen.Origin = Add(V(Middle.X, Middle.Y, 0), V(-Radius.X, -Radius.Y, 0));
			Screen.XAxis  = V(2.0f/Platform->Info.WindowWidth * Radius.X, 0);
			Screen.YAxis  = V(0, 2.0f/Platform->Info.WindowHeight * Radius.Y);

			glViewport(0, 0, Platform->Info.WindowWidth, Platform->Info.WindowHeight);

			PrepareFrame(&TranState->Gui, &TranState->Scratch,
				     Platform->Info.WindowWidth, Platform->Info.WindowHeight,
				     Input, ScreenLike);

#if 0 // normal view

			GL_QUICK_CHECK();

			// TODO: real Id
			if (Game->ShowBattle)
			{
				battlefield_event_queue *Queue = UpdateAndRenderBattlefield(Field,
											    &GameRenderGroup,
											    &TranState->Assets,
											    &TranState->Scratch,
											    DeltaTime);

				plane_result PR = DrawBattlefieldUi(Battle, &TranState->Gui,
								    &AddRenderGroup,
								    &TranState->Scratch);

				////////////////////////////////////////////////////////////////
				/// """AI""" ///////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////
				if (Field->Allegiance[Battle->CurrentEntity] != 0
				    && !IsBusy(Field, Battle->CurrentEntity))
				{
					rand64 R = RandomSeries(ReadBits(DeltaTime));
					BattlefieldAI(Battle, R);
				}

				if (PR.IsHot && !Field->BattleOver)
				{
					HandleInputBattlefield(Battle, DeltaTime, Screen, Input);
				}


				if (Battle->Closed)
				{
					stored_world_index AtkIdx = Game->Battle.AttackerIndex;
					stored_world_index DefIdx = Game->Battle.DefenderIndex;

					FormatOut("[BATTLE] End: {i32} vs {i32}\n",
						  AtkIdx.ToI32, DefIdx.ToI32);

					world_entity *AtkBase = GetEntityByIndex(World, AtkIdx);
					world_entity *DefBase = GetEntityByIndex(World, DefIdx);

					YASSERT(AtkBase);
					YASSERT(DefBase);

					if (AtkBase->Type != EntityType_ARMY)
					{
						FormatOut("Unexpected attacker entity type: {i32}\n", AtkBase->Type);
					}
					if (DefBase->Type != EntityType_ARMY)
					{
						FormatOut("Unexpected defender entity type: {i32}\n", DefBase->Type);
					}

					YASSERT(AtkBase->Type == EntityType_ARMY);
					YASSERT(DefBase->Type == EntityType_ARMY);

					u32 AtkSoldiers = AtkBase->Data.Army.NumSoldiers;
					u32 DefSoldiers = DefBase->Data.Army.NumSoldiers;

					i32 CurAtk = 0;
					i32 CurDef = 0;

					for (i32 Entity = 0;
					     Entity < Field->NumEntities;
					     ++Entity)
					{
						unit_stats *EntityStats = &Field->UnitStats[Entity];

						if (EntityStats->Flags.IsDead) continue;

						if (Field->Allegiance[Entity] == 0)
						{
							YASSERT((u32) CurAtk < ARRAYCOUNT(AtkBase->Data.Army.Units));
							AtkBase->Data.Army.Units[CurAtk++] = *EntityStats;
						} else {
							YASSERT((u32) CurDef < ARRAYCOUNT(DefBase->Data.Army.Units));
							DefBase->Data.Army.Units[CurDef++] = *EntityStats;

						}
					}

					AtkBase->Data.Army.NumSoldiers = CurAtk;
					DefBase->Data.Army.NumSoldiers = CurDef;


					if (CurDef <= 0)
					{
						RemoveEntity(GetWorldChunk(World, DefBase->Pos.Chunk),
							     DefIdx);
						FormatOut("Defender {i32} died!\n", DefIdx.ToI32);
						World->ShouldReload = YARPG_TRUE;

						Map->PlayerGold += DefSoldiers;
					} else {
						DefBase->BoundingBox.BottomLeft = V(-0.05f * CurDef,
										   -0.05f * CurDef);
						DefBase->BoundingBox.TopRight = V(+0.05f * CurDef,
										 +0.05f * CurDef);
						Map->PlayerGold += (i32) DefSoldiers - CurDef;
					}

					if (CurAtk <= 0)
					{
						RemoveEntity(GetWorldChunk(World, AtkBase->Pos.Chunk),
							     AtkIdx);
						FormatOut("Attacker {i32} died!\n", AtkIdx.ToI32);
						World->ShouldReload = YARPG_TRUE;
					} else {
						AtkBase->BoundingBox.BottomLeft = V(-0.05f * CurAtk,
										   -0.05f * CurAtk);
						AtkBase->BoundingBox.TopRight = V(+0.05f * CurAtk,
										 +0.05f * CurAtk);
					}

					Reload(World);
					Game->ShowBattle = 0;
				}
			}
			else
			{
				world_event_queue *Queue = UpdateAndRenderWorld(World, DeltaTime,
										&GameRenderGroup);
				plane_result PR = DrawWorldUi(&TranState->Gui, Map);
				world_event *Events = (world_event *) Queue->Data;

				if (PR.IsHot)
				{
					HandleInputWorld(Map, Input, DeltaTime, Screen);
				}

				for (i32 I = 0;
				     I < Queue->NumItems;
				     ++I)
				{
					world_event Event = Events[I];
					switch(Event.Type)
					{
						break;case WorldEventType_COLLISION:
						{
							world_collision_event Collision =
								*Event.Event.Collision;

							;
							world_entity *Attacker = GetEntityByIndex(World, Collision.AttackerIndex);
							world_entity *Defender = GetEntityByIndex(World, Collision.DefenderIndex);
							YASSERT(Attacker);
							YASSERT(Defender);

							if (Attacker->Type == EntityType_ARMY &&
							    Defender->Type == EntityType_ARMY)
							{
								world_allegiance AtkAll = Attacker->Data.Army.Allegiance;
								world_allegiance DefAll = Defender->Data.Army.Allegiance;

								if (AtkAll== WorldAllegiance_OWNED &&
								    DefAll == WorldAllegiance_ENEMY)
								{
									StopEntity(World, Collision.AttackerIndex);
									StopEntity(World, Collision.DefenderIndex);
									PrepareBattlefield(&Game->Battle);
									Game->ShowBattle = 1;

									Game->Battle.AttackerIndex = Collision.AttackerIndex;
									Game->Battle.DefenderIndex = Collision.DefenderIndex;
								} else if (AtkAll == WorldAllegiance_ENEMY &&
									   DefAll == WorldAllegiance_OWNED) {
									StopEntity(World, Collision.AttackerIndex);
									StopEntity(World, Collision.DefenderIndex);
									PrepareBattlefield(&Game->Battle);

									Game->ShowBattle = 1;
									Game->Battle.AttackerIndex = Collision.DefenderIndex;
									Game->Battle.DefenderIndex = Collision.AttackerIndex;
								}
							}
						}
						break;case WorldEventType_MOVFINISHED:
						{
							world_mov_finished_event MovFinished =
								*Event.Event.MovementFinished;

							world_entity *Entity =
								GetEntityByIndex(World, MovFinished.Entity);

							YASSERT(Entity);

							if (Entity->Type == EntityType_ARMY &&
							    Entity->Data.Army.Allegiance != WorldAllegiance_OWNED)
							{

								rand64 R = RandomSeries(ReadBits(DeltaTime));

								v2 NewOffset;
								NewOffset.X = NextBetween(&R, -4.5, 4.5);
								NewOffset.Y = NextBetween(&R, -4.5, 4.5);

								world_position NewPosition;
								NewPosition.Offset = NewOffset;
								NewPosition.Chunk  = Entity->Pos.Chunk;

								SetMoveGoal(World, MovFinished.Entity,
									    1.0, NewPosition);
							}
						}
						break;case WorldEventType_QUIT:
						{
							YASSERT(0);
						}
					}
				}


				if (Game->ShowBattle)
				{
					stored_world_index AtkIdx = Game->Battle.AttackerIndex;
					stored_world_index DefIdx = Game->Battle.DefenderIndex;

					FormatOut("[BATTLE] Start: {i32} vs {i32}\n",
						  AtkIdx.ToI32, DefIdx.ToI32);

					world_entity *AtkBase = GetEntityByIndex(World, AtkIdx);
					world_entity *DefBase = GetEntityByIndex(World, DefIdx);

					YASSERT(AtkBase);
					YASSERT(DefBase);

					YASSERT(AtkBase->Type == EntityType_ARMY);
					YASSERT(DefBase->Type == EntityType_ARMY);

					army_data *AtkArmy = &AtkBase->Data.Army;
					army_data *DefArmy = &DefBase->Data.Army;

					v3 AtkColor = V(0.2, 0.6, 0.05);
					v3 DefColor = V(0.9, 0.05, 0.1);

					idx NumTiles = Field->TileStorage.End;
					idx NumFreeTiles = NumTiles;
					idx NumSoldiers = AtkArmy->NumSoldiers + DefArmy->NumSoldiers;
					YASSERT(NumSoldiers <= NumTiles);

					b32 *UsedUp = PushZeroArray(&TranState->Scratch,
								    NumTiles, *UsedUp);

					rand64 R = RandomSeries((((i64) AtkIdx.ToI32) << 32)
								^ ((i64) DefIdx.ToI32));

					for (idx Soldier = 0;
					     Soldier < AtkArmy->NumSoldiers;
					     ++Soldier)
					{
						idx FreeTileIdx = NextInRange(&R, NumFreeTiles);
						for (idx TileIdx = 0;
						     TileIdx < NumTiles;
						     ++TileIdx)
						{
							if (FreeTileIdx == 0)
							{
								iv2 TilePos = Field->TileStorage.Position[TileIdx];
								AddUnitToBattlefield(Field,
										     TilePos,
										     AtkArmy->Units[Soldier],
										     AtkColor,
										     0);
								FreeTileIdx = -1;
								break;
							}
							else if (!UsedUp[TileIdx])
							{
								FreeTileIdx -= 1;
							}
						}
						YASSERT(FreeTileIdx == -1);
					}

					for (idx Soldier = 0;
					     Soldier < DefArmy->NumSoldiers;
					     ++Soldier)
					{
						idx FreeTileIdx = NextInRange(&R, NumFreeTiles);
						for (idx TileIdx = 0;
						     TileIdx < NumTiles;
						     ++TileIdx)
						{
							if (FreeTileIdx == 0)
							{
								iv2 TilePos = Field->TileStorage.Position[TileIdx];
								AddUnitToBattlefield(Field,
										     TilePos,
										     DefArmy->Units[Soldier],
										     DefColor,
										     1);
								FreeTileIdx = -1;
								break;
							}
							else if (!UsedUp[TileIdx])
							{
								FreeTileIdx -= 1;
							}
						}
						YASSERT(FreeTileIdx == -1);
					}
				}
				if (Map->ShouldQuit) Report.ShouldContinue = YARPG_FALSE;
			}
#else
			INSTRUMENT(Entities)
			{
				UpdateAll(&Game->T, DeltaTime);
				RenderAll(&Game->T, &RenderGroup);
			}

#endif

		}


		INSTRUMENT(Rendering)
		{
			basis_system RenderBasis;
			RenderBasis.Origin = V(0, 0, 0);
			RenderBasis.XAxis  = V(1, 0);
			RenderBasis.YAxis  = V(0, 1);

			render_group *Group = &RenderGroup;
			render_commands *Commands = Group->Commands;
			u32 UsedHeaderMem = Commands->NumHeader * sizeof(render_entity_header);

			u32 DataStart = Commands->Capacity;

			if (Commands->NumHeader)
			{
				render_entity_header *LastHeader
					= &((render_entity_header *) Commands->Data)[Commands->NumHeader - 1];

				DataStart = LastHeader->Start;
			}


			Game->LastRenderGroupSize = UsedHeaderMem + (Commands->Capacity - DataStart);

			INSTRUMENT(DebugGUI)
			{
				Report.ShouldContinue &= UpdateAndRenderDebugUi(&TranState->Gui, CurrentGame,
										DeltaTime, Input, TranState);
			}

			FinishFrame(&TranState->Gui);

			INSTRUMENT(Drawing)
			{
				OpenGLDrawRenderGroup(&TranState->Renderer,
						      &TranState->Scratch,
						      RenderBasis,
						      Commands);
			}

			INSTRUMENT(AssetCleanup)
			{
				UnmarkUsedAll(&TranState->Assets);
			}

		}


		INSTRUMENT(End)
		{
			GL_QUICK_CHECK();

			COMPILERBARRIER();

			FreeAll(&TranState->Scratch);
		}
	}

	CurrentGame->FrameCounter += 1;

	return Report;
}

GAME_RELEASE(GameRelease)
{
	// this should probably all be done by the operating system instead.
	// maybe replace with dummy function in release mode?
	game_memory *Memory = &Platform->Memory;

	game *CurrentGame = (game *) Memory->PersistentMemory;

	if (CurrentGame->TranStateInitialized)
	{
		transient_state *TranState = (transient_state *) Memory->TransientMemory;
		ReleaseYAFFFiles(&TranState->Assets);
		RendererRelease(&TranState->Renderer);
	}

	ReleaseGlyphTable(&CurrentGame->Table);
	ReleaseBackend(&CurrentGame->Backend);
	DestroyWorld(&CurrentGame->Map.World);
}

GAME_PLAY_SOUND(GamePlaySound)
{
	real_sound_buffer Result;

	INSTRUMENT(PlaySound)
	{
		game_memory *Memory        = &Platform->Memory;

		game *CurrentGame          = (game *) Memory->PersistentMemory;
		transient_state *TranState = (transient_state *) Memory->TransientMemory;

		memory_arena *Scratch      = &TranState->Scratch;
		FreeAll(Scratch);

		/* for simd purposes we want a bigger alignment here */
		f32 *FSamples = PushAlignedArray(Scratch, NumSamples, *FSamples,
						 alignof(__m128));
		ZeroArray(NumSamples, FSamples);

		Result.NumSamples   = NumSamples;
		Result.SampleRate   = TargetSampleRate;
		Result.NumChannels  = 1;
		Result.Data         = FSamples;
		TODO(Put a real value in here);
		Result.ChannelPitch = -15;

		for (playing_sound **Sound = &TranState->Sounds;
		     *Sound;)
		{
			idx SamplesPlayed = PlaySamples(*Sound, NumSamples,
							TargetSampleRate,
							FSamples);

			if (SamplesPlayed < NumSamples)
			{
				playing_sound *ToRemove = *Sound;
				*Sound = (*Sound)->Next;
				ToRemove->Next = TranState->FreeSound;
				TranState->FreeSound = ToRemove;
			}
			else
			{
				Sound = &(*Sound)->Next;
			}
		}
	}

	return Result;
}

GAME_COLLATE_DEBUG_EVENTS(GameCollateDebugEvents)
{
	debug_info *DebugInfo = TState->DebugInfo;

	AnalyzePerfEvents(DebugInfo, ThreadId, Events);
}

EXPORT const game_api GameApi =
{
	.Init               = &GameInit,
	.Release            = &GameRelease,
	.UpdateAndRender    = &GameUpdateAndRender,
	.PlaySound          = &GamePlaySound,
	.CollateDebugEvents = &GameCollateDebugEvents,
};
