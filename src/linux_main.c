#define _GNU_SOURCE /*for CPU_* macros */
#define PLATFORM
#include <sys/mman.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <math.h>
#include <string.h>
#include <def.h>


// for dllopen
#include <dlfcn.h>
// for checking wether the file changed
#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>
#include <sys/resource.h>
#include <x86intrin.h>
#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <sys/ioctl.h>
#include <time.h> //clock_gettime

#include <nuklear.h>

#include <game.h>
#include <game_internal.h>

//#include <debug_state.h>
#include <debug/perf.h>
#include <debug/debug_state.h>

#include <opengl/init.h>

// for threads
#include <pthread.h> /* NOTE: maybe use threads.h instead ? */
#include <sched.h>
#include <semaphore.h>
#include <compiler.h>
#include <thread.h>
#include <sound.h>

#include <font.h>
#include <file.h>

#include <input.h>

#include <util/memory.h>
#include <util/ring_alloc.h>

#ifndef DLL_NAME
#define DLL_NAME "libyarpg.so"
#endif

FWDDECLARE(game_code);
FWDDECLARE(input_record);
FWDDECLARE(platform_input);
FWDDECLARE(thread_pool);
FWDDECLARE(perf_stat);

struct perf_stat
{
	long Precision; // in nano seconds
	int CycleHandle;
	int ClockHandle;
};

struct game_code
{
	b32 IsValid;
	time_t Version;
	void *DynamicHandle;

	game_api Game;
};

typedef enum { IDLE, RECORDING, REPLAYING } record_state;

struct input_record
{
	record_state State;
	u64 Size;
	void *Memory;
	FILE *Handle;
};

FWDDECLARE(debug_storage);

struct thread_pool
{
	u32 NumThreads;

	pthread_t *ThreadIds;
	thread_context *ThreadContexts;
	idx EventsPerContainer;
	event_storage Empty;
	debug_event *Events;
};

struct platform_input
{
	iv2 CursorPos;
	mouse_data Mouse;
	keyboard_data Keyboard;
};

enum reload_result
{
	GAME_CODE_DIDNT_RELOAD = -1,
	GAME_CODE_COULDNT_RELOAD = 0,
	GAME_CODE_DID_RELOAD = 1,
};

GAMEFN thread_debug_state *AcquireDebugState(thread_context *Ctx);
GAMEFN void ReleaseDebugState(thread_context *Ctx, thread_debug_state *State);

#define WITHDEBUGSTATE(Name, Ctx) DEFER(Name = AcquireDebugState((Ctx)), \
					ReleaseDebugState((Ctx), Name))

////////////////////////////////////////////////////////////////////////////////
/// GLOBALS ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static char const Title[] = "yaRPG";
static i64 Pid;
static i32 Width = 1000, Height = 1000;
static char const LibraryName[] = DLL_NAME;
static char LibraryLocation[2048];
static b32 Stopped;

static i32 CurrentInputBuffer;
static platform_input InputBuffers[2];

// ASan flags every memory access outside of
// [0x000000000000, 0x00007fff7fff] (low)
// [0x10007fff8000, 0x7fffffffffff] (high)
// as an access violation
// so we have to choose a small BASE_ADDRESS
// On x64 the kernel is situated in the high
// addresses so this should not be a problem
static void * const BASE_ADDRESS = (void *) (20 MB);

static GLFWwindow *Window;

platform_layer Platform;

static input_record InputRecord;
static nuklear_context Ctx;

static b32 ShowEntities;
static b32 ShowRecorder;
static b32 ShowDebug;

static game_code GameHandle;

static sem_t Semaphore;
static u32 volatile NumThreadsWaiting = 0;

static pthread_cond_t AllThreadsIdle = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t CondMutex = PTHREAD_MUTEX_INITIALIZER;

static thread_pool WorkerPool;

#include "def.c"
#include "util/vector.c"
#include "util/math.c"
#include "nuklear.c"
#include "util/arena_allocator.c"
#include "util/list_allocator.c"
#include "opengl/util.c"
#include "opengl/shader.c"
#include "util/memory.c"
#include "util/ring_alloc.c"

#include "font.c"
#include "opengl/init.c"
#include "task_queue.c"
#include "platform/alsa_sound.c"
#include "platform/files.c"

////////////////////////////////////////////////////////////////////////////////

static alsa_audio_state AudioState;

static void
cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	platform_input *CurrentInput = &InputBuffers[CurrentInputBuffer];
	// TODO: should use floor instead!
	CurrentInput->CursorPos.X = (i32) xpos;
	CurrentInput->CursorPos.Y = (i32) ypos;
}

static void
window_content_scale_callback(GLFWwindow *window, float xscale, float yscale)
{
	Platform.Info.DPIXScale = xscale;
	Platform.Info.DPIYScale = yscale;
}

static void
cursor_entered_callback(GLFWwindow *window, int entered)
{
	Platform.WindowActive = (entered == GLFW_TRUE);
}

static void
window_size_callback(GLFWwindow *window, int width, int height)
{
	printf("[WINDOW SIZE] (%d, %d) -> (%d, %d)\n",
	       Platform.Info.WindowWidth, Platform.Info.WindowHeight,
	       width, height);
	Platform.Info.WindowWidth  = width;
	Platform.Info.WindowHeight = height;


}

static void
mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	mouse_data *Mouse = &InputBuffers[CurrentInputBuffer].Mouse;

        i32 TransitionCount;

	switch(action)
	{
		break;case GLFW_PRESS:   TransitionCount = 1;
		break;case GLFW_RELEASE: TransitionCount = 1;
		INVALIDDEFAULT();
	}

	switch (button)
	{
		break;case GLFW_MOUSE_BUTTON_LEFT:
		{
			Mouse->Key[KeyId_MOUSE_BUTTON_1].HalfTransitions += TransitionCount;
		}
		break;case GLFW_MOUSE_BUTTON_MIDDLE:
		{
			Mouse->Key[KeyId_MOUSE_BUTTON_2].HalfTransitions += TransitionCount;
		}
		break;case GLFW_MOUSE_BUTTON_RIGHT:
		{
			Mouse->Key[KeyId_MOUSE_BUTTON_3].HalfTransitions += TransitionCount;
		}
		INVALIDDEFAULT();
	}
}


static void
key_callback(GLFWwindow* window, int key, int scancode,
	     int action, int mods)
{
	keyboard_data *Keyboard = &InputBuffers[CurrentInputBuffer].Keyboard;
	i32 TransitionCount = 1;
	if (GLFW_REPEAT == action) TransitionCount = 2;
	switch(key)
	{
		break;case GLFW_KEY_UP:
		{
			Keyboard->Key[KeyId_UP].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_DOWN:
		{
			Keyboard->Key[KeyId_DOWN].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_RIGHT:
		{
			Keyboard->Key[KeyId_RIGHT].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_LEFT:
		{
			Keyboard->Key[KeyId_LEFT].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_W:
		{
			Keyboard->Key[KeyId_W].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_A:
		{
			Keyboard->Key[KeyId_A].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_S:
		{
			Keyboard->Key[KeyId_S].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_D:
		{
			Keyboard->Key[KeyId_D].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_M:
		{
			Keyboard->Key[KeyId_M].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_N:
		{
			Keyboard->Key[KeyId_N].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_ENTER:
		{
			Keyboard->Key[KeyId_ENTER].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_KP_ENTER:
		{
			Keyboard->Key[KeyId_NUM_ENTER].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_BACKSPACE:
		{
			Keyboard->Key[KeyId_BACKSPACE].HalfTransitions
				+= TransitionCount;
		}
		break;case GLFW_KEY_ESCAPE:
		{
			Keyboard->Key[KeyId_ESCAPE].HalfTransitions
				+= TransitionCount;
		}
	}
}

static void
char_callback(GLFWwindow* window, u32 Glyph)
{
	keyboard_data *Keyboard = &InputBuffers[CurrentInputBuffer].Keyboard;
	u32 NextGlyph = Keyboard->GlyphCount;

	if (NextGlyph == ARRAYCOUNT(Keyboard->Text))
	{
		printf("[PLATFORM TEXT INPUT] Dropped glyph: %u\n", Glyph);
		return;
	}

	Keyboard->Text[Keyboard->GlyphCount++] = Glyph;
}

PRIVATE void
UpdateAndSyncKeyboardBuffers(keyboard_data *From, keyboard_data *To)
{
	for (i32 Key = 0;
	     Key < NUM_KEYBOARD_KEYS;
	     ++Key)
	{
                // this is the key position at the start of the frame
		b32 IsDown = From->Key[Key].IsDown;
		if (From->Key[Key].HalfTransitions % 2) {
			IsDown = !IsDown;
		}
		// now IsDown is the key poisition at the current time
		// should probably be ^=
		From->Key[Key].IsDown = IsDown;
		To->Key[Key].IsDown = IsDown;
	}
}

PRIVATE void
UpdateAndSyncMouseBuffers(mouse_data *From, mouse_data *To)
{
	for (i32 Key = 0;
	     Key < NUM_MOUSE_KEYS;
	     ++Key)
	{
                // this is the key position at the start of the frame
		b32 IsDown = From->Key[Key].IsDown;
		if (From->Key[Key].HalfTransitions % 2) {
			IsDown = !IsDown;
		}

		From->Key[Key].IsDown = IsDown;
		To->Key[Key].IsDown = IsDown;
	}
}

static platform_input *
SwapInputBuffers(void)
{
	i32 Current = CurrentInputBuffer;
	i32 Next = (Current + 1) % (i32) ARRAYCOUNT(InputBuffers);

	platform_input *CurrentBuffer = &InputBuffers[Current];
	platform_input *NextBuffer = &InputBuffers[Next];


	// in case the callback is called during SwapInputBuffers
	// we first need to lock in the current buffer values
	// by accepting input in NextBuffer.
	memset(NextBuffer, 0, sizeof(*NextBuffer));
	COMPILERBARRIER();
	CurrentInputBuffer = Next;
	COMPILERBARRIER();


	NextBuffer->CursorPos = CurrentBuffer->CursorPos;
	UpdateAndSyncKeyboardBuffers(&CurrentBuffer->Keyboard, &NextBuffer->Keyboard);
	UpdateAndSyncMouseBuffers(&CurrentBuffer->Mouse, &NextBuffer->Mouse);

	return CurrentBuffer;
}

CONSTFN GAME_INIT(GameInitStub) { return YARPG_ERROR; }

static enum reload_result
TryReload(game_code *Handle);

CONSTFN GAME_UPDATE_AND_RENDER(GameUpdateAndRenderStub)
{
	game_update_and_render_report Report = {};
	Report.ShouldContinue = YARPG_TRUE;
	return Report;
}

GAME_RELEASE(GameReleaseStub) { }

CONSTFN GAME_PLAY_SOUND(GamePlaySoundStub)
{
	real_sound_buffer Result;
	Result.NumSamples   = 0;
	Result.ChannelPitch = 0;
	Result.SampleRate   = 0;
	Result.NumChannels  = 0;
	Result.Data         = NULL;
	return Result;
}

GAME_COLLATE_DEBUG_EVENTS(GameCollateDebugEventsStub)
{
	return;
}

static game_api const GameStub =
{
	.Init = GameInitStub,
	.UpdateAndRender = GameUpdateAndRenderStub,
	.Release = GameReleaseStub,
	.PlaySound = GamePlaySoundStub,
	.CollateDebugEvents = GameCollateDebugEventsStub,
};

static game_code const INVALID_HANDLE = {
	.IsValid = YARPG_FALSE,
	.Version = 0,
	.DynamicHandle = NULL,
	.Game = GameStub,
};

#define CHECK_DL_ERROR() if((error_str = dlerror()) != NULL)	\
	{							\
		puts(error_str);				\
		return INVALID_HANDLE;				\
	}

static game_code
LoadGameCode(void)
{
	char *error_str;

	game_code GameHandle = INVALID_HANDLE;
	struct stat FileStat;
	stat(LibraryLocation, &FileStat);
	GameHandle.Version = FileStat.st_mtime;
	GameHandle.DynamicHandle = dlopen(LibraryLocation, RTLD_NOW | RTLD_LOCAL);

        CHECK_DL_ERROR();

	game_api *GameApi = (game_api *) dlsym(GameHandle.DynamicHandle, "GameApi");

	CHECK_DL_ERROR();

	YASSERT(GameApi);

	GameHandle.Game = *GameApi;

	GameHandle.IsValid = YARPG_TRUE;
	return GameHandle;
}

// TODO: use inotify instead
static b32
ShouldReload(game_code *GameHandle)
{
	if (GameHandle->IsValid)
	{
		struct stat FileStat;
		stat(LibraryLocation, &FileStat);

		return FileStat.st_mtime != GameHandle->Version;
	} else {
                return YARPG_TRUE;
	}

}

PRIVATE void
ReleaseGameCode(game_code GameHandle)
{
	if (GameHandle.DynamicHandle)
	{
		dlclose(GameHandle.DynamicHandle);

		YASSERT(!dlopen(LibraryLocation, RTLD_NOW | RTLD_NOLOAD),
			"[DYNAMIC] Shared library was not unloaded properly!");

	}
}

#if 0
#define THREAD_LOG(...) printf(__VA_ARGS__)
#else
#define THREAD_LOG(...) (void)0
#endif

PRIVATE void *
WorkerThread(void *Arg)
{
	thread_context *Ctx = (thread_context *) Arg;

	ThisThread = Ctx;
	i32 ThreadNum = Ctx->Id;

	THREAD_LOG("[THREAD %d] Started\n", ThreadNum);

	GLFWwindow *RenderContext = Ctx->RenderContext;

	if (RenderContext)
	{
		glfwMakeContextCurrent(RenderContext);
	}
	else
	{
		THREAD_LOG("[THREAD %d] No RenderContext -> Quitting\n", ThreadNum);
		return NULL;
	}

	THREAD_LOG("[THREAD %d] RenderContext = %lx\n", ThreadNum, (ptr) RenderContext);

	char *ThreadErrorBuffer = malloc(256);
	//char ThreadErrorBuffer[256];

	for (;;)
	{

		THREAD_LOG("[THREAD %d] Locking ...\n", ThreadNum);
		pthread_mutex_lock(&CondMutex);
		u32 WaitingThreads = InterlockedPreInc(&NumThreadsWaiting);
		if (WaitingThreads == Platform.Info.NumWorkerThreads)
		{
			THREAD_LOG("[THREAD %d] Broadcasting ...\n", ThreadNum);
			YASSERT(!pthread_cond_broadcast(&AllThreadsIdle));
		}
		else
		{
			THREAD_LOG("[THREAD %d] WaitingThreads: %u/%u\n",
			       ThreadNum,
			       WaitingThreads, Platform.Info.NumWorkerThreads);
		}
		THREAD_LOG("[THREAD %d] Unlocking ...\n", ThreadNum);
		pthread_mutex_unlock(&CondMutex);

		i32 Error;
		// INSTRUMENT("Thread waiting")
		{
			Error = sem_wait(&Semaphore);
			THREAD_LOG("[THREAD %d] Locking ...\n", ThreadNum);
			pthread_mutex_lock(&CondMutex);
			THREAD_LOG("[THREAD %d] Starting ...\n", ThreadNum);
			InterlockedPreDec(&NumThreadsWaiting);
			THREAD_LOG("[THREAD %d] Unlocking ...\n", ThreadNum);
			pthread_mutex_unlock(&CondMutex);
		}


		/* sem_wait: 0 on success */
		if (Error)
		{

			i32 NumChars = snprintf(ThreadErrorBuffer,
						256,
						"Thread %d SemError:", ThreadNum);

			YASSERT(NumChars <= 256);

			if (NumChars >= 255) NumChars = 255;
			ThreadErrorBuffer[NumChars] = '\0';

			perror(ThreadErrorBuffer);
			break;
		}

		async_stack *MyStack = &Ctx->Stack;
		idx NumOtherStacks = Ctx->NumOtherStacks;
		async_stack **OtherStacks = Ctx->OtherStacks;

		idx NumTheftTries = 5;

		INSTRUMENT(FindingWork)
		{
		honest_work:
			for (async_frame *CurrentJob = TryPopJob(MyStack);
			     CurrentJob;
			     CurrentJob = TryPopJob(MyStack))
			{
				INSTRUMENT(HonestWork)
				{
					ResumeAsyncFrame(Ctx, CurrentJob);
				}
			}

			async_frame *Stolen = NULL;
			for (idx Try = 0;
			     Try < NumTheftTries;
			     ++Try)
			{
				for (idx Target = 0;
				     Target < NumOtherStacks;
				     ++Target)
				{
					Stolen = TryStealJob(MyStack, OtherStacks[Target]);
					if (Stolen)
					{
						INSTRUMENT(StolenWork)
						{
							ResumeAsyncFrame(Ctx, Stolen);
						}

						// TODO: we can easily check if new
						// honest work has arrived by just
						// comparing the queue head from now
						// to the queue head from before this task
						// and only go back to honest work if it changed
						goto honest_work;
					}
				}
			}
		}
	}

	free(ThreadErrorBuffer);

	return NULL;
}

static thread_context MainThreadContext;

PRIVATE thread_pool
CreateThreads(u32 NumThreads, GLFWwindow *Window)
{
	thread_pool Result;

	pthread_t      *ThreadIds      = (pthread_t *)      malloc(NumThreads * sizeof(*ThreadIds));
	thread_context *ThreadContexts = (thread_context *) malloc(NumThreads * sizeof(*ThreadContexts));

	idx EventsPerContainer = 2048;
	idx ContainerSize = EventsPerContainer * sizeof(debug_event);

	debug_event (*EventData)[EventsPerContainer] = malloc((NumThreads + 2) * ContainerSize);

	Result.ThreadIds          = ThreadIds;
	Result.ThreadContexts     = ThreadContexts;
	Result.NumThreads         = NumThreads;
	Result.EventsPerContainer = EventsPerContainer;
	Result.Events             = (debug_event *) EventData;
	Result.Empty              = (event_storage) { .Capacity = EventsPerContainer,
		                                      .End = 0,
						      .Storage = EventData[NumThreads + 1] };

	cpu_set_t *Set = CPU_ALLOC(NumThreads + 1);
	size_t SetSize = CPU_ALLOC_SIZE(NumThreads + 1);

	{
		// main thread affinity
		CPU_ZERO_S(SetSize, Set);
		CPU_SET_S(0, SetSize, Set);
		int Error = sched_setaffinity(0, SetSize, Set);

		YASSERTF(!Error, "%d", Error);

		MainThreadContext.Id = 0;
		MainThreadContext.Stack = AsyncStack_FromMalloc(512, 1 MB);
		MainThreadContext.OtherStacks = calloc(sizeof(*MainThreadContext.OtherStacks),
						       NumThreads);
		MainThreadContext.NumOtherStacks = 0;
		MainThreadContext.HelpCallThreshold = 0;
		MainThreadContext.RenderContext = Window;

		MainThreadContext.DebugState = ThreadDebugState_FromBuffer(EventsPerContainer,
									   EventData[NumThreads]);

		ThisThread = &MainThreadContext;
	}

	for (u32 CurrentThread = 0;
	     CurrentThread < NumThreads;
	     ++CurrentThread)
	{
		thread_context *Ctx = &ThreadContexts[CurrentThread];
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE,        GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_SAMPLES,               4);
		glfwWindowHint(GLFW_RESIZABLE,             GL_FALSE);
		glfwWindowHint(GLFW_VISIBLE,               GLFW_FALSE);

		GLFWwindow *RenderContext = glfwCreateWindow(50, 50, "", NULL, Window);
		Ctx->Id = CurrentThread + 1;
		Ctx->Stack = AsyncStack_FromMalloc(512, 1 MB);
		Ctx->OtherStacks = calloc(sizeof(*ThreadContexts[CurrentThread].OtherStacks),
					  NumThreads);
		Ctx->OtherStacks[0] = &MainThreadContext.Stack;
		Ctx->NumOtherStacks = 1;
		Ctx->HelpCallThreshold = 0;
		Ctx->RenderContext = RenderContext;
		Ctx->DebugState = ThreadDebugState_FromBuffer(EventsPerContainer,
							      EventData[CurrentThread]);
		pthread_create(&ThreadIds[CurrentThread], NULL, &WorkerThread,
			       (void *) Ctx);
		CPU_ZERO_S(SetSize, Set);
		CPU_SET_S((CurrentThread + 1), SetSize, Set);
		int Error = pthread_setaffinity_np(ThreadIds[CurrentThread], SetSize, Set);
		YASSERTF(!Error, "%d", Error);
	}

	for (u32 OtherThread = 0;
	     OtherThread < NumThreads;
	     ++OtherThread)
	{
		async_stack *ToAdd = &ThreadContexts[OtherThread].Stack;
		MainThreadContext.OtherStacks[MainThreadContext.NumOtherStacks++] =
			ToAdd;
		for (u32 CurrentThread = 0;
		     CurrentThread < NumThreads;
		     ++CurrentThread)
		{
			thread_context *Ctx = &ThreadContexts[CurrentThread];
			if (CurrentThread != OtherThread)
			{
				Ctx->OtherStacks[Ctx->NumOtherStacks++] = ToAdd;
			}
		}
	}

	CPU_FREE(Set);

	// main thread


	return Result;
}

PRIVATE void
DestroyThreads(thread_pool ThreadPool)
{
	for (u32 CurrentThread = 0;
	     CurrentThread < ThreadPool.NumThreads;
	     ++CurrentThread)
	{
		pthread_cancel(ThreadPool.ThreadIds[CurrentThread]);
		pthread_join(ThreadPool.ThreadIds[CurrentThread], NULL);
	}
	free(ThreadPool.ThreadIds);
	free(ThreadPool.ThreadContexts);
}

PRIVATE void
WaitOnThreads(void)
{
	thread_context *MainThread = CurrentThreadContext();
	YASSERT(MainThread->Id == 0, "You can only wait on the main thread!");
	while (DoQueuedWork(MainThread));

	printf("[MAIN THREAD] Locking...\n");
	pthread_mutex_lock(&CondMutex);
	while (InterlockedRead(&NumThreadsWaiting) != Platform.Info.NumWorkerThreads)
	{
		printf("[MAIN THREAD] Waiting ...\n");
		pthread_cond_wait(&AllThreadsIdle, &CondMutex);
		printf("[MAIN THREAD] Woke up ...\n");
	}

	printf("[MAIN THREAD] Unlocking ...\n");
	pthread_mutex_unlock(&CondMutex);

}

PRIVATE void
ResetThreadState(void)
{
	thread_debug_state *State;
	{
		thread_context *MainThread = CurrentThreadContext();
		YASSERT(MainThread->Id == 0, "You can only wait on the main thread!");
		WITHDEBUGSTATE(State, MainThread)
		{
			event_storage *Storage = &State->Events;
			Storage->Start = CurrentTimeStamp();
			Storage->End = 0;
		}
	}

	for (u32 WorkerThread = 0;
	     WorkerThread < WorkerPool.NumThreads;
	     ++WorkerThread)
	{
		// TODO: this should work _if_
		// 1) acquire/release fences are transitive
		// 2) sem_wait/sem_post imply acquire/release fences
		WITHDEBUGSTATE(State, &WorkerPool.ThreadContexts[WorkerThread])
		{
			event_storage *Storage = &State->Events;
			Storage->Start = CurrentTimeStamp();
			Storage->End = 0;
		}
	}
}

enum reload_result
TryReload(game_code *Handle)
{
	if (ShouldReload(Handle))
	{
		WaitOnThreads();
		ResetThreadState();

		ReleaseGameCode(*Handle);
		*Handle = LoadGameCode();

		if (Handle->IsValid)
		{
                        return GAME_CODE_DID_RELOAD;
		}
		else
		{
                        return GAME_CODE_COULDNT_RELOAD;
		}

	}
	return GAME_CODE_DIDNT_RELOAD;
}

PRIVATE void
StartRecord(input_record *R)
{
	YASSERT(R->State == IDLE);
	R->Handle = fopen("input_record", "wb");
	if (R->Handle == NULL)
	{
		return;
	}
	R->State = RECORDING;

	// since only the main thread (i.e. us) can push tasks on the queues
	// we can flush the queues by just finishing all the work thats inside
	// the current queue

	WaitOnThreads();

	u64 BytesWritten = fwrite(R->Memory, 1, R->Size, R->Handle);
	YASSERT(BytesWritten == R->Size);
}

PRIVATE void
RecordInput(input_record *R, platform_input const *I)
{
	YASSERT(R->State == RECORDING);
	u64 BytesWritten = fwrite(I, 1, sizeof(*I), R->Handle);
	YASSERT(BytesWritten == sizeof(*I));
}

PRIVATE void
StopRecord(input_record *R)
{
	YASSERT(R->State == RECORDING);
	fclose(R->Handle);
	R->State = IDLE;
}

PRIVATE void
StartReplay(input_record *R)
{
        YASSERT(R->State == IDLE);

        R->Handle = fopen("input_record", "rb");
	if (R->Handle == NULL)
	{
		return;
	}
	R->State = REPLAYING;

	WaitOnThreads();

	// the queue is cleared now!

	u64 BytesRead = fread(R->Memory, 1, R->Size, R->Handle);
	YASSERT(BytesRead == R->Size);

	game *Game = (game *) R->Memory;
	Game->TranStateInitialized = YARPG_FALSE;
}

PRIVATE void
StopReplay(input_record *R)
{
	YASSERT(R->State == REPLAYING);
	R->State = IDLE;
	fclose(R->Handle);
}

static platform_input
ReplayRecord(input_record *R)
{
	YASSERT(R->State == REPLAYING);
	platform_input I;
	u64 BytesRead = fread(&I, 1, sizeof(I), R->Handle);
	if (BytesRead < sizeof(I))
	{
		YASSERT(BytesRead == 0);
		StopReplay(R);
		StartReplay(R);
		I = ReplayRecord(R);
	}

	return I;
}

PRIVATE void
Stop(void)
{
	// TODO: we should let the game render into a frame buffer
	// that we put to screen every frame, so that when the game is paused
	// we can still clear & redraw the screen, so that we can move the
	// debug menu around.  This might also make it possible to use
	// the platform debug tools to zoom into the paused screen.
	Stopped = YARPG_TRUE;
}

PRIVATE void
Resume(void)
{
	Stopped = YARPG_FALSE;
}

PRIVATE void
ZeroGameMemory(game_memory Memory)
{
	ZeroMemory(Memory.PersistentSize, Memory.PersistentMemory);
	ZeroMemory(Memory.TransientSize,  Memory.TransientMemory);
	ZeroMemory(Memory.DebugSize,      Memory.DebugMemory);
}

PRIVATE void
ResetGameCode(game_code *Handle, platform_layer *Layer)
{
	enum reload_result ReloadResult = TryReload(Handle);

	if (ReloadResult == GAME_CODE_COULDNT_RELOAD)
	{
		puts("Reset Error: Couldnt reload");
	} else {
		ZeroGameMemory(Layer->Memory);
		Handle->Game.Init(Layer);
	}


}

char const *
KeyNames[NUM_KEYBOARD_KEYS] =
{
	[KeyId_UP]        = "Up",
	[KeyId_DOWN]      = "Down",
	[KeyId_RIGHT]     = "Right",
	[KeyId_LEFT]      = "Left",
	[KeyId_W]         = "W",
	[KeyId_A]         = "A",
	[KeyId_S]         = "S",
	[KeyId_D]         = "D",
	[KeyId_M]         = "M",
	[KeyId_N]         = "N",
	[KeyId_ENTER]     = "Enter",
	[KeyId_BACKSPACE] = "Backspace",
	[KeyId_ESCAPE]    = "Escape",
	[KeyId_NUM_ENTER] = "NUM Enter"
};

PRIVATE void
DoShowRecorder(nuklear_context *Ctx, game *GameState)
{
	if (nk_begin(&Ctx->Nuklear, "Recorder", nk_rect(50, 50, 200, 200),
		     NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE)) {
		/* fixed widget pixel width */

		nk_layout_row_dynamic(&Ctx->Nuklear, 30, 3);
		switch (InputRecord.State)
		{
			break;case IDLE:

			if (nk_button_label(&Ctx->Nuklear, "Record")) {
				StartRecord(&InputRecord);
			}
			nk_label(&Ctx->Nuklear, "Stop", NK_TEXT_CENTERED);
			if (nk_button_label(&Ctx->Nuklear, "Replay")) {
				StartReplay(&InputRecord);
			}
			break;case RECORDING:
			nk_label(&Ctx->Nuklear, "Record", NK_TEXT_CENTERED);
			if (nk_button_label(&Ctx->Nuklear, "Stop"))
			{
				StopRecord(&InputRecord);
			}
			if (nk_button_label(&Ctx->Nuklear, "Replay"))
			{
				StopRecord(&InputRecord);
				StartReplay(&InputRecord);
			}
			break;case REPLAYING:
			if (nk_button_label(&Ctx->Nuklear, "Record"))
			{
				StopReplay(&InputRecord);
				StartRecord(&InputRecord);
			}
			if (nk_button_label(&Ctx->Nuklear, "Stop"))
			{
				StopReplay(&InputRecord);
			}
			nk_label(&Ctx->Nuklear, "Replay", NK_TEXT_CENTERED);
		}

		nk_layout_row_begin(&Ctx->Nuklear, NK_STATIC, 30, 2);
		nk_layout_row_push(&Ctx->Nuklear, 100);
		nk_label(&Ctx->Nuklear, "Currently:", NK_TEXT_LEFT);
		nk_layout_row_push(&Ctx->Nuklear, 50);
		switch (InputRecord.State)
		{
			break;case RECORDING:
			nk_label(&Ctx->Nuklear, "Recording", NK_TEXT_RIGHT);

			break;case IDLE:
			nk_label(&Ctx->Nuklear, "Idle", NK_TEXT_RIGHT);

			break;case REPLAYING:
			nk_label(&Ctx->Nuklear, "Replaying", NK_TEXT_RIGHT);
		}
	} else {
		ShowRecorder = 0;
	}
	nk_end(&Ctx->Nuklear);
}

PRIVATE void
DoShowEntities(nuklear_context *Ctx, game *GameState)
{
	if (nk_begin(&Ctx->Nuklear, "EntityViewer", nk_rect(50, 50, 200, 200),
		     NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE))
	{
		char Buffer[64];

		snprintf(Buffer, sizeof(Buffer), "%d", GameState->Map.World.NumActiveEntities);

		nk_layout_row_begin(&Ctx->Nuklear, NK_STATIC, 30, 2);
		nk_layout_row_push(&Ctx->Nuklear, 100);
		nk_label(&Ctx->Nuklear, "NumActiveEntities", NK_TEXT_LEFT);
		nk_layout_row_push(&Ctx->Nuklear, 50);
		nk_label(&Ctx->Nuklear, Buffer, NK_TEXT_RIGHT);

		snprintf(Buffer, sizeof(Buffer), "%d", GameState->Map.World.NumLoadedEntities);

		nk_layout_row_begin(&Ctx->Nuklear, NK_STATIC, 30, 2);
		nk_layout_row_push(&Ctx->Nuklear, 100);
		nk_label(&Ctx->Nuklear, "NumLoadedEntities", NK_TEXT_LEFT);
		nk_layout_row_push(&Ctx->Nuklear, 50);
		nk_label(&Ctx->Nuklear, Buffer, NK_TEXT_RIGHT);
	} else {
		ShowEntities = 0;
	}


	nk_end(&Ctx->Nuklear);
}

static char const *FullScreenToggleText[] =
{
	"Fullscreen", "Windowed"
};

static b32 IsFullScreen = 0;

PRIVATE void
DoShowDebugWindow(nuklear_context *Ctx, game *GameState,
		  game_code *Handle, game_memory Memory)
{
	nk_begin(&Ctx->Nuklear, "PlatformDebug", nk_rect(50, 50, 175, 360),
		 NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_TITLE|NK_WINDOW_NO_SCROLLBAR);

	nk_layout_row_dynamic(&Ctx->Nuklear, 50, 1);
	if (ShowEntities)
	{
		nk_label(&Ctx->Nuklear, "Show Entity Viewer", NK_TEXT_CENTERED);
	} else {
		if (nk_button_label(&Ctx->Nuklear, "Show Entity Viewer"))
		{
			ShowEntities = 1;
		}
	}
	nk_layout_row_dynamic(&Ctx->Nuklear, 50, 1);
	if (ShowRecorder)
	{
		nk_label(&Ctx->Nuklear, "Show Recorder", NK_TEXT_CENTERED);
	} else if (nk_button_label(&Ctx->Nuklear, "Show Recorder")) {
                ShowRecorder = 1;
	}

	nk_layout_row_dynamic(&Ctx->Nuklear, 50, 3);
	if (Stopped)
	{
		if (nk_button_label(&Ctx->Nuklear, "Resume")) {
                        Resume();
		}
		nk_label(&Ctx->Nuklear, "Stop", NK_TEXT_CENTERED);
		if (nk_button_label(&Ctx->Nuklear, "Reset")) {
                        ResetGameCode(Handle, &Platform);
		}
	} else {
		nk_label(&Ctx->Nuklear, "Resume", NK_TEXT_CENTERED);
		if (nk_button_label(&Ctx->Nuklear, "Stop")) {
                        Stop();
		}
		nk_label(&Ctx->Nuklear, "Reset", NK_TEXT_CENTERED);
	}


	nk_layout_row_dynamic(&Ctx->Nuklear, 50, 1);
	if (nk_button_label(&Ctx->Nuklear, "Play")) {
		for (i32 I = 0;
		     I < 50;
		     ++I)
		{
			// PlayAudio(&AudioState);
		}
	}

	nk_layout_row_dynamic(&Ctx->Nuklear, 50, 1);
	if (nk_button_label(&Ctx->Nuklear, FullScreenToggleText[IsFullScreen])) {

		GLFWmonitor *Monitor = NULL;

		if (!IsFullScreen)
		{
			Monitor = glfwGetPrimaryMonitor();
		}

		glfwSetWindowMonitor(Window, Monitor, 0, 0,
				     Platform.Info.WindowWidth,
				     Platform.Info.WindowHeight,
				     60);

		IsFullScreen = !IsFullScreen;

	}


	nk_layout_row_dynamic(&Ctx->Nuklear, 50, 1);
	if (nk_button_label(&Ctx->Nuklear, "Quit")) {
		glfwSetWindowShouldClose(Window, 1);

	}
	nk_end(&Ctx->Nuklear);
}

PRIVATE void
GatherPlatformInfo(platform_info *Info)
{
	// These should be conservatice guesses
	u32 NumPhysicalCores = 1;
	u32 NumVirtualCores  = 1;
	u32 CacheSize        = 1 KB;
	u32 NumWorkerThreads = 1;
	u32 CacheLineSize    = 64;
	u32 PageSize         = 4096;
	i64 MaxStackSize     = 4096;
	i64 MaxMemory        = 20 MB;

	i64 Error;
	if ((Error = sysconf(_SC_LEVEL1_DCACHE_LINESIZE)) != -1)
	{
		CacheLineSize = (u32) Error;
		printf("[PLATFORM INFO] cache line size: %u bytes\n", CacheLineSize);
	} else {
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing cache line size is %u bytes.", CacheLineSize);
	}

	if ((Error = sysconf(_SC_NPROCESSORS_ONLN)) != -1)
	{
		NumPhysicalCores = (u32) Error;
		printf("[PLATFORM INFO] physical core count: %u\n", NumPhysicalCores);
	} else {
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing processor count is %u\n.", NumPhysicalCores);
	}

	if ((Error = sysconf(_SC_NPROCESSORS_CONF)) != -1)
	{
		NumVirtualCores = (u32) Error;
		printf("[PLATFORM INFO] virtual core count: %u\n", NumVirtualCores);
	} else {
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing cache line size is %u\n.", NumVirtualCores);
	}

	if ((Error = sysconf(_SC_LEVEL1_DCACHE_SIZE)) != -1)
	{
		CacheSize = (u32) Error;
		printf("[PLATFORM INFO] cache size: %u bytes \n", CacheSize);
	} else {
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing cache size is %u bytes\n.", CacheSize);
	}

	if ((Error = sysconf(_SC_PAGESIZE)) != -1)
	{
		PageSize = (u32) Error;
		printf("[PLATFORM INFO] page size: %u bytes \n", PageSize);
	} else {
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing page size is %u bytes\n.", PageSize);
	}

	NumWorkerThreads = NumVirtualCores - 1;

	printf("[PLATFORM INFO] Setting number of worker threads to %u\n", NumWorkerThreads);

	struct rlimit rlp;
	if ((Error = getrlimit(RLIMIT_STACK, &rlp)) != 0)
	{
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing max stack size is %ld bytes\n.", MaxStackSize);
	} else {
		MaxStackSize = (i64) rlp.rlim_cur;
		printf("[PLATFORM INFO] max stack size: %ld bytes \n", MaxStackSize);
	}

	if ((Error = getrlimit(RLIMIT_AS, &rlp)) != 0)
	{
		perror("[PLATFORM ERROR]");
		printf("[PLATFORM INFO] Guessing max memory is %ld bytes\n.", MaxMemory);
	} else {
		MaxMemory = (i64) rlp.rlim_cur;
		if (MaxMemory != -1)
		{
			printf("[PLATFORM INFO] max memory: %ld bytes \n", MaxMemory);
		} else {
			puts("[PLATFORM INFO] max memory: No Maximum");
		}

	}

	Info->NumPhysicalCores = NumPhysicalCores;
	Info->CacheLineSize    = CacheLineSize;
	Info->NumVirtualCores  = NumVirtualCores;
	Info->CacheSize 	      = CacheSize;
	Info->NumWorkerThreads = NumWorkerThreads;
	Info->CacheLineSize    = CacheLineSize;
	Info->PageSize         = PageSize;
	Info->MaxStackSize     = MaxStackSize;
	Info->MaxMemory        = MaxMemory;
}

PLATFORMFN PUSH_TASK(SynchronizedPushTask)
{
	i32 Result = PushTask(Fun, Size, Alignment, Arg);
	COMPILERBARRIER();
	if (Result && InterlockedRead(&NumThreadsWaiting) != 0)
	{
		sem_post(&Semaphore);
	}
	return Result;
}

PRIVATE void
SetupPlatformLayer(platform_layer *Platform,
		   sem_t *Semaphore,
		   udx PersistentSize,
		   udx TransientSize,
		   udx DebugSize)
{
	YASSERT(PersistentSize >= MINIMUM_PERSISTENT_SIZE);
	YASSERT(TransientSize >= MINIMUM_TRANSIENT_SIZE);

	game_memory *Memory = &Platform->Memory;

	Memory->PersistentSize = PersistentSize;
	Memory->TransientSize  = TransientSize;
	Memory->DebugSize      = DebugSize;

	// NOTE: we use mmap here to always have the same base address
	// this allows us to backup and reload game data very easily
	byte *GameMemory = mmap(
		BASE_ADDRESS, Memory->PersistentSize + Memory->TransientSize,
		// MAP_FIXED -> force it to start at BASE_ADDRESS
		// MAP_ANONYMOUS -> dont load a file, just write 0s
		// MAP_PRIVATE -> enables copy on write for child processes,
		//                ok since this is a single process thing
		//                so this shouldnt happen. Allows for bigger
		//                mmap regions as well.
		// maybe we should request huge pages ?
		PROT_READ|PROT_WRITE, MAP_FIXED|MAP_ANONYMOUS|MAP_PRIVATE|MAP_NORESERVE,
		// as described in:
		// https://man7.org/linux/man-pages/man2/mmap.2.html
		// we should use -1 and 0 here
		// since we arent backed by a file
		-1, 0);

	if (GameMemory != BASE_ADDRESS)
	{
		perror("mmap error");
		exit(EXIT_FAILURE);
	}

        Memory->PersistentMemory = PTROFFSET(GameMemory,              0);
	Memory->TransientMemory  = PTROFFSET(GameMemory, Memory->PersistentSize);
	Memory->DebugMemory      = calloc(1, DebugSize);

	GatherPlatformInfo(&Platform->Info);

	Platform->API.RequestFont      = &RequestFont;
	Platform->API.RenderGlyph      = &RenderGlyph;

	Platform->API.PushTask         = &SynchronizedPushTask;
	Platform->API.Wait             = &Wait;

	Platform->API.OpenAssetIterator = &OpenAssetIterator;
	Platform->API.StartIterator     = &StartIterator;
	Platform->API.NextFile          = &NextFile;
	Platform->API.ReadFile          = &ReadFile;
	Platform->API.CloseFile         = &CloseFile;
	Platform->API.CloseIterator     = &CloseIterator;

	Platform->API.WriteOut          = &WriteOut;
	Platform->API.PushPerfEvent     = &PushPerfEvent;
	Platform->API.CurrentTimeStamp  = &CurrentTimeStamp;

	Platform->WindowActive = YARPG_TRUE;
}

PRIVATE void
DestroyPlatformLayer(platform_layer *Platform)
{
	TODO(destroy thread pool);

	munmap(Platform->Memory.PersistentMemory,
	       Platform->Memory.PersistentSize + Platform->Memory.TransientSize);
}

PRIVATE void
SetupPaths(void)
{
	// TODO: make this more dynamic, also remove global by
	// passing a string to Load-/ShouldReloadCode instead

	ssize_t NumChars = readlink("/proc/self/exe", GameDirectory, sizeof(GameDirectory));

	YASSERT(NumChars >= 0);
	YASSERT(NumChars < (ssize_t) sizeof(GameDirectory));

	GameDirectory[NumChars] = '\0'; /* just to be sure */

	printf("[GAME LOCATION] %s\n", GameDirectory);

	while (NumChars >= 1 && GameDirectory[NumChars-1] != '/')
	{
		NumChars -= 1;
	}

	YASSERT(NumChars < (ssize_t) sizeof(LibraryLocation));
	YASSERT(NumChars < 2048);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstringop-truncation"
	strncpy(LibraryLocation, GameDirectory, (size_t) NumChars);
#pragma GCC diagnostic pop
	LibraryLocation[NumChars] = '\0';
	strcpy(LibraryLocation + NumChars, LibraryName);

	printf("[LIBRARY LOCATION] %s\n", LibraryLocation);

	NumChars -= 1;

	while (NumChars >= 1 && GameDirectory[NumChars-1] != '/')
	{
		NumChars -= 1;
	}



	memset(GameDirectory + NumChars, 0, sizeof(GameDirectory) - (size_t) NumChars);
	int Error = chdir(GameDirectory);

	if (Error != 0)
	{
		perror("Could not change directory.");
		abort();
	}

	printf("[WORKING DIRECTORY] %s\n", GameDirectory);
}

////////////////////////////////////////////////////////////////////////////////
/// DYNAMICS ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

GAME_INIT(GameInit)
{
	GameHandle = LoadGameCode();

	YASSERT(GameHandle.IsValid);

	b32 Result = GameHandle.Game.Init(Platform);

	return Result;
}

GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
	enum reload_result ReloadResult = TryReload(&GameHandle);

	if (ReloadResult == GAME_CODE_DID_RELOAD)
	{
		game *Game = (game *) Platform->Memory.PersistentMemory;
		Game->TranStateInitialized = YARPG_FALSE;
		puts("Loaded new version.");
	}
	else if (ReloadResult == GAME_CODE_COULDNT_RELOAD)
	{
		puts("Couldnt load new version.");
	}


	game_update_and_render_report Report
		= GameHandle.Game.UpdateAndRender(Platform, DeltaTime);

	return Report;
}

GAME_PLAY_SOUND(GamePlaySound)
{
	real_sound_buffer Result = GameHandle.Game.PlaySound(Platform, DeltaTime,
							     TargetSampleRate, NumSamples);
	return Result;
}

GAME_RELEASE(GameRelease)
{
	GameHandle.Game.Release(Platform);
}

GAME_COLLATE_DEBUG_EVENTS(GameCollateDebugEvents)
{
	GameHandle.Game.CollateDebugEvents(Platform, ThreadId, Events);
}

PRIVATE void
ResetEventStorage(event_storage *Events)
{
	Events->End = 0;
	Events->Start = CurrentTimeStamp();
}

PRIVATE event_storage
SwapEventBuffers(event_storage New, thread_context *Thread)
{
	event_storage Result;
	thread_debug_state *State;
	ResetEventStorage(&New);
	WITHDEBUGSTATE(State, Thread)
	{
		Result = State->Events;
		State->Events = New;
	}
	return Result;
}

////////////////////////////////////////////////////////////////////////////////

int
main(int ArgumentCount, char *Arguments[])
{
	SetupPaths();

	YSASSERT(sizeof(i64) >= sizeof(pid_t), "Cannot cast pid_t to i64!");
	Pid = (i64) getpid();
	printf("[PID] Current PID: %ld\n", Pid);

	opengl Options = {
		.Major = 3, .Minor = 3, .Profile = GLFW_OPENGL_CORE_PROFILE,
		.AntiAlias = 4,
		.ForwardCompat = GL_TRUE,
		.Resizable = GL_TRUE,
		.FullScreen = GL_FALSE,
	};

	ShowDebug = 0;

	InitGLFW();
	InitAlsaAudio(&AudioState);

	Window = OpenWindow(Options, Title, Width, Height);

	glfwGetWindowContentScale(Window, &Platform.Info.DPIXScale,
				          &Platform.Info.DPIYScale);

	glfwGetWindowSize(Window, (int *) &Platform.Info.WindowWidth,
			          (int *) &Platform.Info.WindowHeight);

	printf("[PLATFORM INFO] Scale: %f %f\n",
	       Platform.Info.DPIXScale,
	       Platform.Info.DPIYScale);

	InitNuklear(&Ctx, 6 MB,
		    calloc(1, 6 MB));

	glfwSetKeyCallback(Window, key_callback);
	glfwSetCharCallback(Window, char_callback);
	glfwSetMouseButtonCallback(Window, mouse_button_callback);
        glfwSetCursorPosCallback(Window, cursor_position_callback);
	glfwSetWindowContentScaleCallback(Window, window_content_scale_callback);
	glfwSetWindowSizeCallback(Window, window_size_callback);
	glfwSetCursorEnterCallback(Window, cursor_entered_callback);

	i32 FrameCounter = 0;
	float Red[3] = {1.0f, 0.0f, 0.0f};
	float Green[3] = {0.0f, 1.0f, 0.0f};
	float White[3] = {1.0f, 1.0f, 1.0f};

	int SemError = sem_init(&Semaphore, 0, 0);

	YASSERT(!SemError);

	InitFreeType();
	SetupPlatformLayer(&Platform, &Semaphore,
			   PERSISTENT_MEMORY_SIZE, TRANSIENT_MEMORY_SIZE,
			   DEBUG_MEMORY_SIZE);

	InputRecord.Size   = Platform.Memory.PersistentSize;
	InputRecord.Memory = Platform.Memory.PersistentMemory;

	WorkerPool = CreateThreads(Platform.Info.NumWorkerThreads, Window);

	YASSERT(!GameInit(&Platform));

	platform_input ReplayedRecord;

	double Time = glfwGetTime();

	input_device Devices[2];
	Devices[0].Method = InputMethod_MOUSE;
	Devices[1].Method = InputMethod_KEYBOARD;
	Platform.Input.NumDevices = 2;
	Platform.Input.Devices = Devices;

	u64 FrameCount = 0;

	while(!glfwWindowShouldClose(Window))
	{
		double NewTime     = glfwGetTime();

		platform_input *CurrentInput;
		INSTRUMENT(Input)
		{
			CurrentInput = SwapInputBuffers();

			if (CurrentInput->Keyboard.Key[KeyId_NUM_ENTER].IsDown
			    && (CurrentInput->Keyboard.Key[KeyId_NUM_ENTER].HalfTransitions % 2))
			{
				ShowDebug = !ShowDebug;
			}

			ParseInputsNuklear(&Ctx, CurrentInput->CursorPos, &CurrentInput->Mouse);
		}

		if (!Stopped)
		{
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			if (InputRecord.State == RECORDING)
			{
				RecordInput(&InputRecord, CurrentInput);
			} else if (InputRecord.State == REPLAYING) {
				*CurrentInput = ReplayRecord(&InputRecord);
			}

			Platform.Input.CursorPos = CurrentInput->CursorPos;


			// transform cursor pos
			// TODO: should probably just be -1 .. 1

			Platform.Input.CursorPos.Y = Platform.Info.WindowHeight - Platform.Input.CursorPos.Y;

			Devices[0].Data.Mouse = &CurrentInput->Mouse;
			Devices[1].Data.Keyboard = &CurrentInput->Keyboard;

			f64 Delta = NewTime - Time;
			game_update_and_render_report Report
				= GameUpdateAndRender(&Platform, Delta);

			INSTRUMENT(PlayAudio)
			{
				// PlayAudio(&AudioState, &Platform, &GamePlaySound);
			}


			if (!Report.ShouldContinue)
			{
				glfwSetWindowShouldClose(Window, 1);
			}

			FrameCounter++;
		}
		game *GameState = (game *) Platform.Memory.PersistentMemory;

		INSTRUMENT(Nuklear)
		{
			if (ShowDebug)
			{
				DoShowDebugWindow(&Ctx, GameState, &GameHandle, Platform.Memory);
				if (ShowRecorder) DoShowRecorder(&Ctx, GameState);
				if (ShowEntities) DoShowEntities(&Ctx, GameState);

				struct nk_vec2 scale = {1.0, 1.0};
				DrawNuklear(&Ctx, Platform.Info.WindowWidth, Platform.Info.WindowHeight,
					    scale, NK_ANTI_ALIASING_ON);
			}
		}

		INSTRUMENT(FrameWait)
		{
			glfwSwapBuffers(Window);

		}

		Time = NewTime;
		FrameCount++;

		INSTRUMENT(ParseDebug)
		{
			event_storage Current = WorkerPool.Empty;

			Current = SwapEventBuffers(Current, ThisThread);
			GameCollateDebugEvents(&Platform,
					       ThisThread->Id,
					       Current);

			for (idx Idx = 0;
			     Idx < WorkerPool.NumThreads;
			     ++Idx)
			{
				thread_context *Ctx = &WorkerPool.ThreadContexts[Idx];
				Current = SwapEventBuffers(Current, Ctx);
				GameCollateDebugEvents(&Platform,
						       Ctx->Id,
						       Current);
			}

			WorkerPool.Empty = Current;
		}

		INSTRUMENT(PollEvents)
		{
			glfwPollEvents();
		}

	}

	GameRelease(&Platform);
	DestroyThreads(WorkerPool);

	ReleaseNuklear(&Ctx);

	CloseWindow(Window);

	CloseGLFW();

	sem_destroy(&Semaphore);
	pthread_cond_destroy(&AllThreadsIdle);

	DestroyPlatformLayer(&Platform);

	ReleaseGameCode(GameHandle);

	ReleaseFreeType();

	ReleaseAlsaAudio(&AudioState);

	return EXIT_SUCCESS;
}

key_state CONSTFN
KeyStateNextHalftransition(key_state Current)
{
	Current.HalfTransitions += 1;
        Current.IsDown = !Current.IsDown;
	return Current;
}


PRIVATE i32
PerfEventOpen(struct perf_event_attr *HwEvent, pid_t Pid,
	      int Cpu, int GroupFD, unsigned long Flags)
{
	i32 Result;

	Result = (i32) syscall(__NR_perf_event_open, HwEvent, Pid, Cpu,
			       GroupFD, Flags);
	return Result;
}

WRITE_OUT(WriteOut)
{
	YASSERTF(Target == WriteTarget_OUT || Target == WriteTarget_ERR,
		 "%u", Target);
	ssize_t BytesWritten = 0;
	switch (Target)
	{
		break;case WriteTarget_OUT:
		{
			BytesWritten = write(STDOUT_FILENO, Bytes, NumBytes);
		}
		break;case WriteTarget_ERR:
		{
			BytesWritten = write(STDERR_FILENO, Bytes, NumBytes);
		}
		INVALIDDEFAULT();
	}

	if (BytesWritten < 0)
	{
		perror("Bad write");
	}
	else
	{
		YASSERTF((size_t) BytesWritten == NumBytes, "%ld == %lu",
			 BytesWritten, NumBytes);
	}
}

#include "debug/perf.c"
#include "debug/debug_state.c"
