#include <entity_test.h>

// maybe divide entities into archetypes to make queries faster?
// See: https://gamedev.stackexchange.com/a/173785

PRIVATE component
InitComponent(memory_arena *Arena, idx Size)
{
	component C;

	C.NextId = 0;

	C.Ids = IdMapOfArena(Size, Arena);

	return C;
}

PRIVATE void
InitTest(entity_test *Test, memory_arena *Arena, idx MaxSize)
{
#define INIT_COMPONENT(_, Name) do {					\
		Test->Name ## _C = InitComponent(Arena, MaxSize);	\
		Test->Name = PushArray(Arena, MaxSize, *Test->Name );	\
		Test->Name ## _I = PushArray(Arena, MaxSize, *Test->Name ## _I); \
	} while (0);

	COMPONENTS(INIT_COMPONENT);

	Test->NextEntity = 0;

#undef INIT_COMPONENT
}

entity_id
NextEntity(entity_test *Test)
{
	YASSERT(Test->NextEntity < MAXOF((id_index) 0));
	return (entity_id){Test->NextEntity++};
}

PRIVATE id_index
AddComponentPart(component *C, entity_id Id)
{
	id_index Result = -1;

	id_index *Slot = CreateIdSlot(&C->Ids, Id);

	if (Slot)
	{
		Result = C->NextId++;
		*Slot = Result;
	}

	return Result;
}

PRIVATE id_index
GrabComponentPart(component *C, entity_id Id)
{
	id_index Result = -1;

	id_index *Slot = GetIdSlot(&C->Ids, Id);

	if (Slot)
	{
		Result = *Slot;
	}

	return Result;
}

#define DEFINE_COMPONENT_OPS(Name) \
	typeof(*MEMBER(entity_test, Name)) *				\
	Add ## Name(entity_test *Test, entity_id Id, typeof(*MEMBER(entity_test, Name)) Data) \
	{								\
		id_index CId = AddComponentPart(&Test->Name ## _C, Id);	\
									\
		typeof(*MEMBER(entity_test, Name)) *Result = NULL;	\
									\
		if (CId != -1)						\
		{							\
			Test->Name[CId]   = Data;			\
			Test->Name ## _I[CId] = Id;			\
			Result = &Test->Name[CId];			\
		}							\
									\
		return Result;						\
	}								\
									\
	typeof(*MEMBER(entity_test, Name)) *				\
	Grab ## Name(entity_test *Test, entity_id Id)			\
	{								\
		id_index CId = GrabComponentPart(&Test->Name ## _C, Id);	\
									\
		typeof(*MEMBER(entity_test, Name)) *Result = NULL;	\
		if (CId != -1)						\
		{							\
			Result = &Test->Name[CId];			\
		}							\
									\
		return Result;						\
	}

#define DEFINE_COMPONENT_OPS_P(Type, Name) DEFINE_COMPONENT_OPS(Name)

COMPONENTS(DEFINE_COMPONENT_OPS_P)

#undef DEFINE_COMPONENT_OPS_P
#undef DEFINE_COMPONENT_OPS

#define CITER(EntityTest, Name, Val)					\
	for (typeof(*MEMBER(entity_test, Name)) *Val = (EntityTest)->Name; \
	     Val != (EntityTest)->Name + (EntityTest)->Name ## _C.NextId;	\
	     ++Val)

#define CITER_ID(EntityTest, Name, Val, Id)				\
	for (id_index MACROVAR(Idx) = 0;				\
	     MACROVAR(Idx) < (EntityTest)->Name ## _C.NextId;		\
	     ++MACROVAR(Idx))						\
		for (typeof(*MEMBER(entity_test, Name)) *Val = &(EntityTest)->Name[MACROVAR(Idx)]; \
		     Val; Val = NULL)					\
			for (entity_id Id = (EntityTest)->Name ## _I[MACROVAR(Idx)]; \
			     Id.ToIdx != -1; Id.ToIdx = -1)

PRIVATE void
RenderRects(entity_test *Test, render_group *Group)
{
	CITER(Test, Rect, Rect)
	{
		PushRect(Group,
			 Rect->Offset,
			 Rect->Dim,
			 Rect->Color);
	}
}

PRIVATE void
RenderHexs(entity_test *Test, render_group *Group)
{
	CITER(Test, Hex, Hex)
	{
#if 0
		basis_system HexBasis;// = Group->Basis;

		HexBasis.XAxis  = Scale(Hex->Dim.X, Group->Basis.XAxis);
		HexBasis.YAxis  = Scale(Hex->Dim.Y, Group->Basis.YAxis);
		HexBasis.Origin = BasisTransform(V(Hex->Offset.X,
						   Hex->Offset.Y,
						   0), Group->Basis);

		render_group HexGroup = SubRenderGroup(Group, HexBasis);
		PushHexagon(&HexGroup,
			    V(0, 0),
			    1,
			    Hex->Color);
#else
		PushHexagon(Group, Hex->Offset, Hex->Dim.X, Hex->Color);
#endif
	}
}

PRIVATE void
RenderTexts(entity_test *Test, render_group *Group)
{
	CITER(Test, Texture, Text)
	{
		PushSprite(Group,
			   Text->Offset,
			   Text->Dim,
			   Text->Sprite,
			   COLOR.White);
	}
}

PRIVATE void
RenderTris(entity_test *Test, render_group *Group)
{
	CITER(Test, Triangle, Tri)
	{
		PushTriangle(Group,
			     Tri->Points,
			     Tri->Color);
	}
}

PRIVATE void
RenderLines(entity_test *Test, render_group *Group)
{
	CITER(Test, Line, Line)
	{
		PushLine(Group,
			 Line->Start,
			 Line->End,
			 Line->Thickness,
			 Line->Color);
	}
}

PRIVATE void
UpdatePositions(entity_test *Test)
{
	CITER_ID(Test, Position, Pos, Id)
	{
		// TODO: if we sort each array in regards to the entity_id
		// we could just step through each of the three arrays
		// linearly without having to roundtrip the hash table
		// each time
		// instead we just compare the corresponding ids of the
		// entries and do the action if they are the same
		// otherwise we go to the next entry in the arary that
		// had smallest entity_id

		rect_data *Rect = GrabRect(Test, Id);
		text_data *Text = GrabTexture(Test, Id);
		hex_data  *Hex  = GrabHex(Test, Id);

		if (Rect)
		{
			Rect->Offset = *Pos;
		}

		if (Text)
		{
			Text->Offset = *Pos;
		}

		if (Hex)
		{
			Hex->Offset = *Pos;
		}
	}
}

PRIVATE void
UpdateFollows(entity_test *Test, f32 Dt)
{
	CITER_ID(Test, Follow, Followed, Follower)
	{
		v2 *MyPos     = GrabPosition(Test, Follower);
		v2 *FollowPos = GrabPosition(Test, *Followed);

		if (MyPos && FollowPos)
		{
			v2 Delta = Diff(*FollowPos, *MyPos);

			*MyPos = Add(*MyPos, Scale(F32C(0.5) * Dt, Delta));
		}
	}
}

PRIVATE void
UpdateAll(entity_test *Test, f32 Dt)
{
	INSTRUMENT(Follows)
	{
		UpdateFollows(Test, Dt);
	}

	INSTRUMENT(Positions)
	{
		UpdatePositions(Test);
	}
}

PRIVATE void
RenderAll(entity_test *Test, render_group *Group)
{
	RenderTris(Test, Group);
	RenderRects(Test, Group);
	RenderHexs(Test, Group);
	RenderTexts(Test, Group);
	RenderLines(Test, Group);
}

PRIVATE void
ClearComponent(component *C)
{
	C->NextId = 0;
	ClearIdMap(&C->Ids);
}

PRIVATE void
ClearTest(entity_test *Test)
{
	Test->NextEntity = 0;
#define CLEAR_COMPONENT(_, Name) ClearComponent(&Test->Name ## _C);
	COMPONENTS(CLEAR_COMPONENT);
#undef CLEAR_COMPONENT
}
