#include <def.h>
#include <math.h>
#include <util/vector.h>

////////////////////////////////////////////////////////////////////////////////
/// FLAGS

b32 CONSTFN
IsSet(u32 Flags, u32 Flag)
{
	u32 Result = Flags & Flag;
	return (b32)Result;
}

void
SetFlag(u32 *Flags, u32 Flag)
{
	*Flags |= Flag;
}

void
ClearFlag(u32 *Flags, u32 Flag)
{
	*Flags &= ~Flag;
}

u32 CONSTFN
ReadBitsF32(f32 X)
{
	u32 Result;
	YSASSERT(sizeof(Result) == sizeof(X));

	// this is well defined in C
	union
	{
		u32 Bits;
		f32 Val;
	} U = { .Val = X };

	Result = U.Bits;

	return Result;
}

u32 CONSTFN
ReadBitsI32(i32 X)
{
	u32 Result;
	YSASSERT(sizeof(Result) == sizeof(X));

	union
	{
		u32 Bits;
		i32 Val;
	} U = { .Val = X };

	Result = U.Bits;
	return Result;
}

u32 CONSTFN
ReadBitsU32(u32 X)
{
	// This function is mostly for generic code
	u32 Result;
	YSASSERT(sizeof(Result) == sizeof(X));

	union
	{
		u32 Bits;
		u32 Val;
	} U = { .Val = X };

	Result = U.Bits;
	return Result;
}

u64 CONSTFN
ReadBitsF64(f64 X)
{
	u64 Result;
	YSASSERT(sizeof(Result) == sizeof(X));

	union
	{
		u64 Bits;
		f64 Val;
	} U = { .Val = X };

	Result = U.Bits;

	return Result;

}

u64 CONSTFN
ReadBitsI64(i64 X)
{
	u64 Result;
	YSASSERT(sizeof(Result) == sizeof(X));

	union
	{
		u64 Bits;
		i64 Val;
	} U = { .Val = X };

	Result = U.Bits;
	return Result;
}

u64 CONSTFN
ReadBitsU64(u64 X)
{
	u64 Result = X;
	return Result;
}

basis_system CONSTFN
BasisOfRect(rect R)
{
	basis_system Basis;
	Basis.Origin = V(R.BottomLeft.X, R.BottomLeft.Y, 0.0);
	Basis.XAxis  = V(R.MaxX - R.MinX, 0);
	Basis.YAxis  = V(0, R.MaxY - R.MinY);
	return Basis;
}

u32
HashU32(u32 X)
{
	X = ((X >> 16) ^ X) * 0x45d9f3b;
	X = ((X >> 16) ^ X) * 0x45d9f3b;
	X = (X >> 16) ^ X;
	return X;
}

rect
RectOfMWH
(
	v2 Middle,
	f32 Width,
	f32 Height
)
{
	rect Result;

	v2 Delta = V(Width/2, Height/2);

	Result.BottomLeft = Diff(Middle, Delta);
	Result.TopRight   = Add(Middle, Delta);

	return Result;
}

rect
RectOfTB
(
	v2 BottomLeft,
	v2 TopRight
)
{
	rect Result;

	Result.BottomLeft = BottomLeft;
	Result.TopRight = TopRight;

	return Result;
}

rect
RectOfBounds
(
	f32 Bottom,
	f32 Top,
	f32 Left,
	f32 Right
)
{
	rect Result;

	YASSERT(Bottom <= Top);
	YASSERT(Left <= Right);

	Result.MinX = Left;
	Result.MaxX = Right;
	Result.MinY = Bottom;
	Result.MaxY = Top;

	return Result;
}

idx CONSTFN
ModInc
(
	idx Num,
	idx Modulus
)
{
	YASSERTF((Num >= 0) && (Num < Modulus),
		 "0 <= %ld < %ld",
		 Num, Modulus);
	idx Result = Num + 1;
	if (Result == Modulus)
	{
		Result = 0;
	}

	return Result;
}

idx CONSTFN
ModDec
(
	idx Num,
	idx Modulus
)
{
	YASSERTF((Num >= 0) && (Num < Modulus),
		 "0 <= %ld < %ld",
		 Num, Modulus);
	idx Result;
	if (Num == 0)
	{
		Result = Modulus - 1;
	}
	else
	{
		Result = Num - 1;
	}

	return Result;
}
