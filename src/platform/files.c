#include <def.h>
#include <platform.h>
#include <file.h>

#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

FWDDECLARE(linux_file_iterator);
FWDDECLARE(linux_file);
FWDDECLARE(dirent); /* from dirent.h */

static char GameDirectory[2048];

struct linux_file_iterator
{
	int DirFD;
	int CurrentEntry;
	dirent **Entries;
};

struct linux_file
{
	int FileDesc;
	dirent *Entry;
};

/*
 * Usage:
 * byte *Buffer[RIGHT_SIZE];
 * file_iterator *It;
 * for (file_handle *F = StartAssetFiles(&It);
 *      F;
 *      F = NextFile(It))
 * {
 *    ReadFile(F, Start, Length, Buffer);
 *    DoStuff(Buffer);
 * }
 */

static idx
AbsorbErrno(void)
{
	idx Error = errno;
	YASSERT(errno);
	errno = 0;
	return Error;
}

#define DIR_FILTER(Name) int Name(dirent const *Entry)

static DIR_FILTER(AssetFilter)
{
	char const *AssetEnding = ".yaff";

	int EntryLen  = (int) strlen(Entry->d_name);
	int EndingLen = (int) strlen(AssetEnding);

	int const NotAsset = 0;
	int const IsAsset  = 1;

	if (EntryLen < EndingLen)
	{
//                puts("too small");
                return NotAsset;
	}

	int EndingStart = (EntryLen - EndingLen);
	char const *EntryEnding = &Entry->d_name[EndingStart];

	for (int I = 0;
	     I < EndingLen;
	     ++I)
	{
		if (AssetEnding[I] != EntryEnding[I])
		{
//                        printf("%s (bad char at %d)\n", EntryEnding, I);
			return NotAsset;
		}
	}

//	puts("ok!");
	return IsAsset;
}


#undef DIR_FILTER

static file
OpenFile(dirent *Entry, linux_file_iterator *LIter)
{
	file Handle;

	linux_file *Result = malloc(sizeof(*Result));

	int FileDesc = openat(LIter->DirFD, Entry->d_name, O_RDONLY);

	idx Error, Size;

	if (FileDesc >= 0)
	{
		struct stat Buffer;

		fstat(FileDesc, &Buffer);

		YASSERTF(Buffer.st_size >= 0
			 && Buffer.st_size <= IDX_MAX,
			 "%ld", Buffer.st_size);

		Error = 0;
		Size  = Buffer.st_size;
	} else {
		Error = AbsorbErrno();
		Size  = 0;
	}

	Result->FileDesc = FileDesc;
	Result->Entry    = Entry;
	Handle.Error    = Error;
	Handle.Size     = Size;
	Handle.Platform = Result;

	return Handle;
}

static void
CloseHandle(linux_file *Handle)
{
	if (Handle)
	{
                // TODO: dont close if there was a problem opening it
		close(Handle->FileDesc);
		free(Handle->Entry);
		free(Handle);
	}
}

static dirent *
NextEntry(file_iterator *Iter)
{
	linux_file_iterator *LIter = InterlockedRead(&Iter->Platform);

        YASSERT(LIter);
	dirent *Result = NULL;
	if (LIter->CurrentEntry < Iter->NumEntries)
	{
		Result = LIter->Entries[LIter->CurrentEntry];
		LIter->CurrentEntry += 1;
	}

	return Result;
}

#define EMPTY_ITERATOR ((file_iterator) { .Error = -1, .NumEntries = 0, .Platform = NULL })

OPEN_ASSET_ITERATOR(OpenAssetIterator)
{
	file_iterator Iter = EMPTY_ITERATOR;
	linux_file_iterator *LIter = malloc(sizeof(*LIter));

	if (LIter)
	{
		idx Error = 0;

		dirent **NameList;

		char const *ResourceDir = GameDirectory;

		int DirFD = open(ResourceDir, O_RDONLY);

		if (DirFD == -1)
		{
			Error = AbsorbErrno();
		}

		int NumEntries = scandir(ResourceDir, &NameList, &AssetFilter, alphasort);

		/* NOTE: On Error scandir returns a -1;
		 * this is "gracefully" handled in NextEntry so we do not have
		 * to make a special case here! */

		if (NumEntries < 0)
		{
			close(DirFD);
			DirFD = -1;
			Error = AbsorbErrno();
		}

		LIter->DirFD         = DirFD;
		LIter->Entries       = NameList;
		LIter->CurrentEntry  = 0;
		Iter.NumEntries     = NumEntries;
		Iter.Error          = Error;
		Iter.Platform       = LIter;
	}

	return Iter;
}

#define EMPTY_HANDLE ((file) { .Error = -1, .Size = 0, .Platform = NULL })

START_ITERATOR(StartIterator)
{
        file Handle = EMPTY_HANDLE;

	if (!InterlockedRead(&Iter->Error))
	{
		dirent *Current;
		if ((Current = NextEntry(Iter)))
		{
			Handle = OpenFile(Current, Iter->Platform);
		}
	}

	return Handle;
}

NEXT_FILE(NextFile)
{
	file Result = EMPTY_HANDLE;

	if (!InterlockedRead(&Iter->Error))
	{
		dirent *Current;
		if ((Current = NextEntry(Iter)))
		{
			Result = OpenFile(Current, Iter->Platform);
		}
	}

	return Result;
}

extern platform_layer Platform;

READ_FILE(ReadFile)
{
	YASSERTF(Length >= 0, "%ld", Length);
	YASSERTF(Start >= 0, "%ld", Start);

	idx OldError = InterlockedRead(&Handle->Error);

	if (!OldError && Length > 0)
	{
		linux_file *LHandle = Handle->Platform;

		idx Error = 0;

		if (pread(LHandle->FileDesc, Buffer, (size_t) Length, Start) < 0)
		{
			Error = AbsorbErrno();
		}

		// if OldError is not 0 anymore, then we just discard our error.
		// this is why we use a strong exchange here:
		// we do not care if another thread marked an error already
		// but we do care about false negatives!
		InterlockedCompareExchange(&Handle->Error, EXCHANGE_STRONG, &OldError, Error);
	}
}

CLOSE_FILE(CloseFile)
{

	linux_file *LHandle = Handle->Platform;
	CloseHandle(LHandle);
}

CLOSE_ITERATOR(CloseIterator)
{
	linux_file_iterator *LIter = Iter->Platform;

	if (LIter)
	{
		if (LIter->DirFD >= 0) close(LIter->DirFD);

		if (LIter->Entries)
		{
			for (int I = LIter->CurrentEntry;
			     I < Iter->NumEntries;
			     ++I)
			{
				// every other entry already has an associated file handle
				// and is freed through that file handle
				free(LIter->Entries[I]);
			}
			free(LIter->Entries);
		}

		free(LIter);
	}
}
