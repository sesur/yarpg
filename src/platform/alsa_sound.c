#include <def.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include <x86intrin.h>

FWDDECLARE(alsa_audio_state);

struct alsa_audio_state
{
	snd_pcm_t *Handle;
	u32 PeriodTime; /* in µs */
	u64 BufferSize;
	u32 FramesPerPeriod;
	u32 PeriodsPerBuffer;
	u32 SampleRate;
	byte *Buffer;
};

#define ALSA_CALL(Expr, ErrorStr) do {					\
		i32 Error = (Expr);					\
		if (Error < 0)						\
		{							\
			printf("[AUDIO] " ErrorStr ": %s\n", snd_strerror(Error)); \
			return 0;					\
		}							\
	} while (0)

static void
PrintAlsaInformation(alsa_audio_state *State, snd_pcm_hw_params_t *Params)
{
	/* Display information about the PCM interface */

	puts("[AUDIO] Information:");
	i32 Direction;

#define OFFSET "  "

	printf(OFFSET "PCM handle name = '%s'\n",
	       snd_pcm_name(State->Handle));

	printf(OFFSET "PCM state = %s\n",
	       snd_pcm_state_name(snd_pcm_state(State->Handle)));

	{
		snd_pcm_access_t Access;
		snd_pcm_hw_params_get_access(Params,
					     &Access);
		printf(OFFSET "access type = %s\n",
		       snd_pcm_access_name(Access));
	}

	{
		snd_pcm_format_t Format;
		snd_pcm_hw_params_get_format(Params, &Format);
		printf(OFFSET "format = '%s' (%s)\n",
		       snd_pcm_format_name(Format),
		       snd_pcm_format_description(Format));
	}

	{
		snd_pcm_subformat_t SubFormat;
		snd_pcm_hw_params_get_subformat(Params,
						&SubFormat);
		printf(OFFSET "subformat = '%s' (%s)\n",
		       snd_pcm_subformat_name(SubFormat),
		       snd_pcm_subformat_description(SubFormat));
	}

	{
		u32 NumChannels;
		snd_pcm_hw_params_get_channels(Params, &NumChannels);
		printf(OFFSET "channels = %u\n", NumChannels);
	}

	{
		u32 SamplingRate;
		snd_pcm_hw_params_get_rate(Params, &SamplingRate, &Direction);
		printf(OFFSET "rate %s %u bps\n",
		       Direction == 0 ? "=" :
		       Direction > 0 ? ">" : "<",
		       SamplingRate);
	}

	{
		u32 PeriodTime;
		snd_pcm_hw_params_get_period_time(Params, &PeriodTime, &Direction);
		printf(OFFSET "period time %s %u us\n",
		       Direction == 0 ? "=" :
		       Direction > 0 ? ">" : "<",
		       PeriodTime);
	}

	{
		snd_pcm_uframes_t FramesPerPeriod;
		snd_pcm_hw_params_get_period_size(Params, &FramesPerPeriod, &Direction);
		printf(OFFSET "period size %s %lu frames\n",
		       Direction == 0 ? "=" :
		       Direction > 0 ? ">" : "<",
		       FramesPerPeriod);
	}

	{
		u32 BufferTime;
		snd_pcm_hw_params_get_buffer_time(Params, &BufferTime, &Direction);
		printf(OFFSET "buffer time %s %u us\n",
		       Direction == 0 ? "=" :
		       Direction > 0 ? ">" : "<",
		       BufferTime);
	}


	{
		snd_pcm_uframes_t FramesPerBuffer;
		snd_pcm_hw_params_get_buffer_size(Params,
						  &FramesPerBuffer);
		printf(OFFSET "buffer size = %lu frames\n", FramesPerBuffer);
	}

	{
		u32 Periods;
		snd_pcm_hw_params_get_periods(Params, &Periods, &Direction);
		printf(OFFSET "periods per buffer %s %u periods\n",
		       Direction == 0 ? "=" :
		       Direction > 0 ? ">" : "<",
		       Periods);
	}

	{
		u32 Numerator, Denumerator;
		snd_pcm_hw_params_get_rate_numden(Params,
						  &Numerator, &Denumerator);
		printf(OFFSET "exact rate = %u/%u bps\n", Numerator, Denumerator);
	}


	{
		i32 SigBits = snd_pcm_hw_params_get_sbits(Params);
		printf(OFFSET "significant bits = %d\n", SigBits);
	}

	// deprecated
	/* { */
	/* 	u32 TickTime; */
	/* 	snd_pcm_hw_params_get_tick_time(Params, &TickTime, &Direction); */
	/* 	printf(OFFSET "tick time = %u us\n", TickTime); */
	/* } */


	{
		i32 Batched = snd_pcm_hw_params_is_batch(Params);
		printf(OFFSET "is batched = %d\n", Batched);
	}

	{
		i32 BlockTransfered = snd_pcm_hw_params_is_block_transfer(Params);
		printf(OFFSET "is block transfer = %d\n", BlockTransfered);
	}

	{
		i32 Doubled = snd_pcm_hw_params_is_double(Params);
		printf(OFFSET "is double = %d\n", Doubled);
	}

	{
		i32 HalfDuplexed = snd_pcm_hw_params_is_half_duplex(Params);
		printf(OFFSET "is half duplex = %d\n", HalfDuplexed);
	}

	{
		i32 JointDuplexed = snd_pcm_hw_params_is_joint_duplex(Params);
		printf(OFFSET "is joint duplex = %d\n", JointDuplexed);
	}

	{
		i32 Overranged = snd_pcm_hw_params_can_overrange(Params);
		printf(OFFSET "can overrange = %d\n", Overranged);
	}

	{
		i32 MMaped = snd_pcm_hw_params_can_mmap_sample_resolution(Params);
		printf(OFFSET "can mmap = %d\n", MMaped);
	}

	{
		i32 AcceptsPause = snd_pcm_hw_params_can_pause(Params);
		printf(OFFSET "can pause = %d\n", AcceptsPause);
	}

	{
		i32 AcceptsResume = snd_pcm_hw_params_can_resume(Params);
		printf(OFFSET "can resume = %d\n", AcceptsResume);
	}

	{
		i32 SyncStarted = snd_pcm_hw_params_can_sync_start(Params);
		printf(OFFSET "can sync start = %d\n", SyncStarted);
	}

#undef OFFSET
}

static b32
SetupHWParams(alsa_audio_state *State)
{
	/* snd_pcm_hw_params_t *Params; */

	snd_pcm_t *Handle = State->Handle;

	/* snd_pcm_hw_params_alloca(&Params); */

	/* snd_pcm_hw_params_any(Handle, Params); */

	/* /\* set the following params: */
	/*  * - Left & Right channel interleaved, i.e. 1 sample left, 1 sample right, .... */
	/*  * - 16 bit little endian, if CPU is big endian, set to big endian instead */
	/*  * - enable stereo audio */
	/*  * - set to 48000 hz (standard value) */
	/*  * - period to 32 frames, 1 frame is 2 samples (1 left, 1 right) */
	/*  *\/ */

	/* ALSA_CALL(snd_pcm_hw_params_set_access(Handle, Params, */
	/* 				       SND_PCM_ACCESS_RW_INTERLEAVED), */
	/* 	  "unable to set left/right interleaved access"); */

	/* ALSA_CALL(snd_pcm_hw_params_set_format(Handle, Params, */
	/* 				       SND_PCM_FORMAT_S16_LE), */
	/* 	  "unable to set little endian format"); */

	/* ALSA_CALL(snd_pcm_hw_params_set_channels(Handle, Params, 2), */
	/* 	  "unable to enable stereo"); */

	/* u32 SampleRate = 48000; */
	/* i32 Direction; /\* -1, 0, or 1 depending on wether hw rate is lower, approx same, or higher*\/ */
	/* ALSA_CALL(snd_pcm_hw_params_set_rate_near(Handle, Params, */
	/* 					  &SampleRate, &Direction), */
	/* 	  "unable to set sampling rate"); */

        /* printf("[AUDIO] sampling rate direction: %d\n", Direction); */

	/* snd_pcm_uframes_t FramesPerPeriod = 48000; */
	/* ALSA_CALL(snd_pcm_hw_params_set_period_size_near(Handle, */
	/* 						 Params, &FramesPerPeriod, &Direction), */
	/* 	  "unable to set period size"); */

	/* printf("[AUDIO] period size direction: %d\n", Direction); */

	/* ALSA_CALL(snd_pcm_hw_params(Handle, Params), */
	/* 	  "unable to set hw parameters"); */

	/* /\* setup the alsa buffer *\/ */
	/* ALSA_CALL(snd_pcm_hw_params_get_period_size(Params, &FramesPerPeriod, */
	/* 					    &Direction), */
	/* 	  "unable to get period size"); */

	/* printf("[AUDIO] period size direction (get): %d\n", Direction); */

	/* State->FramesPerPeriod = FramesPerPeriod; */
	/* State->BufferSize = FramesPerPeriod * 4; /\* 2 bytes/sample, 2 channels *\/ */
	/* State->Buffer = (byte *) malloc(State->BufferSize); */

	/* ALSA_CALL(snd_pcm_hw_params_get_period_time(Params, */
	/* 					    &State->PeriodTime, &Direction), */
	/* 	"unable to get period time"); */

	/* printf("[AUDIO] period time direction (get): %d\n", Direction); */

	/* PrintAlsaInformation(State, Params); */

	snd_pcm_hw_params_t *Params;
	snd_pcm_hw_params_alloca(&Params);

	snd_pcm_hw_params_any(State->Handle, Params);
	snd_pcm_hw_params_set_access(State->Handle, Params, SND_PCM_ACCESS_RW_INTERLEAVED);
	snd_pcm_hw_params_set_format(State->Handle, Params, SND_PCM_FORMAT_S16_LE);
	snd_pcm_hw_params_set_channels(State->Handle, Params, 1);
	snd_pcm_hw_params_set_rate(State->Handle, Params, 48000, 0);
        //	snd_pcm_hw_params_set_period_time(State->Handle, Params, 100000, 0); // 0.1 seconds

	// 800 f/t = 48000 f/s * 1/60 t/s
	snd_pcm_hw_params_set_buffer_size(State->Handle, Params, 8000);
	snd_pcm_hw_params_set_period_size(State->Handle, Params, 800, 0);
	snd_pcm_hw_params_set_periods(State->Handle, Params, 60, 0);

	PrintAlsaInformation(State, Params);
	snd_pcm_hw_params(State->Handle, Params);

	i32 Direction;
	snd_pcm_uframes_t FramesPerPeriod;
	snd_pcm_hw_params_get_period_size(Params, &FramesPerPeriod, &Direction);
	State->FramesPerPeriod = (u32) FramesPerPeriod;
	snd_pcm_hw_params_get_periods(Params, &State->PeriodsPerBuffer, &Direction);
	snd_pcm_hw_params_get_rate(Params, &State->SampleRate, &Direction);
	return 1;
}

#define SAMPLE_RATE 48000

i16 Samples[SAMPLE_RATE] = {0};

static b32
InitAlsaAudio(alsa_audio_state *State)
{
	int Mode = SND_PCM_NONBLOCK;
	ALSA_CALL(snd_pcm_open(&State->Handle, "default",
			       SND_PCM_STREAM_PLAYBACK, Mode),
		  "unable to open pcm device");

	YASSERT(State->Handle, "Failed to get default audio device.");

	ALSA_CALL(snd_pcm_nonblock(State->Handle, 1 /* nonblock */), "set non block");

	printf("[AUDIO] Handle = %lx\n", (idx) State->Handle);

	if (!SetupHWParams(State))
	{
		return 0;
	}

        puts("[AUDIO] Initialization finished.");

	snd_pcm_status_t *Status;
	snd_pcm_status_alloca(&Status);
	snd_pcm_status(State->Handle, Status);

	snd_htimestamp_t Ts;
	snd_pcm_status_get_audio_htstamp(Status, &Ts);

	u64 Nsec = (u64) Ts.tv_nsec + (u64) Ts.tv_sec * U64C(1000000000);

	printf("Timestamp: %lu\n", Nsec);


	ALSA_CALL(snd_pcm_prepare(State->Handle), "Prepare");

	idx NumSamples = snd_pcm_avail(State->Handle);
	YASSERT(NumSamples >= 0);
	snd_pcm_writei(State->Handle,
		       Samples,
		       (udx) NumSamples);


	return 1;
}

static void
CheckAlsaState(snd_pcm_t *Handle)
{
	snd_pcm_state_t State = snd_pcm_state(Handle);
	char const *S = "ERROR";
	switch(State)
	{
		break;case SND_PCM_STATE_OPEN:         S = "OPEN";
		break;case SND_PCM_STATE_SETUP:        S = "SETUP";
		break;case SND_PCM_STATE_PREPARED:     S = "PREPARED";
		break;case SND_PCM_STATE_RUNNING:      S = "RUNNING";
		break;case SND_PCM_STATE_XRUN:         S = "XRUN";
		break;case SND_PCM_STATE_DRAINING:     S = "DRAINING";
		break;case SND_PCM_STATE_PAUSED:       S = "PAUSED";
		break;case SND_PCM_STATE_SUSPENDED:    S = "SUSPENDED";
		break;case SND_PCM_STATE_DISCONNECTED: S = "DISCONNECTED";
		break;case SND_PCM_STATE_PRIVATE1:     S = "PRIVATE1";
	}
	printf("State: %s\n", S);
}

static b32
PlayAudio(alsa_audio_state *State,
	  platform_layer *Platform,
	  game_play_sound *Callback)
{
	idx PeriodSize       = State->FramesPerPeriod;
	idx NumSamplesToPlay = PeriodSize;

	f32 FScale = -((f32) I16_MIN);
	__m128 FScaleWide = _mm_set1_ps(FScale);
	while (NumSamplesToPlay >= PeriodSize)
	{
		idx NumSamples;
		idx Delay;
		int Error;
		if ((Error = snd_pcm_avail_delay(State->Handle, &NumSamples, &Delay)) < 0)
		{
			snd_pcm_recover(State->Handle, Error, 0);
			break;
		}

		if (NumSamples > NumSamplesToPlay)
		{
			NumSamples = NumSamplesToPlay;
		}

		while (NumSamples >= PeriodSize)
		{
			idx SamplesToPlay = NumSamples;
			if (SamplesToPlay > SAMPLE_RATE)
			{
				SamplesToPlay = SAMPLE_RATE;
			}
			SamplesToPlay -= SamplesToPlay % PeriodSize;

			real_sound_buffer Sound =
				Callback(Platform, 0.0, (i32) State->SampleRate, (i32) SamplesToPlay);

			idx SampleIdx = 0;
			for (;
			     SampleIdx + 8 <= Sound.NumSamples;
			     SampleIdx += 8)
			{
				__m128 FSamplesW[2];
				__m128i SamplesW[2];

				for (udx I = 0;
				     I < ARRAYCOUNT(FSamplesW);
				     ++I)
				{
					FSamplesW[I] = _mm_load_ps(Sound.Data + SampleIdx + 4 * I);
					FSamplesW[I] = _mm_mul_ps(FSamplesW[I], FScaleWide);
					SamplesW[I]  = _mm_cvtps_epi32(FSamplesW[I]);
				}

				__m128i Result = _mm_packs_epi32(SamplesW[0], SamplesW[1]);

				_mm_store_si128((__m128i *) (Samples + SampleIdx), Result);
			}

			if (Sound.NumSamples < SamplesToPlay)
			{
				memset(Samples + Sound.NumSamples, 0,
				       (udx) (SamplesToPlay - Sound.NumSamples) * sizeof(*Samples));
			}

			for (;
			     SampleIdx < Sound.NumSamples;
			     ++SampleIdx)
			{
				i32 Value = (i32) roundf(FScale * Sound.Data[SampleIdx]);

				if (Value >= I16_MAX)
				{
					Value = I16_MAX;
				} else if (Value <= I16_MIN)
				{
					Value = I16_MIN;
				}

				Samples[SampleIdx] = (i16) Value;
			}

			long Error = snd_pcm_writei(State->Handle,
						    Samples,
						    (udx) SamplesToPlay);

			NumSamples -= SamplesToPlay;

			if (Error < 0)
			{
                                snd_pcm_recover(State->Handle, (int) Error, 0);
			} else {
				if ((idx) Error != SamplesToPlay)
				{
					printf("[AUDIO] short write. %ld -- %ld\n",
					       Error, SamplesToPlay);

				}
				NumSamplesToPlay -= Error;
			}
		}
	}

	return 1;
}

static void
ReleaseAlsaAudio(alsa_audio_state *State)
{
	snd_pcm_drain(State->Handle);
	snd_pcm_close(State->Handle);
        free(State->Buffer);
}

/* snd_pcm_start, snd_pcm_avail, snd_pcm_mmap_begin,
   snd_pcm_mmap_commit, snd_pcm_status_gt_htstamp */
/* snd_pcm_rewind !!, snd_pcm_delay, snd_pcm_rewindable */

// https://www.youtube.com/watch?v=-wDVPreDNjE
