#include <game/world_event.h>

FWDDECLARE(world_queue_alloc);

void
InitWorldEventQueue(world_event_queue *Queue)
{
	Queue->DataStart = ARRAYCOUNT(Queue->Data);
	Queue->NumItems  = 0;
	Queue->Next      = NULL;
}

struct world_queue_alloc
{
	world_event *New;
	void        *Data;
};

static world_queue_alloc
AllocHeaderAndData(world_event_queue *Queue, i32 DataSize, i32 DataAlignment)
{
	world_queue_alloc Result = { .New = NULL, .Data = NULL, };

	world_event *New = NULL;
	void        *Data = NULL;

	i32 NewHeaderEnd = 0;

	{
		i32 Start = sizeof(world_event) * Queue->NumItems;
		i32 End   = Start + sizeof(world_event);

		if (End < Queue->DataStart)
		{
			New = ((world_event *) Queue->Data) + Queue->NumItems;
			NewHeaderEnd = End;
		}
	}

	i32 NewDataStart = 0;
	{
		i32 End = Queue->DataStart;

		i32 Start = End - DataSize;

		void *DataStart = PREVALIGNED(Queue->Data + Start, DataAlignment);

		ptr RealStart = (ptr) DataStart - (ptr) Queue->Data;
		YASSERT(RealStart <= MAXOF(NewHeaderEnd));
		if ((i32) RealStart > NewHeaderEnd)
		{
			Data = DataStart;
			NewDataStart = (i32) RealStart;
		}
	}

	if (New && Data)
	{
		Result.New = New;
		Result.Data = Data;
		Queue->NumItems += 1;
		Queue->DataStart = NewDataStart;
	} else {
		FormatOut("[EVENT QUEUE] Full with {i32} items.\n",
			  Queue->NumItems);
	}

	return Result;
}

b32
AddCollisionEvent(world_event_queue *Queue, world_collision_event Event)
{
	b32 EventAdded = YARPG_FALSE;

	world_event *New                     = NULL;
	world_collision_event *CollisionData = NULL;

	world_queue_alloc Alloc = AllocHeaderAndData(Queue,
					       (i32) sizeof(world_collision_event),
					       (i32) alignof(world_collision_event));

	if (Alloc.New)
	{
		New = Alloc.New;
		CollisionData = (world_collision_event *) Alloc.Data;

		New->Event.Collision = CollisionData;
		New->Type = WorldEventType_COLLISION;
		*CollisionData = Event;

		EventAdded = YARPG_TRUE;
	}

        return EventAdded;
}

b32
AddQuitEvent(world_event_queue *Queue)
{
	b32 EventAdded = YARPG_FALSE;

	world_event *New = NULL;

	world_queue_alloc Alloc = AllocHeaderAndData(Queue, 0, 1);

	if (Alloc.New)
	{
		New = Alloc.New;
		New->Type = WorldEventType_QUIT;
		New->Event.Unused = NULL;
		EventAdded = YARPG_TRUE;
	}

	return EventAdded;
}

b32
AddMovementFinishedEvent(world_event_queue *Queue,
			 world_mov_finished_event Event)
{
	b32 EventAdded = YARPG_FALSE;

	world_event *New                    = NULL;
	world_mov_finished_event *EventData = NULL;

	world_queue_alloc Alloc = AllocHeaderAndData(Queue,
					       (i32) sizeof(*EventData),
					       (i32) alignof(*EventData));

	if (Alloc.New)
	{
		New = Alloc.New;
		EventData = (world_mov_finished_event *) Alloc.Data;

		New->Event.MovementFinished = EventData;
		New->Type = WorldEventType_MOVFINISHED;
		*EventData = Event;

		EventAdded = YARPG_TRUE;
	}

	return EventAdded;
}
