#include <def.h>
#include <game/battlefield.h>
#include <util/random.h>
#include <util/layout.h>

#include "../util/hex.c"

#include "battlefield_event.c"
#include <util/noise.h>

/* Return wether entity ``Idx`` is playing an animation. */
b32
IsBusy(battlefield *Field, i32 Idx)
{
	b32 Busy = Field->UnitStats[Idx].Flags.IsMoving || Field->UnitStats[Idx].Flags.IsAttacking;
	/* b32 Result = Field->Attack[Idx].DefIdx < 0; // no attack animation */
	/* Result = Result && Eq(Field->MovGoal[Idx].Goal, INVALID_POS); // no move animation */
	return Busy;
}

static tile_feature const OceanFeatures[] =
{
	TileFeature_NONE,
};

static tile_feature const LakeFeatures[] =
{
	TileFeature_NONE,
};

static tile_feature const PlainsFeatures[] =
{
	TileFeature_NONE,
	TileFeature_MARSH,
	TileFeature_FORREST,
	TileFeature_HILLS,
};

static tile_feature const GraslandFeatures[] =
{
	TileFeature_NONE,
	TileFeature_MARSH,
	TileFeature_FORREST,
	TileFeature_RAINFORREST,
	TileFeature_HILLS,
};

static tile_feature const SnowFeatures[] =
{
	TileFeature_NONE,
	TileFeature_HILLS,
};

static tile_feature const TundraFeatures[] =
{
	TileFeature_NONE,
	TileFeature_MARSH,
	TileFeature_FORREST,
	TileFeature_HILLS,
};

static tile_feature const DesertFeatures[] =
{
	TileFeature_NONE,
	TileFeature_HILLS,
	TileFeature_MARSH,
};

static i32 TypeNumFeatures[TileType_COUNT] =
{
	[TileType_OCEAN]    = ARRAYCOUNT(OceanFeatures),
	[TileType_LAKE]     = ARRAYCOUNT(LakeFeatures),
	[TileType_PLAINS]   = ARRAYCOUNT(PlainsFeatures),
	[TileType_GRASLAND] = ARRAYCOUNT(GraslandFeatures),
	[TileType_SNOW]     = ARRAYCOUNT(SnowFeatures),
	[TileType_TUNDRA]   = ARRAYCOUNT(TundraFeatures),
	[TileType_DESERT]   = ARRAYCOUNT(DesertFeatures),
};

static tile_feature const * const FeatureList[] =
{
	[TileType_OCEAN]    = OceanFeatures,
	[TileType_LAKE]     = LakeFeatures,
	[TileType_PLAINS]   = PlainsFeatures,
	[TileType_GRASLAND] = GraslandFeatures,
	[TileType_SNOW]     = SnowFeatures,
	[TileType_TUNDRA]   = TundraFeatures,
	[TileType_DESERT]   = DesertFeatures,
};

PRIVATE tile_feature
RandomFeatureOfType(rand64 *R, tile_type Type)
{
	i32 FeatureNum = NextInRange(R, TypeNumFeatures[Type]);
	tile_feature Result = FeatureList[Type][FeatureNum];
	return Result;
}

PRIVATE v2
WorldPosition(iv2 Pos)
{
	v2 AsV2 = ToFloat(Pos);
	v2 WorldPos = HexToLinear(AsV2);
	return WorldPos;
}

PRIVATE tile_data
RandomTileData(rand64 *R)
{
	tile_data Result;

	Result.Type         = NextInRange(R, TileType_COUNT);
	Result.Feature      = RandomFeatureOfType(R, Result.Type);
	Result.IsImpassible = \
		(Result.Type == TileType_OCEAN)
		|| (Result.Type == TileType_LAKE);
	Result.IsOnFire     = 0;
	Result.IsFlooded    = 0;

	return Result;
}

PRIVATE entity_id CONSTFN
PosId(iv2 Pos)
{
	YASSERT(I16_MIN <= Pos.X && Pos.X <= I16_MAX);
	YASSERT(I16_MIN <= Pos.X && Pos.Y <= I16_MAX);
	u64 XBits = (u16)ReadBits(Pos.X);
	u64 YBits = (u16)ReadBits(Pos.Y);

	u64 Bits = (XBits << 32) | YBits;
	idx Id   = (idx) Bits;
	YASSERTF(Id >= 0, "(%d, %d) -> %ld", Pos.X, Pos.Y, Id);
	entity_id Result = (entity_id) { .ToIdx = Id };
	return Result;
}

id_index *
GetTileSlot(tile_storage *Store, iv2 Pos)
{
#if 1
	entity_id Id = PosId(Pos);
	id_index *Result = GetIdSlot(&Store->Map, Id);
	return Result;
#else
	i32 *Result = NULL;

	YASSERT(Store->Size < Store->Capacity);
	YASSERT(ISPOWOFTWO(Store->Capacity));

	u64 Hash = PosHashOf(Pos);

	idx HashIndex = Hash & (Store->Capacity - 1);
	idx ProbeCount = 0;

	while (1)
	{
		i32 *Slot = &Store->IdToIdx[HashIndex];
		i32 Val = *Slot;
		if (Val == -1)
		{
			break;
		}
		else
		{
			iv2 SlotPos = Store->Position[Val];

			if (Eq(SlotPos, Pos))
			{
				Result = &Store->IdToIdx[HashIndex];
				break;
			}

			idx SlotIdx = PosHashOf(SlotPos) & (Store->Capacity - 1);
			idx SlotProbeCount = HashIndex - SlotIdx;
			if (SlotProbeCount < 0) SlotProbeCount += Store->Capacity;
			YASSERT(SlotProbeCount >= 0);

			if (ProbeCount > SlotProbeCount)
			{
				// if the position was actually in here
				// we wouldve swapped with this
				break;
			}

			HashIndex = HashIndex + 1;
			if (HashIndex == Store->Capacity)
			{
				HashIndex = 0;
			}
			ProbeCount += 1;
		}
	}

	return Result;
#endif
}

PRIVATE id_index
AddTileSlot(tile_storage *Store, entity_id Id)
{
#if 1

	id_index *Slot = CreateIdSlot(&Store->Map, Id);
	YASSERT(Slot);

	YASSERT(Store->End != MAXOF(Store->End));
	id_index Idx = Store->End++;

	*Slot = Idx;
	return Idx;
#else
	// using robin hood hashing; see https://programming.guide/robin-hood-hashing.html
	YASSERT(Store->Size < Store->Capacity);
	YASSERT(ISPOWOFTWO(Store->Capacity));

	i32 Result = Store->Size++;

	idx HashIndex = Hash & (Store->Capacity - 1);
	idx ProbeCount = 0;
	i32 Idx = Result;

	while (1)
	{
		i32 *Slot = &Store->IdToIdx[HashIndex];
		i32 Val = *Slot;
		if (Val == -1)
		{
			*Slot = Idx;
			break;
		}
		else
		{
			iv2 TakenPos = Store->Position[Val];
			idx WantedIdx = PosHashOf(TakenPos) & (Store->Capacity - 1);
			idx TakenProbeCount = HashIndex - WantedIdx;
			if (TakenProbeCount < 0) TakenProbeCount += Store->Capacity;
			YASSERT(TakenProbeCount >= 0);
			if (ProbeCount > TakenProbeCount)
			{
				// swap
				*Slot = Idx;

				ProbeCount = TakenProbeCount;
				Idx = Val;
			}
			HashIndex = HashIndex + 1;
			if (HashIndex == Store->Capacity)
			{
				HashIndex = 0;
			}
			ProbeCount += 1;
		}
	}

	return Result;
#endif
}

PRIVATE void
SetupTileStorage(tile_storage *Store, idx Capacity, memory_arena *Arena)
{
	YASSERT(Capacity > 0 && Capacity <= I32_MAX);

	// Occupancy of 1 would probably degrade performance!
	Store->Map      = IdMapOfArena(2 * Capacity, Arena);
	Store->End      = 0;
	Store->Position = PushArray(Arena, Capacity, *Store->Position);
	Store->Data     = PushArray(Arena, Capacity, *Store->Data);
	Store->NeighborIds = PushArray(Arena, 6 * Capacity, *Store->NeighborIds);
	Store->NeighborsEnd = PushArray(Arena, Capacity, *Store->NeighborsEnd);
}

PRIVATE void
SetupNeighbors(tile_storage *Store)
{
	idx LastNeighbor = 0;
	for (idx Tile = 0;
	     Tile < Store->End;
	     ++Tile)
	{
		iv2 Pos = Store->Position[Tile];
		i32 X = Pos.X;
		i32 Y = Pos.Y;
		id_index *Slot = GetTileSlot(Store, Pos);
		YASSERT(Slot);
		id_index Idx = *Slot;

		id_index *Candidates[6] =
			{
				GetTileSlot(Store, IV((X-1), (Y))),
				GetTileSlot(Store, IV((X+1), (Y))),
				GetTileSlot(Store, IV((X), (Y-1))),
				GetTileSlot(Store, IV((X), (Y+1))),
				GetTileSlot(Store, IV((X+1), (Y-1))),
				GetTileSlot(Store, IV((X-1), (Y+1))),
			};

		for (idx I = 0;
		     I < 6;
		     ++I)
		{
			if (Candidates[I])
			{
				Store->NeighborIds[LastNeighbor++] = *Candidates[I];
			}
		}

		Store->NeighborsEnd[Idx] = LastNeighbor;
	}
}

PRIVATE void
MakeRectangularBaseMap(tile_storage *Store, idx Width, idx Height)
{
	YASSERT(Width * Height <= Store->Map.Capacity);
	for (i32 Y = 0;
	     Y < Height;
	     ++Y)
	{
		for (i32 X = 0;
		     X < Width;
		     ++X)
		{
			iv2 Pos = IV(X, Y);
			entity_id Id = PosId(Pos);
			i32 Idx = AddTileSlot(Store, Id);
			Store->Position[Idx] = Pos;
		}
	}
}

PRIVATE void
MakeSphericalBaseMap(tile_storage *Store, iv2 Center, idx Radius)
{
	YASSERTF(1 + 3 * Radius * (Radius + 1) <= Store->Map.Capacity,
		 "%ld <= %ld",
		 1 + 3 * Radius * (Radius + 1),
		 Store->Map.Capacity);
	for (idx Y = -Radius;
	     Y <= Radius;
	     ++Y)
	{
		for (idx X = Max(-Radius, -Y - Radius);
		     X <= Min(Radius, -Y + Radius);
		     ++X)
		{
			iv2 Pos = Add(Center, IV(X, Y));
			entity_id Id = PosId(Pos);
			i32 Idx = AddTileSlot(Store, Id);
			Store->Position[Idx] = Pos;
		}
	}
}

PRIVATE void
GenerateForrestMap(tile_storage *Store, idx Width, idx Height,
		   memory_arena *Scratch)
{
	rand64 Rand = RandomSeries(412322);

	//MakeRectangularBaseMap(Store, Width, Height);
	MakeSphericalBaseMap(Store, IV(Width/2, Width/2), Width/2);
	SetupNeighbors(Store);

	idx NumPoints = Store->End;

	idx Current = 0;
	f32 *Heights[2] =
		{
			PushArray(Scratch, NumPoints, *Heights[0]),
			PushArray(Scratch, NumPoints, *Heights[1]),
		};

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		Heights[Current][Point] = NextBinormal(&Rand);
		if (Point == 283)
		{
			Heights[Current][Point] = 1.0;
		}
	}

	idx NumSmoothings = 3;

	for (idx SmoothStage = 0;
	     SmoothStage < NumSmoothings;
	     ++SmoothStage)
	{
		id_index *Neighbor = &Store->NeighborIds[0];
		for (idx Point = 0;
		     Point < NumPoints;
		     ++Point)
		{
			f32 Avg = Heights[Current][Point];
			f32 Num = 1;
			for (;
			     Neighbor != &Store->NeighborIds[Store->NeighborsEnd[Point]];
			     ++Neighbor)
			{
				Avg += Heights[Current][*Neighbor];
				Num += 1;
			}
			Heights[1-Current][Point] = Avg/Num;
		}
		Current = 1 - Current;
	}

	f32 AvgHeight = 0;
	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		AvgHeight += Heights[Current][Point];
	}
	AvgHeight /= (f32) NumPoints;

	f32 *Noise = PushArray(Scratch, NumPoints, *Noise);

	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		Noise[Point] = FractalV2(3,
					 1,
					 1,
					 0.5,
					 0.5,
					 Add(V(3, 3), Scale(0.25, WorldPosition(Store->Position[Point]))));
		if (Heights[Current][Point] > AvgHeight)
		{
			Store->Data[Point].Type = TileType_GRASLAND;
		}
		else
		{
			Store->Data[Point].Type = TileType_PLAINS;
		}
	}

	idx NeighborsStart = 0;
	for (idx Point = 0;
	     Point < NumPoints;
	     ++Point)
	{
		idx NumNeighbors = Store->NeighborsEnd[Point] - NeighborsStart;
		idx NumHigher    = 0;
		idx NumLower     = 0;

		for (idx NeighborIdx = NeighborsStart;
		     NeighborIdx < Store->NeighborsEnd[Point];
		     ++NeighborIdx)
		{
			idx NId = Store->NeighborIds[NeighborIdx];
			if (Noise[Point] > Noise[NId])
			{
				NumHigher += 1;
			}
			else if (Noise[Point] < Noise[NId])
			{
				NumLower += 1;
			}
		}

		NeighborsStart = Store->NeighborsEnd[Point];

		Store->Data[Point].Feature = 0;

		switch (Store->Data[Point].Type)
		{
			break;case TileType_PLAINS:
			{
				if ((f32) NumLower > F32C(0.9) * (f32) NumNeighbors)
				{
					Store->Data[Point].Feature |= TileFeature_MARSH;
				}

				if ((f32) NumHigher > F32C(0.9) * (f32) NumNeighbors)
				{
					Store->Data[Point].Feature |= TileFeature_HILLS;
				}


				if (Noise[Point] > F32C(0.3))
				{
					Store->Data[Point].Feature |= TileFeature_FORREST;
				}
				/* else if (Noise[Point] < -0.8) */
				/* { */
				/* 	Store->Data[Point].Type = TileType_LAKE; */
				/* } */
			}
			break;case TileType_GRASLAND:
			{
				if (Noise[Point] > 0)
				{
					Store->Data[Point].Feature |= TileFeature_FORREST;
				}

				if ((f32) NumHigher > F32C(0.7) * (f32) NumNeighbors)
				{
					Store->Data[Point].Feature |= TileFeature_HILLS;
				}
			}
			break;case TileType_OCEAN:
			{
				INVALIDCODEPATH();
			}
			break;case TileType_LAKE:
			{
				INVALIDCODEPATH();
			}
			break;case TileType_SNOW:
			{
				INVALIDCODEPATH();
			}
			break;case TileType_TUNDRA:
			{
				INVALIDCODEPATH();
			}
			break;case TileType_DESERT:
			{
				INVALIDCODEPATH();
			}
			break;case TileType_COUNT: INVALIDCODEPATH();
			INVALIDDEFAULT();
		}
	}
}

void
RebuildMap(battlefield *Field, idx Width, idx Height,
	   memory_arena *Scratch)
{
	ClearIdMap(&Field->TileStorage.Map);
	Field->TileStorage.End = 0;
	GenerateForrestMap(&Field->TileStorage, Width, Height, Scratch);
}

void
CreateBattlefield(battlefield *New, i32 Width, i32 Height,
		  i32 MaxEntities, memory_arena *Arena,
		  memory_arena *Scratch)
{
	YASSERT(Width > 0);
	YASSERT(Height > 0);

	New->NumEntities    = 0;
	New->MaxEntities    = MaxEntities;
	New->MiddlePos      = V(0, 0);
	New->BattleOver     = 0;

	New->PStart = 0;
	New->PEnd   = 0;

#define ALLOCATE_COMPONENT(New, Comp, Num, Arena) (New)->Comp = PushArray(Arena, Num, *(New)->Comp)

	ALLOCATE_COMPONENT(New, BoardPosition, MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, DrawnPosition, MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, UnitStats,     MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, Color,         MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, Movement,      MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, HasAttacked,   MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, Allegiance,    MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, Attack,        MaxEntities, Arena);
	ALLOCATE_COMPONENT(New, MovGoal,       MaxEntities, Arena);


	u64 NumTiles = (u64) ((f64) (Width * Height) * 1.3);
	idx TileCapacity = Ceil2(NumTiles);

	//ALLOCATE_COMPONENT(New, Tiles,      NumTiles, Arena);
	SetupTileStorage(&New->TileStorage, TileCapacity, Arena);


	for (i64 I = 0;
	     I < MaxEntities;
	     ++I)
	{
		New->UnitStats[I].Flags.IsMoving    = 0;
		New->UnitStats[I].Flags.IsAttacking = 0;
		New->UnitStats[I].Flags.IsDead      = 0;
		New->Attack[I].DefIdx = -1;
		/* New->MovGoal[I].Goal = INVALID_POS; */
	}

	New->MiscSeries = RandomSeries((i64)Width * (i64)Height + New->MaxEntities);
	New->CombatSeries = RandomSeries(NextI64(&New->MiscSeries));

	GenerateForrestMap(&New->TileStorage, Width, Height, Scratch);
}

void
AddEntityToBattlefield(battlefield *Field, iv2 Position, v3 Color)
{

        i32 New = Field->NumEntities++;
	YASSERT(New < Field->MaxEntities);
	Field->BoardPosition[New] = Position;
	Field->DrawnPosition[New] = HexToLinear(ToFloat(Position));
	Field->Color[New]         = Color;
	Field->Allegiance[New]    = -1;
}

void
AddUnitToBattlefield(battlefield *Field, iv2 Position,
		     unit_stats Stats, v3 Color,
		     i32 Allegiance)
{
	i32 New = Field->NumEntities++;
	YASSERT(New < Field->MaxEntities);
	Field->BoardPosition[New]  = Position;
	Field->DrawnPosition[New]  = HexToLinear(ToFloat(Position));
	Field->UnitStats[New]      = Stats;
	Field->Color[New]          = Color;
	Field->Movement[New].Range = 3;
	Field->Movement[New].Used  = 0;
	Field->HasAttacked[New]    = 0;
	Field->Allegiance[New]     = Allegiance;
}

b32
IsEmpty(battlefield *Field, iv2 HexPos)
{
	b32 Empty = YARPG_TRUE;

	for (i32 I = 0;
	     I < Field->NumEntities;
	     ++I)
	{
		if (Field->BoardPosition[I].X == HexPos.X
		    && Field->BoardPosition[I].Y == HexPos.Y)
		{
			Empty = YARPG_FALSE;
			break;
		}
	}

	return Empty;
}

b32
ValidHex(battlefield *Field, iv2 HexPos)
{
	id_index *Slot = GetTileSlot(&Field->TileStorage, HexPos);
	b32 Valid =  Slot != NULL;
	return Valid;
}

i32
GetIndexOfPos(battlefield *Field, iv2 HexPos)
{
	i32 Entity = -1;

	for (i32 I = 0;
	     I < Field->NumEntities;
	     ++I)
	{
		if (Field->BoardPosition[I].X == HexPos.X
		    && Field->BoardPosition[I].Y == HexPos.Y)
		{
			Entity = I;
			break;
		}
	}

	return Entity;
}

PRIVATE void
DrawBody(render_group *RenderGroup, v2 Middle, v4 Color,
	 sprite_id Head, sprite_id Body)
{
	// body
	v2 BodyLB = Middle;
	BodyLB.X -= F32C(0.25);
	BodyLB.Y -= F32C(0.8);
	PushSprite(RenderGroup,
		   BodyLB, V(0.5, 1.1),
		   Body, Color);

	//TODO: The offsets have to be done better!
	// head
	v2 HeadLB = Middle;
	HeadLB.X -= F32C(0.355);
	HeadLB.Y -= F32C(0.1);

	PushSprite(RenderGroup,
		   HeadLB, V(0.75, 0.75),
		   Head, Color);

	//TODO: Better access to glyph tables
	//extern game *Game;

	/* v4 White = V(1, 1, 1, 1); */

	/* rect StringBounds; */
	/* StringBounds.BottomLeft = HeadLB; */
	/* StringBounds.TopRight   = Add(HeadLB, V(0.75, 0.75)); */

	/* DrawString(&Game->Table, RenderGroup, White, StringBounds, */
	/* 	   4, "Test"); */
}

static v4 const TileColors[] =
{
	[TileType_OCEAN]      = V(0.04f, 0.01f, 0.64f, 1.0f),
	[TileType_LAKE]       = V(0.02f, 0.51f, 0.80f, 1.0f),
	[TileType_SNOW]       = V(0.87f, 0.96f, 0.98f, 1.0f),
	[TileType_TUNDRA]     = V(0.68f, 0.74f, 0.76f, 1.0f),
	[TileType_DESERT]     = V(0.95f, 0.79f, 0.55f, 1.0f),
	[TileType_GRASLAND]   = V(0.07f, 0.51f, 0.01f, 1.0f),
	[TileType_PLAINS]     = V(0.50f, 0.73f, 0.40f, 1.0f),
};

YSASSERT(ARRAYCOUNT(TileColors) == TileType_COUNT);

static f32 const TileMoisture[] =
{
	[TileType_OCEAN]      = F32C(1.0),
	[TileType_LAKE]       = F32C(1.0),
	[TileType_SNOW]       = F32C(0.2),
	[TileType_TUNDRA]     = F32C(0.3),
	[TileType_DESERT]     = F32C(0.08),
	[TileType_GRASLAND]   = F32C(0.8),
	[TileType_PLAINS]     = F32C(0.5),
};

YSASSERT(ARRAYCOUNT(TileMoisture) == TileType_COUNT);

PRIVATE void
DrawTile(render_group *Group, v2 Middle, tile_data Data, rand64 *R)
{
	v4 HexColor = TileColors[Data.Type];
	v4 Fudgy    = V(NextBetween(R, F32C(0.8), F32C(1.0)),
			NextBetween(R, F32C(0.8), F32C(1.0)),
			NextBetween(R, F32C(0.8), F32C(1.0)),
			1.0);
//	HexColor = Hadamard(Fudgy, HexColor);
	PushHexagon(Group, Middle, 1.0, HexColor);
}

PRIVATE void
DrawTree(render_group *Group, asset_store *Assets, v2 Middle,
	 tile_data Data, rand64 *R)
{
	f32 Weights[SpriteTag_COUNT] = {[SpriteTag_MOISTURE] = 1.0};
	f32 Desired[SpriteTag_COUNT] = {[SpriteTag_MOISTURE] = TileMoisture[Data.Type]};

	sprite_id Tree = ClosestSpriteMatchOf(Assets, Category_TREE,
					      Weights, Desired);

	v2 Dim = V(.5, 1);
	v2 Offset = Scale(0.5, Dim);

	PushSprite(Group,
			 Diff(Middle, Offset),
			 Dim,
			 Tree,
			 COLOR.White);
}

PRIVATE void
DrawSwamp(render_group *Group, asset_store *Assets, v2 Middle,
		tile_data Data, rand64 *R)
{
	sprite_id Swamp = FirstSpriteOf(Assets, Category_SWAMP);

	v2 Dim = V(1, 1);
	v2 Offset = Scale(0.5, Dim);

	PushSprite(Group,
			 Diff(Middle, Offset),
			 Dim,
			 Swamp,
			 COLOR.White);
}

PRIVATE void
DrawHill(render_group *Group, asset_store *Assets, v2 Middle,
	 tile_data Data, rand64 *R)
{
	sprite_id Hill = FirstSpriteOf(Assets, Category_HILL);

	v2 Dim = V(1, 0.3);
	v2 Offset = Add(V(0, .5), Scale(0.5, Dim));

	PushSprite(Group,
			 Diff(Middle, Offset),
			 Dim,
			 Hill,
			 COLOR.White);
}

PRIVATE void
DrawBattlefield(battlefield *Field, render_group *RenderGroup, asset_store *Assets)
{
	/* i32 Width = Field->Width; */
	/* i32 Height = Field->Height; */

	/* rand64 Series = RandomSeries((i64)Width * (i64)Height + Field->NumEntities); */

	rand64 Series = RandomSeries(Field->TileStorage.End + Field->NumEntities);

	{
		tile_storage *Store = &Field->TileStorage;
		str Buffer = TEMPSTR(20);
		INSTRUMENT_COUNTED(DrawTiles, Store->End)
		{
			for (idx Idx = 0;
			     Idx < Store->End;
			     ++Idx)
			{
				iv2 Pos   = Store->Position[Idx];
				v2 Coords = ToFloat(Pos);
				v2 Middle = HexToLinear(Coords);

				DrawTile(RenderGroup, Middle, Store->Data[Idx], &Series);
			}
		}

		INSTRUMENT_COUNTED(DrawFeatures, Store->End)
		{
			for (idx Idx = 0;
			     Idx < Store->End;
			     ++Idx)
			{
				iv2 Pos   = Store->Position[Idx];
				v2 Coords = ToFloat(Pos);
				v2 Middle = HexToLinear(Coords);

				if (Store->Data[Idx].Feature & TileFeature_HILLS)
				{
					DrawHill(RenderGroup, Assets,
						 Middle, Store->Data[Idx], &Series);
				}

				if (Store->Data[Idx].Feature & TileFeature_MARSH)
				{
					DrawSwamp(RenderGroup, Assets,
						  Middle, Store->Data[Idx], &Series);
				}

				if (Store->Data[Idx].Feature & TileFeature_FORREST)
				{
					DrawTree(RenderGroup, Assets,
						 Middle, Store->Data[Idx], &Series);
				}
			}
		}

#if 0
		INSTRUMENT_COUNTED(DrawPos, Store->End)
		{
			for (idx Idx = 0;
			     Idx < Store->End;
			     ++Idx)
			{
				iv2 Pos   = Store->Position[Idx];
				v2 Coords = ToFloat(Pos);
				v2 Middle = HexToLinear(Coords);

				FormatInto(Reset(&Buffer), VIEWC("({i32}, {i32})"),
					   Pos.X, Pos.Y);

				v4 White = V(1, 1, 1, 1);
				rect StringBounds;
				StringBounds.BottomLeft = Diff(Middle, V(0.5, 0.5));
				StringBounds.TopRight   = Add(Middle, V(0.5, 0.5));
				DrawString(&Game->Table, RenderGroup, White, StringBounds,
					   Buffer.Size, Buffer.Data);
			}
		}
#endif

	}

	v4 FriendColor  = V(0.2, 0.6, 0.05, 1);
	v4 EnemyColor = V(0.9, 0.05, 0.1, 1);

	// TODO: fix this
	game_battlefield *Battle = CONTAINEROF(Field, game_battlefield, Field);

	for (i32 Entity = 0;
	     Entity < Field->NumEntities;
	     ++Entity)
	{
		v4 QuadColor;
		if (Field->UnitStats[Entity].Flags.IsDead)
		{
			QuadColor = V(0.2, 0.2, 0.2, 1.0);
		} else if (Field->Allegiance[Entity] == 0)
		{
			QuadColor = FriendColor;
		} else {
			QuadColor = EnemyColor;
		}

		v2 Middle = Field->DrawnPosition[Entity];

		unit_stats *Stats = &Field->UnitStats[Entity];

		DrawBody(RenderGroup, Middle, QuadColor, Battle->Head, Battle->Body);

		if (EquipmentTable[Stats->Equipment[EquipmentType_WEAPON]].Data.Weapon.Type == WeaponType_BOW)
		{
			// bow
			PushSprite(RenderGroup, Middle, V(0.5, 1.0), Battle->Bow, QuadColor);
		} else {
			PushSprite(RenderGroup, Middle, V(0.35, 0.35), Battle->Fist, QuadColor);
		}


	}

	for (i64 PIdx = Field->PStart;
	     PIdx < Field->PEnd;
	     ++PIdx)
	{
		particle *Particle = Field->Particles + (PIdx % ARRAYCOUNT(Field->Particles));

		// TODO: probably better to remove dead particles in UpdateParticles()
		if (Particle->TTD > 0)
		{
			v2 HalfDim = Scale(0.5, Particle->Dimensions);

			if (ValidID(Particle->Sprite))
			{
				PushSprite(RenderGroup, Diff(Particle->Position, HalfDim),
						 Particle->Dimensions,
						 Particle->Sprite,
						 Particle->Color);
			} else {
				PushRect(RenderGroup, Diff(Particle->Position, HalfDim),
					 Particle->Dimensions, Particle->Color);
			}

		}
	}
}

PRIVATE void
AddParticle(battlefield *Field,
	    v2 Position, v2 Dimensions, v2 Direction,
	    v4 Color, f32 TTD, f32 Speed, sprite_id Sprite)
{
	i64 Current = Field->PEnd++;

	i64 NumParticles = (Field->PEnd - Field->PStart);

	if (NumParticles > (i64) ARRAYCOUNT(Field->Particles))
	{
		FormatOut("[PARTICLE SYSTEM] Too few particles. Deleting one\n");
		Field->PStart += 1;
	}

	particle *New = Field->Particles + (Current % ARRAYCOUNT(Field->Particles));

	New->Position = Position;
	New->Dimensions = Dimensions;
	New->Direction = Direction;
	New->Color = Color;
	New->TTD = TTD;
	New->Speed = Speed;
	New->Sprite = Sprite;
}

void
ResetBattlefield(battlefield *Field)
{
	Field->MiddlePos   = V(0, 0);
	Field->BattleOver  = YARPG_FALSE;;
	Field->NumEntities = 0;
	Field->PStart = 0;
	Field->PEnd   = 0;
}

FWDDECLARE(combat_stat);

struct combat_stat
{
	i32 Attack;
	i32 Defense;
	i32 NumAttacks;

	i32 MinRange, MaxRange;
};

PRIVATE combat_stat PUREFN
CalculateCombatStat(unit_stats *Unit)
{
	combat_stat Result;

	Result.Attack  = 0;
	Result.Defense = 0;

	i32 WeaponId = Unit->Equipment[EquipmentType_WEAPON];

        equipment Weapon = EquipmentTable[WeaponId];

	YASSERT(Weapon.Type == EquipmentType_WEAPON);

	Result.Attack += Weapon.Data.Weapon.Damage;

	i32 DefenseId = Unit->Equipment[EquipmentType_DEFENSE];

        equipment Defense = EquipmentTable[DefenseId];

	YASSERT(Defense.Type == EquipmentType_DEFENSE);

	Result.Defense += Defense.Data.Defense.Defense;

        i32 ArmorId = Unit->Equipment[EquipmentType_ARMOR];

        equipment Armor = EquipmentTable[ArmorId];

	YASSERT(Armor.Type == EquipmentType_ARMOR);

	Result.Defense += Armor.Data.Armor.Defense;

	Result.NumAttacks = Weapon.Data.Weapon.NumAttacks;;

	Result.MinRange = Weapon.Data.Weapon.MinRange;
	Result.MaxRange = Weapon.Data.Weapon.MaxRange;

	return Result;
}

PRIVATE void
DealDamage(unit_stats *Attacker, unit_stats *Defender)
{
	combat_stat AtkStat = CalculateCombatStat(Attacker);
	combat_stat DefStat = CalculateCombatStat(Defender);

	if (AtkStat.NumAttacks <= 0) return;

	while (AtkStat.NumAttacks--)
	{
		i32 AttackerDMG = AtkStat.Attack - DefStat.Defense;

		if (AttackerDMG <= 0) AttackerDMG = 1;

		Defender->CurrentHealth -= AttackerDMG;
	}

	if (Defender->CurrentHealth <= 0) Defender->CurrentHealth = 0;
}

b32
Fight(battlefield *Field, i32 AtkIdx, i32 DefIdx)
{
	if (IsBusy(Field, AtkIdx))
	{
		FormatOut("[BATTLEFIELD] Entity {i32} (Atk) is busy. Ignoring order.\n",
		       AtkIdx);
		return YARPG_FALSE;
	} else if (IsBusy(Field, DefIdx)) {
		FormatOut("[BATTLEFIELD] Entity {i32} (Def) is busy. Ignoring order.\n",
		       DefIdx);
		return YARPG_FALSE;
	} else {
		Field->UnitStats[AtkIdx].Flags.IsAttacking = 1;
		Field->Attack[AtkIdx].DefIdx = DefIdx;
		Field->Attack[AtkIdx].Clock  = 0.0f;

		i32 WeaponId = Field->UnitStats[AtkIdx].Equipment[EquipmentType_WEAPON];
		equipment Weapon = EquipmentTable[WeaponId];
		YASSERT(Weapon.Type == EquipmentType_WEAPON);

		v4 White = V(1, 1, 1, 1);

		// TODO: fix this
		game_battlefield *Battle = CONTAINEROF(Field, game_battlefield, Field);

		sprite_id Arrow = Battle->Arrow;

		if (Weapon.Data.Weapon.Type == WeaponType_BOW)
		{
			v2 Path = Diff(Field->DrawnPosition[DefIdx],
				       Field->DrawnPosition[AtkIdx]);

			f32 Distance = Length(Path);

			f32 Speed = F32C(1.0);

			AddParticle(Field,
				    Field->DrawnPosition[AtkIdx],
				    V(0.5, 0.1),
				    Diff(Field->DrawnPosition[DefIdx],
					 Field->DrawnPosition[AtkIdx]),
				    White,
				    F32C(0.9) / Speed, Speed, Arrow);
		}

		return YARPG_TRUE;
	}
}

PRIVATE void
UpdateParticles(battlefield *Field, f32 Dt)
{
	i64 NumParticles = Field->PEnd - Field->PStart;

	YASSERTF(NumParticles <= (i64) ARRAYCOUNT(Field->Particles),
		 "NumParticles: %ld", NumParticles);

	for (i64 PIdx = Field->PStart;
	     PIdx < Field->PEnd;
	     ++PIdx)
	{
		particle *Particle = Field->Particles + (PIdx % ARRAYCOUNT(Field->Particles));

		Particle->Position  = Add(Particle->Position,
					 Scale(Dt * Particle->Speed, Particle->Direction));
		Particle->TTD      -= Dt;

		if (Particle->TTD < 0
		    && PIdx == Field->PStart)
		{
			Field->PStart += 1;
		}
	}
}

PRIVATE void
UpdateEntities(battlefield *Field, f32 Dt, battlefield_event_queue *Queue)
{
	// TODO: later!
	// ex: play animations, move units, etc..

	for (i64 Entity = 0;
	     Entity < Field->NumEntities;
	     ++Entity)
	{
		unit_stats *Stats = &Field->UnitStats[Entity];

		i32 WeaponId = Stats->Equipment[EquipmentType_WEAPON];
		equipment Weapon = EquipmentTable[WeaponId];
		YASSERT(Weapon.Type == EquipmentType_WEAPON);

		attack_animation *AtkComp = &Field->Attack[Entity];
		if (AtkComp->DefIdx >= 0)
		{
			i32 DefIdx = AtkComp->DefIdx;

			if (AtkComp->Clock > 1.0)
			{
				unit_stats *Defender = &Field->UnitStats[DefIdx];

				DealDamage(Stats, Defender);

				Stats->Flags.IsAttacking = 0;
				Field->Attack[Entity].DefIdx = -1;

				battlefield_atk_finished_event Event;
				Event.AttackerIndex = (i32) Entity;
				Event.DefenderIndex = (i32) DefIdx;

				Field->DrawnPosition[Entity]
					= HexToLinear(ToFloat(Field->BoardPosition[Entity]));

				YASSERT(BFAddAttackFinishedEvent(Queue, Event));
			} else {
				if (Weapon.Data.Weapon.Type != WeaponType_BOW)
				{
					v2 PosMe = HexToLinear(ToFloat(Field->BoardPosition[Entity]));
					v2 PosDef = Field->DrawnPosition[DefIdx];

					f32 T = AtkComp->Clock;

					combat_stat CStat = CalculateCombatStat(Stats);

					i32 NumAttacks = CStat.NumAttacks;

					f32 Percent = Abs(Sin(T * MC_PI * (f32) NumAttacks));

					if ((Percent > Abs(Sin((T - Dt) * MC_PI * (f32) NumAttacks)))
					    && (Percent > Abs(Sin((T + Dt) * MC_PI * (f32) NumAttacks))))
					{
						v2 StartDirection = Diff(PosDef, PosMe);
						for (i64 I = 0;
						     I < 10;
						     ++I)
						{
							f32 T = 2 * ((f32)I / F32C(9.0)) - 1;
							f32 Angle = F32C(0.3) * T;

							f32 CosAngle = Cos(Angle);
							f32 SinAngle = Sin(Angle);

							f32 X = StartDirection.X * CosAngle
								- StartDirection.Y * SinAngle;

							f32 Y = StartDirection.X * SinAngle
								+ StartDirection.Y * CosAngle;

							v2 Direction = V(X, Y);


							AddParticle(Field, Field->DrawnPosition[DefIdx],
								    V(0.15, 0.15), Direction, V(1.0, 0.0, 0.0, 1),
								    F32C(0.5), F32C(0.5),
								    INVALID_SPRITE_ID);
						}
					}

					Percent *= Percent;
					Percent *= Percent;

					v2 NewPos = Lerp(PosMe, F32C(0.5) * Percent, PosDef);

					Field->DrawnPosition[Entity] = NewPos;

				}

				AtkComp->Clock += Dt;
			}
		}

		movement_animation *MovComp = &Field->MovGoal[Entity];

		if (Stats->Flags.IsMoving)
		{
			iv2 CurrentPos = Field->BoardPosition[Entity];
			iv2 GoalPos    = MovComp->Goal;

			v2 Start = HexToLinear(ToFloat(CurrentPos));
			v2 End   = HexToLinear(ToFloat(GoalPos));

			f32 D = Distance(Start, End);

			f32 Speed = 5 * Dt;

			Field->DrawnPosition[Entity] = Lerp(Start, MovComp->T / D, End);

			MovComp->T += Speed;

			if (MovComp->T > D)
			{
				Stats->Flags.IsMoving = 0;

				battlefield_mov_finished_event Event;
				Event.Entity = (i32) Entity;

				YASSERT(BFAddMovementFinishedEvent(Queue, Event));

				Field->BoardPosition[Entity] = GoalPos;
				Field->DrawnPosition[Entity] = End;
			}
		}

		if (Stats->CurrentHealth <= 0 && !Stats->Flags.IsDead)
		{
			for (idx I = 0;
			     I < 50;
			     ++I)
			{
				f32 T = (f32) I / F32C(49.0);
				v2 Direction = UnitVector(T * MC_PI * F32C(2.0));
				AddParticle(Field, Field->DrawnPosition[Entity],
					    V(0.15, 0.15), Direction, V(1, 0.8, 0.0, 1),
					    1.0, 1.0, INVALID_SPRITE_ID);
			}

			Stats->Flags.IsDead = YARPG_TRUE;
		}
	}
}

battlefield_event_queue *
UpdateAndRenderBattlefield(battlefield *Field,
			   render_group *RenderGroup,
			   asset_store *Assets,
			   memory_arena *Scratch,
			   f32 DeltaTime)
{
	battlefield_event_queue *Queue = PushStruct(Scratch, *Queue);
	InitBattlefieldEventQueue(Queue);

	INSTRUMENT(BattlefieldUnR)
	{
		DrawBattlefield(Field, RenderGroup, Assets);
		UpdateEntities(Field, DeltaTime, Queue);
		UpdateParticles(Field, DeltaTime);
	}


	i64 NumAttacker = 0;
	i64 NumDefender = 0;

	for (i32 Entity = 0;
	     Entity < Field->NumEntities;
	     ++Entity)
	{
		if (!Field->UnitStats[Entity].Flags.IsDead)
		{
			if (Field->Allegiance[Entity] == 0)
			{
				NumAttacker += 1;
			} else {
				NumDefender += 1;
			}
		}
	}

	if (NumAttacker == 0 || NumDefender == 0)
	{
		Field->BattleOver = YARPG_TRUE;
	}

	return Queue;
}

void
BFTeleportEntity(battlefield *Field, i32 Idx, iv2 NewPosition)
{

	Field->BoardPosition[Idx]  = NewPosition;
	Field->DrawnPosition[Idx]  = HexToLinear(ToFloat(NewPosition));

	// stop other animations from running
	// TODO: there has to be a better way.
	// a function that orders an attack
	// (and succedes in that) may expect to receive
	// an atk finished / mov finished event.
	// This prevent this.  May lead to bad bugs!
	Field->UnitStats[Idx].Flags.IsMoving    = 0;
	Field->UnitStats[Idx].Flags.IsAttacking = 0;
	Field->Attack[Idx].DefIdx = -1;
}

b32
BFMoveEntity(battlefield *Field, i32 Idx, iv2 NewPosition)
{
	if (IsBusy(Field, Idx))
	{
		FormatOut("[BATTLEFIELD] Entity {i32} is busy. Ignoring order.\n",
		       Idx);
		return YARPG_FALSE;
	} else {
		Field->UnitStats[Idx].Flags.IsMoving = 1;
		Field->MovGoal[Idx].Goal = NewPosition;
		Field->MovGoal[Idx].T    = 0.0f;
		return YARPG_TRUE;
	}
}

f32
TileCost(tile_data Data)
{
	f32 BaseCost = 1.0;
	tile_feature Feature = Data.Feature;

	f32 AddCost = 0.0;

	switch (Feature)
	{
		break;case TileFeature_NONE:
		{
			AddCost = 0.0f;
		}
		break;case TileFeature_FORREST:
		{
			AddCost = 1.0f;
		}
		break;case TileFeature_MARSH:
		{
			AddCost = 1.0f;
		}
		break;case TileFeature_RAINFORREST:
		{
			AddCost = 1.5f;
		}
		break;case TileFeature_HILLS:
		{
			AddCost = 1.0f;
		}
	}

	f32 Result = BaseCost + AddCost;

	return Result;
}
