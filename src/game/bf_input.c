#include <def.h>
#include <util/hex.h>
#include <game/battlefield.h>

static void
HandleInputBattlefield(game_battlefield *Battle, f32 DeltaTime, basis_system Screen,
		       game_input Input)
{
	battlefield *Field = &Battle->Field;
	b32 HasClicked = YARPG_FALSE;

	b32 HasRightClicked = YARPG_FALSE;
	b32 IsRightDown     = YARPG_FALSE;

	for (i32 DeviceIdx = 0;
	     DeviceIdx < Input.NumDevices;
	     ++DeviceIdx)
	{
		if (Input.Devices[DeviceIdx].Method == InputMethod_MOUSE
		    && Input.Devices[DeviceIdx].Data.Mouse)
		{
			mouse_data *Mouse = Input.Devices[DeviceIdx].Data.Mouse;

			key_state LMB = Mouse->Key[KeyId_MOUSE_BUTTON_1];
			key_state RMB = Mouse->Key[KeyId_MOUSE_BUTTON_3];

			if (LMB.IsDown && (LMB.HalfTransitions & 1))
			{
				HasClicked = YARPG_TRUE;
				Input.Devices[DeviceIdx].Data.Mouse = NULL;
			} else if (RMB.IsDown) {
				if (RMB.HalfTransitions != 0)
				{
					HasRightClicked = YARPG_TRUE;
				} else {
					IsRightDown     = YARPG_TRUE;
				}
				Input.Devices[DeviceIdx].Data.Mouse = NULL;
			}
		}
	}

	v3 CursorPos = V((f32)Input.CursorPos.X, (f32)Input.CursorPos.Y, 0.0);
	v3 RelPos3   = BasisTransform(CursorPos, Screen);
	v2 RelPos    = V(RelPos3.X, RelPos3.Y);

	v2 HexPos = LinearToHex(RelPos);
	// TODO: actual rounding!!
	iv2 IHexPos = RoundHexPos(HexPos);

	Battle->MousePos = IHexPos;

	i32 HoveredEntity = -1;

	for (i32 EntityIdx = 0;
	     EntityIdx < Field->NumEntities;
	     ++EntityIdx)
	{
		v2 DrawnPosition = Field->DrawnPosition[EntityIdx];

		if (Between(DrawnPosition.X - F32C(0.5),
			    RelPos.X,
			    DrawnPosition.X + F32C(0.5))
		    && Between(DrawnPosition.Y - F32C(0.5),
			       RelPos.Y,
			       DrawnPosition.Y + F32C(0.5)))
		{
                        HoveredEntity = EntityIdx;
		}
	}

	Battle->HoveredEntity = HoveredEntity;

	if (HasClicked)
	{
		if (Battle->SelectedEntity < 0)
		{
			for (i32 EntityIdx = 0;
			     EntityIdx < Field->NumEntities;
			     ++EntityIdx)
			{
				v2 DrawnPosition = Field->DrawnPosition[EntityIdx];

				if (Between(DrawnPosition.X - F32C(0.5),
					    RelPos.X,
					    DrawnPosition.X + F32C(0.5))
				    && Between(DrawnPosition.Y - F32C(0.5),
					       RelPos.Y,
					       DrawnPosition.Y + F32C(0.5)))
				{
					Battle->SelectedEntity = EntityIdx;
					Battle->CurrentAction = SelectedBattlefieldAction_NOACTION;
                                }
			}
		}
		else
		{
			iv2 OldPos = Field->BoardPosition[Battle->SelectedEntity];

			unit_stats *Stats = &Field->UnitStats[Battle->SelectedEntity];
			combat_stat CStat = CalculateCombatStat(Stats);

			// TODO: check wether this is ok
			id_index *Slot = GetTileSlot(&Field->TileStorage, IHexPos);
			// do nothing if we did not click on a real tile
			if (Slot)
			{
				action Action = Battle->Actions[*Slot];

				if (Battle->CurrentAction == SelectedBattlefieldAction_MOVEMENT)
				{
					if (Action.Type == ActionType_MOVEMENT)
					{
						f32 Rounded = RoundUp(Action.Data.Movement.MovCost);
						i32 Length = (i32) Rounded;
						FormatOut("[CHECK] {f32} -> {f32} -> {i32}\n",
							  Action.Data.Movement.MovCost,
							  Rounded, Length);

						if (Length <= Field->Movement[Battle->SelectedEntity].Range
						    - Field->Movement[Battle->SelectedEntity].Used
						    && ValidHex(Field, IHexPos)
						    && IsEmpty(Field, IHexPos))
						{
							if (BFMoveEntity(Field, Battle->SelectedEntity, IHexPos))
							{
								Field->Movement[Battle->SelectedEntity].Used += Length;
							}
							Battle->CurrentAction = SelectedBattlefieldAction_NOACTION;

						}
					}
					else
					{
						YASSERT(Action.Type == ActionType_NOACTION);
					}
				}
				else if (Battle->CurrentAction == SelectedBattlefieldAction_ATTACK)
				{

					i32 Length = HexDistance(IHexPos, OldPos);

					if (Length <= CStat.MaxRange
					    && Length >= CStat.MinRange
					    && !IsEmpty(Field, IHexPos))
					{
						i32 Entity = GetIndexOfPos(Field, IHexPos);

						YASSERT(Entity >= 0);

						if (Field->Allegiance[Entity] != 0
						    && !Field->UnitStats[Entity].Flags.IsDead)
						{
							if (Fight(Field, Battle->SelectedEntity, Entity))
							{
								Field->HasAttacked[Battle->SelectedEntity] = YARPG_TRUE;
							}
						}

						Battle->CurrentAction = SelectedBattlefieldAction_NOACTION;
					}
				}
			}
		}
	}

	v2 CursorPos2 = V(CursorPos.X, CursorPos.Y);
	if (HasRightClicked)
	{
		Battle->DraggingState = DraggingState_DRAGGING;
		Battle->OldCursorPos  = CursorPos2;
	}
	else if (IsRightDown)
	{
		if (Battle->DraggingState == DraggingState_DRAGGING)
		{
			v2 DP = Diff(CursorPos2, Battle->OldCursorPos);

			v2 RealDP;
			RealDP.X = DP.X * Screen.XAxis.X
				+  DP.Y * Screen.YAxis.X;
			RealDP.Y = DP.X * Screen.XAxis.Y
				+  DP.Y * Screen.YAxis.Y;

			Field->MiddlePos = Diff(Field->MiddlePos, RealDP);
			Battle->OldCursorPos = CursorPos2;
		}
	}
	else
	{
		Battle->DraggingState = DraggingState_NOTACTIVE;
	}
}
