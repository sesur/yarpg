#include <game/unit.h>

static const equipment NoWeapon  = {
	.Type = EquipmentType_WEAPON,
	.Name   = "No Weapon",
	.Weight = 0.0f,
	.Data = { .Weapon = {
			.Type = WeaponType_NOTHING,
			.HitChance = F32C(0.5),
			.Damage = 1, .NumAttacks = 2,
			.MinRange = 1, .MaxRange = 1,
		} },
};
static const equipment NoArmor   = {
	.Type = EquipmentType_ARMOR,
	.Name = "No Armor",
	.Weight = 0.0f,
	.Data = { .Armor = {
			.Type = ArmorType_NOTHING,
			.Defense = 0,
		} },
};
static const equipment NoDefense = {
	.Type = EquipmentType_DEFENSE,
	.Name = "No Defense",
	.Weight = 0.0f,
	.Data = { .Defense = {
			.Type = DefenseType_NOTHING,
			.BlockChance = F32C(0.5),
			.Defense = 0,
		} },
};

static const equipment SimpleBow = {
	.Type = EquipmentType_WEAPON,
	.Name = "Simple Bow",
	.Weight = 0.0f,
	.Data = { .Weapon = {
			.Type = WeaponType_BOW,
			.HitChance = F32C(0.4),
			.Damage = 2,
			.NumAttacks = 1,
			.MinRange = 2, .MaxRange = 2,
		} },
};

enum { EQ_NO_WEAPON, EQ_NO_ARMOR, EQ_NO_DEFENSE, EQ_SIMPLE_BOW };

static const equipment EquipmentTable[] = {
	[EQ_NO_WEAPON]  = NoWeapon,
	[EQ_NO_ARMOR]   = NoArmor,
	[EQ_NO_DEFENSE] = NoDefense,
	[EQ_SIMPLE_BOW] = SimpleBow,
};


static const i32 Weapons[] = { EQ_NO_WEAPON, EQ_SIMPLE_BOW };
static const i32 Defenses[] = { EQ_NO_DEFENSE };
static const i32 Armors[] = { EQ_NO_ARMOR };

unit_stats
RandomUnit(rand64 *R)
{
	unit_stats Stats = {};

	// missing Stats.Flags
	Stats.Weight        = 2 + NextInRange(R, 10);
	Stats.MaxHealth     = 3 + NextInRange(R, 5);
	Stats.CurrentHealth = 1 + NextInRange(R, Stats.MaxHealth);
	Stats.Speed         = 1 + NextInRange(R, 100);

	Stats.Race          = (race_id) 1 + NextInRange(R, NUM_RACES);

	Stats.Equipment[EquipmentType_ARMOR]   = Armors[NextInRange(R, ARRAYCOUNT(Armors))];
	Stats.Equipment[EquipmentType_WEAPON]  = Weapons[NextInRange(R, ARRAYCOUNT(Weapons))];;
	Stats.Equipment[EquipmentType_DEFENSE] = Defenses[NextInRange(R, ARRAYCOUNT(Defenses))];;

	return Stats;
}
