#include <game/world.h>

#include <util/arena_allocator.h>

#include <game/battlefield.h>

#include <util/random.h>
#include <util/memory.h>
#include <util/image.h>
#include <game/thread.h>
#include <compiler.h>

#include <util/layout.h>
#include <input.h>

#include "world_event.c"

FWDDECLARE(point_rectangle_result);

struct point_rectangle_result
{
	v2  Normal;
	f32 Step;
	b32 Inside;
};

PRIVATE i64
ChunkNumber(iv3 Chunk)
{
	i64 Result = I64C(61) * Chunk.X
		+ I64C(149)   * Chunk.Y
		+ I64C(7)     * Chunk.Z;

	return Result;
}

PRIVATE i32
HashOfChunk(iv3 Chunk)
{
	YSASSERT(ISPOWOFTWO(MAX_LOADED_CHUNKS));

	i32 Result = (i32) ChunkNumber(Chunk);
	Result = Result & (MAX_LOADED_CHUNKS - 1);
	// Result should now be between 0 (inc) and MAX_LOADED_CHUNKS (exc)
	return Result;
}

world_chunk *
GetWorldChunk(world *World, iv3 Chunk)
{
	i32 Hash = HashOfChunk(Chunk);
	world_chunk *Current;
	i32 Collisions = 0;
	for (Current = World->LoadedChunks[Hash];
	     Current;
	     Current = Current->Next)
	{
		if (Eq(Current->Chunk, Chunk))
			break;
                Collisions += 1;
	}
	if (Collisions > 0)
	{
		FormatOut("GetWorldChunk: {i32} Hash collisions\n",
			  (i32) Collisions);
	}

	return Current;
}

world_entity * PUREFN
GetEntityByIndex(world *World, stored_world_index Index)
{
	YASSERT((Index.ToI32 >= 0) &&
		((udx)Index.ToI32 < ARRAYCOUNT(World->LoadedEntities)));

	world_entity *Result = NULL;

	Result = &World->LoadedEntities[Index.ToI32];

	return Result;
}

PRIVATE active_world_entity * PUREFN
GetActiveEntityByIndex(world *World, active_world_index Index)
{
	YASSERT(Index.ToI32 < (i32)ARRAYCOUNT(World->ActiveEntities));
	active_world_entity *Result = NULL;
	Result = &World->ActiveEntities[Index.ToI32];
	return Result;
}

PRIVATE void
RegisterEntity(world_chunk *Chunk, stored_world_index EntityIndex)
{
        YASSERT(Chunk->NumEntities < ENTITIES_PER_BLOCK);
        if (Chunk->NumEntities < ENTITIES_PER_BLOCK)
	{
		Chunk->EntityIndices[Chunk->NumEntities++] = EntityIndex;
	} else {
		// TODO: fix this
		INVALIDCODEPATH();
	}
}

void
RemoveEntity(world_chunk *Chunk, stored_world_index EntityIndex)
{
	YASSERT(Chunk->NumEntities < ENTITIES_PER_BLOCK);

	i32 ChunkIndex;
	for (ChunkIndex = 0;
	     ChunkIndex < Chunk->NumEntities;
	     ++ChunkIndex)
	{
		if (EntityIndex.ToI32 == Chunk->EntityIndices[ChunkIndex].ToI32) break;
	}

	YASSERT(ChunkIndex < Chunk->NumEntities);

	Chunk->EntityIndices[ChunkIndex] = Chunk->EntityIndices[--Chunk->NumEntities];
}

PRIVATE stored_world_index
AddEntity(world *World, entity_type Type, world_position Position,
	  f32 Color[3], rect BoundingBox, world_chunk *Chunk)
{
	YASSERT(Chunk);
	YASSERT(Eq(Chunk->Chunk, Position.Chunk));
	stored_world_index FreeEntityIndex = {World->NumLoadedEntities++};
	world_entity *FreeEntity = GetEntityByIndex(World, FreeEntityIndex);
	FreeEntity->Type = Type;
	FreeEntity->Pos  = Position;
	FreeEntity->BoundingBox = BoundingBox;
	FreeEntity->Color[0] = Color[0];
	FreeEntity->Color[1] = Color[1];
	FreeEntity->Color[2] = Color[2];
	RegisterEntity(Chunk, FreeEntityIndex);
	return FreeEntityIndex;
}

PRIVATE stored_world_index
AddEntity_(world *World,
	   entity_type Type,
	   world_position Position,
	   f32 Color[3], rect BoundingBox)
// Type, Position, ... should probably become "Components" later.
{
	world_chunk *Chunk = GetWorldChunk(World, Position.Chunk);
	if (!Chunk)
	{
		Chunk = GenerateWorldChunk(World, Position.Chunk, PushStruct(World->Arena,
									     world_chunk));
	}
	YASSERT(Chunk);
	return AddEntity(World, Type, Position, Color, BoundingBox, Chunk);
}

PRIVATE stored_world_index
AddTile(world *World, world_chunk *Chunk, v2 Offset, f32 Color[3])
{
	// cant use AddEntity here, because this is called during chunk creation
	// and AddEntity creates a chunk if the chunk isnt there
        stored_world_index FreeEntityIndex = {World->NumLoadedEntities++};
	world_entity *FreeEntity = GetEntityByIndex(World, FreeEntityIndex);
	YASSERT(FreeEntity);
	FreeEntity->Type = EntityType_BACKGROUND;
	FreeEntity->Pos.Chunk = Chunk->Chunk;
	FreeEntity->Pos.Offset = Offset;
	FreeEntity->BoundingBox = (rect) {
		.TopRight = V(+0.5, +0.5),
		.BottomLeft = V(-0.5, -0.5)
	};
	FreeEntity->Color[0] = Color[0];
	FreeEntity->Color[1] = Color[1];
	FreeEntity->Color[2] = Color[2];
        RegisterEntity(Chunk, FreeEntityIndex);

	return FreeEntityIndex;
}

stored_world_index
AddArmy(world *World, world_position Pos, f32 Color[3], world_allegiance Allegiance)
{

	u32 Fudge = ReadBits(Pos.Offset.X) ^ ReadBits(Pos.Offset.Y);

	rand64 R = RandomSeries(ChunkNumber(Pos.Chunk) + Fudge);

	u32 NumSoldiers = (NextU32(&R) % 12) + 1;

        rect Box = (rect) {
		.TopRight = V(F32C(+0.05) * (f32) NumSoldiers, F32C(+0.05) * (f32) NumSoldiers),
		.BottomLeft = V(F32C(-0.05) * (f32) NumSoldiers, F32C(-0.05) * (f32) NumSoldiers)
	};

	stored_world_index EntityIndex = AddEntity_(World, EntityType_ARMY, Pos, Color, Box);

	world_entity *Entity = GetEntityByIndex(World, EntityIndex);

	Entity->Data.Army.NumSoldiers = NumSoldiers;
	Entity->Data.Army.Allegiance  = Allegiance;

	for (u32 I = 0;
	     I < NumSoldiers;
	     ++I)
	{
		Entity->Data.Army.Units[I] = RandomUnit(&R);
	}
	YASSERT(Entity);

	v2 NewOffset;
	NewOffset.X = NextBetween(&R, -4.5, 4.5);
	NewOffset.Y = NextBetween(&R, -4.5, 4.5);

	world_position NewPosition;
	NewPosition.Offset = NewOffset;
	NewPosition.Chunk  = Entity->Pos.Chunk;

	SetMoveGoal(World, EntityIndex,
		    1.0, NewPosition);

	return EntityIndex;
}

PRIVATE stored_world_index
AddCity(world *World, world_position Pos, f32 Color[3], world_allegiance Allegiance)
{
        rect Box = (rect) {
		.TopRight = V(+0.4, +0.4),
		.BottomLeft = V(-0.4, -0.4)
	};


	stored_world_index EntityIndex = AddEntity_(World, EntityType_CITY, Pos, Color, Box);

	world_entity *Entity = GetEntityByIndex(World, EntityIndex);

	rand64 R = RandomSeries(ChunkNumber(Pos.Chunk));

	Entity->Data.City.Population = NextU32(&R) % 50;
	Entity->Data.City.Allegiance  = Allegiance;
	YASSERT(Entity);

	return EntityIndex;
}

PRIVATE stored_world_index
AddWall(world *World, world_position Pos)
{
	f32 Color[3] = {1.0, 0.0, 0.0};

	rect Box = (rect) {
		.TopRight = V(+0.5, +0.5),
		.BottomLeft = V(-0.5, -0.5)
	};

	return AddEntity_(World, EntityType_WALL, Pos, Color, Box);
}

PRIVATE v2 PUREFN
FromAbsolute(world_position AbsPos, iv3 ActiveChunk)
{
	iv3 ChunkOffset = Diff(AbsPos.Chunk, ActiveChunk);

	YASSERT(ChunkOffset.Z == 0);

	v2 Result = Add(AbsPos.Offset,
			V((f32) ChunkOffset.X * CHUNK_DIM,
			  (f32) ChunkOffset.Y * CHUNK_DIM));

	return Result;
}

PRIVATE void
UpdateOrCreateActiveEntity(world *World, stored_world_index BaseIndex)
{
	active_world_index ActiveIndex = {World->NumActiveEntities};
	world_entity *Base = GetEntityByIndex(World, BaseIndex);
	YASSERT(Base);

	for (i32 Entity = 0;
	     Entity < World->NumActiveEntities;
	     ++Entity)
	{
		if (World->ActiveEntities[Entity].BaseIndex.ToI32 == BaseIndex.ToI32)
		{
			ActiveIndex = (active_world_index) {Entity};
			break;
		}
	}

	YASSERT(ActiveIndex.ToI32 < MAX_ACTIVE_ENTITIES);

	World->ActiveEntities[ActiveIndex.ToI32].BaseIndex
		= BaseIndex;
	World->ActiveEntities[ActiveIndex.ToI32].RelPosition
		= FromAbsolute(Base->Pos, World->ActiveChunk);
}

PRIVATE void
LoadActiveEntitiesFromChunk(world *World, world_chunk *Chunk)
{
	YASSERT(World->NumActiveEntities < MAX_ACTIVE_ENTITIES);

	iv3 ActiveChunk = World->ActiveChunk;

	for (i32 Entity = 0; Entity < Chunk->NumEntities; ++Entity)
	{
		stored_world_index EntityIndex = Chunk->EntityIndices[Entity];

		FormatOut("[ACTIVE ENTITY] Loading entity {i32} into {i32} {i32} {i32}\n",
			  EntityIndex.ToI32, Chunk->Chunk.X, Chunk->Chunk.Y, Chunk->Chunk.Z);

		active_world_index ActiveIndex = {World->NumActiveEntities++};

		YASSERT(ActiveIndex.ToI32 < MAX_ACTIVE_ENTITIES);

		active_world_entity *ActiveEntity = GetActiveEntityByIndex(World, ActiveIndex);
		ActiveEntity->BaseIndex = EntityIndex;
		world_entity *Base = GetEntityByIndex(World, EntityIndex);

		YASSERT(Base);

		ActiveEntity->RelPosition = FromAbsolute(Base->Pos, ActiveChunk);
	}
}

PRIVATE void
SetActive(world *World, iv3 Chunk)
{
	// load all entities in Chunk into the active entity list
	World->ActiveChunk = Chunk;

	// unload all loaded entities
	World->NumActiveEntities = 0;

	for (i32 YOffset = -1;
	     YOffset <= 1;
	     ++YOffset)
	{
		for (i32 XOffset = -1;
		     XOffset <= 1;
		     ++XOffset)
		{
			iv3 ChunkToLoad = IV(Chunk.X + XOffset,
					     Chunk.Y + YOffset,
					     Chunk.Z);
			world_chunk *Current = GetWorldChunk(World, ChunkToLoad);
			if (!Current)
			{
				world_chunk *NewChunk = PushStruct(World->Arena, world_chunk);
				GenerateWorldChunk(World, ChunkToLoad, NewChunk);
				Current = GetWorldChunk(World, ChunkToLoad);
			}
			YASSERT(Current);

			LoadActiveEntitiesFromChunk(World, Current);

		}
	}
}

PRIVATE void
RandomiseTiles(world *World, world_chunk *Chunk)
{
	v2 Offset;
	f32 Color[3];
	f32 BWColor[2][3] = {{0.0f, 0.0f, 0.0f},
			     {1.0f, 1.0f, 1.0f}};
	v2 Middle = V((CHUNK_DIM/2.0f), (CHUNK_DIM/2.0f));

	rand64 R = RandomSeries(ChunkNumber(Chunk->Chunk));

	if ((Chunk->Chunk.X + Chunk->Chunk.Y) % 2)
	{
		for (i32 Y = 0;
		     Y < CHUNK_DIM;
		     ++Y)
		{
			for (i32 X = 0;
			     X < CHUNK_DIM;
			     ++X)
			{
				Offset = Diff(V((f32)X, (f32)Y), Middle);
				Color[0] = NextF32(&R);
				Color[1] = NextF32(&R);
				Color[2] = NextF32(&R);
				i32 Index = (Chunk->Chunk.X + Chunk->Chunk.Y + X + Y) % 2;
				AddTile(World, Chunk, Offset, BWColor[(Index + 2) % 2]);
			}
		}
	} else {
		for (i32 Y = 0;
		     Y < CHUNK_DIM;
		     ++Y)
		{
			for (i32 X = 0;
			     X < CHUNK_DIM;
			     ++X)
			{
				Offset = Diff(V((f32)X, (f32)Y), Middle);
				Color[0] = (f32)rand()/(f32)(RAND_MAX);
				Color[1] = (f32)rand()/(f32)(RAND_MAX);
				Color[2] = (f32)rand()/(f32)(RAND_MAX);
//			i32 Index = (Chunk->Chunk.X + Chunk->Chunk.Y + X + Y) % 2;
				AddTile(World, Chunk, Offset, Color);
			}
		}
	}

}

PRIVATE void
CreateBackgroundImage(transient_world_state *Transient, u32 Slot, iv3 Chunk)
{
	image Image;
	Image.Width  = BACKGROUND_CHUNK_SIZE;
	Image.Height = BACKGROUND_CHUNK_SIZE;
	Image.Data   = Transient->Cache[Slot].Data;

	ZeroMemory(BACKGROUND_CHUNK_SIZE * BACKGROUND_CHUNK_SIZE
		   * sizeof(*Image.Data),
		   (byte *) Image.Data);

	i32 SafetyMargin = 2;

	b32 Ready = YARPG_TRUE;

	image Stamps[ARRAYCOUNT(Transient->Stamps)];
	for (udx I = 0;
	     I < ARRAYCOUNT(Stamps);
	     ++I)
	{
		if (!TryLoadSprite(&Transient->State->Assets,
				   Transient->Stamps[I],
				   &Stamps[I]))
		{
			Ready = YARPG_FALSE;
		}
	}
	if (Ready)
	{
		for (i32 YOffset = -SafetyMargin;
		     YOffset <= SafetyMargin;
		     ++YOffset)
		{
			for (i32 XOffset = -SafetyMargin;
			     XOffset <= SafetyMargin;
			     ++XOffset)
			{
				iv3 CurrentChunk = IV(Chunk.X + XOffset,
						      Chunk.Y + YOffset,
						      Chunk.Z);
				rand64 S = RandomSeries(ChunkNumber(CurrentChunk));

				image Sources[3];
				if (1 || NextInRange(&S, 5))
				{
					Sources[0] = Stamps[0];
					Sources[1] = Stamps[1];
					Sources[2] = Stamps[2];
				} else {
					Sources[0] = Stamps[5];
					Sources[1] = Stamps[5];
					Sources[2] = Stamps[4];
				}


				for (i32 I = 0;
				     I < 30;
				     ++I)
				{
					image ToDraw = Sources[NextInRange(&S, ARRAYCOUNT(Sources))];

					u32 Width = ToDraw.Width;
					u32 Height = ToDraw.Height;

					i32 DX = (i32) NextInRange(&S, Image.Width) - Width / 2;
					i32 DY = (i32) NextInRange(&S, Image.Height) - Height / 2;

					DX += XOffset * BACKGROUND_CHUNK_SIZE;
					DY += YOffset * BACKGROUND_CHUNK_SIZE;

					irect Bounds;
					Bounds.MinX = DX;
					Bounds.MaxX = DX + Width;
					Bounds.MinY = DY;
					Bounds.MaxY = DY + Height;

					BlitPremultiplied(Image, ToDraw, Bounds);
				}
			}
		}

		COMPILERBARRIER();

		// we expect it to be queued when working on it
		// do not mess with it if its not true
		InterlockedCompareSwap(&Transient->Used[Slot],
				       ImageCacheStatus_QUEUED,
				       ImageCacheStatus_LOADED);
	} else {
		COMPILERBARRIER();
		InterlockedCompareSwap(&Transient->Used[Slot],
				       ImageCacheStatus_QUEUED,
				       ImageCacheStatus_UNUSED);
	}

}

world_chunk *
GenerateWorldChunk(world *World, iv3 Chunk, world_chunk *NewChunk)
{
        FormatOut("[WORLD] Generating Chunk: ({i32}, {i32}, {i32})\n",
		  Chunk.X, Chunk.Y, Chunk.Z);

	i32 Hash = HashOfChunk(Chunk);

	NewChunk->Chunk = Chunk;
	NewChunk->NumEntities = 0;
	//RandomiseTiles(World, NewChunk);
	NewChunk->Next = World->LoadedChunks[Hash];
	World->LoadedChunks[Hash] = NewChunk;

	rand64 R = RandomSeries(ChunkNumber(Chunk));

	world_position Pos;
	Pos.Chunk = Chunk;
	Pos.Offset = V(NextBetween(&R, -4.5, 4.5),
		       NextBetween(&R, -4.5, 4.5));

	f32 Color[3] = {F32C(0.15), F32C(0.25), F32C(0.4)};

	f32 ArmyColor[3] = {F32C(1.0), F32C(0.2), F32C(0.3)};

	AddCity(World, Pos, Color, WorldAllegiance_NEUTRAL);

	world_allegiance PossibleAllegiances[] =
		{
			WorldAllegiance_NEUTRAL,
			WorldAllegiance_ENEMY,
			WorldAllegiance_FRIEND,
		};

	for (i32 I = 0;
	     I < ARMIES_PER_NEW_CHUNK;
	     ++I)
	{

		f32 XOff, YOff;

		XOff = NextBetween(&R, -CHUNK_DIM/2.0f, CHUNK_DIM/2.0f);
		YOff = NextBetween(&R, -CHUNK_DIM/2.0f, CHUNK_DIM/2.0f);


                Pos.Offset = V(XOff, YOff);

		i32 AllIndex = NextInRange(&R, ARRAYCOUNT(PossibleAllegiances));

		AddArmy(World, Pos, ArmyColor, PossibleAllegiances[AllIndex]);
	}



	return NewChunk;
}

void
InitWorld(world *World,
	  memory_arena *Arena)
{
	World->NumLoadedEntities = 0;
	World->FreeChunks = NULL;
	World->Arena = Arena;
	World->ShowPaths = YARPG_FALSE;
	World->ShouldReload = YARPG_FALSE;

	glGenTextures(1, &World->BackgroundTexture);

	FormatOut("[BACKGROUND CHUNK SIZE] {i32}\n", BACKGROUND_CHUNK_SIZE);

	glBindTexture(GL_TEXTURE_2D, World->BackgroundTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
		     BACKGROUND_CHUNK_SIZE * BACKGROUND_IMAGE_CACHE_SIZE,
		     BACKGROUND_CHUNK_SIZE,
		     0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	iv3 StartChunk = IV(0, 0, 0);

	World->CameraPos = (world_position) {StartChunk, V(0.0, 0.0)};
	SetActive(World, StartChunk);
}

void
DestroyWorld(world *World)
{
	glDeleteTextures(1, &World->BackgroundTexture);
}

PRIVATE TASK_CALLBACK_FN(CreateChunkBackgroundTask)
{
	create_chunk_background_task_input *Args =
		*(create_chunk_background_task_input * volatile *) Arg;

	CreateBackgroundImage(Args->State, Args->Slot, Args->Chunk);
}

PRIVATE void
DrawChunkBackground(world *World, iv3 ActiveChunk, iv3 Chunk,
		    render_group *RenderGroup)
{
	transient_world_state *State = World->Transient;
	u32 MySlot   = BACKGROUND_IMAGE_CACHE_SIZE;
	image_cache_status MyStatus;
        for (u32 Slot = 0;
	     Slot < BACKGROUND_IMAGE_CACHE_SIZE;
	     ++Slot)
	{
		image_cache_status Status = InterlockedRead(&State->Used[Slot]);
		if ((Status != ImageCacheStatus_UNUSED)
		    && (Eq(State->Chunk[Slot], Chunk)))
		{
			MySlot = Slot;
			MyStatus = Status;
			break;
		}
	}

	if (MySlot == BACKGROUND_IMAGE_CACHE_SIZE)
	{
		u32 FreeSlot = BACKGROUND_IMAGE_CACHE_SIZE;
		for (u32 Slot = 0;
		     Slot < BACKGROUND_IMAGE_CACHE_SIZE;
		     ++Slot)
		{
			if (InterlockedCompareSwap(&State->Used[Slot],
						       ImageCacheStatus_UNUSED,
						       ImageCacheStatus_QUEUED))
			{
				FreeSlot = Slot;
				break;
			}
		}

		if (FreeSlot != BACKGROUND_IMAGE_CACHE_SIZE)
		{
			State->Chunk[FreeSlot] = Chunk;
			// CreateBackgroundImage(State, FreeSlot, Chunk);
			State->Input[FreeSlot].State = World->Transient;
			State->Input[FreeSlot].Slot  = FreeSlot;
			State->Input[FreeSlot].Chunk = Chunk;

			for (udx I = 0;
			     I < ARRAYCOUNT(World->Transient->Stamps);
			     ++I)
			{
				FetchSprite(&World->Transient->State->Assets,
					     World->Transient->Stamps[I]);
			}

			PushLowPriorityTask(CreateChunkBackgroundTask,
					    &State->Input[FreeSlot]);

			MySlot = FreeSlot;
			MyStatus = ImageCacheStatus_QUEUED;

			FormatOut("[QUEUED] Chunk {i32} {i32} {i32} -> Slot {u32}\n",
			       Chunk.X, Chunk.Y, Chunk.Z, FreeSlot);
		}

	}

	if (MySlot != BACKGROUND_IMAGE_CACHE_SIZE)
	{
		if (MyStatus == ImageCacheStatus_QUEUED)
		{
			return;
		} else if (MyStatus == ImageCacheStatus_LOADED) {
			glBindTexture(GL_TEXTURE_2D, World->BackgroundTexture);
			glTexSubImage2D(GL_TEXTURE_2D,
					0, BACKGROUND_CHUNK_SIZE * MySlot, 0,
					BACKGROUND_CHUNK_SIZE,
					BACKGROUND_CHUNK_SIZE,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					State->Cache[MySlot].Data);
			State->Used[MySlot] = ImageCacheStatus_TEXTURED;

			FormatOut("[WORLD] Loaded Chunk {i32} {i32} {i32} -> Slot {u32}\n",
			       Chunk.X, Chunk.Y, Chunk.Z, MySlot);
		}

		COMPILERBARRIER();

		YASSERT(InterlockedRead(&State->Used[MySlot]) == ImageCacheStatus_TEXTURED);

		f32 TexMinY = 0.0;
		f32 TexMaxY = 1.0;
		f32 TexMinX = (f32) MySlot / BACKGROUND_IMAGE_CACHE_SIZE;
		f32 TexMaxX = (f32) (MySlot+1) / BACKGROUND_IMAGE_CACHE_SIZE;

		f32 Width  = CHUNK_DIM;
		f32 Height = CHUNK_DIM;

		Chunk = Diff(Chunk, ActiveChunk);

		v3 Middle = Scale(CHUNK_DIM, ToFloat(Chunk));

		v4 Color = V(1, 1, 1 , 1);

		texture Background;
		Background.ID = World->BackgroundTexture;
		Background.Origin = V(TexMinX, TexMinY);
		Background.Dimensions = V(TexMaxX - TexMinX, TexMaxY - TexMinY);
		Background.IsMono = 0;

		PushTexture(RenderGroup,
			    V(Middle.X - Width / 2, Middle.Y - Height / 2),
			    V(Width, Height),
			    Background,
			    Color);

#if 1
		rect Rect;
		Rect.MinX = Middle.X - Width / 2;
		Rect.MinY = Middle.Y - Height / 2;
		Rect.MaxX = Middle.X + Width / 2;
		Rect.MaxY = Middle.Y + Height / 2;

		PushRectOutline(RenderGroup, Rect, F32C(0.005), Color);
#endif

		GL_QUICK_CHECK();
	}
}

stored_world_index
GetEntityIndex(world *World, world_entity *Entity)
{
	i32 Index;

	Index = (i32) ((i64) Entity - (i64) &World->LoadedEntities) / sizeof(*Entity);

	return (stored_world_index) {Index};
}

PRIVATE v4 PUREFN
CityColor(world_entity *City)
{
	YASSERT(City->Type == EntityType_CITY);
	v4 Color;
	switch(City->Data.City.Allegiance)
	{
		break;case WorldAllegiance_OWNED:
		Color = V(0, .75, 0, 1);
		break;case WorldAllegiance_FRIEND:
		Color = V(0, 0, .75, 1);
		break;case WorldAllegiance_NEUTRAL:
		Color = V(0.3, 0.3, 0.3, 1);
		break;case WorldAllegiance_ENEMY:
		Color = V(.75, 0, 0, 1);
	}
	return Color;
}

PRIVATE v4 PUREFN
ArmyColor(world_entity *Army)
{
	YASSERT(Army->Type == EntityType_ARMY);
	v4 Color;
	switch(Army->Data.Army.Allegiance)
	{
		break;case WorldAllegiance_OWNED:
		Color = V(0, 1, 0, 1);
		break;case WorldAllegiance_FRIEND:
		Color = V(0, 0, 1, 1);
		break;case WorldAllegiance_NEUTRAL:
		Color = V(0.5, 0.5, 0.5, 1);
		break;case WorldAllegiance_ENEMY:
		Color = V(1, 0, 0, 1);
	}
	return Color;
}

PRIVATE void
InvalidateBackgroundCache(world *World, iv3 ActiveChunk)
{
	transient_world_state *State = World->Transient;
	YASSERT(State->Test == 0xDEADCAFE);

	for (u32 Slot = 0;
	     Slot < BACKGROUND_IMAGE_CACHE_SIZE;
	     ++Slot)
	{
		iv3 LoadedChunk = State->Chunk[Slot];

		iv3 Difference = Diff(LoadedChunk, ActiveChunk);

		b32 CloseX = -1 <= Difference.X && Difference.X <= 1;
		b32 CloseY = -1 <= Difference.Y && Difference.Y <= 1;
		b32 CloseZ = -1 <= Difference.Z && Difference.Z <= 1;

		if (!CloseX || !CloseY || !CloseZ)
		{
			/* dont mess with stuff thats getting worked on in a different thread */
			// maybe we should though?
			if (InterlockedRead(&State->Used[Slot]) == ImageCacheStatus_QUEUED)
			{
				FormatOut("[WORLD] Skipped freeing slot {u32}.\n", Slot);
			} else {
				InterlockedWrite(&State->Used[Slot], ImageCacheStatus_UNUSED) ;
			}

		}
	}
}

PRIVATE void
DrawWorld(world *World, render_group *RenderGroup)
{
	transient_world_state *TranState = World->Transient;

	v3 Transform = V(0, 0, 1);

	iv3 ActiveChunk = World->ActiveChunk;

	InvalidateBackgroundCache(World, ActiveChunk);
	i32 VisibleMargin = 1;
	for (i32 YOffset = -VisibleMargin;
	     YOffset <= VisibleMargin;
	     ++YOffset)
	{
		for (i32 XOffset = -VisibleMargin;
		     XOffset <= VisibleMargin;
		     ++XOffset)
		{
			iv3 Chunk = IV(ActiveChunk.X + XOffset,
				       ActiveChunk.Y + YOffset,
				       ActiveChunk.Z);
			DrawChunkBackground(World, ActiveChunk, Chunk,
					    RenderGroup);
		}
	}

	sprite_id ArmySprite = FirstSpriteOf(&TranState->State->Assets, Category_ARMY);
	sprite_id CitySprite = FirstSpriteOf(&TranState->State->Assets, Category_CITY);

	for (i32 ActiveIndex = 0;
	     ActiveIndex < World->NumActiveEntities;
	     ++ActiveIndex)
	{
		active_world_entity *Entity = &World->ActiveEntities[ActiveIndex];
		world_entity *Base = GetEntityByIndex(World, Entity->BaseIndex);

		f32 Depth = F32C(0.0);

		switch (Base->Type)
		{
			break;case EntityType_ERROR:      Depth = F32C(0.0);
			break;case EntityType_CITY:       Depth = F32C(0.5);
			break;case EntityType_ARMY:       Depth = F32C(0.6);
			break;case EntityType_WALL:       Depth = F32C(0.2);
			break;case EntityType_BACKGROUND: Depth = F32C(0.1);
		}

		v2 RelPos = Entity->RelPosition;
		rect Box = Base->BoundingBox;
		v3 MiddlePos   = V(RelPos.X, RelPos.Y, 0);
		v3 TopRight     = V(Box.TopRight.X, Box.TopRight.Y, Depth);
		v3 BottomLeft = V(Box.BottomLeft.X, Box.BottomLeft.Y, Depth);
		v3 TopLeft    = V(Box.BottomLeft.X, Box.TopRight.Y, Depth);
		v3 BottomRight  = V(Box.TopRight.X, Box.BottomLeft.Y, Depth);

		f32 Width = Box.MaxX - Box.MinX;
		f32 Height = Box.MaxY - Box.MinY;

		f32 Thickness = F32C(0.005) * Min(Width, Height);

		v4 Color;

		v4 Black = V(0.0, 0.0, 0.0, 1.0);

		switch(Base->Type)
		{
		case EntityType_ERROR:
		case EntityType_WALL:
		case EntityType_BACKGROUND:
		{
			Color = V(Base->Color[0], Base->Color[1],
				  Base->Color[2], 1);

			PushRect(RenderGroup,
				 V(MiddlePos.X + BottomLeft.X, MiddlePos.Y + BottomLeft.Y),
				 V(TopRight.X - BottomLeft.X, TopRight.Y - BottomLeft.Y),
				 Color);
		}

			break;case EntityType_CITY:
			{
				Color = CityColor(Base);
				PushSprite(RenderGroup,
					   V(MiddlePos.X + BottomLeft.X, MiddlePos.Y + BottomLeft.Y),
					   V(TopRight.X - BottomLeft.X, TopRight.Y - BottomLeft.Y),
					   CitySprite,
					   Color);
			}

			break;case EntityType_ARMY:
			{
				rect R;
				R.BottomLeft = Add(V(BottomLeft.X, BottomLeft.Y),
						   RelPos);
				R.TopRight   = Add(V(TopRight.X, TopRight.Y),
						   RelPos);

				Color = ArmyColor(Base);
				PushRectOutline(RenderGroup,
                                                R,
						Thickness,
						Black);
				PushSprite(RenderGroup,
					   V(MiddlePos.X + BottomLeft.X, MiddlePos.Y + BottomLeft.Y),
					   V(TopRight.X - BottomLeft.X, TopRight.Y - BottomLeft.Y),
					   ArmySprite,
					   Color);
			}
			INVALIDDEFAULT();
		}



		if (World->ShowPaths && Base->ShouldMove)
		{
			v2 RelGoal = FromAbsolute(Base->MovementGoal, World->ActiveChunk);
			f32 Thickness = F32C(0.05);
			PushLine(RenderGroup,
				 V(MiddlePos.X, MiddlePos.Y),
				 RelGoal,
				 Thickness,
				 Color);
		}
	}
}

void
CanonizePosition(world_position *Position)
{
	f32 Threshold = CHUNK_DIM / 2.0f;
	if (Position->Offset.Y < -Threshold)
	{
		do
		{
			Position->Offset.Y += CHUNK_DIM;
			Position->Chunk.Y  -= 1;
		} while (Position->Offset.Y < -Threshold);
	} else if (Position->Offset.Y > Threshold)
	{
		do
		{
			Position->Offset.Y -= CHUNK_DIM;
			Position->Chunk.Y  += 1;
		} while (Position->Offset.Y > Threshold);
	}
	if (Position->Offset.X < -Threshold)
	{
		do
		{
			Position->Offset.X += CHUNK_DIM;
			Position->Chunk.X  -= 1;
		} while (Position->Offset.X < -Threshold);
	} else if (Position->Offset.X > Threshold)
	{
		do
		{
			Position->Offset.X -= CHUNK_DIM;
			Position->Chunk.X  += 1;
		} while (Position->Offset.X > Threshold);
	}
}

void
SetCameraPos(world *World, world_position CameraPos)
{
	CanonizePosition(&CameraPos);

	if (!Eq(World->CameraPos.Chunk, CameraPos.Chunk))
	{
		FormatOut("[WORLD] Active chunk changed!\n");
		SetActive(World, CameraPos.Chunk);
	}

	InvalidateBackgroundCache(World, CameraPos.Chunk);

	World->CameraPos = CameraPos;
}

void
Reload(world *World)
{
        SetActive(World, World->CameraPos.Chunk);
}

PRIVATE void
MoveEntity(world *World, active_world_entity *Entity, world_entity *Base, v2 Dv)
{
	Entity->RelPosition = Add(Entity->RelPosition,  Dv);
	Base->Pos.Offset = Add(Base->Pos.Offset,  Dv);
	iv3 StartChunk = Base->Pos.Chunk;
	CanonizePosition(&Base->Pos);
	iv3 EndChunk   = Base->Pos.Chunk;

	if (!Eq(StartChunk, EndChunk))
	{
		world_chunk *Start = GetWorldChunk(World, StartChunk);
		world_chunk *End   = GetWorldChunk(World, EndChunk);
		YASSERT(Start);
		YASSERT(End);
                RemoveEntity(Start, Entity->BaseIndex);
		RegisterEntity(End, Entity->BaseIndex);
	}
}

PRIVATE b32
RayLineIntersect(f32 LineX, v2 Point, v2 Ray, f32 MinY, f32 MaxY, f32 *CollisionAt)
{
	f32 XCollision = (LineX - Point.X) / Ray.X;
	b32 Result = (XCollision >= 0.0f)
		&& (XCollision < *CollisionAt)
		&& Between(MinY,
			   Point.Y + Ray.Y * XCollision,
			   MaxY);
	if (Result)
	{
		*CollisionAt = XCollision;
	}
	return Result;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

PRIVATE point_rectangle_result
PointRectangleCollision(v2 Point, v2 Direction, rect Box,
			f32 CollisionAt)
{
	point_rectangle_result Result = { .Inside = YARPG_FALSE };

	if (Direction.X != 0.0 && Direction.X != -0.0)
	{

		if (RayLineIntersect(Box.MinX, Point, Direction, Box.MinY, Box.MaxY, &CollisionAt))
		{
			Result.Inside = YARPG_TRUE;
			Result.Normal = V(-1, 0);
			Result.Step   = CollisionAt;
		}

		if (RayLineIntersect(Box.MaxX, Point, Direction, Box.MinY, Box.MaxY, &CollisionAt))
		{
			Result.Inside = YARPG_TRUE;
			Result.Normal = V(1, 0);
			Result.Step   = CollisionAt;
		}
	}
	if (Direction.Y != 0.0 && Direction.Y != -0.0)
	{
		Point = V(Point.Y, Point.X);
		Direction = V(Direction.Y, Direction.X);
		if (RayLineIntersect(Box.MinY, Point, Direction, Box.MinX, Box.MaxX, &CollisionAt))
		{
			Result.Inside = YARPG_TRUE;
			Result.Normal = V(0, -1);
			Result.Step   = CollisionAt;
		}

		if (RayLineIntersect(Box.MaxY, Point, Direction, Box.MinX, Box.MaxX, &CollisionAt))
		{
			Result.Inside = YARPG_TRUE;
			Result.Normal = V(0, 1);
			Result.Step   = CollisionAt;
		}

	}

	if (!Result.Inside && PointInsideRect(Point, Box))
	{
		Result.Inside = YARPG_TRUE;
		Result.Normal = V(0, 0);
		Result.Step   = 0.0f;
	}

	return Result;
}

#pragma GCC diagnostic pop

PRIVATE void
ReinforceArmy(world *World, stored_world_index Reinforced, stored_world_index Reinforcement)
{
	world_entity *RfdBase = GetEntityByIndex(World, Reinforced);
	world_entity *RfmBase = GetEntityByIndex(World, Reinforcement);

	YASSERT(RfdBase);
	YASSERT(RfmBase);

	YASSERT(RfdBase->Type == EntityType_ARMY);
	YASSERT(RfmBase->Type == EntityType_ARMY);

	i32 RfdSoldiers = RfdBase->Data.Army.NumSoldiers;
	i32 RfmSoldiers = RfmBase->Data.Army.NumSoldiers;
	i32 NumSoldiers = RfdSoldiers + RfmSoldiers;


	i32 RestSoldiers = NumSoldiers - MAXARMYSOLDIERS;
	if (NumSoldiers <= MAXARMYSOLDIERS)
	{
		RfmBase->Data.Army.NumSoldiers = 0;
		RestSoldiers = 0;
		RemoveEntity(GetWorldChunk(World, RfmBase->Pos.Chunk),
			     Reinforcement);
	} else {
		RfmBase->Data.Army.NumSoldiers = RestSoldiers;
		NumSoldiers = MAXARMYSOLDIERS;
		RfmBase->BoundingBox.BottomLeft = V(F32C(-0.05) * (f32) RestSoldiers,
						    F32C(-0.05) * (f32) RestSoldiers);
		RfmBase->BoundingBox.TopRight = V(F32C(+0.05) * (f32) RestSoldiers,
						  F32C(+0.05) * (f32) RestSoldiers);
	}

	for (i32 AddUnits = 0;
	     AddUnits < NumSoldiers - RfdSoldiers;
	     ++AddUnits)
	{
		RfdBase->Data.Army.Units[AddUnits + RfdSoldiers] =
			RfmBase->Data.Army.Units[AddUnits + RestSoldiers];
	}

	RfdBase->Data.Army.NumSoldiers = NumSoldiers;
	RfdBase->BoundingBox.BottomLeft = V(F32C(-0.05) * (f32) NumSoldiers,
					    F32C(-0.05) * (f32) NumSoldiers);
	RfdBase->BoundingBox.TopRight = V(F32C(+0.05) * (f32) NumSoldiers,
					  F32C(+0.05) * (f32) NumSoldiers);

	FormatOut("[WORLD] Reinforcement {i32} (Num: {i32} -> {i32})->> {i32} (Num: {i32} -> {i32})\n",
	       Reinforcement.ToI32, RfmSoldiers, RfmBase->Data.Army.NumSoldiers,
	       Reinforced.ToI32, RfdSoldiers, RfdBase->Data.Army.NumSoldiers);
	World->ShouldReload = YARPG_TRUE;
}

PRIVATE b32
HandleCollision(world *World,
		active_world_entity *Entity,
		world_entity *Base,
		active_world_entity *OtherEntity,
		world_entity *OtherBase,
		world_event_queue *Queue)
{
	stored_world_index EntityIndex = Entity->BaseIndex;
	if (((Base->Type == EntityType_WALL) && (OtherBase->Type == EntityType_ARMY)) ||
	    ((Base->Type == EntityType_ARMY) && (OtherBase->Type == EntityType_WALL)))
	{
		FormatOut("{i32} collided with {i32}\n",
		       Entity->BaseIndex.ToI32, OtherEntity->BaseIndex.ToI32);
		return 1;
	} else if ((Base->Type == EntityType_ARMY) && (OtherBase->Type == EntityType_ARMY)) {
		// StartBattle(World, Entity->BaseIndex, OtherEntity->BaseIndex);

		world_collision_event Event;
		Event.AttackerIndex = Entity->BaseIndex;
		Event.DefenderIndex = OtherEntity->BaseIndex;

		AddCollisionEvent(Queue, Event);


		return 1;
	}
	return 0;
}

PRIVATE b32
ShouldCollide(active_world_entity *Entity,
	      world_entity *Base,
	      active_world_entity *OtherEntity,
	      world_entity *OtherBase)
{
	return OtherEntity != Entity
		&& Base->Type    != EntityType_BACKGROUND
		&& OtherBase->Type != EntityType_BACKGROUND;
}

PRIVATE void
UpdateEntity(world *World, active_world_entity *Entity, f32 Dt,
	     world_event_queue *Queue)
{
	stored_world_index EntityIndex = Entity->BaseIndex;
	world_entity *BaseEntity = GetEntityByIndex(World, EntityIndex);
	YASSERT(BaseEntity);

	if (BaseEntity->ShouldMove)
	{
		YASSERT(BaseEntity->Speed > 0);

		v2 RelTarget = FromAbsolute(BaseEntity->MovementGoal, World->ActiveChunk);

		v2 Diff = Diff(RelTarget, Entity->RelPosition);

		f32 DiffLength = Length(Diff);

		f32 Factor = 1.0;

		if (DiffLength > BaseEntity->Speed)
		{
			Factor = BaseEntity->Speed / DiffLength;
		}

		v2 Dv = Scale(Factor, Diff);

		v2 ProjectedEnd = Add(Entity->RelPosition, Scale(Dt, Dv));

		if (Distance(ProjectedEnd, RelTarget) < 0.0001)
		{
			Dv = Scale((1 / Dt), Diff);

			BaseEntity->ShouldMove = YARPG_FALSE;
			AddMovementFinishedEvent(Queue, (world_mov_finished_event) {EntityIndex});
		}

		BaseEntity->Movement.Direction = V(0, 0);
		BaseEntity->Movement.Accel     = 0;
		BaseEntity->Movement.Dv        = Dv;
	} else {
		BaseEntity->Movement.Dv        = V(0, 0);
	}

	if (BaseEntity->Type == EntityType_ARMY)
	{
		YASSERT(EntityIndex.ToI32 == Entity->BaseIndex.ToI32);
		movement *Movement = &BaseEntity->Movement;

		rect EntityBox = BaseEntity->BoundingBox;

		// TODO: this could technically move the entity further than possible
		// better to normalise the direction and have
		// MovementRemaining equal its length
		// f32 MovementRemaining = Length(Movement->Dv);
		// v2 Direction = 1/MovementRemaining * Movement->Dv;
		f32 MovementRemaining = 1.0f;
		for (i32 Iteration = 0;
		     Iteration < 4 && MovementRemaining > 0.0f;
		     ++Iteration)
		{
			v2 Offset   = Scale(MovementRemaining * Dt, Movement->Dv);
			v2 WallNormal = V(0, 0);
			f32 CollisionAt = 1.0f;
                        active_world_entity *HitEntity = 0;
			world_entity        *HitBase   = 0;
			for (i32 i = 0;
			     i < World->NumActiveEntities;
			     ++i)
			{
				active_world_entity *OtherEntity = World->ActiveEntities + i;
				world_entity *OtherBase = GetEntityByIndex(World, OtherEntity->BaseIndex);
				YASSERT(OtherBase);

				if (ShouldCollide(Entity, BaseEntity, OtherEntity, OtherBase))
				{
					v2 CurrentCenter = Entity->RelPosition;
					v2 OtherCenter   = OtherEntity->RelPosition;
					v2 RelCenter     = Diff(CurrentCenter, OtherCenter);
					rect OtherBox    = OtherBase->BoundingBox;

					// minkowski sum
					rect BoundingBox;
					BoundingBox.BottomLeft = Add(OtherBox.BottomLeft,
								     EntityBox.BottomLeft);
					BoundingBox.TopRight = Add(OtherBox.TopRight,
								   EntityBox.TopRight);


					point_rectangle_result Res = PointRectangleCollision(RelCenter,
											     Offset,
											     BoundingBox,
											     CollisionAt);
					if (Res.Inside)
					{
						WallNormal = Res.Normal;
						CollisionAt = Res.Step;
						HitEntity = OtherEntity;
						HitBase = OtherBase;
					}
				}
			}

			v2 Velocity = Movement->Dv;
			Velocity = Add(Velocity,
				       Scale(MovementRemaining * Dt * Movement->Accel,
					     Movement->Direction));

			if (HitEntity)
			{
				b32 CollisionStop = HandleCollision(World,
								    Entity, BaseEntity,
								    HitEntity, HitBase,
								    Queue);
				if (CollisionStop)
				{
					Velocity = Diff(Velocity,
							Scale(Inner(Velocity, WallNormal),
							      WallNormal));
					if (!Eq(WallNormal, V(0, 0)))
					{
						CollisionAt *= F32C(0.95);
					}
				} else {
					CollisionAt = 1.0;
				}


				if (Between(F32C(0.0), CollisionAt, F32C(0.001)))
				{
					CollisionAt = F32C(0.0);
				}
			}

			Offset = Scale(CollisionAt, Offset);
			MoveEntity(World, Entity, BaseEntity, Offset);
			Movement->Dv = Velocity;

			MovementRemaining *= (1-CollisionAt);
		}
	}



	YASSERT(BaseEntity);
}

PRIVATE void
UpdateWorld(world *World, f32 Dt, world_event_queue *Queue)
{
	for (i32 i = 0; i < World->NumActiveEntities; ++i)
	{
		UpdateEntity(World, World->ActiveEntities + i, Dt,
			     Queue);
	}

	if (World->ShouldReload)
	{
		Reload(World);
		SetCameraPos(World, World->CameraPos);
		World->ShouldReload = YARPG_FALSE;
	}
}

world_position
MovePosition(world_position Start, f32 Dt, v2 Vel, v2 Accel)
{
        world_position Result;
	Result.Chunk = Start.Chunk;
	Result.Offset = Add(Start.Offset,
			    Add(Scale(F32C(0.5) * Dt * Dt, Accel),
				Scale(Dt, Vel)));


	CanonizePosition(&Result);
	return Result;
}

void
InitTransientWorldState(world *World, transient_state *Transient,
			glyph_table *UiTable)
{
        transient_world_state *State = PushStruct(&Transient->Arena, *State);

	ZeroStruct(State);

	State->Test      = 0xDEADCAFE;
	State->State     = Transient;
	World->Transient = State;

		f32 Weights[SpriteTag_COUNT];
	for (udx I = 0;
	     I < ARRAYCOUNT(Weights);
	     ++I)
	{
		Weights[I] = 1.0f;
	}

	f32 Desired[5][SpriteTag_COUNT] = {{ 0.0 }};

	sprite_tag DesiredTags[5] =
	{
		SpriteTag_GRAS,
		SpriteTag_WATER,
		SpriteTag_SAND,
		SpriteTag_LAVA,
		SpriteTag_STONE,
	};


	for (udx I = 0;
	     I < ARRAYCOUNT(DesiredTags);
	     ++I)
	{
		Desired[I][DesiredTags[I]] = 1.0;
	}


	for (udx I = 0;
	     I < ARRAYCOUNT(State->Stamps);
	     ++I)
	{
		State->Stamps[I] = ClosestSpriteMatchOf(&Transient->Assets,
							Category_STAMP,
							Weights,
							Desired[I]);
		FetchSprite(&Transient->Assets, State->Stamps[I]);
	}
}

world_event_queue *
UpdateAndRenderWorld(world *World, f32 DeltaTime,
                     render_group *RenderGroup)
{
	world_event_queue *Queue;
	INSTRUMENT(UpdateAndRenderWorld)
	{
		Queue = PushStruct(&World->Transient->State->Scratch,
				   *Queue);
		InitWorldEventQueue(Queue);

		DrawWorld(World, RenderGroup);

		f32 LongestPossibleTimeStep = F32C(1.0) / F32C(30.0);

		while (DeltaTime > 0)
		{
			f32 SimTimeStep = Min(DeltaTime, LongestPossibleTimeStep);
			UpdateWorld(World, SimTimeStep, Queue);
			DeltaTime -= SimTimeStep;
		}
	}

	return Queue;
}

void
TeleportEntity
(
	world *World,
	stored_world_index Idx,
	world_position NewPos
)
{
	world_entity *Entity = GetEntityByIndex(World, Idx);
	YASSERT(Entity);

	world_position CurrentPos = Entity->Pos;

	if (Eq(NewPos.Chunk, CurrentPos.Chunk))
	{
		world_chunk *CurrentChunk = GetWorldChunk(World, CurrentPos.Chunk);
		world_chunk *NewChunk     = GetWorldChunk(World, NewPos.Chunk);

		if (!NewChunk)
		{
			NewChunk = GenerateWorldChunk(World,
						      NewPos.Chunk,
						      PushStruct(World->Arena,
								 world_chunk));
		}


		YASSERT(CurrentChunk);
		YASSERT(NewChunk);

		if (CurrentChunk && NewChunk)
		{
			RemoveEntity(CurrentChunk, Idx);
			Entity->Pos = NewPos;
			RegisterEntity(NewChunk, Idx);
		}
	} else {
		Entity->Pos = NewPos;
	}

	UpdateOrCreateActiveEntity(World, Idx);
}

void
SetMoveGoal(world *World, stored_world_index Idx,
	    f32 Speed, world_position Pos)
{
	world_entity *Entity = GetEntityByIndex(World, Idx);

	YASSERT(Entity);

	Entity->Speed        = Speed;
	Entity->MovementGoal = Pos;
	Entity->ShouldMove   = YARPG_TRUE;
}

void
StopEntity(world *World, stored_world_index Idx)
{
	world_entity *Entity = GetEntityByIndex(World, Idx);

	YASSERT(Entity);

	Entity->ShouldMove = YARPG_FALSE;
}
