#include <game/battlefield_event.h>

FWDDECLARE(bf_queue_alloc);

void
InitBattlefieldEventQueue(battlefield_event_queue *Queue)
{
	Queue->DataStart = ARRAYCOUNT(Queue->Data);
	Queue->NumItems  = 0;
	Queue->Next      = NULL;
}

struct bf_queue_alloc
{
	battlefield_event *New;
	void        *Data;
};

static bf_queue_alloc
BFAllocHeaderAndData(battlefield_event_queue *Queue, i32 DataSize, i32 DataAlignment)
{
	bf_queue_alloc Result = { .New = NULL, .Data = NULL, };

	battlefield_event *New = NULL;
	void        *Data = NULL;

	i32 NewHeaderEnd = 0;

	{
		i32 Start = sizeof(battlefield_event) * Queue->NumItems;
		i32 End   = Start + sizeof(battlefield_event);

		if (End < Queue->DataStart)
		{
			New = ((battlefield_event *) Queue->Data) + Queue->NumItems;
			NewHeaderEnd = End;
		}
	}

	i32 NewDataStart = 0;
	{
		i32 End = Queue->DataStart;

		i32 Start = End - DataSize;

		void *DataStart = PREVALIGNED(Queue->Data + Start, DataAlignment);

		ptr Diff = (ptr) DataStart - (ptr) Queue->Data;
		YASSERT(Diff <= MAXOF(NewDataStart));
		i32 RealStart = (i32) (Diff);
		if (RealStart > NewHeaderEnd)
		{
			Data = DataStart;
			NewDataStart = RealStart;
		}
	}

	if (New && Data)
	{
		Result.New = New;
		Result.Data = Data;
		Queue->NumItems += 1;
		Queue->DataStart = NewDataStart;
	} else {
		FormatOut("[EVENT QUEUE] Full with {i32} items.\n",
			  Queue->NumItems);
	}

	return Result;
}

b32
BFAddMovementFinishedEvent(battlefield_event_queue *Queue,
			   battlefield_mov_finished_event Event)
{
	b32 EventAdded = YARPG_FALSE;

	battlefield_event *New                    = NULL;
	battlefield_mov_finished_event *EventData = NULL;

	bf_queue_alloc Alloc = BFAllocHeaderAndData(Queue,
					       (i32) sizeof(*EventData),
					       (i32) alignof(*EventData));

	if (Alloc.New)
	{
		New = Alloc.New;
		EventData = (battlefield_mov_finished_event *) Alloc.Data;

		New->Event.MovementFinished = EventData;
		New->Type = BattlefieldEventType_MOVFINISHED;
		*EventData = Event;

		EventAdded = YARPG_TRUE;
	}

	return EventAdded;
}

b32
BFAddAttackFinishedEvent(battlefield_event_queue *Queue,
			 battlefield_atk_finished_event Event)
{
	b32 EventAdded = YARPG_FALSE;

	battlefield_event *New                    = NULL;
	battlefield_atk_finished_event *EventData = NULL;

	bf_queue_alloc Alloc = BFAllocHeaderAndData(Queue,
					       (i32) sizeof(*EventData),
					       (i32) alignof(*EventData));

	if (Alloc.New)
	{
		New = Alloc.New;
		EventData = (battlefield_atk_finished_event *) Alloc.Data;

		New->Event.AttackFinished = EventData;
		New->Type = BattlefieldEventType_ATKFINISHED;
		*EventData = Event;

		EventAdded = YARPG_TRUE;
	}

	return EventAdded;
}
