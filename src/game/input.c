////////////////////////////////////////////////////////////////////////////////

#include <util/string.h>

FWDDECLARE(world_action_bundle);
FWDDECLARE(world_mouse_action_data);

typedef enum
{
	WorldAction_NOACTION,
	WorldAction_CANCELACTION,
	WorldAction_MOUSEACTION,
} world_action;

struct world_mouse_action_data
{
	v2 RelativePos;
	b32 WasPressed;
	b32 IsDown;
	b32 IsDown2;
	b32 WasPressed2;
	i32 Index;
};

struct world_action_bundle
{
        world_action Action;

	union {
		world_mouse_action_data MouseAction;
	} Data;
};

static world_action_bundle
AnalyzeInput(world *World, game_input Input, basis_system Screen)
{
	world_action_bundle Result;
	Result.Action = WorldAction_NOACTION;

	v3 CursorPos;
	CursorPos.X = (f32) Input.CursorPos.X;
	CursorPos.Y = (f32) Input.CursorPos.Y;
	// TODO: think about where the cursor actually is
	// its either at -1 or at 1 depending which way we are looking.
	// it should be -1
	CursorPos.Z = F32C(0.0);


	v3 RelativePos3 = BasisTransform(CursorPos, Screen);
	v2 RelativePos = V(RelativePos3.X, RelativePos3.Y);

	for (i32 I = 0;
	     I < Input.NumDevices;
	     ++I)
	{
                switch (Input.Devices[I].Method)
		{
			break;case InputMethod_MOUSE:
			{
				if (Result.Action != WorldAction_NOACTION) continue;

				mouse_data *Mouse = Input.Devices[I].Data.Mouse;

				if (!Mouse) continue;

				key_state LeftClick = Mouse->Key[KeyId_MOUSE_BUTTON_1];
				key_state RightClick = Mouse->Key[KeyId_MOUSE_BUTTON_3];

				Result.Action = WorldAction_MOUSEACTION;
				Result.Data.MouseAction.RelativePos = RelativePos;
				Result.Data.MouseAction.IsDown = LeftClick.IsDown;
				Result.Data.MouseAction.WasPressed = LeftClick.IsDown &&
					(LeftClick.HalfTransitions & 1);
				Result.Data.MouseAction.IsDown2 = RightClick.IsDown;
				Result.Data.MouseAction.WasPressed2 = RightClick.IsDown &&
					(RightClick.HalfTransitions & 1);

				Result.Data.MouseAction.Index = I;

				if (Result.Data.MouseAction.IsDown)
				{
					FormatOut("[WORLD] {v2} --> {v2}\n",
						  Input.CursorPos, RelativePos);
					/* printf("[WORLD] (%d, %d) --> (%f, %f)\n", */
					/*        Input.CursorPos.X, Input.CursorPos.Y, */
					/*        RelativePos.X, RelativePos.Y); */
				}
			}
			break;case InputMethod_KEYBOARD:
			{
				keyboard_data *Keyboard = Input.Devices[I].Data.Keyboard;

				if (!Keyboard) continue;

				key_state EscapeKey = Keyboard->Key[KeyId_ESCAPE];

				if (EscapeKey.IsDown
				    && (EscapeKey.HalfTransitions & 1))
				{
					Result.Action = WorldAction_CANCELACTION;
					Input.Devices[I].Data.Keyboard = NULL;
				}

			}
			break;case InputMethod_CONTROLLER:
			{

			}
		}
	}

	return Result;
}

static world_position CONSTFN
FromRelative(v2 RelPos, iv3 ActiveChunk)
{
	world_position Result;

	Result.Chunk = ActiveChunk;
	Result.Offset = RelPos;

	CanonizePosition(&Result);

	return Result;
}

static void
HandleInputWorld(game_world *Map, game_input Input, f32 Dt, basis_system Screen)
{
	world *World = &Map->World;
	world_action_bundle Bundle = AnalyzeInput(World, Input, Screen);

	switch (Bundle.Action)
	{
		break;case WorldAction_CANCELACTION:
		{
			Map->InputState.CurrentAction = ActionState_NOACTION;
		}
		break;case WorldAction_MOUSEACTION:
		{
			world_mouse_action_data Mouse = Bundle.Data.MouseAction;
			world_position WorldMouse = FromRelative(Mouse.RelativePos,
								 World->ActiveChunk);


			switch (Map->InputState.CurrentAction)
			{
				break;case ActionState_MOVING:
				{
					if (Mouse.WasPressed)
					{
						stored_world_index BaseIndex = Map->InputState.ActionData.Move.SelectedEntityIndex;
						world_entity *Base = GetEntityByIndex(World, BaseIndex);


						if (Base->Type == EntityType_ARMY &&
						    Base->Data.Army.Allegiance == WorldAllegiance_OWNED)
						{
							SetMoveGoal(World, BaseIndex,
								    1.0,
								    WorldMouse);

							/* StopEntity(World, BaseIndex); */
							/* TeleportEntity(World, BaseIndex, WorldMouse); */

							Input.Devices[Mouse.Index].Data.Mouse = NULL;
							Map->InputState.CurrentAction = ActionState_NOACTION;
						} else {
							Map->InputState.CurrentAction = ActionState_NOACTION;
						}
					}
				}
				break;case ActionState_DRAGGING:
				{
					if (Mouse.IsDown2)
					{
                                                world_position Start = Map->InputState.ActionData.Drag.Start;

						iv3 ChunkDiff = Diff(WorldMouse.Chunk, Start.Chunk);
						v2  OffsetDiff = Diff(WorldMouse.Offset, Start.Offset);

						world_position CurrentPos = World->CameraPos;

						world_position Pos = {
							.Chunk = Diff(CurrentPos.Chunk, ChunkDiff),
							.Offset = Diff(CurrentPos.Offset, OffsetDiff),
						};

						SetCameraPos(World, Pos);
						Input.Devices[Mouse.Index].Data.Mouse = NULL;
					} else {
						Map->InputState.CurrentAction = ActionState_NOACTION;
					}

				}
				break;case ActionState_NOACTION:
				{
					if (Mouse.WasPressed)
					{
						entity_type SelectedType = EntityType_ERROR;

						for (i32 ActiveIndex = 0;
						     ActiveIndex < World->NumActiveEntities;
						     ++ActiveIndex)
						{
							active_world_entity *Entity = &World->ActiveEntities[ActiveIndex];
							world_entity *Base = GetEntityByIndex(World, Entity->BaseIndex);

							v2 RelPos = Entity->RelPosition;
							rect Box = Base->BoundingBox;

							v2 Offset = Diff(Mouse.RelativePos, RelPos);

							if (Between(Box.MinX, Offset.X, Box.MaxX) &&
							    Between(Box.MinY, Offset.Y, Box.MaxY))
							{
								if (SelectedType == EntityType_ARMY)
								{
                                                                        break;
								} else if (SelectedType == EntityType_CITY
									&& Base->Type != EntityType_ARMY)
								{
                                                                        continue;
								}
								SelectedType = Base->Type;

								Map->InputState.CurrentAction = ActionState_MOVING;
								Map->InputState.ActionData.Move.SelectedEntityIndex = Entity->BaseIndex;
								Input.Devices[Mouse.Index].Data.Mouse = NULL;
								FormatOut("[WORLD] Selected {i32}\n",
								       Map->InputState.ActionData.Move.SelectedEntityIndex.ToI32);
							}
						}


					}

					if (Mouse.WasPressed2
					    && Map->InputState.CurrentAction == ActionState_NOACTION)
					{
						Input.Devices[Mouse.Index].Data.Mouse = NULL;
						FormatOut("[WORLD] Started dragging.\n");
						Map->InputState.CurrentAction = ActionState_DRAGGING;
						Map->InputState.ActionData.Drag.Start = WorldMouse;
					}

				}
				INVALIDDEFAULT();

			}
		}
		break;case WorldAction_NOACTION:
		{
			if (Map->InputState.CurrentAction == ActionState_DRAGGING) {
				Map->InputState.CurrentAction = ActionState_NOACTION;
			}
		}
		INVALIDDEFAULT();

	}
}
