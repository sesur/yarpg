#include <game/debug_info.h>

FWDDECLARE(perf_event);

struct perf_event
{
	debug_location const *Loc;
	perf_ts TimeStamp;
	u32 HitCount;
};

static debug_location const SentinelLocation =
{
	.Name = "Sentinel",
	.File = "nowhere",
	.Func = "no-func",
	.Line = 0,
	.Id   = -1,
	.FromWhere = -1,
};

PRIVATE debug_timeline_view
DebugTimelineView_Default
(
	debug_timeline *Timeline
)
{
	perf_ts Now = CurrentTimeStamp();

	debug_timeline_view Result;
	Result.ShowTimeline = 0;

	Result.Zoomed = &SentinelLocation;
	Result.Focus  = Timeline->CapturedTime;

	Result.SortByCycles = 0;
	Result.ShowComplete = 0;
	return Result;
};

PRIVATE void
DebugThread_Default
(
	debug_thread *Thread
)
{
	Thread->Sentinel.Start    = (perf_ts) {};
	Thread->Sentinel.End      = (perf_ts) {};
	Thread->Sentinel.Parent   = &Thread->Sentinel;
	Thread->Sentinel.Prev     = &Thread->Sentinel;
	Thread->Sentinel.Next     = &Thread->Sentinel;
	Thread->Sentinel.HitCount = 0;
	Thread->Sentinel.Loc      = &SentinelLocation;
	Thread->Open              = &Thread->Sentinel;
}

debug_info *
DebugInfo_OfArena
(
	memory_arena *Debug,
	platform_info *PlatformInfo
)
{
	debug_info *Info = PushStruct(Debug, *Info);
	Info->Arena      = Debug;

	Info->NumIds = 0;
	Info->LocToId = IdMapOfArena(NUM_DEBUG_LOCATIONS, Debug);

	idx NumThreads = PlatformInfo->NumWorkerThreads + 1;

	Info->Threads = PushArray(Debug, NumThreads, *Info->Threads);
	Info->NumThreads = NumThreads;
	for (idx ThreadId = 0;
	     ThreadId < NumThreads;
	     ++ThreadId)
	{
		DebugThread_Default(&Info->Threads[ThreadId]);
	}

	Info->FreeBlock = NULL;

	Info->TimelineSentinel = (debug_timeline) {
		.Next = &Info->TimelineSentinel, .Prev = &Info->TimelineSentinel
	};

	Info->FreeTimelineSentinel = (debug_timeline) {
		.Next = &Info->FreeTimelineSentinel, .Prev = &Info->FreeTimelineSentinel
	};

	Info->VisibleTimeline = NULL;

	return Info;
}

PRIVATE void
RegisterDebugLocation
(
	debug_info *Info,
	debug_location const *Loc
)
{
	ptr LocPtr = (ptr) Loc;

	id_index *IdxPtr = CreateIdSlot(&Info->LocToId, (entity_id) {LocPtr});
	if (IdxPtr)
	{
		YASSERT(Info->NumIds <= MAXOF(*IdxPtr));
		*IdxPtr = Info->NumIds++;
	}
}

PRIVATE debug_timing_block *
OpenTimingBlock(debug_info *Debug,
		debug_thread *Thread,
		debug_timing_block *Open,
		perf_event Perf)
{
	debug_timing_block *Block;
	if (Debug->FreeBlock)
	{
		Block = Debug->FreeBlock;
		Debug->FreeBlock = Block->Next;
	} else {
		Block = PushStruct(Debug->Arena, *Block);
	}

	debug_timing_block *Last = Thread->Sentinel.Prev;

	Block->Parent    = Open;
	Block->Prev      = Last;
	Last->Next       = Block;
	Block->Next      = &Thread->Sentinel;
	Thread->Sentinel.Prev = Block;

	Block->Start    = Perf.TimeStamp;
	Block->End      = (perf_ts) {};
	Block->Loc      = Perf.Loc;
	Block->HitCount = Perf.HitCount;

	RegisterDebugLocation(Debug, Perf.Loc);

	return Block;
}

bool
IsBlockOpen
(
	debug_timing_block *Block
)
{
	// End.Clock == 0 means that the block is still open!
	bool IsOpen = Block->End.Clock == 0;

	return IsOpen;
}

PRIVATE debug_timing_block *
CloseTimingBlock(debug_info *Debug, debug_timing_block *Open, perf_event Perf)
{
	debug_timing_block *Result;
	YASSERT(IsBlockOpen(Open));
	YASSERT(Open->Loc == Perf.Loc);
	if (Open->Loc != Perf.Loc)
	{
		FormatOut("[DEBUG] Trying to close block {ptr},"
			  "but open block has is {ptr}\n",
			  (ptr)Perf.Loc, (ptr)Open->Loc);
		Result = Open;
	}
	else
	{
		Open->End = Perf.TimeStamp;

		Result = Open->Parent;
	}

	return Result;
}

PRIVATE perf_event
FromStoredPerfEvent
(
	event_storage *Events,
	stored_perf_event Stored
)
{
	perf_event Result;
	Result.Loc = Stored.Loc;
	Result.TimeStamp.Clock = Events->Start.Clock + Stored.DeltaClock;
	Result.TimeStamp.Cycles = Events->Start.Cycles + Stored.DeltaCycle;
	Result.HitCount = Stored.HitCount;
	return Result;
}

PRIVATE void
DeleteBlocksBeforeInThread
(
	debug_thread *Thread,
	u64 Time,
	debug_info *Debug
)
{
	debug_timing_block *Sentinel = &Thread->Sentinel;
	debug_timing_block *Current = Sentinel->Next;

	while (Current != Sentinel)
	{
		YASSERT(Current->Parent == Sentinel);

		if (Current->End.Clock < Time && !IsBlockOpen(Current))
		{
			debug_timing_block *LastChild = Current;
			while (LastChild->Next->Parent != Sentinel)
			{
				LastChild = LastChild->Next;
			}
			YASSERT(LastChild != Sentinel);
			YASSERT(LastChild->End.Clock < Time);
			debug_timing_block *Next = LastChild->Next;
			Next->Prev = Sentinel;
			Sentinel->Next = Next;

			LastChild->Next = Debug->FreeBlock;
			Debug->FreeBlock = Current;

			Current = Next;
		}
		else
		{
			break;
		}
	}
}

void
DeleteBlocksBefore
(
	debug_info *Debug,
	idx Time
)
{
	for (idx ThreadId = 0;
	     ThreadId < Debug->NumThreads;
	     ++ThreadId)
	{
		DeleteBlocksBeforeInThread(&Debug->Threads[ThreadId],
					   Time,
					   Debug);
	}
}

void
AnalyzePerfEvents
(
	debug_info *Debug,
	idx ThreadId,
	event_storage Events
)
{
	YASSERT(ThreadId < Debug->NumThreads);
	YASSERT(ThreadId >= 0);
	YASSERT(Events.Capacity != Events.End);

	debug_thread *Thread = &Debug->Threads[ThreadId];

	perf_ts Now = CurrentTimeStamp();

	DeleteBlocksBeforeInThread(Thread,
				   Now.Clock - DEFAULT_TIMELINE_LENGTH,
				   Debug);

	debug_timing_block *Current = Thread->Open;

	INSTRUMENT_COUNTED(ParseEvents, Events.End)
	{
		for (debug_event *E = Events.Storage;
		     E != &Events.Storage[Events.End];
		     ++E)
		{
			stored_perf_event Stored = E->Perf;
			perf_event Perf = FromStoredPerfEvent(&Events, Stored);
			switch(Stored.Type)
			{
				break;case PerfEventType_BEGIN:
				{
					Current = OpenTimingBlock(Debug, Thread, Current, Perf);
				}
				break;case PerfEventType_END:
				{
					Current = CloseTimingBlock(Debug, Current, Perf);
				}
			}
		}

		Thread->Open = Current;
	}
}

PRIVATE debug_timing_block *
CopyTimingBlock
(
	memory_arena *Debug,
	debug_timing_block const *ToCopy
)
{
	debug_timing_block *Copy = PushStruct(Debug, *Copy);
	Copy->Start    = ToCopy->Start;
	Copy->End      = ToCopy->End;
	Copy->HitCount = ToCopy->HitCount;
	Copy->Loc      = ToCopy->Loc;
	return Copy;
}

PRIVATE void
DebugThread_CopyTimeSpan
(
	memory_arena *Debug,
	debug_thread *Dest,
	debug_thread const *Src,
	range Time
)
{
	debug_timing_block const *OrigSentinel = &Src->Sentinel;
	debug_timing_block const *OrigParent   = OrigSentinel;
	debug_timing_block *Sentinel           = &Dest->Sentinel;
	debug_timing_block *Head               = Sentinel;
	debug_timing_block *Parent             = Sentinel;

	u64 Begin = Time.Begin < 0 ? 0 : Time.Begin;
	u64 End   = Time.End < 0 ? 0 : Time.End;

	bool StartedCopying = 0;

	for (debug_timing_block *Current = OrigSentinel->Next;
	     Current != OrigSentinel;
	     Current = Current->Next)
	{
		bool DoCopy = 0;
		if (Current->Parent == OrigSentinel)
		{
			bool BeginOk = Current->Start.Clock < End;
			bool EndOk   = IsBlockOpen(Current) || (Begin < Current->End.Clock);
			if (BeginOk && EndOk)
			{
				DoCopy = 1;
				StartedCopying = 1;
			}
			else if (Current->Start.Clock >= End)
			{
				break;
			}
		}
		else
		{
			DoCopy = StartedCopying;
		}

		if (DoCopy)
		{
			YASSERT(Current->Parent == OrigParent);
			YASSERT((OrigParent == OrigSentinel) ^ (Parent != Sentinel));
			debug_timing_block *Copy = CopyTimingBlock(Debug, Current);
			/* if (IsBlockOpen(Copy)) */
			/* { */
			/* 	Copy->End.Clock = End; */
			/* } */
			Copy->Next = Head->Next;
			Copy->Prev = Head;
			Copy->Next->Prev = Copy;
			Copy->Prev->Next = Copy;
			Copy->Parent = Parent;
			Head = Copy;

			debug_timing_block *Next = Current->Next;
			if (Next->Parent != OrigParent)
			{
				if (Next->Parent == Current)
				{
					// go down one level
					OrigParent = Current;
					Parent = Copy;
				}
				else
				{
					// go up multiple levels
					while (Next->Parent != OrigParent)
					{
						YASSERT(OrigParent != OrigSentinel);
						YASSERT(Parent != Sentinel);
						OrigParent = OrigParent->Parent;
						Parent = Parent->Parent;
					}
				}
			}
		}
	}
}

void
CreateSnapshot
(
	debug_info *Debug,
	range Time
)
{
	debug_timeline *Sentinel = &Debug->TimelineSentinel;

	debug_timeline *New;

	if (Debug->FreeTimelineSentinel.Next != &Debug->FreeTimelineSentinel)
	{
		New = Debug->FreeTimelineSentinel.Next;

		New->Next->Prev = New->Prev;
		New->Prev->Next = New->Next;
	}
	else
	{
		New = PushStruct(Debug->Arena, *New);
	}

	New->Prev = Sentinel;
	New->Next = Sentinel->Next;
	New->Next->Prev = New;
	New->Prev->Next = New;

	debug_thread *Threads = PushArray(Debug->Arena, Debug->NumThreads, *Threads);

	perf_ts Start, End;

	for (idx ThreadId = 0;
	     ThreadId < Debug->NumThreads;
	     ++ThreadId)
	{
		debug_thread *Original = &Debug->Threads[ThreadId];
		DebugThread_Default(&Threads[ThreadId]);
		DebugThread_CopyTimeSpan(Debug->Arena, &Threads[ThreadId], Original, Time);
		debug_timing_block *Sentinel = &Threads[ThreadId].Sentinel;
		debug_timing_block *First = Sentinel->Next;
		debug_timing_block *Last  = Sentinel->Prev;

		perf_ts ThreadStart = First->Start;
		perf_ts ThreadEnd;

		if (IsBlockOpen(Last))
		{
			ThreadEnd = CurrentTimeStamp();
		}
		else
		{
			ThreadEnd = Last->End;
		}

		if (ThreadId == 0)
		{
			YASSERT(First != Sentinel);
			YASSERT(Last != Sentinel);
			Start = ThreadStart;
			End   = ThreadEnd;
		}
		else
		{
			if ((First != Sentinel) && (ThreadStart.Clock < Start.Clock))
			{
				Start = ThreadStart;
			}

			if ((Last != Sentinel) && (ThreadEnd.Clock > End.Clock))
			{
				End = ThreadEnd;
			}
		}
	}

	New->Threads      = Threads;
	New->Start        = Start;
	New->End          = End;
	Time.Begin = Start.Clock;
	New->CapturedTime = Time;
	New->View         = DebugTimelineView_Default(New);
}

void
Debug_DeleteTimeline
(
	debug_info *Debug,
	debug_timeline *Timeline
)
{
	if (Debug->VisibleTimeline == Timeline)
	{
		Debug->VisibleTimeline = NULL;
	}

	Timeline->Prev->Next = Timeline->Next;
	Timeline->Next->Prev = Timeline->Prev;

	Timeline->Prev = &Debug->FreeTimelineSentinel;
	Timeline->Next = Debug->FreeTimelineSentinel.Next;

	Timeline->Prev->Next = Timeline;
	Timeline->Next->Prev = Timeline;

	// TODO: free all the debug_timing_blocks!
}
