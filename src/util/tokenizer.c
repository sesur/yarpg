#include <util/tokenizer.h>
#include <util/string.h>

PRIVATE char const *
SkipWhitespace(tokenizer *Tokenizer)
{


	while (IsSpace(*Tokenizer->Start) &&
	       (Tokenizer->Start != Tokenizer->End))
	{
		Tokenizer->Start += 1;
	}
	return Tokenizer->Start;
}

PRIVATE char const *
EatToken(tokenizer *Tokenizer)
{
	while (!IsSpace(*Tokenizer->Start) &&
	       (Tokenizer->Start != Tokenizer->End))
	{
		Tokenizer->Start += 1;
	}
	return Tokenizer->Start;
}

tokenizer
TokenizerStart(str_view Buffer)
{
	tokenizer Tok;

	Tok.Start = &Buffer.Data[0];
	Tok.End   = &Buffer.Data[Buffer.Size];

	SkipWhitespace(&Tok);

	return Tok;
}


str_view
NextToken(tokenizer *Tokenizer)
{
	str_view Result;

	Result.Data = Tokenizer->Start;
	Result.Size = EatToken(Tokenizer) - Result.Data;

	SkipWhitespace(Tokenizer);

	return Result;
}

b32
HasToken(tokenizer *Tokenizer)
{
	return Tokenizer->Start != Tokenizer->End;
}
