#include <util/id_map.h>
#include <util/hash.h>

#define USE_HASH 0
#define USE_SIMD 0
#define USE_FIBONACCI 1

#define SWAP(L, R) do { typeof(L) Tmp = (L); (L) = (R); (R) = Tmp; } while (0)

/* #define Hash(Val) _Generic((Val),			\ */
/* 			   u32: Hash_Murmur3_32,	\ */
/* 			   u64: Hash_Murmur3_64)(Val) */

PRIVATE idx PUREFN
StartPosOf(id_map *Map, entity_id Id)
{
#if USE_HASH
	YSASSERT(sizeof(idx) == sizeof(u64));
	udx Hash = Hash_Murmur3_64((u64)Id.ToIdx);
#else
	udx Hash = (udx) Id.ToIdx;
#endif

#if USE_FIBONACCI
	idx Shift = Map->Shift;
	Hash ^= Hash >> Shift;
	// 11400714819323198485 ~ 2^64 / Phi
	udx Result = (Hash * U64C(11400714819323198485)) >> Shift;
#else
	udx Result = Hash & (Map->Capacity - 1);
#endif

	return Result;
}

PRIVATE idx PUREFN
NextCandidatePos(id_map *Map, idx Pos)
{
	idx Result = (Pos + 1) & (Map->Capacity - 1);
	return Result;
}

PRIVATE idx PUREFN
ProbeSequenceLengthOf(id_map *Map, entity_id Id, idx Pos)
{
	idx PSL = -1;

	if (Id.ToIdx != -1)
	{
		idx StartPos = StartPosOf(Map, Id);
		PSL = Pos - StartPos;

		if (PSL < 0)
		{
			PSL += Map->Capacity;
		}
	}

	return PSL;
}

/* idx MaxShuffles = 0; */
/* idx NumMax = 0; */

PRIVATE void
ShuffleDown(id_map *Map, idx CurrentPos, idx CurrentPSL)
{
//	idx NumShuffles = 0;
	entity_id CurrentEntity = Map->Entities[CurrentPos];
	id_index  CurrentIndex  = Map->Indices[CurrentPos];
	while (CurrentPSL >= 0)
	{
		CurrentPSL += 1;
		idx NextPos = NextCandidatePos(Map, CurrentPos);
		entity_id NextEntity = Map->Entities[NextPos];
		idx NextPSL = ProbeSequenceLengthOf(Map, NextEntity, NextPos);
		if (NextPSL < CurrentPSL)
		{
			Map->MaxPSL = Max(CurrentPSL, Map->MaxPSL);
			SWAP(Map->Entities[NextPos].ToIdx, CurrentEntity.ToIdx);
			SWAP(Map->Indices[NextPos], CurrentIndex);
			CurrentPSL = NextPSL;
		}
		CurrentPos = NextPos;
//		++NumShuffles;
	}

	/* if (MaxShuffles < NumShuffles) */
	/* { */
	/* 	MaxShuffles = NumShuffles; */
	/* 	NumMax = 1; */
	/* } */
	/* else if (MaxShuffles == NumShuffles) */
	/* { */
	/* 	NumMax += 1; */
	/* } */
}

PRIVATE void
ShuffleUp(id_map *Map, idx StartPos)
{
	YASSERT(Map->Entities[StartPos].ToIdx == -1);
	while (1)
	{
		idx NextPos = NextCandidatePos(Map, StartPos);
		entity_id NextEntity = Map->Entities[NextPos];
		if ((NextEntity.ToIdx != -1) &&
		    ProbeSequenceLengthOf(Map, NextEntity, NextPos) >= 0)
		{
			SWAP(Map->Entities[NextPos].ToIdx, Map->Entities[StartPos].ToIdx);
			SWAP(Map->Indices[NextPos], Map->Indices[StartPos]);
			StartPos = NextPos;
		}
		else
		{
			break;
		}
	}

}

id_index *
GetOrAllocIdSlot(id_map *Map, entity_id Id)
{
	idx StartPos = StartPosOf(Map, Id);
	idx PSL = 0;
	// See CreateIdSlot
	idx HighestPSL = Map->MaxPSL + 1;
	for (idx Idx = StartPos;
	     PSL <= HighestPSL;
	     Idx = (PSL++, NextCandidatePos(Map, Idx)))
	{
		YASSERT((PSL == 0) || (Idx != StartPos));
		entity_id SavedEntity = Map->Entities[Idx];
		if (SavedEntity.ToIdx == -1)
		{
			YASSERT(Map->Occupancy < Map->Capacity);

			Map->Entities[Idx] = Id;
			Map->Indices[Idx] = -1;
			Map->Occupancy += 1;
			Map->MaxPSL = Max(PSL, Map->MaxPSL);
			return &Map->Indices[Idx];
		}
		else if (SavedEntity.ToIdx == Id.ToIdx)
		{
			return &Map->Indices[Idx];
		}
		else
		{
			idx SavedPSL = ProbeSequenceLengthOf(Map, SavedEntity, Idx);
			YASSERT(SavedPSL != -1);

			if (SavedPSL < PSL)
			{
				YASSERT(Map->Occupancy < Map->Capacity);

				ShuffleDown(Map, Idx, SavedPSL);
				Map->Entities[Idx] = Id;
				Map->Indices[Idx] = -1;
				Map->Occupancy += 1;
				Map->MaxPSL = Max(PSL, Map->MaxPSL);
				return &Map->Indices[Idx];
			}
		}

	}

	return NULL;
}

#if !USE_SIMD

// return NULL if Id already has a slot
id_index *
CreateIdSlot(id_map *Map, entity_id Id)
{
	YASSERT(Map->Occupancy < Map->Capacity);

	idx Capacity = Map->Capacity;
	idx StartPos = StartPosOf(Map, Id);
	idx Idx = StartPos;

	// Our PSL can be at most Map->MaxPSL + 1, since otherwise
	// we would just shuffle down + swap
	idx HighestPSL = Map->MaxPSL + 1;
	for (idx PSL = 0;
	     PSL <= HighestPSL;
	     ++PSL)
	{
		YASSERT((PSL == 0) || (Idx != StartPos));
		entity_id SavedEntity = Map->Entities[Idx];
		idx NextIdx = Idx + 1;
		if (EXPECT(NextIdx == Capacity, 0))
		{
			NextIdx = 0;
		}
		if (SavedEntity.ToIdx == -1)
		{
			Map->Entities[Idx] = Id;
			Map->Occupancy += 1;
			Map->MaxPSL = Max(PSL, Map->MaxPSL);
			return &Map->Indices[Idx];
		}
		else if (SavedEntity.ToIdx == Id.ToIdx)
		{
			return NULL;
		}
		else
		{
			idx SavedPSL = ProbeSequenceLengthOf(Map, SavedEntity, Idx);
			YASSERT(SavedPSL != -1);

			if (SavedPSL < PSL)
			{
				ShuffleDown(Map, Idx, SavedPSL);
				Map->Entities[Idx] = Id;
				Map->Occupancy += 1;
				Map->MaxPSL = Max(PSL, Map->MaxPSL);
				return &Map->Indices[Idx];
			}

			Idx = NextIdx;
		}
	}

	return NULL;
}

// return NULL if Id does not have a slot
id_index *
GetIdSlot(id_map *Map, entity_id Id)
{
	idx StartPos = StartPosOf(Map, Id);
	idx PSL = 0;
	// MaxPSL cannot change during a search
	idx MaxPSL = Map->MaxPSL;
	// NOTE: We do not need to check if we visited the starting node twice
	// since (PSL <= Map->MaxPSL) already ensures that we do not visit
	// any twice.
	// Map->MaxPSL > Map->Capacity does not make sense.
	for (idx Idx = StartPos;
	     PSL <= MaxPSL;
	     Idx = (PSL++, NextCandidatePos(Map, Idx)))
	{
		entity_id SavedEntity = Map->Entities[Idx];
		if (SavedEntity.ToIdx == -1)
		{
			return NULL;
		}
		else if (SavedEntity.ToIdx == Id.ToIdx)
		{
			return &Map->Indices[Idx];
		}
		else
		{
			idx SavedPSL = ProbeSequenceLengthOf(Map, SavedEntity, Idx);
			YASSERT(SavedPSL != -1);
			if (SavedPSL < PSL)
			{
				/* If Id was actually in this array
				 * we would have swapped it with SavedEntity */
				return NULL;
			}
		}
	}

	return NULL;
}

#endif

b32
DeleteEntity(id_map *Map, idx Pos)
{
	b32 Result = YARPG_TRUE;

	// NOTE: maybe we should just return false instead of crashing here
	YASSERT(Pos >= 0 && Pos < Map->Capacity);

	Map->Entities[Pos].ToIdx = -1;

	ShuffleUp(Map, Pos);

	return Result;
}

void
ClearIdMap(id_map *Map)
{
	Map->Occupancy = 0;
	Map->MaxPSL    = 0;
	for (idx I = 0;
	     I < Map->Capacity;
	     ++I)
	{
		Map->Entities[I].ToIdx = -1;
		Map->Indices[I]        = -1;
	}
}

id_map
IdMapOfArena(idx Size, memory_arena *Arena)
{
	Size = (idx) (F64C(1.35) * (f64)Size); // 1.4 ~ 1/0.75; 0.75 should be max occupancy

	if (!ISPOWOFTWO(Size))
	{
//		FormatOut("[ID MAP] Size not power of two! Enlarging!\n");
		u64 Pow = Ceil2((u64)Size);
		YASSERTF(Pow <= MAXOF(Size), "%lu", Pow);
		Size = (idx)Pow;
	}

	id_map Map;

	Map.Shift     = sizeof(Size) * 8 - TrailingZeroes(Size);
	Map.Capacity  = Size;
	Map.Occupancy = 0;
	Map.MaxPSL    = 0;
	Map.Entities  = PushArray(Arena, Size, *Map.Entities);
	Map.Indices   = PushArray(Arena, Size, *Map.Indices);

	for (idx I = 0;
	     I < Size;
	     ++I)
	{
		Map.Entities[I].ToIdx = -1;
	}

	return Map;
}

PRIVATE __m256i CONSTFN
_mm256_mullow_epi64(__m256i Left, __m256i Right)
{
	// swap idea taken from
        // https://stackoverflow.com/questions/37296289/fastest-way-to-multiply-an-array-of-int64-t/37320416#37320416
	__m256i Swapped = _mm256_shuffle_epi32(Right, 0xB1);
	__m256i LowHigh = _mm256_mullo_epi32(Left, Swapped);

	__m256i LowPart = _mm256_mul_epu32(Left, Right);
	__m256i Shifted  = _mm256_slli_epi64(LowHigh, 32);
	// top 32bit correct, lower 32bit garbage
	__m256i HighPartAndGarbage = _mm256_add_epi32(Shifted, LowHigh);

	__m256i HighMask = _mm256_set1_epi64x(0xFFFFFFFF00000000);
	__m256i HighPart = _mm256_and_si256(HighPartAndGarbage, HighMask);
	__m256i Result   = _mm256_add_epi64(LowPart, HighPart);

	return Result;
}

PRIVATE __m256i CONSTFN
Hash_Murmur3_64X4(__m256i Vals)
{
	__m256i Mul1 = _mm256_set1_epi64x(0xff51afd7ed558ccd);
	__m256i Mul2 = _mm256_set1_epi64x(0xc4ceb9fe1a85ec53);

	__m256i Hashed = Vals;
	Hashed = _mm256_xor_si256(Hashed, _mm256_srli_epi64(Hashed, 33));
	Hashed = _mm256_mullow_epi64(Hashed, Mul1);
	Hashed = _mm256_xor_si256(Hashed, _mm256_srli_epi64(Hashed, 33));
	Hashed = _mm256_mullow_epi64(Hashed, Mul2);
	Hashed = _mm256_xor_si256(Hashed, _mm256_srli_epi64(Hashed, 33));

	return Hashed;
}

PRIVATE __m256i PUREFN //__attribute__((artificial))
StartPosOfX4(id_map *Map, __m256i Ids)
{
#if USE_HASH
	__m256i Hashed   = Hash_Murmur3_64X4(Ids);
#else
	__m256i Hashed   = Ids;
#endif

#if USE_FIBONACCI
	idx Shift = Map->Shift;

	__m256i Mask   = _mm256_set1_epi64x(Shift);
	__m256i Factor = _mm256_set1_epi64x(U64C(11400714819323198485));

	Hashed = _mm256_srlv_epi64(Hashed, Mask);
	__m256i Multiplied = _mm256_mullow_epi64(Hashed, Factor);
	__m256i Result = _mm256_srlv_epi64(Multiplied, Mask);
#else
	__m256i Mask   = _mm256_set1_epi64x(Map->Capacity - 1);
	__m256i Result = _mm256_and_si256(Hashed, Mask);
#endif

	return Result;
}

PRIVATE __m256i PUREFN // __attribute__((artificial))
NextCandidatePosX4(id_map *Map, idx Pos)
{
	__m256i PosX4    = _mm256_set1_epi64x(Pos);
	__m256i Mask     = _mm256_set1_epi64x(Map->Capacity - 1);
	__m256i Offset   = _mm256_setr_epi64x(1, 2, 3, 4);
	__m256i Inc      = _mm256_add_epi64(PosX4, Offset);
	__m256i Result   = _mm256_and_si256(Inc, Mask);
	return Result;
}

PRIVATE __m256i PUREFN // __attribute__((artificial))
ProbeSequenceLengthOfX4(id_map *Map, __m256i Ids, __m256i Pos)
{
	__m256i CapX4    = _mm256_set1_epi64x(Map->Capacity);
	__m256i ZeroX4   = _mm256_set1_epi64x(0);
	__m256i NegX4    = _mm256_set1_epi64x(-1);
	__m256i Mask     = _mm256_add_epi64(CapX4, NegX4);
	__m256i StartPos = StartPosOfX4(Map, Ids);

	__m256i ProbeLength = _mm256_sub_epi64(Pos, StartPos);
	__m256i WrappedP    = _mm256_cmpgt_epi64(ZeroX4, ProbeLength);

	__m256i Added       = _mm256_add_epi64(ProbeLength, CapX4);

	__m256i PSL      = _mm256_blendv_epi8(ProbeLength, Added, WrappedP);

	__m256i InvalidP = _mm256_cmpeq_epi64(Ids, NegX4);

	// since 0xFF... = -1, or-ing InvalidP should be the same
	// as setting the slots to -1 manually
	__m256i Result = _mm256_or_si256(InvalidP, PSL);

	return Result;
}

PRIVATE void
ShuffleDownX4(id_map *Map, idx CurrentPos, idx CurrentPSL)
{
	entity_id CurrentEntity = Map->Entities[CurrentPos];
	id_index  CurrentIndex  = Map->Indices[CurrentPos];
	__m256i Offset = _mm256_setr_epi64x(1, 2, 3, 4);
	while (CurrentPSL >= 0)
	{
		__m256i Current    = _mm256_set1_epi64x(CurrentPSL);
		__m256i Advanced   = _mm256_add_epi64(Current, Offset);

		__m256i Candidates = NextCandidatePosX4(Map, CurrentPos);
		// this probably can be done with 2 maskloads if i think hard enough!
		__m256i Entities   = _mm256_i64gather_epi64((long long const *)Map->Entities, Candidates, sizeof(*Map->Entities));
		__m256i NextPSL    = ProbeSequenceLengthOfX4(Map, Entities, Candidates);

		__m256i SwapP      = _mm256_cmpgt_epi64(Advanced, NextPSL);

		i32 BitMap = _mm256_movemask_pd(_mm256_castsi256_pd(SwapP));
		idx NextPos;
		if (BitMap)
		{
			i32 FirstNonZero = TrailingZeroes(BitMap);

			NextPos = _mm256_varextract_epi64(Candidates, FirstNonZero);
			CurrentPSL = _mm256_varextract_epi64(NextPSL, FirstNonZero);

			SWAP(Map->Entities[NextPos].ToIdx, CurrentEntity.ToIdx);
			SWAP(Map->Indices[NextPos], CurrentIndex);
		}
		else
		{
			NextPos = (CurrentPos + 4) & (Map->Capacity - 1);
			CurrentPSL += 4;
		}

		CurrentPos = NextPos;
	}
}

#if USE_SIMD

id_index *
CreateIdSlot(id_map *Map, entity_id Id)
{
	YASSERT(Map->Occupancy < Map->Capacity);

	idx StartPos = StartPosOf(Map, Id);

	__m256i IdX4       = _mm256_set1_epi64x(Id.ToIdx);
	__m256i NegX4      = _mm256_set1_epi64x(-1);
	__m256i Advance    = _mm256_set1_epi64x(4);
	__m256i PosX4      = _mm256_set1_epi64x(StartPos);
	__m256i Mask       = _mm256_set1_epi64x(Map->Capacity - 1);
	__m256i Offset     = _mm256_setr_epi64x(0, 1, 2, 3);

	__m256i Inc        = _mm256_add_epi64(PosX4, Offset);

	__m256i Candidates  = _mm256_and_si256(Inc, Mask);
	__m256i PSL         = _mm256_setr_epi64x(0, 1, 2, 3);
	__m256i HighestPSL  = _mm256_set1_epi64x(Map->MaxPSL + 1);

	while (1)
	{
		__m256i Entities   = _mm256_i64gather_epi64((long long const *)Map->Entities, Candidates, sizeof(*Map->Entities));
		__m256i SavedPSL   = ProbeSequenceLengthOfX4(Map, Entities, Candidates);

		__m256i InvalidP    = _mm256_cmpeq_epi64(Entities, NegX4);
		__m256i FoundP      = _mm256_cmpeq_epi64(Entities, IdX4);
		__m256i RicherP     = _mm256_cmpgt_epi64(PSL, SavedPSL);
		__m256i ReachedEndP = _mm256_cmpeq_epi64(PSL, HighestPSL);


		i32 InvalidB    = _mm256_movemask_pd(_mm256_castsi256_pd(InvalidP));
		i32 FoundB      = _mm256_movemask_pd(_mm256_castsi256_pd(FoundP));
		i32 RicherB     = _mm256_movemask_pd(_mm256_castsi256_pd(RicherP));
		i32 ReachedEndB = _mm256_movemask_pd(_mm256_castsi256_pd(ReachedEndP));

		if (InvalidB || RicherB)
		{
			i32 FirstInvalid = InvalidB ? TrailingZeroes(InvalidB) : MAXOF(FirstInvalid);
			i32 FirstRicher  = RicherB ? TrailingZeroes(RicherB) : MAXOF(FirstRicher);

			// we ony care about the last 4 bits
			//YASSERT((FirstInvalid < 4) && (FirstRicher < 4));

			Map->Occupancy += 1;

			// if its invalid it may also be richer (its undefined)
			// so if FirstInvalid == FirstRicher, then it was
			// actually invalid and not rich
			if (FirstInvalid <= FirstRicher)
			{
				idx FoundPos = _mm256_varextract_epi64(Candidates, FirstInvalid);

				Map->Entities[FoundPos] = Id;
				return &Map->Indices[FoundPos];
			}
			else
			{
				idx FoundPos = _mm256_varextract_epi64(Candidates, FirstRicher);
				idx FoundPSL = _mm256_varextract_epi64(SavedPSL, FirstRicher);

				ShuffleDownX4(Map, FoundPos, FoundPSL);
				Map->Entities[FoundPos] = Id;
				return &Map->Indices[FoundPos];
			}


		}
		else if (FoundB || ReachedEndB)
		{
			return NULL;
		}
		else
		{
			__m256i NewPSL        = _mm256_add_epi64(PSL, Advance);
			__m256i NewCandidates = _mm256_add_epi64(Candidates, Advance);

			PSL = NewPSL;
			Candidates = _mm256_and_si256(NewCandidates, Mask);
		}

		// this is done this way so we dont "reach" the end
		// on the first loop
		ReachedEndP = _mm256_cmpeq_epi64(PosX4, Candidates);
	}

	return NULL;
}

id_index *
GetIdSlot(id_map *Map, entity_id Id)
{
	idx StartPos = StartPosOf(Map, Id);

	__m256i IdX4       = _mm256_set1_epi64x(Id.ToIdx);
	__m256i NegX4      = _mm256_set1_epi64x(-1);
	__m256i Advance    = _mm256_set1_epi64x(4);
	__m256i PosX4      = _mm256_set1_epi64x(StartPos);
	__m256i Mask       = _mm256_set1_epi64x(Map->Capacity - 1);
	__m256i Offset     = _mm256_setr_epi64x(0, 1, 2, 3);

	__m256i Inc        = _mm256_add_epi64(PosX4, Offset);

	__m256i Candidates  = _mm256_and_si256(Inc, Mask);
	__m256i PSL         = _mm256_setr_epi64x(0, 1, 2, 3);
	__m256i MaxPSL      = _mm256_set1_epi64x(Map->MaxPSL);

	while (1)
	{
		__m256i Entities   = _mm256_i64gather_epi64((long long const *)Map->Entities, Candidates, sizeof(*Map->Entities));
		__m256i SavedPSL   = ProbeSequenceLengthOfX4(Map, Entities, Candidates);

		__m256i InvalidP    = _mm256_cmpeq_epi64(Entities, NegX4);
		__m256i FoundP      = _mm256_cmpeq_epi64(Entities, IdX4);
		__m256i RicherP     = _mm256_cmpgt_epi64(PSL, SavedPSL);
		__m256i ReachedEndP = _mm256_cmpeq_epi64(PSL, MaxPSL);

		i32 InvalidB    = _mm256_movemask_pd(_mm256_castsi256_pd(InvalidP));
		i32 FoundB      = _mm256_movemask_pd(_mm256_castsi256_pd(FoundP));
		i32 RicherB     = _mm256_movemask_pd(_mm256_castsi256_pd(RicherP));
		i32 ReachedEndB = _mm256_movemask_pd(_mm256_castsi256_pd(ReachedEndP));

		if (FoundB)
		{
			i32 FirstNonZero = TrailingZeroes(FoundB);

			idx FoundPos = _mm256_varextract_epi64(Candidates, FirstNonZero);

			return &Map->Indices[FoundPos];
		}
		else if (InvalidB || RicherB || ReachedEndB)
		{
			return NULL;
		}
		else
		{
			__m256i NewPSL        = _mm256_add_epi64(PSL, Advance);
			__m256i NewCandidates = _mm256_add_epi64(Candidates, Advance);

			PSL = NewPSL;
			Candidates = _mm256_and_si256(NewCandidates, Mask);
		}
	}

	return NULL;
}

#endif

#undef SWAP
