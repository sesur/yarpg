#include <def.h>
#include <util/random.h>

rand64
RandomSeries(u64 Seed)
{
	rand64 Result;

	// this ensures that Data != 0
	Result.Data[0] = Seed;
	Result.Data[1] = ~Seed;

	return Result;
}

u64
NextU64(rand64 *Series)
{
	/*
	 * Taken from:
	 * https://en.wikipedia.org/wiki/Xorshift#xorshift+
	 */


	u64 Val1 = Series->Data[0];
	u64 Val2 = Series->Data[1];
	Series->Data[0] = Val2;
	Val1 ^= Val1 << 23;
	Val1 ^= Val1 >> 18;
	Val1 ^= Val2 ^ (Val2 >> 5);
	Series->Data[1] = Val1;
	return (Val1 + Val2);
}

u32
NextU32(rand64 *Series)
{
	u64 Bits = NextU64(Series);
	u32 Result = (u32) (Bits >> 32); // upper 32bits are better
	return Result;
}

i64
NextI64(rand64 *Series)
{
	u64 Bits = NextU64(Series);

	i64 Result = (i64) Bits;

	return Result;
}

u32
NextInRange(rand64 *Series, u32 Max)
{
	u32 Rand = NextU32(Series);
        u32 Result = Rand % Max;

	return Result;
}

f32
NextF32(rand64 *Series)
{
	u32 Rand = NextU32(Series);
	f32 Result = (f32) Rand;

	return Result;
}

static f32
BitCastU32ToF32(u32 Bits)
{
	// NOTE: this is technically not standard compliant!
	// should be ok though!
	// TODO: setup tests for this!
	union { u32 Bits; f32 Value; } Result;
	Result.Bits = Bits;
	return Result.Value;
}

f32
NextNormal(rand64 *Series)
{
	u32 Rand = NextU32(Series);

	// Result is between 1.0 and 2.0
	f32 Result = BitCastU32ToF32(127 << 23 | Rand >> 9);

	Result = Result - F32C(1.0);

	// this is probably fairer but slower ?
	/* f32 Result = (Rand >> 9); */

	/* Result *= 1.19209289551e-7; //0x1.0p-23; */

	return Result;
}

f32
NextBetween(rand64 *Series, f32 Start, f32 End)
{
	f32 Normal = NextNormal(Series);
	f32 Result = (End - Start) * Normal + Start;

	return Result;
}

f32
NextBinormal(rand64 *Series)
{
	return NextBetween(Series, -1, 1);
}
