#include <def.h>
#include <util/noise.h>
/**
 * Implement simplex noise based on https://github.com/SRombauts/SimplexNoise/blob/master/src/SimplexNoise.cpp
 * See also: https://weber.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
 */

static const u8 Permutation[256] = {
    151, 160, 137, 91, 90, 15, 131, 13,
    201, 95, 96, 53, 194, 233, 7, 225,
    140, 36, 103, 30, 69, 142, 8, 99,
    37, 240, 21, 10, 23, 190, 6, 148,
    247, 120, 234, 75, 0, 26, 197, 62,
    94, 252, 219, 203, 117, 35, 11, 32,
    57, 177, 33, 88, 237, 149, 56, 87,
    174, 20, 125, 136, 171, 168, 68, 175,
    74, 165, 71, 134, 139, 48, 27, 166,
    77, 146, 158, 231, 83, 111, 229, 122,
    60, 211, 133, 230, 220, 105, 92, 41,
    55, 46, 245, 40, 244, 102, 143, 54,
    65, 25, 63, 161, 1, 216, 80, 73,
    209, 76, 132, 187, 208, 89, 18, 169,
    200, 196, 135, 130, 116, 188, 159, 86,
    164, 100, 109, 198, 173, 186, 3, 64,
    52, 217, 226, 250, 124, 123, 5, 202,
    38, 147, 118, 126, 255, 82, 85, 212,
    207, 206, 59, 227, 47, 16, 58, 17,
    182, 189, 28, 42, 223, 183, 170, 213,
    119, 248, 152, 2, 44, 154, 163, 70,
    221, 153, 101, 155, 167, 43, 172, 9,
    129, 22, 39, 253, 19, 98, 108, 110,
    79, 113, 224, 232, 178, 185, 112, 104,
    218, 246, 97, 228, 251, 34, 242, 193,
    238, 210, 144, 12, 191, 179, 162, 241,
    81, 51, 145, 235, 249, 14, 239, 107,
    49, 192, 214, 31, 181, 199, 106, 157,
    184, 84, 204, 176, 115, 121, 50, 45,
    127, 4, 150, 254, 138, 236, 205, 93,
    222, 114, 67, 29, 24, 72, 243, 141,
    128, 195, 78, 66, 215, 61, 156, 180
};

PRIVATE u8
Reduce(i32 I)
{
	return Permutation[(u8) (I & 0xFF)];
}

PRIVATE f32
GradientF32(u8 Reduced, f32 X)
{
	u8 H = Reduced & 0x0F;
	f32 Gradient = 1.0f + (H & 7);
	if ((H & 8) != 0) Gradient = -Gradient;
	return Gradient * X;
}

PRIVATE f32
GradientV2(u8 Reduced, v2 Vec)
{
	// TODO: is this really supposed to be 0x3F ?
	u8 H = Reduced & 0x3F;
	f32 U, V;
	if (H < 4)
	{
		U = Vec.X;
		V = F32C(2.0) * Vec.Y;
	}
	else
	{
		U = Vec.Y;
		V = F32C(2.0) * Vec.X;
	}

	if (H & 1)
	{
		U *= -1;
	}

	if (H & 2)
	{
		V *= -1;
	}
	f32 Gradient = U + V;
	return Gradient;
}

PRIVATE f32
GradientV3(u8 Reduced, v3 Vec)
{
	u8 H = Reduced & 0x0F;
	f32 U, V;
	if (H < 8)
	{
		U = Vec.X;
	}
	else
	{
		U = Vec.Y;
	}

	if (H < 4)
	{
		V = Vec.Y;
	}
	else if (H == 12 || H == 14)
	{
		V = Vec.X;
	}
	else
	{
		V = Vec.Z;
	}

	if (H & 1)
	{
		U *= -1;
	}

	if (H & 2)
	{
		V *= -1;
	}

	f32 Gradient = U + V;
	return Gradient;
}

f32 CONSTFN
NoiseF32(f32 X)
{
	i32 IntRange[2];
	IntRange[0] = (int) RoundDown(X);
	IntRange[1] = (IntRange[0]) + 1;
	// now IntRange[0] <= X <= IntRange[1]

	f32 Distance[2];
	Distance[0] = X - (f32) IntRange[0];
	Distance[1] = (f32) IntRange[1] - X;

	f32 Noise = 0;
	for (idx I = 0;
	     I < 2;
	     ++I)
	{
		f32 Factor = 1.0f - Square(Distance[I]);
		f32 Contribution = Square(Factor);
		Noise += Square(Contribution) * GradientF32(Reduce(IntRange[I]), Distance[I]);
	}

	return F32C(0.395) * Noise;
}

f32 CONSTFN
NoiseV2(v2 Vec)
{
	v2 Points[3];

	f32 F2 = F32C(0.366025403);
	f32 G2 = F32C(0.211324865);

	f32 S = (Vec.X + Vec.Y) * F2;
	v2 Skewed = Add(V(S, S), Vec);
	iv2 AsInt = ToInt(RoundDown(Skewed));

	f32 T = (f32) (AsInt.X + AsInt.Y) * G2;

	v2 Origin = Diff(ToFloat(AsInt), V(T, T));

	Points[0] = Diff(Vec, Origin);

	iv2 Simplex;

	if (Points[0].X > Points[0].Y)
	{
		Simplex = IV(1, 0);
	}
	else
	{
		Simplex = IV(0, 1);
	}

	Points[1] = Add(Diff(Points[0], ToFloat(Simplex)), V(G2, G2));
	Points[2] = Add(Diff(Points[0], V(1.0, 1.0)), V(2.0 * G2, 2.0 * G2));

	u8 Gradient[3] = {
		Reduce(AsInt.X + Reduce(AsInt.Y)),
		Reduce(AsInt.X + Simplex.X + Reduce(AsInt.Y + Simplex.Y)),
		Reduce(AsInt.X + 1 + Reduce(AsInt.Y + 1))
	};

	f32 NoiseSum = 0;

	for (idx I = 0;
	     I < 3;
	     ++I)
	{
		f32 Contribution = F32C(0.5) - Square(Points[I].X) - Square(Points[I].Y);
		f32 Noise;
		if (Contribution < 0)
		{
			Noise = 0;
		}
		else
		{
			Contribution = Square(Contribution);
			Noise = Square(Contribution) * GradientV2(Gradient[I], Points[I]);
		}

		NoiseSum += Noise;
	}

	return F32C(45.23065) * NoiseSum;

}

f32 CONSTFN
FractalF32(idx Octaves, f32 Frequency, f32 Amplitude,
	   f32 Lacunarity, f32 Persistance,
	   f32 X)
{
	YASSERT(Octaves > 0);

	f32 A = 0, B = 0;

	f32 Freq = Frequency;
	f32 Ampl = 1;

	for (idx Oct = 0;
	     Oct < Octaves;
	     ++Oct)
	{
		A += Ampl * NoiseF32(Freq * X);
		B += Ampl;

		Ampl *= Lacunarity;
		Freq *= Persistance;
	}

	return Amplitude * (A / B);
}

f32 CONSTFN
FractalV2(idx Octaves, f32 Frequency, f32 Amplitude,
	  f32 Lacunarity, f32 Persistance,
	  v2 Vec)
{
	YASSERT(Octaves > 0);

	f32 A = 0, B = 0;

	f32 Freq = Frequency;
	f32 Ampl = 1.0;

	v2 Delta = V(1, 1);

	for (idx Oct = 0;
	     Oct < Octaves;
	     ++Oct)
	{
		Vec = Add(Vec, Scale(Freq, Delta));
		A += Ampl * NoiseV2(Vec);
		B += Ampl;

		Freq *= Lacunarity;
		Ampl *= Persistance;
	}

	return Amplitude * (A / B);
}
