#if 0
PRIVATE f32
HueToRGB(f32 v1, f32 v2, f32 Hue)
{
	f32 Result;

	if (Hue < 0)
	{
		Hue += 1;
	}
	else if (Hue > 1)
	{
		Hue -= 1;
	}

	if ((6 * Hue) < 1)
	{
		Result = (v1 + (v2 - v1) * 6 * Hue);
	}
	else if ((2 * Hue) < 1)
	{
		Result = v2;
	}
	else if ((3 * Hue) < 2)
	{
		Result = (v1 + (v2 - v1) * ((2.0f / 3) - Hue) * 6);
	}
	else
	{
		Result = v1;
	}

	return Result;
}

v4 CONSTFN
HSLToRGB(v4 Input)
{
	v4 Result;
	Result.A = Input.A;

	if (Input.S < 0 || Input.S > 0)
	{
		f32 Hue = Input.H / 360.0f;

		f32 v2 = (Input.L < 0.5) ? (Input.L * (1 + Input.S)) :
			((Input.L + Input.S) - (Input.L * Input.S));
		f32 v1 = 2 * Input.L - v2;

		Result.R = HueToRGB(v1, v2, Hue + (1.0f / 3));
		Result.G = HueToRGB(v1, v2, Hue);
		Result.B = HueToRGB(v1, v2, Hue - (1.0f / 3));
	}
	else
	{
		Result.R = Result.G = Result.B = Input.L;
	}

	return Result;
}

#else

PRIVATE f32 CONSTFN
FromHue(f32 P, f32 Q, f32 T)
{
	f32 Frac = Fractional(T); // get the fractional part

	YASSERT(0 <= Frac && Frac <= 1);

	f32 Result;

	if (Frac < 1.0/6.0)
	{
		Result = Lerp(P, 6 * Frac, Q);
	}
	else if (Frac < 1.0/2.0)
	{
		Result = Q;
	}
	else if (Frac < 2.0/3.0)
	{
		Result = Lerp(Q, 6 * (Frac - F32C(2.0)/F32C(3.0)), P); //P + (Q - P) * (2.0/3.0 - Frac) * 6;
	}
	else
	{
		Result = P;
	}

	return Result;
}

v4 CONSTFN
HSLToRGB(v4 Input)
{
	f32 H = Input.H;
	f32 S = Input.S;
	f32 L = Input.L;
	f32 AThird = F32C(1.0)/F32C(3.0);

	v4 Result;
	Result.A = Input.A;

	if (S < 0.0 || S > 0.0)
	{
		f32 Q = L < 0.5 ? L * (1 + S) : L + S - L * S;
		f32 P = 2 * L - Q;
		Result.R = FromHue(P, Q, H + AThird);
		Result.G = FromHue(P, Q, H);
		Result.B = FromHue(P, Q, H - AThird);
	}
	else
	{
		Result.R = L;
		Result.G = L;
		Result.B = L;
	}

	return Result;
}
#endif

v4 CONSTFN
RGBToHSL(v4 RGB)
{
	NOTIMPLEMENTED();
}

v4 CONSTFN
ColorOfId(idx Seed)
{
	f32 const Phi = 1.618033988749894848204f;

	v4 Hsl;

	Hsl.H = ((f32) Seed / Phi);
	Hsl.S = F32C(0.6);
	Hsl.L = F32C(0.6);
	Hsl.A = F32C(1.0);

	v4 Result = HSLToRGB(Hsl);

	return Result;
}

v4 CONSTFN
ColorOfF32(f32 Float, f32 Min, f32 Max)
{
	f32 const Phi = F32C(1.618033988749894848204);

	v4 Hsl;

	Hsl.H = Normalize(Min, Float, Max) * 2 * MC_PI;
	Hsl.S = F32C(0.6);
	Hsl.L = F32C(0.6);
	Hsl.A = F32C(1.0);

	v4 Result = HSLToRGB(Hsl);

	return Result;
}
