#include <def.h>
#include <util/image.h>

static u8
AdjustForAlpha(u8 Value, f32 Alpha)
{
	f32 NewValue = Value * Alpha;

	return (u8) NewValue;
}

void
PremultiplyAlpha(image Image)
{
	for (u32 I = 0;
	     I < Image.Height * Image.Width;
	     ++I)
	{
		f32 Alpha = (f32) Image.Data[I].A / 255;

                Image.Data[I].R = AdjustForAlpha(Image.Data[I].R,
						 Alpha);
		Image.Data[I].G = AdjustForAlpha(Image.Data[I].G,
						 Alpha);
		Image.Data[I].B = AdjustForAlpha(Image.Data[I].B,
						 Alpha);
	}
}

static void
UnmultiplyAlpha(image Image)
{
	for (u32 I = 0;
	     I < Image.Height * Image.Width;
	     ++I)
	{
		if (Image.Data[I].A == 0) continue;

		f32 Alpha = (f32) Image.Data[I].A / 255;

                Image.Data[I].R = AdjustForAlpha(Image.Data[I].R,
						 1/Alpha);
		Image.Data[I].G = AdjustForAlpha(Image.Data[I].G,
						 1/Alpha);
		Image.Data[I].B = AdjustForAlpha(Image.Data[I].B,
						 1/Alpha);
	}
}

static void
BlitPixel(pixel *Dest, pixel const *Source)
{
	f32 ScaledSourceAlpha = (f32) Source->A / 255;
	f32 ScaledDestAlpha   = (f32) Dest->A / 255;


	f32 InvSSA = F32C(1.0) - ScaledSourceAlpha;
        f32 R     = InvSSA * ((f32) Dest->R) + ((f32) Source->R);
	f32 G     = InvSSA * ((f32) Dest->G) + ((f32) Source->G);
	f32 B     = InvSSA * ((f32) Dest->B) + ((f32) Source->B);
	f32 A     = 255 * (ScaledSourceAlpha + ScaledDestAlpha
			   - ScaledSourceAlpha * ScaledDestAlpha);

	Dest->R = (u8) R;
	Dest->G = (u8) G;
	Dest->B = (u8) B;
	Dest->A = (u8) A;
}

void
BlitPremultiplied(image Dest, image Source, irect Bounds)
{
	INSTRUMENT(BlitPremultiplied)
	{
		i32 BoundsWidth = Bounds.MaxX - Bounds.MinX;
		i32 BoundsHeight = Bounds.MaxY - Bounds.MinY;

		YASSERT(BoundsWidth <= (i32) Source.Width);
		YASSERT(BoundsHeight <= (i32) Source.Height);

		i32 StartX = Bounds.MinX >= 0 ? Bounds.MinX : 0;
		i32 StartY = Bounds.MinY >= 0 ? Bounds.MinY : 0;

		i32 EndX = (Bounds.MaxX <= (i32) Dest.Width)  ? Bounds.MaxX : (i32) Dest.Width;
		i32 EndY = (Bounds.MaxY <= (i32) Dest.Height) ? Bounds.MaxY : (i32) Dest.Height;

		if (StartX > EndX || StartY > EndY) goto end;

		YASSERT(StartX <= EndX);
		YASSERT(StartY <= EndY);

		i32 DrawWidth = EndX - StartX;
		i32 DrawHeight = EndY - StartY;

		i32 OffsetX = StartX - Bounds.MinX;
		i32 OffsetY = StartY - Bounds.MinY;

		YASSERT(Dest.Width <= I32_MAX);
		YASSERT(Dest.Height <= I32_MAX);
		YASSERT(DrawWidth  <= (i32) Dest.Width);
		YASSERT(DrawHeight <= (i32) Dest.Height);
		YASSERT(DrawWidth  <= (i32) Source.Width);
		YASSERT(DrawHeight <= (i32) Source.Height);
		YASSERT(DrawWidth  >= 0);
		YASSERT(DrawHeight >= 0);

		YASSERT(OffsetX >= 0);
		YASSERT(OffsetY >= 0);

		__m128 Inv255 = _mm_set1_ps(F32C(1/255.0));
		__m128 C255   = _mm_set1_ps(F32C(255.0));
		__m128 One    = _mm_set1_ps(F32C(1.0));

		__m128i Mask32 = _mm_set1_epi32(0xFF);
		__m128i I0     = _mm_set1_epi32(0);
		__m128i I255   = _mm_set1_epi32(255);

		/* Needs to be true to ensure that loading into SIMD registers works. */
		YSASSERT(sizeof(__m128i) == sizeof(pixel[4]));

		for (i32 Y = 0;
		     Y < DrawHeight;
		     ++Y)
		{
			i32 X = 0;
			for (;
			     X + 4 <= DrawWidth;
			     X += 4)
			{
				i32 DX = X + StartX;
				i32 DY = Y + StartY;

				i32 SX = X + OffsetX;
				i32 SY = Y + OffsetY;

				pixel *Dests   = &Dest.Data[DY * (i32) Dest.Width + DX];
				pixel *Sources = &Source.Data[SY * (i32) Source.Width + SX];

				__m128i D = _mm_loadu_si128((__m128i *) Dests);

				/* D      = | RGBA | RGBA | RGBA | RGBA | */
				/* Mask32 = | 0001 | 0001 | 0001 | 0001 | */

				/* DAi    = | 000A | 000A | 000A | 000A | */
				__m128i DAi = _mm_and_si128(Mask32, _mm_srli_epi32(D, 24));
				/* DBi    = | 000B | 000B | 000B | 000B | */
				__m128i DBi = _mm_and_si128(Mask32, _mm_srli_epi32(D, 16));
				/* DGi    = | 000G | 000G | 000G | 000G | */
				__m128i DGi = _mm_and_si128(Mask32, _mm_srli_epi32(D, 8));
				/* DRi    = | 000R | 000R | 000R | 000R | */
				__m128i DRi = _mm_and_si128(Mask32, D);


				__m128 DRs = _mm_cvtepi32_ps(DRi);
				__m128 DGs = _mm_cvtepi32_ps(DGi);
				__m128 DBs = _mm_cvtepi32_ps(DBi);
				__m128 DAs = _mm_cvtepi32_ps(DAi);

				__m128i S = _mm_loadu_si128((__m128i *) Sources);
				/* SAi    = | 000A | 000A | 000A | 000A | */
				__m128i SAi = _mm_and_si128(Mask32, _mm_srli_epi32(S, 24));
				/* SBi    = | 000B | 000B | 000B | 000B | */
				__m128i SBi = _mm_and_si128(Mask32, _mm_srli_epi32(S, 16));
				/* SGi    = | 000G | 000G | 000G | 000G | */
				__m128i SGi = _mm_and_si128(Mask32, _mm_srli_epi32(S, 8));
				/* SRi    = | 000R | 000R | 000R | 000R | */
				__m128i SRi = _mm_and_si128(Mask32, S);

				__m128 SRs = _mm_cvtepi32_ps(SRi);
				__m128 SGs = _mm_cvtepi32_ps(SGi);
				__m128 SBs = _mm_cvtepi32_ps(SBi);
				__m128 SAs = _mm_cvtepi32_ps(SAi);

				SAs = _mm_mul_ps(SAs, Inv255);
				DAs = _mm_mul_ps(DAs, Inv255);
				__m128 ISAs = _mm_sub_ps(One, SAs);

				__m128 Rs = _mm_add_ps(_mm_mul_ps(ISAs, DRs),
						       SRs);

				__m128 Gs = _mm_add_ps(_mm_mul_ps(ISAs, DGs),
						       SGs);

				__m128 Bs = _mm_add_ps(_mm_mul_ps(ISAs, DBs),
						       SBs);

				__m128 As = _mm_sub_ps(_mm_add_ps(SAs,
								  DAs),
						       _mm_mul_ps(SAs,
								  DAs));
				As = _mm_mul_ps(C255, As);


				// TODO: test wether we should round instead of
				//       truncate

				__m128i Ri = _mm_cvttps_epi32(Rs);
				__m128i Gi = _mm_cvttps_epi32(Gs);
				__m128i Bi = _mm_cvttps_epi32(Bs);
				__m128i Ai = _mm_cvttps_epi32(As);

				// make sure that all values lie between 0 and 255
				// probably unecessary

				Ri = _mm_min_epi16(_mm_max_epi16(Ri, I0), I255);
				Gi = _mm_min_epi16(_mm_max_epi16(Gi, I0), I255);
				Bi = _mm_min_epi16(_mm_max_epi16(Bi, I0), I255);
				Ai = _mm_min_epi16(_mm_max_epi16(Ai, I0), I255);

				Gi = _mm_slli_epi32(Gi, 8);
				Bi = _mm_slli_epi32(Bi, 16);
				Ai = _mm_slli_epi32(Ai, 24);

				__m128i RG = _mm_or_si128(Ri, Gi);
				__m128i RGB = _mm_or_si128(RG, Bi);
				__m128i Result = _mm_or_si128(RGB, Ai);


				_mm_storeu_si128((__m128i *) Dests, Result);
			}

			for (;
			     X < DrawWidth;
			     ++X)
			{
				i32 DX = X + StartX;
				i32 DY = Y + StartY;

				i32 SX = X + OffsetX;
				i32 SY = Y + OffsetY;

				BlitPixel(&Dest.Data[DY * (i32) Dest.Width + DX],
					  &Source.Data[SY * (i32) Source.Width + SX]);
			}
		}
	end:
	}
}
