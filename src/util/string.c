#include <def.h>
#include <util/string.h>

u32 PUREFN
CStringLength(char const *Chars)
{
	u32 Length = 0;

	while (*Chars++) Length++;

	return Length;
}

str
MakeStrFromBuffer(u64 Capacity, char *Data)
{
	str Result;

	Result.Capacity = Capacity;
	Result.Size     = 0;
	Result.Data     = Data;

	return Result;
}


str
MakeStr(u64 Capacity, memory_arena *Arena)
{
	str Result = MakeStrFromBuffer(Capacity, PushArray(Arena, Capacity, *Result.Data));

	return Result;
}

str *
Reset(str *String)
{
	String->Size = 0;
	return String;
}

char *
AsCStr(str String)
{
	YASSERT(String.Size != String.Capacity);
	String.Data[String.Size] = '\0';
	return String.Data;
}

str_view
ViewOfString(str String)
{
	str_view View = { .Size = String.Size, .Data = String.Data };
	return View;
}

str_view PUREFN
ViewOfChars(char const *Chars)
{
	str_view View = { .Size = CStringLength(Chars), .Data = Chars};

	return View;
}

str_view
ViewCopy(str_view V)
{
	return V;
}

str_view
ViewOfCountedChars(u64 Size, char const *Chars)
{
	str_view View = { .Size = Size, .Data = Chars};
	return View;
}

b32 PUREFN
IsPrefix(str_view Prefix, str_view Str)
{
	if (Prefix.Size > Str.Size) return 0;

	char const *Start = &Prefix.Data[0];
	char const *End   = &Prefix.Data[Prefix.Size];

	char const *Current = &Str.Data[0];

	while (Start != End)
	{
		if (*Start++ != *Current++)
		{
			return 0;
		}
	}

	return 1;
}

b32
ViewEq(str_view Left, str_view Right)
{
        if (Left.Size != Right.Size)
	{
		return 0;
	}

	u64 Size = Right.Size;
	for (u64 I = 0;
	     I < Size;
	     ++I)
	{
		if (Left.Data[I] != Right.Data[I])
		{
			return 0;
		}
	}

        return 1;
}

str_view
SkipChars(u64 NumChars, str_view View)
{
	char const *Data = NULL;
	u64 Size = 0;

	if (NumChars < View.Size)
	{
		Data = &View.Data[NumChars];
		Size = View.Size - NumChars;
	}

	str_view Result = { .Data = Data, .Size = Size };

	return Result;
}

str_view
Restrict(u64 Length, str_view View)
{
	char const *Data = View.Data;
	u64 Size = Length;

	if (Length > View.Size)
	{
                Length = View.Size;
	}

	str_view Result = { .Data = Data, .Size = Size };

	return Result;
}

b32
IsSpace(char C)
{
	return ((C == '\n') ||
		(C == '\t') ||
		(C == '\v') ||
		(C == '\f') ||
		(C == '\r') ||
		(C == ' '));
}

b32
IsDigit(char C)
{
	return ((C >= '0') && (C <= '9'));
}

str_view PUREFN
Trim(str_view Str)
{
	// NOTE: should probably somehow merge this with
	//       SkipWhitespace in tokenizer.c

	str_view Result;

	char const *Current = &Str.Data[0];
	char const *End     = &Str.Data[Str.Size];
	while (IsSpace(*Current) &&
	       (Current != End))
	{
		Current += 1;
	}

	Result.Data = Current;
	Result.Size = End - Current;

	return Result;
}

static const u64 PowersOf10[] =
{
	U64C(10000000000000000000),
	U64C(1000000000000000000),
	U64C(100000000000000000),
	U64C(10000000000000000),
	U64C(1000000000000000),
	U64C(100000000000000),
	U64C(10000000000000),
	U64C(1000000000000),
	U64C(100000000000),
	U64C(10000000000),
	U64C(1000000000),
	U64C(100000000),
	U64C(10000000),
	U64C(1000000),
	U64C(100000),
	U64C(10000),
	U64C(1000),
	U64C(100),
	U64C(10),
	U64C(1),
};

parse64
ParseU64(str_view Str)
{
	str_view Trimmed = Trim(Str);

	char const *Begin   = &Trimmed.Data[0];
	char const *End     = &Trimmed.Data[Trimmed.Size];
	char const *Current = Begin;

	// parsing is only successful if we read at least one digit
	b32 Valid  = IsDigit(*Current);
	u64 Number = 0;
#if 1
	while (IsDigit(*Current) && (Current != End))
	{
		u64 NextNumber = 10 * Number + (*Current - '0');

		if (NextNumber < Number)
		{
			// overflow!
			Valid = YARPG_FALSE;
			break;
		}
		else
		{
			Number = NextNumber;
		}

		++Current;
	}
#else
	// TODO: check wether this is actually faster than the top version
	udx NumDigits = 0;
	while (IsDigit(*Current) && (Current != End))
	{
		NumDigits += 1;
		Current   += 1;
	}

	Current = Begin;

	if (NumDigits >= ARRAYCOUNT(PowersOf10))
	{
		Valid = YARPG_FALSE;
	}
	else
	{
		idx Power = ARRAYCOUNT(PowersOf10) - NumDigits;
		for (udx Digit = 0; Digit < NumDigits; ++Digit)
		{
			Number += PowersOf10[Power++] * (Begin[Digit] - '0');
		}
	}
#endif

	parse64 Result = { .Valid = Valid, .U64 = Number };

	return Result;
}

parse64
ParseI64(str_view Str)
{
	parse64 Result = { .Valid = YARPG_FALSE };

	str_view Trimmed = Trim(Str);

	str_view Number = Trimmed;
	i64 Neg = 0;

	if (Trimmed.Data[0] == '-')
	{
		Number = SkipChars(1, Trimmed);
		Neg = 1;
	}

	parse64 P = ParseU64(Number);
	u64 N = P.U64;

	if (Neg)
	{
		if (P.Valid && (N <= (-(u64)I64_MIN)))
		{
			Result.Valid = YARPG_TRUE;
			Result.I64 = -N;
		}
	}
	else
	{
		if (P.Valid && (N <= I64_MAX))
		{
			Result.Valid = YARPG_TRUE;
			Result.I64   = N;
		}
	}

	return Result;
}

parse32
ParseI32(str_view Str)
{
	parse32 Result = { .Valid = YARPG_FALSE };

	parse64 P = ParseI64(Str);

	if (P.Valid && (P.I64 >= I32_MIN) && (P.I64 <= I32_MAX))
	{
		Result.Valid = YARPG_TRUE;
		Result.I32   = (i32) P.I64;
	}

	return Result;
}

parse32
ParseU32(str_view Str)
{
	parse32 Result = { .Valid = YARPG_FALSE };

	parse64 P = ParseU64(Str);

	if (P.Valid && (P.U64 <= U32_MAX))
	{
		Result.Valid = YARPG_TRUE;
		Result.U32   = (u32) P.I64;
	}

	return Result;
}

str *
Clear(str *Buf)
{
	ZeroMemory(Buf->Size, (byte *) Buf->Data);
	str *Result = Reset(Buf);
	return Result;
}
