#include <util/bmp.h>

#pragma push_macro("READ_32_BITS")
#pragma push_macro("READ_16_BITS")
#define READ_32_BITS(From, To) do {			\
		To = (((typeof(To))From[0]   << 0)	\
		      | ((typeof(To))From[1] << 8)	\
		      | ((typeof(To))From[2] << 16)	\
		      | ((typeof(To))From[3] << 24));	\
		From += 4;			\
	} while (0)
#define READ_16_BITS(From, To) do {			\
		To = (((typeof(To))From[0]   << 0)	\
		      | ((typeof(To))From[1] << 8));	\
		From += 2;				\
	} while (0)

static b32
LoadBitmapCoreHeader(dib_header *DIB, byte *Data)
{
	byte *Head = Data;

	// ignore size
	Head += 4;

	u16 BitmapWidth;
	u16 BitmapHeight;
	u16 BPP;

	READ_16_BITS(Head, BitmapWidth);
	READ_16_BITS(Head, BitmapHeight);
	Head += 2; // ignore color planes
	READ_16_BITS(Head, BPP);

	DIB->Type         = DIB_CORE_HEADER;
	DIB->BitmapWidth  = (u32) BitmapWidth;
	DIB->BitmapHeight = (u32) BitmapHeight;
	DIB->BPP          = BPP;
	DIB->XDirection   = 1;
	DIB->YDirection   = 1;

	switch (BPP)
	{
	case 8:
	case 16:
	case 32:
	case 64:
		return 1;
	default:
		return 0;
	}
}

static b32
LoadBitmapInfoHeader(dib_header *DIB, byte *Data)
{
	byte *Head = Data;

	i32 BitmapWidth;
	i32 BitmapHeight;
	u16 BPP;
        READ_32_BITS(Head, BitmapWidth);
	READ_32_BITS(Head, BitmapHeight);

	i32 XDirection = 1, YDirection = 1;

	if (BitmapWidth < 0)
	{
		XDirection = -1;
		BitmapWidth = -BitmapWidth;
	}

	if (BitmapHeight < 0)
	{
                YDirection = -1;
		BitmapHeight = -BitmapHeight;
	}

	// ignore color planes
	Head += 2;

	READ_16_BITS(Head, BPP);

	DIB->Type         = DIB_INFO_HEADER;
	DIB->BitmapWidth  = (u32) BitmapWidth;
	DIB->BitmapHeight = (u32) BitmapHeight;
	DIB->BPP          = BPP;
	DIB->XDirection   = XDirection;
	DIB->YDirection   = YDirection;

	switch (BPP)
	{
	case 8:
	case 16:
	case 32:
	case 64:
		return 1;
	default:
		return 0;
	}
}

b32
LoadBMPHeader(bmp_header *Header, byte *FileContents)
{
	b32 Success = 1;

	byte *Head = FileContents;

	u16 MagicNumber = (Head[0] << 8) | Head[1];
	YASSERT(MagicNumber == ('B' << 8 | 'M'));

	Head += 2;

	u32 NumBytes;
	u32 DataOffset;
	u32 DibHeaderSize;

	READ_32_BITS(Head, NumBytes);

	Head += 4; // Skip reserved

	READ_32_BITS(Head, DataOffset);
	READ_32_BITS(Head, DibHeaderSize);

	Header->MagicNumber = MagicNumber;
	Header->NumBytes    = NumBytes;
	Header->DataOffset  = DataOffset;

	switch (DibHeaderSize)
	{
		break;case 12: Success = LoadBitmapCoreHeader(&Header->DIB, Head);
		break;case 40: case 124: Success = LoadBitmapInfoHeader(&Header->DIB, Head);

		break;default: Success = 0;
	};


	return Success;
}

u32 PUREFN
NumPixelsInBMP(bmp_header *Header)
{
	u32 Result = (Header->DIB.BitmapWidth
		      * Header->DIB.BitmapHeight);

	return Result;
}

b32
LoadBMPData(bmp_header *Header, byte *FileContents, image *Image)
{
	pixel *Buffer = Image->Data;

	Image->Width  = Header->DIB.BitmapWidth;
	Image->Height = Header->DIB.BitmapHeight;

	byte *PixelData = FileContents + Header->DataOffset;

	u32 RowPitch = ((Header->DIB.BitmapWidth * Header->DIB.BPP + 31) / 32) * 4;

	YASSERT(Header->DIB.BPP == 32);

	byte *Start;
	if (Header->DIB.YDirection > 0)
	{
		Start = PixelData;
	} else {
		Start = PixelData + RowPitch * (Header->DIB.BitmapHeight - 1);
	}

	for (u32 Y = 0;
	     Y < Header->DIB.BitmapHeight;
	     ++Y)
	{
		byte *Row = Start + Header->DIB.YDirection * RowPitch * Y;

		if (Header->DIB.XDirection < 0)
		{
			Row += (Header->DIB.BitmapWidth - 1) * Header->DIB.BPP / 8;
		}

		for (u32 X = 0;
		     X < Header->DIB.BitmapWidth;
		     ++X)
		{
			byte *Current = Row + Header->DIB.XDirection * X * Header->DIB.BPP / 8;

			u8 R, G, B, A;

			B = Current[0];
			G = Current[1];
			R = Current[2];
			A = Current[3];

			pixel *Pixel = &Buffer[X + Y * Header->DIB.BitmapWidth];
			Pixel->R = R;
			Pixel->G = G;
			Pixel->B = B;
			Pixel->A = A;
		}
	}

	return 1;
}
#pragma pop_macro("READ_32_BITS")
#pragma pop_macro("READ_16_BITS")
