#include <util/utf8.h>
#include <util/string.h>
#include <util/math.h>
#include <compiler.h>

u32
LoadGlyph(u32 StreamLength, byte const *Bytestream, u32 *Glyph)
{
	u32 Length = 0;
	u32 Point  = 0;

	byte First = *Bytestream;
	if ((First >> 7) == 0) {
		/* First: 0xxx xxxx */
		Length = 1;

		if (Length > StreamLength) goto ESPACE;

		Point  = (u32) First;
	} else if ((First >> 5) == 0b110) {
		/* First:  110x xxxx */
		/* Second: 10xx xxxx */
		Length = 2;
		if (Length > StreamLength) goto ESPACE;
		u32 A = Bytestream[0] & 0b00011111;
		u32 B = Bytestream[1] & 0b00111111;
		Point = (A << 6) | B;
	} else if ((First >> 4) == 0b1110) {
		/* First:  1110 xxxx */
		/* Second: 10xx xxxx */
		/* Third:  10xx xxxx */
		Length = 3;
		if (Length > StreamLength) goto ESPACE;
		u32 A = Bytestream[0] & 0b00001111;
		u32 B = Bytestream[1] & 0b00111111;
		u32 C = Bytestream[2] & 0b00111111;
		Point = (A << 12) | (B << 6) | C;
	} else if ((First >> 3) == 0b11110) {
		/* First:  1111 0xxx */
		/* Second: 10xx xxxx */
		/* Third:  10xx xxxx */
		/* Fourth: 10xx xxxx */
		Length = 4;
		if (Length > StreamLength) goto ESPACE;
		u32 A = Bytestream[0] & 0b00000111;
		u32 B = Bytestream[1] & 0b00111111;
		u32 C = Bytestream[2] & 0b00111111;
		u32 D = Bytestream[3] & 0b00111111;
		Point = (A << 18) | (B << 12) | (C << 6) | D;
	} else {
                /* TODO: handle bad character*/
		Length = 1;
		INVALIDCODEPATH();
	}
	goto RETURN;
ESPACE:
	Point  = 0;
	INVALIDCODEPATH();
RETURN:
	*Glyph = Point;
	return Length;
}

u32
SaveGlyph(u32 StreamLength, byte *Bytestream, u32 Glyph)
{
	u8 DataPacket = 0b10000000;
	u8 HeaderPacket[4] = {
		0b00000000,
		0b11000000,
		0b11100000,
		0b11110000
	};

	u32 Length = 0;
	/*
	 * Any UTF32 codepoint decomposes into at most 4 UTF8 bytes;
	 * Bytes[0] is the byte thats written first into the bytestream
	 * and should contain the header packet.
	 */
	u8 Bytes[4];

	if (Glyph < (1 << 7))
	{
		Length = 1;
		u8 Data = (u8) Glyph;
		Bytes[0] = HeaderPacket[Length-1] | Data;
	} else if (Glyph < (1 << 11)) {
		Length = 2;
		u8 Data = (u8) Glyph & 0b00111111;
		Bytes[1] = DataPacket | Data;

		Data = (u8) (Glyph >> 6);
		Bytes[0] = HeaderPacket[Length-1] | Data;
	} else if (Glyph < (1 << 16)) {
		Length = 3;
		u8 Data = (u8) Glyph & 0b00111111;
		Bytes[2] = DataPacket | Data;

		Data = (u8) (Glyph >> 6) & 0b00111111;
		Bytes[1] = DataPacket | Data;

		Data = (u8) (Glyph >> 12);
		Bytes[0] = HeaderPacket[Length-1] | Data;
	} else if (Glyph < (1 << 21)) {
		Length = 4;
		u8 Data = (u8) Glyph & 0b00111111;
		Bytes[3] = DataPacket | Data;

		Data = (u8) (Glyph >> 6) & 0b00111111;
		Bytes[2] = DataPacket | Data;

		Data = (u8) (Glyph >> 12) & 0b00111111;
		Bytes[1] = DataPacket | Data;

		Data = (u8) (Glyph >> 18);
		Bytes[0] = HeaderPacket[Length-1] | Data;
	} else {
		INVALIDCODEPATH();
	}

	if (Length > StreamLength) INVALIDCODEPATH();

	for (u32 I = 0;
	     I < Length;
	     ++I)
	{
		Bytestream[I] = Bytes[I];
	}

	return Length;
}

PRIVATE b32 CONSTFN
ValidUtf8(str_view View)
{
	YASSERT(View.Size <= MAXOF(IDXC(0)));
	idx ToGo = (idx)View.Size;

	char const *Head = View.Data;

	while (ToGo >= 32)
	{
		__m256i Data = _mm256_loadu_si256((__m256i const *) Head);

		Head += 32;
		ToGo -= 32;
	}

	for (idx I = 0;
	     I < ToGo;
	     ++I)
	{

	}

	return 0;
}

idx CONSTFN
GlyphCount(str_view View)
{
	idx ToGo = (idx)View.Size;
	char const *End = View.Data;

	idx NumGlyphs = 0;

	u32 TrailingDataMask = 0;
	char Rest[32] = {};

	YSASSERT(sizeof(Rest) == sizeof(__m256i));

	__m256i DataVal = _mm256_set1_epi8(I8C(0b1000'0000));
	__m256i C2Val   = _mm256_set1_epi8(I8C(0b1100'0000));
	__m256i C3Val   = _mm256_set1_epi8(I8C(0b1110'0000));
	__m256i C4Val   = _mm256_set1_epi8(I8C(0b1111'0000));
	__m256i EVal    = _mm256_set1_epi8(I8C(0b1111'1000));

	__m256i DataMask = _mm256_set1_epi8(I8C(0b1100'0000));
	__m256i C2Mask   = _mm256_set1_epi8(I8C(0b1110'0000));
	__m256i C3Mask   = _mm256_set1_epi8(I8C(0b1111'0000));
	__m256i C4Mask   = _mm256_set1_epi8(I8C(0b1111'1000));
	__m256i EMask    = _mm256_set1_epi8(I8C(0b1111'1000));

	// a byte has type <x> if (byte & <x>Mask) == <x>Val

	while (ToGo > 0)
	{
		// In memory Data looks like this
		// [ Head[31], Head[30], ..., Head[1], Head[0] ]
		__m256i Data;
		u32     OkBytes = U32C(-1); // probably clearer to use U32_MAX
		u32     NumOk;
		if (ToGo >= 32)
		{
			Data = _mm256_loadu_si256((__m256i const *) End);
			NumOk = 32;
		}
		else
		{
			for (idx I = 0;
			     I < ToGo;
			     ++I)
			{
				Rest[I] = End[I];
			}
			OkBytes = OkBytes >> (32 - ToGo);
			YASSERT(TrailingZeroes(~OkBytes) == ToGo);
			NumOk   = (u32) ToGo;
			Data = _mm256_loadu_si256((__m256i const *) &Rest);

			if (TrailingDataMask &&
			    TrailingZeroes(~TrailingDataMask) > ToGo)
			{
				goto adjust_trailing;
			}
		}

		// the only ascii characters have a 0 in the highest bit
		// inverting ControlBitsUpper means that only ascii chars
		// have the high bit set. All logical simd operations only
		// care about the upper bit so IsAscii is basically a mask
		// for ascii bytes
		__m256i Rotated0 = Data;
		u32 R0 = (u32) _mm256_movemask_epi8(Rotated0);

		// since nonloaded bytes are set to 0 we know
		// bits in R0 are only set if there non ascii characters
		if (R0)
		{
#if 1
                        // we would want to rotate byte wise but that version does not exist
			// thankfully since we rotate <= 4 bits and only care
			// about the topmost bit each time it does not matter that we
			// rotate 64bit lanes instead
			__m256i Rotated1 = _mm256_slli_epi64(Data, 1);
			__m256i Rotated2 = _mm256_slli_epi64(Data, 2);
			__m256i Rotated3 = _mm256_slli_epi64(Data, 3);
			__m256i Rotated4 = _mm256_slli_epi64(Data, 4);

			u32 R1 = (u32) _mm256_movemask_epi8(Rotated1);
			u32 R2 = (u32) _mm256_movemask_epi8(Rotated2);
			u32 R3 = (u32) _mm256_movemask_epi8(Rotated3);
			u32 R4 = (u32) _mm256_movemask_epi8(Rotated4);

			u32 S1 = R0;      // 1 bit  set in a row
			u32 S2 = S1 & R1; // 2 bits set in a row
			u32 S3 = S2 & R2; // 3 bits set in a row
			u32 S4 = S3 & R3; // 4 bits set in a row
			u32 S5 = S4 & R4; // 5 bits set in a row

			u32 IsA  = OkBytes & ~R0; // 0XXX XXXX "Ascii"
			// do not need to mask the following since everything
			// thats not an OkByte is 0, so its not set
			u32 IsD  = S1 & ~S2;      // 10XX XXXX "Data"
			u32 IsC2 = S2 & ~S3;      // 110X XXXX "Control with 2 Ones"
			u32 IsC3 = S3 & ~S4;      // 1110 XXXX "Control with 3 Ones"
			// it is unnecessary to do
			// u32 IsC4 = S4 & ~S5;
			// since everything that has S5 set is an error anyways
			// and will be discarded
			u32 IsC4 = S4;            // 1111 0XXX "Control with 4 Ones"
			u32 IsE  = S5;            // 1111 1XXX <- Not allowed
#else
			u32 IsA  = OkBytes & ~R0;

			__m256i DataMasked = _mm256_and_si256(Data, DataMask);
			__m256i C2Masked   = _mm256_and_si256(Data, C2Mask);
			__m256i C3Masked   = _mm256_and_si256(Data, C3Mask);
			__m256i C4Masked   = _mm256_and_si256(Data, C4Mask);
			__m256i EMasked    = _mm256_and_si256(Data, EMask);

			__m256i DataBytes = _mm256_cmpeq_epi8(DataVal, DataMasked);
			__m256i C2Bytes   = _mm256_cmpeq_epi8(C2Val, C2Masked);
			__m256i C3Bytes   = _mm256_cmpeq_epi8(C3Val, C3Masked);
			__m256i C4Bytes   = _mm256_cmpeq_epi8(C4Val, C4Masked);
			__m256i EBytes    = _mm256_cmpeq_epi8(EVal, EMasked);

			u32 IsD  = _mm256_movemask_epi8(DataBytes);
			u32 IsC2 = _mm256_movemask_epi8(C2Bytes);
			u32 IsC3 = _mm256_movemask_epi8(C3Bytes);
			u32 IsC4 = _mm256_movemask_epi8(C4Bytes);
			u32 IsE  = _mm256_movemask_epi8(EBytes);
#endif

			u32 GlyphStarts = OkBytes & (~IsD);

			u64 UpperC2 = ((u64) IsC2);
			u64 UpperC3 = ((u64) IsC3);
			u64 UpperC4 = ((u64) IsC4);

			u64 ControlData = (UpperC2 | UpperC3 | UpperC4) << 1
				        | (UpperC3 | UpperC4) << 2
				        | (UpperC4 << 3);

			u32 ThisExpectedData = (u32) (ControlData & 0xFFFF'FFFF);
			u32 NextExpectedData = (u32) (ControlData >> 32);

			u32 ExpectedD = ThisExpectedData | TrailingDataMask;

			b32 BadTrailing = (b32) (TrailingDataMask & GlyphStarts);
			b32 BadData     = (ThisExpectedData != IsD);
			b32 HasError    = BadTrailing | (b32)IsE | BadData;

			if (HasError)
			{
				if (BadTrailing)
				{
					// the last control block was bollocks
					goto adjust_trailing;
				}

				u32 AllErrors = IsE | (ThisExpectedData ^ IsD);

				u32 FirstError = FirstSet(AllErrors);

				YASSERT(FirstError > 0);

				// u32 >> 32 is not well defined; have to saveguard against it
				if (FirstError == 1) break;
#if 1
				u32 ErrorMask = U32C(-1) >> (32 - FirstError + 1);

				GlyphStarts = GlyphStarts & ErrorMask;

				u32 ErrorPos = 1 << (FirstError-1);

				// if we expected data at the error, but we didnt find it
				// then the previous glyph is invalid, as such
				// we subtract 1 from NumGlyphs here and add it
				// later since we do not unset that glyph start
				if (ErrorPos & ThisExpectedData & ~IsD) NumGlyphs -= 1;

#else

				u32 MaxValidPos = FirstError - 1;

				GlyphStarts = IsA & ~(U32C(-1) << MaxValidPos);
				if (MaxValidPos >= 1)
				{
					GlyphStarts |= IsC2 & ~(U32C(-1) << (MaxValidPos - 1));
				}

				if (MaxValidPos >= 2)
				{
					GlyphStarts |= IsC3 & ~(U32C(-1) << (MaxValidPos - 2));
				}

				if (MaxValidPos >= 3)
				{
					GlyphStarts |= IsC4 & ~(U32C(-1) << (MaxValidPos - 3));
				}

				GlyphStarts = GlyphStarts & OkBytes;
#endif
			}

			NumGlyphs += PopCount(GlyphStarts);
			TrailingDataMask = NextExpectedData;
		}
		else
		{
			if (TrailingDataMask)
			{
				goto adjust_trailing;
			}
			// PopCount would also work
			// but TrailingZeroes + ~ is faster
			// only works because OkBytes always has the form
			// 00...0011...11
			NumGlyphs += NumOk;
			TrailingDataMask = 0;
		}

		End += NumOk;
		ToGo -= 32;
	}

adjust_trailing:
	// if trailing mask data is not zero,
	// that means that the last control block
	// is not legal, so we just need to remove that
	// TODO: adjust End if we plan to return that information
	NumGlyphs -= (TrailingDataMask != 0);

	return NumGlyphs;
}

// GlyphCountValid is like GlyphCount
// except it assumes that all of View
// represents a valid utf8 string
idx CONSTFN
GlyphCountValid(str_view View)
{
	idx ToGo = (idx)View.Size;
	char const *End = View.Data;

	idx NumGlyphs = 0;

	char Rest[32] = {};

	YSASSERT(sizeof(Rest) == sizeof(__m256i));

	while (ToGo > 0)
	{
		// In memory Data looks like this
		// [ Head[31], Head[30], ..., Head[1], Head[0] ]
		__m256i Data;
		u32     OkBytes = U32C(-1);
		u32     NumOk;
		if (ToGo >= 32)
		{
			Data = _mm256_loadu_si256((__m256i const *) End);
			NumOk = 32;
		}
		else
		{
			for (idx I = 0;
			     I < ToGo;
			     ++I)
			{
				Rest[I] = End[I];
			}
			OkBytes = OkBytes >> (32 - ToGo);
			NumOk   = (u32) ToGo;
			Data = _mm256_loadu_si256((__m256i const *) &Rest);
		}

		// the only ascii characters have a 0 in the highest bit
		// inverting ControlBitsUpper means that only ascii chars
		// have the high bit set. All logical simd operations only
		// care about the upper bit so IsAscii is basically a mask
		// for ascii bytes
		__m256i Rotated0 = Data;
		u32 R0 = (u32) _mm256_movemask_epi8(Rotated0);

		// since nonloaded bytes are set to 0 we know
		// bits in R0 are only set if there non ascii characters
		if (R0)
		{
                        // we would want to rotate byte wise but that version does not exist
			// thankfully since we rotate <= 4 bits and only care
			// about the topmost bit each time it does not matter that we
			// rotate 64bit lanes instead
			__m256i Rotated1 = _mm256_slli_epi64(Data, 1);
			u32 R1 = (u32) _mm256_movemask_epi8(Rotated1);

			u32 S1 = R0;      // 1 bit  set in a row
			u32 S2 = S1 & R1; // 2 bits set in a row

			u32 IsD  = S1 & ~S2;      // 10XX XXXX "Data"

			u32 GlyphStarts = OkBytes & (~IsD);

			NumGlyphs += PopCount(GlyphStarts);
		}
		else
		{
			NumGlyphs += NumOk;
		}

		End += NumOk;
		ToGo -= 32;
	}

	return NumGlyphs;
}
