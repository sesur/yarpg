#include <util/layout.h>

irect
VerticalRegion(irect Bounds, i32 Divisions, i32 Start, i32 End)
{
        Start = Divisions < Start ? Divisions : Start;
        End   = Divisions < End   ? Divisions : End;
	Start = End       < Start ? End       : Start;

	b32 UseRest = (Start == End);

	i32 _Start = Divisions - Start;
	i32 _End   = Divisions - End;

	Start = _End;
	End   = _Start;

	irect Result;

	Result.MinX = Bounds.MinX;
	Result.MaxX = Bounds.MaxX;

	i32 Height = Bounds.MaxY - Bounds.MinY;

	if (UseRest)
	{
		Result.MinY = Bounds.MinY + (Height * Start) / Divisions;
		Result.MaxY = Bounds.MaxY;
	}
	else
	{
		Result.MinY = Bounds.MinY + (Height * Start) / Divisions;
		Result.MaxY = Bounds.MinY + (Height * End) / Divisions;
	}


	return Result;
}

irect
HorizontalRegion(irect Bounds, i32 Divisions, i32 Start, i32 End)
{
        Start = Divisions < Start ? Divisions : Start;
        End   = Divisions < End   ? Divisions : End;
	Start = End       < Start ? End       : Start;

	b32 UseRest = (Start == End);

	irect Result;

	Result.MinY = Bounds.MinY;
	Result.MaxY = Bounds.MaxY;

	i32 Width = Bounds.MaxX - Bounds.MinX;

	if (UseRest)
	{
		Result.MinX = Bounds.MinX + (Width * Start) / Divisions;
		Result.MaxX = Bounds.MaxX;
	}
	else
	{
		Result.MinX = Bounds.MinX + (Width * Start) / Divisions;
		Result.MaxX = Bounds.MinX + (Width * End) / Divisions;
	}

	return Result;
}

irect
GridRegion(irect Bounds, iv2 Divisions, iv2 Start, iv2 End)
{
	Start.X = Divisions.X < Start.X ? Divisions.X : Start.X;
        End.X   = Divisions.X < End.X   ? Divisions.X : End.X;
	Start.X = End.X       < Start.X ? End.X       : Start.X;

	Start.Y = Divisions.Y < Start.Y ? Divisions.Y : Start.Y;
        End.Y   = Divisions.Y < End.Y   ? Divisions.Y : End.Y;
	Start.Y = End.Y       < Start.Y ? End.Y       : Start.Y;

	i32 _Start = Divisions.Y - Start.Y;
	i32 _End   = Divisions.Y - End.Y;

	Start.Y = _End;
	End.Y   = _Start;

	iv2 UseRest = IV((Start.X == End.X), (Start.Y == End.Y));

	irect Result;

	i32 Height = Bounds.MaxY - Bounds.MinY;

	if (UseRest.Y)
	{
		Result.MinY = Bounds.MinY + (Height * Start.Y) / Divisions.Y;
		Result.MaxY = Bounds.MaxY;
	}
	else
	{
		Result.MinY = Bounds.MinY + (Height * Start.Y) / Divisions.Y;
		Result.MaxY = Bounds.MinY + (Height * End.Y) / Divisions.Y;
	}

	i32 Width = Bounds.MaxX - Bounds.MinX;

	if (UseRest.X)
	{
		Result.MinX = Bounds.MinX + (Width * Start.X) / Divisions.X;
		Result.MaxX = Bounds.MaxX;
	}
	else
	{
		Result.MinX = Bounds.MinX + (Width * Start.X) / Divisions.X;
		Result.MaxX = Bounds.MinX + (Width * End.X) / Divisions.X;
	}

	return Result;
}
