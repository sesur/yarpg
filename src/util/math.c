#include <util/math.h>
#include <compiler.h>
#include <math.h>

i32
ToInt_f32(f32 Val)
{
	i32 Int = (i32) Val;

	return Int;
}

iv2
ToInt_v2(v2 Val)
{
	iv2 Int = IV((i32) Val.X, (i32) Val.Y);

	return Int;
}

i32
Round_f32(f32 Val)
{
	i32 Rounded;
	if (Val < 0)
	{
		Rounded = (i32) (Val - 0.5f);
	} else {
		Rounded = (i32) (Val + 0.5f);
	}

	return Rounded;
}

iv2
Round_v2(v2 Val)
{
	iv2 Rounded = IV(Round(Val.X), Round(Val.Y));

	return Rounded;
}

f32
ToFloat_i32(i32 Val)
{
	f32 Float = (f32) Val;

	return Float;
}

v2
ToFloat_iv2(iv2 Val)
{
	v2 Float = V(ToFloat(Val.X), ToFloat(Val.Y));

	return Float;
}

v3
ToFloat_iv3(iv3 Val)
{
	v3 Float = V(ToFloat(Val.X), ToFloat(Val.Y), ToFloat(Val.Z));

	return Float;
}

rect
ToFloat_irect(irect Val)
{
	rect Float;

	Float.MinX = (f32) Val.MinX;
	Float.MaxX = (f32) Val.MaxX;
	Float.MinY = (f32) Val.MinY;
	Float.MaxY = (f32) Val.MaxY;

	return Float;
}

i32
ClampI32(i32 Min, i32 X, i32 Max)
{
	i32 Result;

	YASSERT(Min <= Max);
	if (X < Min)      Result = Min;
	else if (X > Max) Result = Max;
	else              Result = X;

	return Result;
}

i64
ClampI64(i64 Min, i64 X, i64 Max)
{
	i64 Result;

	YASSERT(Min <= Max);
	if (X < Min)      Result = Min;
	else if (X > Max) Result = Max;
	else              Result = X;

	return Result;
}

u64
ClampU64(u64 Min, u64 X, u64 Max)
{
	u64 Result;

	YASSERT(Min <= Max);
	if (X < Min)      Result = Min;
	else if (X > Max) Result = Max;
	else              Result = X;

	return Result;
}

f32
ClampF32(f32 Min, f32 X, f32 Max)
{
	f32 Result;

	YASSERT(Min <= Max);
	if (X < Min)      Result = Min;
	else if (X > Max) Result = Max;
	else              Result = X;

	return Result;
}

f32
AbsF32(f32 Val)
{
	return Val >= 0 ? Val : -Val;
}

i32
AbsI32(i32 Val)
{
	return Val >= 0 ? Val : -Val;
}


i32
MaxI32(i32 A, i32 B)
{
	return A > B ? A : B;
}

i32
MinI32(i32 A, i32 B)
{
	return A <= B ? A : B;
}

f32
MaxF32(f32 A, f32 B)
{
	return A > B ? A : B;
}

f32
MinF32(f32 A, f32 B)
{
	return A <= B ? A : B;
}

idx
MaxIdx(idx A, idx B)
{
	return A > B ? A : B;
}

idx
MinIdx(idx A, idx B)
{
	return A <= B ? A : B;
}

u32
MaxU32(u32 A, u32 B)
{
	return A > B ? A : B;
}

u32
MinU32(u32 A, u32 B)
{
	return A <= B ? A : B;
}

u64
MaxU64(u64 A, u64 B)
{
	return A > B ? A : B;
}

u64
MinU64(u64 A, u64 B)
{
	return A <= B ? A : B;
}

f32
SqrtF32(f32 X)
{
	__m128 Value = _mm_set_ss(X);
	__m128 Root  = _mm_sqrt_ss(Value);
	f32 Result   = _mm_cvtss_f32(Root);
	return Result;
}

f32
RSqrtF32(f32 X)
{
	__m128 Value = _mm_set_ss(X);
	__m128 Root  = _mm_rsqrt_ss(Value);
	f32 Result   = _mm_cvtss_f32(Root);
	return Result;
}

f32
SinF32(f32 X)
{
	f32 Result = sinf(X);
	return Result;
}

f32
CosF32(f32 X)
{
	f32 Result = cosf(X);
	return Result;
}

f32
ArcCosF32(f32 X)
{
	f32 Result = acosf(X);
	return Result;
}

f32
RoundUpF32(f32 X)
{
	__m128 Value  = _mm_set_ss(X);
	__m128 Ceiled = _mm_round_ss(ZeroX8, Value, _MM_FROUND_NO_EXC|_MM_FROUND_TO_POS_INF);
	f32 Result    = _mm_cvtss_f32(Ceiled);
	return Result;
}

f32
RoundDownF32(f32 X)
{
	__m128 Value   = _mm_set_ss(X);
	__m128 Floored = _mm_round_ss(ZeroX8, Value, _MM_FROUND_NO_EXC|_MM_FROUND_TO_NEG_INF);
	f32 Result     = _mm_cvtss_f32(Floored);
	return Result;
}

f32
RoundZeroF32(f32 X)
{
	__m128 Value     = _mm_set_ss(X);
	__m128 Truncated = _mm_round_ss(ZeroX8, Value, _MM_FROUND_NO_EXC|_MM_FROUND_TO_ZERO);
	f32 Result       = _mm_cvtss_f32(Truncated);
	return Result;
}

f32
RoundNearestF32(f32 X)
{
	__m128 Value   = _mm_set_ss(X);
	__m128 Rounded = _mm_round_ss(ZeroX8, Value, _MM_FROUND_NO_EXC|_MM_FROUND_TO_NEAREST_INT);
	f32 Result     = _mm_cvtss_f32(Rounded);
	return Result;
}

f32
FractionalF32(f32 X)
{
	f32 Result = X - RoundDown(X);
	return Result;
}

u64 CONSTFN
Ceil2U64(u64 X)
{
	u64 Result;
	if (X <= 1)
	{
		Result = 1;
	}
	else
	{
		Result = U64C(1) << (64 - LeadingZeroes(X-1));
	}
	return Result;
}

u32 CONSTFN
Ceil2U32(u32 X)
{
	u32 Result;
	if (X <= 1)
	{
		Result = 1;
	}
	else
	{
		Result = U32C(1) << (32 - LeadingZeroes(X-1));
	}
	return Result;
}

f32
SquareF32(f32 X)
{
	f32 S = X * X;
	return S;
}

v2
RoundDownV2(v2 Vec)
{
	// TODO: vectorize this
	v2 Result;
	Result.X = RoundDown(Vec.X);
	Result.Y = RoundDown(Vec.Y);
	return Result;
}

f32 CONSTFN
Normalize_f32(f32 Min, f32 T, f32 Max)
{
	YASSERT(Min < Max);
	f32 Result;

	if (T < Min)
	{
		Result = 0;
	}
	else if (T > Max)
	{
		Result = 1;
	}
	else
	{
		Result = (T - Min) / (Max - Min);
	}

	return Result;
}

f32 CONSTFN
SmoothStep_f32(f32 Min, f32 T, f32 Max)
{
	// 3x^2 - 2x^3

	f32 X = Normalize(Min, T, Max);
	f32 Result = X * X * (3 - 2 * X);

	return Result;
}

f32 CONSTFN
SmootherStep_f32(f32 Min, f32 T, f32 Max)
{
	// 6x^2 - 15x^4 + 10x^3

	f32 X = Normalize(Min, T, Max);
	f32 Result = X * X * X * (X * (X * 6 - 15) + 10);

	return Result;
}

f32 CONSTFN
ReScale_f32(f32 T, f32 Min, f32 Max, f32 NewMin, f32 NewMax)
{
	f32 Normalized = Normalize(Min, T, Max);
	f32 Result     = Lerp(NewMin, Normalized, NewMax);
	return Result;
}
