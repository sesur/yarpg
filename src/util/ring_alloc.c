#include <util/ring_alloc.h>

ring_alloc
Ring_FromBuffer
(
	idx NumBytes,
	byte Buffer[NumBytes]
)
{
	ring_alloc Result;
	Result.Capacity = NumBytes;
	Result.Begin = 0;
	Result.End   = 0;
	Result.Data  = Buffer;
	return Result;
}

PRIVATE bool
Ring_IndexIsAllocated
(
	ring_alloc const *Alloc,
	idx Idx
)
{
	idx End = Alloc->End;
	idx Begin = Alloc->Begin;

	YASSERT(Idx >= 0);
	YASSERT(Idx < Alloc->Capacity);

	bool CanChange;
	if (Begin < End)
	{
		CanChange = (Idx <= Begin) || (Idx >= End);
	}
	else
	{
		CanChange = (Idx <= Begin) && (Idx >= End);
	}

	return CanChange;
}

PRIVATE bool
Ring_CanIncreaseBegin
(
	ring_alloc const *Alloc,
	idx NewBegin
)
{
	bool CanIncrease;

	idx End = Alloc->End;
	idx Begin = Alloc->Begin;

	YASSERT(NewBegin >= 0);
	YASSERT(NewBegin < Alloc->Capacity);

	// Begin may not be set to End during a push operation
	// since that would indicate that the buffer is empty which
	// is not possible since we just pushed!
	if (Begin < End)
	{
		CanIncrease = (NewBegin >= Begin) && (NewBegin < End);
	}
	else
	{
		CanIncrease = (NewBegin >= Begin) || (NewBegin < End);
	}

	return CanIncrease;
}

void
Ring_ReserveTo
(
	ring_alloc *Alloc,
	idx NewBegin
)
{
	YASSERT(Ring_CanIncreaseBegin(Alloc, NewBegin));
	Alloc->Begin = NewBegin;
}

void
Ring_PopTo
(
	ring_alloc *Alloc,
	idx NewBegin
)
{
	YASSERT(Ring_IndexIsAllocated(Alloc, NewBegin));
	Alloc->Begin = NewBegin;
}

void
Ring_DequeueTo
(
	ring_alloc *Alloc,
	idx NewEnd
)
{
	YASSERT(Ring_IndexIsAllocated(Alloc, NewEnd));
	Alloc->End = NewEnd;
}

PRIVATE idx PUREFN
Ring_FitsIntoSpan
(
	void *Data,
	idx Begin,
	idx End,
	idx Size,
	idx Alignment
)
{
	idx Result;

	void *PtrStart = NEXTALIGNED(Data + Begin, Alignment);
	idx StartOffset = PtrStart - Data;

	// Since StartOffset + Size will become the new Begin
	// we have to prevent it from reaching End, so this has
	// to be a '>'!
	if (End - StartOffset > Size)
	{
		Result = StartOffset;
	}
	else
	{
		Result = -1;
	}

	return Result;
}

idx
Ring_DryPush
(
	ring_alloc const *Alloc,
	idx Size,
	idx Alignment
)
{
	i32 Result;
	idx Begin = Alloc->Begin;
	idx End   = Alloc->End;
	void *Data = Alloc->Data;
	idx DataSize = Alloc->Capacity;

	YASSERT(Begin >= 0);
	YASSERT(Begin < DataSize);
	YASSERT(End >= 0);
	YASSERT(End < DataSize);

	if (Begin < End)
	{
		/*        v Begin   v End
		 * +------+---------+------+
		 * | USED | FREE    | USED |
		 * +------+---------+------+
		 */

		Result = Ring_FitsIntoSpan(Data, Begin, End, Size, Alignment);
	}
	else // Begin == End means empty buffer!
	{
		/*        v End     v Begin
		 * +------+---------+------+
		 * | FREE | USED    | FREE |
		 * +------+---------+------+
		 */


		Result = Ring_FitsIntoSpan(Data, Begin, DataSize - 1, Size, Alignment);

		if (Result < 0)
		{
			Result = Ring_FitsIntoSpan(Data, 0, End, Size, Alignment);
		}
	}

	return Result;
}

idx
Ring_Push
(
	ring_alloc *Alloc,
	idx Size,
	idx Alignment
)
{
	idx AllocStart = Ring_DryPush(Alloc, Size, Alignment);
	if (AllocStart != -1)
	{
		idx NewBegin = AllocStart + Size;
		if (NewBegin == Alloc->Capacity)
		{
			NewBegin = 0;
		}
		Ring_ReserveTo(Alloc, NewBegin);
	}
	return AllocStart;
}

void *
Ring_DataFrom
(
	ring_alloc const *Alloc,
	idx DataStart
)
{
	YASSERT(Ring_IndexIsAllocated(Alloc, DataStart));
	void *Data = Alloc->Data + DataStart;
	return Data;
}
