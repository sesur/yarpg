#include <util/delaunay.h>

PRIVATE edge_allocator
EdgeAllocator_FromArray
(
	idx Begin,
	idx End,
	quad_edge Edges[End - Begin]
)
{
	edge_allocator Alloc;
	Alloc.Base      = Edges;
	Alloc.Begin     = Begin;
	Alloc.End       = End;
	Alloc.FirstFree = -1;
	Alloc.LastFree  = -1;
	Alloc.FirstUnallocated = Begin;
	return Alloc;
}

PRIVATE idx
NumDelaunayEdges
(
	idx NumPoints
)
{
	idx NumEdges;
	if (NumPoints <= 2)
	{
		NumEdges = 3; // is enough
	}
	else
	{
		NumEdges = 3 * NumPoints - 6;
	}
	return NumEdges;
}

PRIVATE edge_allocator
EdgeAllocator_FromArena
(
	memory_arena *Arena,
	idx NumPoints
)
{
	idx NumEdges = NumDelaunayEdges(NumPoints);

	YASSERT(NumEdges <= MAXOF(I32C(0)));
	edge_allocator Result = EdgeAllocator_FromArray(0, NumEdges,
							PushArray(Arena, NumEdges, *Result.Base));

	return Result;
}

PRIVATE edge_allocator
EdgeAllocator_SplitLeft
(
	edge_allocator *Alloc,
	i32 NumPoints
)
{
	idx NumEdges = NumDelaunayEdges(NumPoints);
	YASSERT(Alloc->FirstUnallocated == Alloc->Begin);
	i32 Split = Alloc->Begin + NumEdges;
	YASSERT(Split < Alloc->End);
	edge_allocator Left = EdgeAllocator_FromArray(Alloc->Begin, Split, Alloc->Base);
	Alloc->Begin = Split;
	Alloc->FirstUnallocated = Split;
	return Left;
}

PRIVATE void
EdgeAllocator_FreeUnallocated
(
	edge_allocator *Alloc
)
{
	if (Alloc->FirstUnallocated != Alloc->End)
	{
		Alloc->Base[Alloc->FirstUnallocated].Edges[0].Origin = Alloc->FirstFree;
		Alloc->Base[Alloc->FirstUnallocated].Edges[0].Next = NULL;
		Alloc->Base[Alloc->FirstUnallocated].Edges[1].Next = NULL;
		Alloc->Base[Alloc->FirstUnallocated].Edges[2].Next = NULL;
		Alloc->Base[Alloc->FirstUnallocated].Edges[3].Next = NULL;
		for (idx Unalloc = Alloc->FirstUnallocated + 1;
		     Unalloc < Alloc->End;
		     ++Unalloc)
		{
			Alloc->Base[Unalloc].Edges[0].Origin = Unalloc - 1;
			Alloc->Base[Unalloc].Edges[0].Next = NULL;
			Alloc->Base[Unalloc].Edges[1].Next = NULL;
			Alloc->Base[Unalloc].Edges[2].Next = NULL;
			Alloc->Base[Unalloc].Edges[3].Next = NULL;
		}
		Alloc->FirstFree = Alloc->End - 1;
		if (Alloc->LastFree == -1)
		{
			Alloc->LastFree = Alloc->FirstUnallocated;
		}
		Alloc->FirstUnallocated = Alloc->End;
	}
}

PRIVATE void
EdgeAllocator_FuseLeft
(
	edge_allocator *Right,
	edge_allocator Left
)
{
	YASSERT(Left.Base == Right->Base);
	YASSERT(Left.End  == Right->Begin);

	EdgeAllocator_FreeUnallocated(&Left);

	if (Left.FirstFree != -1)
	{
		YASSERT(Left.LastFree != -1);
		Left.Base[Left.LastFree].Edges[0].Origin = Right->FirstFree;
		Right->FirstFree = Left.FirstFree;

		if (Right->LastFree == -1)
		{
			Right->LastFree = Left.LastFree;
		}
	}
	else
	{
		YASSERT(Left.LastFree == -1);
	}

	Right->Begin = Left.Begin;
}

PRIVATE edge_record *
MakeEdge
(
	edge_allocator *Alloc,
	i32 From,
	i32 To
)
{
	quad_edge *NewQuadEdge = NULL;
	i32 FirstFree = Alloc->FirstFree;

	if (FirstFree != -1)
	{
		YASSERT(FirstFree >= Alloc->Begin);
		NewQuadEdge = &Alloc->Base[FirstFree];
		i32 Next = NewQuadEdge->Edges[0].Origin;

		if (Next == -1)
		{
			YASSERTF(FirstFree == Alloc->LastFree,
				 "%d == %d",
				 FirstFree, Alloc->LastFree);
			Alloc->FirstFree = -1;
			Alloc->LastFree  = -1;
		}
		else
		{
			Alloc->FirstFree = Next;
		}
	}
	else
	{
		YASSERT(Alloc->FirstUnallocated >= Alloc->Begin);
		YASSERTF(Alloc->FirstUnallocated < Alloc->End,
			 "%d < %d",
			 Alloc->FirstUnallocated, Alloc->End);
		i32 Idx = Alloc->FirstUnallocated++;
		NewQuadEdge = &Alloc->Base[Idx];
	}

	edge_record *Result   = &NewQuadEdge->Edges[0];
	edge_record *Dual     = &NewQuadEdge->Edges[1];
	edge_record *Reversed = &NewQuadEdge->Edges[2];
	edge_record *RDual    = &NewQuadEdge->Edges[3];

	edge_record *Next;
	edge_record *Rot;
	i32 Origin;

	*Result   = (edge_record) { .Origin = From, .Next = Result,   .Rot = Dual };
	*Dual     = (edge_record) { .Origin = -1,   .Next = RDual,    .Rot = Reversed };
	*Reversed = (edge_record) { .Origin = To,   .Next = Reversed, .Rot = RDual };
	*RDual    = (edge_record) { .Origin = -1,   .Next = Dual,     .Rot = Result };

	return Result;
}

PRIVATE bool
SameEdge
(
	edge_record *First,
	edge_record *Second
)
{
#if 0
	edge_record *BaseRecord = (edge_record *) Alloc->Base;
	idx FirstQuad = (First - BaseRecord) / 4;
	idx SecondQuad = (Second - BaseRecord) / 4;

	bool AreSame = (FirstQuad == SecondQuad);
#else

	bool AreSame = (First == Second) || (First->Rot == Second) ||
		(First->Rot->Rot == Second) || (First->Rot->Rot->Rot == Second);
#endif

	return AreSame;
}

PRIVATE void
FreeEdge
(
	edge_allocator *Alloc,
	edge_record *ToFree
)
{
	// a quad edge is 4 edge_records,
	// so every 4 consecutive edge_records you get one quad edge

	// to find the quad_edge for a edge_record, you just need to
	// divide its index by 4 (while rounding down)
	quad_edge *QEdges = Alloc->Base;
	edge_record *Records = &QEdges->Edges[0];
	idx ToFreeIndex = ToFree - Records;
	idx QuadIndex   = ToFreeIndex / 4;

	YASSERT(QuadIndex >= Alloc->Begin);

	YASSERTF(QuadIndex < Alloc->FirstUnallocated,
		 "%ld < %d",
		 QuadIndex, Alloc->FirstUnallocated);
	quad_edge *QuadToFree = &QEdges[QuadIndex];
	QuadToFree->Edges[0].Next = NULL;
	// the following three are not necessary!
	QuadToFree->Edges[1].Next = NULL;
	QuadToFree->Edges[2].Next = NULL;
	QuadToFree->Edges[3].Next = NULL;
	i32 FirstFree = Alloc->FirstFree;
	QuadToFree->Edges[0].Origin = FirstFree;

	if (FirstFree == -1)
	{
		Alloc->FirstFree = QuadIndex;
		Alloc->LastFree = QuadIndex;
	}
	else
	{
		Alloc->FirstFree = QuadIndex;
	}
}

PRIVATE bool
UsedEdge
(
	edge_record const *Edge
)
{
	bool InUse = (Edge->Next != NULL);
	return InUse;
}

PRIVATE edge_record *
ReverseEdge
(
	edge_record const *Edge
)
{
	edge_record *Dual = Edge->Rot;
	edge_record *Reverse = Dual->Rot;
	return Reverse;
}

PRIVATE edge_record *
PrevEdge
(
	edge_record *Edge
)
{
	edge_record *Dual     = Edge->Rot;
	edge_record *NextFace = Dual->Next;
	edge_record *Previous = NextFace->Rot;

	return Previous;
}

PRIVATE edge_record *
NextDestEdge
(
	edge_record *Edge
)
{
	edge_record *Dual      = Edge->Rot;
	edge_record *RDual     = ReverseEdge(Dual);
	edge_record *OtherFace = RDual->Next;
	edge_record *NextDest  = OtherFace->Rot;

	return NextDest;
}

PRIVATE void
Splice
(
	edge_record *First,
	edge_record *Second
)
{
	YASSERT(UsedEdge(First->Next->Rot));
	YASSERT(UsedEdge(Second->Next->Rot));
	YASSERT(UsedEdge(First));
	YASSERT(UsedEdge(Second));
#define SWAP(L, R) do { typeof(L) Tmp = (L); (L) = (R); (R) = Tmp; } while (0)
	SWAP(First->Next->Rot->Next, Second->Next->Rot->Next);
	SWAP(First->Next, Second->Next);
#undef SWAP
}

PRIVATE void
DeleteEdge
(
	edge_allocator *Alloc,
	edge_record *ToDelete
)
{
	Splice(ToDelete, PrevEdge(ToDelete));
	Splice(ReverseEdge(ToDelete), PrevEdge(ReverseEdge(ToDelete)));
	FreeEdge(Alloc, ToDelete);
}

PRIVATE i32
EdgeDestination
(
	edge_record const *Record
)
{
	edge_record *Reversed = ReverseEdge(Record);
	i32 Destination = Reversed->Origin;
	return Destination;
}

PRIVATE edge_record *
ConnectEdges
(
	edge_allocator *Alloc,
	edge_record *First,
	edge_record *Second
)
{
	edge_record *Connection = MakeEdge(Alloc,
					   EdgeDestination(First),
					   Second->Origin);

	Splice(Connection, NextDestEdge(First));
	Splice(ReverseEdge(Connection), Second);

	return Connection;
}

PRIVATE bool
CounterClockWise
(
	v2 *Points,
	idx One,
	idx Two,
	idx Three
)
{
	// one -> two -> three is counter clock wise,
	// if three is left of one -> two

	return LeftOf(Points[One], Points[Two], Points[Three]);
}

PRIVATE bool
PointInCircumCircle
(
	v2 *Points,
	idx One,
	idx Two,
	idx Three,
	idx Test
)
{
	// Test wether `Test` is inside the circumcircle
	// of the triangle `One`, `Two`, `Three`

	// The triangle is assumed to be in ccw order!

	v2 Triangle1 = Points[One];
	v2 Triangle2 = Points[Two];
	v2 Triangle3 = Points[Three];
	v2 TestP     = Points[Test];

	YASSERT(One != Two);
	YASSERT(Two != Three);
	YASSERT(Three != One);

	bool Inside;
	// while this might seem unnecessary the code that is getting generated
	// for the computation of Col3 on higher optimization levels leads
	// to very inaccurate results.  In the case where
	// Test is the same as another point it actually turns a zero
	// into a nonzero number resulting in changing Inside from 0 to 1.
	// In practice this seems to only affect the result if the Test is exactly
	// one of the other three points so we check this here explicitly.
	// If the issue manifests again we need to disable fma instructions
	// in the code generation of this code!
	if ((Test == One) || (Test == Two) || (Test == Three))
	{
		Inside = 0;
	}
	else
	{
		v3 Col1 = V(Triangle1.X - TestP.X, Triangle2.X - TestP.X, Triangle3.X - TestP.X);
		v3 Col2 = V(Triangle1.Y - TestP.Y, Triangle2.Y - TestP.Y, Triangle3.Y - TestP.Y);
		v3 Col3 = V(Square(Triangle1.X) - Square(TestP.X) +
			    Square(Triangle1.Y) - Square(TestP.Y),
			    Square(Triangle2.X) - Square(TestP.X) +
			    Square(Triangle2.Y) - Square(TestP.Y),
			    Square(Triangle3.X) - Square(TestP.X) +
			    Square(Triangle3.Y) - Square(TestP.Y));
		f32 Det = Determinant(Col1, Col2, Col3);

		Inside = Det > 0;
	}

	return Inside;
}

PRIVATE bool
PointLeftOfEdge
(
	v2 *Points,
	i32 Test,
	edge_record *Edge
)
{

	bool IsLeft = CounterClockWise(Points,
				       Test,
				       Edge->Origin,
				       EdgeDestination(Edge));
	return IsLeft;
}

PRIVATE bool
PointRightOfEdge
(
	v2 *Points,
	idx Test,
	edge_record *Edge
)
{
	bool IsRight = CounterClockWise(Points,
					Test,
					EdgeDestination(Edge),
					Edge->Origin);
	return IsRight;
}

PRIVATE bool
ValidCandidateEdge
(
	v2 *Points,
	edge_record *Candidate,
	edge_record *Basel
)
{
	bool IsValid = PointRightOfEdge(Points, EdgeDestination(Candidate), Basel);
	return IsValid;
}

FWDDECLARE(delaunay_data);
FWDDECLARE(delaunay_future);
struct delaunay_future
{
	edge_pair Pair;
	bool Finished;
};
struct delaunay_data
{
	edge_allocator *Alloc;
	v2 *Points;
	idx Low;
	idx High;
	delaunay_future volatile *Future;
};

PRIVATE edge_pair DelaunayStep(edge_allocator *Alloc, v2 *Points, idx Low, idx High);

PRIVATE TASK_CALLBACK_FN(DelaunayWrapper)
{
	delaunay_data Data = *(delaunay_data volatile *) Arg;
	Data.Future->Pair = DelaunayStep(Data.Alloc, Data.Points, Data.Low, Data.High);
	InterlockedWrite(&Data.Future->Finished, 1);
}

// returns two hull edges pointing down
// the first one starts at Low (is ccw)
// the second one ends at High (was ccw but is returnd as backwards ref)
// That way you just have to follow
// the edges to find the lowest LR-edge!
PRIVATE edge_pair
DelaunayStep
(
	edge_allocator *Alloc,
	v2 *Points,
	idx Low,
	idx High
)
{
	idx NumEdges = High - Low + 1;

	YASSERT(NumEdges >= 2);

	edge_pair Result;

	switch (NumEdges)
	{
		break;case 2:
		{
			edge_record *Edge = MakeEdge(Alloc, Low, High);
			Result = (edge_pair) { {Edge, ReverseEdge(Edge)} };
		}
		break;case 3:
		{
			idx Middle = Low + 1;
			YASSERT(Middle + 1 == High);
			edge_record *LowMiddle = MakeEdge(Alloc, Low, Middle);
			edge_record *MiddleHigh = MakeEdge(Alloc, Middle, High);

			Splice(ReverseEdge(LowMiddle), MiddleHigh);

			if (CounterClockWise(Points, Low, Middle, High))
			{
				edge_record *Connection = ConnectEdges(Alloc, MiddleHigh, LowMiddle);
				Result = (edge_pair) { {LowMiddle, ReverseEdge(MiddleHigh)} };
			}
			else if (CounterClockWise(Points, Low, High, Middle))
			{
				edge_record *Connection = ConnectEdges(Alloc, MiddleHigh, LowMiddle);
				Result = (edge_pair) { {ReverseEdge(Connection), Connection} };
			}
			else
			{
				// they are colinear
				Result = (edge_pair) { {LowMiddle, ReverseEdge(MiddleHigh)} };
			}

		}
		break;default:
		{
			YASSERT(NumEdges > 3);
			idx Middle = (Low + High) / 2;
			edge_pair LeftPair, RightPair;
			idx NumLeft = Middle - Low + 1;
			idx NumRight = High - (Middle + 1) + 1;
			if (NumEdges > DELAUNAY_ASYNC_THRESHOLD)
			{
				edge_allocator LeftAlloc = EdgeAllocator_SplitLeft(Alloc, NumLeft);
				YASSERT(NumDelaunayEdges(NumLeft) <= LeftAlloc.End - LeftAlloc.Begin);
				YASSERT(NumDelaunayEdges(NumRight) <= Alloc->End - Alloc->Begin);
				delaunay_future volatile LeftFuture = {};
				YASSERT(!InterlockedRead(&LeftFuture.Finished));
				{
					delaunay_data   LeftData =
						{
							.Alloc  = &LeftAlloc,
							.Points = Points,
							.Low    = Low,
							.High   = Middle,
							.Future = &LeftFuture
						};
					while (!PushTask(DelaunayWrapper, sizeof(LeftData),
							 alignof(LeftData), &LeftData))
					{
						Wait();
					}
				}
				RightPair = DelaunayStep(Alloc, Points,
							 Middle + 1, High);

				while (!InterlockedRead(&LeftFuture.Finished))
				{
					Wait();
				}

				EdgeAllocator_FuseLeft(Alloc, LeftAlloc);

				LeftPair = LeftFuture.Pair;
			}
			else
			{
				LeftPair = DelaunayStep(Alloc, Points,
							Low, Middle);
				RightPair = DelaunayStep(Alloc, Points,
							 Middle + 1, High);
			}


			edge_record *LowEdge = LeftPair.Refs[0]; // starts at low, pointing left/down
			edge_record *HighEdge = RightPair.Refs[1]; // starts high, pointing right/down

			YASSERT(LowEdge->Origin == Low);
			YASSERT(HighEdge->Origin == High);

			edge_record *Left = LeftPair.Refs[1];
			edge_record *Right = RightPair.Refs[0];

			while (1)
			{
				if (PointLeftOfEdge(Points, Right->Origin, Left))
				{
					Left = NextDestEdge(Left);
					continue;
				}

				if (PointRightOfEdge(Points, Left->Origin, Right))
				{
					Right = ReverseEdge(Right)->Next;
					continue;
				}

				break;
			}

			edge_record *Basel = ConnectEdges(Alloc, ReverseEdge(Right), Left);

			if (Left->Origin == LowEdge->Origin)
			{
				LowEdge = ReverseEdge(Basel);
			}

			if (Right->Origin == HighEdge->Origin)
			{
				HighEdge = Basel;
			}

			bool LValid, RValid;
			do
			{
				edge_record *LeftCandidate = ReverseEdge(Basel)->Next;

				if (ValidCandidateEdge(Points, LeftCandidate, Basel))
				{
					for (edge_record *Next = LeftCandidate->Next;
					     PointInCircumCircle(Points,
								 EdgeDestination(Basel),
								 Basel->Origin,
								 EdgeDestination(LeftCandidate),
								 EdgeDestination(Next));
					     Next = LeftCandidate->Next)
					{
						YASSERTF(!SameEdge(LeftCandidate, LowEdge),
							 "%d -> %d vs %d -> %d\n",
							 LeftCandidate->Origin,
							 ReverseEdge(LeftCandidate)->Origin,
							 LowEdge->Origin,
							 ReverseEdge(LowEdge)->Origin);
						YASSERTF(!SameEdge(LeftCandidate, HighEdge),
							 "%d -> %d vs %d -> %d\n",
							 LeftCandidate->Origin,
							 ReverseEdge(LeftCandidate)->Origin,
							 HighEdge->Origin,
							 ReverseEdge(HighEdge)->Origin);
						DeleteEdge(Alloc, LeftCandidate);
						LeftCandidate = Next;
					}
				}

				edge_record *RightCandidate = PrevEdge(Basel);

				if (ValidCandidateEdge(Points, RightCandidate, Basel))
				{
					for (edge_record *Prev = PrevEdge(RightCandidate);
					     PointInCircumCircle(Points,
								 EdgeDestination(Basel),
								 Basel->Origin,
								 EdgeDestination(RightCandidate),
								 EdgeDestination(Prev));
					     Prev = PrevEdge(RightCandidate))
					{
						YASSERTF(!SameEdge(RightCandidate, LowEdge),
							 "%d -> %d vs %d -> %d\n",
							 RightCandidate->Origin,
							 ReverseEdge(RightCandidate)->Origin,
							 LowEdge->Origin,
							 ReverseEdge(LowEdge)->Origin);
						YASSERTF(!SameEdge(RightCandidate, HighEdge),
							 "%d -> %d vs %d -> %d\n",
							 RightCandidate->Origin,
							 ReverseEdge(RightCandidate)->Origin,
							 HighEdge->Origin,
							 ReverseEdge(HighEdge)->Origin);
						DeleteEdge(Alloc, RightCandidate);
						RightCandidate = Prev;
					}
				}

				LValid = ValidCandidateEdge(Points, LeftCandidate, Basel);
				RValid = ValidCandidateEdge(Points, RightCandidate, Basel);

				if (LValid && RValid)
				{
					if (PointInCircumCircle(Points,
								EdgeDestination(LeftCandidate),
								LeftCandidate->Origin,
								RightCandidate->Origin,
								EdgeDestination(RightCandidate)))
					{
						goto connect_right;
					}
					else
					{
						goto connect_left;
					}
				}
				else if (LValid && !RValid)
				{
				connect_left:
					Basel = ConnectEdges(Alloc,
							     ReverseEdge(Basel),
							     ReverseEdge(LeftCandidate));
				}
				else if (!LValid && RValid)
				{
				connect_right:
					Basel = ConnectEdges(Alloc,
							     RightCandidate,
							     ReverseEdge(Basel));

				}
			}
			while (LValid || RValid);

			Result = (edge_pair) { {LowEdge, HighEdge} };
		}
	}

	return Result;
}

PRIVATE v2
CircumCenterOf
(
	v2 A,
	v2 B,
	v2 C
)
{
	v2 Left = Diff(B, A);
	v2 Right = Diff(C, A);

	f32 LeftSq = Inner(Left, Left);
	f32 RightSq = Inner(Right, Right);

	f32 Denom = 0.5 / (Left.X * Right.Y - Left.Y * Right.X);

	v2 Delta;
	Delta.X = (Right.Y * LeftSq - Left.Y * RightSq) * Denom;
	Delta.Y = (Left.X * RightSq - Right.X * LeftSq) * Denom;

	v2 Result = Add(Delta, A);

	return Result;
}

PRIVATE bool
ComputeVoronoiVertex
(
	edge_record *Edge,
	v2 *Origins,
	v2 *Vertices,
	i32 Index
)
{
	bool IsBorder;

	Edge->Origin = Index;

	idx NumNeighbors = 0;

	for (edge_record *Current = Edge->Next;
	     Current != Edge;
	     Current = Current->Next)
	{
		NumNeighbors += 1;
		YASSERT(Current->Origin == -1);
		Current->Origin = Index;
	}

	if (NumNeighbors != 2)
	{
		/* FormatOut("Vertex {i32} is the border vertex!\n", */
		/* 	  Edge->Origin); */
		IsBorder = 1;
	}
	else
	{
		edge_record *Other = Edge->Next->Rot;

		idx One   = ReverseEdge(Edge)->Rot->Origin;
		idx Two   = Edge->Rot->Origin;
		idx Three = ReverseEdge(Other)->Origin;

		YASSERT(One != Three);
		YASSERT(Two != Three);

		Vertices[Index] = CircumCenterOf(Origins[One], Origins[Two], Origins[Three]);

		/* FormatOut("Vertex {i32} of triangle {i32}, {i32}, {i32}: {v2}\n", */
		/* 	  Index, One, Two, Three, Vertices[Index]); */

		IsBorder = 0;
	}

	return IsBorder;
}

PRIVATE i32
AssociateIndex
(
	quad_edge *Edge,
	i32 NextAvailable,
	i32 *BorderVertex,
	v2 *Origins,
	v2 *Vertices
)
{
	edge_record *Normal = &Edge->Edges[0];
	edge_record *Reverse = &Edge->Edges[2];
	edge_record *Dual = &Edge->Edges[1];
	edge_record *RDual = &Edge->Edges[3];

	i32 Start = Normal->Origin;
	i32 End   = Reverse->Origin;

	if (Dual->Origin == -1)
	{
		i32 Index = NextAvailable++;
		bool IsBorder = ComputeVoronoiVertex(Dual, Origins, Vertices, Index);

		if (IsBorder)
		{
			YASSERT(*BorderVertex == -1);
			*BorderVertex = Index;
		}
	}

	if (RDual->Origin == -1)
	{
		i32 Index = NextAvailable++;
		bool IsBorder = ComputeVoronoiVertex(RDual, Origins, Vertices, Index);

		if (IsBorder)
		{
			YASSERT(*BorderVertex == -1);
			*BorderVertex = Index;
		}
	}

	/* FormatOut("The regions {i32}, {i32} share vertices {i32}, {i32}.\n", */
	/* 	  Start, End, Dual->Origin, RDual->Origin); */

	return NextAvailable++;
}

PRIVATE i32
LeftVoronoiVertex
(
	edge_record const *Record
)
{
	edge_record *RDual = ReverseEdge(Record)->Rot;
	i32 Vertex = RDual->Origin;
	return Vertex;
}

PRIVATE v2
LeftBisector
(
	v2 Start,
	v2 End
)
{
	v2 Delta = Diff(End, Start);
	v2 Ortho = LeftOrtho(Delta);

	return Ortho;
}

PRIVATE v2
RightBisector
(
	v2 Start,
	v2 End
)
{
	v2 Delta = Diff(End, Start);
	v2 Ortho = RightOrtho(Delta);

	return Ortho;
}

PRIVATE b32
ForwardXIntersect
(
	f32 *Step,
	v2 *IntersectionPoint,
	v2 Start,
	v2 Direction,
	f32 TargetX,
	f32 MaxFactor
)
{
	b32 Found;

	f32 const VerySmall = 1e-6;
	f32 Distance = TargetX - Start.X;

	if (Between(-VerySmall, Distance, VerySmall))
	{
		Found = 1;
		*IntersectionPoint = Start;
		*Step = 0;
	}
	else if (Between(-VerySmall, Direction.X, VerySmall))
	{
		Found = 0;
	}
	else
	{
		f32 Factor = Distance / Direction.X;

		if (Factor < 0 || Factor > MaxFactor)
		{
			Found = 0;
		}
		else
		{
			Found = 1;
			*Step = Factor;
			*IntersectionPoint = Add(Start, Scale(Factor, Direction));
		}
	}

	return Found;
}

PRIVATE b32
ForwardYIntersect
(
	f32 *Step,
	v2 *IntersectionPoint,
	v2 Start,
	v2 Direction,
	f32 TargetY,
	f32 MaxFactor
)
{
	v2 StartX = V(Start.Y, Start.X);
	v2 DirectionX = V(Direction.Y, Direction.X);
	v2 Tmp;

	b32 Found = ForwardXIntersect(Step, &Tmp, StartX, DirectionX, TargetY,
				      MaxFactor);

	if (Found)
	{
		*IntersectionPoint = V(Tmp.Y, Tmp.X);
	}

	return Found;
}

PRIVATE void
AddIntersection
(
	v2 *Intersections,
	idx *Borders,
	f32 FirstStep,
	idx Count,
	v2 Intersection,
	idx Border,
	f32 Step

)
{
	if ((Count == 1) && (FirstStep > Step))
	{
		Intersections[1] = Intersections[0];
		Borders[1] = Borders[0];
		Intersections[0] = Intersection;
		Borders[0] = Border;
	}
	else
	{
		Intersections[Count] = Intersection;
		Borders[Count] = Border;
		FirstStep = Step;
	}
}

PRIVATE idx
FindIntersections
(
	v2 Intersections[static 2], // intersection point
	idx Borders[static 2],      // index of intersected border
	v2 Start,
	v2 End,
	rect BoundingBox,
	bool IsRay
)
{
	// we need to sort the intersections before we return them!
	v2 Direction = Diff(End, Start);

	f32 MaxFactor = IsRay ? MAXOF(MaxFactor) : 1.0;

	idx Count = 0;
	v2 Temp;
	// there is actually no need to initialize FirstStep here
	// since we only "access" it when Count == 1, but
	// since we factored out AddIntersection to a function
	// we need to initialize it to make the compiler happy!
	f32 FirstStep = MaxFactor;
	f32 Step;

	if(ForwardXIntersect(&Step, &Temp, Start, Direction,
			     BoundingBox.MaxX, MaxFactor)
	   && Between(BoundingBox.MinY,
		      Temp.Y,
		      BoundingBox.MaxY))
	{
		YASSERT(Count < 2);

		AddIntersection(Intersections, Borders, FirstStep,
				Count, Temp, 0, Step);

		Count++;
	}

	if(ForwardYIntersect(&Step, &Temp, Start, Direction,
			     BoundingBox.MaxY, MaxFactor)
	   && Between(BoundingBox.MinX,
		      Temp.X,
		      BoundingBox.MaxX))
	{
		YASSERT(Count < 2);

		AddIntersection(Intersections, Borders, FirstStep,
				Count, Temp, 1, Step);

		Count++;
	}

	if(ForwardXIntersect(&Step, &Temp, Start, Direction,
			     BoundingBox.MinX, MaxFactor)
	   && Between(BoundingBox.MinY,
		      Temp.Y,
		      BoundingBox.MaxY))
	{
		YASSERT(Count < 2);

		AddIntersection(Intersections, Borders, FirstStep,
				Count, Temp, 2, Step);

		Count++;
	}

	if(ForwardYIntersect(&Step, &Temp, Start, Direction,
			     BoundingBox.MinY, MaxFactor)
	   && Between(BoundingBox.MinX,
		      Temp.X,
		      BoundingBox.MaxX))
	{
		YASSERT(Count < 2);

		AddIntersection(Intersections, Borders, FirstStep,
				Count, Temp, 3, Step);

		Count++;
	}

	return Count;
}

TODO("Do not allocate a vertex for the border vertex, instead leave it negative \
     -2 for example, so we can just check Idx < 0 instead of Idx == Border");

PRIVATE v2
LloydStep
(
	rect BoundingBox,
	edge_record *Start,
	v2 *DelaunayVertices,
	v2 *VoronoiVertices,
	i32 BorderIdx
)
{
	v2 Accum = V(0, 0);
	idx NumVertices = 0;

	i32 Origin = Start->Origin;

	YASSERT(UsedEdge(Start));

#define Voronoi(Edge) (LeftVoronoiVertex(Edge))
#define IsBorder(Edge) (Voronoi(Edge) == BorderIdx)
#define IsInside(Edge) (!IsBorder(Edge)					\
			&& PointInsideRect(VoronoiVertices[Voronoi(Edge)], BoundingBox))
#define AddVertex(Vec) do {						\
		v2 Copy = (Vec);					\
		Accum = Add(Accum, Copy);				\
		NumVertices += 1;					\
		Last = Copy;						\
	} while (0)

#define AddBorderVertices(Start, End) do {				\
			idx Begin = (Start);				\
			idx Stop = (End);				\
			while (Begin != Stop)				\
			{						\
				AddVertex(BorderVertices[Begin]);	\
				Begin = ModInc(Begin, 4);		\
			}						\
		} while (0)

	edge_record const *Current = Start;
	while (!IsInside(Current))
	{
		Current = Current->Next;
		if (Current == Start)
		{
			break;
		}
	}

	v2 BorderVertices[] =
		{
			V(BoundingBox.MaxX, BoundingBox.MaxY),
			V(BoundingBox.MinX, BoundingBox.MaxY),
			V(BoundingBox.MinX, BoundingBox.MinY),
			V(BoundingBox.MaxX, BoundingBox.MinY)
		};

	bool StartInside;

	if (IsInside(Current))
	{
		StartInside = 1;
	}
	else
	{
		if (IsBorder(Current))
		{
			Current = Current->Next;
		}

		StartInside = 0;
	}

	i32 ExitBorder = -1;
	edge_record const *First = Current;
	bool Inside = StartInside;
	YASSERT(!IsBorder(Current));
	v2 Last = VoronoiVertices[Voronoi(Current)];
	do
	{
		edge_record const *Next = Current->Next;
		idx NextIdx = Voronoi(Next);

		if (NextIdx == BorderIdx)
		{
			edge_record const *Border = Next;
			Next = Next->Next;

			YASSERT(!IsBorder(Next));

			b32 StartInside = Inside;
			b32 EndInside   = IsInside(Next);

			v2 PreBorderVertex = VoronoiVertices[Voronoi(Current)];
			v2 PostBorderVertex = VoronoiVertices[Voronoi(Next)];

			v2 Dir1 = LeftBisector(DelaunayVertices[Border->Origin],
					       DelaunayVertices[ReverseEdge(Border)->Origin]);

			v2 Dir2 = RightBisector(DelaunayVertices[Next->Origin],
						DelaunayVertices[ReverseEdge(Next)->Origin]);

			v2 StartVec = Add(PreBorderVertex, Dir1);
			v2 EndVec   = Add(PostBorderVertex, Dir2);

			idx NumIntersections;
			v2 TempIntersections[2];
			idx TempBorders[2];

			v2 Intersections[2] =
				{ V(SIGNALINGNAN, SIGNALINGNAN), V(SIGNALINGNAN, SIGNALINGNAN) };
			idx Borders[2] = { -20, -20 };

			if (StartInside)
			{
				NumIntersections = FindIntersections(TempIntersections,
								     TempBorders,
								     PreBorderVertex,
								     StartVec,
								     BoundingBox,
								     1 /* IsRay */);
				YASSERT(NumIntersections == 1);
				Intersections[0] = TempIntersections[0];
				Borders[0] = TempBorders[0];
			}

			if (EndInside)
			{
				NumIntersections = FindIntersections(TempIntersections,
								     TempBorders,
								     PostBorderVertex,
								     EndVec,
								     BoundingBox,
								     1);

				YASSERT(NumIntersections == 1);
				Intersections[1] = TempIntersections[0];
				Borders[1] = TempBorders[0];
			}

			if (StartInside && EndInside)
			{
				AddVertex(Intersections[0]);
				AddBorderVertices(Borders[0], Borders[1]);
				AddVertex(Intersections[1]);
				AddVertex(PostBorderVertex);
				ExitBorder = -1;
			}
			else if (!StartInside && EndInside)
			{
				AddBorderVertices(ExitBorder, Borders[1]);
				AddVertex(Intersections[1]);
				AddVertex(PostBorderVertex);
				ExitBorder = -1;
			}
			else if (StartInside && !EndInside)
			{
				AddVertex(Intersections[0]);
				ExitBorder = Borders[0];
			}
			else
			{
				YASSERT(StartInside == 0);
				YASSERT(EndInside == 0);
			}

			Inside = EndInside;

			YASSERTF(Inside ? (ExitBorder == -1) : (ExitBorder != -1),
				 "Center:%d; Other: %d, %d, %d",
				 Origin, EdgeDestination(Current),
				 EdgeDestination(Border),
				 EdgeDestination(Next));
		}
		else if (Inside)
		{
			if (IsInside(Next))
			{
				AddVertex(VoronoiVertices[NextIdx]);
			}
			else
			{
				v2 Intersections[2];
				idx Borders[2];

				idx NumIntersections = FindIntersections(Intersections,
									 Borders,
									 Last,
									 VoronoiVertices[NextIdx],
									 BoundingBox, 0);
				YASSERT(NumIntersections == 1);

				AddVertex(Intersections[0]);
				ExitBorder = Borders[0];
				Inside     = 0;
			}
		}
		else
		{
			v2 Intersections[2];
			idx Borders[2];

			idx NumIntersections = FindIntersections(Intersections,
								 Borders,
								 VoronoiVertices[Voronoi(Current)],
								 VoronoiVertices[NextIdx],
								 BoundingBox, 0);

			YASSERT(NumIntersections <= 2);
			YASSERT(NumIntersections >= 0);

			if (NumIntersections != 0)
			{
				if (!StartInside)
				{
					YASSERT(NumIntersections == 2);
					YASSERT(Borders[0] != Borders[1]);
					// last is not really defined here
					// so we just set last to something that
					// does not cause visual artifacts
					Last = BorderVertices[Borders[0]];
					AddBorderVertices(Borders[0], Borders[1]);
					AddVertex(Intersections[0]);
					AddVertex(Intersections[1]);
					ExitBorder = Borders[1];

					// now there is no difference anymore
					StartInside = 1;
				}
				else
				{
					YASSERT(ExitBorder != -1);

					AddBorderVertices(ExitBorder, Borders[0]);

					if (NumIntersections == 2)
					{
						YASSERT(!IsInside(Next));
						AddVertex(Intersections[0]);
						AddVertex(Intersections[1]);
						ExitBorder = Borders[1];
					}
					else if (NumIntersections == 1)
					{
						YASSERT(IsInside(Next));

						AddVertex(Intersections[0]);
						AddVertex(VoronoiVertices[NextIdx]);
						ExitBorder = -1;
						Inside     = 1;
					}
				}
			}
			else
			{
				YASSERT(!IsInside(Next));
			}
		}

		Current = Next;
	}
	while (Current != First);

#undef IsBorder
#undef IsInside
#undef Voronoi
#undef AddVertex
#undef AddBorderVertices

#if 0
	v2 Last = VoronoiVertices[LeftVoronoiVertex(Start)];
	do
	{
		edge_record *Next = Current->Next;
		YASSERT(Next->Origin == Origin);

		// this is the index of the triangle
		// constructed by Current and Next
		i32 VoronoiIdx = LeftVoronoiVertex(Next);
		v2 New;

		// TODO: handle borders correctly
		//       even if VoronoiIdx != Border
		//       (for example if resulting New is outside the box!)

		if (VoronoiIdx != Border)
		{
			New = VoronoiVertices[VoronoiIdx];
		}
		else
		{
			v2 Begin = DelaunayVertices[Next->Origin];
			v2 End   = DelaunayVertices[ReverseEdge(Next)->Origin];

			v2 Delta = LeftBisector(Begin, End);

			v2 Temp = Add(Last, Scale(100, Delta));

			entity_id Triangle = NextEntity(Test);
			tri_data Data = (tri_data)
				{
					.Points =
					{
						DelaunayVertices[Origin],
						Last,
						Temp
					},
					.Color = ColorOfId(Origin)
				};
			AddTriangle(Test, Triangle, Data);

			edge_record *NextNext = Next->Next;

			v2 Begin2 = DelaunayVertices[NextNext->Origin];
			v2 End2   = DelaunayVertices[ReverseEdge(NextNext)->Origin];

			v2 Delta2 = RightBisector(Begin2, End2);

			i32 Future = LeftVoronoiVertex(NextNext);

			YASSERT(Future != Border);

			Last = Temp;
			New = Add(VoronoiVertices[Future], Scale(100, Delta2));

		}

		entity_id Triangle = NextEntity(Test);
		tri_data Data = (tri_data)
			{
				.Points =
				{
					DelaunayVertices[Origin],
					Last,
					New
				},
				.Color = ColorOfId(Origin)
			};
		AddTriangle(Test, Triangle, Data);
		Last = New;

		// TODO: mark QuadEdge(Next) as consumed
		Current = Next;
	}
	while (Current != Start);
	NumVertices = 1;
#endif



	v2 VoronoiCenter = Scale(1.0/NumVertices, Accum);
	return VoronoiCenter;
}

FWDDECLARE(lloyd_data);
FWDDECLARE(lloyd_common);

struct lloyd_common
{
	rect BoundingBox;
	v2 *DelaunayVertices;
	v2 *VoronoiVertices;
	i32 BorderIdx;
	v2 *Updated;
	edge_record **RecordWithOrigin;
};

struct lloyd_data
{
	lloyd_common *Common;
	idx Start;
	idx End;

	idx volatile *TasksCompleted;
};

TASK_CALLBACK_FN(LloydWrapper)
{
	lloyd_data Data = *(lloyd_data volatile *)Arg;
	lloyd_common *Common = Data.Common;
	for (idx Origin = Data.Start;
	     Origin < Data.End;
	     ++Origin)
	{
		Common->Updated[Origin] = LloydStep(Common->BoundingBox,
						    Common->RecordWithOrigin[Origin],
						    Common->DelaunayVertices,
						    Common->VoronoiVertices,
						    Common->BorderIdx);
	}
	InterlockedPreInc(Data.TasksCompleted);
}

void
ComputeLloydStep
(
	memory_arena *Scratch,
	rect BoundingBox,
	idx NumPoints,
	v2 Points[static NumPoints],
	v2 Updated[static NumPoints],
	idx NumEdges,
	quad_edge QEdges[static NumEdges]
)
{
	idx SupVertex = 2 * NumPoints - 5 + 1;  // + 1 for the border vertex
	v2 *Vertices = PushArray(Scratch, SupVertex, *Vertices);

	i32 NumVertices = 0;
	i32 BorderVertex = -1;

	for (idx Edge = 0;
	     Edge < NumEdges;
	     ++Edge)
	{
		quad_edge *Current = &QEdges[Edge];
		if (UsedEdge(&Current->Edges[0]))
		{
			NumVertices = AssociateIndex(Current,
						     NumVertices,
						     &BorderVertex,
						     Points,
						     Vertices);
			YASSERT(NumVertices <= SupVertex);
		}
	}

	edge_record **RecordWithOrigin = PushZeroArray(Scratch, NumPoints, *RecordWithOrigin);
	idx volatile Finished = 0;
	lloyd_common Common = { BoundingBox, Points, Vertices, BorderVertex,
		                Updated, RecordWithOrigin };
	idx RecordsFound = 0;
	for (idx Edge = 0;
	     Edge < NumEdges;
	     ++Edge)
	{
		quad_edge *Current = &QEdges[Edge];
		if (UsedEdge(&Current->Edges[0]))
		{
			edge_record *Edge[2] = {&Current->Edges[0], &Current->Edges[2]};
			for (idx Idx = 0;
			     Idx < 2;
			     ++Idx)
			{
				i32 Origin = Edge[Idx]->Origin;
				if (!RecordWithOrigin[Origin])
				{
					RecordsFound += 1;
					RecordWithOrigin[Origin] = Edge[Idx];
				}
			}
		}
	}

	YASSERT(RecordsFound == NumPoints);

	idx NumTasks = 0;
	idx volatile TaskCompleted = 0;
	idx CurrentPoint = 0;
	while (CurrentPoint < NumPoints)
	{
		idx End = Min(NumPoints, CurrentPoint + LLOYD_ASYNC_STEP_SIZE);
		NumTasks += 1;
		lloyd_data Data = { &Common, CurrentPoint, End, &TaskCompleted };
		while (!PushTask(LloydWrapper, sizeof(Data), alignof(Data), &Data))
		{
			Wait();
		}
		CurrentPoint = End;
	}




	while (InterlockedRead(&TaskCompleted) != NumTasks)
	{
		Wait();
	}

	YASSERT(BorderVertex != -1);
}

void
DrawVoronoiRegion
(
	rect BoundingBox,
	edge_record *Start,
	v2 *DelaunayVertices,
	v2 *VoronoiVertices,
	i32 BorderIdx,
	v4 Color,
	entity_test *Test
)
{
	v2 Accum = V(0, 0);
	idx NumVertices = 0;

	i32 Origin = Start->Origin;

#define Voronoi(Edge) (LeftVoronoiVertex(Edge))
#define IsBorder(Edge) (Voronoi(Edge) == BorderIdx)
#define IsInside(Edge) (!IsBorder(Edge)					\
			&& PointInsideRect(VoronoiVertices[Voronoi(Edge)], BoundingBox))
#define AddVertex(Vec) do {						\
		v2 Copy = (Vec);					\
		if (0) {						\
			entity_id Hex = NextEntity(Test);		\
			AddPosition(Test, Hex, Copy);			\
			AddHex(Test, Hex, (hex_data) { .Color = V(1, 0, 0, 1), .Dim = V(0.01, 0.01), .Offset = V(0, 0)}); \
		}							\
		entity_id Triangle = NextEntity(Test);			\
		AddTriangle(Test, Triangle, (tri_data)			\
			    {						\
				    .Points = { DelaunayVertices[Origin], Last, Copy }, \
				    .Color = Color,			\
			    }						\
			   );						\
		Accum = Add(Accum, Copy);				\
		NumVertices += 1;					\
		Last = Copy;						\
	} while (0)

#define AddBorderVertices(Start, End) do {				\
			idx Begin = (Start);				\
			idx Stop = (End);				\
			while (Begin != Stop)				\
			{						\
				AddVertex(BorderVertices[Begin]);	\
				Begin = ModInc(Begin, 4);		\
			}						\
		} while (0)

	edge_record *Current = Start;
	while (!IsInside(Current))
	{
		Current = Current->Next;
		if (Current == Start)
		{
			break;
		}
	}

	v2 BorderVertices[] =
		{
			V(BoundingBox.MaxX, BoundingBox.MaxY),
			V(BoundingBox.MinX, BoundingBox.MaxY),
			V(BoundingBox.MinX, BoundingBox.MinY),
			V(BoundingBox.MaxX, BoundingBox.MinY)
		};

	bool StartInside;

	if (IsInside(Current))
	{
		StartInside = 1;
	}
	else
	{
		if (IsBorder(Current))
		{
			Current = Current->Next;
		}

		StartInside = 0;
	}

	i32 ExitBorder = -1;
	edge_record *First = Current;
	bool Inside = StartInside;
	YASSERT(!IsBorder(Current));
	v2 Last = VoronoiVertices[Voronoi(Current)];
	do
	{
		edge_record *Next = Current->Next;
		idx NextIdx = Voronoi(Next);

		if (NextIdx == BorderIdx)
		{
			edge_record *Border = Next;
			Next = Next->Next;

			YASSERT(!IsBorder(Next));

			b32 StartInside = Inside;
			b32 EndInside   = IsInside(Next);

			v2 PreBorderVertex = VoronoiVertices[Voronoi(Current)];
			v2 PostBorderVertex = VoronoiVertices[Voronoi(Next)];

			v2 Dir1 = LeftBisector(DelaunayVertices[Border->Origin],
					       DelaunayVertices[ReverseEdge(Border)->Origin]);

			v2 Dir2 = RightBisector(DelaunayVertices[Next->Origin],
						DelaunayVertices[ReverseEdge(Next)->Origin]);

			v2 StartVec = Add(PreBorderVertex, Dir1);
			v2 EndVec   = Add(PostBorderVertex, Dir2);

			idx NumIntersections;
			v2 TempIntersections[2];
			idx TempBorders[2];

			v2 Intersections[2] =
				{ V(SIGNALINGNAN, SIGNALINGNAN), V(SIGNALINGNAN, SIGNALINGNAN) };
			idx Borders[2] = { -20, -20 };

			if (StartInside)
			{
				NumIntersections = FindIntersections(TempIntersections,
								     TempBorders,
								     PreBorderVertex,
								     StartVec,
								     BoundingBox,
								     1 /* IsRay */);
				YASSERT(NumIntersections == 1);
				Intersections[0] = TempIntersections[0];
				Borders[0] = TempBorders[0];
			}

			if (EndInside)
			{
				NumIntersections = FindIntersections(TempIntersections,
								     TempBorders,
								     PostBorderVertex,
								     EndVec,
								     BoundingBox,
								     1);

				YASSERT(NumIntersections == 1);
				Intersections[1] = TempIntersections[0];
				Borders[1] = TempBorders[0];
			}

			if (StartInside && EndInside)
			{
				AddVertex(Intersections[0]);
				AddBorderVertices(Borders[0], Borders[1]);
				AddVertex(Intersections[1]);
				AddVertex(PostBorderVertex);
				ExitBorder = -1;
			}
			else if (!StartInside && EndInside)
			{
				AddBorderVertices(ExitBorder, Borders[1]);
				AddVertex(Intersections[1]);
				AddVertex(PostBorderVertex);
				ExitBorder = -1;
			}
			else if (StartInside && !EndInside)
			{
				AddVertex(Intersections[0]);
				ExitBorder = Borders[0];
			}
			else
			{
				YASSERT(StartInside == 0);
				YASSERT(EndInside == 0);
			}

			Inside = EndInside;

			YASSERT(Inside ? (ExitBorder == -1) : (ExitBorder != -1));
		}
		else if (Inside)
		{
			if (IsInside(Next))
			{
				AddVertex(VoronoiVertices[NextIdx]);
			}
			else
			{
				v2 Intersections[2];
				idx Borders[2];

				idx NumIntersections = FindIntersections(Intersections,
									 Borders,
									 Last,
									 VoronoiVertices[NextIdx],
									 BoundingBox, 0);
				YASSERT(NumIntersections == 1);

				AddVertex(Intersections[0]);
				ExitBorder = Borders[0];
				Inside     = 0;
			}
		}
		else
		{
			v2 Intersections[2];
			idx Borders[2];

			idx NumIntersections = FindIntersections(Intersections,
								 Borders,
								 VoronoiVertices[Voronoi(Current)],
								 VoronoiVertices[NextIdx],
								 BoundingBox, 0);

			YASSERT(NumIntersections <= 2);
			YASSERT(NumIntersections >= 0);

			if (NumIntersections != 0)
			{
				if (!StartInside)
				{
					YASSERT(NumIntersections == 2);
					YASSERT(Borders[0] != Borders[1]);
					// last is not really defined here
					// so we just set last to something that
					// does not cause visual artifacts
					Last = BorderVertices[Borders[0]];
					AddBorderVertices(Borders[0], Borders[1]);
					AddVertex(Intersections[0]);
					AddVertex(Intersections[1]);
					ExitBorder = Borders[1];

					// now there is no difference anymore
					StartInside = 1;
				}
				else
				{
					YASSERT(ExitBorder != -1);

					AddBorderVertices(ExitBorder, Borders[0]);

					if (NumIntersections == 2)
					{
						YASSERT(!IsInside(Next));
						AddVertex(Intersections[0]);
						AddVertex(Intersections[1]);
						ExitBorder = Borders[1];
					}
					else if (NumIntersections == 1)
					{
						YASSERT(IsInside(Next));

						AddVertex(Intersections[0]);
						AddVertex(VoronoiVertices[NextIdx]);
						ExitBorder = -1;
						Inside     = 1;
					}
				}
			}
			else
			{
				YASSERT(!IsInside(Next));
			}
		}

		Current = Next;
	}
	while (Current != First);

#undef IsBorder
#undef IsInside
#undef Voronoi
#undef AddVertex
#undef AddBorderVertices
}

void
DrawVoronoiRegions
(
	memory_arena *Scratch,
	rect BoundingBox,
	idx NumPoints,
	v2 Points[static NumPoints],
	f32 Heights[static NumPoints],
	idx NumEdges,
	quad_edge QEdges[static NumEdges],
	entity_test *Test
)
{
	idx SupVertex = 2 * NumPoints - 5 + 1;  // + 1 for the border vertex
	v2 *Vertices = PushArray(Scratch, SupVertex, *Vertices);

	i32 NumVertices = 0;
	i32 BorderVertex = -1;

	for (idx Edge = 0;
	     Edge < NumEdges;
	     ++Edge)
	{
		quad_edge *Current = &QEdges[Edge];
		if (UsedEdge(&Current->Edges[0]))
		{
			NumVertices = AssociateIndex(Current,
						     NumVertices,
						     &BorderVertex,
						     Points,
						     Vertices);
			YASSERT(NumVertices <= SupVertex);
		}
	}

	edge_record **RecordWithOrigin = PushZeroArray(Scratch, NumPoints, *RecordWithOrigin);

	idx RecordsFound = 0;
	for (idx Edge = 0;
	     Edge < NumEdges;
	     ++Edge)
	{
		quad_edge *Current = &QEdges[Edge];
		if (UsedEdge(&Current->Edges[0]))
		{
			edge_record *Edge[2] = {&Current->Edges[0], &Current->Edges[2]};
			for (idx Idx = 0;
			     Idx < 2;
			     ++Idx)
			{
				i32 Origin = Edge[Idx]->Origin;
				if (!RecordWithOrigin[Origin])
				{
					RecordsFound += 1;
					RecordWithOrigin[Origin] = Edge[Idx];
				}
			}
		}
	}

	YASSERT(RecordsFound == NumPoints);

	for (idx Origin = 0;
	     Origin < NumPoints;
	     ++Origin)
	{
		DrawVoronoiRegion(BoundingBox,
				  RecordWithOrigin[Origin],
				  Points,
				  Vertices,
				  BorderVertex,
				  ColorOfF32(Heights[Origin], 0, 1),
				  Test);
	}

	YASSERT(BorderVertex != -1);
}

FWDDECLARE(quicksort_data);
struct quicksort_data
{
	idx Low, High;
	v2 *Points;
	bool volatile *Finished;
};

PRIVATE void QuickSortPointsByX(idx Low, idx High, v2 *Points);

PRIVATE TASK_CALLBACK_FN(QuickSortWrapper)
{
	quicksort_data Data = *(quicksort_data volatile *)Arg;
	QuickSortPointsByX(Data.Low, Data.High, Data.Points);
	InterlockedWrite(Data.Finished, 1);
}

PRIVATE idx
SelectMedian
(
	v2 *Points,
	idx A,
	idx B,
	idx C
)
{
	idx Median;
	if (Points[A].X < Points[B].X)
	{
		if (Points[B].X < Points[C].X)
		{
			Median = B;
		}
		else if (Points[A].X < Points[C].X)
		{
			Median = C;
		}
		else
		{
			Median = A;
		}
	}
	else
	{
		if (Points[A].X < Points[C].X)
		{
			Median = A;
		}
		else if (Points[B].X < Points[C].X)
		{
			Median = C;
		}
		else
		{
			Median = B;
		}
	}

	return Median;
}

void
QuickSortPointsByX
(
	idx Low,
	idx High,
	v2 *Points
)
{
#define SWAP(P1, P2) do { typeof(P1) Tmp = (P1); (P1) = (P2); (P2) = Tmp; } while (0)
	// todo: sort points Low, ..., High
	if (Low < High)
	{
		idx PivotPos = Low - 1;

		idx Middle = (High + Low) / 2;
		idx ActualPivot = SelectMedian(Points, Low, Middle, High);

		SWAP(Points[ActualPivot], Points[High]);

		f32 Pivot = Points[High].X;
		f32 PivotY = Points[High].Y;

		for (idx Current = Low;
		     Current < High;
		     ++Current)
		{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
			if ((Points[Current].X < Pivot)
			    || (Points[Current].X == Pivot &&
				Points[Current].Y < PivotY)
			   )
			{
				PivotPos += 1;

				SWAP(Points[Current], Points[PivotPos]);
			}
#pragma GCC diagnostic pop
		}

		PivotPos += 1;
		SWAP(Points[PivotPos], Points[High]);

		idx NumLeftPoints = PivotPos - Low;
		if (NumLeftPoints > QUICKSORT_ASYNC_THRESHOLD)
		{
			bool Finished = 0;
			{
				quicksort_data Data = { Low, PivotPos - 1, Points, &Finished };
				while (!PushTask(QuickSortWrapper, sizeof(Data), alignof(Data), &Data))
				{
					Wait();
				}
			}
			QuickSortPointsByX(PivotPos + 1, High, Points);
			while (!InterlockedRead(&Finished))
			{
				Wait();
			}
		}
		else
		{
			QuickSortPointsByX(Low, PivotPos - 1, Points);
			QuickSortPointsByX(PivotPos + 1, High, Points);
		}
	}
#undef SWAP
}

PRIVATE void
SortPointsByX
(
	idx NumPoints,
	v2 Points[static NumPoints]
)
{
	QuickSortPointsByX(IDXC(0), NumPoints-1, Points);
	for (idx Idx = 0;
	     Idx < NumPoints - 1;
	     ++Idx)
	{
		YASSERTF(Points[Idx].X <= Points[Idx + 1].X,
			 "%f < %f",
			 Points[Idx].X, Points[Idx + 1].X);
	}
}

edge_allocator
ComputeDelaunayTriangulation
(
	memory_arena *Scratch,
	idx NumPoints,
	v2 Points[static NumPoints]
)
{
	INSTRUMENT(QuickSort)
	{
		SortPointsByX(NumPoints, Points);
	}

	edge_allocator Alloc;

	if (NumPoints <= 2)
	{
		// probably special case this!
		Alloc = (edge_allocator) {};
	}
	else
	{
		Alloc = EdgeAllocator_FromArena(Scratch, NumPoints);
		edge_pair HullPair = DelaunayStep(&Alloc, Points,
						  0, NumPoints - 1);
	}

	return Alloc;
}
