#include <def.h>

#include <util/arena_allocator.h>
#include <util/memory.h>

#ifdef DEBUG
#include <string.h>
#endif

void
InitializeArena(memory_arena *Arena, udx Size, void *Base)
{
        Arena->Size = Size;
	Arena->Base = (byte *) Base;
	Arena->Used = 0;
	DEBUG_DO(
		Arena->Peak = 0;
		);
}

memory_arena
MemoryArena_FromBuffer
(
	idx Size,
	void *Buf
)
{
	memory_arena Result;
	InitializeArena(&Result, Size, Buf);
	return Result;
}

void *
TryPushSize(memory_arena *Arena, udx Size, u32 Alignment)
{
	void *Result;
	void *Current = Arena->Base + Arena->Used;

	void *NextPossible = NEXTALIGNED(Current, Alignment);

	idx Diff = (idx) ((ptr) NextPossible - (ptr) Arena->Base);

	YASSERTF(Diff >= 0, "%ld", Diff);

	udx Used = (udx) Diff;

	if (Used + Size < Arena->Size)
	{
		Result = Arena->Base + Used;
		Arena->Used = Used + Size;
		DEBUG_DO(
			if (Arena->Peak < Arena->Used) Arena->Peak = Arena->Used;
			);
	}
	else
	{
		Result = NULL;
	}

	return Result;
}

void *
PushSize(memory_arena *Arena, udx Size, u32 Alignment)
{
	void *Result = TryPushSize(Arena, Size, Alignment);

	YASSERTF(Result, "Trying to allocate %lu bytes when only %lu are left.",
		 Size, Arena->Size - Arena->Used);

	return Result;
}

void *
TryPushZero(memory_arena *Arena, udx Size, u32 Alignment)
{
	void *Ptr = TryPushSize(Arena, Size, Alignment);
	if (Ptr) ZeroMemory(Size, Ptr);
	return Ptr;
}

void *
PushZero(memory_arena *Arena, udx Size, u32 Alignment)
{
	void *Ptr = PushSize(Arena, Size, Alignment);
	ZeroMemory(Size, Ptr);
	return Ptr;
}

void
FreeAll(memory_arena *Arena)
{
	DEBUG_DO(
		SetMemory(Arena->Used, 0xDE, (byte *) Arena->Base);
		);

	Arena->Used = 0;
}

memory_arena
SplitArena(memory_arena *Arena, udx Size)
{
	/*         Arena               Arena       NewArena
	 * Base->  +----+              +----+
	 *         |    |	       |    |
	 *         |    |	       |    |
	 *         |    |	       |    |
	 *         |    |	       |    |
	 *         |    |        ~>    |    |
	 *         |    |	       |    |
	 *         |    |	       |    |
	 *         |    |	       |    |
	 *         +----+ -\	       +----+      +----+
	 *         |    |  |			   |    |
	 *         |    |  | Size		   |    |
	 *         |    |  |			   |    |
	 *         |    |  |			   |    |
	 *         +----+ -/			   +----+
	 */

	memory_arena Result;

	if (Size <= Arena->Size - Arena->Used)
	{
		InitializeArena(&Result, Size, Arena->Base + (Arena->Size - Size));
		Arena->Size -= Size;
	} else {
		INVALIDCODEPATH();
	}

	return Result;
}

memory_arena
SplitRest
(
	memory_arena *Arena
)
{
	memory_arena Result = SplitArena(Arena, Arena->Size - Arena->Used);
	return Result;
}

void
AbsorbEmptyArena(memory_arena *Arena, memory_arena ToAbsorb)
{
	YASSERT(Arena->Base + Arena->Size == ToAbsorb.Base);
        FreeAll(&ToAbsorb);

	Arena->Size += ToAbsorb.Size;
}
