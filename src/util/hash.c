#include <def.h>
#include <util/hash.h>

u32 CONSTFN
Hash_Murmur3_32(u32 Val)
{
	u32 Result = Val;

	Result ^= Result >> 16;
	Result *= 0x85ebca6b;
	Result ^= Result >> 13;
	Result *= 0xc2b2ae35;
	Result ^= Result >> 16;

	return Result;
}

u64 CONSTFN
Hash_Murmur3_64(u64 Val)
{
	u64 Result = Val;

	Result ^= Result >> 33;
	Result *= U64C(0xff51afd7ed558ccd);
	Result ^= Result >> 33;
	Result *= U64C(0xc4ceb9fe1a85ec53);
	Result ^= Result >> 33;

	return Result;
}

u64 CONSTFN
Hash_Fibonacci(u64 Val, u64 Shift)
{
	u64 Hash = Val ^ (Val >> Shift);

	u64 Result = (Hash * U64C(11400714819323198485)) >> Shift;

	return Result;
}
