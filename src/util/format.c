#include <def.h>
#include <util/string.h>

FWDDECLARE(scan_result);
FWDDECLARE(frac_format_spec); // text_, int_

static struct frac_format_spec
{
	u32 MaxSize;   // 0 = ignored
	u32 Precision; // 0 = ignored
	u32 Min;       // 0 = ignored
} const FRAC_SPEC_NONE = { 0, 0, 0 };

struct scan_result
{
	u64 CharsSkipped;
	b32 Found;
};

static u64 DigitArray[] =
{
	U64C(1),
	U64C(10),
	U64C(100),
	U64C(1000),
	U64C(10000),
	U64C(100000),
	U64C(1000000),
	U64C(10000000),
	U64C(100000000),
	U64C(1000000000),
	U64C(10000000000),
	U64C(100000000000),
	U64C(1000000000000),
	U64C(10000000000000),
	U64C(100000000000000),
	U64C(1000000000000000),
	U64C(10000000000000000),
	U64C(100000000000000000),
	U64C(1000000000000000000),
	U64C(10000000000000000000),
};

static char HexDigits[16] =
{
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', 'A', 'B',
	'C', 'D', 'E', 'F',
};

PRIVATE u32
NumDigitsOf(u64 Val)
{
	u32 NumDigits;
	for (NumDigits = 1;
	     NumDigits < ARRAYCOUNT(DigitArray);
	     ++NumDigits)
	{
		if (Val < DigitArray[NumDigits]) break;
	}
	return NumDigits;
}

PRIVATE format_result
CombineFormatResults(format_result A, format_result B)
{
	format_result Result;
	Result.RequiredSize = A.RequiredSize + B.RequiredSize;
	Result.WrittenSize  = A.WrittenSize  + B.WrittenSize;
	return Result;
}

PRIVATE scan_result
FormatScan(str_view View, char ToSearch)
{
	scan_result Result = { .Found = 0, .CharsSkipped = 0 };
	b32 Escaped = 0;
	char const *End = &View.Data[View.Size];
	for (char const *Current = View.Data;
	     Current != End;
	     ++Current, ++Result.CharsSkipped)
	{
		char C = *Current;

		if (C == '\\')
		{
			Escaped = !Escaped;
		}
		else if ((!Escaped) && (C == ToSearch))
		{
			Result.Found = 1;
			return Result;
		} else {
			Escaped = 0;
		}
	}

	return Result;
}

PRIVATE u64
PrintChar(write_target Target, char C)
{
	WriteOut(Target, sizeof(C), (byte *) &C);

	return sizeof(C);
}

PRIVATE u64
PrintPtr(write_target Target, void const *Ptr)
{
	ptr Mem = (ptr) Ptr;

#define Size (sizeof(Ptr) * 2 + 2)

	char Digits[Size];

	idx Start = 2;
	idx Offset = 0;
	b32 Started = 0;

	if (Ptr)
	{
		for (udx I = 0;
		     I < sizeof(Ptr) * 2;
		     ++I)
		{
			udx Shift = (sizeof(Ptr) * 2 - I - 1) * 4;
			udx Mask  = UDXC(0xF) << Shift;
			udx Val   = (Mem & Mask) >> Shift;

			YASSERTF(Val < 16, "%lu", Val);

			char C = HexDigits[Val];
			/* if (Val > 9) */
			/* { */
			/* 	C = (Val - 10) + 'A'; */
			/* } else { */
			/* 	C = Val + '0'; */
			/* } */

			if (Val)
			{
				Digits[Start + Offset++] = C;
				Started = 1;
			} else if (!Started){
				Start += 1;
			}
		}
	}
	else
	{
		Start = Size - 1;
		Digits[Start] = '0';
	}



	Digits[Start-2] = '0';
	Digits[Start-1] = 'x';

	u64 CharsWritten = Size - (Start - 2);

	WriteOut(Target, CharsWritten, (byte *) &Digits[Start-2]);


#undef Size

	return CharsWritten;
}

PRIVATE format_result
PrintU64Into(str *Buffer, u64 Num)
{
	format_result Result = {};

	u32 Required = NumDigitsOf(Num);

	Result.RequiredSize = Required;
	u64 EmptySize = Buffer->Capacity - Buffer->Size;

	if (Required <= EmptySize)
	{
		Result.WrittenSize = Required;
		Buffer->Size += Required;

		char *Current = &Buffer->Data[Buffer->Size];
		do
		{
			char Mod = (char) (Num % 10);

			char C = Mod + '0';

			*(--Current) = C;

			Num  = Num / 10;
		} while (Num != 0);

	}

	return Result;
}

PRIVATE format_result
PrintTimeInto(str *Buffer, u64 Time)
{
	u64 NanoSeconds, MicroSeconds, MilliSeconds, Seconds, Minutes, Hours;

	NanoSeconds = Time % 1000;
	Time /= 1000;
	MicroSeconds = Time % 1000;
	Time /= 1000;
	MilliSeconds = Time % 1000;
	Time /= 1000;
	Seconds = Time % 60;
	Time /= 60;
	Minutes = Time % 60;
	Time /= 60;
	Hours = Time;

	format_result Result = FormatInto(Buffer, VIEWC("{u64}:{u64}:{u64}.{u64}-{u64}'{u64}"),
					  Hours, Minutes, Seconds,
					  MilliSeconds, MicroSeconds, NanoSeconds);

	return Result;
}

PRIVATE format_result
PrintU32Into(str *Buffer, u32 Num)
{
	return PrintU64Into(Buffer, (u64) Num);
}

PRIVATE format_result
PrintI64Into(str *Buffer, i64 I)
{
	if (I < 0)
	{
		u64 U = (u64)(-I);

		char *MinusToBe = &Buffer->Data[Buffer->Size];
		Buffer->Size += 1;

		format_result Result = PrintU64Into(Buffer, U);

		if (Result.WrittenSize)
		{
			*MinusToBe = '-';
			Result.WrittenSize += 1;
		}
		else
		{
			Buffer->Size -= 1;
		}
		Result.RequiredSize += 1;
		return Result;
	}
	else
	{
		return PrintU64Into(Buffer, (u64) I);
	}
}

PRIVATE format_result
PrintI32Into(str *Buffer, i32 I)
{
	return PrintI64Into(Buffer, (i64) I);
}

PRIVATE format_result
PrintViewInto(str *Buffer, str_view View)
{
	u64 EmptySize = Buffer->Capacity - Buffer->Size;

	format_result Result;
	Result.RequiredSize = (u32) View.Size;
	Result.WrittenSize  = (u32) Min(View.Size, EmptySize);

	MoveMemory(Result.WrittenSize, (byte const *) View.Data, (byte *) (Buffer->Data + Buffer->Size));

	Buffer->Size += Result.WrittenSize;

	return Result;
}

PRIVATE format_result
PrintCharInto(str *Buffer, char C)
{
	format_result Result;
	Result.RequiredSize = 1;
	if (Buffer->Size != Buffer->Capacity)
	{
		Result.WrittenSize = 1;
		Buffer->Data[Buffer->Size++] = C;
	}
	else
	{
		Result.WrittenSize = 0;
	}

	return Result;
}

PRIVATE format_result
PrintF32Into(str *Buffer, f32 Float, frac_format_spec Spec)
{
	format_result Result = {};
	if (Float < 0)
	{
		Result = CombineFormatResults(Result, PrintCharInto(Buffer, '-'));
		Float = -Float;
	}

	// TODO: this isnt actually safe; Float could be bigger than U32_MAX
	u32 Whole = (u32) Float;
	f32 Frac  = Float - (f32) Whole;

	Result = CombineFormatResults(Result, PrintU32Into(Buffer, Whole));
	Result = CombineFormatResults(Result, PrintCharInto(Buffer, '.'));
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

	// TODO: this is bad and slow and wrong
	// it should definitely change if used
	// in the actual game
	if (Frac == 0.0 || Frac == -0.0)
	{
		Result = CombineFormatResults(Result, PrintCharInto(Buffer, '0'));
	}
	else
	{
		// since this isnt very acurate anyways,
		// we just print up to 5 places

		u32 MaxPrecision = 5;

		if (Spec.Precision)
		{
			MaxPrecision = Spec.Precision;
		}

		idx NumWritten = 0;
		YASSERTF(Frac > 0, "%f", Frac);
                do
		{
			Frac = 10 * Frac;
			u32 Whole = (u32) Frac;
			Frac = Frac - (f32) Whole;
			YASSERTF(Whole < 10, "%u", Whole);
			Result = CombineFormatResults(Result, PrintU32Into(Buffer, Whole));
			NumWritten += 1;
		}
		while (Frac > 0 && NumWritten < MaxPrecision);
	}
#pragma GCC diagnostic pop

	return Result;
}

PRIVATE format_result
PrintPtrInto(str *Buffer, void const *Ptr)
{
	format_result Result = {};

	ptr Mem = (ptr) Ptr;

	u64 EmptySize = Buffer->Capacity - Buffer->Size;
	u64 Required = (sizeof(Ptr) * 2 + 2);
	Result.RequiredSize = (u32) Required;
	if (Required <= EmptySize)
	{
		Result.WrittenSize = (u32) Required;
		char *Digits = &Buffer->Data[Buffer->Size];

		idx Start = 2;
		idx Offset = 0;
		b32 Started = 0;

		for (udx I = 0;
		     I < sizeof(Ptr) * 2;
		     ++I)
		{
			udx Shift = (sizeof(Ptr) * 2 - I - 1) * 4;
			udx Mask  = UDXC(0xF) << Shift;
			udx Val   = (Mem & Mask) >> Shift;

			YASSERTF(Val < 16, "%lu", Val);

			char C = HexDigits[Val];
			/* if (Val > 9) */
			/* { */
			/* 	C = (Val - 10) + 'A'; */
			/* } else { */
			/* 	C = Val + '0'; */
			/* } */

			if (Val)
			{
				Digits[Start + Offset++] = C;
				Started = 1;
			} else if (!Started){
				Start += 1;
			}
		}

		Digits[Start-2] = '0';
		Digits[Start-1] = 'x';

		Buffer->Size += Required;
	}

	return Result;
}

PRIVATE format_result
PrintVectorInto(str *Buffer, idx NumElements,
		f32 Array[static NumElements])
{
	format_result Result = {};
	Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC("{ ")));

	YASSERTF(NumElements >= 1, "%ld", NumElements);

	Result = CombineFormatResults(Result, PrintF32Into(Buffer, Array[0], FRAC_SPEC_NONE));

	for (idx I = 1;
	     I < NumElements;
	     ++I)
	{
		Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC(", ")));
                Result = CombineFormatResults(Result, PrintF32Into(Buffer, Array[I], FRAC_SPEC_NONE));
	}

	Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC(" }")));

	return Result;
}

PRIVATE format_result
PrintIVectorInto(str *Buffer, idx NumElements,
		 i32 Array[static NumElements])
{
	format_result Result = {};

	YASSERTF(NumElements >= 1, "%ld", NumElements);

	Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC("{ ")));

	Result = CombineFormatResults(Result, PrintI32Into(Buffer, Array[0]));

	for (idx I = 1;
	     I < NumElements;
	     ++I)
	{
		Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC(", ")));
                Result = CombineFormatResults(Result, PrintI32Into(Buffer, Array[I]));
	}

	Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC(" }")));

	return Result;
}

PRIVATE u64
PrintU32(write_target Target, u32 U)
{
	char Digits[10];
	idx Current = 10;

	do
	{
		char Mod = (char) (U % 10);

		char C = Mod + '0';

		Digits[--Current] = C;

		U  = U / 10;
	} while (U != 0);

	WriteOut(Target, (10 - Current) * sizeof(*Digits), (byte *) &Digits[Current]);

	return (10 - Current);
}

PRIVATE u64
PrintI32(write_target Target, i32 I)
{
	u64 CharsWritten;

	if (I >= 0)
	{
		// we double negate here since Abs(I32_MIN) > Abs(I32_MAX)
		// i.e. I = -I, when I < 0, could overflow
		I = -I;
		CharsWritten = 0;
	}
	else
	{
		CharsWritten = PrintChar(Target, '-');
	}

	CharsWritten += PrintU32(Target, -I);

	return CharsWritten;
}

PRIVATE u64
PrintU64(write_target Target, u64 U)
{
	char Digits[20];
	idx Current = 20;

	do
	{
		char Mod = (char) (U % 10);

		char C = Mod + '0';

		Digits[--Current] = C;

		U  = U / 10;
	} while (U != 0);

	WriteOut(Target, (20 - Current) * sizeof(*Digits), (byte *) &Digits[Current]);

	return (20 - Current);
}

PRIVATE u64
PrintTime(write_target Target, u64 Time)
{
	u64 NanoSeconds, MicroSeconds, MilliSeconds, Seconds, Minutes, Hours;

	NanoSeconds = Time % 1000;
	Time /= 1000;
	MicroSeconds = Time % 1000;
	Time /= 1000;
	MilliSeconds = Time % 1000;
	Time /= 1000;
	Seconds = Time % 60;
	Time /= 60;
	Minutes = Time % 60;
	Time /= 60;
	Hours = Time;

	u64 Result = Format(Target, VIEWC("{u64}:{u64}:{u64}.{u64}-{u64}'{u64}"),
			    Hours, Minutes, Seconds,
			    MilliSeconds, MicroSeconds, NanoSeconds);

	return Result;
}

PRIVATE u64
PrintI64(write_target Target, i64 I)
{
	u64 CharsWritten;

	if (I >= 0)
	{
		// we double negate here since Abs(I64_MIN) > Abs(I64_MAX)
		// i.e. I = -I, when I < 0, could overflow
		I = -I;
		CharsWritten = 0;
	}
	else
	{
		CharsWritten = PrintChar(Target, '-');
	}

	CharsWritten += PrintU64(Target, -I);

	return CharsWritten;
}

PRIVATE u64
PrintView(write_target Target, str_view View)
{
	u64 CharsWritten = View.Size;
	WriteOut(Target, View.Size * sizeof(*View.Data), (byte const *) View.Data);
	return CharsWritten;
}

PRIVATE u64
PrintF32(write_target Target, f32 F, frac_format_spec Spec)
{
	u64 CharsWritten = 0;
	if (F < 0)
	{
		CharsWritten += PrintChar(Target, '-');
		F = -F;
	}

	// TODO: this isnt actually safe; F could be bigger than U32_MAX
	u32 Whole = (u32) F;
	f32 Frac  = F - (f32) Whole;

	CharsWritten += PrintU32(Target, Whole);
	CharsWritten += PrintChar(Target, '.');
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

	// TODO: this is bad and slow and wrong
	// it should definitely change if used
	// in the actual game
	if (Frac == 0.0 || Frac == -0.0)
	{
		CharsWritten += PrintChar(Target, '0');
	} else {
		// since this isnt very acurate anyways,
		// we just print up to 5 places

		// this is probably not okn
		u32 MaxPrecision = 5;
		if (Spec.Precision)
		{
			MaxPrecision = Spec.Precision;
		}

		idx NumWritten = 0;
		YASSERTF(Frac > 0, "%f", Frac);
                do
		{
			Frac = 10 * Frac;
			u32 Whole = (u32) Frac;
			Frac = Frac - (f32) Whole;
			YASSERTF(Whole < 10, "%u", Whole);
			CharsWritten += PrintU32(Target, Whole);
			NumWritten += 1;
		} while (Frac > 0 && NumWritten < MaxPrecision);
	}
#pragma GCC diagnostic pop

	return CharsWritten;
}

PRIVATE u64
PrintIVector(write_target Target, idx NumElements, i32 Array[static NumElements])
{
	u64 CharsWritten = 0;
	CharsWritten += PrintView(Target, VIEWC("{ "));

	YASSERTF(NumElements >= 1, "%ld", NumElements);

	CharsWritten += PrintI32(Target, Array[0]);

	for (idx I = 1;
	     I < NumElements;
	     ++I)
	{
		CharsWritten += PrintView(Target, VIEWC(", "));
                CharsWritten += PrintI32(Target, Array[I]);
	}

	CharsWritten += PrintView(Target, VIEWC(" }"));

	return CharsWritten;
}

PRIVATE u64
PrintVector(write_target Target, idx NumElements, f32 Array[static NumElements])
{
	u64 CharsWritten = 0;
	CharsWritten += PrintView(Target, VIEWC("{ "));

	YASSERTF(NumElements >= 1, "%ld", NumElements);

	CharsWritten += PrintF32(Target, Array[0], FRAC_SPEC_NONE);

	for (idx I = 1;
	     I < NumElements;
	     ++I)
	{
		CharsWritten += PrintView(Target, VIEWC(", "));
                CharsWritten += PrintF32(Target, Array[I], FRAC_SPEC_NONE);
	}

	CharsWritten += PrintView(Target, VIEWC(" }"));

	return CharsWritten;
}

PRIVATE format_result
PrintFormattedInto(str *Buffer, str_view Format, va_list Args)
{
	format_result Result = {};

	if (ViewEq(Format, VIEWC("str")))
	{
		Result = PrintViewInto(Buffer, va_arg(Args, str_view));
	}
	else if (ViewEq(Format, VIEWC("i32")))
	{
		Result = PrintI32Into(Buffer, va_arg(Args, i32));
	}
	else if (ViewEq(Format, VIEWC("f32")))
	{
		// when calling a function all varargs arguments get
		// standard promoted; in particular f32 gets promoted
		// to f64. This is why we get a f64 arg here and
		// convert it back to f32.
		Result = PrintF32Into(Buffer, (f32)va_arg(Args, f64), FRAC_SPEC_NONE);
	}
	else if (ViewEq(Format, VIEWC("u32")))
	{
		Result = PrintU32Into(Buffer, va_arg(Args, u32));
	}
	else if (ViewEq(Format, VIEWC("u64")))
	{
		Result = PrintU64Into(Buffer, va_arg(Args, u64));
	}
	else if (ViewEq(Format, VIEWC("i64")))
	{
		Result = PrintI64Into(Buffer, va_arg(Args, i64));
	}
	else if (ViewEq(Format, VIEWC("ptr")))
	{
		Result = PrintPtrInto(Buffer, va_arg(Args, void const *));
	}
	else if (ViewEq(Format, VIEWC("time")))
	{
		Result = PrintTimeInto(Buffer, va_arg(Args, u64));
	}
	else if (ViewEq(Format, VIEWC("char[*]")))
	{
		char const *CStr = va_arg(Args, char const *);
		Result = PrintViewInto(Buffer, ViewOf(CStr));
	}
	else if (Format.Size == 2 && Format.Data[0] == 'v')
	{
		idx Second = Format.Data[1] - '0';
		switch (Second)
		{
			break;case 2:
			{
				v2 V = va_arg(Args, v2);
				Result = PrintVectorInto(Buffer, 2, V.AsArray);
			}
			break;case 3:
			{
				v3 V = va_arg(Args, v3);
				Result = PrintVectorInto(Buffer, 3, V.AsArray);
			}
			break;case 4:
			{
				v4 V = va_arg(Args, v4);
				Result = PrintVectorInto(Buffer, 4, V.AsArray);
			}
			break;default:
			{
				goto not_recognized;
			}
		}
	}
	else if (Format.Size == 3 && Format.Data[0] == 'i' && Format.Data[1] == 'v')
	{
		idx Second = Format.Data[2] - '0';
		switch (Second)
		{
			break;case 2:
			{
				iv2 V = va_arg(Args, iv2);
				Result = PrintIVectorInto(Buffer, 2, V.AsArray);
			}
			break;case 3:
			{
				iv3 V = va_arg(Args, iv3);
				Result = PrintIVectorInto(Buffer, 3, V.AsArray);
			}
			break;case 4:
			{
				iv4 V = va_arg(Args, iv4);
				Result = PrintIVectorInto(Buffer, 4, V.AsArray);
			}
			break;default:
			{
				goto not_recognized;
			}
		}
	}
	else
	{
	not_recognized:
		Result = PrintViewInto(Buffer,  VIEWC("Not recognized specifier '"));
		Result = CombineFormatResults(Result, PrintViewInto(Buffer, Format));
		Result = CombineFormatResults(Result, PrintViewInto(Buffer, VIEWC("'")));
	}

	return Result;
}

PRIVATE u64
PrintFormatted(write_target Target, str_view Format, va_list Args)
{
	u64 CharsWritten;
	if (ViewEq(Format, VIEWC("str")))
	{
		CharsWritten = PrintView(Target, va_arg(Args, str_view));
	}
	else if (ViewEq(Format, VIEWC("i32")))
	{
		CharsWritten = PrintI32(Target, va_arg(Args, i32));
	}
	else if (ViewEq(Format, VIEWC("f32")))
	{
		frac_format_spec Spec = FRAC_SPEC_NONE;
		CharsWritten = PrintF32(Target, (f32)va_arg(Args, f64), Spec);
	}
	else if (ViewEq(Format, VIEWC("u32")))
	{
		CharsWritten = PrintU32(Target, va_arg(Args, u32));
	}
	else if (ViewEq(Format, VIEWC("u64")))
	{
		CharsWritten = PrintU64(Target, va_arg(Args, u64));
	}
	else if (ViewEq(Format, VIEWC("i64")))
	{
		CharsWritten = PrintI64(Target, va_arg(Args, i64));
	}
	else if (ViewEq(Format, VIEWC("ptr")))
	{
		CharsWritten = PrintPtr(Target, va_arg(Args, void const *));
	}
	else if (ViewEq(Format, VIEWC("time")))
	{
		CharsWritten = PrintTime(Target, va_arg(Args, u64));
	}
	else if (ViewEq(Format, VIEWC("char[*]")))
	{
		char const *CStr = va_arg(Args, char const *);
		CharsWritten = PrintView(Target, ViewOf(CStr));
	}
	else if (Format.Size == 2 && Format.Data[0] == 'v')
	{
		idx Second = Format.Data[1] - '0';
		switch (Second)
		{
			break;case 2:
			{
				v2 V = va_arg(Args, v2);
				CharsWritten = PrintVector(Target, 2, V.AsArray);
			}
			break;case 3:
			{
				v3 V = va_arg(Args, v3);
				CharsWritten = PrintVector(Target, 3, V.AsArray);
			}
			break;case 4:
			{
				v4 V = va_arg(Args, v4);
				CharsWritten = PrintVector(Target, 4, V.AsArray);
			}
			break;default:
			{
				goto not_recognized;
			}
		}
	}
	else if (Format.Size == 3 && Format.Data[0] == 'i' && Format.Data[1] == 'v')
	{
		idx Third = Format.Data[2] - '0';
		switch (Third)
		{
			break;case 2:
			{
				iv2 V = va_arg(Args, iv2);
				CharsWritten = PrintIVector(Target, 2, V.AsArray);
			}
			break;case 3:
			{
				iv3 V = va_arg(Args, iv3);
				CharsWritten = PrintIVector(Target, 3, V.AsArray);
			}
			break;case 4:
			{
				iv4 V = va_arg(Args, iv4);
				CharsWritten = PrintIVector(Target, 4, V.AsArray);
			}
			break;default:
			{
				goto not_recognized;
			}
		}
	}
	else
	{
	not_recognized:
		CharsWritten = PrintView(Target,  VIEWC("Not recognized specifier '"));
		CharsWritten += PrintView(Target, Format);
		CharsWritten += PrintView(Target, VIEWC("'"));
	}
	return CharsWritten;
}

u64
Format(write_target Target, str_view FormatStr, ...)
{
	va_list Args;
	u64 CharsSkipped = 0;
	u64 CharsWritten = 0;

	DEFER(va_start(Args, FormatStr), va_end(Args))
	{
		while (CharsSkipped < FormatStr.Size)
		{
			str_view ToGo = SkipChars(CharsSkipped, FormatStr);
			scan_result Scan = FormatScan(ToGo, '{');
			u64 NewChars = Scan.CharsSkipped;
			str_view Normal = Restrict(NewChars, ToGo);
			CharsWritten += PrintView(Target, Normal);

			CharsSkipped += NewChars;

			if (Scan.Found)
			{
				str_view FormatSpecifier = SkipChars(NewChars, ToGo);
				scan_result Scan = FormatScan(FormatSpecifier, '}');
				u64 FormatChars = Scan.CharsSkipped;
				if (Scan.Found)
				{
					str_view Format = Restrict(FormatChars - 1,
								   SkipChars(1, FormatSpecifier));
					CharsWritten += PrintFormatted(Target, Format, Args);
					CharsSkipped += FormatChars + 1; // skip '{' and '}'
				}
				else
				{
					CharsWritten += PrintView(Target, FormatSpecifier);
					CharsSkipped += FormatChars;
				}
			}
		}

		YASSERT(CharsSkipped == FormatStr.Size, "Everything was analyzed");
	}

	return CharsWritten;
}

format_result
FormatInto(str *Buffer, str_view FormatStr, ...)
{
	va_list Args;
	u64 CharsSkipped = 0;

	format_result Result = {0, 0};

	DEFER(va_start(Args, FormatStr), va_end(Args))
	{
		while (CharsSkipped < FormatStr.Size)
		{
			str_view ToGo = SkipChars(CharsSkipped, FormatStr);
			scan_result Scan = FormatScan(ToGo, '{');
			u64 NewChars = Scan.CharsSkipped;
			str_view Normal = Restrict(NewChars, ToGo);
			Result = CombineFormatResults(Result, PrintViewInto(Buffer, Normal));

			CharsSkipped += NewChars;

			if (Scan.Found)
			{
				str_view FormatSpecifier = SkipChars(NewChars, ToGo);
				scan_result Scan = FormatScan(FormatSpecifier, '}');
				u64 FormatChars = Scan.CharsSkipped;
				if (Scan.Found)
				{
					str_view Format = Restrict(FormatChars - 1,
								   SkipChars(1, FormatSpecifier));
					Result = CombineFormatResults(Result,
								     PrintFormattedInto(Buffer, Format, Args));
					CharsSkipped += FormatChars + 1; // skip '{' and '}'
				}
				else
				{
					Result = CombineFormatResults(Result, PrintViewInto(Buffer, FormatSpecifier));
					CharsSkipped += FormatChars;
				}
			}
		}

		YASSERT(CharsSkipped == FormatStr.Size, "Everything was analyzed");
	}

	return Result;
}
