#include <util/wav.h>
#include <util/math.h>

FWDDECLARE(chunk_header);
FWDDECLARE(riff_chunk);
FWDDECLARE(fmt_chunk);
FWDDECLARE(data_chunk);

#define MAGIC_NUMBER(a, b, c, d) (((u32)(a) << 0)	\
				  | (((u32) (b)) << 8)	\
				  | (((u32) (c)) << 16)	\
				  | (((u32) (d)) << 24))

#define RIFFID MAGIC_NUMBER('R', 'I', 'F', 'F')
#define FMTID  MAGIC_NUMBER('f', 'm', 't', ' ')
#define DATAID MAGIC_NUMBER('d', 'a', 't', 'a')
#define WAVEID MAGIC_NUMBER('W', 'A', 'V', 'E')
#define PCM_SIZE 16

YSASSERT(0x46464952 == RIFFID);
YSASSERT(0x20746d66 == FMTID);
YSASSERT(0x61746164 == DATAID);
YSASSERT(0x45564157 == WAVEID);

struct chunk_header
{
	u32 Id;
	u32 Size;
};

struct riff_chunk
{
	u32 Format;
};

struct fmt_chunk
{
	u16 AudioFormat;
	u16 NumChannels;
	u32 SampleRate;
	u32 ByteRate;
	u16 ByteAlign;
	u16 BitsPerSample;
	byte ExtraBytes[];
} __attribute__((packed));


wav_header
LoadWAVHeader(u32 NumBytes, byte *WAV)
{
	wav_header Result;

	byte *Current = WAV;
	chunk_header *FirstChunk = (chunk_header *) Current;

	YASSERT(FirstChunk->Id == RIFFID);
	YASSERT(FirstChunk->Size + sizeof(chunk_header) == NumBytes);

	riff_chunk *Riff = (riff_chunk *) PTROFFSET(FirstChunk, sizeof(chunk_header));
	YASSERT(Riff->Format == WAVEID);

	Current = PTROFFSET(Current, sizeof(chunk_header) + sizeof(riff_chunk));
	chunk_header *SecondChunk = (chunk_header *) Current;

	YASSERT(SecondChunk->Id == FMTID);
	YASSERT(SecondChunk->Size == PCM_SIZE);

	fmt_chunk *Fmt = (fmt_chunk *) PTROFFSET(SecondChunk, sizeof(chunk_header));
	YASSERT(Fmt->AudioFormat      == 1); /* 1 = PCM, i.e. no compression */
	YASSERT(Fmt->BitsPerSample    == 16);

	Current = PTROFFSET(Current, sizeof(chunk_header) + SecondChunk->Size);
	chunk_header *ThirdChunk = (chunk_header *) Current;

	YASSERT(ThirdChunk->Id == DATAID);
	idx NumChannels    = Fmt->NumChannels;
	idx BytesPerSample = Fmt->BitsPerSample / 8;
	idx NumSamples     = ThirdChunk->Size / (NumChannels * BytesPerSample);

	Result.NumChannels  = Fmt->NumChannels;
	Result.ChannelPitch = sizeof(f32) * NumSamples; // output is always floating point
	Result.SampleRate   = Fmt->SampleRate;
	Result.NumSamples   = NumSamples;

	return Result;
}

void
LoadWAV(real_sound_buffer *Buffer,
	u32 NumBytes,
	byte *WAV)
{
	byte *Current = WAV;
	chunk_header *FirstChunk = (chunk_header *) Current;

	YASSERT(FirstChunk->Id == RIFFID);
	YASSERT(FirstChunk->Size + sizeof(chunk_header) == NumBytes);

	riff_chunk *Riff = (riff_chunk *) PTROFFSET(FirstChunk, sizeof(chunk_header));
	YASSERT(Riff->Format == WAVEID);

	Current = PTROFFSET(Current, sizeof(chunk_header) + sizeof(riff_chunk));
	chunk_header *SecondChunk = (chunk_header *) Current;

	YASSERT(SecondChunk->Id == FMTID);
	YASSERT(SecondChunk->Size == PCM_SIZE);

	fmt_chunk *Fmt = (fmt_chunk *) PTROFFSET(SecondChunk, sizeof(chunk_header));
	YASSERT(Fmt->AudioFormat      == 1); /* 1 = PCM, i.e. no compression */
	YASSERT(Fmt->BitsPerSample    == 16);
	YASSERT(Buffer->NumChannels   == Fmt->NumChannels);
	YASSERT((u32) Buffer->SampleRate    == Fmt->SampleRate);

	Current = PTROFFSET(Current, sizeof(chunk_header) + SecondChunk->Size);
	chunk_header *ThirdChunk = (chunk_header *) Current;

	YASSERT(ThirdChunk->Id == DATAID);
	idx NumChannels    = Fmt->NumChannels;
	idx BytesPerSample = Fmt->BitsPerSample / 8;
	idx NumSamples     = ThirdChunk->Size / (NumChannels * BytesPerSample);

	YASSERT(Buffer->NumSamples == NumSamples);

	YASSERT(NumSamples * (NumChannels * BytesPerSample) == ThirdChunk->Size);

	i16 (*WavData)[NumChannels] = (i16 (*)[NumChannels]) PTROFFSET(Current, sizeof(chunk_header));

	f32 (*ChannelData)[NumSamples] = (f32 (*)[NumSamples]) Buffer->Data;

	f32 FScale = 1.0 / (f32) I16_MAX;

	for (idx I = 0;
	     I < NumChannels;
	     ++I)
	{
		for (idx Sample = 0;
		     Sample < NumSamples;
		     ++Sample)
		{
			ChannelData[I][Sample] =
				Clamp(-1, FScale * WavData[Sample][I], 1);
		}
	}
}
