#include <def.h>
#include <util/hex.h>
#include <util/math.h>

#define Sqrt3Halfs 0.866025403785f

v2
HexToLinear(v2 HexPos)
{
	v2 Result;

	Result.X = F32C(1.5) * HexPos.X;
	Result.Y = Sqrt3Halfs * HexPos.X + Sqrt3Halfs * 2 * HexPos.Y;

	return Result;
}

v2
LinearToHex(v2 LinPos)
{
	v2 Result;

	Result.X = F32C(2.0)/F32C(3.0) * LinPos.X;
	Result.Y = F32C(-1.0)/F32C(3.0) * LinPos.X + LinPos.Y / (Sqrt3Halfs * 2);

	return Result;
}

iv2
RoundHexPos(v2 HexPos)
{
	// Convert "axial" hex coordinates x, y
	// into cube coordinates x, y, z
	// with x + y + z = 0

	f32 X = HexPos.X;
	f32 Y = HexPos.Y;
	f32 Z = -X - Y;

	i32 RoundedX = Round(X);
	i32 RoundedY = Round(Y);
	i32 RoundedZ = Round(Z);

	f32 DX = Abs((f32) RoundedX - X);
	f32 DY = Abs((f32) RoundedY - Y);
	f32 DZ = Abs((f32) RoundedZ - Z);

	iv2 Result;

	if ((DX >= DY) && (DX >= DZ))
	{
		// DX is the biggest difference => make X direction smaller
		Result.X = -RoundedY - RoundedZ;
		Result.Y = RoundedY;
	} else if (DZ >= DY) {
		// DZ is the biggest difference => make Z direction smaller
		Result.X = RoundedX;
		Result.Y = RoundedY;
	} else {
		// DY is the biggest difference => make Y direction smaller
		Result.X = RoundedX;
		Result.Y = -RoundedX - RoundedZ;
	}

	return Result;
}

i32
HexLength(iv2 Pos)
{
	i32 P = Abs(Pos.X);
	i32 Q = Abs(Pos.Y);
	i32 R = Abs(-Pos.X-Pos.Y);

	i32 Result = (P + Q + R) / 2;

	return Result;
}

i32
HexDistance(iv2 HexPosA, iv2 HexPosB)
{
	iv2 Delta = Diff(HexPosA, HexPosB);

	i32 Result = HexLength(Delta);

	return Result;
}
