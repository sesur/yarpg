#include <def.h>

#include <util/vector.h>

#define DefineAddT(T)							\
	AddTProt(T)							\
	{								\
		T Result;						\
		for (u64 I = 0; I < ARRAYCOUNT(Result.AsArray); ++I)	\
		{							\
			Result.AsArray[I] = A.AsArray[I] + B.AsArray[I]; \
		}							\
		return Result;						\
	}

#define DefineHadamardT(T)						\
	HadamardTProt(T)						\
	{								\
		T Result;						\
		for (u64 I = 0; I < ARRAYCOUNT(Result.AsArray); ++I)	\
		{							\
			Result.AsArray[I] = A.AsArray[I] * B.AsArray[I]; \
		}							\
		return Result;						\
	}

#define DefineDiffT(T)							\
	DiffTProt(T)							\
	{								\
		T Result;						\
		for (u64 I = 0; I < ARRAYCOUNT(Result.AsArray); ++I)	\
		{							\
			Result.AsArray[I] = A.AsArray[I] - B.AsArray[I]; \
		}							\
		return Result;						\
	}

#define DefineScaleT(T)							\
	ScaleTProt(T)							\
	{								\
		T Result;						\
		for (u64 I = 0; I < ARRAYCOUNT(Result.AsArray); ++I)	\
		{							\
			Result.AsArray[I] = X * V.AsArray[I];		\
		}							\
		return Result;						\
	}

#define DefineEqT(T)							\
	EqTProt(T)							\
	{								\
		b32 Result = 1;						\
		for (u64 I = 0; I < ARRAYCOUNT(A.AsArray); ++I)	\
		{							\
			Result &= A.AsArray[I] == B.AsArray[I];		\
		}							\
		return Result;						\
	}

#define DefineStandardOpsT(T) DefineAddT(T) DefineHadamardT(T) DefineDiffT(T) DefineEqT(T)

#define DefineScaleIT(T)							\
	ScaleITProt(T)							\
	{								\
		T Result;						\
		for (u64 I = 0; I < ARRAYCOUNT(Result.AsArray); ++I)	\
		{							\
			Result.AsArray[I] = X * V.AsArray[I];		\
		}							\
		return Result;						\
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
DefineStandardOpsT(v2)
DefineStandardOpsT(v3)
DefineStandardOpsT(v4)
DefineStandardOpsT(iv2)
DefineStandardOpsT(iv3)
DefineStandardOpsT(iv4)
#pragma GCC diagnostic pop

DefineScaleT(v2)
DefineScaleT(v3)
DefineScaleT(v4)
DefineScaleIT(iv2)

irect CONSTFN
Offset_irectiv2(irect Rect, iv2 Offset)
{
	irect Result;

	Result.MinX = Rect.MinX + Offset.X;
	Result.MinY = Rect.MinY + Offset.Y;
	Result.MaxX = Rect.MaxX + Offset.X;
	Result.MaxY = Rect.MaxY + Offset.Y;

	return Result;
}

f32 CONSTFN
Inner_v2(v2 A, v2 B)
{
	f32 Res = A.X * B.X + A.Y * B.Y;
	return Res;
}

f32 CONSTFN
Length_v2(v2 A)
{
	f32 LengthSq = Inner(A, A);
	f32 Result = Sqrt(LengthSq);
	return Result;
}

f32 CONSTFN
OneOverLength_v2(v2 A)
{
	f32 LengthSq = Inner(A, A);
	f32 Result = RSqrt(LengthSq);
	return Result;
}

f32 CONSTFN
Inner_v3(v3 A, v3 B)
{
	f32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z;
	return Res;
}

f32 CONSTFN
Length_v3(v3 A)
{
	f32 LengthSq = Inner(A, A);
	f32 Result = Sqrt(LengthSq);
	return Result;
}

f32 CONSTFN
OneOverLength_v3(v3 A)
{
	f32 LengthSq = Inner(A, A);
	f32 Result = RSqrt(LengthSq);
	return Result;
}

f32 CONSTFN
Distance(v2 A, v2 B)
{
	v2 Delta = Diff(A, B);
	f32 Result = Length(Delta);
	return Result;
}

f32 CONSTFN
OneOverDistance(v2 A, v2 B)
{
	v2 Delta = Diff(A, B);
	f32 Result = OneOverLength(Delta);
	return Result;
}

f32 CONSTFN
Lerp_f32(f32 A, f32 T, f32 B)
{
	f32 Result = T * B + (1 - T) * A;
	return Result;
}

v2 CONSTFN
Lerp_v2(v2 A, f32 T, v2 B)
{
	v2 Result = Add(Scale(1-T, A),
			Scale(T, B));

	return Result;
}

v3 CONSTFN
BasisTransform(v3 Vector, basis_system Basis)
{
	v3 Result;
	Result.X = Inner(V(Vector.X, Vector.Y), Basis.XAxis) + Basis.Origin.X;
	Result.Y = Inner(V(Vector.X, Vector.Y), Basis.YAxis) + Basis.Origin.Y;
	Result.Z = Vector.Z + Basis.Origin.Z;

	return Result;
}

v2 CONSTFN
BasisStretch(v2 Vector, basis_system Basis)
{
	v2 Result;

	Result.X = Inner(Vector, Basis.XAxis);
	Result.Y = Inner(Vector, Basis.YAxis);

	return Result;
}

v2 CONSTFN
UnitVector(f32 Angle)
{
	v2 Result;
	Result.X = Sin(Angle);
	Result.Y = Cos(Angle);
	return Result;
}

v2 CONSTFN
RightOrtho
(
	v2 Vec
)
{
	v2 Ortho = V(Vec.Y, -Vec.X);

	return Ortho;
}

v2 CONSTFN
LeftOrtho
(
	v2 Vec
)
{
	v2 Ortho = V(-Vec.Y, Vec.X);

	return Ortho;
}

b32 CONSTFN
PointInsideRect
(
	v2 Point,
	rect Rect
)
{
	b32 XInside = (Point.X <= Rect.MaxX) && (Point.X >= Rect.MinX);
	b32 YInside = (Point.Y <= Rect.MaxY) && (Point.Y >= Rect.MinY);

	return XInside && YInside;
}

f32
SmallVectorAngle
(
	v2 Vec,
	v2 Base
)
{
	// returns the angle between Base and Vec
	// starting from Base and going left
	f32 Val = Inner(Base, Vec) / (Length(Base) * Length(Vec));
	f32 Angle = ArcCos(Val);
	YASSERT(!isnan(Angle));

	return Angle;
}

f32
LeftVectorAngle
(
	v2 Vec,
	v2 Base
)
{
	// returns the angle between Base and Vec
	// starting from Base and going left
	f32 Val = Inner(Base, Vec) / (Length(Base) * Length(Vec));
	f32 Angle = ArcCos(Val);
	YASSERT(!isnan(Angle));

	f32 Direction = Base.X * Vec.Y - Base.Y * Vec.X;

	if (Direction < 0)
	{
		Angle = 2 * MC_PI - Angle;
	}

	return Angle;
}

f32
RightVectorAngle
(
	v2 Vec,
	v2 Base
)
{
	// returns the angle between Base and Vec
	// starting from Base and going left
	f32 Val = Inner(Base, Vec) / (Length(Base) * Length(Vec));
	f32 Angle = ArcCos(Val);
	YASSERT(!isnan(Angle));

	f32 Direction = Base.X * Vec.Y - Base.Y * Vec.X;

	if (Direction >= 0)
	{
		Angle = 2 * MC_PI - Angle;
	}

	return Angle;
}

f32
Determinant_v2
(
	v2 Col1,
	v2 Col2
)
{
	f32 Pos = Col1.X * Col2.Y;
	f32 Neg = Col1.Y * Col2.X;
	f32 Result = Pos - Neg;
	return Result;
}

f32
Determinant_v3
(
	v3 Col1,
	v3 Col2,
	v3 Col3
)
{
	f32 Pos = Col1.X * Col2.Y * Col3.Z + Col2.X * Col3.Y * Col1.Z + Col3.X * Col1.Y * Col2.Z;
	f32 Neg = Col3.X * Col2.Y * Col1.Z + Col2.X * Col1.Y * Col3.Z + Col1.X * Col3.Y * Col2.Z;
	f32 Result = Pos - Neg;

	return Result;
}

bool
LeftOf
(
	v2 Start,
	v2 End,
	v2 TestP
)
{
	v2 Vec = Diff(End, Start);
	v2 Test = Diff(TestP, Start);
	f32 Direction = Determinant(Vec, Test);
	bool IsLeftOf = Direction > 0;
	return IsLeftOf;
}

bool
RightOf
(
	v2 Start,
	v2 End,
	v2 TestP
)
{
	v2 Vec = Diff(End, Start);
	v2 Test = Diff(TestP, Start);
	f32 Direction = Determinant(Vec, Test);
	bool IsLeftOf = Direction < 0;
	return IsLeftOf;
}
