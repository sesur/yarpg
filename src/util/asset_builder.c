#include <util/asset_builder.h>
#include <yaff_format.h>

FWDDECLARE(asset_builder);

#define INVALID_INFO -1

struct asset_builder
{
        asset_store  *Store;

	u32 NextAssetByType[AssetType_COUNT];
	u32 AssetStartByType[AssetType_COUNT];
	u32 NumAssetsByType[AssetType_COUNT];

	idx NextTag;
	idx NumTags;
};

static asset_builder
BeginBuilder(asset_store *Store,
	     u32 NumAssetsByType[AssetType_COUNT],
	     idx NumTags)
{

	asset_builder Builder = { 0 };
	Builder.Store        = Store;

	idx CurrentAsset = 0;
	for (i64 Type = 0;
	     Type < AssetType_COUNT;
	     ++Type)
	{
		Builder.AssetStartByType[Type] = (u32) CurrentAsset;
		Builder.NumAssetsByType[Type] = NumAssetsByType[Type];

		CurrentAsset += NumAssetsByType[Type];
	}

	Builder.NumTags = NumTags;

	return Builder;
}

static asset_store *
EndBuilder(asset_builder *Builder)
{
	for (i64 Type = 0;
	     Type < AssetType_COUNT;
	     ++Type)
	{
		YASSERT(Builder->NextAssetByType[Type] == Builder->NumAssetsByType[Type]);
	}

	YASSERT(Builder->NextTag <= Builder->NumTags);

	return Builder->Store;
}

static asset_info *
NextAssetByType(asset_builder *Builder, asset_type Type)
{
	YASSERT(Type < AssetType_COUNT);

	asset_store *Store = Builder->Store;

	YASSERT(Builder->NextAssetByType[Type] < Builder->NumAssetsByType[Type]);

	asset_info *Result = &Store->AssetInfos[Builder->AssetStartByType[Type]
					       + Builder->NextAssetByType[Type]++];

	return Result;
}

static idx
CurrentAssetIdxByType(asset_builder *Builder, asset_type Type)
{
	YASSERT(Type < AssetType_COUNT);

	idx Result = Builder->AssetStartByType[Type]
		+ Builder->NextAssetByType[Type];

	return Result;
}

static asset_info *
NextText(asset_builder *Builder)
{
	return NextAssetByType(Builder, AssetType_TEXT);
}

static idx
CurrentTextIdx(asset_builder *Builder)
{

        return CurrentAssetIdxByType(Builder, AssetType_TEXT);
}

static asset_info *
NextSprite(asset_builder *Builder)
{
	return NextAssetByType(Builder, AssetType_SPRITE);
}

static idx
CurrentSpriteIdx(asset_builder *Builder)
{
	return CurrentAssetIdxByType(Builder, AssetType_SPRITE);
}

static asset_info *
NextSound(asset_builder *Builder)
{
	return NextAssetByType(Builder, AssetType_SOUND);
}

static idx
CurrentSoundIdx(asset_builder *Builder)
{
	return CurrentAssetIdxByType(Builder, AssetType_SOUND);
}

static asset_tag *
NextTag(asset_builder *Builder)
{
	asset_store *Store = Builder->Store;
	YASSERT(Builder->NextTag < Builder->NumTags);

	asset_tag *Result = &Store->Tags[Builder->NextTag++];

	return Result;
}

static idx
CurrentTagIdx(asset_builder *Builder)
{
	idx Result = Builder->NextTag;

	return Result;
}

static void
SetTagInfo(asset_builder *Builder, asset_type Type, i32 Id, f32 Period)
{
	asset_store *Store = Builder->Store;
	switch (Type)
	{
		break;case AssetType_SPRITE:
		{
			Store->SpriteTagInfo[Id].Period = Period;
		}
		break;case AssetType_SOUND:
		{
			Store->SoundTagInfo[Id].Period = Period;
		}
		break;case AssetType_TEXT:
		{
			Store->TextTagInfo[Id].Period = Period;
		}
		break;case AssetType_COUNT:
		{
			INVALIDCODEPATH();
		}
	}
}

asset_store *
LoadYAFFFiles(asset_store *Store, memory_arena *Perm,
	      memory_arena *Scratch)
{
	file_iterator Iter = OpenAssetIterator();

	idx NumAssetFiles = Iter.NumEntries;

#define MAKE_TEMP_ARRAY(Type, Name, Count) Type *Name = PushArray(Scratch, Count, *Name)
#define RANGE_TO_ARGS(Range) (Range).Begin, (Range).End - (Range).Begin
#define ASSERT_U32(X) YASSERTF((X) >= 0 && (X) < U32_MAX, "%ld", X)

	file *AssetFiles = PushArray(Perm, NumAssetFiles, *AssetFiles);

	MAKE_TEMP_ARRAY(yaff_directory, Dirs, NumAssetFiles);
	MAKE_TEMP_ARRAY(yaff_asset_tag     *, Tags, NumAssetFiles);
	MAKE_TEMP_ARRAY(category           *, LoadedCategories, NumAssetFiles);
	MAKE_TEMP_ARRAY(yaff_category_info *, CategoryInfos, NumAssetFiles);
	MAKE_TEMP_ARRAY(yaff_asset_info    *, AssetInfos, NumAssetFiles);
	MAKE_TEMP_ARRAY(yaff_sprite        *, SpriteInfosByFile, NumAssetFiles);
	MAKE_TEMP_ARRAY(yaff_sound         *, SoundInfosByFile, NumAssetFiles);
	MAKE_TEMP_ARRAY(yaff_text          *, TextInfosByFile, NumAssetFiles);

        u32 NumAssets   = 0;
	u32 NumTags     = 0;

	u32 NumAssetsByType[AssetType_COUNT] = { 0 };

	ZeroArray(ARRAYCOUNT(Store->CategoryInfo), Store->CategoryInfo);

	{
		idx FileIdx = 0;
		idx CurrentAssetFile = 0;
		for (file File = StartIterator(&Iter);
		     CurrentAssetFile < NumAssetFiles;
		     ++CurrentAssetFile, File = NextFile(&Iter))
		{
			yaff_header Header;
			ReadFile(&File, 0, sizeof(yaff_header),
				 (byte *) &Header);

			if (File.Error)
			{
				CloseFile(&File);
				continue;
			}


#define DECOMPOSE(U32) ((char) (U32)), ((char) ((U32) >> 8)), ((char) ((U32) >> 16)), ((char) ((U32) >> 24))
			/* YASSERTF(Header.MagicNumber == YAFF_MAGIC_NUMBER, */
			/* 	 "%c%c%c%C == %c%c%c%c", */
			/* 	 DECOMPOSE(Header.MagicNumber), */
			/* 	 DECOMPOSE(YAFF_MAGIC_NUMBER)); */

			if (Header.MagicNumber != YAFF_MAGIC_NUMBER)
			{
				CloseFile(&File);
				FormatOut("[LOAD YAFF] Wrong file type: {char}.{char}.{char}-{char} != {char}.{char}.{char}-{char}\n",
				       DECOMPOSE(Header.MagicNumber),
				       DECOMPOSE(YAFF_MAGIC_NUMBER));
				continue;
			};

#undef DECOMPOSE
#define DECOMPOSE(U32) ((char) (U32) + '0'), ((char) ((U32) >> 8) + '0'), ((char) ((U32) >> 16) + '0'), ((char) ((U32) >> 24))
			if (Header.Version != YAFF_VERSION)
			{
				CloseFile(&File);
				FormatOut("[LOAD YAFF] Wrong version: {char}.{char}.{char}-{char} != {char}.{char}.{char}-{char}\n",
				       DECOMPOSE(Header.Version),
				       DECOMPOSE(YAFF_VERSION));
				continue;
			};
			/* YASSERTF(Header.Version == YAFF_VERSION, */
			/* 	 "", */

			/* 	); */
#undef DECOMPOSE



			u64 FileSize = Header.FileSize;

			YASSERTF(FileSize == (u64) File.Size,
				 "%lu <> %ld",
				 FileSize,
				 File.Size);

			u64 DirectoryStart = Header.DirectoryStart;

			yaff_directory *Dir = &Dirs[FileIdx];

			ReadFile(&File, DirectoryStart, sizeof(yaff_directory),
				 (byte *) Dir);

			if (File.Error)
			{
				CloseFile(&File);
				continue;
			}

#define DECOMPOSE(U32) ((char) (U32)), ((char) ((U32) >> 8)), ((char) ((U32) >> 16)), ((char) ((U32) >> 24))

			if (Dir->MagicNumber != YAFF_DIR_MAGIC_NUMBER)
			{
				CloseFile(&File);
				FormatOut("[LOAD YAFF] Error reading file : {char}{char}{char}{char} != {char}{char}{char}{char}\n",
				       DECOMPOSE(Dir->MagicNumber),
				       DECOMPOSE(YAFF_DIR_MAGIC_NUMBER));
				continue;
			};

#undef DECOMPOSE
			/* YASSERTF(Dir->MagicNumber == YAFF_DIR_MAGIC_NUMBER, */
			/* 	 "%u", Dir->MagicNumber); */

			NumAssets += Dir->NumAssets;
			NumTags     += Dir->NumTags;

			MAKE_TEMP_ARRAY(category,           CurrentLoadedCategories,    Dir->NumCategories);
			MAKE_TEMP_ARRAY(yaff_category_info, CurrentCategoryInfos, Dir->NumCategories);
			MAKE_TEMP_ARRAY(yaff_asset_info,      CurrentAssetInfos,     Dir->NumAssets);
			MAKE_TEMP_ARRAY(yaff_asset_tag,       CurrentTags,           Dir->NumTags);

			MAKE_TEMP_ARRAY(yaff_sprite,           CurrentSprites,       RangeLength(Dir->AssetIdRangeByType[AssetType_SPRITE]));
			MAKE_TEMP_ARRAY(yaff_sound,           CurrentSounds,         RangeLength(Dir->AssetIdRangeByType[AssetType_SOUND]));
			MAKE_TEMP_ARRAY(yaff_text,            CurrentTexts,          RangeLength(Dir->AssetIdRangeByType[AssetType_TEXT]));


			ReadFile(&File, RANGE_TO_ARGS(Dir->CategoryIdRange),
				 (byte *) CurrentLoadedCategories);

			ReadFile(&File, RANGE_TO_ARGS(Dir->CategoryInfoRange),
				 (byte *) CurrentCategoryInfos);

			ReadFile(&File, RANGE_TO_ARGS(Dir->AssetInfoRange),
				 (byte *) CurrentAssetInfos);

			ReadFile(&File, RANGE_TO_ARGS(Dir->TagsRange),
				 (byte *) CurrentTags);

			byte *InfoBytes[] =
			{
				[AssetType_SPRITE] = (byte *) CurrentSprites,
				[AssetType_TEXT]    = (byte *) CurrentTexts,
				[AssetType_SOUND]   = (byte *) CurrentSounds,
			};

			YSASSERT(ARRAYCOUNT(InfoBytes) == AssetType_COUNT);

			for (idx Type = 0;
			     Type < AssetType_COUNT;
			     ++Type)
			{
				ReadFile(&File, RANGE_TO_ARGS(Dir->TypedAssetInfoRange[Type]),
					 InfoBytes[Type]);
			}

			if (File.Error)
			{
				CloseFile(&File);
				continue;
			}

			for (idx Category = 0;
			     Category < Dir->NumCategories;
			     ++Category)
			{

				for (i64 Type = 0;
				     Type < AssetType_COUNT;
				     ++Type)
				{
					idx AddAssets = (CurrentCategoryInfos[Category].Assets[Type].End
							 - CurrentCategoryInfos[Category].Assets[Type].Begin);

					ASSERT_U32(AddAssets);

					Store->CategoryInfo[CurrentLoadedCategories[Category]].Assets[Type].End
						+= AddAssets;

					NumAssetsByType[Type] += (u32) AddAssets;
				}
			}

			LoadedCategories[FileIdx]    = CurrentLoadedCategories;
			CategoryInfos[FileIdx] = CurrentCategoryInfos;
			AssetInfos[FileIdx]     = CurrentAssetInfos;
			Tags[FileIdx]           = CurrentTags;

			SpriteInfosByFile[FileIdx] = CurrentSprites;
			SoundInfosByFile[FileIdx]   = CurrentSounds;
			TextInfosByFile[FileIdx]    = CurrentTexts;

			AssetFiles[FileIdx] = File;

			FileIdx += 1;
		}
		YASSERT(FileIdx <= NumAssetFiles);
		NumAssetFiles = FileIdx;
	}

	CloseIterator(&Iter);

	Store->NumFiles     = NumAssetFiles;
	Store->Files        = AssetFiles;

	Store->AssetCount   = NumAssets;
	Store->TagCount     = NumTags;

	Store->AssetInfos   = PushArray(Perm, NumAssets, *Store->AssetInfos);
	Store->Tags         = PushArray(Perm, NumTags, *Store->Tags);

	sprite_info *SpriteInfos = PushArray(Perm,
					     NumAssetsByType[AssetType_SPRITE],
					     *SpriteInfos);

	text_info *TextInfos = PushArray(Perm,
					 NumAssetsByType[AssetType_TEXT],
					 *TextInfos);

	sound_info *SoundInfos = PushArray(Perm,
					   NumAssetsByType[AssetType_SOUND],
					   *SoundInfos);

	ZeroArray(SpriteTag_COUNT, Store->SpriteTagInfo);
	ZeroArray(SoundTag_COUNT, Store->SoundTagInfo);
	ZeroArray(TextTag_COUNT, Store->TextTagInfo);

	Store->LRU.LRUNext = Store->LRU.LRUPrev = &Store->LRU;

	InitializeListAllocator(&Store->Data, ASSET_DATA_SIZE,
				PushSize(Perm, ASSET_DATA_SIZE, 1));

	Store->BlocksUsed  = 0;
	Store->Scratch     = SplitArena(Perm, ASSET_SCRATCH_SIZE);

	Store->SpriteInfos = SpriteInfos;
	Store->SoundInfos   = SoundInfos;
	Store->TextInfos    = TextInfos;


	asset_builder Builder =  BeginBuilder(Store, NumAssetsByType, NumTags);

	range *AssetIdRangeByType = Store->AssetIdRangeByType;

        for (i64 Type = 0;
	     Type < AssetType_COUNT;
	     ++Type)
	{
		idx Start = Builder.AssetStartByType[Type];
		idx Size  = Builder.NumAssetsByType[Type];
		AssetIdRangeByType[Type] = R(Start, Start + Size);
	}


	for (idx DesiredCategory = 0;
	     DesiredCategory < Category_COUNT;
	     ++DesiredCategory)
	{
//		BeginAsset(&Builder, DesiredCategory);

		category_info *Category = &Store->CategoryInfo[DesiredCategory];

		for (i64 Type = 0;
		     Type < AssetType_COUNT;
		     ++Type)
		{
			YASSERT(Category->Assets[Type].Begin == 0);
			Category->Assets[Type].Begin += CurrentAssetIdxByType(&Builder, Type);
			Category->Assets[Type].End   += CurrentAssetIdxByType(&Builder, Type);
		}

		for (idx AssetFile = 0;
		     AssetFile < NumAssetFiles;
		     ++AssetFile)
		{
			yaff_directory *Dir  = &Dirs[AssetFile];
			file           File = AssetFiles[AssetFile];
			category     *Loaded = LoadedCategories[AssetFile];

			range TagsRange = Dir->TagsRange;
			range CategoryIdRange = Dir->CategoryIdRange;

			for (idx CategoryIdx = 0;
			     CategoryIdx < Dir->NumCategories;
			     ++CategoryIdx)
			{
				if (Loaded[CategoryIdx] == DesiredCategory)
				{
					yaff_category_info CategoryInfo = CategoryInfos[AssetFile][CategoryIdx];

					range *Assets = CategoryInfo.Assets;

					yaff_directory *Dir                = &Dirs[AssetFile];

					yaff_asset_info *CurrentAssetInfos = AssetInfos[AssetFile];
					yaff_asset_tag  *CurrentTags       = Tags[AssetFile];

					yaff_sprite      *CurrentSprites   = SpriteInfosByFile[AssetFile];
					yaff_sound      *CurrentSounds     = SoundInfosByFile[AssetFile];
					yaff_text       *CurrentTexts      = TextInfosByFile[AssetFile];

					for (idx Type = 0;
					     Type < AssetType_COUNT;
					     ++Type)
					{
						for (idx Asset = Assets[Type].Begin;
						     Asset < Assets[Type].End;
						     ++Asset)
						{
							ASSERT_U32(CurrentAssetInfos[Asset].DataRange.Begin);
							ASSERT_U32(CurrentAssetInfos[Asset].DataRange.End);

							asset_location Loc;
							Loc.FileId = AssetFile;
							Loc.Start  = CurrentAssetInfos[Asset].DataRange.Begin;
							Loc.End    = CurrentAssetInfos[Asset].DataRange.End;

							idx InfoIdx = Builder.NextAssetByType[Type];

							switch (Type)
							{
								break;case AssetType_SPRITE:
								{
									range AssetTypeIdRange = Dir->AssetIdRangeByType[AssetType_SPRITE];

									YASSERT(AssetTypeIdRange.Begin <= Asset && Asset < AssetTypeIdRange.End);
									yaff_sprite *Sprite = &CurrentSprites[Asset - AssetTypeIdRange.Begin];
									sprite_info *SpriteInfo = &SpriteInfos[InfoIdx];

									SpriteInfo->Width = Sprite->Width;
									SpriteInfo->Height = Sprite->Height;
								}
								break;case AssetType_SOUND:
								{
									range AssetTypeIdRange = Dir->AssetIdRangeByType[AssetType_SOUND];

									YASSERT(AssetTypeIdRange.Begin <= Asset && Asset < AssetTypeIdRange.End);
									yaff_sound *Sound = &CurrentSounds[Asset - AssetTypeIdRange.Begin];
									sound_info *SoundInfo = &SoundInfos[InfoIdx];

									SoundInfo->HasNext = Sound->HasNext;
									SoundInfo->NumChannels = Sound->NumChannels;
									SoundInfo->SampleRate = Sound->SampleRate;
									SoundInfo->NumSamples = Sound->NumSamples;
								}
								break;case AssetType_TEXT:
								{
									range AssetTypeIdRange = Dir->AssetIdRangeByType[AssetType_TEXT];

									YASSERT(AssetTypeIdRange.Begin <= Asset && Asset < AssetTypeIdRange.End);
									yaff_text *Text = &CurrentTexts[Asset - AssetTypeIdRange.Begin];
									text_info *TextInfo = &TextInfos[InfoIdx];
									TextInfo->NumBytes = Text->Length;
									TextInfo->NumChars = Text->NumChars;
								}
								INVALIDCASE(AssetType_COUNT);
								INVALIDDEFAULT();
							}

							asset_info *AssetInfo
								= NextAssetByType(&Builder, Type);
							AssetInfo->Status   = AssetStatus_ZERO;
							AssetInfo->Header   = NULL;
							AssetInfo->Location = Loc;

							AssetInfo->Tags.Begin = CurrentTagIdx(&Builder);
							for (idx Tag = CurrentAssetInfos[Asset].Tags.Begin;
							     Tag < CurrentAssetInfos[Asset].Tags.End;
							     ++Tag)
							{
								asset_tag *AssetTag = NextTag(&Builder);
								AssetTag->Value = CurrentTags[Tag].Value;
								AssetTag->Id    = CurrentTags[Tag].Id;
							}
							AssetInfo->Tags.End = CurrentTagIdx(&Builder);
						}
					}
				}
			}
		}

		for (idx Type = 0;
		     Type < AssetType_COUNT;
		     ++Type)
		{
			YASSERTF(Category->Assets[Type].End == CurrentAssetIdxByType(&Builder, Type),
				 "%ld == %ld",
				 Category->Assets[Type].End,
				 CurrentAssetIdxByType(&Builder, Type));
		}
	}

	return EndBuilder(&Builder);
}
#undef MAKE_TEMP_ARRAY
#undef RANGE_TO_ARGS
#undef INVALID_INFO
#undef ASSET_SCRATCH_SIZE
#undef ASSET_DATA_SIZE
#undef ASSERT_U32

void
ReleaseYAFFFiles(asset_store *Store)
{
	for (idx File = 0;
	     File < Store->NumFiles;
	     ++File)
	{
		CloseFile(&Store->Files[File]);
	}
}
