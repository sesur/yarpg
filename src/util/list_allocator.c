#include <util/list_allocator.h>

#define USEFUL_SIZE 10 KB
#define LIST_ALLOCATOR_BLOCK_ALIGNMENT 1 KB

static void
PrintList(list_allocator *Alloc)
{
	udx CompleteSize = 0;
	for (memory_block *Current = Alloc->Sentinel.Next;
	     Current != &Alloc->Sentinel;
	     Current = Current->Next)
	{
		CompleteSize += Current->Size + sizeof(memory_block);

#ifndef PLATFORM
		FormatOut("{char[*]} {u64} ",
			  (Current->Flags & ListAllocator_USED) ? "USED" : "FREE",
			  Current->Size);
#else
		printf("%s %lu ", (Current->Flags & ListAllocator_USED) ?
		       "USED" : "FREE", Current->Size);
#endif
	}

#ifndef PLATFORM
	FormatOut(" = {u64}\n", CompleteSize);
#else
	printf("= %lu\n", CompleteSize);
#endif

}

static void
AssertZeroedMemory(udx Size, void *Memory)
{
	byte *Mem = Memory;
	for (udx i = 0;
	     i < Size;
	     ++i)
	{
		YASSERT(Mem[i] == 0);
	}
}

static void
TestIntegrityToNext(memory_block *Block)
{
	YASSERTF((ptr) (Block + 1) + Block->Size == (ptr) Block->Next,
		 "%lx + %lx == %lx (%lx)",
		 (ptr) (Block + 1),
		 Block->Size,
		 (ptr) Block->Next,
		 (ptr) Block->Next - ((ptr) (Block + 1) + Block->Size));
}

static void
AssertIntegrity(memory_block *Block)
{
	if (!(Block->Flags & ListAllocator_SENTINEL))
	{
		memory_block *Prev = Block->Prev;
		memory_block *Next = Block->Next;

		if (!(Prev->Flags & ListAllocator_SENTINEL))
		{
			TestIntegrityToNext(Prev);
		}

		if (!(Next->Flags & ListAllocator_SENTINEL))
		{
			TestIntegrityToNext(Block);
		}
	}
}

static void
TryReclaimingMemory(list_allocator *Alloc)
{

}

static void
InsertBlock(memory_block *Prev, udx Size, void *Memory)
{
	YASSERT(Prev);
	YASSERT(Prev->Next);
	YASSERT(Prev->Prev);
        //	AssertZeroedMemory(Size, Memory);
	memory_block *NewBlock = (memory_block *) Memory;
	NewBlock->Flags = 0;
	NewBlock->Prev = Prev;
	NewBlock->Next = Prev->Next;
	NewBlock->Prev->Next = NewBlock;
	NewBlock->Next->Prev = NewBlock;
	NewBlock->Size = Size;

	DEBUG_DO(
		AssertIntegrity(NewBlock);
		);

}

b32
InitializeListAllocator(list_allocator *Alloc,
			udx   MaxSize,
			void           *Memory)
{
	void *AlignedMemory = NEXTALIGNED(Memory, LIST_ALLOCATOR_BLOCK_ALIGNMENT);
	udx Useless = (ptr) AlignedMemory - (ptr) Memory;
	Alloc->Sentinel.Prev  = &Alloc->Sentinel;
	Alloc->Sentinel.Next  = &Alloc->Sentinel;
	Alloc->Sentinel.Size  = Useless;
	Alloc->Sentinel.Flags = ListAllocator_USED | ListAllocator_SENTINEL;

	YASSERTF(MaxSize > sizeof(memory_block) + Useless,
		 "%lu > %lu + %lu",
		 MaxSize,
		 sizeof(memory_block),
		 Useless);

	InsertBlock(&Alloc->Sentinel, MaxSize - Useless - sizeof(memory_block),
		    AlignedMemory);
//	PrintList(Alloc);
	return 0;
}

void *
SlowAlloc(list_allocator *Alloc, udx Size)
{
//	Size += sizeof(memory_block);
        for (memory_block *Current = Alloc->Sentinel.Next;
	     Current != &Alloc->Sentinel;
	     Current = Current->Next)
	{
		if (!(Current->Flags & ListAllocator_USED) &&
		    Size <= Current->Size)
		{
			Current->Flags |= ListAllocator_USED;
			void *FreeMemory = (void *) (Current+1);

			// Try splitting this block

			void *NextPossible = NEXTALIGNED(PTROFFSET(FreeMemory, Size),
							 LIST_ALLOCATOR_BLOCK_ALIGNMENT);
			udx Offset = (ptr) NextPossible - (ptr) FreeMemory;

			udx NewBlockSize = Current->Size - Offset;

			if (NewBlockSize > USEFUL_SIZE + sizeof(memory_block))
			{
				YASSERTF(Size <= Offset, "Size: %lu, Offset: %lu",
					 Size, Offset);
				Current->Size = Offset;
				InsertBlock(Current,
                                            NewBlockSize - sizeof(memory_block),
					    NextPossible);
			}

                        return FreeMemory;
		}
	}

//        YASSERT(0, "Out of Memory");
	return NULL;
}

static void
AbsorbBlock(memory_block *Absorber, memory_block *Absorbee)
{
	DEBUG_DO(
		AssertIntegrity(Absorber);
		);
	Absorber->Size += Absorbee->Size + sizeof(memory_block);
	Absorber->Next = Absorbee->Next;
        Absorber->Next->Prev = Absorber;
}

void *
SlowRealloc(list_allocator *Alloc, udx Size, void *Old)
{
	memory_block *Block = (memory_block *) Old - 1;
	memory_block *Next = Block->Next;
	memory_block *Prev = Block->Prev;
	memory_block *Start = Block, *End = Block;
	b32 NextFree = !!(Next->Flags & ListAllocator_USED);
	b32 PrevFree = !!(Prev->Flags & ListAllocator_USED);
        udx MaxPossibleSize = Block->Size;
	if (Size < MaxPossibleSize)
	{
		return Old;
	}
	if (NextFree)
	{
		MaxPossibleSize += sizeof(memory_block);
		MaxPossibleSize += Next->Size;
		End = Next;
		if (Size < MaxPossibleSize) goto ReallocNext;
	}
	/* if (PrevFree) */
	/* { */
	/* 	MaxPossibleSize += sizeof(memory_block); */
	/* 	MaxPossibleSize += Prev->Size; */
        /*         Start = Prev; */
	/* 	if (Size < MaxPossibleSize) goto Realloc; */
	/* } */

        // SLOW PATH
	void *NewMem = SlowAlloc(Alloc, Size);
#ifndef PLATFORM
	MoveMemory(Block->Size, Old, NewMem);
#else
	memcpy(NewMem, Old, Block->Size);
#endif
	SlowFree(Old);
	return NewMem;

ReallocNext:
	AbsorbBlock(Block, Next);
	return Old;

}

void *
SlowArrayAlloc(list_allocator *Alloc, u32 Count, udx Size)
{
	return SlowAlloc(Alloc, Count * Size);
}

void
SlowFree(void *ToFree)
{
	memory_block *Block = (memory_block *) ToFree - 1;
	Block->Flags &= ~ListAllocator_USED;

	DEBUG_DO(
		AssertIntegrity(Block);
		);

	if (!(Block->Next->Flags & ListAllocator_USED)) AbsorbBlock(Block, Block->Next);
	if (!(Block->Prev->Flags & ListAllocator_USED))
	{
		Block = Block->Prev;
		AbsorbBlock(Block, Block->Next);
	}

	DEBUG_DO(
		ZeroMemory(Block->Size, (byte *) (Block+1));
		);
}
