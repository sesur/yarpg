#include <util/memory.h>
//#include <debug_state.h>

void
ZeroMemory(u64 NumBytes, byte *Memory)
{
	//INSTRUMENT_COUNTED(ZeroMemory, NumBytes)
	{
		if (NumBytes < 32)
		{
			byte const *End = &Memory[NumBytes];

			while (Memory != End)
			{
				*Memory++ = 0;
			}
		}
		else
		{
			INSTRUMENT_COUNTED(ZeroMemory, NumBytes)
			{
				byte *End = Memory + NumBytes;
				__m256i ZeroX32 = _mm256_setzero_si256();

				__m256i *AVXStart = (__m256i *) NEXTALIGNED(Memory,
									    alignof(__m256));
				__m256i *AVXEnd   = (__m256i *) PREVALIGNED(End,
									    alignof(__m256));
				if ((ptr)AVXStart != (ptr)Memory)
				{
					_mm256_storeu_si256((__m256i *) Memory,
							    ZeroX32); // do a bit of work twice
				}

				if ((ptr) AVXEnd != (ptr) End)
				{
					_mm256_storeu_si256((__m256i *) PTROFFSET(End, -32),
							    ZeroX32); // do some more work twice
				}

				while (AVXStart != AVXEnd)
				{
					_mm256_stream_si256(AVXStart++, ZeroX32);
				}
			}
		}
	}
}

void
SetMemory(u64 NumBytes, byte Val, byte *Memory)
{
//	INSTRUMENT_COUNTED(SetMemory, NumBytes)
	{
		if (NumBytes < 32)
		{
			byte const *End = &Memory[NumBytes];

			while (Memory != End)
			{
				*Memory++ = Val;
			}
		}
		else
		{
//			INSTRUMENT_COUNTED(SetMemory, NumBytes)
			{
				byte *End = Memory + NumBytes;
				__m256i ValX32 = _mm256_set1_epi8((i8) Val);

				__m256i *AVXStart = (__m256i *) NEXTALIGNED(Memory,
									    alignof(__m256));
				__m256i *AVXEnd   = (__m256i *) PREVALIGNED(End,
									    alignof(__m256));
				if ((ptr)AVXStart != (ptr)Memory)
				{
					_mm256_storeu_si256((__m256i *) Memory,
							    ValX32); // do a bit of work twice
				}

				if ((ptr) AVXEnd != (ptr) End)
				{
					_mm256_storeu_si256((__m256i *) PTROFFSET(End, -32),
							    ValX32); // do some more work twice
				}

				while (AVXStart != AVXEnd)
				{
					_mm256_stream_si256(AVXStart++, ValX32);
				}
			}
		}
	}
}

void
MoveMemory(u64 NumBytes, byte const * restrict From, byte * restrict To)
{
//	INSTRUMENT_COUNTED(MoveMemory, NumBytes)
	{
		if (NumBytes < 32)
		{
			byte const *End = &To[NumBytes];

			while (To != End)
			{
				*To++ = *From++;
			}
		}
		else
		{
			INSTRUMENT_COUNTED(MoveMemory, NumBytes)
			{
				__m256i const * restrict FromV = (__m256i const * restrict) From;

				byte *AVXStart = NEXTALIGNED(To, alignof(__m256));

				ptr Diff = ((ptr)AVXStart - (ptr)To);

				__m256i const *AVXFrom  = PTROFFSET(FromV, Diff);

				_mm256_storeu_si256((__m256i *) To,
						    _mm256_loadu_si256(FromV)); // do a bit of work twice

				YASSERTF(Diff < 32,
					 "%lu < 32", Diff);

				u64 Rest = NumBytes - Diff;

				__m256i *Stream = (__m256i *) AVXStart;
				while (Rest >= 32)
				{
					__m256i ValX32 = _mm256_loadu_si256(AVXFrom++);
					_mm256_stream_si256(Stream++, ValX32);
					Rest -= 32;
				}

				__m256i *RestP = PTROFFSET(Stream, Rest - 32);
				__m256i const *RestF = PTROFFSET(AVXFrom, Rest - 32);
				_mm256_storeu_si256(RestP, _mm256_loadu_si256(RestF)); // do some more work twice
			}
		}
	}
}
