rect
RectMinMax(v2 Min, v2 Max)
{
	rect Result;

	Result.Min = Min;
	Result.Max = Max;

	return Result;
}

rect
RectCenterHalfDim(v2 Center, v2 HalfDim)
{
	rect Result;

	Result.Min = Diff(Center, HalfDim);
	Result.Max = Add(Center, HalfDim);

	return Result;
}

rect
RectCenterDim(v2 Center, v2 Dim)
{
	return RectCenterHalfDim(Center, Scale(0.5, Dim));
}

rect
RectMinDim(v2 Min, v2 Dim)
{
	rect Result;

	Result.Min = Min;
	Result.Max = Add(Min, Dim);

	return Result;
}
