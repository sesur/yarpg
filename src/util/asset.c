#include <util/asset.h>
#include <util/random.h>

FWDDECLARE(fetch_asset_task);

struct fetch_asset_task
{
	asset_store  *Assets;
	asset_header *Header;
};

PRIVATE byte *
AssetPushTemp(asset_store *Assets, idx Size)
{
	/* NOTE: this is only called in the main thread! */
	idx OldBlocksUsed = InterlockedPostInc(&Assets->BlocksUsed);
	if (OldBlocksUsed == 0)
	{
                FreeAll(&Assets->Scratch);
	}
	byte *Result = PushSize(&Assets->Scratch, Size, 1);

	return Result;
}

PRIVATE void
LRUAdd(asset_store *Assets, asset_header *Header)
{
	DEBUG_DO(
		// they are only set to NULL for debug sessions!
		YASSERT(!Header->LRUPrev && !Header->LRUNext);
		);

	Header->LRUPrev = &Assets->LRU;
	Header->LRUNext = Assets->LRU.LRUNext;

	Header->LRUPrev->LRUNext = Header;
	Header->LRUNext->LRUPrev = Header;

	Header->Flags |= AssetFlag_INUSE;
}

PRIVATE void
LRURemove(asset_header *Header)
{
	YASSERT(Header->LRUPrev);
	YASSERT(Header->LRUNext);

	Header->LRUPrev->LRUNext = Header->LRUNext;
	Header->LRUNext->LRUPrev = Header->LRUPrev;

	DEBUG_DO(
		Header->LRUPrev = NULL;
		Header->LRUNext = NULL;
		);
}

PRIVATE void
LRUTouch(asset_store *Assets, asset_header *Header)
{
	LRURemove(Header);
	LRUAdd(Assets, Header);
}

PRIVATE void
AssetPopTemp(asset_store *Assets)
{
	/* NOTE: this may be called from any thread */
	YASSERT(InterlockedPostDec(&Assets->BlocksUsed) > 0);
}

PRIVATE asset_header *
AllocAsset(asset_store *Assets, asset_type Type, asset_id Id, idx Size, udx Alignment)
{
	asset_header *Result = NULL;

        udx CompleteSize = Size + sizeof(asset_header) + Alignment - 1;

	Result = SlowAlloc(&Assets->Data, CompleteSize);

	asset_header *Header = Assets->LRU.LRUPrev;

	while (!Result)
	{
		while (Header != &Assets->LRU
		       && ((Header->Flags & AssetFlag_INUSE)
			   || (Header->Flags & AssetFlag_LOCKED)))
		{
			Header = Header->LRUPrev;
		}

		if (Header != &Assets->LRU)
		{
			asset_info *Info = GetAssetInfo(Assets, Header->Id);
			char const *TypeName = AssetTypeName[Type];
			idx Id = Header->Id.ToIdx;

			YASSERTF(Header == Info->Header,
				 "%lx == %lx",
				 (ptr) Header,
				 (ptr) Info->Header);
			asset_header *Prev = Header->LRUPrev;
			if (InterlockedCompareSwap(&Info->Status,
						   AssetStatus_LOADED,
						   AssetStatus_ZERO))
			{
				FormatOut("[ASSETS]  Removing {char[*]}: {i64}\n",
					  TypeName, Id);
				LRURemove(Header);
				SlowFree(Header);

			}
			Header = Prev;
                        Result = SlowAlloc(&Assets->Data, CompleteSize);
		} else {
			return NULL;
		}
	}

	Result->Type = Type;
	Result->Size = Size;
	Result->Id   = Id;
	Result->Data.Raw = (byte *) NEXTALIGNED(PTROFFSET(Result, sizeof(asset_header)), Alignment);

	DEBUG_DO(
		Result->LRUNext = NULL;
		Result->LRUPrev = NULL;
		);

	YASSERTF((ptr) Result->Data.Raw - (ptr) PTROFFSET(Result, sizeof(asset_header))
		 < Alignment,
		 "%lx - %lx < %lu",
		 (ptr) Result->Data.Raw,
		 (ptr) PTROFFSET(Result, sizeof(asset_header)),
		 Alignment);

	YASSERTF((ptr) PTROFFSET(Result, sizeof(asset_header)) - (ptr) Result
		 == sizeof(asset_header),
		 "%lx - %lx == %lu",
		 (ptr) PTROFFSET(Result, sizeof(asset_header)),
		 (ptr) Result,
		 sizeof(asset_header));

	LRUAdd(Assets, Result);

	return Result;
}

PRIVATE asset_id
FirstAssetOfType(asset_store const *Assets, category Category, asset_type Type)
{
	YASSERT(Type < AssetType_COUNT);
	YASSERT(Category < Category_COUNT);
	asset_id Id = INVALID_ASSET_ID;

	category_info Info = Assets->CategoryInfo[Category];

	if (Info.Assets[Type].Begin < Info.Assets[Type].End)
	{
		Id = AssetID(Info.Assets[Type].Begin);
	}

	return Id;
}

PRIVATE asset_id
RandomAssetOfType(asset_store const *Assets, category Category, rand64 *Series,
		  asset_type Type)
{
	YASSERT(Type < AssetType_COUNT);
	YASSERT(Category < Category_COUNT);
	asset_id AssetId = INVALID_ASSET_ID;
	category_info Info = Assets->CategoryInfo[Category];

	if (Info.Assets[Type].Begin < Info.Assets[Type].End)
	{
		i32 RandomId = (i32) Info.Assets[Type].Begin
			+ NextInRange(Series, (u32) (Info.Assets[Type].End - Info.Assets[Type].Begin));
		AssetId = AssetID(RandomId);
	}

	return AssetId;
}

PRIVATE asset_id
ClosestAssetMatchOfType(asset_store const *Assets, category Category,
			f32 Weights[], f32 Desired[],
			asset_type Type)
{
	YASSERT(Type < AssetType_COUNT);
	YASSERT(Category < Category_COUNT);
	asset_id AssetId = INVALID_ASSET_ID;
	category_info Info = Assets->CategoryInfo[Category];
	f32 ClosestScore = F32_MAX;

	for (idx I = Info.Assets[Type].Begin;
	     I < Info.Assets[Type].End;
	     ++I)
	{
		f32 Score = 0;
		asset_id Id = AssetID(I);
		asset_info *Info = GetAssetInfo(Assets, Id);

		range Tags = Info->Tags;
		for (idx Tag = Tags.Begin;
		     Tag < Tags.End;
		     ++Tag)
		{
			i32 TagId    = Assets->Tags[Tag].Id;
			f32 TagValue = Assets->Tags[Tag].Value;
			Score += Weights[TagId] * Abs(Desired[TagId] - TagValue);
		}

		if (Tags.Begin != Tags.End && Score < ClosestScore)
		{
                        AssetId = Id;
			ClosestScore = Score;
		}
	}

	return AssetId;
}

PRIVATE asset_id
ExactAssetMatchOfType(asset_store const *Assets, category Category,
		      idx NumTags,
		      b32 Set[static NumTags],
		      f32 Desired[static NumTags],
		      f32 MaxError[static NumTags],
		      asset_type Type)
{
	YASSERT(Type < AssetType_COUNT);
	YASSERT(Category < Category_COUNT);
	asset_id AssetId = INVALID_ASSET_ID;
	category_info Info = Assets->CategoryInfo[Category];

	i32 NumSet = 0;
	for (idx I = 0;
	     I < NumTags;
	     ++I)
	{
		if (Set[I]) NumSet += 1;
	}

	for (idx I = Info.Assets[Type].Begin;
	     I < Info.Assets[Type].End;
	     ++I)
	{
		b32 CouldMatch = YARPG_TRUE;
		i32 Sets  = 0;
		asset_id Id = AssetID(I);
		asset_info *Info = GetAssetInfo(Assets, AssetID(Id.ToIdx));

		range Tags = Info->Tags;
		for (idx Tag = Tags.Begin;
		     Tag < Tags.End;
		     ++Tag)
		{
			i32 TagId    = Assets->Tags[Tag].Id;
			f32 TagValue = Assets->Tags[Tag].Value;
			if (Set[TagId])
			{
				Sets += 1;
                                CouldMatch &= Abs(Desired[TagId] - TagValue)
					< MaxError[TagId];
			}

		}

		if (CouldMatch && Sets == NumSet)
		{
			AssetId = Id;
			break;
		}
	}

	return AssetId;
}

sprite_id
FirstSpriteOf(asset_store const *Assets, category Category)
{
	return FirstAssetOfType(Assets, Category, AssetType_SPRITE).Sprite;
}

sprite_id
RandomSpriteOf(asset_store const *Assets, category Category, rand64 *Series)
{
	return RandomAssetOfType(Assets, Category, Series, AssetType_SPRITE).Sprite;
}

sprite_id
ClosestSpriteMatchOf(asset_store const *Assets, category Category,
		      f32 Weights[SpriteTag_COUNT], f32 Desired[SpriteTag_COUNT])
{
	return ClosestAssetMatchOfType(Assets, Category, Weights, Desired, AssetType_SPRITE).Sprite;
}

sprite_id
ExactSpriteMatchOf(asset_store const *Assets, category Category,
		    b32 Set[SpriteTag_COUNT],
		    f32 Desired[SpriteTag_COUNT],
		    f32 MaxError[SpriteTag_COUNT])
{
	return ExactAssetMatchOfType(Assets, Category, SpriteTag_COUNT,
				     Set, Desired, MaxError, AssetType_SPRITE).Sprite;
}

sound_id
FirstSoundOf(asset_store const *Assets, category Category)
{
	return FirstAssetOfType(Assets, Category, AssetType_SOUND).Sound;
}

sound_id
RandomSoundOf(asset_store const *Assets, category Category, rand64 *Series)
{
	return RandomAssetOfType(Assets, Category, Series, AssetType_SOUND).Sound;
}

sound_id
ClosestSoundMatchOf(asset_store const *Assets, category Category,
		    f32 Weights[SoundTag_COUNT], f32 Desired[SoundTag_COUNT])
{
	return ClosestAssetMatchOfType(Assets, Category, Weights, Desired, AssetType_SOUND).Sound;
}

sound_id
ExactSoundMatchOf(asset_store const *Assets, category Category,
		  b32 Set[SoundTag_COUNT],
		  f32 Desired[SoundTag_COUNT],
		  f32 MaxError[SoundTag_COUNT])
{
	return ExactAssetMatchOfType(Assets, Category, SoundTag_COUNT,
				     Set, Desired, MaxError, AssetType_SOUND).Sound;
}

text_id
FirstTextOf(asset_store const *Assets, category Category)
{
	return FirstAssetOfType(Assets, Category, AssetType_TEXT).Text;
}

text_id
RandomTextOf(asset_store const *Assets, category Category, rand64 *Series)
{
	return RandomAssetOfType(Assets, Category, Series, AssetType_TEXT).Text;
}

text_id
ClosestTextMatchOf(asset_store const *Assets, category Category,
		   f32 Weights[TextTag_COUNT], f32 Desired[TextTag_COUNT])
{
	return ClosestAssetMatchOfType(Assets, Category, Weights, Desired, AssetType_TEXT).Text;
}

text_id
ExactTextMatchOf(asset_store const *Assets, category Category,
		 b32 Set[TextTag_COUNT],
		 f32 Desired[TextTag_COUNT],
		 f32 MaxError[TextTag_COUNT])
{
	return ExactAssetMatchOfType(Assets, Category, TextTag_COUNT,
				     Set, Desired, MaxError, AssetType_TEXT).Text;
}

asset_info *
GetAssetInfo(asset_store const *Assets, asset_id Asset)
{
	YASSERT(ValidID(Asset));
	asset_info *Result = &Assets->AssetInfos[Asset.ToIdx];

	return Result;
}

PRIVATE void
LoadAssetData(asset_store *Assets, asset_location Location, byte *FileContents)
{
	INSTRUMENT(LoadAssetData)
	{
		u32 NumBytes = (u32) (Location.End - Location.Start);

		idx FileId = Location.FileId;
		YASSERTF(FileId >= 0 && FileId < Assets->NumFiles,
			 "%ld", FileId);
		ReadFile(&Assets->Files[FileId], Location.Start, NumBytes, FileContents);
	}
}

sprite_info *
GetSpriteInfo(asset_store const *Assets, sprite_id Sprite)
{
	idx Index = Sprite.ToIdx;

	range SpriteRange = Assets->AssetIdRangeByType[AssetType_SPRITE];

	YASSERTF(SpriteRange.Begin <= Index
		 && Index < SpriteRange.End,
		 "%ld <= %ld < %ld",
		 SpriteRange.Begin,
		 Index,
		 SpriteRange.End);

	idx SpriteIndex = Index - SpriteRange.Begin;

	sprite_info *Result = &Assets->SpriteInfos[SpriteIndex];

	return Result;
}

sound_info *
GetSoundInfo(asset_store const *Assets, sound_id Sound)
{
	idx Index = Sound.ToIdx;

	range SoundRange = Assets->AssetIdRangeByType[AssetType_SOUND];

	YASSERTF(SoundRange.Begin <= Index
		 && Index < SoundRange.End,
		 "%ld <= %ld < %ld",
		 SoundRange.Begin,
		 Index,
		 SoundRange.End);

	idx SoundIndex = Index - SoundRange.Begin;

	sound_info *Result = &Assets->SoundInfos[SoundIndex];

	return Result;
}

text_info *
GetTextInfo(asset_store const *Assets, text_id Text)
{
	idx Index = Text.ToIdx;

	range TextRange = Assets->AssetIdRangeByType[AssetType_TEXT];

	YASSERTF(TextRange.Begin <= Index
		 && Index < TextRange.End,
		 "%ld <= %ld < %ld",
		 TextRange.Begin,
		 Index,
		 TextRange.End);

	idx TextIndex = Index - TextRange.Begin;

	text_info *Result = &Assets->TextInfos[TextIndex];

	return Result;
}

PRIVATE void
FinalizeData(asset_store *Assets, byte *Data, asset_type Type, asset_id Id)
{
	switch (Type)
	{
		break;case AssetType_SPRITE:
		{
			sprite_info *SpriteInfo = GetSpriteInfo(Assets, Id.Sprite);

			pixel *PixelData = (pixel *) Data;

			YASSERTF((ptr) PixelData % alignof(pixel) == 0,
				 "%lu mod %lu == %lu",
				 (ptr) PixelData,
				 alignof(pixel),
				 (ptr) PixelData % alignof(pixel));

			image Image;
			Image.Width  = (u32) SpriteInfo->Width;
			Image.Height = (u32) SpriteInfo->Height;
			Image.Data   = PixelData;

			PremultiplyAlpha(Image);
		}
		break;case AssetType_TEXT:
		{

		}
		break;case AssetType_SOUND:
		{

		}
		INVALIDCASE(AssetType_COUNT);
		INVALIDDEFAULT();
	}
}

PRIVATE void
LoadAssetDataInto(asset_store *Assets, asset_type Type, asset_id Id, byte *Buffer)
{
	asset_info *AssetInfo = GetAssetInfo(Assets, Id);
	LoadAssetData(Assets, AssetInfo->Location, Buffer);
	FinalizeData(Assets, Buffer, Type, Id);
}

void
LoadTextDataInto(asset_store *Assets, text_id Text, byte *Buffer)
{
	LoadAssetDataInto(Assets, AssetType_TEXT, AssetID(Text.ToIdx), Buffer);
}

PRIVATE TASK_CALLBACK_FN(FetchAssetTask)
{
	fetch_asset_task *Task     = *(fetch_asset_task * volatile *) Arg;
	asset_store  *Assets       = Task->Assets;
	asset_header *Header       = Task->Header;
	byte         *Data         = Header->Data.Raw;

	asset_info *AssetInfo = GetAssetInfo(Assets, Header->Id);
	LoadAssetDataInto(Assets, Header->Type, Header->Id, Data);

	/* ---------- */ COMPILERBARRIER(); /* ---------- */

	YASSERT(InterlockedCompareSwap(&AssetInfo->Status,
				       AssetStatus_QUEUED,
				       AssetStatus_LOADED));

	AssetPopTemp(Assets); /* for Task */
}

PRIVATE void
FetchAsset(asset_store *Assets,
	   asset_id Id,
	   asset_type Type)
{
	if (ValidID(Id))
	{
		asset_info *Info = GetAssetInfo(Assets, Id);

		idx AssetSize = Info->Location.End - Info->Location.Start;

		idx AssetAlignment = 0;

		switch (Type)
		{
			// maybe always have a minimum alignment
			// of __m128 for better aligned access?
			break;case AssetType_SPRITE:
			{
				AssetAlignment = alignof(pixel);
			}

			break;case AssetType_SOUND:
			{
				AssetAlignment = alignof(f32);
			}

			break;case AssetType_TEXT:
			{
				AssetAlignment = alignof(char);
			}
			INVALIDCASE(AssetType_COUNT);
			INVALIDDEFAULT();
		}

		if (InterlockedCompareSwap(&Info->Status, AssetStatus_ZERO, AssetStatus_QUEUED))
		{
			asset_header *Header;

			fetch_asset_task *Task;

			if ((Task = (fetch_asset_task *) AssetPushTemp(Assets, sizeof(fetch_asset_task)))
			    && (Header = AllocAsset(Assets, Type, Id,
						    AssetSize, AssetAlignment)))
			{
				u32 NumBytes = (u32) (Info->Location.End - Info->Location.Start);

				YASSERTF((idx) NumBytes == AssetSize,
					 "%u == %ld",
					 NumBytes, AssetSize);

				Info->Header = Header;
				Task->Assets = Assets;
				Task->Header = Header;

				YASSERT(Task->Header != NULL);

				/* ---------- */ COMPILERBARRIER(); /* ---------- */

				PushLowPriorityTask(FetchAssetTask, Task);
			} else {
				AssetPopTemp(Assets);
				YASSERT(InterlockedCompareSwap(&Info->Status,
							       AssetStatus_QUEUED,
							       AssetStatus_ZERO));
			}
		}
	}
}

void
FetchSprite(asset_store *Assets, sprite_id Sprite)
{
	FetchAsset(Assets, AssetID(Sprite.ToIdx), AssetType_SPRITE);
}

b32
TryLoadSprite(asset_store *Assets, sprite_id Sprite, image *ToLoad)
{
	b32 Result = YARPG_FALSE;
	if (ValidID(Sprite))
	{
		asset_info *Info = GetAssetInfo(Assets, AssetID(Sprite.ToIdx));

		if (InterlockedRead(&Info->Status) == AssetStatus_LOADED)
		{
			asset_header *Header = Info->Header;

			sprite_info *SpriteInfo = GetSpriteInfo(Assets, Sprite);

			YASSERT(Header);
			YASSERT(Header->Type == AssetType_SPRITE);
			YASSERT(Header->Id.ToIdx == Sprite.ToIdx);

			pixel *PixelData = Header->Data.Pixels;

			ToLoad->Width  = (u32) SpriteInfo->Width;
			ToLoad->Height = (u32) SpriteInfo->Height;

                        // TODO: should probably be a const_image or something like that
			ToLoad->Data   = PixelData;

			Result = YARPG_TRUE;

//			LRUTouch(Assets, Info->Header);
		}

	}

	return Result;
}

void
FetchText(asset_store *Assets, text_id Text)
{
	FetchAsset(Assets, AssetID(Text.ToIdx), AssetType_TEXT);
}

b32
TryLoadText(asset_store *Assets, text_id Text, text *ToLoad)
{
	b32 Result = YARPG_FALSE;

	if (ValidID(Text))
	{
		asset_info *Info = GetAssetInfo(Assets, AssetID(Text.ToIdx));

		if (InterlockedRead(&Info->Status) == AssetStatus_LOADED)
		{
			asset_header *Header = Info->Header;
			YASSERT(Header);
			YASSERT(Header->Type == AssetType_TEXT);
			YASSERT(Header->Id.ToIdx == Text.ToIdx);

			text_info *TextInfo = GetTextInfo(Assets, Text);

			char const *Text = Header->Data.Text;

			ToLoad->NumBytes = (u32) TextInfo->NumBytes;
			ToLoad->NumChars = (u32) TextInfo->NumChars;
			ToLoad->Data     = Text;
			Result  = YARPG_TRUE;

//			LRUTouch(Assets, Info->Header);
		}
	}
	return Result;
}

void
FetchSound(asset_store *Assets, sound_id Sound)
{
	FetchAsset(Assets, AssetID(Sound.ToIdx), AssetType_SOUND);
}

b32
TryLoadSound(asset_store *Assets, sound_id Sound, real_sound_buffer *ToLoad,
	     sound_id *NextPart)
{
	b32 Result = YARPG_FALSE;
	*NextPart = INVALID_SOUND_ID;

	if (ValidID(Sound))
	{
		asset_info *Info = GetAssetInfo(Assets, AssetID(Sound.ToIdx));

		if (InterlockedRead(&Info->Status) == AssetStatus_LOADED)
		{
			asset_header *Header = Info->Header;
			YASSERT(Header);
			YASSERT(Header->Type == AssetType_SOUND);
			YASSERT(Header->Id.ToIdx == Sound.ToIdx);

			sound_info *SoundInfo = GetSoundInfo(Assets, Sound);

			f32 *Samples = Header->Data.Samples;

			ToLoad->NumChannels   = SoundInfo->NumChannels;
			ToLoad->SampleRate    = SoundInfo->SampleRate;
			ToLoad->NumSamples    = SoundInfo->NumSamples;
			ToLoad->ChannelPitch  = sizeof(f32) * SoundInfo->NumSamples;

			ToLoad->Data = Samples;

                        if (SoundInfo->HasNext)
			{
				sound_id NextId = SoundID(Sound.ToIdx + 1);
				*NextPart = NextId;
			}

			Result = YARPG_TRUE;

//			LRUTouch(Assets, Info->Header);
		}
	}
	return Result;
}

PRIVATE void
FixLRU(asset_header *Header)
{
	Header->LRUNext->LRUPrev = Header;
	Header->LRUPrev->LRUNext = Header;
}

void
UnmarkUsedAll(asset_store *Assets)
{
	for (asset_header *Header = Assets->LRU.LRUNext;
	     Header != &Assets->LRU;
	     Header = Header->LRUNext)
	{
		Header->Flags &= ~AssetFlag_INUSE;
	}
}
