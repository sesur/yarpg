#include <util/hex.h>

static void
DrawUnitUi(ui_state *Ui, irect Bounds,
	   battle_movement *Movement,
	   unit_stats *Stats,
	   game_battlefield *Battle);

typedef struct { iv2 Positions[6]; } neighbors;

static neighbors
NeighborsOf(iv2 Pos)
{
	neighbors Neighbors;

	Neighbors.Positions[0] = Add(Pos, IV( 1,  0));
	Neighbors.Positions[1] = Add(Pos, IV( 0,  1));
	Neighbors.Positions[2] = Add(Pos, IV(-1,  0));
	Neighbors.Positions[3] = Add(Pos, IV( 0, -1));
	Neighbors.Positions[4] = Add(Pos, IV(-1,  1));
	Neighbors.Positions[5] = Add(Pos, IV( 1, -1));

	return Neighbors;
}

static void
ResetActions(action *Actions, battlefield *Field)
{
        YSASSERT(ActionType_NOACTION == 0);
	ZeroArray(Field->TileStorage.End, Actions);
}

static void
FillActions(action *Actions, battlefield *Field, i32 Id,
	    memory_arena *Scratch)
{
	ResetActions(Actions, Field);

	FWDDECLARE(stack_chunk);
	FWDDECLARE(costed_pos);

	struct costed_pos
	{
		iv2 Pos;
		f32 Cost;
	};

#define SCSIZE 128

	struct stack_chunk
	{
		stack_chunk *Next;
		costed_pos Pos[SCSIZE];
	};

	i64 Used = 0;
	i64 Free = 0;

	stack_chunk *PopOf, *PushTo, *Last;

#define SINIT(Stack) do {					\
		Stack = PushStruct(Scratch, *Stack);		\
		Stack->Next = NULL;				\
	} while (0)



#define SEMPTY() (PushTo == PopOf && Used == Free)
#define SPOP(P) do {						\
		if (PushTo == PopOf)				\
		{						\
			YASSERT(Used < Free);			\
			P = PopOf->Pos[Used++];			\
			if (Used == Free)			\
			{					\
				Used = Free = 0;		\
			}					\
		} else {					\
			P = PopOf->Pos[Used++];			\
			if (Used == ARRAYCOUNT(PopOf->Pos))	\
			{					\
				Used = 0;			\
				stack_chunk *Tmp = PopOf;	\
				YASSERT(Tmp->Next != NULL);	\
				PopOf = Tmp->Next;		\
				Tmp->Next = NULL;		\
				YASSERT(Last->Next == NULL);	\
				Last->Next = Tmp;		\
				Last = Last->Next;		\
			}					\
		}						\
	} while (0)



#define SPUSH(P) do {							\
		YASSERT(Free < (i64) ARRAYCOUNT(PushTo->Pos));		\
		PushTo->Pos[Free++] = P;					\
		if (Free == (i64) ARRAYCOUNT(PushTo->Pos))		\
		{							\
			Free = 0;					\
			if (PushTo == Last)				\
			{						\
				stack_chunk *Tmp;			\
				SINIT(Tmp);				\
				PushTo->Next = Tmp;			\
				PushTo = Last = Tmp;			\
			} else {					\
				YASSERT(PushTo->Next != NULL);		\
				PushTo = PushTo->Next;			\
			}						\
		}							\
	} while (0)

	SINIT(Last);

	PushTo = Last;
	PopOf = Last;

	unit_stats     *Stats = &Field->UnitStats[Id];
	battle_movement *Mvmt = &Field->Movement[Id];

	costed_pos StartPos;
	StartPos.Cost = 0;
	StartPos.Pos  = Field->BoardPosition[Id];

	SPUSH(StartPos);

	f32 MaxCost = (f32) (Mvmt->Range - Mvmt->Used);

	costed_pos Current;
	while (!SEMPTY())
	{
		SPOP(Current);
		iv2 CurrentPos = Current.Pos;
		f32 CurrentCost = Current.Cost;

		id_index *MaybeId = GetTileSlot(&Field->TileStorage, CurrentPos);
		YASSERT(MaybeId);
		id_index Id = *MaybeId;

		i64 Start = Id ? Field->TileStorage.NeighborsEnd[Id - 1] : 0;

		for (i64 I = Start;
		     I < Field->TileStorage.NeighborsEnd[Id];
		     ++I)
		{
			id_index NeighborId = Field->TileStorage.NeighborIds[I];
			YASSERT(NeighborId != -1);
			iv2 NeighborPos = Field->TileStorage.Position[NeighborId];

			tile_data *Data = &Field->TileStorage.Data[NeighborId];
			costed_pos Next;
			Next.Pos  = NeighborPos;
			Next.Cost = CurrentCost + TileCost(*Data);

			if (Data->IsImpassible) continue;
			if (!IsEmpty(Field, Next.Pos)) continue;
			if (Next.Cost > MaxCost) continue;

			/* YASSERT(Next.Pos.X < Field->Width); */
			/* YASSERT(Next.Pos.Y < Field->Height); */
			action *NextAction = &Actions[NeighborId];

			switch (NextAction->Type)
			{
				break;case ActionType_NOACTION:
				{
					NextAction->Type = ActionType_MOVEMENT;
					NextAction->Data.Movement.From = CurrentPos;
					NextAction->Data.Movement.MovCost = Next.Cost;
					SPUSH(Next);
				}
				break;case ActionType_MOVEMENT:
				{
					if (NextAction->Data.Movement.MovCost > Next.Cost)
					{
						NextAction->Data.Movement.From = CurrentPos;
						NextAction->Data.Movement.MovCost = Next.Cost;
						SPUSH(Next);
					}
				}
				break;case ActionType_ATTACK:
				{
					INVALIDCODEPATH();
				}
				break;default:
				{
					FormatOut("Type: {i32}, Pos: {v2}\n",
						  NextAction->Type, Next.Pos);
					YASSERT(0);
				}

			}
		}
	}

#undef SEMPTY
#undef SPOP
#undef SPUSH
#undef SCSIZE
}

static void
DrawCityUi(ui_state *Ui, game_world *Map, world_entity *City)
{
	world *World = &Map->World;

	enum CITY_UI_ID
	{
		INVALID_ID,
		CREATE_ARMY_BUTTON_ID,
	};

	transient_world_state *TranState = World->Transient;
	irect FrameBounds;
	FrameBounds.MinX = 0;
	FrameBounds.MinY = 0;
	FrameBounds.MaxX = Ui->FrameWidth;
	FrameBounds.MaxY = Ui->FrameHeight;

	irect Bounds = GridRegion(FrameBounds,
				  IV(6, 3),
				  IV(1, 2),
				  IV(5, 3));

	char const *DescString   = "Pop:";
	u32 DescLength   = 4;
	BeginWindow(Ui,
		    CITY_UI_ID, "<name of city>", &Bounds,
		    WindowOption_TITLE);

	irect DrawableBounds = Ui->DrawableBounds;

	irect InfoBounds = VerticalRegion(DrawableBounds, 5, 0, 1);
	irect InsideBounds = VerticalRegion(DrawableBounds, 5, 2, 4);

	str Buffer = TEMPSTR(50);

	u32 NumChars = FormatInto(&Buffer, VIEWC("{u32}"), City->Data.City.Population).WrittenSize;

	Label(Ui,
	      HorizontalRegion(InfoBounds, 50, 0, 20),
	      TextPosition_CENTERED,
	      DescLength, DescString, COLOR.White, 1.0);
	Label(Ui,
	      HorizontalRegion(InfoBounds, 50, 22, 50),
	      TextPosition_CENTERED, NumChars, Buffer.Data,
	      COLOR.White, 1.0);

	str_view ButtonStr = VIEWC("Create Army");

	Label(Ui,
	      HorizontalRegion(InsideBounds, 50, 1, 20),
	      TextPosition_LEFT,
	      (u32) ButtonStr.Size, ButtonStr.Data, COLOR.White, F32C(0.44));

	if (Button(Ui, CREATE_ARMY_BUTTON_ID,
		   HorizontalRegion(InsideBounds, 50, 25, 49),
		   0, NULL))
	{
		i32 NumSoldiers = 10; // soldiers to spawn

		if (City->Data.City.Population < (u32) NumSoldiers)
		{
			NumSoldiers = City->Data.City.Population;
		}

		i32 const SoldierCost = 3;

		if (SoldierCost * NumSoldiers > Map->PlayerGold)
		{
			NumSoldiers = Map->PlayerGold / SoldierCost;
		}

		if (NumSoldiers > 0)
		{
			f32 Color[3] = {F32C(0.3), F32C(0.8), F32C(0.2)};
			world_position P = City->Pos;
			stored_world_index ArmyIndex = AddArmy(World, P, Color, WorldAllegiance_OWNED);
			City->Pos = P;
			world_entity *Army = GetEntityByIndex(World, ArmyIndex);
			YASSERT(Army);

			City->Data.City.Population -= NumSoldiers;
			Army->Data.Army.NumSoldiers = NumSoldiers;
			Map->PlayerGold -= SoldierCost * NumSoldiers;

			rect Box = (rect) {
				.TopRight = V(F32C(+0.05) * (f32) NumSoldiers,
					      F32C(+0.05) * (f32) NumSoldiers),
				.BottomLeft = V(F32C(-0.05) * (f32) NumSoldiers,
						F32C(-0.05) * (f32) NumSoldiers)
			};

			Army->BoundingBox = Box;

			World->ShouldReload = YARPG_TRUE;

			FormatOut("[WORLD] Spawning army {i32}\n", ArmyIndex.ToI32);
		} else {
			FormatOut("[WORLD] Cannot spawn army :/\n");
		}
	}



	EndWindow(Ui);
}

static i32
ArmyUpkeep(army_data *Army)
{
	return (i32) (1.5 * Army->NumSoldiers);
}

static void
DrawArmyUi(ui_state *Ui, game_world *Map, world_entity *Army)
{
	world *World = &Map->World;
	transient_world_state *TranState = World->Transient;
	irect FrameBounds;
	FrameBounds.MinX = 0;
	FrameBounds.MinY = 0;
	FrameBounds.MaxX = Ui->FrameWidth;
	FrameBounds.MaxY = Ui->FrameHeight;

	irect Bounds = GridRegion(FrameBounds,
				  IV(20, 20),
				  IV(1, 5),
				  IV(5, 8));

	char const *String = "Army";
	u32 StringLength = 4;
	char const *DescString = "Count";
	u32 DescLength = 5;

	enum ARMY_UI_ID
	{
		INVALID_ID,
		DISBAND_BUTTON_ID,
		DETAILS_BUTTON_ID,
	};

	YASSERT(BeginWindow(Ui,
			    ARMY_UI_ID, "Army", &Bounds,
			    0));

	irect DrawableBounds = Ui->DrawableBounds;

	i32 VirtualRows = 20;

	Label(Ui,
	      VerticalRegion(DrawableBounds, VirtualRows, 0, 5),
	      TextPosition_CENTERED, StringLength,
	      String, COLOR.White, 1.0);

	irect InfoBounds = VerticalRegion(DrawableBounds, VirtualRows, 6, 10);

	char Buf[50];

	str Buffer = {
		.Capacity = ARRAYCOUNT(Buf),
		.Size     = 0,
		.Data     = Buf,
	};

	PrintU32Into(Reset(&Buffer), Army->Data.Army.NumSoldiers);

	Label(Ui,
	      HorizontalRegion(InfoBounds, 50, 0, 20),
	      TextPosition_CENTERED,
	      DescLength, DescString, COLOR.White, 1.0);
	Label(Ui,
	      HorizontalRegion(InfoBounds, 50, 22, 50),
	      TextPosition_CENTERED, (u32) Buffer.Size, Buffer.Data,
	      COLOR.White, 1.0);

	DescString = "Upkeep";
	DescLength = 6;
	InfoBounds = VerticalRegion(DrawableBounds, VirtualRows, 11, 15);

	PrintU32Into(Reset(&Buffer), ArmyUpkeep(&Army->Data.Army));

	Label(Ui,
	      HorizontalRegion(InfoBounds, 50, 0, 20),
	      TextPosition_CENTERED,
	      DescLength, DescString, COLOR.White, 1.0);
	Label(Ui,
	      HorizontalRegion(InfoBounds, 50, 22, 50),
	      TextPosition_CENTERED, (u32) Buffer.Size, Buffer.Data,
	      COLOR.White, 1.0);

	b32 CanDisband = (Army->Data.Army.Allegiance == WorldAllegiance_OWNED);

	irect ButtonBounds = VerticalRegion(DrawableBounds, VirtualRows, 16, 19);

	i32 Start = 1;

	if (CanDisband)
	{
		if (Button(Ui, DISBAND_BUTTON_ID,
			   HorizontalRegion(ButtonBounds, 30, 1, 14),
			   7, "Disband"))
		{
			Map->PlayerGold += Army->Data.Army.NumSoldiers * 2;
			stored_world_index Index = GetEntityIndex(World, Army);
			RemoveEntity(GetWorldChunk(World, Army->Pos.Chunk),
				     Index);
			World->ShouldReload = YARPG_TRUE;
			Game->Map.InputState.CurrentAction = ActionState_NOACTION;


		}

		Start = 16;
	}

	if (Button(Ui, DETAILS_BUTTON_ID,
		   HorizontalRegion(ButtonBounds, 30, Start, 29),
		   7, "Details"))
	{
                Map->ShowDetails = 1;
		Map->DetailsIndex = GetEntityIndex(World, Army);
	}

	EndWindow(Ui);
}

static plane_result
DrawWorldUi(ui_state *Ui, game_world *Map)
{
	plane_result Result;
	Result.IsHot = 0;
	Result.IsActive = 0;

	world *World = &Map->World;
	transient_world_state *TranState = World->Transient;

	irect FrameBounds;
	FrameBounds.MinX = 0;
	FrameBounds.MinY = 0;
	FrameBounds.MaxX = Ui->FrameWidth;
	FrameBounds.MaxY = Ui->FrameHeight;

	irect MainMenuBounds = GridRegion(FrameBounds, IV(11, 11), IV(3, 3), IV(8, 8));

	if (Map->MenuOpen
	    && (Map->MenuOpen = BeginWindow(Ui, MAIN_MENU_UI_ID, "Mainmenu",
					   &MainMenuBounds,
					   WindowOption_TITLE | WindowOption_CLOSABLE)))
	{
		enum MAIN_MENU_IDS
		{
			INVALID_ID,
			QUIT_BUTTON_ID,
		};

		i32 NumRows = 10;
		i32 CurrentRow = 9;

		irect Bounds = GridRegion(Ui->DrawableBounds, IV(11, NumRows+1),
					  IV(2, CurrentRow), IV(9, CurrentRow+1));

		CurrentRow += 1;

		if (Button(Ui, QUIT_BUTTON_ID, Bounds, 4, "Quit"))
		{
			Map->ShouldQuit = YARPG_TRUE;
			// quit
		}
	}
	EndWindow(Ui);

	if (Map->MenuOpen) return Result;

	irect DetailsBounds = GridRegion(FrameBounds, IV(100, 100),
					 IV(5, 5), IV(95, 95));

	if (Map->ShowDetails
	    && (Map->ShowDetails = BeginWindow(Ui, WORLD_DETAILS_UI_ID, "Details",
					      &DetailsBounds, WindowOption_TITLE | WindowOption_CLOSABLE)))
	{
		world_entity *Army = GetEntityByIndex(World, Map->DetailsIndex);
		YASSERT(Army);
		YASSERT(Army->Type == EntityType_ARMY);

		army_data *Data = &Army->Data.Army;

		i32 UnitCardX = 0;
		i32 UnitCardY = 0;

		irect Bounds = Ui->DrawableBounds;

		// TODO: real unit cards!
		battle_movement PlaceHolder;
		PlaceHolder.Range = 3;
		PlaceHolder.Used  = 0;

		for (u32 I = 0;
		     I < Data->NumSoldiers;
		     ++I)
		{
			irect UnitCardRegion = GridRegion(Bounds, IV(5, 4),
							  IV(UnitCardX, UnitCardY),
							  IV(UnitCardX + 1, UnitCardY + 1));

			DrawUnitUi(Ui, UnitCardRegion,
				   &PlaceHolder,
				   Data->Units + I,
				   NULL);

			UnitCardX += 1;
                        if (UnitCardX == 5)
			{
				UnitCardX = 0;
				UnitCardY += 1;
			}
		}
	}
	EndWindow(Ui);

	if (Map->ShowDetails) return Result;

	irect UiBounds = FrameBounds;

        if (BeginWindow(Ui, WORLD_UI_ID,
			"WorldUi", &UiBounds,
			WindowOption_TRANSPARENT))
	{
		enum WORLD_UI_IDS
		{
			INVALID_ID,
			PLANE_ID,
			MENU_BUTTON_ID,
			END_TURN_ID,
		};

		plane GamePlane = { .Id = PLANE_ID, .Bounds = FrameBounds };

		Result = Plane(Ui, GamePlane);

		plane_result PR = Plane(Ui, GamePlane);

		irect Bounds = GridRegion(Ui->DrawableBounds, IV(11, 11), IV(0, 0), IV(2, 1));

		if (Button(Ui, MENU_BUTTON_ID, Bounds, 4, "Menu"))
		{
			// toggle menu
			Map->MenuOpen ^= 1;
		}

		Bounds = GridRegion(Ui->DrawableBounds, IV(22, 11), IV(9, 0), IV(15, 1));

		//char Buf[5] = { 0 };

//		str Buffer = STATICSTR(5);
		str Buffer = TEMPSTR(5);

		PrintI32Into(Reset(&Buffer), Map->PlayerGold);

		Label(Ui, HorizontalRegion(Bounds, 14, 0, 3),
		      TextPosition_LEFT, 1, "G", COLOR.Gold, 1.0);
		Label(Ui, HorizontalRegion(Bounds, 14, 3, 14),
		      TextPosition_CENTERED, (u32) Buffer.Size, Buffer.Data, COLOR.Gold, 1.0);

		Bounds = GridRegion(Ui->DrawableBounds, IV(11, 11), IV(7, 10), IV(11, 11));

		if (Button(Ui, END_TURN_ID, Bounds, 8, "End Turn"))
		{
			FormatOut("[WORLD] End turn!\n");
			for (i32 Idx = 0;
			     Idx < World->NumLoadedEntities;
			     ++Idx)
			{
				world_entity* Base = World->LoadedEntities + Idx;


				switch (Base->Type)
				{
					break;case EntityType_CITY:
					{
						YSASSERT(sizeof(i32) >= sizeof(world_allegiance));
						i32 Val = (i32) Base->Data.City.Allegiance;
						Val = (Val + 1) % 4;
						Base->Data.City.Allegiance = (world_allegiance) Val;
					}
					break;case EntityType_ARMY:
					{
						YSASSERT(sizeof(i32) >= sizeof(world_allegiance));
						i32 Val = (i32) Base->Data.Army.Allegiance;
						Val = (Val + 1) % 4;
						Base->Data.Army.Allegiance = (world_allegiance) Val;
					}
					break;case EntityType_ERROR: case EntityType_WALL: case EntityType_BACKGROUND: ;
				}
			}
		}
	}
	EndWindow(Ui);

	if (Map->InputState.CurrentAction == ActionState_MOVING)
	{
		// active_world_entity *Entity = &World->ActiveEntities[];
		world_entity *Base
			= GetEntityByIndex(World,
					   Map->InputState.ActionData.Move.SelectedEntityIndex);

		switch (Base->Type)
		{
			break;case EntityType_CITY: DrawCityUi(Ui, Map, Base);
			break;case EntityType_ARMY: DrawArmyUi(Ui, Map, Base);

		case EntityType_ERROR:
		case EntityType_WALL:
		case EntityType_BACKGROUND: ;
		}
	}

	return Result;
}

static void
EndTurn(game_battlefield *Battle)
{
	battlefield *Field = &Battle->Field;
	for (i32 I = 0;
	     I < Field->NumEntities;
	     ++I)
	{
		Field->Movement[I].Used = 0;
		Field->HasAttacked[I]   = 0;
	}

	Battle->SelectedEntity = -1;

	Battle->TurnCount += 1;
	Battle->CurrentEntity = 0;
}

static void
Next(game_battlefield *Battle)
{
	i32 NextEntity = Battle->CurrentEntity;
	b32 ShouldEndTurn    = 0;

	do {
		NextEntity += 1;
		if (NextEntity >= Battle->Field.NumEntities)
		{
			ShouldEndTurn = 1;
			NextEntity = 0;
		}
		if (NextEntity == Battle->CurrentEntity) break;
	} while (Battle->Field.UnitStats[NextEntity].Flags.IsDead);

	if (ShouldEndTurn) EndTurn(Battle);

        Battle->CurrentEntity = NextEntity;

	if (Battle->Field.Allegiance[NextEntity] == 0)
	{
		Battle->SelectedEntity = NextEntity;
	} else {
		Battle->SelectedEntity = -1;
	}
	Battle->CurrentAction = SelectedBattlefieldAction_NOACTION;
}

static void
KillAllEnemies(game_battlefield *Battle)
{
	battlefield *Field = &Battle->Field;
	for (i32 Entity = 0;
	     Entity < Field->NumEntities;
	     ++Entity)
	{
		if (Field->Allegiance[Entity] != 0)
		{
			// TODO: maybe we sholud set IsDead to true here instead?
			// BFTeleportEntity(Field, Entity, INVALID_POS);
			Field->UnitStats[Entity].CurrentHealth = 0;
                }
	}
}

static void
DrawUnitUi(ui_state *Ui, irect Bounds,
	   battle_movement *Movement,
	   unit_stats *Stats,
	   game_battlefield *Battle)
{
	irect DescBounds = HorizontalRegion(Bounds, 2, 0, 1);
	irect VisBounds  = HorizontalRegion(Bounds, 2, 1, 2);

	i32 NumStats = 5;
	i32 CurrentStat = 0;

	str Buffer = TEMPSTR(50);

	combat_stat CStat = CalculateCombatStat(Stats);

	// Movement

	i32 PosMovement = Movement->Range - Movement->Used;

	FormatInto(Reset(&Buffer), VIEWC("{i32} / {i32}"),
		   (i32) PosMovement,
		   (i32) Movement->Range);

	str_view MvmntText = VIEWC("Movmnt");

	Label(Ui, VerticalRegion(DescBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_LEFT,
	      (u32) MvmntText.Size, MvmntText.Data,
	      COLOR.White, 1.0);
	Label(Ui, VerticalRegion(VisBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_CENTERED,
	      (u32) Buffer.Size, Buffer.Data,
	      COLOR.White, 1.0);


	CurrentStat += 1;
	// Movement

	v4 HBColor = V(0, 0, 0, 1);
	HBColor.G = (f32) Stats->CurrentHealth / (f32) Stats->MaxHealth;

	if (HBColor.G < F32C(0.0)) HBColor.G = F32C(0.0);
	else if (HBColor.G > F32C(1.0)) HBColor.G = F32C(1.0);

	HBColor.R = F32C(1.0) - HBColor.G;


	// HEALTH
	Label(Ui, VerticalRegion(DescBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_LEFT,
	      6, "Health",
	      COLOR.White, 1.0);
	irect HealthBounds = VerticalRegion(VisBounds, NumStats, CurrentStat, CurrentStat + 1);
	VerticalBar(Ui, HealthBounds,
		    0.0,
		    (f32) Stats->CurrentHealth,
		    (f32) Stats->MaxHealth, HBColor);

	FormatInto(Reset(&Buffer), VIEWC("{i32} / {i32}"),
		   (i32) Stats->CurrentHealth, (i32) Stats->MaxHealth);

	Text(Ui, HealthBounds,
	     TextPosition_CENTERED,
	     (u32) Buffer.Size, Buffer.Data,
	     COLOR.White, 1.0);

	CurrentStat += 1;
	// HEALTH

	// RANGE

	Label(Ui, VerticalRegion(DescBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_LEFT,
	      5, "Range",
	      COLOR.White, 1.0);
	irect RangeBounds = VerticalRegion(VisBounds, NumStats, CurrentStat, CurrentStat + 1);
	if (CStat.MinRange == CStat.MaxRange)
	{
		PrintI32Into(Reset(&Buffer), CStat.MinRange);
	} else {
		FormatInto(Reset(&Buffer), VIEWC("{i32} - {i32}"),
			   (i32)CStat.MinRange, (i32)CStat.MaxRange);
	}
	Label(Ui, RangeBounds,
	      TextPosition_CENTERED,
	      (u32) Buffer.Size, Buffer.Data,
	      COLOR.White, 1.0);

	CurrentStat += 1;
	// RANGE

	// WEIGHT

	Label(Ui, VerticalRegion(DescBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_LEFT,
	      6, "Weight",
	      COLOR.White, 1.0);

	PrintI32Into(Reset(&Buffer), Stats->Weight);
	Label(Ui, VerticalRegion(VisBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_CENTERED,
	      (u32) Buffer.Size, Buffer.Data,
	      COLOR.White, 1.0);

	CurrentStat += 1;

	// WEIGHT

	// ATK + DEF

	Label(Ui, VerticalRegion(DescBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_LEFT,
	      6, "Combat",
	      COLOR.White, 1.0);
	if (CStat.NumAttacks == 1)
	{
		FormatInto(Reset(&Buffer), VIEWC("{i32} | {i32}"),
			   (i32) CStat.Attack, (i32) CStat.Defense);
	} else {
		FormatInto(Reset(&Buffer), VIEWC("{i32} (x{i32})| {i32}"),
			   (i32) CStat.Attack,
			   (i32) CStat.NumAttacks,
			   (i32) CStat.Defense);
	}

	Label(Ui, VerticalRegion(VisBounds, NumStats, CurrentStat, CurrentStat + 1),
	      TextPosition_CENTERED,
	      (u32) Buffer.Size, Buffer.Data,
	      COLOR.White, 1.0);

	if (Battle)
	{
		/* game *Game = CONTAINEROF(Battle, game, Battle); */

		/* HeadTexture.ID = Game->Map.World.BackgroundTexture; */

		irect HeadBounds;
		HeadBounds.MinX = Bounds.MinX + 50;
		HeadBounds.MaxX = Bounds.MaxX - 50;
		HeadBounds.MinY = Bounds.MaxY;
		HeadBounds.MaxY = Bounds.MaxY + 200;

//		Texture(Ui, HeadBounds, Battle->Head);
		Border(Ui, HeadBounds, 5, COLOR.Black);
	}


	CurrentStat += 1;

	// ATK + DEF
}

PRIVATE plane_result
DrawBattlefieldUi(game_battlefield *Battle, ui_state *Ui,
		  render_group *RenderGroup,
		  memory_arena *Scratch)
{
	plane_result Result;
	Result.IsHot = 0;
	Result.IsActive = 0;

	battlefield *Field = &Battle->Field;

	// Draw Cursor
	v4 TransWhite = V(1, 1, 1, 0.5);

	v2 MousePos = ToFloat(Battle->MousePos);

	PushHexagon(RenderGroup, HexToLinear(MousePos),
		    1.0, TransWhite);
	// Draw UI

	i32 NumAtk = 0;
	i32 NumDef = 0;

        for (i32 I = 0;
	     I < Field->NumEntities;
	     ++I)
	{
		if (Field->UnitStats[I].CurrentHealth <= 0) continue;
		if (Field->Allegiance[I] == 0)
		{
			NumAtk += 1;
		} else if (Field->Allegiance[I] == 1) {
			NumDef += 1;
		} else {
			INVALIDCODEPATH();
		}
	}

	irect FrameBounds;
	FrameBounds.MinX = 0;
	FrameBounds.MinY = 0;
	FrameBounds.MaxX = Ui->FrameWidth;
	FrameBounds.MaxY = Ui->FrameHeight;

	if (Field->BattleOver)
	{
		irect EndBattleBounds = GridRegion(FrameBounds, IV(10, 10), IV(2, 2), IV(8, 8));
		BeginWindow(Ui, BATTLEFIELD_END_BATTLE_UI_ID, "End battle",
			    &EndBattleBounds, WindowOption_TITLE);

		enum END_BATTLE_IDS
		{
			INVALID_ID,
			CLOSE_BUTTON_ID,
		};

		irect DrawableBounds = Ui->DrawableBounds;

		if (Button(Ui, CLOSE_BUTTON_ID,
		       GridRegion(DrawableBounds,
				  IV(10, 10),
				  IV(3, 8),
				  IV(7, 9)),
		       5, "Close"))
		{
			Battle->Closed = YARPG_TRUE;
		}

		EndWindow(Ui);
	} else {
		if (BeginWindow(Ui, BATTLEFIELD_MAIN_UI_ID, "Battlefield UI",
				&FrameBounds, WindowOption_TRANSPARENT))
		{
			enum BATTLEFIELD_UI_ID
				{
					INVALID_ID,
					PLANE_ID,
					NEXT_BUTTON_ID,
					END_BATTLE_BUTTON_ID,
					KILL_ALL_BUTTON_ID,
					MOVE_BUTTON_ID,
					ATTACK_BUTTON_ID,
					CANCEL_BUTTON_ID,
				};

			plane GamePlane = { .Id = PLANE_ID, .Bounds = FrameBounds };

			Result = Plane(Ui, GamePlane);

			irect DrawableBounds = Ui->DrawableBounds;

			SetHotkey(Ui, NEXT_BUTTON_ID, Key(Key_N));
			if (Button(Ui, NEXT_BUTTON_ID, GridRegion(DrawableBounds, IV(50, 50),
								  IV(40, 47), IV(50, 50)),
				   4, "Next"))
			{
				Next(Battle);
				ResetActions(Battle->Actions, Field);
			}

			if (Button(Ui, END_BATTLE_BUTTON_ID, GridRegion(DrawableBounds, IV(50, 50),
									IV(0, 0), IV(10, 3)),
				   10, "End Battle"))
			{
				Field->BattleOver = YARPG_TRUE;
			}

			if (Button(Ui, KILL_ALL_BUTTON_ID, GridRegion(DrawableBounds, IV(50, 50),
								      IV(40, 0), IV(50, 3)),
				   8, "Kill all"))
			{
				KillAllEnemies(Battle);
			}

			str Buffer = TEMPSTR(15);

			PrintI32Into(Reset(&Buffer), NumAtk);

			Label(Ui, GridRegion(DrawableBounds, IV(50, 50),
					     IV(5, 5), IV(10, 8)),
			      TextPosition_CENTERED,
			      (u32) Buffer.Size, Buffer.Data,
			      COLOR.White, 1.0);

			PrintI32Into(Reset(&Buffer), NumDef);

			Label(Ui, GridRegion(DrawableBounds, IV(50, 50),
					     IV(40, 5), IV(45, 8)),
			      TextPosition_CENTERED,
			      (u32) Buffer.Size, Buffer.Data,
			      COLOR.White, 1.0);

			PrintI32Into(Reset(&Buffer), Battle->TurnCount);

			Label(Ui, GridRegion(DrawableBounds, IV(50, 50),
					     IV(23, 0), IV(27, 3)),
			      TextPosition_CENTERED,
			      (u32) Buffer.Size, Buffer.Data,
			      COLOR.White, 1.0);

			i32 DetailedIdx = Battle->HoveredEntity;

			if (DetailedIdx < 0) {
				DetailedIdx = Battle->SelectedEntity;
			}

			if (DetailedIdx >= 0)
			{
				battle_movement *Movement = &Field->Movement[DetailedIdx];
				unit_stats      *Stats    = &Field->UnitStats[DetailedIdx];

				DrawUnitUi(Ui,
					   GridRegion(DrawableBounds, IV(50, 50),
						      IV(0, 35), IV(18, 50)),
					   Movement,
					   Stats,
					   Battle);
			}


			if (Battle->SelectedEntity >= 0)
			{
				battle_movement *Movement = &Field->Movement[Battle->SelectedEntity];
				unit_stats      *Stats    = &Field->UnitStats[Battle->SelectedEntity];

				switch (Battle->CurrentAction)
				{
					break;case SelectedBattlefieldAction_NOACTION:
					{
						if (Field->Allegiance[Battle->SelectedEntity]
						    == 0 &&
						    Battle->SelectedEntity == Battle->CurrentEntity)
						{
							if (Movement->Used < Movement->Range)
							{
								SetHotkey(Ui, MOVE_BUTTON_ID,
									  Key(Key_M));
								if (Button(Ui, MOVE_BUTTON_ID,
									   GridRegion(DrawableBounds, IV(50, 50),
										      IV(20, 41), IV(30, 44)),
									   4, "Move"))
								{
									FillActions(Battle->Actions,
										    Field,
										    Battle->SelectedEntity,
										    Scratch);
									Battle->CurrentAction = SelectedBattlefieldAction_MOVEMENT;
								}
							} else {
								Label(Ui, GridRegion(DrawableBounds, IV(50, 50),
										     IV(20, 41), IV(30, 44)),
								      TextPosition_CENTERED,
								      4, "Move", COLOR.White, 1.0);
							}

							if (Field->HasAttacked[Battle->SelectedEntity])
							{
								Label(Ui, GridRegion(DrawableBounds, IV(50, 50),
										     IV(20, 44), IV(30, 47)),
								      TextPosition_CENTERED,
								      6, "Attack", COLOR.White, 1.0);
							} else {
								SetHotkey(Ui, ATTACK_BUTTON_ID,
									  Key(Key_A));
								if (Button(Ui, ATTACK_BUTTON_ID, GridRegion(DrawableBounds, IV(50, 50),
													    IV(20, 44), IV(30, 47)),
									   6, "Attack"))
								{
									Battle->CurrentAction = SelectedBattlefieldAction_ATTACK;
								}
							}
						}

						SetHotkey(Ui, CANCEL_BUTTON_ID,
							  Key(Key_ESCAPE));
						if (Button(Ui, CANCEL_BUTTON_ID,
							   GridRegion(DrawableBounds, IV(50, 50),
								      IV(20, 47), IV(30, 50)),
							   6, "Cancel"))
						{
							Battle->SelectedEntity = -1;
						}

						if (!IsBusy(&Battle->Field, Battle->CurrentEntity))
						{
							v4 TransYellow = Scale(F32C(0.8), COLOR.Yellow);
							PushHexagon(RenderGroup,
								    Field->DrawnPosition[Battle->CurrentEntity],
								    F32C(0.8), TransYellow);
						}


					}
					break;case SelectedBattlefieldAction_MOVEMENT:
					{
						iv2 BoardPosition = Field->BoardPosition[Battle->SelectedEntity];

						v4 HighlightColor = V(0.3, 0.2, 0.4, 0.5);
						v4 Yellow         = V(0.8, 0.8, 0.0, 1.0);

						/* FormatOut("[UI] MousePos = ({i32}, {i32})", */
						/* 	  Battle->MousePos.X, */
						/* 	  Battle->MousePos.Y); */

						for (i64 Idx = 0;
						     Idx < Field->TileStorage.End;
						     ++Idx)
						{
							iv2 HexPos    = Field->TileStorage.Position[Idx];
							action Action = Battle->Actions[Idx];
							switch (Action.Type)
							{
								break;case ActionType_NOACTION:
								{

								}
								break;case ActionType_MOVEMENT:
								{
									PushHexagon(RenderGroup, HexToLinear(ToFloat(HexPos)),
										    F32C(0.8), HighlightColor);
								}
								break;case ActionType_ATTACK:
								{

								}
							}
						}

						iv2 Pos = Battle->MousePos;

						id_index *Slot = GetTileSlot(&Field->TileStorage,
									     Pos);

						if (Slot)
						{
							action PosAction = Battle->Actions[*Slot];
							if (PosAction.Type == ActionType_MOVEMENT)
							{
								do
								{
									PushLine(RenderGroup, HexToLinear(ToFloat(Pos)),
										 HexToLinear(ToFloat(PosAction.Data.Movement.From)), F32C(0.005), Yellow);
									Pos = PosAction.Data.Movement.From;
									id_index *Slot = GetTileSlot(&Field->TileStorage,
												     Pos);
									YASSERT(Slot);
									PosAction = Battle->Actions[*Slot];
								} while (!Eq(Pos, BoardPosition));
							}
						}

						SetHotkey(Ui, CANCEL_BUTTON_ID, Key(Key_ESCAPE));
						if (Button(Ui, CANCEL_BUTTON_ID,
							   GridRegion(DrawableBounds, IV(50, 50),
								      IV(20, 47), IV(30, 50)),
							   6, "Cancel"))
						{
							Battle->CurrentAction = SelectedBattlefieldAction_NOACTION;
						}
					}
					break;case SelectedBattlefieldAction_ATTACK:
					{
						iv2 BoardPosition = Field->BoardPosition[Battle->SelectedEntity];

						v4 HighlightColor = V(0.6, 0.2, 0.1, 0.5);

						combat_stat CStat = CalculateCombatStat(Stats);

						i32 Min = CStat.MinRange;
						i32 Max = CStat.MaxRange;

						for (i32 X = -Max;
						     X <= Max;
						     ++X)
						{
							for (i32 Y = MaxI32(-Max, -Max-X);
							     Y <= MinI32(Max, Max-X);
							     ++Y)
							{
								if (X == 0 && Y == 0) continue;

								iv2 IHexPos = Add(BoardPosition,
										  IV(X, Y));

								if (!ValidHex(Field, IHexPos)) continue;

								if (IsEmpty(Field, IHexPos)) continue;

								i32 EntityIdx = GetIndexOfPos(Field, IHexPos);
								YASSERT(EntityIdx >= 0);

								if (Field->Allegiance[EntityIdx] == 0) continue;

								if (Field->UnitStats[EntityIdx].Flags.IsDead) continue;

								if (HexDistance(IHexPos, BoardPosition) < Min) continue;

								v2 HexPos = ToFloat(IHexPos);

								PushHexagon(RenderGroup, HexToLinear(HexPos),
									    F32C(0.8), HighlightColor);
							}

						}
						if (Button(Ui, CANCEL_BUTTON_ID,
							   GridRegion(DrawableBounds, IV(50, 50),
								      IV(20, 47), IV(30, 50)),
							   6, "Cancel"))
						{
							Battle->CurrentAction = SelectedBattlefieldAction_NOACTION;
						}
					}
				}
			} else {
				v4 TransYellow = Scale(F32C(0.8), COLOR.Yellow);

				PushHexagon(RenderGroup,
					    Field->DrawnPosition[Battle->CurrentEntity],
					    F32C(0.8), TransYellow);
			}
		}
		EndWindow(Ui);
	}




	// NOTE: Not calling EndWindow here is on purpose:
	// its closed in the calling function

	return Result;
}
