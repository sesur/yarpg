#include <def.h>

#include <compiler.h>

#include "def.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <util/bmp.h>
#include "util/load_bmp.c"

#include <util/math.h>
#include "util/math.c"
#include "util/vector.c"

#include <util/wav.h>
#include "util/wav.c"

#include <yaff_format.h>
#include <util/asset_ids.h>

FWDDECLARE(asset_builder);
FWDDECLARE(vector);
FWDDECLARE(proto_asset);

struct proto_asset
{
	char const *File;
	range Tags;

	asset_type Type;

	union
	{
		// only used for sprites
		v2 Anchor;
		// only used for sounds
		idx MaxPartSize;
	};

	yaff_category_info *OfCategory;
};

struct vector
{
	idx Capacity;
	idx Size;
	idx MemberSize;
	byte *Data;
};

struct asset_builder
{
	vector CategoryIds;
	vector Categories;
	vector Sprites;
	vector Texts;
	vector Sounds;
	vector Tags;

	yaff_category_info *CurrentCategory;
	range                *CurrentTags;
};

static void
InitVector_(vector *V, idx Capacity, idx MemberSize)
{
	V->MemberSize = MemberSize;
	V->Size       = 0;
	V->Capacity   = Capacity;
	V->Data       = calloc(V->Capacity, V->MemberSize);

	YASSERT(V->Data);
}

static void *
VectorNext_(vector *V)
{
	if (V->Size + 1 == V->Capacity)
	{
		V->Capacity *= 2;
		V->Data = realloc(V->Data, V->Capacity * V->MemberSize);

		YASSERT(V->Data);
	}

	idx Index = V->Size++;

	return V->Data + (V->MemberSize) * Index;
}

#define InitVector(V, Type) InitVector_((V), 50, sizeof(Type))
#define InitVectorWithCap(V, Cap, Type) InitVector_((V), (Cap), sizeof(Type))
#define VectorNext(V, Type) (Type *) VectorNext_(V)

#define INVALID_INFO -1

static asset_builder
BeginBuilder(void)
{
	asset_builder Builder;

	InitVector(&Builder.CategoryIds, category);
	InitVector(&Builder.Categories,   yaff_category_info);
	InitVector(&Builder.Sprites,     proto_asset);
	InitVector(&Builder.Texts,        proto_asset);
	InitVector(&Builder.Sounds,       proto_asset);
	InitVector(&Builder.Tags,         yaff_asset_tag);

	Builder.CurrentCategory = NULL;
	Builder.CurrentTags = NULL;

	return Builder;
}

static void
ResetBuilder(asset_builder *Builder)
{

	Builder->CategoryIds.Size = 0;
	Builder->Categories.Size = 0;
	Builder->Sprites.Size = 0;
	Builder->Texts.Size = 0;
	Builder->Sounds.Size = 0;
	Builder->Tags.Size = 0;
	Builder->CurrentCategory = NULL;
	Builder->CurrentTags = NULL;
}

static void
BeginCategory(asset_builder *Builder, category Category)
{
	YASSERT((i64) Category < (i64) Category_COUNT);
	YASSERT(Builder->CurrentCategory == NULL);

        category *Prev = (category *) Builder->CategoryIds.Data;

	for (idx I = 0;
	     I < Builder->CategoryIds.Size;
	     ++I)
	{
		YASSERTF(Prev[I] != Category, "%u", Category);
	}


	category      *NextId      = VectorNext(&Builder->CategoryIds, category);
	yaff_category_info *Next   = VectorNext(&Builder->Categories, yaff_category_info);

	*NextId = Category;

	memset(Next->Assets, 0, sizeof(Next->Assets));
	Builder->CurrentCategory = Next;
}

static void
EndCategory(asset_builder *Builder)
{
	YASSERT(Builder->CurrentCategory != NULL);

	if (Builder->CurrentTags)
	{
		Builder->CurrentTags->End = Builder->Tags.Size;
	}

	Builder->CurrentCategory              = NULL;
	Builder->CurrentTags              = NULL;
}

static proto_asset *
AddProtoAsset(asset_builder *Builder, vector *V, asset_type Type, char const *File)
{
        if (Builder->CurrentTags)
	{
		Builder->CurrentTags->End = Builder->Tags.Size;
	}

	YASSERT(Builder->CurrentCategory);
	proto_asset *Next   = VectorNext(V, proto_asset);

	Next->File           = File;
	Next->Type           = Type;
	Next->MaxPartSize    = 0;
	Next->OfCategory         = Builder->CurrentCategory;
	Next->Tags.Begin     = Builder->Tags.Size;
	Next->Tags.End       = Builder->Tags.Size;

	Builder->CurrentTags = &Next->Tags;

	return Next;
}

static void
AddText(asset_builder *Builder, char const *File)
{
        AddProtoAsset(Builder, &Builder->Texts, AssetType_TEXT, File);
}

static void
AddSprite(asset_builder *Builder, char const *File, v2 Anchor)
{
	proto_asset *Asset = AddProtoAsset(Builder, &Builder->Sprites, AssetType_SPRITE, File);
	Asset->Anchor = Anchor;
}

static void
AddSound(asset_builder *Builder, char const *File, idx MaxPartSize)
{
	proto_asset *Asset = AddProtoAsset(Builder, &Builder->Sounds, AssetType_SOUND, File);
	Asset->MaxPartSize = MaxPartSize;

}

static void
AddTag(asset_builder *Builder, i32 Id, f32 Value)
{
	yaff_asset_tag *Next = VectorNext(&Builder->Tags, yaff_asset_tag);

	Next->Id    = Id;
	Next->Value = Value;


}

static void
Write(FILE *File, range Range, byte *Source)
{
	idx NumBytes = Range.End - Range.Begin;

	YASSERTF(NumBytes >= 0,
		 "%ld = %ld - %ld",
		 NumBytes, Range.End, Range.Begin);

	YSASSERT(sizeof(long) == sizeof(Range.Begin));

	fseek(File, Range.Begin, SEEK_SET);

	idx Written = fwrite(Source, 1, NumBytes, File);

	YASSERTF(Written == NumBytes,
		 "%ld <-> %ld", Written, NumBytes);
}

static yaff_header
MakeYAFFHeader(u64 FileSize, u64 DirectoryStart)
{
	YASSERTF(FileSize > DirectoryStart,
		 "%lu ?> %lu",
		 FileSize, DirectoryStart);
	yaff_header Header;
	Header.MagicNumber    = YAFF_MAGIC_NUMBER;
	Header.Version        = YAFF_VERSION;
	Header.DirectoryStart = DirectoryStart; // TODO
	Header.FileSize       = FileSize; // TODO

	return Header;
}

static yaff_directory
MakeYAFFDirectory(u32 NumAssets,
                  u32 NumTags,
		  u32 NumCategories,
		  range AssetIdRangeByType[static AssetType_COUNT],
		  range CategoryIdRange,
		  range CategoryInfoRange,
		  range AssetInfoRange,
		  range TypedAssetInfoRange[static AssetType_COUNT],
		  range TagsRange)
{
	yaff_directory Dir;

	Dir.MagicNumber = YAFF_DIR_MAGIC_NUMBER;

	Dir.NumAssets   = NumAssets;
	Dir.NumTags     = NumTags;
	Dir.NumCategories    = NumCategories;

	Dir.CategoryIdRange   = CategoryIdRange;
	Dir.CategoryInfoRange = CategoryInfoRange;
	Dir.AssetInfoRange     = AssetInfoRange;
	Dir.TagsRange	       = TagsRange;

	for (idx I = 0;
	     I < AssetType_COUNT;
	     ++I)
	{
		Dir.AssetIdRangeByType[I] = AssetIdRangeByType[I];
		Dir.TypedAssetInfoRange[I] = TypedAssetInfoRange[I];
	}

	return Dir;
}

static u64
WriteAssetHeader(FILE *File, u64 Cursor, yaff_header Header)
{
	range DataRange = R(Cursor, Cursor + sizeof(Header));
        Write(File, DataRange, (byte *) &Header);
	return DataRange.End;
}

static u64
WriteAssetDirectory(FILE *File, u64 Cursor, yaff_directory Dir)
{
	range DataRange = R(Cursor, Cursor + sizeof(Dir));
	Write(File, DataRange, (byte *) &Dir);
	return DataRange.End;
}

static u64
PopulateSpriteInfos(u64 Cursor, FILE *Out, u32 NumProtos, vector *Dest,
		     vector *SpriteInfo,
		     proto_asset Source[static NumProtos])
{
	for (u32 I = 0;
	     I < NumProtos;
	     ++I)
	{
		int File = open(Source[I].File, O_RDONLY);

		YASSERTF(File >= 0, "Error trying to open file '%s'",
			Source[I].File);

		struct stat Buffer;

		fstat(File, &Buffer);

		YASSERTF(Buffer.st_size >= 0
			 && Buffer.st_size <= U32_MAX,
			 "%ld", Buffer.st_size);

		u32 Length = Buffer.st_size;

		byte *Data = mmap(NULL, Length, PROT_READ, MAP_PRIVATE, File, 0);

		YASSERT(Data != MAP_FAILED);

		bmp_header Header;

		YASSERT(LoadBMPHeader(&Header, Data));

		u32 NumPixels = Header.DIB.BitmapWidth * Header.DIB.BitmapHeight;

		u32 Size = sizeof(pixel) * NumPixels;

		pixel *PixelData = malloc(Size);

		image Sprite;
		Sprite.Data = PixelData;
		YASSERT(LoadBMPData(&Header, Data, &Sprite));

		YASSERTF(Sprite.Width == Header.DIB.BitmapWidth,
			 "%u == %u",
			 Sprite.Width, Header.DIB.BitmapWidth);

		YASSERTF(Sprite.Height == Header.DIB.BitmapHeight,
			 "%u == %u",
			 Sprite.Height, Header.DIB.BitmapHeight);

		range DataRange = R(Cursor, Cursor + Size);

		Write(Out, DataRange, (byte *) PixelData);

		Cursor = DataRange.End;

		free(PixelData);

		YASSERT(!munmap(Data, Length));
		YASSERT(!close(File));

		yaff_sprite *Info = VectorNext(SpriteInfo, yaff_sprite);
		Info->Width  = Header.DIB.BitmapWidth;
		Info->Height = Header.DIB.BitmapHeight;
		Info->Anchor = Source[I].Anchor;

		yaff_asset_info *Current = VectorNext(Dest, yaff_asset_info);
		Current->DataRange = DataRange;
		Current->Tags      = Source[I].Tags;

		Source[I].OfCategory->Assets[AssetType_SPRITE].End += 1;
	}

	return Cursor;
}

PRIVATE u64
PopulateSoundInfos(u64 Cursor, FILE *Out, u32 NumProtos, vector *Dest,
		   vector *SoundInfo,
		   proto_asset Source[static NumProtos])
{
	idx MaxSamplesPerPart = Source->MaxPartSize;
	for (u32 I = 0;
	     I < NumProtos;
	     ++I)
	{
		int File = open(Source[I].File, O_RDONLY);

		YASSERT(File >= 0);

		struct stat Buffer;

		fstat(File, &Buffer);

		YASSERTF(Buffer.st_size >= 0
			 && Buffer.st_size <= U32_MAX,
			 "%ld", Buffer.st_size);

		u32 Length = Buffer.st_size;

		byte *Data = mmap(NULL, Length, PROT_READ, MAP_PRIVATE, File, 0);

		YASSERT(Data != MAP_FAILED);

		real_sound_buffer SoundBuf;
		wav_header WavHeader = LoadWAVHeader(Length, Data);

		u32 AllSamples = WavHeader.NumChannels * (WavHeader.ChannelPitch / sizeof(f32));
		SoundBuf.NumChannels  = WavHeader.NumChannels;
		SoundBuf.ChannelPitch = WavHeader.ChannelPitch;
		SoundBuf.SampleRate   = WavHeader.SampleRate;
		SoundBuf.NumSamples   = WavHeader.NumSamples;
		SoundBuf.Data         = malloc(AllSamples * sizeof(f32));
		LoadWAV(&SoundBuf, Length, Data);

		YASSERT(SoundBuf.NumChannels == 1);
		idx RemainingSamples = AllSamples;
		while (RemainingSamples > 0)
		{
			idx SamplesOfPart = Min(RemainingSamples, MaxSamplesPerPart);

			u32 AssetSize = sizeof(f32) * SamplesOfPart;

			yaff_sound *Info = VectorNext(SoundInfo, yaff_sound);

			Info->NumChannels  = WavHeader.NumChannels;
			Info->SampleRate   = WavHeader.SampleRate;
			Info->NumSamples   = SamplesOfPart;
			Info->HasNext      = SamplesOfPart != RemainingSamples;

			range DataRange = R(Cursor, Cursor + AssetSize);
			Write(Out, DataRange, (byte *) (SoundBuf.Data + (AllSamples - RemainingSamples)));

			Cursor = DataRange.End;

			yaff_asset_info *Current = VectorNext(Dest, yaff_asset_info);
			Current->DataRange        = DataRange;
			Source[I].OfCategory->Assets[AssetType_SOUND].End += 1;

			if (RemainingSamples == AllSamples)
			{
				Current->Tags = Source[I].Tags;
			} else {
				Current->Tags = R(0, 0);
			}

			RemainingSamples -= SamplesOfPart;
		}


		YASSERT(!munmap(Data, Length));
		YASSERT(!close(File));
	}

	return Cursor;
}

u32 CONSTFN
Utf8Bytes(char First)
{
	u32 Length = 0;

	if ((First >> 7) == 0) {
		/* First: 0xxxxxxx */
		Length = 1;
	} else if ((First >> 5) == 0b110) {
		/* First:  110xxxxx */
		/* Second: 10xxxxxx */
		Length = 2;
	} else if ((First >> 4) == 0b1110) {
		/* First:  1110xxxx */
		/* Second: 10xxxxxx */
		/* Third:  10xxxxxx */
		Length = 3;
	} else if ((First >> 3) == 0b11110) {
		/* First:  11110xxx */
		/* Second: 10xxxxxx */
		/* Third:  10xxxxxx */
		/* Fourth: 10xxxxxx */
		Length = 4;
	} else {
		INVALIDCODEPATH();
	}

	return Length;
}

PRIVATE u64
PopulateTextInfos(u64 Cursor, FILE *Out, u32 NumProtos, vector *Dest,
		  vector *TextInfo,
		  proto_asset Source[static NumProtos])
{
	for (u32 I = 0;
	     I < NumProtos;
	     ++I)
	{
		int File = open(Source[I].File, O_RDONLY);

                YASSERT(File >= 0);

		struct stat Buffer;

		fstat(File, &Buffer);

		YASSERTF(Buffer.st_size >= 0
			 && Buffer.st_size <= U32_MAX,
			 "%ld", Buffer.st_size);

		u32 Length = Buffer.st_size;

		char *Data = mmap(NULL, Length, PROT_READ, MAP_PRIVATE, File, 0);

		YASSERT(Data != MAP_FAILED);

		u32 CurrentPos = 0;
		u32 NumChars = 0;

		while (CurrentPos < Length)
		{
			NumChars   += 1;
			CurrentPos += Utf8Bytes(Data[CurrentPos]);
		}

		YASSERTF(CurrentPos == Length,
			 "%u == %u",
			 NumChars, CurrentPos);

		u32 Size = sizeof(char) * Length;

		yaff_text *Info = VectorNext(TextInfo, yaff_text);
		Info->NumChars = NumChars;
		Info->Length   = Length;

		range DataRange = R(Cursor, Cursor + Size);

		Write(Out, DataRange, (byte *) Data);

		Cursor = DataRange.End;

		YASSERT(!munmap(Data, Length));
		YASSERT(!close(File));

		yaff_asset_info *Current = VectorNext(Dest, yaff_asset_info);
		Current->DataRange = DataRange;
		Current->Tags      = Source[I].Tags;

		Source[I].OfCategory->Assets[AssetType_TEXT].End += 1;
	}
	return Cursor;
}

PRIVATE void
BuildAssetFile(asset_builder *Builder, char const *File)
{
	FILE *Out = fopen(File, "wb");

	YASSERT(Out);

	u64 Cursor = sizeof(yaff_header);

	vector AssetInfoV;
	vector TypedAssetInfoV[AssetType_COUNT];

	u32 NumAssets     = Builder->Sprites.Size + Builder->Texts.Size + Builder->Sounds.Size;
	u32 NumTags       = Builder->Tags.Size;
	u32 NumCategories = Builder->Categories.Size;

	InitVectorWithCap(&AssetInfoV, NumAssets, yaff_asset_info);

	InitVectorWithCap(&TypedAssetInfoV[AssetType_SPRITE],
			  Builder->Sprites.Size,
			  yaff_sprite);

	InitVectorWithCap(&TypedAssetInfoV[AssetType_SOUND],
			  Builder->Sounds.Size,
			  yaff_sound);

	InitVectorWithCap(&TypedAssetInfoV[AssetType_TEXT],
			  Builder->Texts.Size,
			  yaff_text);

	range AssetIdRangeByType[AssetType_COUNT];


	AssetIdRangeByType[AssetType_SPRITE].Begin = AssetInfoV.Size;
	Cursor = PopulateSpriteInfos(Cursor, Out,
				      Builder->Sprites.Size,
				      &AssetInfoV,
				      &TypedAssetInfoV[AssetType_SPRITE],
				      (proto_asset *) Builder->Sprites.Data);
	AssetIdRangeByType[AssetType_SPRITE].End = AssetInfoV.Size;

	AssetIdRangeByType[AssetType_SOUND].Begin = AssetInfoV.Size;
	Cursor = PopulateSoundInfos(Cursor, Out,
				    Builder->Sounds.Size,
				    &AssetInfoV,
				    &TypedAssetInfoV[AssetType_SOUND],
				    (proto_asset *) Builder->Sounds.Data);
	AssetIdRangeByType[AssetType_SOUND].End = AssetInfoV.Size;

	AssetIdRangeByType[AssetType_TEXT].Begin = AssetInfoV.Size;
	Cursor = PopulateTextInfos(Cursor, Out,
				   Builder->Texts.Size,
				   &AssetInfoV,
				   &TypedAssetInfoV[AssetType_TEXT],
				   (proto_asset *) Builder->Texts.Data);
	AssetIdRangeByType[AssetType_TEXT].End = AssetInfoV.Size;

	idx CurrentAsset[AssetType_COUNT];
	for (idx Type = 0;
	     Type < AssetType_COUNT;
	     ++Type)
	{
		CurrentAsset[Type] = AssetIdRangeByType[Type].Begin;

	}

	NumAssets = AssetInfoV.Size;

	YASSERT(NumCategories == Builder->CategoryIds.Size);

	yaff_asset_info    *AssetInfos    = (yaff_asset_info *) AssetInfoV.Data;
	yaff_asset_tag     *Tags          = (yaff_asset_tag *) Builder->Tags.Data;
	category           *CategoryIds   = (category *) Builder->CategoryIds.Data;
	yaff_category_info *CategoryInfos = (yaff_category_info *) Builder->Categories.Data;

	for (idx CatIdx = 0;
	     CatIdx < Builder->Categories.Size;
	     ++CatIdx)
	{
		yaff_category_info *Category = &CategoryInfos[CatIdx];

		printf("[%ld = %s] ",
		       CatIdx,
		       CategoryNames[CategoryIds[CatIdx]]);

		for (i32 Type = 0;
		     Type < AssetType_COUNT;
		     ++Type)
		{
			Category->Assets[Type].Begin += CurrentAsset[Type];
			Category->Assets[Type].End += CurrentAsset[Type];

			printf("%s: %ld - %ld ",
			       AssetTypeName[Type],
			       Category->Assets[Type].Begin,
			       Category->Assets[Type].End);

			CurrentAsset[Type] = Category->Assets[Type].End;
		}

		puts("");
	}

	range TagsRange = R(Cursor, Cursor + NumTags * sizeof(yaff_asset_tag));
	Cursor = TagsRange.End;

	range AssetInfoRange = R(Cursor, Cursor + NumAssets * sizeof(yaff_asset_info));
	Cursor = AssetInfoRange.End;

	range CategoryIdRange = R(Cursor, Cursor + NumCategories * sizeof(category));
	Cursor = CategoryIdRange.End;

	range CategoryInfoRange = R(Cursor, Cursor + NumCategories * sizeof(yaff_category_info));
	Cursor = CategoryInfoRange.End;

	u64 AssetInfoSize[AssetType_COUNT] =
	{
		[AssetType_SPRITE] = sizeof(yaff_sprite),
		[AssetType_SOUND] = sizeof(yaff_sound),
		[AssetType_TEXT] = sizeof(yaff_text),
	};

	range TypedAssetInfoRange[AssetType_COUNT];

	for (idx Type = 0;
	     Type < AssetType_COUNT;
	     ++Type)
	{
		YASSERT(TypedAssetInfoV[Type].Size == RangeLength(AssetIdRangeByType[Type]));

		TypedAssetInfoRange[Type] = R(Cursor, Cursor
					 + TypedAssetInfoV[Type].Size * AssetInfoSize[Type]);
		Cursor = TypedAssetInfoRange[Type].End;
	}

	Write(Out, TagsRange,         (byte *) Tags);
	Write(Out, AssetInfoRange,    (byte *) AssetInfos);
	Write(Out, CategoryIdRange,   (byte *) CategoryIds);
	Write(Out, CategoryInfoRange, (byte *) CategoryInfos);

	for (idx Type = 0;
	     Type < AssetType_COUNT;
	     ++Type)
	{
		Write(Out, TypedAssetInfoRange[Type], TypedAssetInfoV[Type].Data);
	}

	yaff_directory Dir = MakeYAFFDirectory(NumAssets,
					       NumTags,
					       NumCategories,
					       AssetIdRangeByType,
					       CategoryIdRange,
					       CategoryInfoRange,
					       AssetInfoRange,
					       TypedAssetInfoRange,
					       TagsRange);

	u64 DirectoryStart = Cursor;
	Cursor = WriteAssetDirectory(Out, Cursor, Dir);

	yaff_header Header = MakeYAFFHeader(Cursor, DirectoryStart);
	WriteAssetHeader(Out, 0, Header);

	free(AssetInfos);

	fflush(Out);
	fclose(Out);
}

int
main(int NumArgs, char *Args[static NumArgs])
{
	v2 const DefaultAnchor = V(0.0, 0.0);

	asset_builder Builder = BeginBuilder();

#define BeginCategory(...) BeginCategory(&Builder  __VA_OPT__(,) __VA_ARGS__)
#define EndCategory(...) EndCategory(&Builder __VA_OPT__(,) __VA_ARGS__)
#define AddText(...) AddText(&Builder __VA_OPT__(,) __VA_ARGS__)
#define AddSprite(...) AddSprite(&Builder __VA_OPT__(,) __VA_ARGS__)
#define	AddSound(...) AddSound(&Builder __VA_OPT__(,) __VA_ARGS__)
#define AddTag(...) AddTag(&Builder __VA_OPT__(,) __VA_ARGS__)
#define BuildAssetFile(...) BuildAssetFile(&Builder __VA_OPT__(,) __VA_ARGS__)
#define ResetBuilder(...) ResetBuilder(&Builder __VA_OPT__(,) __VA_ARGS__)
#define CONCAT(A, B) A ## B
#define Category(Name) DEFER(BeginCategory(CONCAT(Category_, Name)), EndCategory())

	Category(VERTEXSHADER)
	{
		AddText("res/shader/RendererVertex.glsl");
		AddText("res/shader/DefaultVertex.glsl");
	}

	Category(FRAGSHADER)
	{
		AddText("res/shader/FullRendererFragment.glsl");
		AddText("res/shader/DefaultFragment.glsl");
	}

	Category(STAMP)
	{
		AddSprite("res/stamps/gras_new.bmp", DefaultAnchor);
		AddTag(SpriteTag_GRAS, 1);
		AddSprite("res/stamps/water_new.bmp", DefaultAnchor);
		AddTag(SpriteTag_WATER, 1);
		AddSprite("res/stamps/lava.bmp", DefaultAnchor);
		AddTag(SpriteTag_LAVA, 1);
		AddSprite("res/stamps/stone.bmp", DefaultAnchor);
		AddTag(SpriteTag_STONE, 1);
		AddSprite("res/stamps/sand.bmp", DefaultAnchor);
		AddTag(SpriteTag_SAND, 1);
	}

	Category(TREE)
	{
		AddSprite("res/test/round-tree.bmp", DefaultAnchor);
		AddTag(SpriteTag_MOISTURE, 0.7);
		AddSprite("res/test/other-tree.bmp", DefaultAnchor);
		AddTag(SpriteTag_MOISTURE, 0.4);
		AddSprite("res/test/kaktus.bmp", DefaultAnchor);
		AddTag(SpriteTag_MOISTURE, 0.1);
	}

	Category(HILL)
	{
		AddSprite("res/test/hill.bmp", DefaultAnchor);
	}

	Category(SWAMP)
	{
		AddSprite("res/test/swamp.bmp", DefaultAnchor);
	}

	Category(HEAD)
	{
		AddSprite("res/body/head.bmp", DefaultAnchor);
	}

	Category(TORSO)
	{
		AddSprite("res/body/torso.bmp", DefaultAnchor);
	}

	// one second of samples in 48000khz
        idx OneSecond = 48000;

	Category(MUSIC)
	{
		AddSound("res/sounds/test.wav", OneSecond);
		AddTag(SoundTag_ID, 0);
		AddSound("res/sounds/click.wav", OneSecond);
		AddTag(SoundTag_ID, 1);
		AddSound("res/sounds/sine.wav", OneSecond);
		AddTag(SoundTag_ID, 2);
	}

	Category(SOUNDEFFECT)
	{
		AddSound("res/sounds/click.wav", OneSecond);
	}

	Category(CITY)
	{
		AddSprite("res/test/Haus.bmp", DefaultAnchor);
	}

	Category(ARMY)
	{
		AddSprite("res/test/Flagge.bmp", DefaultAnchor);
	}

	BuildAssetFile("asset.yaff");
	ResetBuilder();

	Category(BOW)
	{
		AddSprite("res/test/Bogen.bmp", V(135.0/143.0, 285.0/581.0));
	}

	Category(FIST)
	{
		AddSprite("res/test/Faust.bmp", V(8.0/249.0, 152.0/252.0));
	}

	Category(ARROW)
	{
		AddSprite("res/test/Pfeil.bmp", DefaultAnchor);
	}

	BuildAssetFile("weapons.yaff");
	ResetBuilder();

	// TODO: build second file!

	return 0;
}
