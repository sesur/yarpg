

thread_local thread_context *ThisThread;

GAMEFN thread_context *
CurrentThreadContext
(
	void
)
{
	return ThisThread;
}

GAMEFN void
ResumeAsyncFrame
(
	thread_context *ThisThread,
	async_frame *Frame
)
{
	void *Data = Ring_DataFrom(&ThisThread->Stack.Alloc, Frame->ArgOffset);
	Frame->Fun((thread_context *)ThisThread, Data);
}

PRIVATE i32
UpdateTail
(
	async_stack *Stack
)
{
	i32 Tail = Stack->Tail;
	i32 Head = Stack->Head;
	async_frame *Frames = Stack->Frames;

	while (Tail != Head)
	{
		if (InterlockedRead(&Frames[Tail].Status) ==
		    AsyncFrameStatus_EMPTY)
		{
			// also update the new mem end!
			Ring_DequeueTo(&Stack->Alloc,
				       Frames[Tail].ArgOffset + Frames[Tail].Size);
			if (Stack->Alloc.End >= Stack->Alloc.Capacity)
			{
				YASSERT(Stack->Alloc.End == Stack->Alloc.Capacity);
				Stack->Alloc.End = 0;
			}
			Tail = ModInc(Tail, Stack->Capacity);
		}
		else
		{
			break;
		}
	}

	InterlockedWrite(&Stack->Tail, Tail);
	return Tail;
}

PRIVATE async_frame *
AllocateFrame
(
	async_stack *Stack,
	taskfn Fn,
	idx Size,
	idx Alignment
)
{
	YASSERT(Size <= MAXOF(I32C(0)));
	YASSERT(Alignment <= MAXOF(I32C(0)));
	async_frame *Result = NULL;

	idx FrameSize = 0;

	// Idea: Instead of the thief thread updating the tail counter
	//       we could instead update it here by walking through the list
	//       until we hit a non empty one (or the head!)
	//       This way we can remove lots of synchronization around the tail!
	idx StackTail = UpdateTail(Stack);
	idx StackHead = Stack->Head;

	// the queue is full if StackTail = StackHead + 1
	if (StackTail != ModInc(StackHead, Stack->Capacity))
	{
		idx NextHead = ModInc(StackHead, Stack->Capacity);

		i32 StartOffset = Ring_DryPush(&Stack->Alloc,
					       Size, Alignment);

		if (StartOffset >= 0)
		{
			async_frame *NewFrame = &Stack->Frames[StackHead];
			async_frame_status Status = InterlockedRead(&NewFrame->Status);
			YASSERT(Status == AsyncFrameStatus_EMPTY);
			NewFrame->Fun       = Fn;
			NewFrame->Size      = Size;
			NewFrame->Alignment = Alignment;
			NewFrame->ArgOffset = StartOffset;
			Result = NewFrame;
		}
	}

	return Result;
}

PRIVATE void
IncAsyncStackHead
(
	async_stack *Stack
)
{
	YASSERT(Stack == &CurrentThreadContext()->Stack);
	i32 SavedHead = Stack->Head;
	i32 NewHead = ModInc(SavedHead, Stack->Capacity);
	async_frame *TopMost = &Stack->Frames[SavedHead];
	Ring_ReserveTo(&Stack->Alloc, TopMost->ArgOffset + TopMost->Size);
	YASSERT(InterlockedExchange(&TopMost->Status, AsyncFrameStatus_READY)
		== AsyncFrameStatus_EMPTY);
	InterlockedWriteEx(&Stack->Head, NewHead, __ATOMIC_RELEASE);
	// YASSERT(InterlockedExchange(&Stack->Head, NewHead) == SavedHead);


}

PRIVATE void
DecAsyncStackHead
(
	async_stack *Stack
)
{
	YASSERT(Stack == &CurrentThreadContext()->Stack);
	i32 SavedHead = Stack->Head;
	i32 NewHead = ModDec(SavedHead, Stack->Capacity);
	async_frame *TopMost = &Stack->Frames[ModDec(NewHead, Stack->Capacity)];
	Ring_PopTo(&Stack->Alloc, TopMost->ArgOffset + TopMost->Size);
	YASSERT(InterlockedExchange(&Stack->Head, NewHead) == SavedHead);
}

GAMEFN bool
TryPushJob
(
	taskfn Fn,
	idx Size,
	idx Alignment,
	void *Args
)
{
	YASSERT(Size <= MAXOF(I32C(0)));
	YASSERT(Alignment <= MAXOF(I32C(0)));
	YASSERT(Size > 0);
	YASSERT(Alignment > 0);
	YASSERT(ISPOWOFTWO(Alignment));

	thread_context *ThisThread = CurrentThreadContext();
	async_stack *Stack = &ThisThread->Stack;

	bool Success;

	async_frame *Frame = AllocateFrame(Stack,
					   Fn,
					   Size,
					   Alignment);
	if (Frame)
	{
		memcpy(Stack->Alloc.Data + Frame->ArgOffset, Args, Size);
		IncAsyncStackHead(Stack);
		Success = 1;
	}
	else
	{
		Success = 0;
	}

	return Success;
}

PRIVATE async_frame *
TryPopJob
(
	async_stack *Stack
)
{
	// Head is only changed on this thread
	// no need for atomic reads!
	i32 Head = Stack->Head;
	i32 Prev = ModDec(Head, Stack->Capacity);
	async_frame *Current = &Stack->Frames[Prev];

	if (InterlockedCompareSwap(&Current->Status,
				   AsyncFrameStatus_READY,
				   AsyncFrameStatus_EMPTY))
	{
		DecAsyncStackHead(Stack);
	}
	else
	{
		Current = NULL;
	}

	return Current;
}

PRIVATE void
CopyFrameArgs
(
	async_stack *DestStack,
	async_frame *Dest,
	async_stack *SrcStack,
	async_frame *Src
)
{
	YASSERT(Dest->Size == Src->Size);
	memcpy(DestStack->Alloc.Data + Dest->ArgOffset,
	       SrcStack->Alloc.Data + Src->ArgOffset,
	       Src->Size);
}

PRIVATE async_frame *
TryStealJob
(
	async_stack *Stack,
	async_stack *Target
)
{
	i32 CurrentIdx = InterlockedReadEx(&Target->Tail, __ATOMIC_ACQUIRE);
	i32 LastIdx    = InterlockedReadEx(&Target->Head, __ATOMIC_ACQUIRE);
	async_frame *Stolen  = NULL;

	while ((CurrentIdx != LastIdx) && (Stolen == NULL))
	{
		async_frame *ToSteal = &Target->Frames[CurrentIdx];
		if (InterlockedCompareSwap(&ToSteal->Status,
					   AsyncFrameStatus_READY,
					   AsyncFrameStatus_READ))
		{
			async_frame *New = AllocateFrame(Stack,
							 ToSteal->Fun,
							 ToSteal->Size,
							 ToSteal->Alignment);
			if (New)
			{
				CopyFrameArgs(Stack, New, Target, ToSteal);
				// do not update the head here!
				// try steal job does the implicit TryPopJob!
				COMPILERBARRIER();
				YASSERT(InterlockedExchange(&ToSteal->Status,
							    AsyncFrameStatus_EMPTY)
					== AsyncFrameStatus_READ);
				Stolen = New;
			}
		}

		CurrentIdx = ModInc(CurrentIdx, Target->Capacity);
	}

	return Stolen;
}

PRIVATE bool
DoQueuedWorkWithStealing
(
	async_stack *MainStack,
	idx NumTheftTargets,
	async_stack **TheftTargets,
	idx MaxStealTries
)
{
	bool DidWork;
	// pop only fails if the queue is empty
	// in that case the queue will not unempty itself
	// without this thread doing a job so we do not need
	// to attempt further pops
	async_frame *CurrentJob = TryPopJob(&ThisThread->Stack);

	for (idx Try = 0;
	     (CurrentJob == NULL) && (Try < MaxStealTries);
	     ++Try)
	{
		for (idx Other = 0;
		     (CurrentJob == NULL) && (Other < NumTheftTargets);
		     ++Other)
		{
			// we should probably only steal jobs from threads
			// that still have a lot of stuff to do.  Stealing tasks
			// takes a lot of time and requires synchronization
			// which may make the whole program a bit slower.

			// an idea would be to have a counter per stack that
			// shows the approximate(!) number of tasks currently
			// in the queue.  The counter is only updated by the
			// owning thread with relaxed memory order which should
			// make reading/writing to it not that slow. With this
			// setup we can choose to only steal tasks from queues
			// that have at least n entries (for some good choice of n).
			CurrentJob = TryStealJob(&ThisThread->Stack, TheftTargets[Other]);
		}
	}

	if (CurrentJob != NULL)
	{
		ResumeAsyncFrame(ThisThread, CurrentJob);
		DidWork = 1;
	}
	else
	{
		DidWork = 0;
	}

	return DidWork;
}

PRIVATE async_stack
AsyncStack_FromMalloc
(
	idx StackSize,
	idx ArgSize
)
{
	async_stack Stack;
	Stack.Alloc = Ring_FromBuffer(ArgSize, calloc(1, ArgSize));
	Stack.Capacity = StackSize;
	Stack.Frames = calloc(sizeof(*Stack.Frames), Stack.Capacity);
	Stack.Head  = 0;
	Stack.Tail  = 0;
	return Stack;
}

PUSH_TASK(PushTask)
{
	// (taskfn Fun, void *Arg)
	bool Success = TryPushJob(Fun, Size, Alignment, Arg);
	return Success;
}

PRIVATE bool
DoQueuedWork
(
	thread_context *Ctx
)
{
	// (void)
	idx StealAttempts = 2;
	bool Success = DoQueuedWorkWithStealing(&Ctx->Stack,
						Ctx->NumOtherStacks,
						Ctx->OtherStacks,
						StealAttempts);
	return Success;
}



WAIT(Wait)
{
	DoQueuedWork(CurrentThreadContext());
}
