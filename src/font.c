#include <font.h>
#include <ft2build.h>
#include FT_FREETYPE_H

char const *FontPath[FONT_COUNT] = {
	[FONT_ROBOTTO] = "contrib/Nuklear/extra_font/Roboto-Regular.ttf",
	[FONT_DROID_SANS] = "contrib/Nuklear/extra_font/DroidSans.ttf",
	[FONT_PROGGY_TINY] = "contrib/Nuklear/extra_font/ProggyTiny.ttf"
};

static FT_Library Library;
static FT_Face    Face;
static font_id    LoadedFont = NO_FONT;

i32
InitFreeType(void)
{
	i32 Error;
	if ((Error = FT_Init_FreeType(&Library)) != 0)
	{
		puts("[FreeType]: Could not initialize FreeType");
                return Error;
	}

	return 0;
}

i32
ReleaseFreeType(void)
{
	FT_Done_FreeType(Library);

	return 0;
}

REQUEST_FONT(RequestFont)
{
	font_info  FInfo;
	if (LoadedFont != NO_FONT)
	{
		FT_Done_Face(Face);
	}

	i32 Error;
	if ((Error = FT_New_Face(Library, FontPath[FontId], 0, &Face)) != 0)
	{
                printf("[FreeType]: Failed to load font \"%s\"\n",
		       FontPath[FontId]);
		FInfo.Error = Error;

		return FInfo;
	}

	YASSERT(!FT_Select_Charmap(Face, FT_ENCODING_UNICODE));

	LoadedFont = FontId;

	FT_Set_Pixel_Sizes(Face, 0, PtSize);
	FInfo.PtSize             = PtSize;
	/* FInfo.Ascender           = Face->ascender / 64; */
	/* FInfo.Descender          = Face->descender / 64; */
	/* FInfo.LinePitch          = Face->height / 64; */
	/* FInfo.MaxHeight          = (Face->bbox.yMax - Face->bbox.yMin) / 64; */
	/* FInfo.MaxWidth           = (Face->bbox.xMax - Face->bbox.xMin) / 64; */

	FInfo.Ascender           = (i16) (Face->size->metrics.ascender / 64);
	FInfo.Descender          = (i16) (Face->size->metrics.descender / 64);
	FInfo.LinePitch          = (u16) (Face->size->metrics.height / 64);
	FInfo.MaxHeight          = (i16) (Face->size->metrics.ascender
					  - Face->size->metrics.descender) / 64;
	FInfo.MaxWidth           = (i16) (Face->size->metrics.max_advance / 64);
	FInfo.UnderlinePos       = (Face->underline_position);
	FInfo.UnderlineThickness = (Face->underline_thickness);
	FInfo.UnitsPerEM         = (Face->units_per_EM);
	FInfo.Error              = 0;

	return FInfo;
}

RENDER_GLYPH(RenderGlyph)
{
	YASSERT(LoadedFont != NO_FONT);
	char_info CInfo;

	u32 GlyphIndex;

	if (Char) GlyphIndex = FT_Get_Char_Index(Face, Char);
	else      GlyphIndex = 0; /* .notdef glyph index */

	i32 Error;
	// FT_LOAD_BITMAP_METRICS_ONLY
	if ((Error = FT_Load_Glyph(Face, GlyphIndex, FT_LOAD_RENDER)) != 0)
	{
		printf("Could not render char: 0x%x\n", Char);
		CInfo.Error = Error;
		return CInfo;
	}

	CInfo.Width       = (i32) Face->glyph->bitmap.width;
	CInfo.Height      = (i32) Face->glyph->bitmap.rows;
	CInfo.BearingLeft = Face->glyph->bitmap_left;
	CInfo.BearingTop  = Face->glyph->bitmap_top;
	CInfo.Advance     = (u32) (Face->glyph->advance.x / 64);
	CInfo.Error       = 0;
	CInfo.Buffer      = Face->glyph->bitmap.buffer;
	CInfo.Pitch       = (u32) Face->glyph->bitmap.pitch;


	return CInfo;
}
