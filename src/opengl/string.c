#include <opengl/string.h>

#include <def.h>
#include <opengl/opengl.h>
#include <font.h>

#include <game_internal.h>

static void
SetupMissingGlyph(glyph_table *Tbl)
{
	char_info CInfo = RenderGlyph(0);

	YASSERT(!CInfo.Error);

	i32 GlyphWidth  = Tbl->FontInfo.MaxWidth;
	i32 GlyphHeight = Tbl->FontInfo.MaxHeight;

	if (!(CInfo.Width <= GlyphWidth))
	{
		FormatErr("[ERROR] MissingChar Glyph has width {i32} when max width is {i32}\n",
		       CInfo.Width, GlyphWidth);
		YASSERT(0);
	}

	if (!(CInfo.Height <= GlyphHeight))
	{
		FormatErr("[ERROR] MissingChar Glyph has height {i32} when max height is {i32}\n",
		       CInfo.Height, GlyphHeight);
		YASSERT(0);
	}

#if defined(NO_GL)
	irect Region;
	Region.MinX = 0;
	Region.MinY = 0;
	Region.MaxX = CInfo.Width + Region.MinX;
	Region.MaxY = CInfo.Height + Region.MinY;
	WriteTexture(Tbl->Atlas, Region, CInfo.Buffer);
#else
	glBindTexture(GL_TEXTURE_2D, Tbl->AtlasTexture);

	GL_QUICK_CHECK();

	glTexSubImage2D(
		GL_TEXTURE_2D,
		0, 0, 0,
		CInfo.Width,
                CInfo.Height,
		GL_RED,
		GL_UNSIGNED_BYTE,
		CInfo.Buffer);

	GL_QUICK_CHECK();

	glBindTexture(GL_TEXTURE_2D, 0);
#endif

	Tbl->LoadedGlyph[0] = 0;
	Tbl->GlyphInfo[0].Advance     = CInfo.Advance;
	Tbl->GlyphInfo[0].BearingTop  = CInfo.BearingTop;
	Tbl->GlyphInfo[0].BearingLeft = CInfo.BearingLeft;
	Tbl->GlyphInfo[0].Width       = CInfo.Width;
	Tbl->GlyphInfo[0].Height      = CInfo.Height;
	Tbl->GlyphInfo[0].Valid       = 1;
	Tbl->GlyphInfo[0].Used        = 1;
	Tbl->GlyphInfo[0].StartX      = 0;
	Tbl->GlyphInfo[0].StartY      = 0;
}

void
InitGlyphTable(glyph_table *Tbl, font_id FontId, u32 PtSize, memory_arena *Scratch)
{
	font_info FontInfo = RequestFont(FontId, PtSize);

	YASSERT(!FontInfo.Error);

	/* FontInfo.MaxWidth  = FontInfo.MaxWidth / 64; */
	/* FontInfo.MaxHeight = FontInfo.MaxHeight / 64; */

	u32 TextureWidth  = FontInfo.MaxWidth  * GLYPH_TBL_DIM;
	u32 TextureHeight = FontInfo.MaxHeight * GLYPH_TBL_DIM;

	Tbl->FontInfo      = FontInfo;
	ZeroArray(ARRAYCOUNT(Tbl->LoadedGlyph), Tbl->LoadedGlyph);
	ZeroArray(ARRAYCOUNT(Tbl->GlyphInfo), Tbl->GlyphInfo);

#if defined(NO_GL)
	iv2 Dim    = IV(TextureWidth, TextureHeight);
	Tbl->Atlas = CreateMonoTexture(NULL, Dim);
	// we zero the texture so we can draw it
	// without invoking weird UB behaviour
	ZeroTexture(Tbl->Atlas);
#else
	Tbl->TextureWidth = TextureWidth;
	Tbl->TextureHeight = TextureHeight;
	GLuint Texture;

	glGenTextures(1, &Texture);

	GL_QUICK_CHECK();

	Tbl->AtlasTexture  = Texture;

	glBindTexture(GL_TEXTURE_2D, Texture);

	/* Better to use glTexStorage if available but thats only core since 4.2 */

	// since we want to draw the whole texture sometimes, we have to make
	// sure to completely initialize the whole image.
	u8 *Test = PushArray(Scratch, TextureWidth * TextureHeight, *Test);
	ZeroArray(TextureWidth * TextureHeight, Test);

	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RED, TextureWidth, TextureHeight, 0,
		GL_RED, GL_UNSIGNED_BYTE, Test);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glBindTexture(GL_TEXTURE_2D, 0);
#endif

	SetupMissingGlyph(Tbl);

        GL_QUICK_CHECK();
}

void
ReleaseGlyphTable(glyph_table *Tbl)
{
#if defined(NO_GL)
	ReleaseTexture(Tbl->Atlas);
#else
	glDeleteTextures(1, &Tbl->AtlasTexture);
#endif
}

void
InvalidateGlyphTable(glyph_table *Tbl)
{
	// possibly faster to just memset to 0 ?
	for (u32 Idx = 0;
	     Idx < ARRAYCOUNT(Tbl->GlyphInfo);
	     ++Idx)
	{
		Tbl->GlyphInfo[Idx].Valid = 0;
	}
	SetupMissingGlyph(Tbl);
}

u32
EnsureLoaded(glyph_table *Tbl, u32 Char)
{
	YASSERT(Char);

#if 0
	TODO(Change to id_map);

	u32 Idx = 0;
	id_index *Slot = GetOrAllocIdSlot(&Tbl->Map, (entity_id) {Char});
	if (Slot)
	{
		if (*Slot == -1)
		{
			YASSERT(Tbl->FreeGlyphs > 0);
			*Slot = Idx = Tbl->FreeSlots[--Tbl->FreeGlyphs];
			goto not_loaded;
		}
		else
		{
			YASSERT(*Slot >= 0);
			Idx = (u32) *Slot;
			goto found;
		}
	}
	else
	{
		goto tbl_full;
	}
#else


#define BYTE_NUM(x, n) (((x) >> ((n) * 8)) & 0xFF)

	/* TODO: better hash */
	u64 TmpHash = 569 * BYTE_NUM(Char, 0)
		   +  233 * BYTE_NUM(Char, 1)
		   +  149 * BYTE_NUM(Char, 2)
		   +   43 * BYTE_NUM(Char, 3);

#undef BYTE_NUM

	u32 Hash = TmpHash % GLYPH_TBL_SIZE;

	u32 Idx;
	for (u32 Offset = 0;
	     Offset < GLYPH_TBL_SIZE;
	     ++Offset)
	{
		Idx = (Hash + Offset) % GLYPH_TBL_SIZE;

		u32 Val = Tbl->LoadedGlyph[Idx];

		if (Char == Val) goto found;
		else if (0 == Val && 0 != Idx)
			goto not_loaded;
        }

	goto tbl_full;
#endif
found:
	if (!Tbl->GlyphInfo[Idx].Valid) goto not_loaded;

	Tbl->GlyphInfo[Idx].Used = 1;

	return Idx;

not_loaded:
	char_info CInfo = RenderGlyph(Char);

	if (CInfo.Error) goto tbl_full;

	i32 GlyphWidth  = Tbl->FontInfo.MaxWidth;
	i32 GlyphHeight = Tbl->FontInfo.MaxHeight;

	if (!(CInfo.Width <= GlyphWidth))
	{
		FormatErr("[ERROR] Char {u32} has width {i32} when max width is {i32}\n",
			  Char, CInfo.Width, GlyphWidth);
		YASSERT(0);
	}

	if (!(CInfo.Height <= GlyphHeight))
	{
		FormatErr("[ERROR] Char {u32} has height {i32} when max height is {i32}\n",
		       Char, CInfo.Height, GlyphHeight);
		YASSERT(0);
	}

	i32 AtlasX  = Idx % GLYPH_TBL_DIM;
	i32 AtlasY  = Idx / GLYPH_TBL_DIM;

	i32 XOffset = GlyphWidth  * AtlasX;
	i32 YOffset = GlyphHeight * AtlasY;
#if defined(NO_GL)
	irect Region;
	Region.MinX = XOffset;
	Region.MinY = YOffset;
	Region.MaxX = Region.MinX + CInfo.Width;
	Region.MaxY = Region.MinY + CInfo.Height;
	WriteTexture(Tbl->Atlas, Region, CInfo.Buffer);
#else
	glBindTexture(GL_TEXTURE_2D, Tbl->AtlasTexture);

	GL_QUICK_CHECK();

	glTexSubImage2D(
		GL_TEXTURE_2D,
		0, XOffset, YOffset,
		CInfo.Width,
                CInfo.Height,
		GL_RED,
		GL_UNSIGNED_BYTE,
		CInfo.Buffer);

	GL_QUICK_CHECK();

	glBindTexture(GL_TEXTURE_2D, 0);
#endif

	Tbl->LoadedGlyph[Idx] = Char;
	Tbl->GlyphInfo[Idx].Advance     = CInfo.Advance;
	Tbl->GlyphInfo[Idx].BearingTop  = CInfo.BearingTop;
	Tbl->GlyphInfo[Idx].BearingLeft = CInfo.BearingLeft;
	Tbl->GlyphInfo[Idx].Width       = CInfo.Width;
	Tbl->GlyphInfo[Idx].Height      = CInfo.Height;
	Tbl->GlyphInfo[Idx].Valid       = 1;
	Tbl->GlyphInfo[Idx].Used        = 1;
	Tbl->GlyphInfo[Idx].StartX      = XOffset;
	Tbl->GlyphInfo[Idx].StartY      = YOffset;

	return Idx;

tbl_full:

	// we should have kept track of the first item that was loaded
	// but wasnt used and overwritten that instead.

	return 0;

}

static u32
FirstChar(char const *String, u32 *CodePoint)
{
	u32 Length = 0;
	u32 Point  = 0;

	byte First = *String;
	if ((First >> 7) == 0) {
		/* First: 0xxxxxxx */
		Length = 1;
		Point  = (u32) First;
	} else if ((First >> 5) == 0b110) {
		/* First:  110xxxxx */
		/* Second: 10xxxxxx */
		Length = 2;
		u32 A = String[0] & 0b00111111;
		u32 B = String[1] & 0b00111111;
		Point = (A << 6) | B;
	} else if ((First >> 4) == 0b1110) {
		/* First:  1110xxxx */
		/* Second: 10xxxxxx */
		/* Third:  10xxxxxx */
		Length = 3;
		u32 A = String[0] & 0b00011111;
		u32 B = String[1] & 0b00111111;
		u32 C = String[2] & 0b00111111;
		Point = (A << 12) | (B << 6) | C;
	} else if ((First >> 3) == 0b11110) {
		/* First:  11110xxx */
		/* Second: 10xxxxxx */
		/* Third:  10xxxxxx */
		/* Fourth: 10xxxxxx */
		Length = 4;
		u32 A = String[0] & 0b00001111;
		u32 B = String[1] & 0b00111111;
		u32 C = String[2] & 0b00111111;
		u32 D = String[3] & 0b00111111;
		Point = (A << 18) | (B << 12) | (C << 6) | D;
	} else {
                /* TODO: handle bad character*/
		Length = 1;
		INVALIDCODEPATH();
	}

	*CodePoint = Point;
	return Length;
}

void
StringDimensionsUTF8(glyph_table *Table,
		     u32 NumChars,
		     char const *String,
		     i32 *StringWidth, i32 *StringHeight)
{
	i32 FullWidth = 0;
	i32 MaxHeight = 0;
	char const *Head = String;
	for (u32 CharNum = 0;
	     CharNum < NumChars;
	     ++CharNum)
	{
		u32 Char;// = String[CurrentChar];
		Head += FirstChar(Head, &Char);
		if (!Char) break;
		u32 Idx = EnsureLoaded(Table, Char);
		if (CharNum + 1 == NumChars)
		{
			FullWidth += Table->GlyphInfo[Idx].Width;
		} else {
			FullWidth += Table->GlyphInfo[Idx].Advance;
		}

		i32 GlyphHeight = Table->GlyphInfo[Idx].Height;

		if (GlyphHeight > MaxHeight) MaxHeight = GlyphHeight;
	}

	*StringWidth  = FullWidth;
	*StringHeight = MaxHeight;
}

void
StringDimensionsUTF32(glyph_table *Table,
		      u32 NumChars,
		      u32 const *String,
		      i32 *StringWidth, i32 *StringHeight)
{
	i32 FullWidth = 0;
	i32 MaxHeight = 0;
	for (u32 CharNum = 0;
	     CharNum < NumChars;
	     ++CharNum)
	{
		u32 Char = String[CharNum];
		if (!Char) break;
		u32 Idx = EnsureLoaded(Table, Char);
		if (CharNum + 1 == NumChars)
		{
			FullWidth += Table->GlyphInfo[Idx].Width;
		} else {
			FullWidth += Table->GlyphInfo[Idx].Advance;
		}

		i32 GlyphHeight = Table->GlyphInfo[Idx].Height - Table->GlyphInfo[Idx].BearingTop;

		if (GlyphHeight > MaxHeight) MaxHeight = GlyphHeight;
	}

	*StringWidth  = FullWidth;
	*StringHeight = MaxHeight;
}

/*
struct render_char_data
{
	u32         Idx;
	glyph_table *Table;
	batch_data  (*Vertices)[4];
	u32         (*Indices)[6];
	f32         X, Y;
};

TASK_CALLBACK_FN(RenderChar)
{
	struct render_char_data *Data = (struct render_char_data *) Arg;

	f32          X        = Data->X;
	f32          Y        = Data->Y;
	u32          Idx      = Data->Idx;
	glyph_table *Table    = Data->Table;
	batch_data  *Vertices = Data->Vertices;
	u32         *Indices  = Data->Indices;

	f32 GlyphX = X + Table->GlyphInfo[Idx].BearingLeft * Scale;
	f32 GlyphY = Y - (Table->GlyphInfo[Idx].Height
			  - Table->GlyphInfo[Idx].BearingTop) * Scale;

	i32 Left  = Table->GlyphInfo[Idx].StartX;
	i32 Right = Left + Table->GlyphInfo[Idx].Width;
	i32 Up    = Table->GlyphInfo[Idx].StartY;
	i32 Down  = Up + Table->GlyphInfo[Idx].Height;

	f32 TexLeft  = (f32) Left  / Table->TextureWidth;
	f32 TexRight = (f32) Right / Table->TextureWidth;
	f32 TexDown  = (f32) Down  / Table->TextureHeight;
	f32 TexUp    = (f32) Up    / Table->TextureHeight;

	f32 Width      = Table->GlyphInfo[Idx].Width * Scale;
	f32 Height     = Table->GlyphInfo[Idx].Height * Scale;

	Vertices[0].Data = (vertex_data) {{GlyphX, GlyphY+Height, 0.0},
					  {0.0f, 0.0f, 1.0f},
					  {TexLeft, TexUp},
					  {Color[0], Color[1], Color[2], 1.0f}};
	Vertices[0].TextureId = TextureId;
	Vertices[1].Data = (vertex_data) {{GlyphX, GlyphY, 0.0},
					  {0.0f, 0.0f, 1.0f},
					  {TexLeft, TexDown},
					  {Color[0], Color[1], Color[2], 1.0f}};
	Vertices[1].TextureId = TextureId;
	Vertices[2].Data = (vertex_data){{GlyphX+Width, GlyphY, 0.0},
					 {0.0f, 0.0f, 1.0f},
					 {TexRight, TexDown},
					 {Color[0], Color[1], Color[2], 1.0f}};
	Vertices[2].TextureId = TextureId;
	Vertices[3].Data = (vertex_data){{GlyphX+Width, GlyphY+Height, 0.0},
					 {0.0f, 0.0f, 1.0f},
					 {TexRight, TexUp},
					 {Color[0], Color[1], Color[2], 1.0f}};
	Vertices[3].TextureId = TextureId;

	Indices[0] = IndexOffset + 0;
	Indices[1] = IndexOffset + 1;
	Indices[2] = IndexOffset + 2;
	Indices[3] = IndexOffset + 0;
	Indices[4] = IndexOffset + 2;
	Indices[5] = IndexOffset + 3;
}
*/

void
RenderStringUTF32(glyph_table *Table, render_group *RenderGroup,
		  f32 StartX, f32 StartY, f32 StringHeight,
		  v4 Color, u32 NumChars, u32 const *String)
{
	INSTRUMENT_COUNTED(RenderString, NumChars)
	{
#if defined(NO_GL)
		f32 AtlasWidth  = (f32) Table->Atlas.Size.Width;
		f32 AtlasHeight = (f32) Table->Atlas.Size.Height;
#else
		f32 AtlasWidth  = (f32) Table->TextureWidth;
		f32 AtlasHeight = (f32) Table->TextureHeight;
#endif

		f32 Scale = StringHeight / Table->FontInfo.MaxHeight;

		f32 X = StartX;
		f32 Y = StartY - Table->FontInfo.Descender * Scale;

		for (u32 CurrentChar = 0; CurrentChar < NumChars && String[CurrentChar]; ++CurrentChar)
		{
			u32 Char = String[CurrentChar];
			if ((char) Char != '\n')
			{
				u32 Idx = EnsureLoaded(Table, Char);

				f32 GlyphX = X + (f32) Table->GlyphInfo[Idx].BearingLeft * Scale;
				f32 GlyphY = Y - (f32) (Table->GlyphInfo[Idx].Height
							- Table->GlyphInfo[Idx].BearingTop) * Scale;

				i32 Left  = Table->GlyphInfo[Idx].StartX;
				i32 Right = Left + Table->GlyphInfo[Idx].Width;
				i32 Up    = Table->GlyphInfo[Idx].StartY;
				i32 Down  = Up + Table->GlyphInfo[Idx].Height;

				f32 TexLeft  = (f32) Left  / AtlasWidth;
				f32 TexRight = (f32) Right / AtlasWidth;
				f32 TexDown  = (f32) Down  / AtlasHeight;
				f32 TexUp    = (f32) Up    / AtlasHeight;

				f32 Width      = (f32) Table->GlyphInfo[Idx].Width * Scale;
				f32 Height     = (f32) Table->GlyphInfo[Idx].Height * Scale;

				X += (f32) Table->GlyphInfo[Idx].Advance * Scale;

				texture Texture;
#if defined(NO_GL)
				Texture.ID = Table->Atlas.Id;
#else
				Texture.ID = Table->AtlasTexture;
#endif
				Texture.Origin = V(TexLeft, TexDown);
				Texture.Dimensions = V(TexRight - TexLeft,
						       TexUp - TexDown);
				PushTexture(RenderGroup,
					    V(GlyphX, GlyphY), V(Width, Height),
					    Texture,
					    Color);
			}
			else
			{
				X = StartX;
				Y -= Table->FontInfo.LinePitch * Scale;
			}

		}
	}
}

void
DrawString(glyph_table *Table, render_group *RenderGroup,
	   v4 Color, rect Bounds, u32 NumChars, char const *String)
{
	i32 SWidth, SHeight;
	StringDimensions(Table, NumChars, String, &SWidth, &SHeight);

	extern transient_state *TState;

	i32 NumBytes = 512;

	basis_system Basis = BasisOfRect(Bounds);

	render_group StringGroup = SubRenderGroup(RenderGroup, Basis);

	// f32 Scale = 1.0 / MaxI32(SWidth, SHeight);

	v2 Scale = V(1.0 / SWidth, 1.0 / SHeight);

	v2 Current = V(0, 0);

#if defined(NO_GL)
	f32 AtlasWidth  = (f32) Table->Atlas.Size.Width;
	f32 AtlasHeight = (f32) Table->Atlas.Size.Height;
#else
	f32 AtlasWidth  = (f32) Table->TextureWidth;
	f32 AtlasHeight = (f32) Table->TextureHeight;
#endif

	char const *Head = String;
       	for (u32 CurrentChar = 0; CurrentChar < NumChars; ++CurrentChar)
	{
		YASSERT(String[CurrentChar]);
		u32 Char;// = String[CurrentChar];
		Head += FirstChar(Head, &Char);
                if ((char) Char != '\n')
		{
			u32 Idx = EnsureLoaded(Table, Char);

			//TODO: take BearingLeft into consideration!
			v2 Offset = V(0, //Table->GlyphInfo[Idx].BearingLeft,
				      Table->GlyphInfo[Idx].BearingTop
				      - Table->GlyphInfo[Idx].Height);

			v2 GlyphPos = Add(Current, Hadamard(Scale, Offset));

			i32 Left  = Table->GlyphInfo[Idx].StartX;
			i32 Right = Left + Table->GlyphInfo[Idx].Width;
			i32 Up    = Table->GlyphInfo[Idx].StartY;
			i32 Down  = Up + Table->GlyphInfo[Idx].Height;

			f32 TexLeft  = (f32) Left  / AtlasWidth;
			f32 TexRight = (f32) Right / AtlasWidth;
			f32 TexDown  = (f32) Down  / AtlasHeight;
			f32 TexUp    = (f32) Up    / AtlasHeight;

			v2 GlyphDim = Hadamard(Scale,
					       V(Table->GlyphInfo[Idx].Width,
						 Table->GlyphInfo[Idx].Height));

			texture Texture;
#if defined(NO_GL)
			Texture.ID         = Table->Atlas.Id;
#else
			Texture.ID         = Table->AtlasTexture;
#endif
			Texture.Origin     = V(TexLeft, TexDown);
			Texture.Dimensions = V(TexRight - TexLeft,
					       TexUp - TexDown);
			Texture.IsMono     = 1;

			PushTexture(&StringGroup,
				    GlyphPos,
				    GlyphDim,
				    Texture,
				    Color);

			Current.X += (f32) Table->GlyphInfo[Idx].Advance * Scale.X;
		}
		else
		{
                        Current.X = 0;
			Current.Y -= Table->FontInfo.LinePitch * Scale.Y;
		}

	}
}

void
RenderStringUTF8(glyph_table *Table, render_group *RenderGroup,
		 f32 StartX, f32 StartY, f32 StringHeight,
		 v4 Color, u32 NumChars, char const *String)
{
        INSTRUMENT_COUNTED(RenderString, NumChars)
	{
#if defined(NO_GL)
		f32 AtlasWidth  = (f32) Table->Atlas.Size.Width;
		f32 AtlasHeight = (f32) Table->Atlas.Size.Height;
#else
		f32 AtlasWidth  = (f32) Table->TextureWidth;
		f32 AtlasHeight = (f32) Table->TextureHeight;
#endif

		f32 Scale = 1.0; //StringHeight / Table->FontInfo.MaxHeight;

		f32 X = StartX;
		f32 Y = StartY; // + Table->FontInfo.Descender * Scale;

		char const *Head = String;
		for (u32 CurrentChar = 0; CurrentChar < NumChars; ++CurrentChar)
		{
			YASSERT(String[CurrentChar]);
			u32 Char;// = String[CurrentChar];
			Head += FirstChar(Head, &Char);
			if ((char) Char != '\n')
			{
				u32 Idx = EnsureLoaded(Table, Char);

				f32 GlyphX = X + (f32) Table->GlyphInfo[Idx].BearingLeft * Scale;
				f32 GlyphY = Y - (f32) (Table->GlyphInfo[Idx].Height
							- Table->GlyphInfo[Idx].BearingTop) * Scale;

//			f32 GlyphY = Y - Table->GlyphInfo[Idx].BearingTop * Scale;

				i32 Left  = Table->GlyphInfo[Idx].StartX;
				i32 Right = Left + Table->GlyphInfo[Idx].Width;
				i32 Up    = Table->GlyphInfo[Idx].StartY;
				i32 Down  = Up + Table->GlyphInfo[Idx].Height;

				f32 TexLeft  = (f32) Left  / AtlasWidth;
				f32 TexRight = (f32) Right / AtlasWidth;
				f32 TexDown  = (f32) Down  / AtlasHeight;
				f32 TexUp    = (f32) Up    / AtlasHeight;

				f32 Width      = (f32) Table->GlyphInfo[Idx].Width * Scale;
				f32 Height     = (f32) Table->GlyphInfo[Idx].Height * Scale;

				texture Texture;
#if defined(NO_GL)
				Texture.ID         = Table->Atlas.Id;
#else
				Texture.ID         = Table->AtlasTexture;
#endif
				Texture.Origin     = V(TexLeft, TexDown);
				Texture.Dimensions = V(TexRight - TexLeft,
						       TexUp - TexDown);
				Texture.IsMono     = 1;

				PushTexture(RenderGroup,
					    V(GlyphX, GlyphY),
					    V(Width, Height),
					    Texture,
					    Color);

				X += (f32) Table->GlyphInfo[Idx].Advance * Scale;
			}
			else
			{
				X = StartX;
				Y -= Table->FontInfo.LinePitch * Scale;
			}

		}
	}
}
