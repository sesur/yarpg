#include <opengl/imgui.h>

#include <def.h>

#include <game.h>

#include <util/layout.h>
#include <util/utf8.h>
#include <util/string.h>

#define WNDCOL_ACTIVE V(0.2, 0.2, 0.2, 1.0)
#define WNDCOL_HOT    V(0.1, 0.1, 0.1, 1.0)
#define WNDCOL_COLD   V(0.05, 0.05, 0.05, 1.0)

#define WGTCOL_ACTIVE V(0.9, 0.9, 0.9, 1.0)
#define WGTCOL_HOT    V(0.75, 0.75, 0.75, 1.0)
#define WGTCOL_COLD   V(0.6, 0.6, 0.6, 1.0)
#define WGTCOL_BKG    V(0.35, 0.35, 0.35, 1.0)

PRIVATE void
GetMouse(ui_state *UiState)
{
	UiState->Next.Mouse.DeviceNum = -1;
	UiState->Next.Keyboard.DeviceNum = -1;
        for (i32 I = 0;
	     I < UiState->Input.NumDevices;
	     ++I)
	{
		input_device *Device = UiState->Input.Devices + I;

                if (Device->Method == InputMethod_MOUSE)
		{
                        if (Device->Data.Mouse)
			{
				UiState->Next.Mouse.Data = *Device->Data.Mouse;
				UiState->Next.Mouse.DeviceNum = I;

			}
		} else if (Device->Method == InputMethod_KEYBOARD)
		{
			if (Device->Data.Keyboard)
			{
				UiState->Next.Keyboard.Data = *Device->Data.Keyboard;
				UiState->Next.Keyboard.DeviceNum = I;
			}
		}
	}

	if (UiState->Next.Keyboard.DeviceNum == -1)
	{
		ZeroStruct(&UiState->Next.Keyboard.Data);
	}

	if (UiState->Next.Mouse.DeviceNum == -1)
	{
		ZeroStruct(&UiState->Next.Mouse.Data);
	}
}

/* ConsumeExternalMouse consumes the mouse for things that are not part of the
 * Ui system, whereas ConsumeMouse consumes the mouse even for widgets that are
 * part of the current ui.
 */

PRIVATE void
ConsumeExternalMouse(ui_state *UiState)
{
	// UiState->Input.Devices[UiState->Mouse.DeviceNum].Data.Mouse = NULL;
}

PRIVATE void
ConsumeMouse(ui_state *UiState)
{
        /* ConsumeExternalMouse(UiState); */
	/* if (UiState->Mouse.Data) */
	/* { */
	/* 	UiState->Mouse.Data = NULL; */
	/* } */
}

PRIVATE void
ConsumeExternalKeyboard(ui_state *UiState)
{
//	UiState->Input.Devices[UiState->Keyboard.DeviceNum].Data.Keyboard = NULL;
}

PRIVATE void
ConsumeKeyboard(ui_state *UiState)
{
        /* ConsumeExternalKeyboard(UiState); */
	/* if (UiState->Keyboard.Data) */
	/* { */
	/* 	UiState->Keyboard.Data = NULL; */
	/* } */
}

PRIVATE b32
InsideIRect(irect Rect, iv2 Point)
{

	b32 XWorks = Rect.BottomLeft.X <= Point.X && Point.X <= Rect.TopRight.X;
	b32 YWorks = Rect.BottomLeft.Y <= Point.Y && Point.Y <= Rect.TopRight.Y;
	b32 Result = XWorks && YWorks;
	return Result;
}

PRIVATE rect
GetScreenBounds(ui_state *UiState, irect Bounds)
{
	rect Result;

	i32 X = Bounds.MinX;
	i32 Y = Bounds.MinY;
	i32 Width = Bounds.MaxX - X;
	i32 Height = Bounds.MaxY - Y;

	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	f32 ScreenHalfWidth = (f32) UiState->FrameWidth / F32C(2.0);
	f32 ScreenHalfHeight = (f32) UiState->FrameHeight / F32C(2.0);
	f32 OnScreenX = ((f32) X - ScreenHalfWidth) / ScreenHalfWidth;
	f32 OnScreenY = (ScreenHalfHeight - (f32) Y) / ScreenHalfHeight;
	f32 OnScreenWidth = (f32) Width / ScreenHalfWidth;
	f32 OnScreenHeight = (f32) Height / ScreenHalfHeight;

	Result.MinX = OnScreenX;
	Result.MinY = OnScreenY - OnScreenHeight;
	Result.MaxX = OnScreenX + OnScreenWidth;
	Result.MaxY = OnScreenY;

	return Result;
}

rect
LocalToScreen(ui_state *State, irect Rect)
{
	return GetScreenBounds(State, Rect);
}

PRIVATE void
DrawIRect(ui_state *UiState, irect Rect, v4 Color)
{

	i32 X      = Rect.BottomLeft.X;
	i32 Y      = Rect.BottomLeft.Y;
	i32 Width  = Rect.TopRight.X - X;
	i32 Height = Rect.TopRight.Y - Y;

	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	PushRect(UiState->RenderGroup,
		 V(X, Y),
		 V(Width, Height),
		Color);
}

/* static b32 */
/* IsHotWidget(ui_state *UiState, irect Box) */
/* { */
/* 	b32 Result = (UiState->HotWindow == UiState->CurrentWindow) && */
/* 		UiState->Mouse.HotWidget == 0 && */
/* 		InsideIRect(Box, Diff(UiState->Mouse.Pos, UiState->HotBox.BottomLeft)); */

/*         return Result; */
/* } */

PRIVATE b32
TestAndSetHotWidget(ui_state *UiState, i32 WidgetId, irect Box)
{
	/* b32 Result = IsHotWidget(UiState, Box); */
	/* if (Result) */
	/* { */
	/* 	UiState->Mouse.HotWidget = WidgetId; */
	/* } */
	/* return Result; */

	YASSERT(UiState->CurrentWindow <= MAXOF(UiState->Next.Interaction.Hot.Window));
	YASSERT(WidgetId <= MAXOF(UiState->Next.Interaction.Hot.Widget));

	if (UiState->Next.Interaction.Hot.Window == UiState->CurrentWindow &&
	    InsideIRect(Box, Diff(UiState->Next.Mouse.Pos, UiState->HotBox.BottomLeft)))
	{
		UiState->Next.Interaction.Hot.Widget = WidgetId;
	}

	b32 Result = ((UiState->Current.Interaction.Hot.Window == UiState->CurrentWindow)
		      && (UiState->Current.Interaction.Hot.Widget == WidgetId));

	return Result;

}

/* static b32 */
/* IsActiveWidget(ui_state *UiState, irect Box) */
/* { */
/* 	b32 Result = (UiState->ActiveWindow == UiState->CurrentWindow) && */
/* 		UiState->Mouse.Data && */
/* 		UiState->Mouse.Data->Key[KeyId_MOUSE_BUTTON_1].IsDown && */
/* 		UiState->Mouse.ActiveWidget == 0 && */
/* 		InsideIRect(Box, Diff(UiState->Mouse.Pos, UiState->ActiveBox.BottomLeft)); */

/* 	return Result; */
/* } */

/* static b32 */
/* WasActiveWidget(ui_state *UiState, i32 WidgetId) */
/* { */
/* 	b32 Result = (UiState->Last.ActiveWindow == UiState->CurrentWindow) */
/* 		&& (UiState->Last.Mouse.ActiveWidget == WidgetId); */

/* 	return Result; */
/* } */

PRIVATE b32
WillNotBeActiveWidget(ui_state *UiState, i32 WidgetId)
{
	YASSERT(UiState->CurrentWindow <= MAXOF(UiState->Next.Interaction.Active.Window));
	YASSERT(WidgetId <= MAXOF(UiState->Next.Interaction.Active.Widget));
	b32 Result = !((UiState->Next.Interaction.Active.Window == UiState->CurrentWindow)
		       && (UiState->Next.Interaction.Active.Widget == WidgetId));

	return Result;
}

PRIVATE b32
TestAndSetActiveWidget(ui_state *UiState, i32 WidgetId, irect Box)
{
	/*
	 * We should be active if
	 * - Our window is currently active (which implies that the button is pressed)
	 * - AND
	 *     we either were active last frame;
	 *     OR
	 *     we should be active this frame; that is nobody was active last frame
	 *       and the cursor is in our border
	 */

	YASSERT(UiState->CurrentWindow <= MAXOF(UiState->Next.Interaction.Active.Window));
	YASSERT(WidgetId <= MAXOF(UiState->Next.Interaction.Active.Widget));

	b32 IsActive = (UiState->Current.Interaction.Active.Widget == WidgetId)
		&& (UiState->Current.Interaction.Active.Window == UiState->CurrentWindow);

	b32 ShouldBeActive = (UiState->Current.Interaction.Active.Widget == 0)
		&& InsideIRect(Box, Diff(UiState->Next.Mouse.Pos, UiState->ActiveBox.BottomLeft));

	if ((UiState->Next.Interaction.Active.Window == UiState->CurrentWindow)
	    && (IsActive || ShouldBeActive))
	{
		UiState->Next.Interaction.Active.Widget = WidgetId;
	}

	return IsActive;
}

PRIVATE b32
TestAndSetHotWindow(ui_state *UiState)
{
	YASSERT(UiState->CurrentWindow);
	YASSERT(UiState->CurrentWindow <= MAXOF(UiState->Next.Interaction.Hot.Window));

	if (InsideIRect(UiState->CurrentBox,
			UiState->Next.Mouse.Pos))
	{
		UiState->Next.Interaction.Hot.Window = (i16) UiState->CurrentWindow;
		UiState->Next.Interaction.Hot.Widget = 0;
		UiState->HotBox                      = UiState->CurrentBox;
	}

	b32 Result = (UiState->Current.Interaction.Hot.Window == UiState->CurrentWindow);

	return Result;
}

PRIVATE b32
TestAndSetActiveWindow(ui_state *UiState)
{
	YASSERT(UiState->CurrentWindow);
	YASSERT(UiState->CurrentWindow <= MAXOF(UiState->Next.Interaction.Active.Window));

	if ((UiState->Current.Interaction.Active.Window <= UiState->CurrentWindow)
	    && UiState->Next.Mouse.Data.Key[KeyId_MOUSE_BUTTON_1].IsDown
	    && InsideIRect(UiState->CurrentBox,
			   UiState->Next.Mouse.Pos))
	{
		UiState->Next.Interaction.Active.Window      = (i16) UiState->CurrentWindow;
		UiState->Next.Interaction.Active.Widget      = 0;
		UiState->Next.Interaction.Active.Interaction = 0;
		UiState->ActiveBox                           = UiState->CurrentBox;
	}

	b32 Result = (UiState->Current.Interaction.Active.Window == UiState->CurrentWindow);

	return Result;
}

PRIVATE void
SetCurrentWindow(ui_state *UiState, i32 WindowId, irect Box, u32 Flags)
{
	YASSERT(!UiState->CurrentWindow);
	UiState->CurrentWindow  = WindowId;
	UiState->CurrentBox     = Box;
	irect Bounds;
	Bounds.MinX = 0;
	Bounds.MinY = 0;
	Bounds.MaxX = Box.MaxX - Box.MinX;
	Bounds.MaxY = Box.MaxY - Box.MinY;
	UiState->DrawableBounds = Bounds;

	if (!(Flags & WindowOption_NONINTERACTIVE))
	{
		TestAndSetHotWindow(UiState);
		TestAndSetActiveWindow(UiState);
	}
}

void
InitGui(ui_state *UiState,
	glyph_table *Table)
{
	ZeroStruct(&UiState->Next);
	ZeroStruct(&UiState->Current);
	UiState->Glyphs        = Table;
	UiState->CurrentWindow = 0;
}

void
PrepareFrame(ui_state *UiState,
	     memory_arena *Arena,
	     i32 FrameWidth, i32 FrameHeight,
	     game_input Input,
	     render_group *RenderGroup)
{
	UiState->Current        = UiState->Next;
	UiState->FrameHeight    = FrameHeight;
	UiState->FrameWidth     = FrameWidth;
	UiState->Input          = Input;
	UiState->Next.Mouse.Pos = Input.CursorPos;
	UiState->RenderGroup    = RenderGroup;

	GetMouse(UiState);

	if (!UiState->Next.Mouse.Data.Key[KeyId_MOUSE_BUTTON_1].IsDown)
	{
		ZeroStruct(&UiState->Next.Interaction.Active);
	}
	// if the mouse is down, but no item is active, we should
	// ensure that no item can be activated.
	// this prevents situations where we accidentally click a button
	// when we hover over it while having our mouse down.
	else if (UiState->Next.Interaction.Hot.Window == 0
		 && UiState->Next.Interaction.Active.Window == 0)
	{
		UiState->Next.Interaction.Active.Window = -1;

	}
	else if (UiState->Next.Interaction.Hot.Widget == 0
		 && UiState->Next.Interaction.Active.Widget == 0)
	{
		UiState->Next.Interaction.Active.Widget = -1;
	}

	ZeroStruct(&UiState->Next.Interaction.Hot);
}

void FinishFrame(ui_state *UiState)
{

}

void Text(ui_state *UiState, irect Bounds, u32 Position, u32 NumChars,
	  char const *Content, v4 TextColor, f32 TextScale)
{
	if (NumChars == 0) return;
	i32 X, Y, Width, Height;
	Y = Bounds.MinY;
	Height = Bounds.MaxY - Y;
	X = Bounds.MinX;
	Width = Bounds.MaxX - X;

	u32 StringLength = NumChars;
	i32 StringWidth, StringHeight;

	// u32 UTF32Content[StringLength];


	// for(u32 I = 0;
	//     I < StringLength;
	//     ++I)
	// {
	// 	UTF32Content[I] = (u32) Content[I];
	// }

	// StringDimensionsW(UiState->Glyphs, StringLength,
	// 		  UTF32Content,
	// 		  &StringWidth, &StringHeight);

	StringDimensions(UiState->Glyphs,
			 StringLength, Content,
			 &StringWidth, &StringHeight);

	u32 YPosition = Position & 0x0F;
	u32 XPosition = Position & 0xF0;

	i32 StringX = 0, StringY = 0;

	switch (YPosition)
	{
		break;case TextPosition_Y_BOTTOM:
		{
			StringY = 0;
		}

		break;case TextPosition_Y_CENTERED:
		{
			StringY = (Height - StringHeight)/2;
		}

		break;case TextPosition_Y_TOP:
		{
			StringY = Height - StringHeight;
		}
		INVALIDDEFAULT();
	}

	switch (XPosition)
	{
		break;case TextPosition_X_LEFT:
		{
			StringX = 0;
		}

		break;case TextPosition_X_CENTERED:
		{
			StringX = (Width - StringWidth)/2;
		}

		break;case TextPosition_X_RIGHT:
		{
			StringX = Width - StringWidth;
		}
		INVALIDDEFAULT();
	}

	X += StringX;
	Y += StringY;

	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

#if defined(IMGUI_SHOW_TEXT_BBOX)

	v4 BoxColor = V(1, 0, 1, 1);
	PushRect(UiState->RenderGroup, V(X, Y), V(StringWidth, StringHeight),
		BoxColor);
#endif

	RenderString(UiState->Glyphs, UiState->RenderGroup,
		     (f32) X, (f32) Y, (f32) Height,
		     TextColor,
		     StringLength, Content);

	/* v4 Color = V(TextColor[0], TextColor[1], TextColor[2], 1.0); */
	/* DrawString(UiState->Glyphs, UiState->RenderGroup, */
	/* 	   Color, ToFloat(Offset(Bounds, UiState->CurrentBox.BottomLeft)), */
	/* 	   StringLength, Content); */
}

b32
Button(ui_state *UiState, i32 Id, irect Bounds,
       u32 NumChars, char const *Label)
{
	b32 Hot           = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active        = TestAndSetActiveWidget(UiState, Id, Bounds);
	b32 WontBeActive  = WillNotBeActiveWidget(UiState, Id);
	b32 WasClicked    = Active && WontBeActive;

	v4 Color = WGTCOL_COLD;
	if (Active)
	{
		Color = WGTCOL_ACTIVE;
	} else if (Hot)
	{
		Color = WGTCOL_HOT;
	}

	DrawIRect(UiState, Bounds, Color);
	if (NumChars)
	{
		v4 White = V(1.0, 1.0, 1.0, 1);
		f32 TextScale = 1.0;
		Text(UiState, Bounds, TextPosition_CENTERED, NumChars,
		     Label, White, TextScale);
	}

	if (WasClicked)
	{
		/* EnqueueSound(FirstSoundOf(&TState->Assets, */
		/* 			  AssetKind_SOUNDEFFECT), 1.0, 1.0); */
	}


	return WasClicked;
}

b32
Slider(ui_state *UiState,
       i32 Id, irect Bounds,
       f32 *Val, f32 Min, f32 Max)
{
        YASSERT(UiState->CurrentWindow);
        f32 Current = *Val;
	YASSERT(Min <= Current && Current <= Max);
	f32 Percent = (Current - Min) / (Max - Min);
	if (Percent < 0.0) Percent = 0.0;
	else if (Percent > 1.0) Percent = 1.0;

	b32 Hot = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Bounds);
	b32 WasChanged = 0;

	i32 X = Bounds.MinX;
	i32 Y = Bounds.MinY;
	i32 Width = Bounds.MaxX - X;
	i32 Height = Bounds.MaxY - Y;
	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	v4 Foreground;
	if (Active)
	{
		Foreground = WGTCOL_ACTIVE;
	} else if (Hot)
	{
		Foreground = WGTCOL_HOT;
	} else {
		Foreground = WGTCOL_COLD;
	}

	if (Active)
	{
                f32 Direction = (f32) (UiState->Current.Mouse.Pos.X - X) / (f32) Width;
                if (Direction < 0.0)
		{
			Direction = 0.0;
		} else if (Direction > 1.0)
		{
			Direction = 1.0;
		}

//		Percent = Direction;
		*Val = Direction * (Max - Min) + Min;
		WasChanged = 1;
	}

	v4 Background = WGTCOL_BKG;

	PushRect(UiState->RenderGroup, V(X, Y), V(Width, Height), Background);
	PushRect(UiState->RenderGroup, V(0.9 * Percent * Width + X, Y), V(0.1 * Width, Height), Foreground);

	return WasChanged;
}

b32
CheckBox(ui_state *UiState, i32 Id, irect Bounds, b32 *Bool)
{
	b32 WasChanged = 0;
	b32 Value = *Bool;

	b32 Hot = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Bounds);
	b32 WontBeActive = WillNotBeActiveWidget(UiState, Id);
	b32 WasClicked = Active && WontBeActive;


	if (WasClicked)
	{
		Value = !Value;
		*Bool = Value;
		WasChanged = !WasChanged;
	}

	Bounds = Offset(Bounds, UiState->CurrentBox.BottomLeft);
	f32 X = (f32) Bounds.BottomLeft.X;
	f32 Y = (f32) Bounds.BottomLeft.Y;
	f32 Width = (f32) (Bounds.TopRight.X - Bounds.BottomLeft.X);
	f32 Height = (f32) (Bounds.TopRight.Y - Bounds.BottomLeft.Y);

	v4 Foreground = WGTCOL_COLD;
	if (Active) Foreground = WGTCOL_ACTIVE;
	else if (Hot) Foreground =  WGTCOL_HOT;

	v4 Background = WGTCOL_BKG;

	PushRect(UiState->RenderGroup, V(X, Y), V(Width, Height), Foreground);
	PushRect(UiState->RenderGroup, V(X + 0.05 * Width, Y + 0.05 * Height),
		 V(Width * 0.9, Height * 0.9), Background);


	if (Value)
	{
		PushRect(UiState->RenderGroup, V(X + 0.2 * Width,
						Y + 0.2 * Height),
			 V(0.6 * Width, 0.6 * Height), Foreground);
	}

	return WasChanged;
}

PRIVATE b32
Swiper(ui_state *UiState, i32 Id, irect Bounds, iv2 *Delta)
{
	b32 Hot    = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Bounds);
	b32 Result = YARPG_FALSE;
	if (Active)
	{
		*Delta = Diff(UiState->Next.Mouse.Pos, UiState->Current.Mouse.Pos);
		Result = YARPG_TRUE;
	}

	return Result;
}

PRIVATE b32
TitleBar(ui_state *UiState, i32 Id, i32 CloseId,
	 irect Bounds, iv2 *Delta,
	 char const *Content, b32 Closable)
{
	b32 ShouldClose = 0;
	v4 Color = V(0.03, 0.66, 0.05, 1.0);
	DrawIRect(UiState, Bounds, Color);

	i32 X = Bounds.MinX;
	i32 Y = Bounds.MinY;
	i32 Width = Bounds.MaxX - X;
	i32 Height = Bounds.MaxY - Y;

	i32 TextDivisions = 15;
	i32 Divisions = TextDivisions;

	if (Closable) Divisions += 1;

	irect TextBounds = HorizontalRegion(Bounds, Divisions, 0, TextDivisions);

	b32 Hot    = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Bounds);
	if (Active)
	{
		*Delta = Diff(UiState->Next.Mouse.Pos, UiState->Current.Mouse.Pos);
// Diff(UiState->Current.Mouse.Pos, UiState->OldMousePos);
	} else {
		*Delta = IV(0, 0);
	}

	if (Closable)
	{
		irect ClosableBox = HorizontalRegion(Bounds, Divisions, TextDivisions, Divisions);

		irect Actual      = GridRegion(ClosableBox, IV(5, 5), IV(1, 1), IV(4, 4));

		CheckBox(UiState, CloseId, Actual, &ShouldClose);
	}



        u32 Count = CStringLength(Content);
	v4 Pink = V(1, 0, 1, 1);
	Text(UiState, TextBounds, TextPosition_CENTERED, Count,
	     Content, Pink, 1.0);
	return !ShouldClose;
}

b32
Label(ui_state *UiState, irect Bounds, u32 Position,
      u32 NumChars, char const *Content, v4 TextColor, f32 TextScale)
{
	TestAndSetHotWidget(UiState, -1, Bounds);
	TestAndSetActiveWidget(UiState, -1, Bounds);
	v4 Color =  WGTCOL_BKG;
	DrawIRect(UiState, Bounds, Color);

	Text(UiState, Bounds, Position, NumChars, Content, TextColor, TextScale);

	return 0;
}

void
BlockGraph(ui_state *UiState, irect Bounds,
	   u32 BufferSize, f32 *Buffer,
	   u32 Start, u32 Count,
	   f32 Min, f32 Zero, f32 Max, v4 Color)
{
	YASSERTF(Min <= Zero && Zero <= Max, "Min: %f, Zero: %f, Max: %f",
		 Min, Zero, Max);


	v4 BKGColor = WGTCOL_BKG;
	DrawIRect(UiState, Bounds, BKGColor);

	f32 Width = (f32) (Bounds.MaxX - Bounds.MinX);

	f32 PartitionWidth = Width / (f32) Count;

	Bounds = Offset(Bounds, UiState->CurrentBox.BottomLeft);

	f32 Scale = 1 / (Max - Min);
	f32 Baseline = Scale * (Zero - Min);
	f32 BY = Lerp((f32) Bounds.MinY, Baseline, (f32) Bounds.MaxY);

	v4 HalfWhite = V(1, 1, 1, 0.5);

	for (u32 Current = 0;
	     Current < Count;
	     ++Current)
	{
		u32 Index = (Start + Current) % BufferSize;

		f32 CurrentVal = Clamp(Min, Buffer[Index], Max);

		f32 Normalized = Scale * (CurrentVal - Min);

		f32 Y = Lerp((f32) Bounds.MinY, Normalized, (f32) Bounds.MaxY);

                f32 Progress = ((f32) Current) / (f32) (Count - 1);

		f32 SX = Lerp((f32) Bounds.MinX, Progress, (f32) Bounds.MaxX - PartitionWidth);

		if (CurrentVal < Baseline)
		{
			/* Y < BY */
			PushRect(UiState->RenderGroup,
				 V(SX, Y),
				 V(PartitionWidth, BY - Y),
				 Color);
		}
		else
		{
			/* BY < Y */
			PushRect(UiState->RenderGroup,
				 V(SX, BY),
				 V(PartitionWidth, Y - BY),
				 Color);
		}

	}

	PushLine(UiState->RenderGroup, V(Bounds.MinX, BY), V(Bounds.MaxX, BY),
		 1.0, HalfWhite);
}

void
Graph(ui_state *UiState, irect Bounds,
      u32 BufferSize, f32 *Buffer,
      u32 Start, u32 Count,
      f32 Min, f32 Max, v4 Color)
{
        v4 BKGColor = WGTCOL_BKG;
	DrawIRect(UiState, Bounds, BKGColor);

	f32 Width = (f32) (Bounds.MaxX - Bounds.MinX);

	YASSERT(Count > 1);

	f32 PartitionWidth = Width / (f32) (Count-1);

	Bounds = Offset(Bounds, UiState->CurrentBox.BottomLeft);

	u32 FirstIndex = (Start) % BufferSize;

	f32 FirstVal = Clamp(Min, Buffer[FirstIndex], Max);

	f32 Normalized = (FirstVal - Min) / (Max - Min);

	f32 LastVal = Lerp((f32) Bounds.MinY, Normalized, (f32) Bounds.MaxY);

	for (u32 Current = 1;
	     Current < Count;
	     ++Current)
	{
		u32 Index = (Start + Current) % BufferSize;

		f32 CurrentVal = Clamp(Min, Buffer[Index], Max);

		f32 Normalized = (CurrentVal - Min) / (Max - Min);

		f32 Val = Lerp((f32) Bounds.MinY, Normalized, (f32) Bounds.MaxY);

                f32 Progress = ((f32) Current) / (f32) (Count - 1);

		f32 SX = Lerp((f32) Bounds.MinX, Progress, (f32) Bounds.MaxX - PartitionWidth);
		f32 EX = SX + PartitionWidth;

                v2 Triangle1[3] = {
			V(SX, Bounds.MinY),
			V(SX, LastVal),
			V(EX, Bounds.MinY)
		};

		v2 Triangle2[3] = {
			V(EX, Bounds.MinY),
			V(SX, LastVal),
                        V(EX, Val)
		};

		PushTriangle(UiState->RenderGroup, Triangle1, Color);
		PushTriangle(UiState->RenderGroup, Triangle2, Color);

		LastVal = Val;
	}
}

b32
BeginWindow(ui_state *UiState, i32 Id,
	    char const *Title,
	    irect *Bounds, b32 Flags)
{
	b32 Result = YARPG_TRUE;
        SetCurrentWindow(UiState, Id, *Bounds, Flags);

	enum DEFAULT_WINDOW_ID
	{
		INVALID_ID,
		TITLEBAR_ID,
		CLOSE_BUTTON_ID,
		SWIPER_1_ID,
		SWIPER_2_ID,
		SWIPER_3_ID,
	};

	if (!(Flags & WindowOption_TRANSPARENT))
	{
		v4 Color;
		if (UiState->Current.Interaction.Active.Window == Id)
		{
			Color = WNDCOL_ACTIVE;
		}
		else if (UiState->Current.Interaction.Hot.Window == Id)
		{
			Color = WNDCOL_HOT;
		}
		else
		{
			Color = WNDCOL_COLD;
		}

		i32 Width  = Bounds->MaxX - Bounds->MinX;
		i32 Height = Bounds->MaxY - Bounds->MinY;
		irect Box  = {
			.TopRight = IV(Width, Height),
			.BottomLeft = IV(0, 0)
		};

		DrawIRect(UiState, Box, Color);
	}

	if (Flags & WindowOption_TITLE)
	{
		irect TitleBarBounds;

		TitleBarBounds.MinX = 0;
		TitleBarBounds.MaxX = Bounds->MaxX - Bounds->MinX;
		i32 Height = Bounds->MaxY - Bounds->MinY;
		TitleBarBounds.MinY = Height - 48;
		TitleBarBounds.MaxY = Height;

		UiState->DrawableBounds.MaxY -= 48;

		iv2 Delta;
		Result = TitleBar(UiState, -TITLEBAR_ID, -CLOSE_BUTTON_ID,
				  TitleBarBounds, &Delta, Title,
				  (Flags & WindowOption_CLOSABLE));

		if (Flags & WindowOption_MOVABLE)
		{
			*Bounds = Offset(*Bounds, Delta);
		}
	}

	if (Flags & WindowOption_RESIZE_BOTTOM)
	{
		irect SwiperBounds;
		SwiperBounds.MinY = UiState->DrawableBounds.MinY;
		SwiperBounds.MaxY = UiState->DrawableBounds.MinY + 5;
		SwiperBounds.MaxX = UiState->DrawableBounds.MaxX - 5;
		SwiperBounds.MinX = UiState->DrawableBounds.MinX + 5;
		UiState->DrawableBounds.MinY += 5;
		iv2 Delta;
		if (Swiper(UiState, -SWIPER_1_ID, SwiperBounds, &Delta))
		{
                        Bounds->MinY += Delta.Y;
		}


	}

	if (Flags & WindowOption_RESIZE_RIGHT)
	{
		irect SwiperBounds;
		SwiperBounds.MaxY = UiState->DrawableBounds.MaxY - 5;
		SwiperBounds.MinY = UiState->DrawableBounds.MinY + 5;
		SwiperBounds.MaxX = UiState->DrawableBounds.MaxX;
		SwiperBounds.MinX = UiState->DrawableBounds.MaxX - 5;
		UiState->DrawableBounds.MaxX -= 5;
		iv2 Delta;
		if (Swiper(UiState, -SWIPER_2_ID, SwiperBounds, &Delta))
		{
                        Bounds->MaxX += Delta.X;
		}


	}

	if (Flags & WindowOption_RESIZE_LEFT)
	{
		irect SwiperBounds;
		SwiperBounds.MaxY = UiState->DrawableBounds.MaxY - 5;
		SwiperBounds.MinY = UiState->DrawableBounds.MinY + 5;
		SwiperBounds.MaxX = UiState->DrawableBounds.MinX + 5;
		SwiperBounds.MinX = UiState->DrawableBounds.MinX;
		UiState->DrawableBounds.MinX += 5;
		iv2 Delta;
		if (Swiper(UiState, -SWIPER_3_ID, SwiperBounds, &Delta))
		{
                        Bounds->MinX += Delta.X;
		}


	}



	return Result;
}

void
EndWindow(ui_state *UiState)
{
	UiState->CurrentWindow = 0;
}

void
VerticalBar(ui_state *UiState, irect Bounds,
	    f32 Min, f32 Val, f32 Max, v4 Color)
{
	v4 BKGColor = WGTCOL_BKG;
	DrawIRect(UiState, Bounds, BKGColor);

	// Transform data from Min -- Max to 0.0 -- 1.0

	f32 Range = Max - Min;


	f32 TransformedVal;
	if (Range < 0.0001) TransformedVal = 1.0;
	else                TransformedVal = Clamp(0.0, (Val - Min) / Range, 1.0);


	i32 BarEndX = (i32) Lerp((f32) Bounds.MinX, TransformedVal, (f32) Bounds.MaxX);

	irect BarBounds = Bounds;
	BarBounds.MaxX = BarEndX;
	BarBounds.MinY = (i32) Lerp((f32) Bounds.MinY, F32C(0.05), (f32) Bounds.MaxY);
	BarBounds.MaxY = (i32) Lerp((f32) Bounds.MinY, F32C(0.95), (f32) Bounds.MaxY);
	DrawIRect(UiState, BarBounds, Color);
}

plane_result
Plane(ui_state *UiState, plane Desc)
{
	i32 Id = Desc.Id;
	irect Bounds = Desc.Bounds;

	plane_result Result;
	Result.Updated  = Desc;

	b32 Hot       = TestAndSetHotWidget(UiState, Id, Bounds);

	// b32 Active    = TestAndSetActiveWidget(UiState, Id, Bounds);
	// b32 WasClicked = Active && !WasActive;

	// f32 Color[3];

	// if (Active) {
	// 	Color[0] = WGTCOL_ACTIVE;
	// 	Color[1] = WGTCOL_ACTIVE;
	// 	Color[2] = WGTCOL_ACTIVE;
	// } else if (Hot) {
	// 	Color[0] = WGTCOL_HOT;
	// 	Color[1] = WGTCOL_HOT;
	// 	Color[2] = WGTCOL_HOT;
	// } else {
	// 	Color[0] = WGTCOL_BKG;
	// 	Color[1] = WGTCOL_BKG;
	// 	Color[2] = WGTCOL_BKG;
	// }

	// DrawIRect(UiState, Bounds, Color);

	Result.IsHot    = Hot;
	Result.IsActive = 0;

	return Result;
}

void
Border(ui_state *UiState, irect Bounds, i32 Thickness, v4 Color)
{
	irect Up, Down, Left, Right;

	Up.MinX = Bounds.MinX;
	Up.MaxX = Bounds.MaxX;
	Up.MinY = Bounds.MaxY - Thickness;
	Up.MaxY = Bounds.MaxY;

	Down.MinX = Bounds.MinX;
	Down.MaxX = Bounds.MaxX;
	Down.MinY = Bounds.MinY;
	Down.MaxY = Bounds.MinY + Thickness;

	Left.MinX = Bounds.MinX;
	Left.MaxX = Bounds.MinX + Thickness;
	Left.MinY = Bounds.MinY;
	Left.MaxY = Bounds.MaxY;

	Right.MinX = Bounds.MaxX - Thickness;
	Right.MaxX = Bounds.MaxX;
	Right.MinY = Bounds.MinY;
	Right.MaxY = Bounds.MaxY;

	DrawIRect(UiState, Up, Color);
	DrawIRect(UiState, Down, Color);
	DrawIRect(UiState, Left, Color);
	DrawIRect(UiState, Right, Color);
}

textinput_result
Textinput(ui_state *UiState, i32 Id, irect Bounds, textinput *Desc)
{
	textinput_result Result;
	b32 BufferChanged = YARPG_FALSE;
	b32 EnterPressed  = YARPG_FALSE;

	v4 White = V(1, 1, 1, 1);

	b32 Hot = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Bounds);
	b32 WontBeActive = WillNotBeActiveWidget(UiState, Id);
	b32 WasClicked = WontBeActive && Active;

	b32 HasInput = Active;

	if (HasInput)
	{
		keyboard_data *Keyboard = &UiState->Current.Keyboard.Data;
		YASSERT(Keyboard);

		if (Keyboard->GlyphCount)
		{
			for (u32 I = 0;
			     I < Keyboard->GlyphCount;
			     ++I)
			{
				YASSERT(Desc->Head < Desc->Capacity);
				u32 BytesWritten = SaveGlyph(Desc->Capacity - Desc->Head,
							     Desc->Buffer + Desc->Head,
							     Keyboard->Text[I]);

				Desc->Head += BytesWritten;
				Desc->NumChars += 1;
				BufferChanged = YARPG_TRUE;
			}
		}
		else
		{
			i32 NumDelete = 0;
			key_state DeleteKey = Keyboard->Key[KeyId_BACKSPACE];
			if (DeleteKey.IsDown && (DeleteKey.HalfTransitions & 1))
			{
				NumDelete = KeyStateNumPressed(DeleteKey);
			}

			while (--NumDelete >= 0)
			{
				if (Desc->Head == 0) break;


				i32 NewColumn = Desc->Head;
				do
				{
					NewColumn -= 1;
				} while (NewColumn >= 0 && !UTF8HEADERBYTE(Desc->Buffer[NewColumn]));

				YASSERT(NewColumn >= 0);

				ZeroArray(Desc->Head - NewColumn, Desc->Buffer + NewColumn);
				Desc->Head = NewColumn;
				Desc->NumChars -= 1;

				BufferChanged = YARPG_TRUE;
			}
		}

		key_state EnterKey = Keyboard->Key[KeyId_ENTER];
		if (EnterKey.IsDown && (EnterKey.HalfTransitions & 1))
		{
			EnterPressed = YARPG_TRUE;
		}


		ConsumeKeyboard(UiState);
	}

	Label(UiState, Bounds, TextPosition_LEFT,
	      Desc->NumChars, (char const *) Desc->Buffer,
	      White, 1.0);

	if (HasInput)
	{
		v4 Gold = V(1.0, 0.8, 0.0, 1.0);
		Border(UiState, Bounds, 5, Gold);
        }

	Result.BufferChanged = (bool) BufferChanged;
	Result.EnterPressed  = (bool) EnterPressed;

	return Result;
}

void
Texture(ui_state *UiState, irect Bounds, texture Tex)
{
	v2 Pos = Add(ToFloat(Bounds.BottomLeft),
		     ToFloat(UiState->CurrentBox.BottomLeft));
	v2 Dims = Diff(ToFloat(Bounds.TopRight),
		       ToFloat(Bounds.BottomLeft));
	PushTexture(UiState->RenderGroup, Pos, Dims, Tex, V(1, 1, 1, 1));
}

gui_key
Key(key_id Key)
{
	gui_key Result;
	Result.Mod = (modifier) {};
	Result.Key = Key;
	return Result;
}

void
SetHotkey(ui_state *UiState, i32 Id, gui_key Key)
{
	keyboard_data *Keyboard = &UiState->Current.Keyboard.Data;
	YASSERT(Keyboard);

	key_state State;
	switch (Key.Key)
	{
		break;case Key_A:
		{
			State = Keyboard->Key[KeyId_A];
		}
		break;case Key_S:
		{
			State = Keyboard->Key[KeyId_S];
		}
		break;case Key_R:
		{
			State = Keyboard->Key[KeyId_D];
		}
		break;case Key_ESCAPE:
		{
			State = Keyboard->Key[KeyId_ESCAPE];
		}
		break;case Key_N:
		{
			State = Keyboard->Key[KeyId_N];
		}
		break;case Key_ENTER:
		{
			State = Keyboard->Key[KeyId_ENTER];
		}
		break;case Key_M:
		{
			State = Keyboard->Key[KeyId_M];
		}
		INVALIDDEFAULT();
	}

	bool IsOkayToSet = (UiState->Next.Interaction.Active.Window == 0)
		|| (UiState->Next.Interaction.Active.Window == UiState->CurrentWindow
		    && UiState->Next.Interaction.Active.Widget == 0);

	if (((State.IsDown && State.HalfTransitions >= 1) ||
	    (!State.IsDown && State.HalfTransitions >= 2))
	    && IsOkayToSet)
	{
		UiState->Next.Interaction.Active.Window = UiState->CurrentWindow;
		UiState->Next.Interaction.Active.Widget = Id;
		ConsumeKeyboard(UiState);
	}
}

b32
VerticalSlider
(
	ui_state *UiState,
	i32 Id,
	irect Bounds,
	f32 *Val,
	f32 Min,
	f32 Max
)
{
	YASSERT(UiState->CurrentWindow);
        f32 Current = *Val;
	YASSERT(Min <= Current && Current <= Max);
	f32 Percent = (Current - Min) / (Max - Min);
	if (Percent < 0.0) Percent = 0.0;
	else if (Percent > 1.0) Percent = 1.0;

	b32 Hot = TestAndSetHotWidget(UiState, Id, Bounds);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Bounds);
	b32 WasChanged = 0;

	i32 X = Bounds.MinX;
	i32 Y = Bounds.MinY;
	i32 Width = Bounds.MaxX - X;
	i32 Height = Bounds.MaxY - Y;
	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	v4 Foreground;
	if (Active)
	{
		Foreground = WGTCOL_ACTIVE;
	} else if (Hot)
	{
		Foreground = WGTCOL_HOT;
	} else {
		Foreground = WGTCOL_COLD;
	}

	if (Active)
	{
                f32 Direction = (f32) (UiState->Current.Mouse.Pos.Y - Y) / (f32) Height;
                if (Direction < 0.0)
		{
			Direction = 0.0;
		} else if (Direction > 1.0)
		{
			Direction = 1.0;
		}

//		Percent = Direction;
		*Val = Direction * (Max - Min) + Min;
		WasChanged = 1;
	}

	v4 Background = WGTCOL_BKG;

	PushRect(UiState->RenderGroup, V(X, Y), V(Width, Height), Background);
	PushRect(UiState->RenderGroup, V(X, 0.9 * Percent * Height + Y), V(Width, 0.1 * Height), Foreground);

	return WasChanged;
}
