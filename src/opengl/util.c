#include <opengl/opengl.h>

#ifdef PLATFORM
#include <stdio.h>
#else
#include <util/string.h>
#endif

void
OpenGLCheckErrors(char const *Prefix)
{
	GLenum Error = glGetError();
#ifdef PLATFORM
	while (GL_NO_ERROR != Error)
	{
		printf("[OPENGL] %s: ERROR %u\n", Prefix, Error);
		switch (Error)
		{
			break;case GL_INVALID_ENUM:                  puts("INVALID ENUM");
                        break;case GL_INVALID_VALUE:                 puts("INVALID VALUE");
			break;case GL_INVALID_OPERATION:             puts("INVALID OPERATION");
			break;case GL_STACK_OVERFLOW:                puts("STACK OVERFLOW");
			break;case GL_STACK_UNDERFLOW:               puts("STACK UNDERFLOW");
			break;case GL_OUT_OF_MEMORY:                 puts("OUT OF MEMORY");
			break;case GL_INVALID_FRAMEBUFFER_OPERATION: puts("INVALID FRAMEBUFFER OPERATION");
			break;case GL_CONTEXT_LOST:                  puts("CONTEXT LOST");
			break;default: printf("UNKOWN GL ERROR (0x%x)\n", Error);
		}
	}
#else
	YSASSERT(sizeof(Error) == sizeof(u32));
	while (GL_NO_ERROR != Error)
	{

		FormatOut("[OPENGL] {char[*]}: ERROR {u32}\n", Prefix, Error);
		switch (Error)
		{
			break;case GL_INVALID_ENUM:                  FormatOut("INVALID ENUM\n");
                        break;case GL_INVALID_VALUE:                 FormatOut("INVALID VALUE\n");
			break;case GL_INVALID_OPERATION:             FormatOut("INVALID OPERATION\n");
			break;case GL_STACK_OVERFLOW:                FormatOut("STACK OVERFLOW\n");
			break;case GL_STACK_UNDERFLOW:               FormatOut("STACK UNDERFLOW\n");
			break;case GL_OUT_OF_MEMORY:                 FormatOut("OUT OF MEMORY\n");
			break;case GL_INVALID_FRAMEBUFFER_OPERATION: FormatOut("INVALID FRAMEBUFFER OPERATION\n");
			break;case GL_CONTEXT_LOST:                  FormatOut("CONTEXT LOST\n");
			break;default: FormatOut("UNKOWN GL ERROR ({ptr})\n", (ptr)Error);
		}
	}
#endif
}
