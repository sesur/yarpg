#include <opengl/renderer.h>

#define NO_TEXTURE_SLOT (I32C(-1))

PRIVATE void
SetupMonoColoredTexture(GLuint *Texture, u32 Color)
{
	glGenTextures(1, Texture);
	GL_QUICK_CHECK();
	glBindTexture(GL_TEXTURE_2D, *Texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);

	// White with 100% Alpha

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, 1, 0, GL_RGBA,
		     GL_UNSIGNED_BYTE, &Color);

	GL_QUICK_CHECK();

	glBindTexture(GL_TEXTURE_2D, 0);
}

PRIVATE void
SetupErrorTexture(GLuint *Texture)
{
	glGenTextures(1, Texture);
	GL_QUICK_CHECK();
	glBindTexture(GL_TEXTURE_2D, *Texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);

	// White with 100% Alpha
	u32 Pixels[16] = {
		-1, 0, -1, 0,
		0, -1, 0, -1,
		-1, 0, -1, 0,
		0, -1, 0, -1
	};

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 4, 4, 0, GL_RGBA,
		     GL_UNSIGNED_BYTE, Pixels);

	GL_QUICK_CHECK();

	glBindTexture(GL_TEXTURE_2D, 0);
}

PRIVATE void
CheckShader(render_shader Shader)
{
	YASSERT(Shader.Program != 0);
	for (i32 Idx = 0;
	     (u32) Idx < ARRAYCOUNT(Shader.Attributes);
	     ++Idx)
	{
		YASSERT(Shader.Attributes[Idx] != -1);
	}

	for (i32 Idx = 0;
	     (u32) Idx < ARRAYCOUNT(Shader.Uniforms);
	     ++Idx)
	{
		YASSERT(Shader.Uniforms[Idx] != -1);
	}
}

PRIVATE void
InitTextureStore(texture_store *Store, memory_arena *Arena)
{
	for (u64 I = 0;
	     I < TEXTURE_STORE_NUM_ENTRIES;
	     ++I)
	{
		Store->Ids[I] = INVALID_SPRITE_ID;
		Store->Status[I] = TextureStatus_UNUSED;
	}

	glGenTextures(TEXTURE_STORE_NUM_ENTRIES, Store->Textures);

	Store->Free  = NULL;
	Store->Arena = Arena;
}

PRIVATE render_batch *
NewSizedBatch(renderer *Renderer, memory_arena *Arena, i32 Size)
{
	idx TextureSlots = Renderer->Backend->TextureSlots;
	YASSERT(TextureSlots >= 2);

	render_batch *Result = PushStruct(Arena, render_batch);
	YASSERT(Result);
	Result->IsMono        = PushArray(Arena, TextureSlots, *Result->IsMono);
	Result->Textures      = PushArray(Arena, TextureSlots, *Result->Textures);

	Result->Indices       = PushArray(Arena, Size, *Result->Indices);
	Result->VertexData    = PushArray(Arena, Size, *Result->VertexData);
	YASSERT(Result->IsMono);
	YASSERT(Result->Textures);

	Result->Size         = Size;
	Result->Free         = Result->Size;
	Result->TexturesUsed = 2;
	Result->Textures[0]  = Renderer->Backend->NullTexture;
	Result->Textures[1]  = Renderer->Backend->ErrorTexture;
	Result->TextureCount = (i32) TextureSlots;
	Result->VertexCount  = 0;


	return Result;
}

void
CreateBackend(opengl_backend *Backend, memory_arena *Perm)
{
	glGenBuffers(2, Backend->VertexBuffers);
	glGenVertexArrays(1, &Backend->StreamVAO);

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &Backend->TextureSlots);
	YASSERT(Backend->TextureSlots > 8);

	u32 White = 0xFFFFFFFF;
	SetupMonoColoredTexture(&Backend->NullTexture, White);
	SetupErrorTexture(&Backend->ErrorTexture);
	InitTextureStore(&Backend->Store, Perm);

	str Buffer = MakeStr(512, Perm);
	format_result Res = FormatInto(&Buffer, VIEWC("#define NUM_TEXTURES {u32}\n"),
				       Backend->TextureSlots);
	str_view PreambleView = ViewOf(Buffer);
	YASSERTF(Res.RequiredSize == Res.WrittenSize,
		 "%u < %u", Res.RequiredSize, Res.WrittenSize);

	FormatOut("[OPENGL BACKEND] Preamble Size: {i32}, Content: '{str}'\n",
		  PreambleView.Size, PreambleView);


	Backend->ShaderPreamble = PreambleView;
}

void
RendererCreate(renderer *Renderer, opengl_backend *Backend, render_shader Shader)
{
//	DURING_DEBUG( CheckShader(Shader); );

	Renderer->Backend = Backend;

	glUseProgram(Shader.Program);
	glBindVertexArray(Renderer->Backend->StreamVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer->Backend->StreamVBO);
	glBufferData(GL_ARRAY_BUFFER,
		     BATCH_VERTEX_COUNT * sizeof(batch_data),
		     NULL, GL_DYNAMIC_DRAW);

	GL_QUICK_CHECK();

	glVertexAttribPointer(Shader.Position, 3, GL_FLOAT, GL_FALSE,
			      sizeof(batch_data),
			      (void const *) offsetof(batch_data, Data.Position));
	glVertexAttribPointer(Shader.Color, 4, GL_FLOAT, GL_FALSE,
			      sizeof(batch_data),
			      (void const *) offsetof(batch_data, Data.Color));
	glVertexAttribPointer(Shader.Transform, 3, GL_FLOAT, GL_FALSE,
			      sizeof(batch_data),
			      (void const *) offsetof(batch_data, Data.Transform));
	glVertexAttribPointer(Shader.TexturePos, 2, GL_FLOAT, GL_FALSE,
			      sizeof(batch_data),
			      (void const *) offsetof(batch_data, Data.TexturePos));
	glVertexAttribPointer(Shader.TextureId, 1, GL_FLOAT, GL_FALSE,
			      sizeof(batch_data),
			      (void const *) offsetof(batch_data, TextureId));

	YASSERT(offsetof(batch_data, Data.Position)
		     == offsetof(vertex_data, Position));

	YASSERT(offsetof(batch_data, Data.Transform)
		     == offsetof(vertex_data, Transform));

	YASSERT(offsetof(batch_data, Data.Color)
		     == offsetof(vertex_data, Color));

	GL_QUICK_CHECK();

	glEnableVertexAttribArray(Shader.Position);
	glEnableVertexAttribArray(Shader.Color);
	glEnableVertexAttribArray(Shader.Transform);
	glEnableVertexAttribArray(Shader.TextureId);
	glEnableVertexAttribArray(Shader.TexturePos);

	GL_QUICK_CHECK();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Renderer->Backend->StreamIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		     BATCH_VERTEX_COUNT * sizeof(u32),
		     NULL, GL_DYNAMIC_DRAW);


	str Buffer = TEMPSTR(128);
	for (GLint Idx = 0;
	     Idx < (GLint) Renderer->Backend->TextureSlots;
	     ++Idx)
	{
		YSASSERT(sizeof(GLint) == sizeof(i32));
		FormatInto(Reset(&Buffer), VIEWC("Textures[{i32}]"), Idx);
		GLint Location = glGetUniformLocation(Shader.Program, AsCStr(Buffer));
		YASSERT(Location != -1);
		GL_QUICK_CHECK();
		glUniform1i(Location, Idx);
	}

	/* GLint TextureSlots[Renderer->TextureSlots]; */

	/* for (i64 I = 0; */
	/*      I < Renderer->TextureSlots; */
	/*      ++I) */
	/* { */
	/* 	TextureSlots[I] = I; */
	/* } */

	/* glUniform1iv(glGetUniformLocation(Shader.Program, "Textures"), */
	/* 	     Renderer->TextureSlots, TextureSlots); */

	GL_QUICK_CHECK();

	Renderer->IsMonoLocation = glGetUniformLocation(Shader.Program, "IsMono");

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glUseProgram(0);

	GL_QUICK_CHECK();

	Renderer->Shader = Shader;
	Renderer->CurrentTextureId = 0; // NullTexture is on


}

PRIVATE void
ResetBatch(renderer *Renderer, render_batch *Batch)
{
	Batch->Free         = Batch->Size;
	Batch->VertexCount  = 0;
	Batch->TexturesUsed = 2;

	/* This is always true; dont need to reset it!
	 * Result->Textures[0]  = Renderer->NullTexture;
	 * Result->Textures[1]  = Renderer->ErrorTexture; */

	if (Renderer->CurrentTextureId > 1)
	{
		// if the current texture is _not_ the null or error texture
		// then we have to preserve that

		Batch->Textures[2] =
			Batch->Textures[Renderer->CurrentTextureId];
		Batch->IsMono[2] =
			Batch->IsMono[Renderer->CurrentTextureId];
		Renderer->CurrentTextureId = 2;
		Batch->TexturesUsed = 3;
	}
}

void
FlushRenderer(renderer *Renderer)
{
	render_batch *Current = Renderer->Batch;
	i32 Used = Current->Size - Current->Free;
	if (Used == 0) return;
	u64 NumData = Current->VertexCount * sizeof(batch_data) + Used * sizeof(u32);
	INSTRUMENT_COUNTED(Flushing, NumData)
	{
		INSTRUMENT(Setup)
		{
			glUseProgram(Renderer->Shader.Program);
			glBindVertexArray(Renderer->Backend->StreamVAO);
			glBindBuffer(GL_ARRAY_BUFFER, Renderer->Backend->StreamVBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Renderer->Backend->StreamIBO);
			glUniformMatrix4fv(
				Renderer->Shader.Projection,
				1, GL_TRUE, Renderer->Projection);
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

			for (i32 Idx = 0;
			     Idx < Current->TexturesUsed;
			     ++Idx)
			{
				glActiveTexture(GL_TEXTURE0 + Idx);
				glBindTexture(GL_TEXTURE_2D, Current->Textures[Idx]);
				GL_QUICK_CHECK();

			}
			glUniform1iv(Renderer->IsMonoLocation, Current->TexturesUsed, Current->IsMono);

			YASSERT(Used < Current->Size);

			DEBUG_DO(
				Renderer->DebugInfo.DrawCalls    += 1;
				Renderer->DebugInfo.TexturesUsed += Current->TexturesUsed;
				Renderer->DebugInfo.IndicesUsed  += Used;
				Renderer->DebugInfo.BytesUploaded += NumData;
				);
		}

		/* glBufferSubData( */
		/* 	GL_ARRAY_BUFFER, 0, */
		/* 	Used * sizeof(batch_data), */
		/* 	Current->VertexData); */
		/* glBufferSubData( */
		/* 	GL_ELEMENT_ARRAY_BUFFER, 0, */
		/* 	Used * sizeof(u32), */
		/* 	Current->Indices); */

		INSTRUMENT_COUNTED(Upload, Used * (sizeof(batch_data) + sizeof(u32)))
		{
			glBufferData(GL_ARRAY_BUFFER,
				     Current->VertexCount * sizeof(batch_data),
				     Current->VertexData, GL_DYNAMIC_DRAW);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				     Used * sizeof(u32),
				     Current->Indices, GL_DYNAMIC_DRAW);
		}

		INSTRUMENT(DrawCall)
		{
			glDrawElements(
				GL_TRIANGLES,
				Used,
				GL_UNSIGNED_INT,
				NULL);
		}

		GL_QUICK_CHECK();

		ResetBatch(Renderer, Current);
	}
}

PRIVATE i32
RegisterTexture(render_batch *Batch, GLuint Texture, b32 IsMono)
{
	i32 Result = NO_TEXTURE_SLOT; // not found

	GLuint *Register = Batch->Textures;
	for (i32 Idx = 0;
	     Result == NO_TEXTURE_SLOT && Idx < Batch->TexturesUsed;
	     ++Idx)
	{
                if (Texture == Register[Idx])
		{
			Result = Idx;
		}
	}

	if (Result == NO_TEXTURE_SLOT && Batch->TexturesUsed < Batch->TextureCount)
	{
                Result = Batch->TexturesUsed;
		++Batch->TexturesUsed;
		Batch->Textures[Result] = Texture;
		Batch->IsMono[Result] = IsMono;
	}

	return Result;
}

void
RendererPrepare(renderer *Renderer, memory_arena *Arena, f32 Projection[4][4])
{
	Renderer->Batch = NewSizedBatch(Renderer, Arena, BATCH_VERTEX_COUNT);
	Renderer->Arena = Arena;
	Renderer->CurrentTextureId = 0; // NullTexture
	Renderer->Projection = (f32 *) Projection;

	DEBUG_DO(
		Renderer->DebugInfo.DrawCalls    = 0;
		Renderer->DebugInfo.TexturesUsed = 0;
		Renderer->DebugInfo.IndicesUsed  = 0;
		Renderer->DebugInfo.BytesUploaded = 0;
		Renderer->DebugInfo.TextureCount = Renderer->Batch->TextureCount;
		Renderer->DebugInfo.IndexCount = Renderer->Batch->Size;
		);
}

PRIVATE void
AddTriangles(renderer *Renderer,
	     i32 NumVertices, vertex_data Data[],
	     i32 NumIndices,  u32 Indices[])
{
	// NumVertices should always be smaller than NumIndices
	YASSERT(NumVertices <= NumIndices);
	i32 NumData = NumIndices;
	YASSERT(NumData < BATCH_VERTEX_COUNT);

	render_batch *Current = Renderer->Batch;
	if (Current->Free < NumData)
	{
		FlushRenderer(Renderer);
		/* render_batch *Next = NewBatch(Renderer); */
		/* Current->Next = Next; */
		/* Renderer->Last = Next; */
                /* Current = Next; */
	}
	YASSERT(NumData < Current->Free);
	u32 IndexStart  = Current->Size - Current->Free;
	u32 VertexStart = Current->VertexCount;
	Current->Free -= NumData;
	Current->VertexCount += NumVertices;
        for (i32 i = 0; i < NumVertices; ++i)
	{
                Current->VertexData[VertexStart+i].Data = Data[i];
		Current->VertexData[VertexStart+i].TextureId = (f32) Renderer->CurrentTextureId;
		/* printf("Written: %f from %d\n", Current->VertexData[Start+i].TextureId, */
		/*        Renderer->CurrentTextureId); */
	}
	for (i32 i = 0; i < NumIndices; ++i)
	{
                Current->Indices[IndexStart+i] = Indices[i] + VertexStart;
	}
}

void
DrawTriangles(renderer *Renderer,
	      u32 NumVertices, vertex_data *Data)
{
	if (NumVertices >= 3)
	{
		u32 NumTriangles = NumVertices/3;
		u32 NumIndices = NumTriangles * 3;
		u32 First = 0, Second = 1, Third = 2;

                u32 (*Indices)[3] = (u32 (*)[3]) PushArray(Renderer->Arena, NumTriangles * 3, f32);
                for (u32 CurrentTriangle = 0;
		     CurrentTriangle < NumTriangles;
		     ++CurrentTriangle)
		{
			YASSERT(First < NumVertices);
			YASSERT(Second < NumVertices);
			YASSERT(Third < NumVertices);
			Indices[CurrentTriangle][0] = First;
			Indices[CurrentTriangle][1] = Second;
			Indices[CurrentTriangle][2] = Third;
			First  = Third + 1;
			Second = First + 1;
			Third  = Second + 1;
		}
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, (u32 *) Indices);
	}
}

void
DrawTriangleElements(renderer *Renderer,
		     u32 NumVertices, vertex_data *Data,
		     u32 NumIndices, u32 *Indices)
{
	if (NumVertices >= 3)
	{
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, Indices);
	}
}

void
DrawTriangleStrip(renderer *Renderer,
		  u32 NumVertices, vertex_data *Data)
{
	if (NumVertices >= 3)
	{
		u32 NumTriangles = NumVertices - 2;
		u32 NumIndices = 3 * NumTriangles;
		u32 First = 0, Second = 1, Third = 2;
		u32 (*Indices)[3] = (u32 (*)[3]) PushArray(Renderer->Arena, NumTriangles * 3, f32);
                for (u32 CurrentTriangle = 0;
		     CurrentTriangle < NumTriangles;
		     ++CurrentTriangle)
		{

			YASSERT(First < NumVertices);
			YASSERT(Second < NumVertices);
			YASSERT(Third < NumVertices);
			Indices[CurrentTriangle][0] = First;
			Indices[CurrentTriangle][1] = Second;
			Indices[CurrentTriangle][2] = Third;
			First = Second;
			Second = Third;
			Third  = Third + 1;
		}
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, (u32 *) Indices);
	}
}

void
DrawTriangleFan(renderer *Renderer,
		u32 NumVertices, vertex_data *Data)
{
	if (NumVertices >= 3)
	{
		u32 NumTriangles = NumVertices - 2;
		u32 NumIndices = 3 * NumTriangles;
		u32 First = 0, Second = 1, Third = 2;
		u32 (*Indices)[3] = (u32 (*)[3]) PushArray(Renderer->Arena, NumTriangles * 3, f32);
                for (u32 CurrentTriangle = 0;
		     CurrentTriangle < NumTriangles;
		     ++CurrentTriangle)
		{
			YASSERT(First < NumVertices);
			YASSERT(Second < NumVertices);
			YASSERT(Third < NumVertices);
			Indices[CurrentTriangle][0] = First;
			Indices[CurrentTriangle][1] = Second;
			Indices[CurrentTriangle][2] = Third;
			Second = Third;
			Third  = Third + 1;
		}
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, (u32 *) Indices);
	}
}

void
RendererFinish(renderer *Renderer)
{

	FlushRenderer(Renderer);
	Renderer->Batch = NULL;
	Renderer->Arena = NULL;
	Renderer->CurrentTextureId = 0; // NullTexture
	Renderer->Projection = NULL;
}

void
ReleaseBackend(opengl_backend *Backend)
{
	glDeleteBuffers(2, Backend->VertexBuffers);
	glDeleteVertexArrays(1, &Backend->StreamVAO);
	glDeleteTextures(1, &Backend->NullTexture);
	glDeleteTextures(1, &Backend->ErrorTexture);
	glDeleteTextures(TEXTURE_STORE_NUM_ENTRIES, Backend->Store.Textures);
}

void
RendererRelease(renderer *Renderer)
{
	glDeleteProgram(Renderer->Shader.Program);
}

PRIVATE void
BindTexture_(renderer *Renderer, GLuint Texture, b32 IsMono)
{
        i32 TextureId = RegisterTexture(Renderer->Batch, Texture, IsMono);
	if (TextureId == NO_TEXTURE_SLOT)
	{
		FlushRenderer(Renderer);
		/* render_batch *Next = NewBatch(Renderer); */
		/* Renderer->Last->Next = Next; */
		/* Renderer->Last = Next; */
		TextureId = RegisterTexture(Renderer->Batch, Texture, IsMono);
	}
	YASSERT(TextureId != NO_TEXTURE_SLOT);
	Renderer->CurrentTextureId = TextureId;
}

void
BindMonoTexture(renderer *Renderer, GLuint Texture)
{
	BindTexture_(Renderer, Texture, YARPG_TRUE);
}

void
BindTexture(renderer *Renderer, GLuint Texture)
{
	BindTexture_(Renderer, Texture, YARPG_FALSE);
}

PRIVATE TASK_CALLBACK_FN(ThreadedTextureLoad)
{
	threaded_texture_load_args *Args = *(threaded_texture_load_args * volatile *) Arg;

	texture_store *Store = Args->Store;
	i32 Idx = Args->Idx;

	INSTRUMENT_COUNTED(ThreadedTextureLoad, Args->ToLoad.Width * Args->ToLoad.Height)
	{
		glBindTexture(GL_TEXTURE_2D, Store->Textures[Idx]);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
			     Args->ToLoad.Width, Args->ToLoad.Height,
			     0, GL_RGBA, GL_UNSIGNED_BYTE, Args->ToLoad.Data);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
				GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
				GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
				GL_LINEAR);

		// TODO: better idea:
		// instead of loading the image into game memory
		// and copying it to the gpu when needed
		// its probably better to first create the memory on the gpu
		// map it into ram and load the image directly into it.
		// This would mean that we would not need to give threads
		// the ability to call opengl functions at all.
		// Might even be faster than this, since the synchronisation
		// with glFinish is pretty slow.
		glFinish();

		COMPILERBARRIER();

		InterlockedWrite(&Store->Status[Idx], TextureStatus_LOADED);

		GL_QUICK_CHECK();

		while(!InterlockedCompareExchange(&Store->Free, EXCHANGE_WEAK,
						  &Args->Next, Args));
	}
}

PRIVATE void
LoadThreadedTexture(texture_store *Store, i32 Idx, image ToLoad)
{
	threaded_texture_load_args *Args;
	if ((Args = InterlockedRead(&Store->Free)))
	{
		FormatOut("[THREADED LOAD] Old Args\n");
		while(!InterlockedCompareExchange(&Store->Free, EXCHANGE_WEAK,
						  &Args, Args->Next));
	}
	else
	{
		FormatOut("[THREADED LOAD] New Args\n");
		Args = PushStruct(Store->Arena, *Args);
	}

	Args->Store  = Store;
	Args->Idx    = Idx;
	Args->ToLoad = ToLoad;

	PushHighPriorityTask(ThreadedTextureLoad, Args);
}

loaded_texture
RegisterForFrame(opengl_backend *Backend, asset_store *Assets, sprite_id SpriteId)
{
	texture_store *Store = &Backend->Store;
	GLuint ErrorTexture = Backend->ErrorTexture;
	GLuint NullTexture  = Backend->NullTexture;
	YSASSERT(sizeof(GLuint) == sizeof(u32));
	u32 Texture;
	idx Id = SpriteId.ToIdx;
	if (Id >= 0)
	{
		TODO(Use id_map);
#if 0
		id_index *Slot = GetOrAllocIdSlot(Store->Map, (entity_id) {SpriteId.ToIdx});
		if (!Slot)
		{
			Texture = ErrorTexture;
		}
		else
		{
			id_index Idx = *Slot;
			if (Idx == -1)
			{
				Texture = NullTexture;
			}
			else
			{
				Texture = Textures[Idx];
			}
		}
#else
		u32 UId = (u32) Id;
		u32 H = Hash(UId) % ARRAYCOUNT(Store->Ids);

		i32 Idx = (i32) H;
		YASSERT(Idx >= 0);
		while (1)
		{
			if (Store->Ids[Idx].ToIdx == Id)
			{
				YASSERT(InterlockedRead(&Store->Status[Idx]) != TextureStatus_UNUSED);
				break;
			}
			else if (Store->Ids[Idx].ToIdx == INVALID_SPRITE_ID.ToIdx)
			{
				YASSERT(InterlockedRead(&Store->Status[Idx]) == TextureStatus_UNUSED);
				Store->Ids[Idx] = SpriteId;
				break;
			}
			else
			{
				Idx += 1;
				Idx %= ARRAYCOUNT(Store->Ids);

				if (Idx == (i32) H)
				{
					Idx = -1;
					break;
				}
			}
		}

		if (Idx >= 0)
		{
			texture_status Status = InterlockedRead(&Store->Status[Idx]);
			switch (Status)
			{
				break;case TextureStatus_UNUSED:
				{
					image Image;
					if (TryLoadSprite(Assets, SpriteId, &Image))
					{
						InterlockedWrite(&Store->Status[Idx], TextureStatus_WORKED);

						LoadThreadedTexture(Store, Idx,
								    Image);

						Texture = NullTexture;
					} else {
						FetchSprite(Assets, SpriteId);
						Store->Status[Idx] = TextureStatus_QUEUED;
						Texture = NullTexture;
					}

				}
				break;case TextureStatus_QUEUED:
				{
					image Image;
					if (TryLoadSprite(Assets, SpriteId, &Image))
					{
						InterlockedWrite(&Store->Status[Idx], TextureStatus_WORKED);

						LoadThreadedTexture(Store, Idx,
								    Image);

						FormatOut("[RENDERER] Threaded load of {i32}\n", Idx);

					}
					Texture = NullTexture;
				}
				break;case TextureStatus_WORKED:
				{
					Texture = NullTexture;
				}
				break;case TextureStatus_LOADED:
				{
					Texture = Store->Textures[Idx];
				}
				INVALIDDEFAULT();
			}
		}
		else
		{
			Texture = ErrorTexture;
		}
#endif
	}
	else
	{
		Texture = ErrorTexture;
	}
	loaded_texture Result = { Texture };

	return Result;
}

void
UnbindTexture(renderer *Renderer)
{
        BindTexture(Renderer, Renderer->Backend->NullTexture);
}

triangle_alloc
AllocTriangles(renderer *Renderer, u32 NumTriangles)
{
	triangle_alloc Alloc = {};

	if (NumTriangles != 0)
	{
		idx NumVertices = 3 * NumTriangles;
		render_batch *Current = Renderer->Batch;

		if (Current->Free < 3)
		{
			FlushRenderer(Renderer);
		}

		YASSERT(Current->Free >= 3);

		if (NumVertices > Current->Free)
		{
			NumTriangles = Current->Free / 3;
			NumVertices  = NumTriangles * 3;
		}

		YASSERT(NumVertices <= Current->Free);

		u32 IndexStart       = Current->Size - Current->Free;
		u32 VertexStart      = Current->VertexCount;
		Current->Free        -= (i32) NumVertices;
		Current->VertexCount += (i32) NumVertices;

		Alloc.Count = NumTriangles;

		Alloc.Positions  = &Current->VertexData[VertexStart].Data.Position;
		Alloc.Transforms = &Current->VertexData[VertexStart].Data.Transform;
		Alloc.TexturePos = &Current->VertexData[VertexStart].Data.TexturePos;
		Alloc.Colors     = &Current->VertexData[VertexStart].Data.Color;

		Alloc.PositionStride   = sizeof(batch_data);
		Alloc.TransformStride  = sizeof(batch_data);
		Alloc.TexturePosStride = sizeof(batch_data);
		Alloc.ColorStride      = sizeof(batch_data);

		for (idx Index = 0; Index < NumVertices; ++Index)
		{
			Current->Indices[IndexStart+Index] = VertexStart + (u32) Index;
			Current->VertexData[IndexStart+Index].TextureId =
				(f32) Renderer->CurrentTextureId;
		}
	}

	return Alloc;
}

PRIVATE GLenum
TextureStorageType(b32 IsMono)
{
	GLenum Result;
	if (IsMono)
	{
		Result = GL_RED;
	}
	else
	{
		Result = GL_RGBA;
	}
	return Result;
}

PRIVATE custom_texture
AllocateTexture(b32 IsMono, iv2 Size, void *Data)
{
	custom_texture Result;

	GLuint Id;
	glGenTextures(1, &Id);

	GLenum Type = TextureStorageType(IsMono);
	glBindTexture(GL_TEXTURE_2D, Id);
	glTexImage2D(GL_TEXTURE_2D, 0, Type, Size.Width, Size.Height, 0,
		     Type, GL_UNSIGNED_BYTE, Data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // not sure if we need this

	Result.Id     = Id;
	Result.Size   = Size;
	Result.IsMono = IsMono;

	GL_QUICK_CHECK();

	return Result;
}

custom_texture
CreateTexture(opengl_backend *Backend, iv2 Size)
{
	custom_texture Result = AllocateTexture(0, Size, NULL);

	return Result;
}

custom_texture
CreateMonoTexture(opengl_backend *Backend, iv2 Size)
{
	custom_texture Result = AllocateTexture(1, Size, NULL);

	return Result;
}

custom_texture
LoadImage(opengl_backend *Backend, image Image)
{
	iv2 Size = IV(Image.Width, Image.Height);
	custom_texture Result = AllocateTexture(1, Size, Image.Data);

	return Result;
}

void
ReleaseTexture(custom_texture Tex)
{
	glDeleteTextures(1, &Tex.Id);
}

void
WriteTexture(custom_texture Tex, irect Region, void *Image)
{
	glBindTexture(GL_TEXTURE_2D, Tex.Id);
	GLenum Type = TextureStorageType(Tex.IsMono);

	i32 Width = Region.MaxX - Region.MinX;
	i32 Height = Region.MaxY - Region.MinY;

	glTexSubImage2D(
		GL_TEXTURE_2D,
		0, Region.MinX, Region.MinY,
		Width, Height,
		Type,
		GL_UNSIGNED_BYTE,
		Image);

	GL_QUICK_CHECK();
}

void
ZeroTexture(custom_texture Tex)
{
	/*
	GLuint FrameBuffer;
	glGenFramebuffers(1, &FrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, FrameBuffer);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, Tex.Id, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	YASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
	glViewport(0, 0, Tex.Size.Width, Tex.Size.Height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	*/

	// Just hope this is not really needed!
}
