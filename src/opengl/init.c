#include <opengl/init.h>

#include <stdio.h>

static void
InitGLFW()
{
	i32 Success = glfwInit();
	/* glfwInit can set errno even if succeds */
	YASSERT(Success == GLFW_TRUE);
}

static void
CloseGLFW()
{
	glfwTerminate();
}

#if 0

static void GLAPIENTRY
MessageCallback(GLenum Source, GLenum Type, GLuint Id,
		GLenum Severity, GLsizei Length,
		GLchar const *Message, void const *UserData)
{
	fprintf(stderr,
		"[GL %s] type: 0x%x, severity = 0x%x, message = '%s'\n",
		(Type == GL_DEBUG_TYPE_ERROR ? "ERROR" : "WARNING"),
		Type, Severity, Message);
}

#endif

static GLFWwindow *
OpenWindow(opengl Options, char const *Title, i32 Width, i32 Height)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, Options.Major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, Options.Minor);
	glfwWindowHint(GLFW_OPENGL_PROFILE,        Options.Profile);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, Options.ForwardCompat);
	glfwWindowHint(GLFW_SAMPLES,               Options.AntiAlias);
	glfwWindowHint(GLFW_RESIZABLE,             Options.Resizable);

	GLFWmonitor *Monitor = Options.FullScreen ? glfwGetPrimaryMonitor() : NULL;

	GLFWwindow* Window = glfwCreateWindow(Width, Height, Title, Monitor, NULL);

	YASSERT(Window);

	/* glfwCreateWindow can set errno even if it succeeds */
	glfwMakeContextCurrent(Window);

	int Success = gladLoadGL(glfwGetProcAddress);

	YASSERT(Success, "Could not load opengl functions.");

	if (glfwExtensionSupported("EGL_EXT_swap_control_tear"))
	{
		puts("[OPENGL] Running with screen tear control. (EGL)");
		glfwSwapInterval(-1);
	}
	else if (glfwExtensionSupported("GLX_EXT_swap_control_tear"))
	{
		puts("[OPENGL] Running with screen tear control. (GLX)");
		glfwSwapInterval(-1);
	} else if (glfwExtensionSupported("WGL_EXT_swap_control_tear")){
		puts("[OPENGL] Running with screen tear control. (WGL)");
		glfwSwapInterval(-1);
	} else {
		puts("[OPENGL] Running without screen tear control");
		glfwSwapInterval(1);
	}

	printf("[OPENGL] Vendor: %s\n",      (char const *) glGetString(GL_VENDOR));
	printf("[OPENGL] Renderer: %s\n",    (char const *) glGetString(GL_RENDERER));
	printf("[OPENGL] Version: %s\n",     (char const *) glGetString(GL_VERSION));
	printf("[OPENGL] Shader Lang: %s\n", (char const *) glGetString(GL_SHADING_LANGUAGE_VERSION));

#if 0
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);
#endif

	return Window;
}

static void
CloseWindow(GLFWwindow *Window)
{
	glfwDestroyWindow(Window);
}
