#include <opengl/shader.h>

static void
LoadAttributes(GLuint Program,
	       u32 NumAttributes,
	       char const *AttributeNames[static NumAttributes],
	       GLint AttributeLocations[static NumAttributes])
{
        YASSERT(NumAttributes > 0);
	YASSERT(AttributeNames);
	YASSERT(AttributeLocations);

	for (u32 Attribute = 0;
	     Attribute < NumAttributes;
	     ++Attribute)
	{
		AttributeLocations[Attribute] =
			glGetAttribLocation(Program, AttributeNames[Attribute]);
	}
}

static void
LoadUniforms(GLuint Program,
	     u32 NumUniforms,
	     char const *UniformNames[static NumUniforms],
	     GLint UniformLocations[static NumUniforms])
{
        YASSERT(NumUniforms > 0);
	YASSERT(UniformNames);
	YASSERT(UniformLocations);

	for (u32 Uniform = 0;
	     Uniform < NumUniforms;
	     ++Uniform)
	{
		UniformLocations[Uniform] =
			glGetUniformLocation(Program, UniformNames[Uniform]);
	}
}

static void
ReportStageError(GLuint Stage)
{
	GLint LogLength;
	GLsizei WrittenLength;
	glGetShaderiv(Stage, GL_INFO_LOG_LENGTH, &LogLength);
	YASSERT(LogLength > 0);
	char Log[LogLength];
	glGetShaderInfoLog(Stage, LogLength, &WrittenLength, Log);

	// WrittenLength does _not_ include the 0 Terminator
	YASSERT(WrittenLength < LogLength);

#ifdef PLATFORM
	puts("Stage Error. Log Attached:");
	puts(Log);
#else
	FormatErr("[OPENGL SHADER] Stage Error. Log Attached:\n---\n{char[*]}\n---\n",
		  Log);
#endif
}

static void
ReportProgramError(GLuint Program)
{
        GLint LogLength;
	GLsizei WrittenLength;
	glGetProgramiv(Program, GL_INFO_LOG_LENGTH, &LogLength);
	char Log[LogLength];
	glGetProgramInfoLog(Program, LogLength, &WrittenLength, Log);

	// WrittenLength does _not_ include the 0 Terminator
	YASSERT(WrittenLength < LogLength);

#ifdef PLATFORM
	puts("Program Error. Log Attached:");
	puts(Log);
#else
	FormatErr("[OPENGL SHADER] Program Error. Log Attached:\n---\n{char[*]}\n---\n",
		  Log);
#endif
}

static b32
LoadStage(GLenum Type,
	  char const *Source,
	  GLuint *Stage)
{
	GLuint Shader = glCreateShader(Type);
	*Stage = Shader;
	glShaderSource(Shader, 1, &Source, NULL);
	glCompileShader(Shader);

	GLint Status;
	glGetShaderiv(Shader, GL_COMPILE_STATUS, &Status);

	return Status == GL_TRUE;
}

b32
BuildProgram(u32 NumStages,
	     char const *Pipeline[static NumStages],
	     GLenum StageTypes[static NumStages],
	     GLuint *Result)
{
	GLuint Program = glCreateProgram();
	*Result = Program;
        if (!Program) return YARPG_FALSE;
	////////////////////////////////////////////////////////////////////////
	/// Compile & Attach all the stages
	////////////////////////////////////////////////////////////////////////
	YASSERT(NumStages != 0);
	YASSERT(Pipeline);
	GLuint Stages[NumStages];
	for (u32 CurrentStage = 0;
	     CurrentStage < NumStages;
	     ++CurrentStage)
	{
		YASSERT(Pipeline[CurrentStage] != NULL);

		if (!LoadStage(StageTypes[CurrentStage],
			       Pipeline[CurrentStage],
			       &Stages[CurrentStage]))
		{
			ReportStageError(Stages[CurrentStage]);
			YASSERT(0);
		}
		glAttachShader(Program, Stages[CurrentStage]);
	}



	////////////////////////////////////////////////////////////////////////
	/// Link & Validate the program
	////////////////////////////////////////////////////////////////////////

	GLint status;
	glLinkProgram(Program);
	glGetProgramiv(Program, GL_LINK_STATUS, &status);
	if (status != GL_TRUE)
	{
		ReportProgramError(Program);
                return YARPG_FALSE;
	}

	glValidateProgram(Program);
        glGetProgramiv(Program, GL_VALIDATE_STATUS, &status);
	if (status != GL_TRUE)
	{
		ReportProgramError(Program);
                return YARPG_FALSE;
	}

	////////////////////////////////////////////////////////////////////////
	/// Cleanup used Stages to free up VRAM
	////////////////////////////////////////////////////////////////////////

	// we should technically call glDetachShader first
	// but this can make debugging harder
	// since then the graphics card cannot refer to the shaders source
	// code.

	for (u32 CurrentStage = 0;
	     CurrentStage < NumStages;
	     ++CurrentStage)
	{
#ifndef DEBUG
		glDetachShader(Program, Stages[CurrentStage]);
#endif
		// in any case we mark the shader as deleted, so it gets cleaned
		// up when the program gets deleted
		glDeleteShader(Stages[CurrentStage]);
	}
        return YARPG_TRUE;
}


void
LookupProgramLocations(GLuint Program,
		       u32 NumAttributes,
		       char const *AttributeNames[static NumAttributes],
		       GLint AttributeLocations[static NumAttributes],
		       u32 NumUniforms,
		       char const *UniformNames[static NumUniforms],
		       GLint UniformLocations[static NumUniforms])
{
	if (NumAttributes > 0)
	{
		LoadAttributes(Program, NumAttributes, AttributeNames, AttributeLocations);
	}
        if (NumUniforms > 0)
	{
		LoadUniforms(Program, NumUniforms, UniformNames, UniformLocations);
	}
}
