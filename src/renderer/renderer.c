#include <def.h>

static u32 RenderEntitySize[] = {
	[RenderEntityType_RECTANGLE]    = sizeof(render_entity_rectangle),
	[RenderEntityType_HEXAGON]      = sizeof(render_entity_hexagon),
	[RenderEntityType_LINE]         = sizeof(render_entity_line),
	[RenderEntityType_TRIANGLE]     = sizeof(render_entity_triangle),
	[RenderEntityType_TEXTURE]      = sizeof(render_entity_texture),
};
static u16 RenderEntityAlignment[] = {
	[RenderEntityType_RECTANGLE]    = alignof(render_entity_rectangle),
	[RenderEntityType_HEXAGON]      = alignof(render_entity_hexagon),
	[RenderEntityType_LINE]         = alignof(render_entity_line),
	[RenderEntityType_TRIANGLE]     = alignof(render_entity_triangle),
	[RenderEntityType_TEXTURE]      = alignof(render_entity_texture),
};

YSASSERT(ARRAYCOUNT(RenderEntitySize) == RenderEntityType_COUNT);
YSASSERT(ARRAYCOUNT(RenderEntityAlignment) == RenderEntityType_COUNT);

static void *
RenderAlloc2(render_group *Group, render_entity_type Type,
	     u32 Size, u16 Alignment)
{
	render_commands *Commands = Group->Commands;
	u32 LastDataStart;
	render_entity_header *Header;

	b32 NewHeader = 1;
	if (Commands->NumHeader == 0)
	{
		// we are currently writing the first header
		LastDataStart = Commands->Capacity;
		Header = &((render_entity_header *)Commands->Data)[Commands->NumHeader];
		Header->NumEntities = 0;
	}
	else
	{
		render_entity_header *LastHeader = &((render_entity_header *)Commands->Data)[Commands->NumHeader-1];

		if (LastHeader->Type == Type)
		{
			Header = LastHeader;
			NewHeader = 0;
		}
		else
		{
			Header = LastHeader + 1;
			Header->NumEntities = 0;
		}

		LastDataStart = LastHeader->Start;
	}

	void *Result = NULL;

	u32 DataStart;
	if (LastDataStart < Size)
	{
		DataStart = 0;
	}
	else
	{
		DataStart = PREVALIGNED(LastDataStart - Size, Alignment);
	}

	if ((Commands->NumHeader + NewHeader) * sizeof(render_entity_header) < DataStart)
	{
		YASSERT(DataStart);
		Commands->NumHeader += NewHeader;
		Header->Type = Type;
		Header->Start = DataStart;
		Header->NumEntities += 1;
		Result = (void *) &Commands->Data[DataStart];
	}

	return Result;
}

PRIVATE void *
RenderAlloc(render_group *Group, render_entity_type Type)
{
	void *Result = NULL;

	u32 Size = RenderEntitySize[Type];
	u16 Alignment = RenderEntityAlignment[Type];

	return RenderAlloc2(Group, Type, Size, Alignment);
}

#define STRETCH(X) BasisStretch((X), Group->Basis)
#define TRANSFORM(X) BasisTransform((X), Group->Basis)

render_entity_rectangle *
PushRect(render_group *Group, v2 Origin, v2 Dimensions, v4 Color)
{
	render_entity_rectangle *Result =
		(render_entity_rectangle *) RenderAlloc(Group, RenderEntityType_RECTANGLE);

	if (Result)
	{
		v3 ZOrigin = V(Origin.X, Origin.Y, Group->Level);
		Result->Origin     = TRANSFORM(ZOrigin);
		Result->Dimensions = STRETCH(Dimensions);
		Result->Color      = Color;
	}

        return Result;
}

render_entity_line *
PushLine(render_group *Group, v2 Origin, v2 End, f32 Thickness, v4 Color)
{
	render_entity_line *Result =
		(render_entity_line *) RenderAlloc(Group, RenderEntityType_LINE);

	if (Result)
	{
		v3 ZOrigin = V(Origin.X, Origin.Y, Group->Level);
		v3 ZEnd = V(End.X, End.Y, Group->Level);
		Result->Origin     = TRANSFORM(ZOrigin);
		Result->End        = TRANSFORM(ZEnd);
		Result->Color      = Color;
		Result->Thickness  = Thickness;
	}


        return Result;
}

void
PushRectOutline(render_group *Group, rect Rect, f32 Thickness, v4 Color)
{
	PushLine(Group, V(Rect.MinX-Thickness, Rect.MinY), V(Rect.MaxX+Thickness, Rect.MinY), Thickness, Color);
	PushLine(Group, V(Rect.MaxX, Rect.MinY), V(Rect.MaxX, Rect.MaxY), Thickness, Color);
	PushLine(Group, V(Rect.MaxX+Thickness, Rect.MaxY), V(Rect.MinX-Thickness, Rect.MaxY), Thickness, Color);
	PushLine(Group, V(Rect.MinX, Rect.MaxY), V(Rect.MinX, Rect.MinY), Thickness, Color);
}

render_entity_texture *
PushSprite(render_group *Group, v2 Origin, v2 Dimensions, sprite_id SpriteId, v4 Color)
{
	loaded_texture Tex = RegisterForFrame(Group->Backend, Group->Assets, SpriteId);

	texture Texture;
	YASSERT(Tex.Id <= MAXOF(Texture.ID));
	Texture.ID         = Tex.Id;
	Texture.IsMono     = 0;
	Texture.Origin     = V(0, 0);
	Texture.Dimensions = V(1, 1);

	render_entity_texture *Result = PushTexture(Group, Origin, Dimensions, Texture, Color);

	return Result;
}

render_entity_texture *
PushTexture(render_group *Group, v2 Origin, v2 Dimensions, texture Texture, v4 Color)
{
	render_entity_texture *Result =
		(render_entity_texture *) RenderAlloc(Group, RenderEntityType_TEXTURE);

	if (Result)
	{
		v3 ZOrigin = V(Origin.X, Origin.Y, Group->Level);
		Result->Origin     = TRANSFORM(ZOrigin);
		Result->Dimensions = STRETCH(Dimensions);
		Result->Color      = Color;
		Result->Texture    = Texture;
	}

	return Result;
}

render_group
CreateRenderGroup(opengl_backend *Backend, render_commands *Commands,
		  asset_store *Assets, basis_system Basis)
{
	YASSERT(Backend);
	YASSERT(Commands);
	YASSERT(Assets);

	render_group Group;

	Group.Backend  = Backend;
	Group.Commands = Commands;
	Group.Assets   = Assets;
	Group.Basis    = Basis;
	Group.Level    = F32C(0.0);

	return Group;
}

void
ResetCommands(render_commands *Commands)
{
	Commands->NumHeader = 0;
}

render_group
SubRenderGroup(render_group *Base, basis_system Basis)
{
	render_group Result = *Base;

	basis_system Transformed;
	Transformed.XAxis  = BasisStretch(Basis.XAxis, Base->Basis);
	Transformed.YAxis  = BasisStretch(Basis.YAxis, Base->Basis);
	Transformed.Origin = BasisTransform(Basis.Origin, Base->Basis);
	Result.Basis = Transformed;

	return Result;
}

render_commands
CreateRenderCommands(udx Size, byte Buffer[static Size])
{
	render_commands Commands;

	Commands.Capacity   = (i32) Size;
	Commands.NumHeader  = 0;
	Commands.Data       = Buffer;

	return Commands;
}

render_entity_hexagon *
PushHexagon(render_group *Group, v2 Center, f32 Radius, v4 Color)
{
#if 0
	render_entity_hexagon *Result =
		(render_entity_hexagon *) RenderAlloc(Group, RenderEntityType_HEXAGON);

	if (Result)
	{
		TODO(this has to be done differently);
		Result->Center = TRANSFORM(Center);
		Result->Radius = Radius;
		Result->Color  = Color;
	}

	return Result;
#else
	v2 const Points[6] =
		{
			Add(Center, Scale(Radius, V(-1, 0))),
			Add(Center, Scale(Radius, V(-0.5, MC_S3H))),
			Add(Center, Scale(Radius, V(-0.5, -MC_S3H))),
			Add(Center, Scale(Radius, V(0.5, MC_S3H))),
			Add(Center, Scale(Radius, V(0.5, -MC_S3H))),
			Add(Center, Scale(Radius, V(1, 0))),
		};
	PushTriangle(Group, Points, Color);
	PushTriangle(Group, Points+1, Color);
	PushTriangle(Group, Points+2, Color);
	PushTriangle(Group, Points+3, Color);
	return NULL;
#endif
}

render_entity_triangle *
PushTriangle(render_group *Group, v2 const Points[3], v4 Color)
{
	render_entity_triangle *Result =
		(render_entity_triangle *) RenderAlloc(Group, RenderEntityType_TRIANGLE);

	if (Result)
	{
		Result->Points[0] = TRANSFORM(V(Points[0].X, Points[0].Y, Group->Level));
		Result->Points[1] = TRANSFORM(V(Points[1].X, Points[1].Y, Group->Level));
		Result->Points[2] = TRANSFORM(V(Points[2].X, Points[2].Y, Group->Level));
		Result->Color  = Color;
	}

	return Result;
}

#undef TRANSFORM
#undef STRETCH
