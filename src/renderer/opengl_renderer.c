#include <renderer/renderer.h>
#include <opengl/opengl.h>
#include <opengl/renderer.h>

PRIVATE void
CreateMatrix(f32 Storage[4][4], basis_system Screen)
{
	v4 Origin = V(Screen.Origin.X, Screen.Origin.Y, 0, 1);
	v4 XAxis  = V(Screen.XAxis.X, Screen.XAxis.Y, 0, 0);
	v4 YAxis  = V(Screen.YAxis.X, Screen.YAxis.Y, 0, 0);
	v4 ZAxis  = V(0, 0, 1, 0);
	/* The Matrix should look like this:
	 * |       |       | 0 |        |
	 * | XAxis | YAxis | 0 | Origin |
	 * |       |       | 1 |        |
	 * |       |       | 0 |        |
	 */

	idx Column = 0;
	for (idx Row = 0;
	     Row < 4;
	     ++Row)
	{
		Storage[Row][Column] = XAxis.AsArray[Row];
	}
	Column += 1;
	for (idx Row = 0;
	     Row < 4;
	     ++Row)
	{
		Storage[Row][Column] = YAxis.AsArray[Row];
	}
	Column += 1;
	for (idx Row = 0;
	     Row < 4;
	     ++Row)
	{
		Storage[Row][Column] = ZAxis.AsArray[Row];
	}
	Column += 1;
	for (idx Row = 0;
	     Row < 4;
	     ++Row)
	{
		Storage[Row][Column] = Origin.AsArray[Row];
	}
        YASSERT(Column == 3);
}

static v4
PremultiplyColorAlpha(v4 Color)
{
	v4 Premult;
	Premult.R = Color.R * Color.A;
	Premult.G = Color.G * Color.A;
	Premult.B = Color.B * Color.A;
	Premult.A = Color.A;
	return Premult;
}

PRIVATE void
SubmitRectangles(renderer *Renderer,
		 basis_system Screen,
		 u32 NumRects,
		 render_entity_rectangle Rects[static NumRects])
{
#if 0
	for (u32 Entity = NumRects;
	     (Entity--) != 0;)
	{
		render_entity_rectangle *Rect = &Rects[Entity];

		v2 ScreenLB = Rect->Origin;

		v2 ScreenLT = Add(Rect->Origin,
				  V(0, Rect->Dimensions.Y));
		v2 ScreenRB = Add(Rect->Origin,
				  V(Rect->Dimensions.X, 0));
		v2 ScreenRT = Add(Rect->Origin,
				  Rect->Dimensions);

		vertex_data Vertices[4] = {
			{V(ScreenLB.X, ScreenLB.Y, 0.0),
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Rect->Color)},
			{V(ScreenRB.X, ScreenRB.Y, 0.0),
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Rect->Color)},
			{V(ScreenLT.X, ScreenLT.Y, 0.0),
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Rect->Color)},
			{V(ScreenRT.X, ScreenRT.Y, 0.0),
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Rect->Color)},
		};

		DrawTriangleStrip(
			Renderer,
			4, Vertices);
	}

#else
	v2 TexturePos = V(0.0, 0.0);
	v3 Transform = V(0.0, 0.0, 1.0);

	triangle_alloc Alloc = AllocTriangles(Renderer, NumRects * 2);
	for (u32 CurrentRect = 0;
	     CurrentRect < NumRects;)
	{
		u32 NumFullRects = Alloc.Count >> 1;

		u32 EndRect = CurrentRect + NumFullRects;

		for (;
		     CurrentRect < EndRect;
		     ++CurrentRect)
		{
			render_entity_rectangle *Rect = &Rects[NumRects - 1 - CurrentRect];

			v3 ScreenLB = Rect->Origin;

			v3 ScreenLT = Add(Rect->Origin,
					  V(0, Rect->Dimensions.Y, 0));
			v3 ScreenRB = Add(Rect->Origin,
					  V(Rect->Dimensions.X, 0, 0));
			v3 ScreenRT = Add(Rect->Origin,
					  V(Rect->Dimensions.X, Rect->Dimensions.Y, 0));

			v4 Color = PremultiplyColorAlpha(Rect->Color);

			*Alloc.Positions = ScreenLB;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = ScreenRB;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = ScreenLT;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);

			*Alloc.Positions = ScreenRB;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = ScreenLT;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = ScreenRT;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);

			for (idx I = 0;
			     I < 6;
			     ++I)
			{
				*Alloc.Transforms = Transform;
				Alloc.Transforms = PTROFFSET(Alloc.Transforms, Alloc.TransformStride);
			}

			for (idx I = 0;
			     I < 6;
			     ++I)
			{
				*Alloc.Colors = Color;
				Alloc.Colors = PTROFFSET(Alloc.Colors, Alloc.ColorStride);
			}

			for (idx I = 0;
			     I < 6;
			     ++I)
			{
				*Alloc.TexturePos = TexturePos;
				Alloc.TexturePos = PTROFFSET(Alloc.TexturePos, Alloc.TexturePosStride);
			}
		}

		if (Alloc.Count & 1)
		{
			render_entity_rectangle *Rect = &Rects[NumRects - 1 - CurrentRect++];

			v3 ScreenLB = Rect->Origin;

			v3 ScreenLT = Add(Rect->Origin,
					  V(0, Rect->Dimensions.Y, 0));
			v3 ScreenRB = Add(Rect->Origin,
					  V(Rect->Dimensions.X, 0, 0));
			v3 ScreenRT = Add(Rect->Origin,
					  V(Rect->Dimensions.X, Rect->Dimensions.Y, 0));

			v2 TexturePos = V(0.0, 0.0);
			v3 Transform = V(0.0, 0.0, 1.0);
			v4 Color = PremultiplyColorAlpha(Rect->Color);

			*Alloc.Positions = ScreenLB;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = ScreenRB;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = ScreenLT;
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);

			for (idx I = 0;
			     I < 3;
			     ++I)
			{
				*Alloc.Transforms = Transform;
				Alloc.Transforms = PTROFFSET(Alloc.Transforms, Alloc.TransformStride);
			}

			for (idx I = 0;
			     I < 3;
			     ++I)
			{
				*Alloc.Colors = Color;
				Alloc.Colors = PTROFFSET(Alloc.Colors, Alloc.ColorStride);
			}

			for (idx I = 0;
			     I < 3;
			     ++I)
			{
				*Alloc.TexturePos = TexturePos;
				Alloc.TexturePos = PTROFFSET(Alloc.TexturePos, Alloc.TexturePosStride);
			}

			Alloc = AllocTriangles(Renderer, NumRects - CurrentRect + 1);
			YASSERT(Alloc.Count >= 1);

			for (idx I = 0;
			     I < 3;
			     ++I)
			{
				*Alloc.Transforms = Transform;
				Alloc.Transforms = PTROFFSET(Alloc.Transforms, Alloc.TransformStride);
			}

			for (idx I = 0;
			     I < 3;
			     ++I)
			{
				*Alloc.Colors = Color;
				Alloc.Colors = PTROFFSET(Alloc.Colors, Alloc.ColorStride);
			}

			for (idx I = 0;
			     I < 3;
			     ++I)
			{
				*Alloc.TexturePos = TexturePos;
				Alloc.TexturePos = PTROFFSET(Alloc.TexturePos, Alloc.TexturePosStride);
			}

			*Alloc.Positions = V(ScreenRB.X, ScreenRB.Y, 0.0);
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = V(ScreenLT.X, ScreenLT.Y, 0.0);
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
			*Alloc.Positions = V(ScreenRT.X, ScreenRT.Y, 0.0);
			Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);

		}
		else
		{
			Alloc = AllocTriangles(Renderer, NumRects - CurrentRect);
		}
	}
#endif
}

PRIVATE void
SubmitTriangles(renderer *Renderer,
		basis_system Screen,
		u32 NumTriangles,
		render_entity_triangle Triangles[static NumTriangles])
{
	for (u32 Entity = NumTriangles;
	     (Entity--) != 0;)
	{
		render_entity_triangle *Triangle = &Triangles[Entity];
		vertex_data Vertices[3];
		v4 Color = PremultiplyColorAlpha(Triangle->Color);
		for (i64 I = 0;
		     I < 3;
		     ++I)
		{
			v3 Point = Triangle->Points[I];
			Vertices[I].Position = Point;
			Vertices[I].Transform = V(0, 0, 1);
			Vertices[I].TexturePos = V(0, 0);
			Vertices[I].Color = Color;
		}

		DrawTriangles(Renderer,
			      3, Vertices);
	}
}

PRIVATE void
SubmitLines(renderer *Renderer,
	    basis_system Screen,
	    u32 NumLines,
	    render_entity_line Lines[static NumLines])
{
	for (u32 Entity = NumLines;
	     (Entity--) != 0;)
	{
		render_entity_line *Line = &Lines[Entity];
		v3 Start = Line->Origin;
		v3 End = Line->End;
		f32 Thickness = Line->Thickness;

		v3 Diff = Diff(End, Start);

		v3 Orthogonal = V(-Diff.Y, Diff.X, 0);

		v3 Normal = Scale(1/Length(Orthogonal), Orthogonal);

		v3 ScreenLB = Diff(Start,
				   Scale(Thickness, Normal));

		v3 ScreenLT = Add(Start,
				  Scale(Thickness, Normal));
		v3 ScreenRB = Diff(End,
				   Scale(Thickness, Normal));
		v3 ScreenRT = Add(End,
				  Scale(Thickness, Normal));

		vertex_data Vertices[4] = {
			{ScreenLB,
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Line->Color)},
			{ScreenRB,
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Line->Color)},
			{ScreenLT,
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Line->Color)},
			{ScreenRT,
			 V(0.0, 0.0, 1.0),
			 V(0.0f, 0.0f),
			 PremultiplyColorAlpha(Line->Color)},
		};

		DrawTriangleStrip(
			Renderer,
			4, Vertices);
	}
}

#define Sqrt3Halfs 0.866025403785f

static v3 const HexEdges[6] =
{
	V(-1, 0, 0),
	V(-0.5, Sqrt3Halfs, 0),
	V(-0.5, -Sqrt3Halfs, 0),
	V(0.5, Sqrt3Halfs, 0),
	V(0.5, -Sqrt3Halfs, 0),
	V(1, 0, 0),
};

PRIVATE void
SubmitHexagons(renderer *Renderer,
	       basis_system Screen,
	       u32 NumHexagons,
	       render_entity_hexagon Hexagons[static NumHexagons])
{
#if 1
	for (u32 Entity = NumHexagons;
	     (Entity--) != 0;)
	{
		render_entity_hexagon *Hex = &Hexagons[Entity];

		vertex_data Vertices[6];

		for (i32 I = 0;
		     I < 6;
		     ++I)
		{
			v3 Transformed = Add(Hex->Center, Scale(Hex->Radius, HexEdges[I]));
			Vertices[I].Position = V(Transformed.X, Transformed.Y, 0.0);
			Vertices[I].Transform  = V(0, 0, 1);
			Vertices[I].TexturePos = V(0, 0);
			Vertices[I].Color      = PremultiplyColorAlpha(Hex->Color);
		}

		DrawTriangleStrip(Renderer, 6, Vertices);
	}
#else
	idx TrianglesNeeded = NumHexagons * 4;
	triangle_alloc Alloc = AllocTriangles(Renderer, TrianglesNeeded);
	FormatOut("[FIRST TRIANGLE ALLOC] Wanted: {i64}; Got {u32}\n",
		  TrianglesNeeded, Alloc.Count);

	idx TriangleIndices[][3] =
	{
		{0, 1, 2},
		{1, 4, 2},
		{1, 3, 4},
		{3, 5, 4},
	};

	idx CurrentTriangleIndex = 0;

	v2 TexturePos = V(0.0, 0.0);
	v3 Transform = V(0.0, 0.0, 1.0);

	for (udx CurrentHex = 0;
	     CurrentHex < NumHexagons;
	     ++CurrentHex)
	{
		render_entity_hexagon *Hex = &Hexagons[NumHexagons - 1 - CurrentHex];
		v4 Color = PremultiplyColorAlpha(Hex->Color);
		for (udx CurrentTriangle = 0;
		     CurrentTriangle < 4;
		     ++CurrentTriangle)
		{
			YASSERTF(CurrentTriangleIndex < (idx)Alloc.Count,
				 "%ld < %ld",
				 CurrentTriangleIndex, (idx)Alloc.Count);
			for (udx Vertex = 0;
			     Vertex < 3;
			     ++Vertex)
			{
				v2 VertexPos = HexEdges[TriangleIndices[CurrentTriangle][Vertex]];
				v2 Transformed = Add(Hex->Center, Scale(Hex->Radius, VertexPos));

				*Alloc.Positions = V(Transformed.X, Transformed.Y, 0.0);
				Alloc.Positions  = PTROFFSET(Alloc.Positions, Alloc.PositionStride);
				*Alloc.Transforms = Transform;
				Alloc.Transforms = PTROFFSET(Alloc.Transforms, Alloc.TransformStride);
				*Alloc.Colors = Color;
				Alloc.Colors = PTROFFSET(Alloc.Colors, Alloc.ColorStride);
				*Alloc.TexturePos = TexturePos;
				Alloc.TexturePos = PTROFFSET(Alloc.TexturePos, Alloc.TexturePosStride);
			}

			if (Alloc.Count == ++CurrentTriangleIndex)
			{
				YASSERTF(CurrentTriangleIndex <= TrianglesNeeded,
					 "%ld <= %ld",
					 CurrentTriangleIndex,
					 TrianglesNeeded);
				TrianglesNeeded -= CurrentTriangleIndex;
				CurrentTriangleIndex = 0;
				if (TrianglesNeeded != 0)
				{
					Alloc = AllocTriangles(Renderer, TrianglesNeeded);
					FormatOut("[TRIANGLE ALLOC] Wanted: {i64}; Got {u32}\n",
						  TrianglesNeeded, Alloc.Count);
				}
			}
		}
	}
#endif
}

PRIVATE void
SubmitTextures(renderer *Renderer,
	       basis_system Screen,
	       u32 NumTextures,
	       render_entity_texture Textures[static NumTextures])
{
	for (u32 Entity = NumTextures;
	     (Entity--) != 0;)
	{
		render_entity_texture *Texture = &Textures[Entity];

		v3 ScreenLB = Texture->Origin;

		v3 ScreenLT = Add(Texture->Origin,
				  V(0, Texture->Dimensions.Y, 0));
		v3 ScreenRB = Add(Texture->Origin,
				  V(Texture->Dimensions.X, 0, 0));
		v3 ScreenRT = Add(Texture->Origin,
				  V(Texture->Dimensions.X, Texture->Dimensions.Y, 0));

		v2 TextureLB = Texture->Texture.Origin;
		v2 TextureLT = Add(Texture->Texture.Origin,
				   V(0, Texture->Texture.Dimensions.Y));
		v2 TextureRB = Add(Texture->Texture.Origin,
				   V(Texture->Texture.Dimensions.X, 0));
		v2 TextureRT = Add(Texture->Texture.Origin,
				   Texture->Texture.Dimensions);

		vertex_data Vertices[4] = {
			{ScreenLB,
			 V(0.0, 0.0, 1.0),
			 TextureLB,
			 PremultiplyColorAlpha(Texture->Color)},
			{ScreenRB,
			 V(0.0, 0.0, 1.0),
			 TextureRB,
			 PremultiplyColorAlpha(Texture->Color)},
			{ScreenLT,
			 V(0.0, 0.0, 1.0),
			 TextureLT,
			 PremultiplyColorAlpha(Texture->Color)},
			{ScreenRT,
			 V(0.0, 0.0, 1.0),
			 TextureRT,
			 PremultiplyColorAlpha(Texture->Color)},
		};

		if (Texture->Texture.IsMono)
		{
			BindMonoTexture(Renderer, Texture->Texture.ID);
		} else {
			BindTexture(Renderer, Texture->Texture.ID);
		}


		GL_QUICK_CHECK();

		DrawTriangleStrip(
			Renderer,
			4, Vertices);

		UnbindTexture(Renderer);
	}
}

PRIVATE void
OpenGLDrawRenderGroup_(renderer *Renderer, basis_system Screen, render_commands *Commands)
{
	for (render_entity_header *Header = (render_entity_header *) Commands->Data;
	     Header != &((render_entity_header *) Commands->Data)[Commands->NumHeader];
	     ++Header)
	{
		switch(Header->Type)
		{
			break;case RenderEntityType_RECTANGLE:
			{
				render_entity_rectangle *Rects = (render_entity_rectangle *)
					&Commands->Data[Header->Start];
				INSTRUMENT_COUNTED(DrawRect, Header->NumEntities)
				{

					SubmitRectangles(Renderer, Screen,
							 Header->NumEntities, Rects);
				}
			}
			break;case RenderEntityType_TRIANGLE:
			{
				render_entity_triangle *Triangles = (render_entity_triangle *) &Commands->Data[Header->Start];

				INSTRUMENT_COUNTED(DrawTriangle, Header->NumEntities)
				{
					SubmitTriangles(Renderer, Screen,
							Header->NumEntities, Triangles);
				}


			}
			break;case RenderEntityType_LINE:
			{
				render_entity_line *Lines = (render_entity_line *) &Commands->Data[Header->Start];

				INSTRUMENT_COUNTED(DrawLine, Header->NumEntities)
				{
					SubmitLines(Renderer, Screen,
						    Header->NumEntities, Lines);

				}
			}
			break;case RenderEntityType_HEXAGON:
			{
				render_entity_hexagon *Hexagons = (render_entity_hexagon *) &Commands->Data[Header->Start];
				INSTRUMENT_COUNTED(DrawHexagon, Header->NumEntities)
				{
					SubmitHexagons(Renderer, Screen,
						       Header->NumEntities, Hexagons);
				}
			}
			break;case RenderEntityType_TEXTURE:
			{
				render_entity_texture *Textures = (render_entity_texture *) &Commands->Data[Header->Start];

				INSTRUMENT_COUNTED(DrawTexture, Header->NumEntities)
				{
					SubmitTextures(Renderer, Screen,
						       Header->NumEntities, Textures);
				}
			}
			break;case RenderEntityType_COUNT: INVALIDCODEPATH();
			INVALIDDEFAULT();
		};
	}
}

void
OpenGLDrawRenderGroup(renderer *Renderer, memory_arena *Scratch,
		      basis_system Screen, render_commands *Commands)
{
	f32 Projection[4][4];
	CreateMatrix(Projection, Screen);

	RendererPrepare(Renderer, Scratch, Projection);

	basis_system OpenGLSystem =
		{
			.Origin = V(0, 0, 0),
			.XAxis  = V(1, 0),
			.YAxis  = V(0, 1),
		};

	OpenGLDrawRenderGroup_(Renderer, OpenGLSystem, Commands);

	RendererFinish(Renderer);
}
