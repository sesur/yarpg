#include <debug/debug_state.h>
#include <debug/debug_state_internal.h>

PRIVATE event_storage
EventStorage_FromBuffer
(
	idx EventCapacity,
	debug_event Storage[static EventCapacity]
)
{
	event_storage Events;
	Events.Capacity = EventCapacity;
	Events.End      = 0;
	Events.Storage  = Storage;
	Events.Start    = CurrentTimeStamp();
	return Events;
}

PRIVATE thread_debug_state
ThreadDebugState_FromBuffer
(
	idx EventCapacity,
	debug_event Storage[static EventCapacity]
)
{
	thread_debug_state State;
	State.InUse = 0;

	State.Events = EventStorage_FromBuffer(EventCapacity, Storage);
	return State;
}

PRIVATE bool
AllocPerfEvent
(
	event_storage *Store,
	debug_location const *Loc,
	u32 DeltaTime,
	u32 DeltaCycle,
	u32 HitCount,
	u16 Type
)
{
	bool Success;
	if (Store->End < Store->Capacity)
	{
		debug_event *Event = &Store->Storage[Store->End++];
		Event->Perf.Loc        = Loc;
		Event->Perf.DeltaClock = DeltaTime;
		Event->Perf.DeltaCycle = DeltaCycle;
		Event->Perf.HitCount   = HitCount;
		Event->Perf.Type       = Type;
		Success = 1;
	}
	else
	{
		Success = 0;
	}

	return Success;
}

bool
PushPerfEvent
(
	u32 HitCount,
	debug_location const *Loc,
	perf_event_type Type
)
{
	thread_context *ThisThread = CurrentThreadContext();

	perf_ts Stamp = CurrentTimeStamp();

	bool Success;

	thread_debug_state *DebugState;
	WITHDEBUGSTATE(DebugState, ThisThread)
	{
		event_storage *Events = &DebugState->Events;
		i64 DeltaT = Stamp.Clock  - Events->Start.Clock;
		i64 DeltaC = Stamp.Cycles - Events->Start.Cycles;

		if (DeltaT >= MINOF(MEMBER(stored_perf_event, DeltaClock))
		    && DeltaT <= MAXOF(MEMBER(stored_perf_event, DeltaClock))
		    && DeltaC >= MINOF(MEMBER(stored_perf_event, DeltaCycle))
		    && DeltaC <= MAXOF(MEMBER(stored_perf_event, DeltaCycle)))
		{
			Success = AllocPerfEvent(Events, Loc,
						 DeltaT, DeltaC,
						 HitCount, Type);
		}
		else
		{
			Success = 0;
		}
	}

	return Success;
}

void
ReleaseDebugState
(
	thread_context *Ctx,
	thread_debug_state *State
)
{
	thread_debug_state *State2 = &Ctx->DebugState;
	YASSERT(State2 == State);
	YASSERT(InterlockedExchange(&State->InUse, 0) == 1);
}

thread_debug_state *
AcquireDebugState
(
	thread_context *Ctx
)
{
	thread_debug_state *State = &Ctx->DebugState;
	while (InterlockedExchange(&State->InUse, 1) != 0);
	return State;
}
