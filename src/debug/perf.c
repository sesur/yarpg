#include <debug/perf.h>


//	#define CLOCK_ID CLOCK_THREAD_CPUTIME_ID;
//	#define CLOCK_ID CLOCK_PROCESS_CPUTIME_ID;
//	#define CLOCK_ID CLOCK_MONOTONIC;
#define CLOCK_ID CLOCK_MONOTONIC_RAW

perf_ts CurrentTimeStamp
(
	void
)
{
	struct timespec Spec;

	u64 Cycles, Clock;
	u32 CoreId;

	// maybe its better to use getrusage somehow ?

	YSASSERT(sizeof(unsigned) == sizeof(CoreId));
	Cycles = __rdtscp(&CoreId);

	// this should ensure that we wait for rdtscp to finish before moving on
	__builtin_ia32_lfence();

	clock_gettime(CLOCK_ID, &Spec);
	Clock = (u64)Spec.tv_sec * U64C(1000000000)
		+ (u64)Spec.tv_nsec;

        perf_ts Result = { .Clock  = Clock,
		           .Cycles = Cycles,
		           .CoreId = CoreId };

	return Result;
}

#undef CLOCK_ID
