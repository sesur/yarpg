#ifndef GUARD_INTRINSICS_H
#define GUARD_INTRINSICS_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <x86intrin.h>

PRIVATE inline i64 __attribute__ ((always_inline))
_mm256_varextract_epi64(__m256i Val, idx Idx)
{
	i64 Result = 0;
	/* switch (Idx) */
	/* { */
	/* 	break;case 0: */
	/* 	{ */
	/* 		Result = _mm256_extract_epi64((Val), 0); */
	/* 	} */
	/* 	break;case 1: */
	/* 	{ */
	/* 		Result = _mm256_extract_epi64((Val), 1); */
	/* 	} */
	/* 	break;case 2: */
	/* 	{ */
	/* 		Result = _mm256_extract_epi64((Val), 2); */
	/* 	} */
	/* 	break;case 3: */
	/* 	{ */
	/* 		Result = _mm256_extract_epi64((Val), 3); */
	/* 	} */
	/* 	INVALIDDEFAULT(); */
	/* } */
#define MAXIDX (sizeof(Val) / sizeof(i64))
	YASSERT(0 <= Idx && Idx < (idx)MAXIDX);
	i64 Data[MAXIDX] __attribute__((aligned (32)));
	_mm256_storeu_si256((__m256i *) Data, Val);
	Result = Data[Idx];
	return Result;
#undef MAXIDX
}

#endif /* GUARD_INTRINSICS_H */
