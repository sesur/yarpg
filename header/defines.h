#ifndef GUARD_DEFINES_H
#define GUARD_DEFINES_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

// world_types.h
// this is assumed to always be a power of 2
#define MAX_LOADED_CHUNKS 128

// game_internal.h
#define RENDER_GROUP_SIZE (10 MB)
#define SCRATCH_ARENA_SIZE 20 MB

// platform.h
// numbers are made up at the moment
#define MINIMUM_TRANSIENT_SIZE (5 MB)
#define MINIMUM_PERSISTENT_SIZE (10 MB)

#define PERSISTENT_MEMORY_SIZE (120 MB)
#define TRANSIENT_MEMORY_SIZE (80 MB)

// TODO: only do debug stuff in DEBUG is defined
#define DEBUG_MEMORY_SIZE (1 GB)

// unit.h
#define NUM_EQUIPMENT 4
#define NUM_RACES 3

// string.h
#define GLYPH_TBL_DIM  16
#define GLYPH_TBL_SIZE (GLYPH_TBL_DIM * GLYPH_TBL_DIM)

// world.c
#define MAXARMYSOLDIERS 20
#define ARMIES_PER_NEW_CHUNK 3

// asset_builder.c
#define ASSET_DATA_SIZE (5 MB)
#define ASSET_SCRATCH_SIZE (1 MB)

// debug/perf.h
#define NUM_PERF_STORAGES 2
#define MAX_PERF_EVENTS (4 * U16_MAX)

// util/delaunay.c
#define DELAUNAY_ASYNC_THRESHOLD 512
#define QUICKSORT_ASYNC_THRESHOLD 1024
#define LLOYD_ASYNC_STEP_SIZE 2048

// debug/debug_info.c
#define DEFAULT_TIMELINE_LENGTH (5 MIN)

#endif /* GUARD_DEFINES_H */
