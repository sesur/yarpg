#ifndef GUARD_YAFF_FORMAT_H
#define GUARD_YAFF_FORMAT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(yaff_asset_info);
FWDDECLARE(yaff_category_info);
FWDDECLARE(yaff_asset_tag);
FWDDECLARE(yaff_header);
FWDDECLARE(yaff_directory);
FWDDECLARE(yaff_text);
FWDDECLARE(yaff_sprite);
FWDDECLARE(yaff_sound);

typedef enum
{
	AssetType_SPRITE,
	AssetType_TEXT,
	AssetType_SOUND,
	AssetType_COUNT,
} asset_type;

static char const *AssetTypeName[] = {
	[AssetType_SPRITE] = "Sprite",
	[AssetType_TEXT]   = "Text",
	[AssetType_SOUND]  = "Sound",
};

YSASSERT(ARRAYCOUNT(AssetTypeName) == AssetType_COUNT);

typedef enum
{
	SpriteTag_ID,
	SpriteTag_GRAS,
	SpriteTag_LAVA,
	SpriteTag_WATER,
	SpriteTag_STONE,
	SpriteTag_SAND,
	SpriteTag_MOISTURE,

	SpriteTag_COUNT,
} sprite_tag;

typedef enum
{
	SoundTag_ID,
	SoundTag_COUNT,
} sound_tag;

typedef enum
{
	TextTag_COUNT,
} text_tag;

struct yaff_category_info
{
	range Assets[AssetType_COUNT];
};

struct yaff_asset_info
{
	range DataRange; // 16

	/* this could be two u32 */
	range Tags;      // 16
};

struct yaff_sprite
{
	u32 Width, Height;
	/* Coordinates of Anchor are between -1 and 1 */
	/* just some point inside the texture */
	/* interpretation up to the user */
	v2 Anchor;
};

struct yaff_sound
{
	b32 HasNext;
	i32 NumChannels;

	i32 SampleRate;
	i32 NumSamples;
};

struct yaff_text
{
	u32 Length, NumChars;
};

struct yaff_asset_tag
{
	i32 Id;
	f32 Value;
};

struct yaff_header
{
	u32 MagicNumber; // always yaff
	u32 Version;

	u64 FileSize;
	u64 DirectoryStart;
};

/* A yaff file consists of four segments: A header segment, a data segment,
 * an asset info segment and a directory segment; in the following order:
 *
 * Segments:
 *     Start
 * /------------\
 * | Header     |
 * +------------+ <- may contain padding
 * | Data       |
 * +------------+ <- may contain padding
 * | Meta       |
 * +------------+ <- may contain padding
 * | Directory  |
 * \------------/
 *     End
 *
 * -- header segment
 * The header segment is used to identify a yaff file and gives the offset
 * to the start of the directory segment.
 *
 * -- directory segment
 * The directory segment includes the sizes and offsets of the asset info
 * and data segment. It also includes the number of assets of each type,
 * the number of tags used and the number of asset kinds represented in this
 * file.
 *
 * -- data segment
 * The data segment includes all the data for each of the assets in no
 * particular order.
 *
 * -- meta segment
 * The meta segment is split into three parts. First comes an array of asset_tags,
 * then comes the infos of each asset included in this file and at the end
 * we have the infos for all asset kinds included in this file.
 * Where these informations start/end is described in the directory.
 */

struct yaff_directory
{
	u32 MagicNumber; // always ydir

	u32 NumCategories;
	u32 NumAssets;
	u32 NumTags;

	range AssetIdRangeByType[AssetType_COUNT];
	range CategoryIdRange;
	range CategoryInfoRange;
	range AssetInfoRange;
	range TagsRange;

	range TypedAssetInfoRange[AssetType_COUNT];

	u64 DataStart;
};

#define MAGIC_NUMBER(a, b, c, d) (((u32)(a) << 0)		\
				  | (((u32) (b)) << 8)		\
				  | (((u32) (c)) << 16)		\
				  | (((u32) (d)) << 24))

#define YAFF_MAGIC_NUMBER     MAGIC_NUMBER('y', 'a', 'f', 'f')
#define YAFF_VERSION          MAGIC_NUMBER(0, 1, 0, 'g')
#define YAFF_DIR_MAGIC_NUMBER MAGIC_NUMBER('y', 'd', 'i', 'r')

//#undef MAGIC_NUMBER

#endif /* GUARD_YAFF_FORMAT_H */
