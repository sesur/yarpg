#ifndef GUARD_DEBUG_H
#define GUARD_DEBUG_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#if defined(DEBUG)

#include <signal.h>

#ifdef SIGTRAP
#define DEBUG_BREAK() do { raise(SIGTRAP); __builtin_unreachable(); } while (0)
#else
#warning SIGTRAP not defined
#define DEBUG_BREAK() do { raise(SIGINT); __builtin_unreachable(); } while (0)
#endif


// STRING is defined in def.h
#include <stdio.h>

#define ASSERTION_STR "%s:%d (%s): Assertion '%s' failed."
#define YASSERT_1(Expr) do {if (!(Expr)) {				\
			fprintf(stderr, ASSERTION_STR "\n",		\
				__FILE__, __LINE__,			\
				__func__, STRING(Expr));		\
			DEBUG_BREAK();					\
		}} while (0)
#define YASSERT_2(Expr, Msg) do {if (!(Expr)) {				\
			fprintf(stderr, ASSERTION_STR " (%s)\n",	\
				__FILE__, __LINE__,			\
				__func__, STRING(Expr),			\
				(Msg));					\
			DEBUG_BREAK();					\
		}} while (0)
#define YASSERT_DISPATCH(_1, _2, Name, ...) Name
#define YASSERT(Args...) YASSERT_DISPATCH(Args, YASSERT_2, YASSERT_1)(Args)
#define YASSERTF(Expr, Msg, ...) do {if (!(Expr)) {			\
			fprintf(stderr, ASSERTION_STR " (" Msg ")\n",	\
				__FILE__, __LINE__,			\
				__func__, STRING(Expr),			\
				__VA_ARGS__);				\
			DEBUG_BREAK();					\
		}} while (0)

#define INVALIDCODEPATH() YASSERT(!"Invalid code path")
#else /* DEBUG */

#define YASSERT(Expr, ...) if (!(Expr)) { __builtin_unreachable(); }
#define YASSERTF(Expr, ...) if (!(Expr)) { __builtin_unreachable(); }

#define INVALIDCODEPATH() __builtin_unreachable()

#endif /* DEBUG */

#define static_assert(Args...) _Static_assert(Args)
#define YSASSERT_1(Expr) static_assert(Expr)
#define YSASSERT_2(Expr, Msg) static_assert((Expr), Msg)
#define YSASSERT_DISPATCH(_1, _2, Name, ...) Name
#define YSASSERT(Args...) YSASSERT_DISPATCH(Args, YSASSERT_2, YSASSERT_1)(Args)

#define INVALIDDEFAULT() break;default: INVALIDCODEPATH()
#define INVALIDCASE(Case) break;case Case: { INVALIDCODEPATH(); }

#endif /* GUARD_DEBUG_H */

#if 0
// Failed experiment (for now)
#define CONVOF(Expr, Between) _Generic((Exrp),				\
				       i32: "%d" Between "%d",		\
				       i64: "%ld" Between "%ld",	\
				       u32: "%u" Between "%u",		\
				       u64: "%ld" Between "%ld",	\
				       f32: "%f" Between "%f",		\
				       f64: "%lf" Between "%lf",	\
				       idx: "%ld" Between "%ld",	\
				       udx: "%ld" Between "%ld",	\
				       memory_index: "%lu" Between "%lu")

#define YASSERTEQ(Left, Right) YASSERTF((Left) == (Right),	\
					CONVOF(Left, " == "),	\
					(Left), (Right))
#define YASSERTNEQ(Left, Right) YASSERTF((Left) != (Right),	\
					 CONVOF(Left, " != "),	\
					 (Left), (Right))

#define YASSERTL(Left, Right) YASSERTF((Left) < (Right),	\
				       CONVOF(Left, " < " ),	\
				       (Left), (Right))

#define YASSERTG(Left, Right) YASSERTF((Left) > (Right),	\
				       CONVOF(Left, " > "),	\
				       (Left), (Right))

#define YASSERTLE(Left, Right) YASSERTF((Left) <= (Right),	\
					CONVOF(Left, " <= "),	\
					(Left), (Right))

#define YASSERTGE(Left, Right) YASSERTF((Left) >= (Right),	\
					CONVOF(Left, " >= "),	\
					(Left), (Right))

#define YASSERTP(Pred, X) YASSERTF((Pred)(X), CONVOF(X), X)
#define YASSERTNP(Pred, X) YASSERTF(!(Pred)(X), CONVOF(X), X)

#endif
