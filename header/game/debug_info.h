#ifndef GUARD_GAME_DEBUG_INFO_H
#define GUARD_GAME_DEBUG_INFO_H

#include <debug/debug_state.h>
#include <util/id_map.h>

#define NUM_DEBUG_LOCATIONS 512

FWDDECLARE(debug_info);
FWDDECLARE(debug_timing_block);
FWDDECLARE(debug_thread);
FWDDECLARE(debug_info);
FWDDECLARE(debug_timeline_view);
FWDDECLARE(debug_timeline);
FWDDECLARE(debug_counter);

struct debug_timing_block
{
	perf_ts Start, End;

	debug_timing_block *Parent;
	debug_timing_block *Prev, *Next;

	u32 HitCount;
	debug_location const *Loc;
};

struct debug_thread
{
	debug_timing_block Sentinel;
	debug_timing_block *Open;
};

struct debug_timeline_view
{
	bool ShowTimeline;

	// timeline view
	range Focus;
	debug_location const *Zoomed;

	// function view
	bool SortByCycles;
	bool ShowComplete;
};

struct debug_timeline
{
	perf_ts Start, End;
	range   CapturedTime;
	debug_thread *Threads;

	debug_timeline_view View;

	debug_timeline *Prev, *Next;
};

struct debug_counter
{
	u64 HitCount;
	u64 ActualHitCount;
	u64 Clock;
	u64 Cycles;
};

struct debug_info
{
	memory_arena *Arena;
	/* debug_location Locations[NUM_DEBUG_LOCATIONS]; */
	idx NumIds;
	id_map LocToId;

	idx NumThreads;
	debug_thread *Threads;

	debug_timeline TimelineSentinel;
	debug_timeline FreeTimelineSentinel;

	debug_timing_block *FreeBlock;

	debug_timeline *VisibleTimeline;
};

GAMEFN debug_info *DebugInfo_OfArena(memory_arena *Debug, platform_info *Info);
GAMEFN void AnalyzePerfEvents(debug_info *Debug, idx ThreadId, event_storage Events);
GAMEFN bool IsBlockOpen(debug_timing_block *Block);
GAMEFN void CreateSnapshot(debug_info *Info, range Time);
GAMEFN void Debug_DeleteTimeline(debug_info *Info, debug_timeline *Timeline);

#endif /* GUARD_GAME_DEBUG_INFO_H */
