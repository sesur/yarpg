#ifndef GUARD_GAME_THREAD_H
#define GUARD_GAME_THREAD_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <thread.h>

PLATFORMFN b32
PushLowPriorityTask(taskfn Fun, void *Arg);

PLATFORMFN b32
PushHighPriorityTask(taskfn Fun, void *Arg);

#endif
