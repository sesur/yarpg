#ifndef GUARD_GAME_WORLD_TYPES_H
#define GUARD_GAME_WORLD_TYPES_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(world_position);
FWDDECLARE(world_entity);
FWDDECLARE(active_world_entity);
FWDDECLARE(world_chunk);
FWDDECLARE(world_entity_list);
FWDDECLARE(world);
FWDDECLARE(movement);
FWDDECLARE(transient_world_state);
FWDDECLARE(background_image);
FWDDECLARE(create_chunk_background_task_input);
FWDDECLARE(world_input);
FWDDECLARE(city_data);
FWDDECLARE(army_data);
FWDDECLARE(world_input_state);
FWDDECLARE(world_map_dragging_data);
FWDDECLARE(world_map_moving_data);
FWDDECLARE(active_world_index);
FWDDECLARE(stored_world_index);

// The world is split in chunks and loaded lazily
// it has 3 dimensions

YSASSERT(ISPOWOFTWO(MAX_LOADED_CHUNKS),
	 "MAX_LOADED_CHUNKS has to be a power of two.");

#define CHUNK_DIM 9
#define ENTITIES_PER_BLOCK 512
#define MAX_ACTIVE_ENTITIES 4096
#define MAX_LOADED_ENTITIES (8 * 4096)

////////////////////////////////////////////////////////////////////////////////
// implementations /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct active_world_index
{
	i32 ToI32;
};

struct stored_world_index
{
	i32 ToI32;
};

#define INVALIDWORLDINDEX {-1}
#define ISVALIDWORLDINDEX(Idx) ((Idx).ToI32 >= 0)

YSASSERT(sizeof(active_world_index) == sizeof(MEMBER(active_world_index, ToI32)));
YSASSERT(sizeof(stored_world_index) == sizeof(MEMBER(stored_world_index, ToI32)));

typedef enum
{
	EntityType_ERROR,
	EntityType_CITY,
	EntityType_ARMY,
	EntityType_WALL,
	EntityType_BACKGROUND,
} entity_type;

typedef enum
{
	WorldAllegiance_OWNED,
	WorldAllegiance_FRIEND,
	WorldAllegiance_NEUTRAL,
	WorldAllegiance_ENEMY,
} world_allegiance;

typedef enum
{
	ActionState_NOACTION,
	ActionState_DRAGGING,
	ActionState_MOVING,
} action_state;

typedef enum
{
	WorldEntityTexture_NO_TEXTURE,
	WorldEntityTexture_HAS_TEXTURE,
} world_entity_texture;

typedef enum
{
	ImageCacheStatus_UNUSED = 0,
	ImageCacheStatus_QUEUED,
	ImageCacheStatus_LOADED,
	ImageCacheStatus_TEXTURED,
} image_cache_status;

struct world_position
{
	// "address" of the chunk
	iv3 Chunk;
	// offset into the chunk
	v2  Offset;
};

struct movement
{
	v2 Dv;
	f32 Accel;
	v2 Direction;
};

struct city_data
{
	u32 Population;
	world_allegiance Allegiance;
};

struct army_data
{
	u32 NumSoldiers;

	unit_stats Units[20];

	world_allegiance Allegiance;
};

struct world_entity
{
	entity_type    Type;
	world_position Pos;
	movement       Movement;
	rect           BoundingBox;
	f32            Color[3];

	f32 Speed;
	world_position MovementGoal;
	b32 ShouldMove;

	union {
		city_data City;
		army_data Army;
	} Data;
};

struct active_world_entity
{
        stored_world_index BaseIndex;
	// Position relative to current Chunk
	v2 RelPosition;
};

struct world_chunk
{
        iv3 Chunk;
	i32 NumEntities;
        stored_world_index EntityIndices[ENTITIES_PER_BLOCK];

	world_chunk *Next;
};

struct world_entity_list
{
	i32 NumEntities;
	world_entity Entities[ENTITIES_PER_BLOCK];
	world_entity_list *Next;
};

struct world_map_dragging_data
{
	world_position Start;
};

struct world_map_moving_data
{
	stored_world_index SelectedEntityIndex;
};

struct world_input_state
{
	action_state CurrentAction;

	union {
		world_map_dragging_data Drag;
		world_map_moving_data Move;
	} ActionData;

	world_position LastMousePos;
	key_state LastMouseState;
};

struct world
{
	i32 NumLoadedEntities;
	i32 NumActiveEntities;
	world_chunk *LoadedChunks[MAX_LOADED_CHUNKS];
	world_chunk *FreeChunks;
	active_world_entity ActiveEntities[MAX_ACTIVE_ENTITIES];
        world_entity LoadedEntities[MAX_LOADED_ENTITIES];
	memory_arena *Arena;
	world_position CameraPos;
	iv3 ActiveChunk;

	transient_world_state *Transient;

	// This is used as a cache for background textures
	GLuint                 BackgroundTexture;

	b32 ShouldReload;
	b32 ShowPaths;
};

struct background_image
{
#define BACKGROUND_TILE_SIZE 50
#define BACKGROUND_CHUNK_SIZE CHUNK_DIM  * BACKGROUND_TILE_SIZE
	pixel Data[BACKGROUND_CHUNK_SIZE * BACKGROUND_CHUNK_SIZE];
};

struct create_chunk_background_task_input
{
	transient_world_state *State;
	u32 Slot;
	iv3 Chunk;
};

struct transient_world_state
{
	u64 Test;
	transient_state *State;

	sprite_id Stamps[5];

#define BACKGROUND_IMAGE_CACHE_SIZE 9
	background_image                   Cache[BACKGROUND_IMAGE_CACHE_SIZE];
	image_cache_status                 Used[BACKGROUND_IMAGE_CACHE_SIZE];
	iv3                                Chunk[BACKGROUND_IMAGE_CACHE_SIZE];
	create_chunk_background_task_input Input[BACKGROUND_IMAGE_CACHE_SIZE];
};

#endif
