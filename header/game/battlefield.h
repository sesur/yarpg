#ifndef GUARD_GAME_BATTLEFIELD_H
#define GUARD_GAME_BATTLEFIELD_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <renderer/renderer.h>
#include <util/arena_allocator.h>
#include <opengl/imgui.h>
#include <game/unit.h>
#include <game/battlefield_event.h>
#include <util/id_map.h>

FWDDECLARE(battlefield);
FWDDECLARE(battlefield_input);
FWDDECLARE(battle_movement);
FWDDECLARE(battle_result);
FWDDECLARE(attack_animation);
FWDDECLARE(movement_animation);
FWDDECLARE(particle);
FWDDECLARE(tile_data);
FWDDECLARE(tile_storage);

struct battle_movement
{
	i32 Range;
	i32 Used;
};

struct attack_animation
{
	i32 DefIdx;
	f32 Clock;
};

struct battle_result
{
	b32 BattleOver;
};

struct particle
{
        v2 Position;
	v2 Dimensions;
	v2 Direction;
	v4 Color;

	sprite_id Sprite;

	f32 TTD; // time till death
	f32 Speed;
};

struct movement_animation
{
	f32 T;
	iv2 Goal;
};

/* typedef enum */
/* { */
/* 	TileType_MOUNTAIN, */
/* 	TileType_SEA, */
/* 	TileType_FLATLAND, */
/* 	TileType_DESERT, */
/* 	TileType_OCEAN, */

/* 	TileType_COUNT, */
/* } tile_type; */

typedef enum
{
	TileType_OCEAN,
	TileType_LAKE,
	TileType_PLAINS,
	TileType_GRASLAND,
	TileType_SNOW,
	TileType_TUNDRA,
	TileType_DESERT,

	TileType_COUNT,
} tile_type;

/* struct tile_info */
/* { */
/*         struct { */
/* 		b32 OnFire : 1; */
/* 		b32 IsWater : 1; */
/* 	} Flags; */

/* 	tile_type Type; */
/* }; */

/* typedef enum */
/* { */
/* 	TileFeature_FORREST = (1 << 0), */
/* 	TileFeature_HILLS   = (1 << 0), */
/* } tile_feature; */

/* typedef enum */
/* { */
/* 	TileModifier_IMPASSABLE = (1 << 0), */
/* 	TileModifier_ONFIRE     = (1 << 1), */
/* 	TileModifier_FLOODED    = (1 << 2), */
/* } tile_modifier; */

/* struct tile_features */
/* { */
/* 	b32 HasForrest : 1; */
/* 	b32 HasHills   : 1; */
/* }; */

/* struct tile_modifiers */
/* { */
/* 	b32 IsImpassible : 1; */
/* 	b32 IsOnFire     : 1; */
/* 	b32 IsFlooded    : 1; */
/* }; */

typedef enum
{
	TileFeature_NONE        = 0,
	TileFeature_MARSH       = (1 << 0),
	TileFeature_RAINFORREST = (1 << 1),
	TileFeature_FORREST     = (1 << 2),
	TileFeature_HILLS       = (1 << 3),
} tile_feature;

struct tile_data
{
	tile_type      Type;

	// as long as the number of "flags" is small
	// its probably better to just pack them all
	// into the same struct
	union
	{
		struct
		{
			u32  Feature      : 8; /* Normal Forrest = 1, RainForrest = 2*/
			bool IsImpassible : 1;
			bool IsOnFire     : 1;
			bool IsFlooded    : 1;
		};
	};

	/* tile_features  Features; */
	/* tile_modifier  Modifiers; */
};

struct tile_storage
{
	id_map    Map;
	id_index  End;
	// packed array of all neighbor indices
	id_index  *NeighborIds;
	// Neighbors of index I are the ids in NeighborIds[X] .. NeighborIds[Y]
	// where X = NeighborsEnd[I-1] and Y = NeighborsEnd[I]
	idx       *NeighborsEnd;
	iv2       *Position;
	tile_data *Data;
};

struct battlefield
{
	i32 BattleOver;

	i32 NumEntities;
	i32 MaxEntities;

	iv2 *BoardPosition;
	v2  *DrawnPosition;
	unit_stats *UnitStats;
	v3  *Color;
	battle_movement *Movement;
	b32 *HasAttacked;
	i32 *Allegiance;
	movement_animation *MovGoal;
	attack_animation *Attack;

	tile_storage TileStorage;

	i64 PStart;
	i64 PEnd;
	particle Particles[256];

	v2 MiddlePos; // (0, 0) in screen coordinates

	rand64 CombatSeries;
	rand64 MiscSeries;
};

GAMEFN void
CreateBattlefield(battlefield *New, i32 Width, i32 Height,
		  i32 MaxEntities, memory_arena *Arena,
		  memory_arena *Scratch);

GAMEFN void
AddEntityToBattlefield(battlefield *Field, iv2 Position, v3 Color);

GAMEFN void
AddUnitToBattlefield(battlefield *Field,
		     iv2 Position,
		     unit_stats Stats,
		     v3 Color,
		     i32 Allegiance);

GAMEFN battlefield_event_queue *
UpdateAndRenderBattlefield(battlefield *Field,
			   render_group *RenderGroup,
			   asset_store *Assets,
			   memory_arena *Scratch,
                           f32 DeltaTime);

GAMEFN void ResetBattlefield(battlefield *Field);

GAMEFN i32 GetIndexOfPos(battlefield *Field, iv2 HexPos);
GAMEFN b32 IsEmpty(battlefield *Field, iv2 HexPos);
GAMEFN b32 Fight(battlefield *Field, i32 AtkIdx, i32 DefIdx);
GAMEFN b32 BFMoveEntity(battlefield *Field, i32 Idx, iv2 NewPosition);
GAMEFN void BFTeleportEntity(battlefield *Field, i32 Idx, iv2 NewPosition);

GAMEFN b32 IsBusy(battlefield *Field, i32 Idx);
GAMEFN b32 ValidHex(battlefield *Field, iv2 HexPos);

// return NULL if the tile doesnt exist
GAMEFN id_index *GetTileSlot(tile_storage *Store, iv2 Pos);
GAMEFN f32 TileCost(tile_data Data);
GAMEFN void RebuildMap(battlefield *Field, idx Width, idx Height,
		       memory_arena *Scratch);

#endif
