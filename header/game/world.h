#ifndef GUARD_GAME_WORLD_H
#define GUARD_GAME_WORLD_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#include <util/arena_allocator.h>
#include <transient.h>
#include <platform.h> // struct key_state
#include <input.h>
#include <renderer/renderer.h>
#include <game/unit.h>
#include <game/world_types.h>
#include <game/world_event.h>

GAMEFN world_chunk *GetWorldChunk(world *World, iv3 Chunk);
GAMEFN world_chunk *GenerateWorldChunk(world *World, iv3 Chunk, world_chunk *NewChunk);
GAMEFN void InitWorld(world *World, memory_arena *Arena);
GAMEFN void DestroyWorld(world *World);
GAMEFN stored_world_index AddArmy(world *World, world_position Pos, f32 Color[3], world_allegiance Allegiance);
GAMEFN stored_world_index AddWall(world *World, world_position Pos);
GAMEFN void CanonizePosition(world_position *Position);
GAMEFN void SetCameraPos(world *World, world_position CameraPos);
GAMEFN world_position MovePosition(world_position Start, f32 Dt, v2 Vel, v2 Accel);
GAMEFN world_entity *GetEntityByIndex(world *World, stored_world_index Index);
GAMEFN stored_world_index GetEntityIndex(world *World, world_entity *Entity);
GAMEFN void Reload(world *World);
GAMEFN void InitTransientWorldState(world *World, transient_state *Transient,
				    glyph_table *UiTable);
GAMEFN world_event_queue *UpdateAndRenderWorld(world *World, f32 DeltaTime,
					       render_group *RenderGroup);
GAMEFN void RemoveEntity(world_chunk *Chunk, stored_world_index EntityIndex);
GAMEFN void TeleportEntity(world *World, stored_world_index Entity,
			   world_position Pos);

GAMEFN void SetMoveGoal(world *World, stored_world_index Entity,
			f32 Speed, world_position Pos);

GAMEFN void StopEntity(world *World, stored_world_index Entity);

#endif
