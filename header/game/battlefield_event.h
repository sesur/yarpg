#ifndef GUARD_GAME_BATTLEFIELD_EVENT_H
#define GUARD_GAME_BATTLEFIELD_EVENT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <game/battlefield.h>

FWDDECLARE(battlefield_event);
FWDDECLARE(battlefield_event_queue);
FWDDECLARE(battlefield_mov_finished_event);
FWDDECLARE(battlefield_atk_finished_event);

typedef enum
{
	BattlefieldEventType_MOVFINISHED,
	BattlefieldEventType_ATKFINISHED,
} battlefield_event_type;

struct battlefield_event
{
	battlefield_event_type Type;
        union {
		battlefield_atk_finished_event *AttackFinished;
		battlefield_mov_finished_event *MovementFinished;
		void *Unused;
	} Event;
};

struct battlefield_atk_finished_event
{
	i32 AttackerIndex;
	i32 DefenderIndex;
};

struct battlefield_mov_finished_event
{
	i32 Entity;
};

struct battlefield_event_queue
{
	i32 NumItems;
	i32 DataStart;
	battlefield_event_queue *Next;
        byte Data[1 << 12];
};

GAMEFN void InitBattlefieldEventQueue(battlefield_event_queue *Queue);

GAMEFN b32 BFAddMovementFinishedEvent(battlefield_event_queue *Queue,
				      battlefield_mov_finished_event Event);
GAMEFN b32 BFAddAttackFinishedEvent(battlefield_event_queue *Queue,
				    battlefield_atk_finished_event Event);

#endif
