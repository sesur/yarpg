#ifndef GUARD_GAME_WORLD_EVENT_H
#define GUARD_GAME_WORLD_EVENT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <game/world_types.h>

FWDDECLARE(world_event);
FWDDECLARE(world_event_queue);
FWDDECLARE(world_collision_event);
FWDDECLARE(world_mov_finished_event);

typedef enum
{
	WorldEventType_COLLISION,
	WorldEventType_QUIT,
	WorldEventType_MOVFINISHED,
} world_event_type;

struct world_event
{
	world_event_type Type;
        union {
		world_collision_event *Collision;
		world_mov_finished_event *MovementFinished;
		void *Unused;
	} Event;
};

struct world_collision_event
{
	stored_world_index AttackerIndex;
	stored_world_index DefenderIndex;
};

struct world_mov_finished_event
{
	stored_world_index Entity;
};

struct world_event_queue
{
	i32 NumItems;
	i32 DataStart;
	world_event_queue *Next;
        byte Data[1 << 12];
};

GAMEFN void InitWorldEventQueue(world_event_queue *Queue);

GAMEFN b32 AddCollisionEvent(world_event_queue *Queue, world_collision_event Event);
GAMEFN b32 AddMovementFinishedEvent(world_event_queue *Queue, world_mov_finished_event Event);
GAMEFN b32 AddQuitEvent(world_event_queue *Queue);

#endif
