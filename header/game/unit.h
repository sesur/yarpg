#ifndef GUARD_GAME_UNIT_H
#define GUARD_GAME_UNIT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/random.h>

FWDDECLARE(unit_stats);
FWDDECLARE(weapon_data);
FWDDECLARE(defense_data);
FWDDECLARE(armor_data);
FWDDECLARE(equipment);

typedef enum
{
	EquipmentType_ARMOR,
	EquipmentType_WEAPON,
	EquipmentType_DEFENSE,
} equipment_type;

typedef enum
{
	WeaponType_NOTHING,
	WeaponType_FIST,
	WeaponType_BOW,
	WeaponType_SWORD,
} weapon_type;

typedef enum
{
	DefenseType_NOTHING,
	DefenseType_TOWERSHIELD,
	DefenseType_SHIELD,
} defense_type;

typedef enum
{
	ArmorType_NOTHING,
	ArmorType_LEATHER,
	ArmorType_STEEL,
} armor_type;

struct weapon_data
{
	weapon_type Type;
	f32 HitChance;
	i32 Damage;

	i32 MinRange, MaxRange;

	i32 NumAttacks;
};

struct defense_data
{
	defense_type Type;
        f32 BlockChance;
	i32 Defense;
};

struct armor_data
{
	armor_type Type;
	i32 Defense;
};

struct equipment
{
	equipment_type Type;

	f32 Weight;

	char const *Name;

	union {
		weapon_data Weapon;
		armor_data Armor;
		defense_data Defense;
	} Data;
};

typedef enum
{
	RaceId_ERROR = -1,
	RaceId_NONE,
	RaceId_HUMAN,
	RaceId_ELF,
	RaceId_ORK,
} race_id;

struct unit_stats
{
        i32 Weight;
	i32 CurrentHealth, MaxHealth;

	race_id Race;

	i32 Speed;

	struct
	{
		u64 IsDead      : 1;
		u64 IsMoving    : 1;
		u64 IsAttacking : 1;
	} Flags;

	i32 Equipment[NUM_EQUIPMENT];
};

GAMEFN unit_stats
RandomUnit(rand64 *R);

static equipment const EquipmentTable[4];

#endif
