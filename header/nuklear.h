#ifndef GUARD_NUKLEAR_H
#define GUARD_NUKLEAR_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#include <opengl/opengl.h>
#include <opengl/shader.h>
#include <util/arena_allocator.h>
#include <util/list_allocator.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#include <Nuklear/nuklear.h>

#include <platform.h>

FWDDECLARE(nuklear_shader);
FWDDECLARE(device);
FWDDECLARE(nuklear_context);

struct nuklear_shader
{
	GLuint Program;
	union {
		struct {
			GLint Position;
			GLint TexturePos;
			GLint Color;
		};
		GLint Attributes[3];
	};
	GLint Projection;
};

struct device
{
	struct nk_buffer Commands;
	struct nk_draw_null_texture Nothing;
	GLuint ArrayBuffer;
	union {
		struct {
			GLuint VertexBuffer, IndexBuffer;
		};
		GLuint Buffers[2];
	};
        nuklear_shader Shader;
	GLuint FontTexture;

};

struct nuklear_context
{
	device              Graphics;
	struct nk_font      *Font;
	struct nk_context   Nuklear;
	list_allocator      ListAlloc;
	memory_arena        ArenaAlloc;
        struct nk_allocator PermAlloc;
	struct nk_allocator TempAlloc;

};

GAMEFN void InitNuklear(nuklear_context *Ctx, udx MemSize, void *Memory);
GAMEFN void DrawNuklear(nuklear_context *Ctx, int width, int height,
			struct nk_vec2 scale, enum nk_anti_aliasing AA);
GAMEFN void ReleaseNuklear(nuklear_context *Ctx);
GAMEFN void ParseInputsNuklear(nuklear_context *Ctx, iv2 MousePos, mouse_data *Input);

#endif
