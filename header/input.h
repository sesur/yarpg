#ifndef GUARD_INPUT_H
#define GUARD_INPUT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#define INPUT_TEXT_BUFFER_COUNT 4

FWDDECLARE(key_state);
FWDDECLARE(mouse_data);
FWDDECLARE(keyboard_data);
FWDDECLARE(controller_data);
FWDDECLARE(input_device);
FWDDECLARE(game_input);

typedef enum
{
	KeyId_UP,
	KeyId_DOWN,
	KeyId_RIGHT,
	KeyId_LEFT,
	KeyId_W,
	KeyId_A,
	KeyId_S,
	KeyId_D,
	KeyId_M,
	KeyId_N,
	KeyId_ENTER,
	KeyId_BACKSPACE,
	KeyId_ESCAPE,
	KeyId_NUM_ENTER,
        NUM_KEYBOARD_KEYS,

	KeyId_INTERACT = KeyId_ENTER,
	KeyId_DEBUG    = KeyId_NUM_ENTER,
} keyboard_key_id;

typedef enum
{
	KeyId_MOUSE_BUTTON_1,
	KeyId_MOUSE_BUTTON_2,
	KeyId_MOUSE_BUTTON_3,
	NUM_MOUSE_KEYS,
} mouse_key_id;

typedef enum
{
	KeyId_CONTROLLER_BUTTON_1,
	NUM_CONTROLLER_KEYS,
} controller_key_id;

struct key_state
{
        i32 HalfTransitions;
        b32 IsDown;
};

typedef enum
{
	InputMethod_MOUSE,
	InputMethod_KEYBOARD,
	InputMethod_CONTROLLER,
} input_method;

struct mouse_data
{
	key_state Key[NUM_MOUSE_KEYS];
};

struct keyboard_data
{
	key_state Key[NUM_KEYBOARD_KEYS];

	u32 GlyphCount;
	u32 Text[INPUT_TEXT_BUFFER_COUNT];
};

struct controller_data
{
	key_state Key[NUM_CONTROLLER_KEYS];

	f32 StickA, StickB;
};

struct input_device
{
	input_method Method;
	union {
		mouse_data *Mouse;
		keyboard_data *Keyboard;
		controller_data *Controller;
	} Data;
};

struct game_input
{
	i32 NumDevices;
	iv2 CursorPos;
	input_device *Devices;
};

#endif
