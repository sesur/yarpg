#ifndef GUARD_FONT_H
#define GUARD_FONT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(font_info);
FWDDECLARE(char_info);

typedef enum {
	FONT_ROBOTTO,
        FONT_DROID_SANS,
	FONT_PROGGY_TINY,
	FONT_COUNT,
	NO_FONT,
	/* CURRENT_FONT = -1, */
	DEFAULT_FONT = FONT_ROBOTTO,
} font_id;

struct font_info
{
	i16 Ascender, Descender, MaxHeight, MaxWidth;
	u16 UnitsPerEM;
	u16 LinePitch;

	i16 UnderlinePos;
	i16 UnderlineThickness;

	u32 PtSize;

	i32 Error;
};

struct char_info
{
	u32 Advance;
	i32 BearingLeft;
	i32 BearingTop;
        i32 Width;
	i32 Height;

	i32 Error;

	u32  Pitch;
	byte *Buffer;
};

#define REQUEST_FONT(name) font_info name(font_id FontId, u32 PtSize)
typedef REQUEST_FONT(request_font);
PLATFORMFN REQUEST_FONT(RequestFont);

#define RENDER_GLYPH(name) char_info name(u32 Char)
typedef RENDER_GLYPH(render_glyph);
PLATFORMFN RENDER_GLYPH(RenderGlyph);

#endif
