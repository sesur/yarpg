#ifndef GUARD_THREAD_H
#define GUARD_THREAD_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/ring_alloc.h>
#include <debug/debug_state_internal.h>

FWDDECLARE(async_frame);
FWDDECLARE(thread_context);
FWDDECLARE(async_stack);

#define TASK_CALLBACK_FN(name) void name(thread_context const *Ctx, void volatile *Arg)
#define TASK_CALLBACK(name) void (*name)(thread_context const *Ctx, void volatile *Arg)
typedef TASK_CALLBACK(taskfn);

typedef enum async_frame_status
{
	AsyncFrameStatus_EMPTY,
	AsyncFrameStatus_READY,
	AsyncFrameStatus_READ,
} async_frame_status;

struct async_frame
{
	taskfn Fun;
	async_frame_status volatile Status;
	i32 Size;
	i32 Alignment;
	i32 ArgOffset;
};

struct async_stack
{
	// this is read only for every thread
	// except the owning thread!
	async_frame *Frames;
	ring_alloc Alloc;
	i32 Capacity;
	i32 Head;
	i32 Tail;
};

struct thread_context
{
	idx Id;
	thread_debug_state DebugState;
	GLFWwindow *RenderContext;
	async_stack Stack;

	// if the number of elements in Stack
	// is bigger than HelpCallThreshold
	// the thread will try to wake up another thread
	// to help with the workload
	i32 HelpCallThreshold;

	idx NumOtherStacks;
	async_stack **OtherStacks;
};

#define PUSH_TASK(name) b32 name(taskfn Fun, idx Size, idx Alignment, void *Arg)
typedef PUSH_TASK(push_task);
PLATFORMFN PUSH_TASK(PushTask);

#define WAIT(Name) void Name(void)
typedef WAIT(wait);
PLATFORMFN WAIT(Wait);

#define ENSURE_IN_THREAD(ThreadId) (YASSERT(CurrentThreadContext()->Id == (ThreadId)))
#define ENSURE_MAINTHREAD() ENSURE_IN_THREAD(0)

#endif
