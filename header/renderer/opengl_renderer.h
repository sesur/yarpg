#ifndef GUARD_RENDERER_OPENGL_RENDERER_H
#define GUARD_RENDERER_OPENGL_RENDERER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <renderer/renderer_types.h>
#include <opengl/renderer.h>

GAMEFN void OpenGLDrawRenderGroup(renderer *Renderer, memory_arena *Scratch,
				  basis_system Screen, render_commands *Commands);

#endif
