#ifndef GUARD_RENDERER_RENDERER_H
#define GUARD_RENDERER_RENDERER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <util/asset.h>
#include <renderer/renderer_types.h>
#include <opengl/renderer.h>

FWDDECLARE(render_group);

struct render_group
{
	render_commands *Commands;
	opengl_backend *Backend;
	asset_store *Assets;

	basis_system Basis;

	f32 Level;
};

GAMEFN render_commands CreateRenderCommands(udx Size, byte Buffer[static Size]);
GAMEFN void ResetCommands(render_commands *Commands);

GAMEFN render_group CreateRenderGroup(opengl_backend *Backend,
				      render_commands *Commands,
				      asset_store *Assets,
				      basis_system Basis);

GAMEFN render_group SubRenderGroup(render_group *Group, basis_system Basis);

GAMEFN render_entity_rectangle *PushRect(render_group *Group,
					 v2 Origin, v2 Dimensions, v4 Color);

GAMEFN render_entity_texture *PushSprite(render_group *Group,
					 v2 Origin, v2 Dimensions,
					 sprite_id TexId, v4 Color);

GAMEFN render_entity_texture *PushTexture(render_group *Group,
					  v2 Origin, v2 Dimensions,
					  texture Tex, v4 Color);

GAMEFN render_entity_hexagon *PushHexagon(render_group *Group,
					  v2 Center, f32 Radius, v4 Color);

GAMEFN render_entity_triangle *PushTriangle(render_group *Group,
					    v2 const Points[3], v4 Color);

GAMEFN render_entity_line *PushLine(render_group *Group, v2 Origin, v2 End,
				    f32 Thickness, v4 Color);

GAMEFN void PushRectOutline(render_group *Group, rect Rect,
			    f32 Thickness, v4 Color);

#if 0

FWDDECLARE(renderer_backend);

texture_t
CreateTextureOfSize(renderer_backend *Backend, idx Width, idx Height);

texture_t
TextureOfSprite(renderer_backend *Backend, asset_store *Assets, sprite_id Sprite);

// alternative 1:

void /* overwrites texture */
FillTexture(renderer_backend *Backend, texture Texture, irect Pos, pixel *Data);

void /* blits textures together */
BlitTexture(renderer_backend *Backend, texture Texture, irect Pos, pixel *Data);

// alternative 2:

typedef enum
{
	BlendMode_OVERWRITE,
	BlendMode_BLEND,
} blend_Mode;

struct render_group
{
	//....

	texture_t Target; // Target may be screen
	blend_mode Blend;
};

#endif

#endif
