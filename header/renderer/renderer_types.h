#ifndef GUARD_RENDERER_RENDERER_TYPES_H
#define GUARD_RENDERER_RENDERER_TYPES_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/asset_types.h>
#include <opengl/renderer.h>

FWDDECLARE(render_commands);
FWDDECLARE(render_entity_header);
FWDDECLARE(render_entity_rectangle);
FWDDECLARE(render_entity_hexagon);
FWDDECLARE(render_entity_line);
FWDDECLARE(render_entity_triangle);
FWDDECLARE(render_entity_texture);
FWDDECLARE(texture);

typedef enum
{
	RenderEntityType_RECTANGLE,
	RenderEntityType_TEXTURE,
	RenderEntityType_HEXAGON,
	RenderEntityType_LINE,
	RenderEntityType_TRIANGLE,

	RenderEntityType_COUNT
} render_entity_type;


struct texture
{
	i32 ID;
	v2 Origin;
	v2 Dimensions;
	b32 IsMono;
};

struct render_commands
{
	i32 Capacity;
	i32 NumHeader;

	/*
	 * Data is organized this way:
	 *
	 * Data ~> +--------------+ *
	 *         | Header 1     | |
	 *         +--------------+ | Header starts at start, grows downwards
	 *         | Header 2     | |
	 *         +--------------+ |
	 *         | ...          | v
	 *         +--------------+
	 *         | Header N     |
	 *         +--------------+ <-- current top
	 *         |              |
	 *         |   <empty>    |
	 *         |              |
	 *         +--------------+ <-- current bottom
	 *         | Data N       |
	 *         +--------------+
	 *         | ...          | ^
	 *         +--------------+ |
	 *         | Data 2       | | Data starts at end, grows upward
	 *         +--------------+ |
	 *         | Data 1       | |
	 *         +--------------+ *
	 *
	 * Pros:
	 * This way it would be easy to swap Elements (by swaping their headers)
	 * and we could have a pseudo random access to render_entities,
	 * if we included a pointer/offset to the data in the header.
	 * Also prevents memory waste, since everything is more tightly packed.
	 * Cons:
	 * Might lead to worse cache performance, since we constantly change
	 * reading direction while parsing the data,
	 * render_commands struct becomes slightly bigger, since we need to store
	 * additional offsets (i.e. current top, current bottom).
	 */

	byte *Data;
};

struct render_entity_header
{
	render_entity_type Type;

	u32 NumEntities;
	u32 Start;
};

struct render_entity_rectangle
{
        v3 Origin;
	v2 Dimensions;
        v4 Color;
};

struct render_entity_triangle
{
        v3 Points[3];
        v4 Color;
};

struct render_entity_line
{
	v3 Origin;
	v3 End;
	v4 Color;
	f32 Thickness;
};

struct render_entity_hexagon
{
	v3 Center;
	f32 Radius;
	v4 Color;
};

struct render_entity_textured_rectangle
{
	v3 Origin;
	v2 Dimensions;
        v4 Color;
        loaded_texture Texture;
};

struct render_entity_texture
{
	v3 Origin;
	v2 Dimensions;
        v4 Color;
	texture Texture;
};

#endif /* GUARD_RENDERER_TYPES_H */
