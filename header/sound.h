#ifndef GUARD_SOUND_H
#define GUARD_SOUND_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(sound_buffer);
FWDDECLARE(real_sound_buffer);

struct sound_buffer
{
	i32 NumChannels;
	i32 BitsPerSample;
	i32 SampleRate;
	i32 ChannelPitch; /* ChannelPitch = -1 means the
			     channels are interleaved */

	i32 NumSamples;
	void *Data;
};

struct real_sound_buffer
{
	i32 NumChannels;
	i32 ChannelPitch;
	i32 SampleRate;

	i32 NumSamples;
	f32 *Data;
};

#define INTERLEAVED_CHANNEL_PITCH -1

#endif /* GUARD_SOUND_H */
