#ifndef GUARD_OPENGL_SHADER_H
#define GUARD_OPENGL_SHADER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <opengl/opengl.h>

GAMEFN b32 BuildProgram(
	u32 NumStages,
	char const *Pipeline[static NumStages],
	GLenum StageTypes[static NumStages],
	GLuint *Result);
GAMEFN void LookupProgramLocations(
	GLuint Program,
	u32 NumAttributes,
	char const *AttributeNames[static NumAttributes],
	GLint AttributeLocations[static NumAttributes],
	u32 NumUniforms,
	char const *UniformNames[static NumUniforms],
	GLint UniformLocations[static NumUniforms]);

#endif
