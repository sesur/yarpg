#ifndef GUARD_OPENGL_STRING_H
#define GUARD_OPENGL_STRING_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <opengl/opengl.h>
#include <opengl/shader.h>
#include <opengl/renderer.h>
#include <renderer/renderer.h>

#include <font.h>

#define NO_GL

FWDDECLARE(glyph_table);
FWDDECLARE(glyph_info);

struct glyph_info
{
	u32 Advance;
	i32 BearingLeft;
	i32 BearingTop;
        i32 Width;
	i32 Height;

	i32 StartX, StartY;

	b32 Valid; // is this still valid? if not reload!
	b32 Used;  // was this used this frame (or can it be savely overwritten)
};

struct glyph_table
{
	u32      LoadedGlyph[GLYPH_TBL_SIZE];
	glyph_info GlyphInfo[GLYPH_TBL_SIZE];

	font_info  FontInfo;

#if defined(NO_GL)
	custom_texture Atlas;
#else
	GLuint AtlasTexture;
	u32 TextureWidth, TextureHeight;
#endif
};

GAMEFN void InitGlyphTable(glyph_table *Tbl, font_id FontId, u32 PtSize, memory_arena *Scratch);
GAMEFN void ReleaseGlyphTable(glyph_table *Tbl);
GAMEFN void InvalidateGlyphTable(glyph_table *Tbl);
GAMEFN u32 EnsureLoaded(glyph_table *Tbl, u32 Char);

#define StringDimensions(Tbl, NumChars, String, Width, Height) \
	_Generic((*String), \
		 u32: StringDimensionsUTF32,\
		 char: StringDimensionsUTF8)(Tbl, NumChars, String, Width, Height)

GAMEFN void StringDimensionsUTF32(glyph_table *Table,
				  u32 NumChars,
				  u32 const *String,
				  i32 *StringWidth, i32 *StringHeight);

GAMEFN void StringDimensionsUTF8(glyph_table *Table,
				 u32 NumChars,
				 char const *String,
				 i32 *StringWidth, i32 *StringHeight);

#define RenderString(Tbl, Group, StartX, StartY, Scale, Color, NumChars, String) \
	_Generic((*String), \
		 u32: RenderStringUTF32, \
		 char: RenderStringUTF8)(Tbl, Group, StartX, StartY, Scale, Color, NumChars, String)

GAMEFN void RenderStringUTF32(glyph_table *Table, render_group *RenderGroup,
			      f32 StartX, f32 StartY, f32 Scale,
			      v4 Color, u32 NumChars, u32 const *String);

GAMEFN void RenderStringUTF8(glyph_table *Table,
			     render_group *RenderGroup,
			     f32 StartX, f32 StartY, f32 StringHeight,
			     v4 Color,
			     u32 NumChars, char const *String);

GAMEFN void DrawString(glyph_table *Table, render_group *RenderGroup,
		       v4 Color, rect Bounds, u32 NumChars,
		       char const *String);

#endif
