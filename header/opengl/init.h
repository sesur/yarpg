#ifndef GUARD_OPENGL_INIT_H
#define GUARD_OPENGL_INIT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#include <opengl/opengl.h>

FWDDECLARE(opengl);

struct opengl
{
	i32 Major, Minor, Profile;
	i32 AntiAlias;
	b32 ForwardCompat;
	b32 Resizable;
	b32 FullScreen;
};

PRIVATE void InitGLFW(void);
PRIVATE void CloseGLFW(void);
PRIVATE GLFWwindow *OpenWindow(opengl Options, char const *Title,
			      i32 Width, i32 Height);
PRIVATE void CloseWindow(GLFWwindow *Window);

#endif
