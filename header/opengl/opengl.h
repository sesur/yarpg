#ifndef GUARD_OPENGL_OPENGL_H
#define GUARD_OPENGL_OPENGL_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#include <glad2/gl.h>
#include <glad2/khrplatform.h>

#include <GLFW/glfw3.h>

#define GL_STACK_OVERFLOW  0x0503
#define GL_STACK_UNDERFLOW 0x0504
#define GL_CONTEXT_LOST    0x0507

GAMEFN void OpenGLCheckErrors(char const *Prefix);

#define GL_QUICK_CHECK() OpenGLCheckErrors(__FILE__ ":" STRING(__LINE__))

#endif
