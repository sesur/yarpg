#ifndef GUARD_OPENGL_RENDERER_H
#define GUARD_OPENGL_RENDERER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/string.h>
#include <opengl/opengl.h>
#include <opengl/shader.h>
#include <util/arena_allocator.h>

// should this be "dynamic" ?
#define BATCH_VERTEX_COUNT 16384

FWDDECLARE(render_shader);
FWDDECLARE(batch_data);
FWDDECLARE(vertex_data);
FWDDECLARE(render_batch);
FWDDECLARE(renderer);
FWDDECLARE(texture_store);
FWDDECLARE(texture_upload_task);
FWDDECLARE(renderer_debug_info);
FWDDECLARE(opengl_backend);
FWDDECLARE(triangle_alloc);
FWDDECLARE(threaded_texture_load_args);
FWDDECLARE(loaded_texture);

struct loaded_texture
{
	u32 Id;
};

struct threaded_texture_load_args
{
	threaded_texture_load_args *Next;
	texture_store *Store;
	i32           Idx;
	image         ToLoad;
};

struct render_shader
{
	GLuint Program;
	union {
		struct {
			GLint  Position;
			GLint  Color;
			GLint  TexturePos;
			GLint  Transform;
			GLint  TextureId;
		};
		GLint Attributes[5];
	};

	union {
		struct {
			GLint Projection;
			GLint Textures;
		};
		GLint Uniforms[2];
	};

};

struct vertex_data
{
	v3 Position;
	v3 Transform;
	v2 TexturePos;
	v4 Color;
};

struct batch_data
{
	vertex_data Data;
	float TextureId;
};

struct render_batch
{
	i32 Free;
	i32 Size;
        i32 TexturesUsed;
	i32 TextureCount;
	i32 VertexCount;

	u32         *Indices;
	batch_data  *VertexData;
	b32         *IsMono;
	GLuint      *Textures;
};

typedef enum
{
	TextureStatus_UNUSED,
	TextureStatus_QUEUED,
	TextureStatus_LOADED,
	TextureStatus_WORKED,
} texture_status;

struct texture_store
{
#define TEXTURE_STORE_NUM_ENTRIES 128
	sprite_id      Ids[TEXTURE_STORE_NUM_ENTRIES];
	texture_status Status[TEXTURE_STORE_NUM_ENTRIES];
	GLuint         Textures[TEXTURE_STORE_NUM_ENTRIES];

	memory_arena *Arena;

	threaded_texture_load_args * volatile Free;
};

struct renderer_debug_info
{
	idx DrawCalls;
	idx TexturesUsed;
	idx IndicesUsed;

	u64 BytesUploaded;

	idx TextureCount;
	idx IndexCount;
};

struct opengl_backend
{
	GLuint StreamVAO;
	union {
		struct { GLuint StreamVBO, StreamIBO; };
		GLuint VertexBuffers[2];
	};

	GLuint        NullTexture, ErrorTexture;
        texture_store Store;
        i32           TextureSlots;

	str_view      ShaderPreamble;
};

struct renderer
{
	opengl_backend *Backend;
	// permanent

	render_shader Shader;
	GLuint        CurrentTextureId;
	GLuint        IsMonoLocation;
	// debug
	renderer_debug_info DebugInfo;

	// transient
	render_batch *Batch;
	memory_arena *Arena;
	f32          *Projection;
};

struct triangle_alloc
{
	u32 Count;

	// 3 per triangle!
	v3 *Positions;
	v3 *Transforms;
	v2 *TexturePos;
	v4 *Colors;

	u32 PositionStride;
	u32 TransformStride;
	u32 TexturePosStride;
	u32 ColorStride;
};

GAMEFN void CreateBackend(opengl_backend *Backend, memory_arena *Perm);
GAMEFN void ReleaseBackend(opengl_backend *Backend);

GAMEFN void RendererCreate(renderer *Renderer,
                           opengl_backend *Backend,
			   render_shader Shader);

GAMEFN void RendererPrepare(renderer *Renderer, memory_arena *Arena,
			    f32 Projection[4][4]);

GAMEFN triangle_alloc AllocTriangles(renderer *Renderer, u32 NumTriangles);

GAMEFN void DrawTriangles(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data);

GAMEFN void DrawTriangleElements(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data,
	u32 NumIndices,
	u32 *Indices);

GAMEFN void DrawTriangleStrip(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data);

GAMEFN void DrawTriangleFan(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data);

GAMEFN void DrawTexturedTriangles(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data,
	GLuint Texture);

GAMEFN void RendererFlush(renderer *Renderer);
GAMEFN void RendererFinish(renderer *Renderer);

GAMEFN void RendererRelease(renderer *Renderer);
GAMEFN void BindTexture(renderer *Renderer, GLuint Texture);
GAMEFN void BindMonoTexture(renderer *Renderer, GLuint Texture);
GAMEFN void UnbindTexture(renderer *Renderer);
GAMEFN void FlushRenderer(renderer *Renderer);

GAMEFN loaded_texture RegisterForFrame(opengl_backend *Backend, asset_store *Assets, sprite_id SpriteId);

FWDDECLARE(custom_texture);

struct custom_texture
{
	GLuint Id;
	iv2    Size;
	b32    IsMono;
};

GAMEFN custom_texture CreateTexture(opengl_backend *Backend, iv2 Size);
GAMEFN custom_texture CreateMonoTexture(opengl_backend *Backend, iv2 Size);
GAMEFN custom_texture LoadImage(opengl_backend *Backend, image Image);
GAMEFN void           ReleaseTexture(custom_texture Tex);
GAMEFN void           WriteTexture(custom_texture Tex, irect Region, void *Image);
#endif
