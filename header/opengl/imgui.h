#ifndef GUARD_OPENGL_IMGUI_H
#define GUARD_OPENGL_IMGUI_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <opengl/renderer.h>
#include <opengl/string.h>
#include <input.h>
#include <renderer/renderer.h>

FWDDECLARE(ui_state);
FWDDECLARE(style);
FWDDECLARE(ui_input_state);
FWDDECLARE(ui_mouse_state);
FWDDECLARE(ui_keyboard_state);
FWDDECLARE(ui_interaction);
FWDDECLARE(ui_interaction_state);
FWDDECLARE(gui_key);
FWDDECLARE(modifier);

typedef i32 widget_id;

struct ui_interaction
{
	i32 Widget;
	i16 Window;
	i16 Interaction;
};

struct ui_interaction_state
{
	ui_interaction Hot, Active;
};

struct style
{
	v4 TextColor;
	v4 BackgroundColor;
	v4 WidgetColor;
};

struct ui_mouse_state
{
	iv2 Pos;
	mouse_data Data;
	i32 DeviceNum;
};

struct ui_keyboard_state
{
	keyboard_data Data;
	i32 DeviceNum;
};

struct ui_input_state
{
	ui_mouse_state Mouse;
	ui_keyboard_state Keyboard;

	ui_interaction_state Interaction;
};

typedef enum
{
	Key_A,
	Key_S,
	Key_R,
	Key_ESCAPE,
	Key_N,
	Key_ENTER,
	Key_M,
} key_id;

struct modifier
{
	i32 Test;
};

struct gui_key
{
	modifier Mod;
	key_id   Key;
};

struct ui_state
{
        i32 FrameHeight, FrameWidth;
        game_input Input;

	i32 CurrentWindow;
	irect ActiveBox, HotBox, CurrentBox;

	ui_input_state       Current, Next;

	glyph_table  *Glyphs;

	irect  DrawableBounds;

	render_group *RenderGroup;
};

typedef enum
{
        TextPosition_Y_TOP        = 0x01,
	TextPosition_Y_CENTERED   = 0x02,
	TextPosition_Y_BOTTOM     = 0x03,
	TextPosition_X_RIGHT      = 0x10,
	TextPosition_X_CENTERED   = 0x20,
	TextPosition_X_LEFT       = 0x30,
	TextPosition_RIGHT        = TextPosition_Y_CENTERED | TextPosition_X_RIGHT,
	TextPosition_CENTERED     = TextPosition_Y_CENTERED | TextPosition_X_CENTERED,
	TextPosition_LEFT         = TextPosition_Y_CENTERED | TextPosition_X_LEFT,
} text_position;

typedef enum
{
	WindowOption_TRANSPARENT   = (1 << 1),
	WindowOption_CLOSABLE      = (1 << 2),
	WindowOption_MOVABLE       = (1 << 3),
	WindowOption_TITLE         = (1 << 4),
	WindowOption_RESIZE_BOTTOM = (1 << 5),
	WindowOption_RESIZE_RIGHT  = (1 << 6),
	WindowOption_RESIZE_LEFT   = (1 << 7),
	WindowOption_NONINTERACTIVE = (1 << 8),




	WindowOption_RESIZE = WindowOption_RESIZE_BOTTOM |
	                      WindowOption_RESIZE_RIGHT  |
	                      WindowOption_RESIZE_LEFT,
} window_options;

GAMEFN void InitGui(ui_state *UiState,
		    glyph_table  *Table);
GAMEFN void PrepareFrame(ui_state *UiState,
			 memory_arena *Arena,
			 i32 FrameWidth, i32 FrameHeight,
			 game_input Input, render_group *RenderGroup);
GAMEFN void FinishFrame(ui_state *UiState);
GAMEFN b32 BeginWindow(ui_state *UiState,
		       i32 Id,
		       char const *Title, irect *Bounds, b32 Flags);
GAMEFN void EndWindow(ui_state *UiState);

GAMEFN b32 Button(ui_state *UiState, i32 Id, irect Bounds,
		  u32 NumChars, char const *Text);

GAMEFN void VerticalBar(ui_state *UiState, irect Bounds,
			f32 Min, f32 Val, f32 Max, v4 Color);

GAMEFN b32 Slider(ui_state *UiState,
		  i32 Id, irect Bounds,
		  f32 *Val, f32 Min, f32 Max);

GAMEFN b32 VerticalSlider(ui_state *UiState,
			  i32 Id, irect Bounds,
			  f32 *Val, f32 Min, f32 Max);
GAMEFN b32 CheckBox(ui_state *UiState, i32 Id, irect Bounds,
		    b32 *Bool);

GAMEFN b32 Label(ui_state *UiState, irect Bounds, u32 TextPosition, u32 NumChars,
		 char const *Content, v4 TextColor, f32 TextScale);
GAMEFN void Text(ui_state *UiState, irect Bounds, u32 TextPosition, u32 NumChars,
		 char const *Content, v4 TextColor, f32 TextScale);

GAMEFN void Graph(ui_state *UiState, irect Bounds,
		  u32 BufferSize, f32 *Buffer,
		  u32 Start, u32 Count,
		  f32 Min, f32 Max, v4 Color);

GAMEFN rect LocalToScreen(ui_state *State, irect Rect);

GAMEFN void BlockGraph(ui_state *UiState, irect Bounds,
		       u32 BufferSize, f32 *Buffer,
		       u32 Start, u32 Count,
		       f32 Min, f32 Zero, f32 Max, v4 Color);

FWDDECLARE(plane);
FWDDECLARE(plane_result);

struct plane
{
	i32 Id;
        irect Bounds;
};

struct plane_result
{
	plane Updated;
	i32 IsHot;
	i32 IsActive;
};

GAMEFN plane_result Plane(ui_state *UiState, plane Desc);

GAMEFN void Border(ui_state *UiState, irect Bounds, i32 Thickness, v4 Color);

FWDDECLARE(textinput);
struct textinput
{
	u32 Capacity;
	i32 NumChars;
	u32 Head;
	byte *Buffer;
};

FWDDECLARE(textinput_result);
struct textinput_result
{
	bool BufferChanged : 1;
	bool EnterPressed  : 1;
};

GAMEFN textinput_result Textinput(ui_state *UiState, i32 Id, irect Bounds,
				  textinput *Desc);

GAMEFN void Texture(ui_state *UiState, irect Bounds, texture Tex);

GAMEFN gui_key Key(key_id Key);
GAMEFN void SetHotkey(ui_state *UiState, i32 Id, gui_key Key);

#endif
