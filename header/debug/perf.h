#ifndef GUARD_DEBUG_PERF_H
#define GUARD_DEBUG_PERF_H

FWDDECLARE(perf_ts);

struct perf_ts
{
	u64 Clock;
	u64 Cycles;
	u32 CoreId;
};

#define CURRENT_TIME_STAMP(Name) perf_ts Name(void)
typedef CURRENT_TIME_STAMP(current_time_stamp);
GAMEFN CURRENT_TIME_STAMP(CurrentTimeStamp);

#endif /* GUARD_PERF_H */
