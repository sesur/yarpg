#ifndef GUARD_DEBUG_DEBUG_STATE
#define GUARD_DEBUG_DEBUG_STATE

FWDDECLARE(perf_event);
FWDDECLARE(stored_perf_event);
FWDDECLARE(debug_event);
FWDDECLARE(event_storage);
FWDDECLARE(debug_location);

typedef enum
{
	PerfEventType_BEGIN,
	PerfEventType_END,
} perf_event_type;

struct stored_perf_event
{
	debug_location const *Loc;

	// relative to the start time of the debug_state
	u32 DeltaClock;
	u32 DeltaCycle;

	u32 HitCount;
	u16 Padding_1;
	u8 Padding_2;
	u8 Type;

};

YSASSERT(sizeof(stored_perf_event) == 24);

struct debug_event
{
	stored_perf_event Perf;
};

struct event_storage
{
	perf_ts Start;
	idx Capacity;
	idx End;
	debug_event *Storage;
};

typedef enum
{
	DebugId_PLATFORM,
	DebugId_GAME,
	DebugId_COUNT
} debug_id;

struct debug_location
{
	char const *Name;
	char const *File;
	char const *Func;
	udx         Line;
	udx         Id;
	debug_id    FromWhere;
};

#define PUSH_PERF_EVENT(Name) bool Name(u32 HitCount, debug_location const *Loc, \
					perf_event_type Type)
typedef PUSH_PERF_EVENT(push_perf_event);
GAMEFN PUSH_PERF_EVENT(PushPerfEvent);

#ifdef PLATFORM
#define DEBUGID DebugId_PLATFORM
#else
#define DEBUGID DebugId_GAME
#endif

#define INSTRUMENT_COUNTED(BlockName, Count)				\
	static const debug_location MACROVAR(Source) =			\
	{								\
		.Name = #BlockName,					\
		.Func = __func__,					\
		.Line = __LINE__,					\
		.File = __FILE__,					\
		.Id   = __COUNTER__,					\
		.FromWhere = DEBUGID,					\
	};								\
	b32 MACROVAR(DidStart);						\
	DEFER(MACROVAR(DidStart) = PushPerfEvent((Count), &MACROVAR(Source), PerfEventType_BEGIN), \
	      MACROVAR(DidStart) && PushPerfEvent(0, &MACROVAR(Source), PerfEventType_END))

#define INSTRUMENT(Name) INSTRUMENT_COUNTED(Name, 1)

#endif /* GUARD_DEBUG_DEBUG_STATE */
