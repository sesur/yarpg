#ifndef GUARD_DEBUG_DEBUG_STATE_INTERNAL
#define GUARD_DEBUG_DEBUG_STATE_INTERNAL

#include <debug/perf.h>
#include <debug/debug_state.h>

FWDDECLARE(thread_debug_state);

struct thread_debug_state
{
	event_storage Events;

	bool volatile InUse;
};

GAMEFN thread_debug_state ThreadDebugState_FromBuffer(idx EventCapacity,
						      debug_event Storage[static EventCapacity]);

#endif /* GUARD_DEBUG_DEBUG_STATE_INTERNAL */
