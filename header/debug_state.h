#ifndef GUARD_DEBUG_STATE_H
#define GUARD_DEBUG_STATE_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <debug/perf.h>
#include <util/string.h>

FWDDECLARE(debug_state);
FWDDECLARE(debug_counter);
FWDDECLARE(debug_perf_source);
FWDDECLARE(debug_frame);
FWDDECLARE(debug_thread);
FWDDECLARE(debug_timing_block);
FWDDECLARE(debug_console);
FWDDECLARE(debug_output);

struct debug_console
{
	b32 Visible;
	f32 PercentShowing;

	u32 NumChars;
	str Input;
	str Output;
};

struct debug_counter
{
	u32 Count;
	f32 Min, Max, Avg;
};

#define NUM_GATHERED_DATA        240 // approx 1 seconds
#define MAX_NUM_INSTRUMENTATIONS 512

struct debug_perf_source
{
	char const *Name;
	char const *File;
	char const *Func;
	udx         Line;
	udx         Id;
};

struct debug_frame
{
	u64 StartTime;
	u64 Num;
};

struct debug_timing_block
{
	u64 StartTime;
	u64 EndTime;

	debug_timing_block *Prev;
	debug_timing_block *Parent;
	union
	{
		// last child is not really important during
		// iteration so we can overwrite it with the next free block
		// while freeing a node

		debug_timing_block *LastChild;
		debug_timing_block *NextFree;
	};

	u32 Id;
	u32 BlockId;

	u32 HitCount;
};

struct debug_thread
{
	// TODO: should have LeftOpen, RightOpen, Closed
	debug_timing_block *Open;
	debug_timing_block *Closed;
};

struct debug_output
{
	idx CurrentBuffer;
	idx NumBuffers;
	str *Buffers;
};

struct debug_state
{
	u8               volatile Lock;
	udx              volatile CurrentStorage;
	perf_event                EventStorage[NUM_PERF_STORAGES][MAX_PERF_EVENTS];
	perf_event     * volatile NextEvent;
	u32              volatile InternalBlockId;

	memory_arena Arena;
	udx CurrentData;

	udx NumInstrumentations;
	udx NumPlatform;

        debug_perf_source *Source[MAX_NUM_INSTRUMENTATIONS];
	i64 Times[MAX_NUM_INSTRUMENTATIONS];
	u64 Hits[MAX_NUM_INSTRUMENTATIONS];

	idx NumFrames;
	idx LastFrame;
	debug_frame *Frames; // ring buffer of size NumFrames

	idx                 NumThreads;
	debug_thread       *Threads;
	debug_timing_block *FreeBlock;

	b32 volatile AcceptData;
	u32 CurrentId;
	// viewing window of DoShowPerformance
	// FrameStart = 0 means that the newest (finished) frame is shown
	idx FirstFrameToShow;
	idx NumFramesToShow;
	b32 ByFunction;

	debug_console Console;

	debug_output Output;
};

#define GET_DEBUG_STATE(Memory) NEXTALIGNED(Memory, alignof(debug_state))

#ifndef PLATFORM
#define NEXTCOUNTER() __COUNTER__
#else
#define NEXTCOUNTER() __COUNTER__ + DebugState->NumInstrumentations
#endif

#ifdef DEBUG

#define INSTRUMENT_COUNTED(BlockName, Count)				\
	udx MACROVAR(Id) = NEXTCOUNTER();				\
	static debug_perf_source MACROVAR(Source) =			\
	{								\
		.Name = #BlockName,					\
		.Func = __func__,					\
		.Line = __LINE__,					\
		.File = __FILE__,					\
	};								\
	MACROVAR(Source).Id = MACROVAR(Id);				\
	InterlockedWriteEx(&DebugState->Source[MACROVAR(Id)], &MACROVAR(Source), __ATOMIC_RELAXED); \
	u32 MACROVAR(BlockId) = NextBlockId();				\
	b32 MACROVAR(DidStart);						\
	DEFER(MACROVAR(DidStart) = PushPerfEvent(MACROVAR(Id), (Count), MACROVAR(BlockId), PerfEventType_BEGIN, 0), \
	      MACROVAR(DidStart) && PushPerfEvent(MACROVAR(Id), 0, MACROVAR(BlockId), PerfEventType_END, MACROVAR(DidStart)))

#else

#define INSTRUMENT_COUNTED(...)

#endif

#define INSTRUMENT(Name) INSTRUMENT_COUNTED(Name, 1)
#define FRAME_END(FrameCount) PushPerfEvent(NEXTCOUNTER(), (FrameCount), U32C(-1), PerfEventType_FRAME, 1)


#endif /* GUARD_DEBUG_STATE_H */
