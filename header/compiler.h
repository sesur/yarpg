#ifndef GUARD_COMPILER_H
#define GUARD_COMPILER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

// for atomics see: https://gcc.gnu.org/onlinedocs/gcc-4.4.3/gcc/Atomic-Builtins.html#Atomic-Builtins
#define COMPILERBARRIER() do { asm volatile("" : : : "memory"); } while (0)

enum exchange_type
{
	EXCHANGE_STRONG = 0,
	EXCHANGE_WEAK = 1,
};

#define InterlockedExchangeEx(Ptr, New, Order) __atomic_exchange_n((Ptr), (New), (Order))
#define InterlockedExchange(Ptr, New) InterlockedExchangeEx((Ptr), (New), __ATOMIC_ACQ_REL)

#define InterlockedCompareSwapEx(Ptr, Type, Expected, Desired,		\
				 Succ, Fail)				\
	({								\
		typeof(Expected) Tmp = (Expected);			\
		__atomic_compare_exchange_n((Ptr), &Tmp, (Desired),	\
					    (Type),			\
					    (Succ),			\
					    (Fail));			\
	})

#define InterlockedCompareSwap(Ptr, Expected, Desired)			\
	InterlockedCompareSwapEx((Ptr), EXCHANGE_STRONG,		\
				 (Expected), (Desired),			\
				 __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE)

#define InterlockedCompareExchangeEx(Ptr, Type, Expected, Desired,	\
				     Succ, Fail)			\
	__atomic_compare_exchange_n((Ptr), (Expected), (Desired),	\
				    (Type),				\
				    (Succ), (Fail))			\

#define InterlockedCompareExchange(Ptr, Type, Expected, Desired)	\
	InterlockedCompareExchangeEx((Ptr), (Type),			\
				     (Expected), (Desired),		\
				     __ATOMIC_ACQ_REL,			\
				     __ATOMIC_ACQUIRE)			\

#define InterlockedWeakCompareExchange(Ptr, Expected, Desired)		\
	InterlockedCompareExchangeEx((Ptr), EXCHANGE_WEAK,		\
				     (Expected), (Desired),		\
				     __ATOMIC_ACQ_REL,			\
				     __ATOMIC_ACQUIRE)			\

#define InterlockedStrongCompareExchange(Ptr, Expected, Desired)	\
	InterlockedCompareExchangeEx((Ptr), EXCHANGE_STRONG,		\
				     (Expected), (Desired),		\
				     __ATOMIC_ACQ_REL,			\
				     __ATOMIC_ACQUIRE)			\



#define InterlockedPostAddEx(Ptr, Val, Order) __atomic_fetch_add((Ptr), (Val), (Order))
#define InterlockedPreAddEx(Ptr, Val, Order) __atomic_add_fetch((Ptr), (Val), (Order))

#define InterlockedPostSubEx(Ptr, Val, Order) __atomic_fetch_sub((Ptr), (Val), (Order))
#define InterlockedPreSubEx(Ptr, Val, Order) __atomic_sub_fetch((Ptr), (Val), (Order))

#define InterlockedPostAdd(Ptr, Val) InterlockedPostAddEx((Ptr), (Val), __ATOMIC_ACQ_REL)
#define InterlockedPreAdd(Ptr, Val) InterlockedPreAddEx((Ptr), (Val),  __ATOMIC_ACQ_REL)

#define InterlockedPostSub(Ptr, Val) InterlockedPostSubEx((Ptr), (Val), __ATOMIC_ACQ_REL)
#define InterlockedPreSub(Ptr, Val) InterlockedPreSubEx((Ptr), (Val), __ATOMIC_ACQ_REL)

#define InterlockedPostInc(Ptr) InterlockedPostAdd((Ptr), 1)
#define InterlockedPreInc(Ptr) InterlockedPreAdd((Ptr), 1)

#define InterlockedPostDec(Ptr) InterlockedPostSub((Ptr), 1)
#define InterlockedPreDec(Ptr) InterlockedPreSub((Ptr), 1)

#define InterlockedInc(Ptr) ((void) InterlockedPreInc(Ptr))
#define InterlockedDec(Ptr) ((void) InterlockedPreDec(Ptr))

#define InterlockedReadEx(Ptr, Order)             __atomic_load_n((Ptr), (Order))
#define InterlockedWriteEx(Ptr, Val, Order)       __atomic_store_n((Ptr), (Val), (Order))
#define InterlockedCopyFromEx(Ptr, ValPtr, Order) __atomic_load((Ptr), (PtrVal), (Order))
#define InterlockedCopyIntoEx(Ptr, ValPtr, Order) __atomic_store((Ptr), (PtrVal), (Order))

#define InterlockedRead(Ptr)             InterlockedReadEx((Ptr), __ATOMIC_ACQUIRE)
#define InterlockedWrite(Ptr, Val)       InterlockedWriteEx((Ptr), (Val), __ATOMIC_RELEASE)
#define InterlockedCopyFrom(Ptr, ValPtr) InterlockedCopyFromEx((Ptr), (PtrVal), __ATOMIC_AQUIRE)
#define InterlockedCopyInto(Ptr, ValPtr) InterlockedCopyIntoEx((Ptr), (PtrVal), __ATOMIC_RELEASE)

#define InterlockedAcquire(Ptr)					\
	({							\
		YSASSERT(sizeof(*Ptr) == 1);			\
		!__atomic_test_and_set(Ptr, __ATOMIC_ACQ_REL);	\
	})
#define InterlockedRelease(Ptr) __atomic_clear(Ptr, __ATOMIC_RELEASE)

#define InterlockedAcquireEx(Ptr, Order) __atomic_test_and_set(Ptr, Order)
#define InterlockedReleaseEx(Ptr, Order) __atomic_clear(Ptr, Order)

#define LoadFence() __builtin_ia32_lfence()
#define StoreFence() __builtin_ia32_sfence()
#define MemFence() __builtin_ia32_mfence()
#define MemoryFence(Order) __atomic_thread_fence(Order)

#define LeadingZeroes(X) _Generic((ReadBits(X)),			\
				  unsigned: __builtin_clz,		\
				  unsigned long: __builtin_clzl,	\
				  unsigned long long: __builtin_clzll)(ReadBits(X))

#define TrailingZeroes(X) _Generic((ReadBits(X)),			\
				   unsigned: __builtin_ctz,		\
				   unsigned long: __builtin_ctzl,	\
				   unsigned long long: __builtin_ctzll)(ReadBits(X))

#define FirstSet(X) _Generic((ReadBits(X)),				\
			     unsigned: __builtin_ffs,			\
			     unsigned long: __builtin_ffsl,		\
			     unsigned long long: __builtin_ffsll)(ReadBits(X))

#define PopCount(X) _Generic((ReadBits(X)),				\
			     unsigned: __builtin_popcount,		\
			     unsigned long: __builtin_popcountl,	\
			     unsigned long long: __builtin_popcountll)(ReadBits(X))

#define EXPECT(Expr, Result) __builtin_expect((Expr), (Result))
#define LIKELY(Expr) !EXPECT(!(Expr), 0)
#define UNLIKELY(Expr) EXPECT((Expr), 0)

#define SIGNALINGNAN SNANF

#endif
