#ifndef GUARD_TRANSIENT_H
#define GUARD_TRANSIENT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#include <opengl/string.h>
#include <opengl/imgui.h>

#include <util/image.h>
#include <util/asset.h>
#include <sound.h>

#include <game/debug_info.h>


FWDDECLARE(transient_state);
FWDDECLARE(playing_sound);
FWDDECLARE(hit_cycle);

struct playing_sound
{
        sound_id Id;
	playing_sound *Next;

	f32 Volume;

	f32 Speed;
	f32 SecondsElapsed;
};

#define NUM_PERFORMANCE_DATA 50

struct transient_state
{
        memory_arena Arena;
	memory_arena Scratch;

	ui_state Gui;

	asset_store Assets;

	renderer Renderer;

	debug_info *DebugInfo;

	playing_sound *Sounds;
	playing_sound *FreeSound;
};

#endif
