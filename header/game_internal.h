#ifndef GUARD_GAME_INTERNAL_H
#define GUARD_GAME_INTERNAL_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <game.h>

#include <util/arena_allocator.h>
#include <renderer/renderer.h>

#include <opengl/string.h>
#include <opengl/imgui.h>

#include <game/world.h>
#include <game/battlefield.h>

#include <transient.h>

#include <entity_test.h>

////////////////////////////////////////////////////////////////////////////////
/// GAME INTERNAL

FWDDECLARE(game);
FWDDECLARE(game_world);
FWDDECLARE(game_battlefield);
FWDDECLARE(action);
FWDDECLARE(map_gen);



////////////////////////////////////////////////////////////////////////////////
/// WORLD

struct game_world
{
	world World;
	stored_world_index HeroIndex;
	i32 PlayerGold;
	world_input_state InputState;

	b32 MenuOpen;
	b32 ShouldQuit;

	b32 ShowDetails;
	stored_world_index DetailsIndex;
};

////////////////////////////////////////////////////////////////////////////////
/// BATTLEFIELD

typedef enum
{
	DraggingState_NOTACTIVE,
	DraggingState_DRAGGING,
} dragging_state;

typedef enum
{
	SelectedBattlefieldAction_NOACTION,
	SelectedBattlefieldAction_MOVEMENT,
	SelectedBattlefieldAction_ATTACK,
} selected_battlefield_action;

struct action
{
	enum { ActionType_NOACTION,
	       ActionType_ATTACK,
	       ActionType_MOVEMENT } Type;

	union {
		struct { iv2 MoveTo; i32 TargetId; } Attack;
		struct { iv2 From; f32 MovCost; } Movement;
	} Data;
};

struct game_battlefield
{
	stored_world_index AttackerIndex;
	stored_world_index DefenderIndex;

	i32 TurnCount;
	iv2 MousePos;

	i32 HoveredEntity;

	i32 SelectedEntity;
	selected_battlefield_action CurrentAction;

	dragging_state DraggingState;
	v2 OldCursorPos;

	sprite_id Head, Body;
	sprite_id Bow, Fist;

	sprite_id Arrow;

	i32 CurrentEntity;
	i32 *TurnOrder;

	action *Actions;

	battlefield Field;

	b32 Closed;
};

FWDDECLARE(map_gen_param);

struct map_gen_param
{
	i32 Octaves;
	i32 OneOverFreq;
	f32 Ampl;
	f32 Lacu;
	f32 Pers;
};

#define MapParams(Macro)			\
	Macro(ELEVATION)			\
	Macro(MOISTURE)				\

#define MapParamEnum(Name) MapParam_ ## Name,

typedef enum
{
	MapParams(MapParamEnum)
	MapParam_COUNT
} map_param;

#undef MapParamEnum

#define MapParamName(Name) [MapParam_ ## Name] = #Name,

static char const *MapParamName[] =
{
	MapParams(MapParamName)
};
#undef MapParamName

typedef enum
{
	BaseMapType_CIRCLE,
	BaseMapType_RECT,
} base_map_type;

/* FWDDECLARE(map_gen_rect); */
/* FWDDECLARE(map_gen_circle); */

/* struct map_gen_rect */
/* { */
/* 	iv2 Dim; */
/* }; */

/* struct map_gen_circle */
/* { */
/* 	i32 Radius; */
/* }; */

struct map_gen
{
	base_map_type Type;

	union
	{
		iv2 Dim;
		i32 Radius;
	};

	u64 Seed;
	idx Selected;
	b32 HeatMap;
	map_gen_param Params[MapParam_COUNT];

	f32 HexRad;
};



////////////////////////////////////////////////////////////////////////////////
/// GAME

struct game
{
	memory_arena Debug;
	memory_arena Arena;
	v2 CameraVel;
	i32 CursorX, CursorY;
	f32 ZoomFactor;
	f32 CameraMatrix[4][4];

	b32 ShowMemoryWindow;
	irect MemoryWindowBounds;
	b32 ShowCameraPosition;
	irect CameraWindowBounds;
	b32 ShowPerformance;
	irect PerformanceWindowBounds;
	b32 ShowFPS;
	irect FPSBounds;
	b32 ShowTest;
	irect TestBounds;
	b32 ShowRenderGroupInfo;
	irect RenderGroupInfoBounds;
	b32 ShowRendererInfo;
	irect RendererInfoBounds;
	b32 ShowMusic;
	irect MusicWindowBounds;

	b32 ShowMapEdit;
	b32 ShowDbgMsg;
	irect DbgMsgBounds;

	b32 ShowUiDebug;

	b32   ShowFontViewer;
	irect FontViewerBounds;
	i32   FontViewerInputCount;
	char  FontViewerInput[12];

	irect WindowBounds;

	f32 DPS;

	u64 FrameCounter;

#define NUM_PERFORMANCE_DATA 50

	f32 PQueueFillGraph[NUM_PERFORMANCE_DATA];
	f32 BQueueFillGraph[NUM_PERFORMANCE_DATA];

	b32 TranStateInitialized;

	glyph_table Table;

	opengl_backend Backend;

	i32 LastRenderGroupSize;

	b32 RenderMap;
	b32              ShowBattle;
	game_world       Map;
	game_battlefield Battle;

	f32 Clock;

	entity_test T;

	map_gen Options;
};

enum WINDOW_ID
{
	INVALID_WINDOW_ID = 0,
	BATTLEFIELD_MAIN_UI_ID,
	BATTLEFIELD_END_BATTLE_UI_ID,
	WORLD_DETAILS_UI_ID,
	CONSOLE_WINDOW_ID,
	MEMORY_WINDOW_ID,
	CAMERA_WINDOW_ID,
	MUSIC_WINDOW_ID,
	PERFORMANCE_WINDOW_ID,
	FONT_WINDOW_ID,
	FPS_WINDOW_ID,
	TEST_WINDOW_ID,
	RENDER_GROUP_INFO_WINDOW_ID,
	RENDERER_INFO_WINDOW_ID,
	WORLD_UI_ID,
	MAIN_MENU_UI_ID,
	CITY_UI_ID,
	ARMY_UI_ID,
	UI_DEBUG_ID,
	MAP_EDIT_WINDOW_ID,
	MAP_DBG_MSG_ID,
};

PLATFORMFN GAME_INIT(GameInit);
PLATFORMFN GAME_UPDATE_AND_RENDER(GameUpdateAndRender);
PLATFORMFN GAME_RELEASE(GameRelease);
PLATFORMFN GAME_PLAY_SOUND(GamePlaySound);
PLATFORMFN GAME_COLLATE_DEBUG_EVENTS(GAME_COLLATE_DEBUG_EVENTS);

#endif
