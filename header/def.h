#ifndef GUARD_DEF_H
#define GUARD_DEF_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#define _GNU_SOURCE

#include <stdint.h>
#include <stddef.h>
#include <stdalign.h>
#include <stdarg.h>

#include <limits.h>
#include <float.h>
#include <defines.h>
#include <iso646.h> // and, or, xor, etc.

#define DO_PRAGMA(x) _Pragma (#x)
#define TODO(x) DO_PRAGMA(message ("TODO - " #x))

#define SYMBOL(X) #X
#define STRING(X) SYMBOL(X)
#define MACROVAR_CONCAT_IMPL(A, B) A ## _MACRO_VAR_ ## B
#define MACROVAR_CONCAT(A, B) MACROVAR_CONCAT_IMPL(A, B)
#define MACROVAR(Name) MACROVAR_CONCAT(Name, __LINE__)
#if 1
#define DEFER_DEBUG ({YASSERT(MACROVAR(Idx) <= 1); (void) 0;}),
#else
#define DEFER_DEBUG
#endif
#define DEFER(Start, End) for (				\
		idx MACROVAR(Idx) = ((Start), 0);	\
		!MACROVAR(Idx);				\
		(MACROVAR(Idx) += 1, DEFER_DEBUG (End)))

#define ATEND(End) DEFER((void)0, (End))

#if !defined(NO_DEFAULT_INLINE)
#define GAMEFN static inline /* used in a component internally */
#define PRIVATE static inline /* used in a file internally */
#else
#define GAMEFN static
#define PRIVATE static
#endif
#define PLATFORMFN static /* used functions between components */
#define EXPORT __attribute__((visibility ("default"))) /* used as part of the public API */

// replace with [[reproducible]], [[unsequenced]] when possible

#if __has_c_attribute(reproducible)
#define PUREFN [[reproducible]]
#else
#define PUREFN __attribute__((pure))
#endif

#if __has_c_attribute(unsequenced)
#define CONSTFN [[unsequenced]]
#else
#define CONSTFN __attribute__((const))
#endif

#if __has_c_attribute(deprecated)
#define DEPRECATED [[deprecated]]
#else
#define DEPRECATED __attribute__((deprecated))
#endif

#if defined(DEBUG)
#define NOTIMPLEMENTED() __builtin_unreachable()
#else
#define NOTIMPLEMENTED() DO_PRAGMA(error ("Not implemented!"))
#endif
#include <debug.h>

#if !defined(thread_local)
#define thread_local _Thread_local
#endif

#define FWDDECLARE(Type) struct Type; typedef struct Type Type
#define ISPOWOFTWO(Num) (((Num) != 0) && ((Num) & ((Num)-1)) == 0)

#define UTF8(Str) u8 ## Str
#define UTF16(Str) u ## Str
#define UTF32(Str) U ## Str

// memory stuff

typedef uintptr_t     ptr;
typedef char unsigned byte;
typedef ptrdiff_t     idx; /* signed     index */
typedef size_t        udx; /* _u_nsigned index */

#define IDX_MAX PTRDIFF_MAX
#define IDX_MIN PTRDIFF_MIN
#define IDXC(X) ((ptrdiff_t) (X))

#define UDX_MAX SIZE_MAX
#define UDX_MIN ((udx) 0)
#define UDXC(X) ((size_t) (X))

#define PTROFFSET(Ptr, Offset) (typeof(Ptr)) ((ptr) (Ptr) + (ptr) (Offset))
#define NEXTALIGNED(Ptr, Alignment) ({					\
			YASSERTF(ISPOWOFTWO(Alignment), "%ld", (idx) Alignment); \
			(typeof(Ptr)) (((ptr) (Ptr) + (Alignment) - 1)	\
				       & ~((ptr)(Alignment) - 1)); \
		})
#define PREVALIGNED(Ptr, Alignment) ({					\
			YASSERTF(ISPOWOFTWO(Alignment), "%ld", (idx) Alignment); \
			(typeof(Ptr)) (((ptr) (Ptr))		\
				       & ~((ptr)(Alignment) - 1)); \
		})

#define ARRAYCOUNT(arr) _Generic(&(arr), typeof(*(arr))(*)[]: sizeof(arr) / sizeof(arr)[0])

#define MEMBER(Struct, Mem) (((typeof(Struct) *) 0)->Mem)

#define CONTAINEROF(Ptr, Struct, Member) ((typeof(Struct) *) PTROFFSET((byte *) Ptr, -offsetof(Struct, Member)))

// unsigned int
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define U64C(X) UINT64_C(X)
#define U32C(X) UINT32_C(X)
#define U16C(X) UINT16_C(X)
#define U8C(X)  UINT8_C(X)

#define U64_MAX UINT64_MAX
#define U32_MAX UINT32_MAX
#define U16_MAX UINT16_MAX
#define U8_MAX  UINT8_MAX

#define U64_MIN U64C(0)
#define U32_MIN U32C(0)
#define U16_MIN U16C(0)
#define U8_MIN  U8C(0)

// signed int
typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

#define I64C(X) INT64_C(X)
#define I32C(X) INT32_C(X)
#define I16C(X) INT16_C(X)
#define I8C(X)  INT8_C(X)

#define I64_MAX INT64_MAX
#define I32_MAX INT32_MAX
#define I16_MAX INT16_MAX
#define I8_MAX  INT8_MAX

#define I64_MIN INT64_MIN
#define I32_MIN INT32_MIN
#define I16_MIN INT16_MIN
#define I8_MIN  INT8_MIN

// bool
typedef i8    b8;
typedef i16   b16;
typedef i32   b32;
typedef i64   b64;

// TODO: only use this bool ?
typedef _Bool bool;

#define YARPG_FALSE 0
#define YARPG_TRUE !YARPG_FALSE

// "errors"
typedef b8    e8;
typedef b16   e16;
typedef b32   e32;
typedef b64   e64;

#define YARPG_SUCCESS 0
#define YARPG_ERROR   -1

// float
typedef float    f32;
typedef double   f64;

YSASSERT(sizeof(f32) == 4 /* * 8 = 32 */);
YSASSERT(sizeof(f64) == 8 /* * 8 = 64 */);

#define F64_MAX DBL_MAX
#define F32_MAX FLT_MAX

#define F64_MIN DBL_MIN
#define F32_MIN FLT_MIN

#define F64C(X) (X)
#define F32C(X) (X ## f)

union signed_vector_2
{
	struct { i32 X, Y; };
	struct { i32 Width, Height; };
	i32 AsArray[2];
};

union signed_vector_3
{
	struct { i32 X, Y, Z; };
	i32 AsArray[3];
};

union signed_vector_4
{
	struct { i32 X, Y, Z, W; };
	i32 AsArray[4];
};

typedef union signed_vector_2 iv2;
typedef union signed_vector_3 iv3;
typedef union signed_vector_4 iv4;
#define IV2(ArgX, ArgY) ((iv2) {		\
			.X = (i32) (ArgX),	\
			.Y = (i32) (ArgY)	\
		})
#define IV3(ArgX, ArgY, ArgZ) ((iv3) {		\
			.X = (i32) (ArgX),	\
			.Y = (i32) (ArgY),	\
			.Z = (i32) (ArgZ)	\
		})
#define IV4(ArgX, ArgY, ArgZ, ArgW) ((iv4) {	\
			.X = (i32) (ArgX),	\
			.Y = (i32) (ArgY),	\
			.Z = (i32) (ArgZ),	\
			.W = (i32) (ArgW)	\
		})
#define IV_DISPATCH(_1,_2,_3,_4, Name, ...) Name
#define IV(...) IV_DISPATCH(__VA_ARGS__, IV4, IV3, IV2)(__VA_ARGS__)

union unsigned_vector_2
{
	struct { u32 X, Y; };
	u32 AsArray[2];
};

union unsigned_vector_3
{
	struct { u32 X, Y, Z; };
	u32 AsArray[3];
};

union unsigned_vector_4
{
	struct { u32 X, Y, Z, W; };
	u32 AsArray[4];
};

typedef union unsigned_vector_2 uv2;
typedef union unsigned_vector_3 uv3;
typedef union unsigned_vector_4 uv4;

#define UV2(ArgX, ArgY) ((uv2) {		\
			.X = (u32) (ArgX),	\
			.Y = (u32) (ArgY)	\
		})
#define UV3(ArgX, ArgY, ArgZ) ((uv3) {		\
			.X = (u32) (ArgX),	\
			.Y = (u32) (ArgY),	\
			.Z = (u32) (ArgZ)	\
		})
#define UV4(ArgX, ArgY, ArgZ, ArgW) ((uv4) {	\
			.X = (u32) (ArgX),	\
			.Y = (u32) (ArgY),	\
			.Z = (u32) (ArgZ),	\
			.W = (u32) (ArgW)	\
		})
#define UV_DISPATCH(_1,_2,_3,_4, Name, ...) Name
#define UV(...) UV_DISPATCH(__VA_ARGS__, UV4, UV3, UV2)(__VA_ARGS__)

union vector_2
{
	struct { f32 X, Y; };
	f32 AsArray[2];
};

union vector_3
{
	struct { f32 X; f32 Y; f32 Z; };
	struct { f32 R; f32 G; f32 B; };
	f32 AsArray[3];
};

union vector_4
{
	struct { f32 X, Y, Z, W; };
	struct { f32 R, G, B, A; };
	struct { f32 H, S, L; };
	f32 AsArray[4];
};

typedef union vector_2 v2;
typedef union vector_3 v3;
typedef union vector_4 v4;

FWDDECLARE(rect);

struct rect
{
	union {
		v2 TopRight;
		v2 Max;
                struct {
			f32 MaxX, MaxY;
		};
	};
	union {
		v2 BottomLeft;
		v2 Min;
		struct {
			f32 MinX, MinY;
		};
	};
};

FWDDECLARE(irect);
struct irect
{
	union {
		iv2 TopRight;
                struct {
			i32 MaxX, MaxY;
		};
	};
	union {
		iv2 BottomLeft;
		struct {
			i32 MinX, MinY;
		};
	};
};

#define V2(ArgX, ArgY) ((v2) {			\
			.X = (f32) (ArgX),	\
			.Y = (f32) (ArgY)	\
		})
#define V3(ArgX, ArgY, ArgZ) ((v3) {		\
			.X = (f32) (ArgX),	\
			.Y = (f32) (ArgY),	\
			.Z = (f32) (ArgZ)	\
		})
#define V4(ArgX, ArgY, ArgZ, ArgW) ((v4) {	\
			.X = (f32) (ArgX),	\
			.Y = (f32) (ArgY),	\
			.Z = (f32) (ArgZ),	\
			.W = (f32) (ArgW)	\
		})
#define V_DISPATCH(_1,_2,_3,_4, Name, ...) Name
#define V(...) V_DISPATCH(__VA_ARGS__, V4, V3, V2)(__VA_ARGS__)

#define kilo_bytes(value) ((value) * 1024)                       // fits into u32
#define mega_bytes(value) (kilo_bytes(value) * 1024)             // fits into u32
#define giga_bytes(value) (mega_bytes((u64)(value)) * 1024)   // might not fit into u32,
                                                                 // so we cast it to u64
#define tera_bytes(value) (giga_bytes((u64)(value)) * 1024)

#define KB * ((u64) 1024)
#define MB * ((u64) 1024 KB)
#define GB * ((u64) 1024 MB)
#define TB * ((u64) 1024 GB)

#define NSEC * (U64C(1))
#define USEC * (U64C(1000) NSEC)
#define MSEC * (U64C(1000) USEC)
#define SEC  * (U64C(1000) MSEC)
#define MIN  * (U64C(60) SEC)
#define HOUR * (U64C(60) MIN)

#include <intrinsics.h>

GAMEFN rect RectOfMWH(v2 Middle, f32 Width, f32 Height);
GAMEFN rect RectOfTB(v2 TopLeft, v2 BottomRight);
GAMEFN rect RectOfBounds(f32 Bottom, f32 Top, f32 Left, f32 Right);

#ifdef DEBUG
#define DEBUG_DEFINE(Type, Name) Type Name
#define DEBUG_DO(Body) do { Body } while (0)
#else
#define DEBUG_DEFINE(Type, Name)
#define DEBUG_DO(Body)
#endif

FWDDECLARE(basis_system);

struct basis_system
{
	v3 Origin;
	v2 XAxis;
	v2 YAxis;
};

GAMEFN b32
Between(f32 Lower, f32 Check, f32 Upper)
{
	return (Lower <= Check) && (Check <= Upper);
}

#define ReadBits(A) _Generic((A),			\
			     f32: ReadBitsF32,		\
			     i32: ReadBitsI32,		\
			     u32: ReadBitsU32,		\
			     f64: ReadBitsF64,		\
			     i64: ReadBitsI64,		\
			     u64: ReadBitsU64)(A)

GAMEFN u32 ReadBitsF32(f32 X);
GAMEFN u32 ReadBitsI32(i32 X);
GAMEFN u32 ReadBitsU32(u32 X);

GAMEFN u64 ReadBitsF64(f64 X);
GAMEFN u64 ReadBitsI64(i64 X);
GAMEFN u64 ReadBitsU64(u64 X);

GAMEFN basis_system BasisOfRect(rect R);

GAMEFN CONSTFN u32 HashU32(u32 X);

#define Hash(X) _Generic((X),			\
			 u32: HashU32)(X)


FWDDECLARE(range);

struct range
{
	idx Begin;
	idx End;
};

#define R(B, E) ((range) { .Begin = (B), .End = (E) })
#define RangeLength(R) ((R).End - (R).Begin)

// flags
GAMEFN b32 IsSet(u32 Flags, u32 Flag);
GAMEFN void SetFlag(u32 *Flags, u32 Flag);
GAMEFN void ClearFlag(u32 *Flags, u32 Flag);

#define MAXOF(X) _Generic((X),			\
			  u8: U8_MAX,		\
			  i8: I8_MAX,		\
			  u16: U16_MAX,		\
			  i16: I16_MAX,		\
			  u32: U32_MAX,		\
			  i64: I64_MAX,		\
			  i32: I32_MAX,		\
			  f64: F64_MAX,		\
			  f32: F32_MAX,		\
			  u64: U64_MAX)

#define MINOF(X) _Generic((X),			\
			  u8: U8_MIN,		\
			  i8: I8_MIN,		\
			  u16: U16_MIN,		\
			  i16: I16_MIN,		\
			  u32: U32_MIN,		\
			  i32: I32_MIN,		\
			  i64: I64_MIN,		\
			  f64: F64_MIN,		\
			  f32: F32_MIN,		\
			  u64: U64_MIN)

GAMEFN rect RectMinMax(v2 Min, v2 Max);
GAMEFN rect RectCenterHalfDim(v2 Center, v2 HalfDim);
GAMEFN rect RectCenterDim(v2 Center, v2 Dim);
GAMEFN rect RectMinDim(v2 Min, v2 Dim);

GAMEFN idx ModInc(idx Num, idx Modulus);
GAMEFN idx ModDec(idx Num, idx Modulus);

typedef union
{
	struct {
		u8 X, Y, Z, W;
	};
	struct {
		u8 R, G, B, A;
	};
	u8 AsArray[4];
	u32 AsU32;
} pixel;

#define LOOP(Name, Num) for (typeof(N) Name = 0;	\
			     Name < (N);		\
			     ++Name)
#define REPEAT(N) for (typeof(N) MACROVAR(Idx) = 0;	\
		       MACROVAR(Idx) < (N);		\
		       ++MACROVAR(Idx))

#endif
