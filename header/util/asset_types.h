#ifndef GUARD_UTIL_ASSET_TYPES_H
#define GUARD_UTIL_ASSET_TYPES_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <file.h>
#include <sound.h>

#include <yaff_format.h>
#include <util/asset_ids.h>

FWDDECLARE(asset_info);
FWDDECLARE(category_info);
FWDDECLARE(sprite_info);
FWDDECLARE(text_info);
FWDDECLARE(sound_info);
FWDDECLARE(asset_tag);
FWDDECLARE(asset_location);
FWDDECLARE(sprite_id);
FWDDECLARE(text_id);
FWDDECLARE(sound_id);
FWDDECLARE(tag_info);
FWDDECLARE(asset_header);

#define AssetID(Id) ((asset_id) { .ToIdx = (Id) })
#define SpriteID(Id) ((sprite_id) { .ToIdx = (Id) })
#define TextID(Id) ((text_id) { .ToIdx = (Id) })
#define SoundID(Id) ((sound_id) { .ToIdx = (Id) })

#define INVALID_TEXT_ID    TextID(-1)
#define INVALID_SPRITE_ID SpriteID(-1)
#define INVALID_SOUND_ID   SoundID(-1)
#define INVALID_ASSET_ID ((asset_id){ .Sprite = INVALID_SPRITE_ID })

#define ValidID(Id) ((Id).ToIdx != -1)

struct sprite_id
{
	idx ToIdx;
};

struct text_id
{
	idx ToIdx;
};

struct sound_id
{
	idx ToIdx;
};

typedef union
{
	sound_id   Sound;
	sprite_id Sprite;
	text_id    Text;
	idx        ToIdx;
} asset_id;



typedef enum
{
	AssetStatus_ZERO,
	AssetStatus_QUEUED,
	AssetStatus_LOADED,
} asset_status;

struct category_info
{
	range Assets[AssetType_COUNT];
};

struct asset_location
{
	idx Start, End;
	idx FileId;
};

struct asset_info
{
	asset_location Location;
	asset_status volatile Status;
	asset_header *Header;
	range Tags;
};

struct sprite_info
{
	i64 Width, Height;
};

struct text_info
{
	i64 NumBytes, NumChars;
};

struct sound_info
{
	b32 HasNext;
	i32 NumChannels;
	i32 SampleRate;
	i32 NumSamples;
};

struct asset_tag
{
	i32 Id;
	f32 Value;
};

typedef enum
{
	AssetFlag_INUSE  = (1 << 0),
	AssetFlag_LOCKED = (1 << 1),
} asset_flags;

struct asset_header
{
	union {
		f32   *Samples;
		pixel *Pixels;
		char  *Text;
		byte  *Raw;
	} Data;
	asset_header *LRUPrev, *LRUNext;
	idx          Size;
	asset_type   Type;
	asset_id     Id;
	u32          Flags;
};

struct tag_info
{
	// If Period > 0 then every value will be equivalent modulo Period;
	// i.e. Value ~ Value + n * Period for every n in Z
	// For scoring purposes the representant that minimises the score
	// will be chosen (small score = good)
	f32 Period;
};

#endif
