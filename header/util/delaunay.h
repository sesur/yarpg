#ifndef GUARD_UTIL_DELAUNAY_H
#define GUARD_UTIL_DELAUNAY_H

#include <def.h>
#include <entity_test.h>

FWDDECLARE(edge_allocator);
FWDDECLARE(quad_edge);
FWDDECLARE(edge_record);
FWDDECLARE(edge_pair);

struct edge_record
{
	edge_record *Next;
	edge_record *Rot;
	i32 Origin;
};

struct edge_pair
{
	edge_record *Refs[2];
};

struct edge_allocator
{
	quad_edge *Base;
	i32 FirstFree, LastFree;
	i32 Begin, End;
	i32 FirstUnallocated;
};

struct quad_edge
{
	// a quad edge is not used
	// if Edges[0].Next == NULL.
	// In that case Edges[0].Origin is the next free edge index!
	edge_record Edges[4];
};

GAMEFN edge_allocator ComputeDelaunayTriangulation(memory_arena *Scratch,
						   idx NumPoints,
						   v2 Points[static NumPoints]);
GAMEFN void ComputeLloydStep(memory_arena *Scratch, rect BoundingBox,
			    idx NumPoints, v2 Points[static restrict NumPoints],
			    v2 Updated[static restrict NumPoints],
			    idx NumEdges,
			    quad_edge QEdges[static NumEdges]);

#endif /* GUARD_UTIL_DELAUNAY_H */
