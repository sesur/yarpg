#ifndef GUARD_UTIL_MATH_H
#define GUARD_UTIL_MATH_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/vector.h>

// TODO: remove this
#define Round(X) _Generic((X),			\
			  f32: Round_f32,	\
			  v2:  Round_v2)(X)

#define ToInt(X) _Generic((X),			\
			  f32: ToInt_f32,	\
			  v2:  ToInt_v2)(X)

#define ToFloat(X) _Generic((X),			\
			    i32: ToFloat_i32,		\
			    iv2: ToFloat_iv2,		\
			    iv3: ToFloat_iv3,		\
			    irect: ToFloat_irect)(X)

#define Clamp(Min, X, Max) _Generic((X),			\
				    u64: ClampU64,		\
				    i64: ClampI64,		\
				    i32: ClampI32,		\
				    f32: ClampF32)(Min, X, Max)

#define Abs(X) _Generic((X),			\
			i32: AbsI32,		\
			f32: AbsF32)(X)

#define Min(A, B) _Generic((A),			\
			   u64: MinU64,		\
			   u32: MinU32,		\
			   idx: MinIdx,		\
			   f32: MinF32,		\
			   i32: MinI32)(A, B)

#define Max(A, B) _Generic((A),			\
			   u64: MaxU64,		\
			   u32: MaxU32,		\
			   idx: MaxIdx,		\
			   f32: MaxF32,		\
			   i32: MaxI32)(A, B)

#define Square(X) _Generic((X),			\
			   f32: SquareF32)(X)

#define Sqrt(X) _Generic((X),			\
			 f32: SqrtF32)(X)

#define RSqrt(X) _Generic((X),			\
			  f32: RSqrtF32)(X)

#define Cos(X) _Generic((X),			\
			 f32: CosF32)(X)

#define ArcCos(X) _Generic((X),			\
			   f32: ArcCosF32)(X)

#define Sin(X) _Generic((X),			\
			f32: SinF32)(X)

#define RoundZero(X) _Generic((X),			\
			      f32: RoundZeroF32)(X)
#define RoundDown(X) _Generic((X),			\
			      f32: RoundDownF32,	\
			      v2: RoundDownV2)(X)
#define RoundUp(X) _Generic((X),		\
			    f32: RoundUpF32)(X)
#define RoundNearest(X) _Generic((X),				\
				 f32: RoundNearestF32)(X)

#define Fractional(X) _Generic((X),			\
			       f32: FractionalF32)(X)

#define Ceil2(X) _Generic((X),			\
			  u64: Ceil2U64,	\
			  u32: Ceil2U32)(X)

GAMEFN u32 Ceil2U32(u32 Val);
GAMEFN u64 Ceil2U64(u64 Val);

GAMEFN i32 Round_f32(f32 Val);
GAMEFN iv2 Round_v2(v2 Val);

GAMEFN i32 ToInt_f32(f32 Val);
GAMEFN iv2 ToInt_v2(v2 Val);

GAMEFN f32 ToFloat_i32(i32 Val);
GAMEFN v2  ToFloat_iv2(iv2 Val);
GAMEFN v3  ToFloat_iv3(iv3 Val);
GAMEFN rect ToFloat_irect(irect Val);

GAMEFN i32 ClampI32(i32 Min, i32 X, i32 Max);
GAMEFN i64 ClampI64(i64 Min, i64 X, i64 Max);
GAMEFN u64 ClampU64(u64 Min, u64 X, u64 Max);
GAMEFN f32 ClampF32(f32 Min, f32 X, f32 Max);

GAMEFN f32 AbsF32(f32 Val);
GAMEFN i32 AbsI32(i32 Val);

GAMEFN i32 MaxI32(i32 A, i32 B);
GAMEFN i32 MinI32(i32 A, i32 B);

GAMEFN f32 MaxF32(f32 A, f32 B);
GAMEFN f32 MinF32(f32 A, f32 B);

GAMEFN idx MaxIdx(idx A, idx B);
GAMEFN idx MinIdx(idx A, idx B);

GAMEFN u32 MaxU32(u32 A, u32 B);
GAMEFN u32 MinU32(u32 A, u32 B);

GAMEFN u64 MaxU64(u64 A, u64 B);
GAMEFN u64 MinU64(u64 A, u64 B);

GAMEFN f32 SquareF32(f32 X);

GAMEFN f32 SqrtF32(f32 X);
GAMEFN f32 RSqrtF32(f32 X);

GAMEFN f32 CosF32(f32 X);
GAMEFN f32 ArcCosF32(f32 X);
GAMEFN f32 SinF32(f32 X);

GAMEFN f32 RoundUpF32(f32 X);
GAMEFN f32 RoundZeroF32(f32 X);
GAMEFN f32 RoundDownF32(f32 X);
GAMEFN f32 RoundNearestF32(f32 X);
GAMEFN f32 FractionalF32(f32 X);

GAMEFN v2 RoundDownV2(v2 X);

GAMEFN u64 NextPow2U64(u64 X);
GAMEFN u32 NextPow2U32(u32 X);

static __m128 ZeroX8 = (__m128) {};

#define ReScale(T, Min, Max, NewMin, NewMax) ReScale_f32((T), (Min), (Max), (NewMin), (NewMax))
#define Normalize(Min, T, Max) Normalize_f32((Min), (T), (Max))
#define SmoothStep(Min, T, Max) SmoothStep_f32((Min), (T), (Max))
#define SmootherStep(Min, T, Max) SmootherStep_f32((Min), (T), (Max))

GAMEFN f32 SmoothStep_f32(f32 Min, f32 T, f32 Max);
GAMEFN f32 SmootherStep_f32(f32 Min, f32 T, f32 Max);
GAMEFN f32 Normalize_f32(f32 Min, f32 T, f32 Max);
GAMEFN f32 ReScale_f32(f32 T, f32 Min, f32 Max, f32 NewMin, f32 NewMax);

#define MC_S3H 0.866025403785 // sqrt(3)/2
#define MC_PI ((f32) M_PI)
#define MC_TAU // 2 * PI
#define MC_PHI

#endif
