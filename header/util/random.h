#ifndef GUARD_UTIL_RANDOM_H
#define GUARD_UTIL_RANDOM_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(xorshift_128);
FWDDECLARE(pcg_128);

struct xorshift_128
{
	// produces 64 bits of randomness
	u64 Data[2];
};

typedef xorshift_128 rand64;

/*
struct pcg_xsh_rr_64
{
	// produces 32 bits of randomness
	u64 State;
};

typedef pcg_xsh_rr_64 rand32;
*/

GAMEFN rand64
RandomSeries(u64 Seed);

GAMEFN u32
NextU32(rand64 *Series);

GAMEFN u64
NextU64(rand64 *Series);

GAMEFN i64
NextI64(rand64 *Series);

/* Random unsigned inside [0, Max) (left inclusive, right exclusive) */
GAMEFN u32
NextInRange(rand64 *Series, u32 Max);

GAMEFN f32
NextF32(rand64 *Series);

/* Random float inside [Start, End] (inclusive) */
GAMEFN f32
NextBetween(rand64 *Series, f32 Start, f32 End);

/* Random float inside [0, 1] (inclusive) */
GAMEFN f32
NextNormal(rand64 *Series);

/* Random float inside [-1, 1] (inclusive) */
GAMEFN f32
NextBinormal(rand64 *Series);

#endif
