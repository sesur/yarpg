#ifndef GUARD_UTIL_HASH_H
#define GUARD_UTIL_HASH_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

GAMEFN u32 Hash_Murmur3_32(u32 Val);
GAMEFN u64 Hash_Murmur3_64(u64 Val);
GAMEFN u64 Hash_Fibonacci(u64 Val, u64 Shift);

#endif
