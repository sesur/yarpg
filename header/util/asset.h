#ifndef GUARD_UTIL_ASSET_H
#define GUARD_UTIL_ASSET_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/arena_allocator.h>
#include <util/image.h>
#include <util/random.h>
#include <util/asset_types.h>
#include <file.h>
#include <sound.h>

#include <util/list_allocator.h>

FWDDECLARE(asset_store);
FWDDECLARE(text);

struct text
{
	u32 NumBytes;
	u32 NumChars;
	char const *Data;
};

struct asset_store
{
	category_info CategoryInfo[Category_COUNT];

        u32 AssetCount;
	u32 TagCount;

	tag_info SpriteTagInfo[SpriteTag_COUNT];
	tag_info SoundTagInfo[SoundTag_COUNT];
	tag_info TextTagInfo[TextTag_COUNT];

	sprite_info  *SpriteInfos;
	sound_info   *SoundInfos;
	text_info    *TextInfos;

	range AssetIdRangeByType[AssetType_COUNT];

	asset_info *AssetInfos;
	asset_tag  *Tags;

	idx NumFiles;
	file *Files;

	memory_list Data;

	idx volatile BlocksUsed;
	memory_arena Scratch;

	asset_header LRU;
};

GAMEFN sprite_id FirstSpriteOf(asset_store const *Assets, category Category);
GAMEFN sprite_id RandomSpriteOf(asset_store const *Assets, category Category, rand64 *Series);
GAMEFN sprite_id ClosestSpriteMatchOf(asset_store const *Assets, category Category,
				     f32 Weights[SpriteTag_COUNT], f32 Desired[SpriteTag_COUNT]);
GAMEFN sprite_id ExactSpriteMatchOf(asset_store const *Assets, category Category,
				      b32 Set[SpriteTag_COUNT],
				      f32 Desired[SpriteTag_COUNT],
				      f32 MaxError[SpriteTag_COUNT]);

GAMEFN text_id FirstTextOf(asset_store const *Assets, category Category);
GAMEFN text_id RandomTextOf(asset_store const *Assets, category Category, rand64 *Series);
GAMEFN text_id ClosestTextMatchOf(asset_store const *Assets, category Category,
				     f32 Weights[TextTag_COUNT], f32 Desired[TextTag_COUNT]);
GAMEFN text_id ExactTextMatchOf(asset_store const *Assets, category Category,
				b32 Set[TextTag_COUNT],
				f32 Desired[TextTag_COUNT],
				f32 MaxError[TextTag_COUNT]);

GAMEFN sound_id FirstSoundOf(asset_store const *Assets, category Category);
GAMEFN sound_id RandomSoundOf(asset_store const *Assets, category Category, rand64 *Series);
GAMEFN sound_id ClosestSoundMatchOf(asset_store const *Assets, category Category,
				     f32 Weights[SoundTag_COUNT], f32 Desired[SoundTag_COUNT]);
GAMEFN sound_id ExactSoundMatchOf(asset_store const *Assets, category Category,
				  b32 Set[SoundTag_COUNT],
				  f32 Desired[SoundTag_COUNT],
				  f32 MaxError[SoundTag_COUNT]);

GAMEFN asset_info *GetAssetInfo(asset_store const *Assets, asset_id Asset);
GAMEFN sprite_info *GetSpriteInfo(asset_store const *Assets, sprite_id Sprite);
GAMEFN sound_info *GetSoundInfo(asset_store const *Assets, sound_id Sound);
GAMEFN text_info *GetTextInfo(asset_store const *Assets, text_id Text);


GAMEFN void LoadTextDataInto(asset_store *Assets, text_id Text, byte *Buffer);

GAMEFN void FetchSprite(asset_store *Assets, sprite_id Sprite);
GAMEFN b32 TryLoadSprite(asset_store *Assets, sprite_id Sprite, image *ToLoad);

GAMEFN void FetchText(asset_store *Assets, text_id Text);
GAMEFN b32 TryLoadText(asset_store *Assets, text_id Text, text *ToLoad);

GAMEFN void FetchSound(asset_store *Assets, sound_id Sound);
GAMEFN b32 TryLoadSound(asset_store *Assets, sound_id Sound, real_sound_buffer *ToLoad,
			sound_id *NextPart);

GAMEFN void UnmarkUsedAll(asset_store *Assets);

#endif
