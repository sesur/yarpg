#ifndef GUARD_UTIL_STRING_H
#define GUARD_UTIL_STRING_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/arena_allocator.h>

FWDDECLARE(str_view);
FWDDECLARE(str);
FWDDECLARE(str_builder);
FWDDECLARE(parse32);
FWDDECLARE(parse64);

struct parse32
{
	b32 Valid;
	union
	{
		u32 U32;
		b32 B32;
		f32 F32;
		i32 I32;
	};
};

struct parse64
{
	b32 Valid;
	union
	{
		f64 F64;
		u64 U64;
		i64 I64;
	};
};

struct str_view
{
	u64 Size;
	char const *Data;
};

#if 0

struct utf8
{
	char8_t *Begin;
	char8_t *End;
	idx     NumGlyphs;
};

struct utf32
{
	char32_t *Begin;
	char32_t *End;
};

#endif

struct str
{
	u64 Capacity;
	u64 Size;
	char *Data;
};

struct str_builder
{
	u64 TotalSize;
	str *First;
	str *Last;
	memory_arena *Arena;
};

struct format_result
{
	// TODO: is u32 enough?
	u32 RequiredSize;
	u32 WrittenSize;
};

GAMEFN u32 CStringLength(char const *Chars);

GAMEFN str_view ViewOfString(str String);
GAMEFN str_view ViewOfChars(char const *Chars);
GAMEFN str_view ViewOfCountedChars(u64 Count, char const *Chars);
GAMEFN str_view ViewCopy(str_view View);
GAMEFN str_view SkipChars(u64 NumChars, str_view View);
GAMEFN str_view Restrict(u64 Length, str_view View);
GAMEFN b32 ViewEq(str_view Left, str_view Right);
GAMEFN b32 IsPrefix(str_view Prefix, str_view Str);

GAMEFN str MakeStrFromBuffer(u64 Capacity, char *Data);
GAMEFN str MakeStr(u64 Capacity, memory_arena *Arena);
GAMEFN str *Reset(str *String);
GAMEFN str *Clear(str *Buf);
// Requires that String has at least 1 byte free
GAMEFN char *AsCStr(str String);
GAMEFN str_view Trim(str_view Str);

GAMEFN parse32 ParseU32(str_view Str);
GAMEFN parse32 ParseI32(str_view Str);
GAMEFN parse64 ParseU64(str_view Str);
GAMEFN parse64 ParseI64(str_view Str);

#define ViewOfOneArg(Str) _Generic((Str),			\
				   char *: ViewOfChars,		\
				   char const *: ViewOfChars,	\
				   str_view: ViewCopy,		\
				   str: ViewOfString)(Str)

#define ViewOfTwoArg(Size, Str) ViewOfCountedChars((Size), (Str))
#define ViewOfDispatch(_1, _2, Name, ...) Name
#define ViewOf(...) ViewOfDispatch(__VA_ARGS__, ViewOfTwoArg, ViewOfOneArg)(__VA_ARGS__)

#define VIEWC(Str) ({							\
			YSASSERT(__builtin_constant_p(Str));		\
			(str_view) { .Size = sizeof(Str) - 1, .Data = (Str) }; \
		})


// NOTE: probably not a good idea. maybe expose alloca instead ?
#define STATICSTR(N) ({							\
			static char MACROVAR(Array)[N];			\
			((str) { .Capacity = (N), .Size = 0, .Data = MACROVAR(Array) }); \
		})

#define TEMPSTR(N) ({							\
			typeof(N) MACROVAR(Length) = (N);		\
			((str) { .Capacity = MACROVAR(Length),		\
				 .Size = 0,				\
				 .Data = __builtin_alloca(MACROVAR(Length))}); \
		})

#define ISCHARARRAY(Str) _Generic((Str), char *: 1, char const *: 1, default: 0)

GAMEFN b32 IsSpace(char C);
GAMEFN b32 IsDigit(char C);

#endif
