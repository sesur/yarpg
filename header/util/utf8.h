#ifndef GUARD_UTIL_UTF8_H
#define GUARD_UTIL_UTF8_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/string.h>

/* Both functions return the space needed/used by the operation in the byte stream */
GAMEFN u32 LoadGlyph(u32 StreamLength, byte const *Bytestream, u32 *Glyph);
GAMEFN u32 SaveGlyph(u32 StreamLength, byte *Bytestream, u32 Glyph);

GAMEFN idx GlyphCount(str_view View);
GAMEFN idx GlyphCountValid(str_view View);

#define UTF8HEADERBYTE(Byte) ((((byte) (Byte)) >> 6) != 2)

#endif
