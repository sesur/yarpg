#ifndef GUARD_UTIL_COLORS_H
#define GUARD_UTIL_COLORS_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

static struct palette
{
	v4 const Yellow;
	v4 const Gold;
	v4 const Black;
	v4 const White;
	v4 const Pink;
	v4 const Turquoise;
} const COLOR = {
	.Yellow = V(1.0, 0.8, 0.0, 1.0),
	.Gold  = V(0.855, 0.647, 0.126, 1.0),
	.Black = V(0, 0, 0, 1),
	.White = V(1, 1, 1, 1),
	.Pink  = V(1, 0, 1, 1),
	.Turquoise = V(0.3, 0.7, 0.6, 1.0),
};


GAMEFN v4 HSLToRGB(v4 HSL);
GAMEFN v4 RGBToHSL(v4 RGB);

GAMEFN v4 ColorOfId(idx Seed);
GAMEFN v4 ColorOfF32(f32 Val, f32 Min, f32 Max);

#endif
