#ifndef GUARD_UTIL_ASSET_IDS_H
#define GUARD_UTIL_ASSET_IDS_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

typedef enum
{
	Category_ERROR,
	Category_STAMP,
	Category_VERTEXSHADER,
	Category_FRAGSHADER,
	Category_HEAD,
	Category_TORSO,
	Category_TREE,
	Category_HILL,
	Category_SWAMP,

	Category_MUSIC,
	Category_SOUNDEFFECT,

	Category_BOW,
	Category_ARROW,
	Category_FIST,

	Category_ARMY,
	Category_CITY,

	Category_COUNT,
} category;

static char const *CategoryNames[] =
{
	[Category_ERROR]        = "ERROR",
	[Category_STAMP]        = "STAMP",
	[Category_VERTEXSHADER] = "VERTEXSHADER",
	[Category_FRAGSHADER]   = "FRAGSHADER",
	[Category_HEAD]         = "HEAD",
	[Category_TORSO]        = "TORSO",
	[Category_TREE]         = "TREE",
	[Category_HILL]         = "HILL",
	[Category_SWAMP]        = "SWAMP",

	[Category_MUSIC]        = "MUSIC",
	[Category_SOUNDEFFECT]  = "SOUNDEFFECT",

	[Category_BOW]          = "BOW",
	[Category_ARROW]        = "ARROW",
	[Category_FIST]         = "FIST",

	[Category_ARMY]         = "ARMY",
	[Category_CITY]         = "CITY",
};

YSASSERT(ARRAYCOUNT(CategoryNames) == Category_COUNT);

#endif
