#ifndef GUARD_UTIL_ARENA_ALLOCATOR_H
#define GUARD_UTIL_ARENA_ALLOCATOR_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(memory_arena);

struct memory_arena
{
	byte *Base;
	udx Size, Used;
	DEBUG_DEFINE(udx, Peak);
};

GAMEFN void InitializeArena(memory_arena *Arena, udx Size, void *Base);
GAMEFN memory_arena MemoryArena_FromBuffer(idx Size, void *Buf);

GAMEFN void *TryPushSize(memory_arena *Arena, udx SIze, u32 Alignment) __attribute__((alloc_size (2)));
GAMEFN void *PushSize(memory_arena *Arena, udx Size, u32 Alignment) __attribute__((alloc_size (2)));
GAMEFN void *TryPushZero(memory_arena *Arena, udx Size, u32 Alignment) __attribute__((alloc_size (2)));
GAMEFN void *PushZero(memory_arena *Arena, udx Size, u32 Alignment) __attribute__((alloc_size (2)));

GAMEFN void FreeAll(memory_arena *Arena);
GAMEFN memory_arena SplitArena(memory_arena *Arena, udx Size);
GAMEFN memory_arena SplitRest(memory_arena *Arena);
GAMEFN void AbsorbEmptyArena(memory_arena *Arena, memory_arena ToAbsorb);

#define PushStruct(Arena, Struct) ((typeof(Struct) *) PushSize((Arena), sizeof(Struct), alignof(Struct)))
#define PushArray(Arena, Num, Type) ((typeof(Type) *) PushSize((Arena), (Num) * sizeof(Type), alignof(Type)))

#define PushAlignedStruct(Arena, Struct, Align)				\
	({								\
		YSASSERT(Align % alignof(Struct) == 0);			\
		(typeof(Struct) *) PushSize((Arena),			\
					    sizeof(Struct), Align);	\
	})
#define PushAlignedArray(Arena, Num, Type, Align)			\
	({								\
		YSASSERT(Align % alignof(Type) == 0);			\
		(typeof(Type) *) PushSize((Arena),			\
					  (Num) * sizeof(Type), Align); \
	})

#define PushZeroStruct(Arena, Struct) ((typeof(Struct) *) PushZero((Arena), sizeof(Struct), alignof(Struct)))
#define PushZeroArray(Arena, Num, Type) ((typeof(Type) *) PushZero((Arena), (Num) * sizeof(Type), alignof(Type)))

#define WITHTEMP(Name, BaseArena) for (memory_arena Name = SplitRest((BaseArena)); \
				       Name.Base != NULL;		\
				       AbsorbEmptyArena((BaseArena), Name), Name.Base = NULL)

#endif
