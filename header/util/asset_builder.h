#ifndef GUARD_UTIL_ASSET_BUILDER_H
#define GUARD_UTIL_ASSET_BUILDER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <util/asset.h>

GAMEFN asset_store *LoadYAFFFiles(asset_store *Store, memory_arena *Perm,
				  memory_arena *Scratch);

GAMEFN void ReleaseYAFFFiles(asset_store *Store);

#endif
