#ifndef GUARD_UTIL_IMAGE_H
#define GUARD_UTIL_IMAGE_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

FWDDECLARE(image);

struct image
{
	u32 Width;
	u32 Height;
	pixel *Data;
};

GAMEFN void BlitPremultiplied(image Dest, image Source, irect Bounds);
GAMEFN void PremultiplyAlpha(image Image);

#endif
