#ifndef GUARD_UTIL_NOISE_H
#define GUARD_UTIL_NOISE_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

GAMEFN f32 FractalV2(idx Octaves, f32 Frequency, f32 Amplitude,
		     f32 Lacunarity, f32 Persistance,
		     v2 Vec);

GAMEFN f32 FractalF32(idx Octaves, f32 Frequency, f32 Amplitude,
		      f32 Lacunarity, f32 Persistance,
		      f32 X);

GAMEFN f32 NoiseF32(f32 X);
GAMEFN f32 NoiseV2(v2 Vec);

#endif
