#ifndef GUARD_UTIL_MEMORY_H
#define GUARD_UTIL_MEMORY_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

GAMEFN void ZeroMemory(u64 NumBytes, byte *Memory);
GAMEFN void SetMemory(u64 NumBytes, byte Val, byte *Memory);
GAMEFN void MoveMemory(u64 NumBytes, byte const * restrict From, byte * restrict To);

#define ZeroArray(Num, Array) ZeroMemory(sizeof(*Array) * Num, (byte *) (Array))
#define ZeroStruct(Struct) ZeroMemory(sizeof(*Struct), (byte *) (Struct))
#define CopyStruct(From, To) MoveMemory(sizeof(*(To)), (byte *)(From), (byte *)(To))

#endif
