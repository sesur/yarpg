#ifndef GUARD_UTIL_VECTOR_H
#define GUARD_UTIL_VECTOR_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

#include <util/math.h>

#define AddT(T) Add_ ## T
#define AddTProt(T) T AddT(T) (T A, T B)

#define HadamardT(T) Hadamard_ ## T
#define HadamardTProt(T) T HadamardT(T) (T A, T B)

#define EqT(T) Eq_ ## T
#define EqTProt(T) b32 EqT(T) (T A, T B)

#define DiffT(T) Diff_ ## T
#define DiffTProt(T) T DiffT(T) (T A, T B)

#define ScaleT(T) Scale_ ## T
#define ScaleTProt(T) T ScaleT(T) (f32 X, T V)

#define ScaleIT(T) ScaleI_ ## T
#define ScaleITProt(T) T ScaleIT(T) (i32 X, T V)

#define DefineStandardProtT(T)			\
	GAMEFN AddTProt(T);			\
	GAMEFN HadamardTProt(T);		\
	GAMEFN DiffTProt(T);			\
	GAMEFN EqTProt(T)

DefineStandardProtT(v2);
DefineStandardProtT(v3);
DefineStandardProtT(v4);

DefineStandardProtT(iv2);
DefineStandardProtT(iv3);
DefineStandardProtT(iv4);

GAMEFN ScaleTProt(v2);
GAMEFN ScaleTProt(v3);
GAMEFN ScaleTProt(v4);

GAMEFN ScaleITProt(iv2);

#define Eq(A, B) _Generic((A),			\
			  iv3: EqT(iv3),	\
			  iv2: EqT(iv2),	\
			  v2:  EqT(v2))(A, B)

#define Add(A, B) _Generic((A),				\
			   v2: AddT(v2),		\
			   v3: AddT(v3),		\
			   iv2: AddT(iv2))(A, B)

#define Scale(A, B) _Generic((B),			\
			     iv2: ScaleIT(iv2),		\
			     v2: ScaleT(v2),		\
			     v3: ScaleT(v3),		\
			     v4: ScaleT(v4))(A, B)

#define Diff(A, B) _Generic((A),			\
			    v2: DiffT(v2),		\
			    v3: DiffT(v3),		\
			    iv2: DiffT(iv2),		\
			    iv3: DiffT(iv3))(A, B)

#define Offset(A, B) Offset_irectiv2(A, B)

#define Hadamard(A, B) _Generic((A),				\
				v2: HadamardT(v2),		\
				v3: HadamardT(v3),		\
				v4: HadamardT(v4))(A, B)

GAMEFN irect
Offset_irectiv2(irect Rect, iv2 Offset);

GAMEFN f32 Lerp_f32(f32 A, f32 T, f32 B);
GAMEFN v2 Lerp_v2(v2 A, f32 T, v2 B);

#define Lerp(Start, Percent, End) _Generic((Start),			\
					   i32: Lerp_f32,		\
					   f32: Lerp_f32,		\
					   v2:  Lerp_v2)(Start, Percent, End)

#define Inner(LVec, RVec) _Generic((LVec),		\
				   v2: Inner_v2,	\
				   v3: Inner_v3)(LVec, RVec)

#define Length(Vec) _Generic((Vec),			\
			     v2: Length_v2,		\
			     v3: Length_v3)(Vec)

#define OneOverLength(Vec) _Generic((Vec),			\
			     v2: OneOverLength_v2,		\
			     v3: OneOverLength_v3)(Vec)

GAMEFN f32 Inner_v2(v2 A, v2 B);
GAMEFN f32 Length_v2(v2 V);
GAMEFN f32 OneOverLength_v2(v2 A);

GAMEFN f32 Inner_v3(v3 A, v3 B);
GAMEFN f32 Length_v3(v3 V);
GAMEFN f32 OneOverLength_v3(v3 A);

GAMEFN f32 Distance(v2 A, v2 B);
GAMEFN f32 OneOverDistance(v2 A, v2 B);

GAMEFN v2 UnitVector(f32 Angle);
GAMEFN v2 RightOrtho(v2 Vec);
GAMEFN v2 LeftOrtho(v2 Vec);

/**
 * Given a vector v_B (= Vector) in basis B, and a presentation B_C (= basis_system )
 * of basis B in terms of basis C, returns the vector v_C of v in terms of C.
 */
GAMEFN v3 BasisTransform(v3 Vector, basis_system Basis);
GAMEFN v2 BasisStretch(v2 Vector, basis_system Basis);

GAMEFN b32 PointInsideRect(v2 Point, rect Rect);

#define Determinant(First, ...) _Generic((First),			\
					 v2: Determinant_v2,		\
					 v3: Determinant_v3)(First __VA_OPT__(,) __VA_ARGS__)

GAMEFN f32 Determinant_v2(v2 Col1, v2 Col2);
GAMEFN f32 Determinant_v3(v3 Col1, v3 Col2, v3 Col3);

GAMEFN bool LeftOf(v2 Start, v2 End, v2 TestP);
GAMEFN bool RightOf(v2 Start, v2 End, v2 TestP);

GAMEFN f32 SmallVectorAngle(v2 Vec, v2 Base);
GAMEFN f32 LeftVectorAngle(v2 Vec, v2 Base);
GAMEFN f32 RightVectorAngle(v2 Vec, v2 Base);

#endif
