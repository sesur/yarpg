#ifndef GUARD_UTIL_BMP_H
#define GUARD_UTIL_BMP_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/image.h>

FWDDECLARE(bmp_header);
FWDDECLARE(dib_header);

typedef enum
{
	DIB_CORE_HEADER,
	DIB_INFO_HEADER,
} dib_type;

struct dib_header
{
        dib_type Type;
	u32 BitmapWidth;
	u32 BitmapHeight;
	i32 XDirection;
	i32 YDirection;
	u16 BPP;
};

struct bmp_header
{
	u16 MagicNumber;
	u32 NumBytes;
	u32 DataOffset;

	dib_header DIB;
};

GAMEFN b32 LoadBMPHeader(bmp_header *Header, byte *FileContents);

GAMEFN u32 NumPixelsInBMP(bmp_header *Header);

GAMEFN b32 LoadBMPData(bmp_header *Header, byte *FileContents, image *Image);

#endif
