#ifndef GUARD_UTIL_RING_ALLOC_H
#define GUARD_UTIL_RING_ALLOC_H

FWDDECLARE(ring_alloc);

struct ring_alloc
{
	idx Capacity;
	idx Begin; // start of free memory
	idx End;   // end of free memory
	void *Data;
};

GAMEFN ring_alloc Ring_FromBuffer(idx NumBytes, byte Buffer[NumBytes]);

// This function does not actually allocate memory and instead returns either
// -1 if it would not be able to allocate the requested memory or
// x >= 0 if it would be able to allocate the requested memory;
//        in that case 'x' would be the selected start position
GAMEFN idx Ring_DryPush(ring_alloc const *Alloc, idx Size, idx Alignment);
GAMEFN idx Ring_Push(ring_alloc *Alloc, idx Size, idx Alignment);
// move begin towards end
GAMEFN void Ring_PopTo(ring_alloc *Alloc, idx NewBegin);
// move end towards begin
GAMEFN void Ring_DequeueTo(ring_alloc *Alloc, idx NewEnd);
// move begin away from end
GAMEFN void Ring_ReserveTo(ring_alloc *Alloc, idx NewBegin);
// checks wether the access is ok
GAMEFN void *Ring_DataFrom(ring_alloc const *Alloc, idx DataStart);

#endif /* GUARD_UTIL_RING_ALLOC_H */
