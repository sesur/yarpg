#ifndef GUARD_UTIL_TOKENIZER_H
#define GUARD_UTIL_TOKENIZER_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
FWDDECLARE(tokenizer);

struct tokenizer
{
	char const *Start;
	char const *End;
};

GAMEFN PUREFN tokenizer TokenizerStart(str_view Buffer);
GAMEFN str_view  NextToken(tokenizer *Tokenizer);
GAMEFN b32       HasToken(tokenizer *Tokenizer);

#endif
