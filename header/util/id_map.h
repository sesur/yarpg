#ifndef GUARD_UTIL_ID_MAP_H
#define GUARD_UTIL_ID_MAP_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <util/arena_allocator.h>
FWDDECLARE(id_map);
FWDDECLARE(entity_id);
FWDDECLARE(entity_slot);

typedef i16 id_index;

// NOTE: this is using robin hood hashing.
// NOTE: hopscotch hashing (https://programming.guide/hopscotch-hashing.html)
//       may be easier to vectorize and it copes better with high load factor
//       which may come into play, since I do not want to rehash this _at all_!
struct id_map
{
	// assumed to be a power of two
	udx Shift;
	idx Capacity;
	idx Occupancy;
	idx MaxPSL;
	// [ID] -> [Pos in Array]
	entity_id *Entities;
	id_index  *Indices;
};

// NOTE: will we really need more than 4G entities ?
// maybe just use i32 here
struct entity_id
{
	idx ToIdx;
};

struct entity_slot
{
	idx ToIdx;
};

GAMEFN id_index *GetOrAllocIdSlot(id_map *Map, entity_id Id);
GAMEFN id_index *CreateIdSlot(id_map *Map, entity_id Id);
GAMEFN id_index *GetIdSlot(id_map *Map, entity_id Id);
GAMEFN b32 DeleteEntity(id_map *Map, idx Pos);
GAMEFN void ClearIdMap(id_map *Map);
GAMEFN id_map IdMapOfArena(idx Size, memory_arena *Arena);

#endif
