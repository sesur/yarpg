#ifndef GUARD_UTIL_LIST_ALLOCATOR_H
#define GUARD_UTIL_LIST_ALLOCATOR_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(memory_block);
FWDDECLARE(list_allocator);

enum list_allocator_flags
{
	ListAllocator_SENTINEL = 0x1,
	ListAllocator_USED = 0x2,
};

struct memory_block
{
	memory_block *Prev, *Next;
	udx Size; /* DOES NOT INCLUDE THE HEADER ITSELF! */
	b32 Flags;
};

struct list_allocator
{
	memory_block Sentinel;
};

GAMEFN b32 InitializeListAllocator(list_allocator *Alloc, udx Size, void *Memory);

GAMEFN void *SlowAlloc(list_allocator *Alloc, udx Size) __attribute__((alloc_size (2)));
GAMEFN void *SlowRealloc(list_allocator *Alloc, udx Size, void *Old) __attribute__((alloc_size (2)));
GAMEFN void *SlowArrayAlloc(list_allocator *Alloc, u32 Count, udx Size);

GAMEFN void SlowFree(void *ToFree);

typedef list_allocator memory_list;

#endif
