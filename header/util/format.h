#ifndef GUARD_UTIL_FORMAT_H
#define GUARD_UTIL_FORMAT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif


#include <platform.h>
#include <util/string.h>

FWDDECLARE(format_result);

GAMEFN u64 Format(write_target Target, str_view Format, ...);
GAMEFN format_result FormatInto(str *Buffer, str_view Format, ...);

#define FormatOut(FormatStr, ...) Format(WriteTarget_OUT, (ISCHARARRAY(FormatStr) && __builtin_constant_p(FormatStr)) ? VIEWC(FormatStr) : ViewOf(FormatStr) __VA_OPT__(,) __VA_ARGS__)
#define FormatErr(FormatStr, ...) Format(WriteTarget_ERR, (ISCHARARRAY(FormatStr) && __builtin_constant_p(FormatStr)) ? VIEWC(FormatStr) : ViewOf(FormatStr) __VA_OPT__(,) __VA_ARGS__)

#endif
