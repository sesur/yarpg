#ifndef GUARD_UTIL_WAV_H
#define GUARD_UTIL_WAV_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <sound.h>

FWDDECLARE(wav_header);

struct wav_header
{
	i32 NumChannels;
	i32 ChannelPitch;
	i32 SampleRate;

	i32 NumSamples;
};

GAMEFN wav_header
LoadWAVHeader(u32 NumBytes, byte *WAV);

GAMEFN void
LoadWAV(real_sound_buffer *Buffer, u32 NumBytes, byte *WAV);

#endif
