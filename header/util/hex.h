#ifndef GUARD_UTIL_HEX_H
#define GUARD_UTIL_HEX_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

GAMEFN v2 HexToLinear(v2 HexPos);
GAMEFN v2 LinearToHex(v2 LinPos);

GAMEFN iv2 RoundHexPos(v2 HexPos);
GAMEFN i32 HexLength(iv2 HexPos);
GAMEFN i32 HexDistance(iv2 HexPosA, iv2 HexPosB);

#endif
