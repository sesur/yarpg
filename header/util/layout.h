#ifndef GUARD_UTIL_LAYOUT_H
#define GUARD_UTIL_LAYOUT_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

GAMEFN irect VerticalRegion(irect Bounds, i32 Divisions, i32 Start, i32 End);
GAMEFN irect HorizontalRegion(irect Bounds, i32 Divisions, i32 Start, i32 End);
GAMEFN irect GridRegion(irect Bounds, iv2 Divisions, iv2 Start, iv2 End);


#endif
