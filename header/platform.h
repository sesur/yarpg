#ifndef GUARD_PLATFORM_H
#define GUARD_PLATFORM_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <thread.h>
#include <font.h>
#include <file.h>
#include <input.h>

#include <debug/debug_state.h>

FWDDECLARE(game_memory);
FWDDECLARE(platform);
FWDDECLARE(platform_layer);
FWDDECLARE(platform_info);
FWDDECLARE(platform_api);

struct game_memory
{
	u64 PersistentSize, TransientSize, DebugSize;
	void *PersistentMemory, *TransientMemory, *DebugMemory;
};

struct platform_info
{
	i64 MaxStackSize;
	i64 MaxMemory;

	u32 NumPhysicalCores;
	u32 NumVirtualCores;

	u32 CacheSize;
	u32 CacheLineSize;
	u32 NumWorkerThreads;

	u32 PageSize;

	i32 WindowWidth;
	i32 WindowHeight;

	f32 DPIXScale;
	f32 DPIYScale;
};

typedef enum
{
	WriteTarget_OUT = 1,
	WriteTarget_ERR = 2,
} write_target;

#define WRITE_OUT(Name) void Name(write_target Target, u64 NumBytes, byte const Bytes[static NumBytes])
typedef WRITE_OUT(write_out);
GAMEFN WRITE_OUT(WriteOut);

struct platform_api
{
	push_task          *PushTask;
	wait               *Wait;

	// we can probably move this into
	// the game somehow (probably with the asset system)
	request_font       *RequestFont;
	render_glyph       *RenderGlyph;

	open_asset_iterator *OpenAssetIterator;
	start_iterator      *StartIterator;
	next_file           *NextFile;
	read_file           *ReadFile;
	close_file          *CloseFile;
	close_iterator      *CloseIterator;

	write_out           *WriteOut;

	push_perf_event     *PushPerfEvent;
	current_time_stamp  *CurrentTimeStamp;
};

struct platform_layer
{
	platform_info Info;
	platform_api  API;

	game_memory Memory;
	game_input Input;

	b32 WindowActive;
};

#endif
