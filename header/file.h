#ifndef GUARD_FILE_H
#define GUARD_FILE_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>

FWDDECLARE(file_iterator);
FWDDECLARE(file);

struct file_iterator
{
	idx NumEntries;
	idx volatile Error;  /* Error = 0 => no error */

	void *Platform;
};

struct file
{
	idx Size;
	idx volatile Error; /* Error = 0 => no error */

	void *Platform;
};

#define OPEN_ASSET_ITERATOR(Name) file_iterator Name(void)
typedef OPEN_ASSET_ITERATOR(open_asset_iterator);
GAMEFN OPEN_ASSET_ITERATOR(OpenAssetIterator);

#define START_ITERATOR(Name) file Name(file_iterator *Iter)
typedef START_ITERATOR(start_iterator);
GAMEFN START_ITERATOR(StartIterator);

#define NEXT_FILE(Name) file Name(file_iterator *Iter)
typedef NEXT_FILE(next_file);
GAMEFN NEXT_FILE(NextFile);

#define READ_FILE(Name) void Name(file *Handle,		\
				  idx Start, idx Length,	\
				  byte *Buffer)
typedef READ_FILE(read_file);
GAMEFN READ_FILE(ReadFile);

#define CLOSE_ITERATOR(Name) void Name(file_iterator *Iter)
typedef CLOSE_ITERATOR(close_iterator);
GAMEFN CLOSE_ITERATOR(CloseIterator);

#define CLOSE_FILE(Name) void Name(file *Handle)
typedef CLOSE_FILE(close_file);
GAMEFN CLOSE_FILE(CloseFile);

#endif
