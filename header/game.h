#ifndef GUARD_GAME_H
#define GUARD_GAME_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <def.h>
#include <opengl/opengl.h>
#include <platform.h>
#include <input.h>
#include <sound.h>

FWDDECLARE(keyboard_input);
FWDDECLARE(mouse_input);
FWDDECLARE(game_update_and_render_report);
FWDDECLARE(game_api);

struct keyboard_input
{
	b32 Consumed;
	keyboard_data Keyboard;
};

struct mouse_input
{
	b32 Consumed;
	mouse_data Mouse;
};

struct game_update_and_render_report
{
	b32 ShouldContinue;
};

/* Returns wether init was successfull; 0 = Success */
#define GAME_INIT(Name)  e32 Name(platform_layer *Platform)
typedef GAME_INIT(game_init);

/* Returns wether to stop the game or not */
#define GAME_UPDATE_AND_RENDER(Name)  game_update_and_render_report Name(platform_layer *Platform, \
									 f64 DeltaTime)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);

/* Returns nothing */
#define GAME_RELEASE(Name)  void Name(platform_layer *Platform)
typedef GAME_RELEASE(game_release);

/* Return number of samples written */
#define GAME_PLAY_SOUND(Name) real_sound_buffer Name(platform_layer *Platform, f64 DeltaTime, \
						     i32 TargetSampleRate, i32 NumSamples)
typedef GAME_PLAY_SOUND(game_play_sound);

#define GAME_COLLATE_DEBUG_EVENTS(Name) void Name(platform_layer *Platform, idx ThreadId, event_storage Events)
typedef GAME_COLLATE_DEBUG_EVENTS(game_collate_debug_events);



struct game_api
{
	game_init                 *Init;
	game_release              *Release;
	game_update_and_render    *UpdateAndRender;
	game_play_sound           *PlaySound;
	game_collate_debug_events *CollateDebugEvents;
};

#endif
