#ifndef GUARD_ENTITY_TEST_H
#define GUARD_ENTITY_TEST_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#include <util/asset_types.h>
#include <util/id_map.h>
#include <util/arena_allocator.h>

FWDDECLARE(entity_test);
FWDDECLARE(component);
FWDDECLARE(rect_data);
FWDDECLARE(text_data);
FWDDECLARE(hex_data);
FWDDECLARE(bound_data);
FWDDECLARE(line_data);
FWDDECLARE(tri_data);

struct hex_data
{
	v4 Color;
	v2 Offset;
	v2 Dim;
};

struct tri_data
{
	v2 Points[3];
	v4 Color;
};

struct rect_data
{
	v4 Color;
	v2 Offset;
	v2 Dim;
};

struct text_data
{
	v2 Offset;
	v2 Dim;
	sprite_id Sprite;
};

struct line_data
{
	v4 Color;
	v2 Start, End;
	f32 Thickness;
};

struct bound_data
{
	v2 Center;
	f32 Radius;
};

struct component
{
	id_map Ids;
	id_index NextId;
};

#define DEFINE_COMPONENT(Type, Name) component Name ## _C; Type *Name; entity_id *Name ## _I;

#define COMPONENTS(C)			\
	C(line_data, Line)		\
	C(rect_data, Rect)		\
	C(text_data, Texture)		\
	C(bound_data, Bound)		\
	C(v2, Position)		        \
	C(entity_id, Follow)		\
	C(hex_data, Hex)                \
	C(tri_data, Triangle)

struct entity_test
{
	COMPONENTS(DEFINE_COMPONENT);

	idx NextEntity;
};

#undef DEFINE_COMPONENT
#define PROT_COMPONENT_OPS(Name)					\
	GAMEFN typeof(*MEMBER(entity_test, Name)) *			\
	Add ## Name(entity_test *Test, entity_id Id, typeof(*MEMBER(entity_test, Name)) Data); \
	GAMEFN typeof(*MEMBER(entity_test, Name)) *			\
	Grab ## Name(entity_test *Test, entity_id Id)

#define PROT_COMPONENT_OPS_P(Type, Name) PROT_COMPONENT_OPS(Name);

COMPONENTS(PROT_COMPONENT_OPS_P);

#undef PROT_COMPONENTS_OPS_P
#undef PROT_COMPONENT_OPS

/** Idea:
 * Instead of NextEntity, AddComponent, instead use
 * range GenEntities(NumEntites) and
 * void AddComponents(NumEntities, *EntityArray, *ComponentData)
 * This might give a performance boost. */

GAMEFN entity_id NextEntity(entity_test *Test);

#endif
