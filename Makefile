.PHONY: all main lib builder run assets clean test

DEBUG?=1
SANITIZER?=0
ANALYZER?=0

CC := LC_MESSAGES=C $(CC)

RELOUTDIR := release
DEBOUTDIR := debug

TARGETARCH := skylake

WARNINGS := -Wall -Wextra -Wcast-align \
	    -Werror=format-security \
	    -Wno-unused-variable -Wno-unused-function -Wno-unused-but-set-variable \
	    -Wno-variadic-macros -Wno-unused-parameter \
	    -Wcast-qual -Wswitch -Wswitch-enum \
	    -Wuninitialized -Wsuggest-attribute=pure \
	    -Wsuggest-attribute=const \
	    -Wsuggest-attribute=noreturn \
	    -Wmissing-noreturn -Wlogical-op \
	    -Wsuggest-attribute=malloc \
	    -Wformat=2 -Wundef -Werror=float-equal \
	    -Wwrite-strings -Wnull-dereference -Wvarargs \
	    -Wimplicit-function-declaration -Wstrict-prototypes \
	    -Wno-suggest-attribute=noreturn \
	    -Wimplicit-fallthrough -Wstrict-aliasing=2 \
	    -Wmisleading-indentation -fstack-usage -D_FORTIFY_SOURCE=2
## Maybe someday ?
# -Winline
# -Wshadow ; = better global names
# -Wconversion -Wno-sign-conversion
# -Wunused-macros

CCFLAGS := -Werror $(WARNINGS)  \
	   -Iheader -Icontrib \
	   -fno-math-errno -std=gnu2x \
	   -gdwarf-5 -gpubnames \
	   -MMD -MP \
	   -fvisibility=hidden \
	   -mavx2 -mno-avx256-split-unaligned-load -mno-avx256-split-unaligned-store

#CCFLAGS += -time

LDFLAGS := -fuse-ld=gold -Wl,--gdb-index

ifneq ($(SANITIZER), 0)
CCFLAGS += -fsanitize=undefined,unreachable,vla-bound,null,alignment,bounds,enum
endif

ifeq ($(SANITIZER), 1)
# thread sanitation
CCFLAGS += -fsanitize=thread
else ifeq ($(SANITIZER), 2)
# memory sanitation
CCFLAGS += -fsanitize=address
endif

ifeq ($(ANALYZER), 1)
CCFLAGS += -fanalyzer
endif

ifeq ($(DEBUG), 1)
CCFLAGS += -ggdb3 -gsplit-dwarf -DDEBUG $(COMPSAN) -fno-omit-frame-pointer -fstack-protector-all
CCFLAGS += -O0 -march=$(TARGETARCH)
CCFLAGS += -fsanitize-address-use-after-scope

OUTDIR := $(DEBOUTDIR)
else
OUTDIR := $(RELOUTDIR)
CCFLAGS += -O2 -march=$(TARGETARCH)
CCFLAGS += -DRELEASE
endif

LDLIBS :=
MLIBS  := glfw3 freetype2 alsa
BLIBS  :=
LLIBS  :=

FONT := "\"contrib/Nuklear/extra_font/Roboto-Regular.ttf\""

MCCFLAGS := $(CCFLAGS) -DFONT_NAME=$(FONT) -fwhole-program
LCCFLAGS := $(CCFLAGS) -fpic # if size gets too large use -fPIC instead
BCCFLAGS := $(CCFLAGS) -fwhole-program

MLDFLAGS := $(LDFLAGS) -L$(OUTDIR) -Wl,-rpath,$(OUTDIR)
LLDFLAGS := $(LDFLAGS) -Wl,-soname,libyarpg.so -shared
BLDFLAGS := $(LDFLAGS)

MLDLIBS := $(LDLIBS) -pthread -lm -ldl -lnuklear -lglad
LLDLIBS := $(LDLIBS) -lm
BLDLIBS := $(LDLIBS) -lm

ifneq ($(MLIBS),)
MCCFLAGS += $(shell pkg-config --cflags $(MLIBS))
MLDFLAGS += $(shell pkg-config --libs-only-L $(MLIBS))
MLDLIBS  += $(shell pkg-config --libs-only-l $(MLIBS))
endif

ifneq ($(LLIBS),)
MCCFLAGS += $(shell pkg-config --cflags $(LLIBS))
LLDFLAGS += $(shell pkg-config --libs-only-L $(LLIBS))
LLDLIBS  += $(shell pkg-config --libs-only-l $(LLIBS))
endif

ifneq ($(BLIBS),)
MCCFLAGS += $(shell pkg-config --cflags $(BLIBS))
BLDFLAGS += $(shell pkg-config --libs-only-L $(BLIBS))
BLDLIBS  += $(shell pkg-config --libs-only-l $(BLIBS))
endif

DEBUGDATA := $(OUTDIR)/*.dwo
FILES   := $(OUTDIR)/yarpg $(OUTDIR)/libyarpg.so $(OUTDIR)/yaff_builder
DEPENDS := $(OUTDIR)/*.d
STACKINFO := $(OUTDIR)/*.su

all: main lib builder

-include $(DEPENDS)

main: $(OUTDIR)/yarpg

builder: $(OUTDIR)/yaff_builder

lib: $(OUTDIR)/libyarpg.so

CONTRIBS := $(OUTDIR)/libglad.so $(OUTDIR)/libnuklear.a

$(OUTDIR)/libglad.so: contrib/glad2/gl.c
	@mkdir -p $(@D)
	$(CC) -fPIC -Icontrib -Wl,-soname,libglad.so -shared -o $@ $<

$(OUTDIR)/nuklear_impl.o: src/nuklear_impl.c
	@mkdir -p $(@D)
	$(CC) -Icontrib -c $< -o $@
$(OUTDIR)/libnuklear.a: $(OUTDIR)/nuklear_impl.o
	ar rcs $@ $^

$(OUTDIR)/yarpg: src/linux_main.c Makefile $(CONTRIBS)
	@mkdir -p $(@D)
	$(CC) $(MCCFLAGS) $(MLDFLAGS) -o $@ $< $(MLDLIBS)

$(OUTDIR)/libyarpg.so: src/game.c Makefile
	@mkdir -p $(@D)
	$(CC) $(LCCFLAGS) $(LLDFLAGS) -o $@ $< $(LLDLIBS)

$(OUTDIR)/yaff_builder: src/yaff_builder.c Makefile
	@mkdir -p $(@D)
	$(CC) $(BCCFLAGS) $(BLDFLAGS) -o $@ $< $(BLDLIBS)

clean:
	rm -rf $(OUTDIR)

run: main lib
	$(OUTDIR)/yarpg

TESTS := id_map memory utf8 file new_thread

TEST_OUTPUT := $(patsubst %,test/%, $(TESTS))
TEST_DEPENDS := $(patsubst %,%.d,$(TEST_OUTPUT))

-include $(TEST_DEPENDS)

#.PHONY: $(TEST_OUTPUT)

test/%: test/%_test.c Makefile
#	echo $(TEST_DEPENDS)
	$(CC) $(MCCFLAGS) $(MLDFLAGS) -o $@ $< $(MLDLIBS)
	@echo Staring test $@
	@ $@ || true

# test/id_map_test: test/id_map_test.c Makefile
# 	$(CC) $(MCCFLAGS) $(MLDFLAGS) -o $@ $<
# test/memory_test: test/memory_test.c test/../src/util/memory.c Makefile
# 	$(CC) $(MCCFLAGS) $(MLDFLAGS) -o $@ $<

test: $(TEST_OUTPUT) Makefile
#	$(foreach test,$(TEST_OUTPUT),$(test);)

assets: builder
	$(OUTDIR)/yaff_builder
