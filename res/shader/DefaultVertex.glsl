#version 330

// layout(location = 0) in vec3 Position;
// layout(location = 1) in vec2 TexturePos;
// layout(location = 2) in mat4 Transform;
// layout(location = 3) in vec4 Color;

// layout(location = 0) out vec4 VertexPosition;
// layout(location = 1) out vec2 VertexTexturePos;
// layout(location = 2) out vec4 VertexColor;

in vec3 Position;
in vec2 TexturePos;
in vec3 Transform;
in vec4 Color;

uniform mat4 Projection;

out vec4 VertexPosition;
out vec2 VertexTexturePos;
out vec4 VertexColor;

// vec4 stretch(vec4 i)
// {
//   return vec4(i.x * 2, i.y, i.z, i.w);
// }

void main(void)
{
 vec2 WorldPosition = Position.xy * Transform.z + Transform.xy;
  
 VertexPosition = Projection * vec4(Transform.xy + Position.xy * Transform.z, 0.0, 1.0); 
  
//  VertexPosition = Projection * vec4(stretch(WorldPosition), Position.z, 1.0);
  VertexColor = Color;
  VertexTexturePos = TexturePos;
  gl_Position = VertexPosition;
}
