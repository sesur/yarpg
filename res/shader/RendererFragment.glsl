#version 330

#define NUM_TEXTURES %u

uniform sampler2D Textures[NUM_TEXTURES];
uniform bool      IsMono[NUM_TEXTURES];

in vec4 VertexPosition;
in vec2 VertexTexturePos;
in vec4 VertexColor;
in float VertexTextureId;

out vec4 FragmentColor;

void main(void)
{
	int TextureId = int(VertexTextureId);

	if (IsMono[TextureId])
	  {
	    float TextureAlpha = texture(Textures[TextureId], VertexTexturePos).r;

	    FragmentColor = vec4(1.0, 1.0, 1.0, TextureAlpha) * VertexColor;
	  }
	else
	  {
	    FragmentColor = texture(Textures[TextureId], VertexTexturePos) * VertexColor;
	  }
}
