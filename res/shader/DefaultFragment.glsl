#version 330

// layout(location = 0) in vec4 VertexPosition;
// layout(location = 1) in vec2 VertexTexturePos;
// layout(location = 2) in vec4 VertexColor;

// // add texture sampler here ??

// layout(location = 0) out vec4 FragmentColor;

uniform sampler2D Texture;

in vec4 VertexPosition;
in vec2 VertexTexturePos;
in vec4 VertexColor;

out vec4 FragmentColor;

void main(void)
{
  FragmentColor = VertexColor;  
}
