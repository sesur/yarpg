#version 150 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D GlyphAtlas;
uniform vec3 TextColor;

void main()
{
  
  float Alpha = texture(GlyphAtlas, TexCoords).r;
  
  vec4 Sampled = vec4(1.0, 1.0, 1.0, Alpha);
  // if (Alpha < 0.5)
  // {
  //   Sampled = vec4(0.0, 0.0, 0.0, 0.0);
  // } else if (Alpha < 0.7)
  // {
  //   Sampled = vec4(0.0, 1.0, 0.0, Alpha);
  // } else
  // {
  //   Sampled = vec4(1.0, 1.0, 1.0, Alpha);
  // }
  
  
  color = vec4(TextColor, 1.0) * Sampled;
}
