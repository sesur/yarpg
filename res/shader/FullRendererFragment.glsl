#version 330

// NUM_TEXTURES gets defined at load time

uniform sampler2D Textures[NUM_TEXTURES];
uniform bool      IsMono[NUM_TEXTURES];

in vec4 VertexPosition;
in vec2 VertexTexturePos;
in vec4 VertexColor;
in float VertexTextureId;

out vec4 FragmentColor;

void main(void)
{
	int TextureId = int(VertexTextureId);

	vec4 TextureCol;
	if (IsMono[TextureId])
	{
		float TextureAlpha = texture(Textures[TextureId], VertexTexturePos).r;

		TextureCol = TextureAlpha * vec4(1.0, 1.0, 1.0, 1.0);
	}
	else
	{
		TextureCol = texture(Textures[TextureId], VertexTexturePos);
	}
	FragmentColor = TextureCol * VertexColor;
}
