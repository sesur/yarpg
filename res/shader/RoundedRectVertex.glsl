#version 150

in vec2 Position;
in vec3 Color;

out vec2 VertexOriginPos;
out vec3 VertexColor;

void main(void)
{
  gl_Position = vec4(0.3 * Position, 0.0, 1.0);  
  VertexOriginPos = Position;
  VertexColor = Color;
}
