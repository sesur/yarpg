#version 150 core

layout(triangles) in;
layout(line_strip, max_vertices=4) out;
in  vec3 vert_color[];
out vec3 g_color;

void main(void)
{
  g_color = vert_color[0];

  gl_Position = gl_in[0].gl_Position;
  EmitVertex();
  gl_Position = gl_in[1].gl_Position;
  EmitVertex();
  gl_Position = gl_in[2].gl_Position;
  EmitVertex();
  gl_Position = gl_in[0].gl_Position;
  EmitVertex();
  EndPrimitive();
}
