#version 150

precision mediump float;

uniform sampler2D Texture;

in vec2 VertexTexturePos;
in vec4 VertexColor;

out vec4 FragmentColor;

void main(){
	FragmentColor = VertexColor * texture(Texture, VertexTexturePos);
}											 
