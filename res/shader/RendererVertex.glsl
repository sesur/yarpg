#version 330

in vec3 Position;
in vec2 TexturePos;
in vec3 Transform;
in vec4 Color;
in float TextureId;

uniform mat4 Projection;

out vec4 VertexPosition;
out vec2 VertexTexturePos;
out vec4 VertexColor;
out float VertexTextureId;

void main(void)
{
	vec2 WorldPosition = Position.xy * Transform.z + Transform.xy;
  
	VertexPosition = Projection * vec4(Transform.xy + Position.xy * Transform.z, 0.0, 1.0); 
  
	VertexColor = Color;
	VertexTexturePos = TexturePos;
	VertexTextureId = TextureId;
	gl_Position = VertexPosition;
}
