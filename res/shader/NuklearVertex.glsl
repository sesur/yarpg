#version 150

uniform mat4 Projection;

in vec2 Position;
in vec2 TexturePos;
in vec4 Color;

out vec2 VertexTexturePos;
out vec4 VertexColor;

void main() {
  VertexTexturePos = TexturePos;
  VertexColor = Color;
  gl_Position = Projection * vec4(Position.xy, 0, 1);
}
