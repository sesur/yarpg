#ifndef GUARD_TEST_TEST_H
#define GUARD_TEST_TEST_H

#if !defined(__GNUC__) || defined(__clang__)
#pragma once
#endif

#define PLATFORM
#include <debug.h>
#undef DEBUG
#include <def.h>

#undef PRIVATE
#define PRIVATE

idx CONSTFN
ModInc
(
	idx Num,
	idx Modulus
)
{
	YASSERTF((Num >= 0) && (Num < Modulus),
		 "0 <= %ld < %ld",
		 Num, Modulus);
	idx Result = Num + 1;
	if (Result == Modulus)
	{
		Result = 0;
	}

	return Result;
}

idx CONSTFN
ModDec
(
	idx Num,
	idx Modulus
)
{
	YASSERTF((Num >= 0) && (Num < Modulus),
		 "0 <= %ld < %ld",
		 Num, Modulus);
	idx Result = Num - 1;
	if (Result == -1)
	{
		Result = Modulus - 1;
	}

	return Result;
}

#endif
