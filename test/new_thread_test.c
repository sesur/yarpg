#define _GNU_SOURCE /*for CPU_* macros */
#include "test.h"
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <compiler.h>

#include "../header/util/ring_alloc.h"
#include "../src/util/ring_alloc.c"

FWDDECLARE(thread_debug_state);
FWDDECLARE(async_block);
FWDDECLARE(async_stack);
FWDDECLARE(thread_context);
FWDDECLARE(debug_event);
FWDDECLARE(debug_variable);
FWDDECLARE(typed_value);
FWDDECLARE(debug_variable_view);
FWDDECLARE(str_view);
FWDDECLARE(perf_event);
FWDDECLARE(time_stamp);
FWDDECLARE(stored_perf_event);

struct str_view
{
	u64 Size;
	char const *Data;
};

struct perf_event
{
	u64 Cycle;
	u64 Clock;

	u32 HitCount;
	u16 Id;
	perf_event_type Type;
};

struct stored_perf_event
{
	// relative to the start time of the debug_state
	u32 DeltaClock;
	u32 DeltaCycle;

	u32 HitCount;
	u16 Id;
	u8  Type;
	u8  Padding;
};

struct debug_event
{
	stored_perf_event Perf;
};

/* typedef enum */
/* { */
/* 	Type_BOOL, */
/* 	Type_V2, */
/* 	Type_V3, */
/* 	Type_F32, */
/* 	Type_F64, */
/* 	Type_I64, */
/* 	Type_I32, */
/* } type; */

/* struct typed_value */
/* { */
/* 	type Type; */
/* 	union */
/* 	{ */
/* 		bool Value_bool; */
/* 		v2 Value_v2; */
/* 		v3 Value_v3; */
/* 		v4 Value_v4; */
/* 		f32 Value_f32; */
/* 		f64 Value_f64; */
/* 		i64 Value_i64; */
/* 		i32 Value_i32; */
/* 	}; */
/* }; */

/* struct debug_variable */
/* { */
/* 	str_view Name; */
/* 	typed_value Value; */
/* }; */

/* struct debug_variable_view */
/* { */
/* 	debug_variable const *Var; */
/* 	debug_variable_view *Left, *Right, *Top, *Bottom; */
/* }; */

FWDDECLARE(event_storage);

struct event_storage
{
	idx Capacity;
	idx End;
	debug_event *Storage;
};

struct time_stamp
{
	u64 Clock;
	u64 Cycles;
};

struct thread_debug_state
{
	time_stamp Start;

	event_storage Events;

	bool volatile InUse;
};

#define WITHDEBUGSTATE(Name, Ctx) DEFER(Name = AcquireDebugState((Ctx)), \
					ReleaseDebugState((Ctx), Name))

PRIVATE thread_debug_state *AcquireDebugState(thread_context *Ctx);

PRIVATE time_stamp
GrabTimeStamp
(
	void
)
{
	time_stamp Result = {};

	return Result;
}

PRIVATE void ReleaseDebugState(thread_context *Ctx, thread_debug_state *State);

PRIVATE void
RestartDebugState
(
	thread_debug_state *State
)
{
	State->Start = GrabTimeStamp();
	State->Events.End = 0;
}

PRIVATE event_storage
Swap(event_storage New, thread_context *Thread)
{
	event_storage Result;
	thread_debug_state *State;
	WITHDEBUGSTATE(State, Thread)
	{
		Result = State->Events;
		State->Events = New;
		RestartDebugState(State);
	}
	return Result;
}

PRIVATE bool
AllocPerfEvent
(
	event_storage *Store,
	u32 DeltaTime,
	u32 DeltaCycle,
	u32 HitCount,
	u16 Id,
	u16 Type
)
{
	bool Success;
	if (Store->End < Store->Capacity)
	{
		debug_event *Event = &Store->Storage[Store->End++];
		Event->Perf.DeltaClock = DeltaTime;
		Event->Perf.DeltaCycle = DeltaCycle;
		Event->Perf.HitCount   = HitCount;
		Event->Perf.Id         = Id;
		Event->Perf.Type       = Type;
		Success = 1;
	}
	else
	{
		Success = 0;
	}

	return Success;
}

/* void CollateAll(thread_debug_state *Empty) */
/* { */
/* 	thread_debug_state *Current = Empty; */
/* 	for (thread : Threads) */
/* 	{ */
/* 		Current = Swap(Current, Thread); */
/* 		Collate(Current); */
/* 		Empty(Current); */
/* 	} */
/* } */

// the data parameter technically points to "unallocated" memory, so it is paramount
// that nobody writes to it _and_ nobody reads from it except at the very start
// of the function.  Sadly we cannot control the second part!
typedef void (*async_fn)(thread_context const *ExecutingThread, void const *Data);

typedef enum async_block_status
{
	AsyncBlockStatus_EMPTY,
	AsyncBlockStatus_READY,
	AsyncBlockStatus_READ,
} async_block_status;

/*
  struct async_memory_loc
  {
  i32 Alignment;
  i32 Size;
  i32 ArgOffset;
  };

  struct async_small_buf
  {
  i8 NegSize;
  char Data[sizeof(async_memory_loc) - 1];
  };
*/

struct async_block
{
	async_fn Fun;
	async_block_status volatile Status;
	i32 Size;
	i32 Alignment;
	i32 ArgOffset;
	/*
	  union
	  {
	  async_memory_loc NotSbo;
	  async_small_buf  Sbo;
	  };
	*/
};

struct async_stack
{
	// this is read only for every thread
	// except the owning thread!
	async_block *Stack;
	ring_alloc Alloc;
	i32 Capacity;
	i32 volatile Head;
	i32 volatile Tail;
};

struct thread_context
{
	idx ThreadId;
	thread_debug_state DebugState;
	async_stack Stack;

	// if the number of elements in Stack
	// is bigger than HelpCallThreshold
	// the thread will try to wake up another thread
	// to help with the workload
	i32 HelpCallThreshold;

	idx NumOtherStacks;
	async_stack **OtherStacks;
};

void
ReleaseDebugState
(
	thread_context *Ctx,
	thread_debug_state *State
)
{
	thread_debug_state *State2 = &Ctx->DebugState;
	YASSERT(State2 == State);
	YASSERT(InterlockedExchange(&State->InUse, 0) == 1);
}

thread_debug_state *
AcquireDebugState
(
	thread_context *Ctx
)
{
	thread_debug_state *State = &Ctx->DebugState;
	while (InterlockedExchange(&State->InUse, 1) != 0);
	return State;
}

thread_local thread_context *ThisThread;

GAMEFN thread_context *
CurrentThreadContext
(
	void
)
{
	return ThisThread;
}

GAMEFN void
Resume
(
	thread_context *ThisThread,
	async_block *Block
)
{
	Block->Fun(ThisThread, ThisThread->Stack.Alloc.Data + Block->ArgOffset);
}

PRIVATE i32
UpdateTail
(
	async_stack *Stack
)
{
	i32 Tail = Stack->Tail;
	i32 Head = Stack->Head;
	async_block *Blocks = Stack->Stack;

	while (Tail != Head)
	{
		if (InterlockedRead(&Blocks[Tail].Status) ==
		    AsyncBlockStatus_EMPTY)
		{
			// also update the new mem end!
			Ring_DequeueTo(&Stack->Alloc,
				       Blocks[Tail].ArgOffset + Blocks[Tail].Size);
			if (Stack->Alloc.End >= Stack->Alloc.Capacity)
			{
				YASSERT(Stack->Alloc.End == Stack->Alloc.Capacity);
				Stack->Alloc.End = 0;
			}
			Tail = ModInc(Tail, Stack->Capacity);
		}
		else
		{
			break;
		}
	}

	InterlockedWrite(&Stack->Tail, Tail);
	return Tail;
}


PRIVATE async_block *
AllocateBlock
(
	async_stack *Stack,
	async_fn Fn,
	idx Size,
	idx Alignment
)
{
	YASSERT(Size <= MAXOF(I32C(0)));
	YASSERT(Alignment <= MAXOF(I32C(0)));
	async_block *Result = NULL;

	idx BlockSize = 0;

	// Idea: Instead of the thief thread updating the tail counter
	//       we could instead update it here by walking through the list
	//       until we hit a non empty one (or the head!)
	//       This way we can remove lots of synchronization around the tail!
	idx StackTail = UpdateTail(Stack);
	idx StackHead = Stack->Head;

	// the queue is full if StackTail = StackHead + 1
	if (StackTail != ModInc(StackHead, Stack->Capacity))
	{
		idx NextHead = ModInc(StackHead, Stack->Capacity);

		i32 StartOffset = Ring_DryPush(&Stack->Alloc,
					       Size, Alignment);

		if (StartOffset >= 0)
		{
			async_block *NewBlock = &Stack->Stack[StackHead];
			async_block_status Status = InterlockedRead(&NewBlock->Status);
			YASSERT(Status == AsyncBlockStatus_EMPTY);
			NewBlock->Fun       = Fn;
			NewBlock->Size      = Size;
			NewBlock->Alignment = Alignment;
			NewBlock->ArgOffset = StartOffset;
			Result = NewBlock;
		}
	}

	return Result;
}

PRIVATE void
IncAsyncStackHead
(
	async_stack *Stack
)
{
	YASSERT(Stack == &CurrentThreadContext()->Stack);
	i32 SavedHead = Stack->Head;
	i32 NewHead = ModInc(SavedHead, Stack->Capacity);
	async_block *TopMost = &Stack->Stack[SavedHead];
	Ring_ReserveTo(&Stack->Alloc, TopMost->ArgOffset + TopMost->Size);
	YASSERT(InterlockedExchange(&TopMost->Status, AsyncBlockStatus_READY)
		== AsyncBlockStatus_EMPTY);
	InterlockedWriteEx(&Stack->Head, NewHead, __ATOMIC_RELEASE);
	// YASSERT(InterlockedExchange(&Stack->Head, NewHead) == SavedHead);


}

PRIVATE void
DecAsyncStackHead
(
	async_stack *Stack
)
{
	YASSERT(Stack == &CurrentThreadContext()->Stack);
	i32 SavedHead = Stack->Head;
	i32 NewHead = ModDec(SavedHead, Stack->Capacity);
	async_block *TopMost = &Stack->Stack[ModDec(NewHead, Stack->Capacity)];
	Ring_PopTo(&Stack->Alloc, TopMost->ArgOffset + TopMost->Size);
	YASSERT(InterlockedExchange(&Stack->Head, NewHead) == SavedHead);
}

GAMEFN bool
TryPushJob
(
	async_fn Fn,
	idx Size,
	idx Alignment,
	void *Args
)
{
	YASSERT(Size <= MAXOF(I32C(0)));
	YASSERT(Alignment <= MAXOF(I32C(0)));
	YASSERT(Size > 0);
	YASSERT(Alignment > 0);
	YASSERT(ISPOWOFTWO(Alignment));

	thread_context *ThisThread = CurrentThreadContext();
	async_stack *Stack = &ThisThread->Stack;

	bool Success;

	async_block *Block = AllocateBlock(Stack,
					   Fn,
					   Size,
					   Alignment);
	if (Block)
	{
		memcpy(Stack->Alloc.Data + Block->ArgOffset, Args, Size);
		IncAsyncStackHead(Stack);
		Success = 1;
	}
	else
	{
		Success = 0;
	}

	return Success;
}

PRIVATE async_block *
TryPopJob
(
	async_stack *Stack
)
{
	// Head is only changed on this thread
	// no need for atomic reads!
	i32 Head = Stack->Head;
	i32 Prev = ModDec(Head, Stack->Capacity);
	async_block *Current = &Stack->Stack[Prev];

	if (InterlockedCompareSwap(&Current->Status,
				   AsyncBlockStatus_READY,
				   AsyncBlockStatus_EMPTY))
	{
		DecAsyncStackHead(Stack);
	}
	else
	{
		Current = NULL;
	}

	return Current;
}

PRIVATE void
CopyBlockArgs
(
	async_stack *DestStack,
	async_block *Dest,
	async_stack *SrcStack,
	async_block *Src
)
{
	YASSERT(Dest->Size == Src->Size);
	memcpy(DestStack->Alloc.Data + Dest->ArgOffset,
	       SrcStack->Alloc.Data + Src->ArgOffset,
	       Src->Size);
}

PRIVATE async_block *
TryStealJob
(
	async_stack *Stack,
	async_stack *Target
)
{
	i32 CurrentIdx = InterlockedReadEx(&Target->Tail, __ATOMIC_ACQUIRE);
	i32 LastIdx    = InterlockedReadEx(&Target->Head, __ATOMIC_ACQUIRE);
	async_block *Stolen  = NULL;

	while ((CurrentIdx != LastIdx) && (Stolen == NULL))
	{
		async_block *ToSteal = &Target->Stack[CurrentIdx];
		if (InterlockedCompareSwap(&ToSteal->Status,
					   AsyncBlockStatus_READY,
					   AsyncBlockStatus_READ))
		{
			async_block *New = AllocateBlock(Stack,
							 ToSteal->Fun,
							 ToSteal->Size,
							 ToSteal->Alignment);
			/* printf("[STEALING %ld] Getting %d from %ld\n", */
			/*        CurrentThreadContext()->ThreadId, */
			/*        CurrentIdx, */
			/*        (CONTAINEROF(Target, thread_context, Stack))->ThreadId); */
			CopyBlockArgs(Stack, New, Target, ToSteal);
			// do not update the head here!
			// try steal job does the implicit TryPopJob!
//			IncAsyncStackHead(Stack);
			COMPILERBARRIER();
			YASSERT(InterlockedExchange(&ToSteal->Status,
						    AsyncBlockStatus_EMPTY)
				== AsyncBlockStatus_READ);
			Stolen = New;
		}

		CurrentIdx = ModInc(CurrentIdx, Target->Capacity);
	}

	return Stolen;
}

PRIVATE bool
DoQueuedWorkWithStealing
(
	async_stack *MainStack,
	idx NumTheftTargets,
	async_stack **TheftTargets,
	idx MaxStealTries
)
{
	bool DidWork;
	// pop only fails if the queue is empty
	// in that case the queue will not unempty itself
	// without this thread doing a job so we do not need
	// to attempt further pops
	async_block *CurrentJob = TryPopJob(&ThisThread->Stack);

	for (idx Try = 0;
	     (CurrentJob == NULL) && (Try < MaxStealTries);
	     ++Try)
	{
		for (idx Other = 0;
		     (CurrentJob == NULL) && (Other < NumTheftTargets);
		     ++Other)
		{
			CurrentJob = TryStealJob(&ThisThread->Stack, TheftTargets[Other]);
		}
	}

	if (CurrentJob != NULL)
	{
		Resume(ThisThread, CurrentJob);
		DidWork = 1;
	}
	else
	{
		DidWork = 0;
	}

	return DidWork;
}

PRIVATE void *
JobWorker_WithSleep
(
	void *GenericArg
)
{
	ThisThread = (thread_context *) GenericArg;

	idx NumOtherStacks = ThisThread->NumOtherStacks;
	async_stack **OtherStacks = ThisThread->OtherStacks;

	while (1)
	{
		if (DoQueuedWorkWithStealing(&ThisThread->Stack, NumOtherStacks, OtherStacks, 5))
		{
//			printf("[THREAD %ld] Work, Work!!\n", ThisThread->ThreadId);
		}
		else
		{
//			printf("[THREAD %ld] no work for me :(\n", ThisThread->ThreadId);
			usleep(50);
		}
	}

	return NULL;
}

PRIVATE void
InitThreadDebugState
(
	thread_debug_state *State
)
{

}

PRIVATE async_stack
AsyncStackFromMalloc
(
	idx StackSize,
	idx ArgSize
)
{
	async_stack Stack;
	Stack.Alloc = Ring_FromBuffer(ArgSize, calloc(1, ArgSize));
	Stack.Capacity = StackSize;
	Stack.Stack = calloc(sizeof(*Stack.Stack), Stack.Capacity);
	Stack.Head  = 0;
	Stack.Tail  = 0;
	return Stack;
}

PRIVATE void
Printer
(
	thread_context const *ExecutingThread,
	void *Data
)
{
	printf("[THREAD %ld] Work, Work; %ld\n", ExecutingThread->ThreadId,
	       *(idx *) Data);
}

FWDDECLARE(test_pair);
struct test_pair
{
	i8 *Array;
	idx Index;
};

PRIVATE void
Checker
(
	thread_context const *ExecutingThread,
	void const *Data
)
{
	test_pair Pair = *(test_pair const *)Data;
	if (Pair.Array[Pair.Index] != ExecutingThread->ThreadId)
	{
		printf("[Job %ld] I was super stolen!\n", Pair.Index);
	}
	YASSERT(Pair.Array[Pair.Index] != -1);
}

PRIVATE void
Setter
(
	thread_context const *ExecutingThread,
	void const *Data
)
{
	test_pair Pair = *(test_pair const *)Data;
	Pair.Array[Pair.Index] = ExecutingThread->ThreadId;
	if (ExecutingThread->ThreadId != 0)
	{
		while (!TryPushJob(&Checker, sizeof(Pair), alignof(Pair), &Pair));
	}
}

PRIVATE thread_debug_state
DebugState_FromMalloc
(
	idx EventCapacity
)
{
	thread_debug_state State;
	State.InUse = 0;
	State.Start = GrabTimeStamp();

	State.Events.Capacity = EventCapacity;
	State.Events.End      = 0;
	State.Events.Storage  = calloc(EventCapacity, sizeof(*State.Events.Storage));
	return State;
}

int
main
(
	int argc, char *argv[], char *envp[]
)
{
	puts("[START]");
	idx NumThreads = 3;
	idx NumWorkers = NumThreads - 1;
	pthread_t ThreadIds[2];
	thread_context ThreadContexts[3];

	cpu_set_t *Set = CPU_ALLOC(NumThreads);
	size_t SetSize = CPU_ALLOC_SIZE(NumThreads);

	for (i32 CurrentThread = 0;
	     CurrentThread < NumThreads;
	     ++CurrentThread)
	{
		ThreadContexts[CurrentThread].ThreadId  = CurrentThread;
		ThreadContexts[CurrentThread].DebugState = DebugState_FromMalloc(1 << 10);
		ThreadContexts[CurrentThread].Stack = AsyncStackFromMalloc(512, 1 MB);
		ThreadContexts[CurrentThread].OtherStacks = calloc(sizeof(*ThreadContexts[CurrentThread].OtherStacks), NumThreads-1);
		ThreadContexts[CurrentThread].NumOtherStacks = 0;
		ThreadContexts[CurrentThread].HelpCallThreshold = CurrentThread ? 10 : 0;
	}

	for (i32 CurrentThread = 0;
	     CurrentThread < NumThreads;
	     ++CurrentThread)
	{
		async_stack *Stack = &ThreadContexts[CurrentThread].Stack;
		for (i32 Thread = 0;
		     Thread < NumThreads;
		     ++Thread)
		{
			if (CurrentThread != Thread)
			{
				thread_context *Ctx = &ThreadContexts[Thread];
				Ctx->OtherStacks[Ctx->NumOtherStacks++] = Stack;
			}
		}
	}

	for (i32 CurrentThread = 0;
	     CurrentThread < NumThreads;
	     ++CurrentThread)
	{
		thread_context *Ctx = &ThreadContexts[CurrentThread];
		YASSERT(Ctx->NumOtherStacks == NumThreads - 1);
	}

        // main thread stuff
	CPU_ZERO_S(SetSize, Set);
	CPU_SET_S(0, SetSize, Set);
	YASSERTF(!sched_setaffinity(0, SetSize, Set),
		 "%s", strerror(errno));
	ThisThread = &ThreadContexts[0];

	for (u32 WorkerThread = 0;
	     WorkerThread < NumWorkers;
	     ++WorkerThread)
	{
		pthread_create(&ThreadIds[WorkerThread], NULL, &JobWorker_WithSleep,
			       (void *) &ThreadContexts[WorkerThread + 1]);

		CPU_ZERO_S(SetSize, Set);
		CPU_SET_S((WorkerThread + 1), SetSize, Set);
		YASSERTF(!pthread_setaffinity_np(ThreadIds[WorkerThread], SetSize, Set),
			 "%s", strerror(errno));
	}

	CPU_FREE(Set);

	idx TestCount = 1000;
	i8 *Ok = calloc(TestCount, sizeof(*Ok));
	for (idx I = 0; I < TestCount; ++I) Ok[I] = -1;
	idx MissedCount = 0;

	for (idx I = 0;
	     I < TestCount;
	     ++I)
	{
		test_pair Val = { .Array = Ok, .Index = I };
		while (!TryPushJob(&Setter, sizeof(Val), alignof(Val), &Val));
		/* Ok[I] = !DidPush; */
		/* if (!DidPush) */
		/* { */
		/* 	printf("Too slow for %ld\n", I); */
		/* 	++MissedCount; */
		/* } */
//		usleep(10);
	}

	printf("[MAIN THREAD] Finished pushing! (Missed:%ld)\n",
	       MissedCount);

	thread_context *MainThread = CurrentThreadContext();
	async_block *Block;
	while ((Block = TryPopJob(&MainThread->Stack)) != NULL)
	{
		Resume(MainThread, Block);
	}

	for (u32 WorkerThread = 0;
	     WorkerThread < NumWorkers;
	     ++WorkerThread)
	{
		pthread_cancel(ThreadIds[WorkerThread]);
		pthread_join(ThreadIds[WorkerThread], NULL);
	}

	idx NumWorked[3] = {};
	for (idx I = 0;
	     I < TestCount;
	     ++I)
	{
		if (Ok[I] == -1)
		{
			printf("Missed %ld\n", I);
		}
		else
		{
			YASSERT(0 <= Ok[I] && Ok[I] <= NumThreads);
			NumWorked[Ok[I]] += 1;
		}
	}

	for (idx I = 0;
	     I < NumThreads;
	     ++I)
	{
		printf("Thread %ld worked %ld\n", I, NumWorked[I]);
	}

	for (idx I = 0;
	     I < NumThreads;
	     ++I)
	{
		async_stack *Stack = &ThreadContexts[I].Stack;
		UpdateTail(Stack);
		for (idx Job = 0;
		     Job < Stack->Capacity;
		     ++Job)
		{
			async_block *Block = Stack->Stack + Job;
			YASSERT(Block->Status == AsyncBlockStatus_EMPTY);
		}
		printf("Head:%d Tail:%d Start:%ld End:%ld\n",
		       Stack->Head, Stack->Tail,
		       Stack->Alloc.Begin, Stack->Alloc.End);
	}


	return 0;
}

typedef enum
{
	PerfEventType_BEGIN,
	PerfEventType_END,
} perf_event_type;

GAMEFN bool
PushPerfEvent
(
	u32 HitCount,
	u16 Id,
	perf_event_type Type
)
{
	thread_context *ThisThread = CurrentThreadContext();

	time_stamp Stamp = GrabTimeStamp();

	bool Success;

	thread_debug_state *DebugState;
	WITHDEBUGSTATE(DebugState, ThisThread)
	{
		i64 DeltaT = Stamp.Clock  - DebugState->Start.Clock;
		i64 DeltaC = Stamp.Cycles - DebugState->Start.Cycles;

		if (DeltaT >= MINOF(MEMBER(stored_perf_event, DeltaClock))
		    && DeltaT <= MAXOF(MEMBER(stored_perf_event, DeltaClock))
		    && DeltaC >= MINOF(MEMBER(stored_perf_event, DeltaCycle))
		    && DeltaC <= MAXOF(MEMBER(stored_perf_event, DeltaCycle)))
		{
			Success = AllocPerfEvent(&DebugState->Events, DeltaT, DeltaC, HitCount, Id, Type);
		}
		else
		{
			Success = 0;
		}
	}

	return Success;
}
