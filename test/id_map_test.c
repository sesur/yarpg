#undef DEBUG

#include <def.h>
#include <compiler.h>
#include <stdio.h>
#include <time.h>

#include <util/id_map.h>
#include <util/arena_allocator.h>

#define FormatOut(...)

#include "../src/def.c"
#include "../src/util/math.c"
#include "../src/util/vector.c"
#include "../src/util/memory.c"
#include "../src/util/arena_allocator.c"
#include "../src/util/random.c"

#undef PRIVATE
#define PRIVATE
#include "../src/util/hash.c"
#include "../src/util/id_map.c"

#define DONOTOPTIMIZE(v) __asm__ __volatile__ ("" : "+m" (v));

entity_id CONSTFN
PosId(iv2 Pos)
{
	union
	{
		iv2 AsPos;
		entity_id AsId;
	} Switch = { .AsPos = Pos };

	return Switch.AsId;
}

idx CONSTFN
PosValue(idx Width, iv2 Pos)
{
	return Pos.X + Pos.Y * Width;
}

#define TimeSetup() u64 Start, End; struct timespec Spec
#define Time(Message, NumActions)					\
	DEFER((printf("%s %s ", Message, "..."),			\
	       fflush(stdout),						\
	       clock_gettime(CLOCK_MONOTONIC_RAW, &Spec),		\
	       (Start) = (u64)Spec.tv_sec * U64C(1000000000) + (u64)Spec.tv_nsec), \
	      (clock_gettime(CLOCK_MONOTONIC_RAW, &Spec),		\
	       (End) = (u64)Spec.tv_sec * U64C(1000000000) + (u64)Spec.tv_nsec, \
	       printf("finished in %luns (%lf ns/act)\n", End - Start, (f64)(End - Start) / (f64)(NumActions))))
int main(void)
{
	b32 Error = 0;
#define MEM_SIZE (2 GB)
	void *Memory = malloc(MEM_SIZE);
	memory_arena Arena;
	InitializeArena(&Arena, MEM_SIZE, Memory);

	idx Height = 5000;
	idx Width  = 5000;

	id_map Map = IdMapOfArena(Width * Height, &Arena);

	idx NumEntries = Width * Height;
	entity_id *Ids = malloc(NumEntries * sizeof(*Ids));

	srand(time(NULL));
	rand64 Rand = RandomSeries(((i64)rand() << 32) | rand());;

	for (idx I = 0;
	     I < NumEntries;
	     ++I)
	{
		Ids[I].ToIdx = NextI64(&Rand);
		while (Ids[I].ToIdx == -1)
		{
			Ids[I].ToIdx = NextI64(&Rand);
		}
	}

	printf("Mem used: %ld bytes\n", Arena.Used);

	TimeSetup();

	Time("Creating (points)", Width * Height)
	{
		for (idx Y = 0; Y < Height; ++Y)
		{
			for (idx X = 0; X < Width; ++X)
			{
				iv2 Pos = IV(X, Y);

				id_index *Index = CreateIdSlot(&Map, PosId(Pos));

				if (Index == NULL)
				{
					fprintf(stderr, "(%ld, %ld) -> NULL\n", X, Y);
					Error = 1;
					exit(EXIT_FAILURE);
				}
				idx Value = PosValue(Width, Pos) % 512;

				*Index = Value;
			}
		}
	}
	COMPILERBARRIER();
	printf("Occupancy: %lf (%ld / %ld)\n", (f64) Map.Occupancy / (f64) Map.Capacity,
	       Map.Occupancy, Map.Capacity);
	Time("Searching (points)", Width * Height)
	{
		for (idx Y = 0; Y < Height; ++Y)
		{
			for (idx X = 0; X < Width; ++X)
			{
				//iv2 Pos = IV((Width-1) - X, (Height - 1) - Y);
				iv2 Pos = IV(X, Y);

				id_index *Index = GetIdSlot(&Map, PosId(Pos));
//			id_index *Index2 = GetIdSlot(&Map2, PosId(Pos));

				if (Index == NULL)
				{
					fprintf(stderr, "(%ld, %ld) -> NULL\n", X, Y);
					Error = 1;
					exit(EXIT_FAILURE);
				}

				id_index I = *Index;

				DONOTOPTIMIZE(Index);

				if (I != PosValue(Width, Pos) % 512)
				{
					/* idx NewX = I % Width; */
					/* idx NewY = I / Width; */
					fprintf(stderr, "(%ld, %ld) -> ( ??, ?? )\n", X, Y);
					Error = 1;
					exit(EXIT_FAILURE);
				}
			}
		}
	}
	printf("Max PSL: %ld\n", Map.MaxPSL);
//	printf("Shuffles: Max:%ld Num:%ld\n", MaxShuffles, NumMax);

	COMPILERBARRIER();
	ClearIdMap(&Map);
	COMPILERBARRIER();

	Time("Creating (rand)", NumEntries)
	{
		for (idx Entity = 0;
		     Entity < NumEntries;
		     ++Entity)
		{
			id_index *Index = CreateIdSlot(&Map, Ids[Entity]);
			if (Index == NULL)
			{
				fprintf(stderr, "%ld (%ld) -> NULL\n",
					Ids[Entity].ToIdx, Entity);
				Error = 1;
				exit(EXIT_FAILURE);
			}
			idx Value = Entity % 512;
			*Index = Value;
		}
	}
	printf("Occupancy: %lf (%ld / %ld)\n", (f64) Map.Occupancy / (f64) Map.Capacity,
	       Map.Occupancy, Map.Capacity);
	COMPILERBARRIER();
	Time("Searching (rand)", NumEntries)
	{
		for (idx Entity = 0;
		     Entity < NumEntries;
		     ++Entity)
		{
			id_index *Index = GetIdSlot(&Map, Ids[Entity]);

			if (Index == NULL)
			{
				fprintf(stderr, "%ld (%ld) -> NULL\n",
					Ids[Entity].ToIdx, Entity);
				Error = 1;
				exit(EXIT_FAILURE);
			}

			id_index I = *Index;

			DONOTOPTIMIZE(Index);

			if (I != Entity % 512)
			{
				fprintf(stderr, "%ld (%ld) -> ?? ( ?? )\n",
					Ids[Entity].ToIdx, Entity);
				Error = 1;
				exit(EXIT_FAILURE);
			}
		}
	}
	printf("Max PSL: %ld\n", Map.MaxPSL);

	puts("checking hashx4...");
	for (idx I = 0;
	     I < 5000;
	     ++I)
	{
		idx Val = NextI64(&Rand);
		__m256i ValX4 = _mm256_set1_epi64x(Val);


		idx Res = Hash_Murmur3_64(Val);
		__m256i ResX4 = Hash_Murmur3_64X4(ValX4);

		idx OtherRes = _mm256_extract_epi64(ResX4, 0);

		if (OtherRes != Res)
		{
			Error = 1;
			fprintf(stderr, "(%ld): Wanted: %ld; Got %ld\n",
				Val, Res, OtherRes);
			break;
		}

	}

	free(Memory);
	return Error;
}
