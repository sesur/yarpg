#include "test.h"

#include <compiler.h>
#include <util/utf8.h>
#include <util/memory.h>

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>

idx Strlen(str_view);

b32
ValidUTF8Test(void)
{
	b32 Error = 0;

	idx  Length = 37;
	char const *Test = u8"Häoeöe@ǫeoee||˛|˛@˛tąieiooieoti234124";

	str_view TestView = ViewOf(Test);

	idx Count = GlyphCount(TestView);

	if (Count != Length)
	{
		Error = 1;
		fprintf(stderr, "\nExpected: %ld; Got %ld\n",
			Length, Count);
	}



	return Error;
}

b32
InvalidUTF8Test(void)
{
	b32 Error = 0;

	byte Test1[] =
	{
		'a',
		'b',
		0b1100'0000,
		0b1000'0000,

		0b1110'0000,
		0b1000'0000,
		0b1000'0000,

		0b1111'0000,
		0b1000'0000,
		0b1000'0000, // error
	};

	byte Test2[] =
	{
		0b1000'0000, // error
	};

	byte Test3[] =
	{
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		'a',
		0b1100'0000, // error
		'a',
		'a',
		'a',
	};

	byte Test4[] =
	{
		'a',
		0b1000'0000,
		'a',
	};

	byte *Tests[] =
	{
		(byte *) Test1,
		(byte *) Test2,
		(byte *) Test3,
		(byte *) Test4,
	};

	idx Lengths[] =
	{
		ARRAYCOUNT(Test1),
		ARRAYCOUNT(Test2),
		ARRAYCOUNT(Test3),
		ARRAYCOUNT(Test4),
	};

	idx Correct[] =
	{
		4,
		0,
		31,
		1,
	};

	YSASSERT(ARRAYCOUNT(Lengths) == ARRAYCOUNT(Tests));
	YSASSERT(ARRAYCOUNT(Tests) == ARRAYCOUNT(Correct));

	for (udx Test = 0;
	     Test < ARRAYCOUNT(Tests);
	     ++Test)
	{
		str_view View;
		View.Data = (char const *) Tests[Test];
		View.Size = Lengths[Test];

		idx Count = GlyphCount(View);
		idx Length = Correct[Test];

		if (Count != Length)
		{
			Error = 1;
			fprintf(stderr, "\nError at Test %lu: Expected: %ld; Got %ld",
				Test, Length, Count);
		}
	}

	return Error;
}

b32
AsciiTest(void)
{
	b32 Error = 0;

	char const *Test = "eoiudslnuztu2ztlaoitdi0202p";
	idx  Length = strlen(Test);

	str_view TestView = ViewOf(Test);

	idx Count = GlyphCount(TestView);

	if (Count != Length)
	{
		Error = 1;
		fprintf(stderr, "Expected: %ld; Got %ld\n",
			Length, Count);
	}



	return Error;
}

FWDDECLARE(timing);
struct timing
{
	f64 CycleMean, CycleDeviation;
	f64 Mean, Deviation;
};

timing
TimeFunc(idx Func(str_view), str_view View, idx Expected)
{
	struct timeval Start, End;
	f64 Sum = 0;
	f64 Squared = 0;

	f64 CycleSum = 0;
	f64 CycleSquared = 0;

	for (idx Run = 0;
	     Run < 10;
	     ++Run)
	{
		/* Do the test. */
		if (gettimeofday(&Start, NULL)) {
			fprintf(stderr, "gettimeofday failed");
			exit(1);
		}

		u64 CycleS = __rdtsc();

		COMPILERBARRIER();

		idx volatile Result = Func(View);
		YASSERT(Result == Expected);

		u64 CycleE = __rdtsc();

		COMPILERBARRIER();

		if (gettimeofday(&End, NULL)) {
			fprintf(stderr, "gettimeofday failed");
			exit(1);
		}

		/* Compute the time taken and add it to the sums. */
		f64 Time = (End.tv_sec - Start.tv_sec) +
			(End.tv_usec - Start.tv_usec) / 1000000.0;

		u64 Cycles = CycleE - CycleS;

		Sum += Time;
		Squared += Time * Time;
		CycleSum += Cycles;
		CycleSquared += Cycles * Cycles;
	}

	timing Timing;
	Timing.Mean = Sum / 10;
	Timing.Deviation = sqrt(Squared - Sum * Sum / 10);
	Timing.CycleMean = CycleSum / 10;
	Timing.CycleDeviation = sqrt(CycleSquared - CycleSum * CycleSum / 10);
	return Timing;
}

b32
AsciiSpeedTest(void)
{
	b32 Error = 0;
	udx Size = 32;
	struct
	{
		udx Bytes;
		udx Shift;
		char const *Name;
	} Sizes[] =
	{
		{32,  0, "bytes"},
		{128, 0, "bytes"},
		{512, 0, "bytes"},
		{1024, 10, "kilobytes"},
		{4096, 10, "kilobytes"},
		{8192, 10, "kilobytes"},
		{65536, 10, "kilobytes"},
		{1048576, 20, "megabytes"},
		{4194304, 20, "megabytes"},
		{8388608, 20, "megabytes"},
		{67108864, 20, "megabytes"},
		{1073741824, 30, "gigabytes"},
	};
	for (udx I = 0;
	     I < ARRAYCOUNT(Sizes);
	     ++I)
	{
		udx Size = Sizes[I].Bytes;
		udx Shift = Sizes[I].Shift;
		char *AsciiData = malloc(Size + 1);
		SetMemory(Size, 'a', (byte *) AsciiData);
		AsciiData[Size] = '\0';

		str_view View;
		View.Size = Size;
		View.Data = AsciiData;
		timing T1 = TimeFunc(Strlen, View, Size);
		timing T2 = TimeFunc(GlyphCount, View, Size);
		timing T3 = TimeFunc(GlyphCountValid, View, Size);

		printf("Length: %zu %s\n"
		       "%32s  %lf +/- %lfsec %lf +/- %lfcycles\n"
		       "%32s  %lf +/- %lfsec %lf +/- %lfcycles\n"
		       "%32s  %lf +/- %lfsec %lf +/- %lfcycles\n",
		       Size >> Shift, Sizes[I].Name,
		       "strlen", T1.Mean, T1.Deviation, T1.CycleMean, T1.CycleDeviation,
		       "GlyphCount", T2.Mean, T2.Deviation, T2.CycleMean, T2.CycleDeviation,
		       "GlyphCountValid", T3.Mean, T3.Deviation, T3.CycleMean, T3.CycleDeviation);

		free(AsciiData);
		Size = Size << 1;
	}

	return Error;
}

idx
FillUTF8(idx Size, char Buffer[Size])
{
	YASSERT(Size % 8 == 0);
	idx BigSize = Size / 8;
	u64 *BigBuf = (u64 *) Buffer;

	byte A  = 0b0000'0000;
	byte D  = 0b1000'0000;
	byte C2 = 0b1100'0000;
	byte C3 = 0b1110'0000;
	byte C4 = 0b1111'0000;

	struct
	{
		idx NumGlyphs;
		union
		{
			char Bytes[8];
			u64  U64;
		};
	} Variants[] =
	{
		{8, {.Bytes = { A, A, A, A, A, A, A, A }}},
		{4, {.Bytes = { A, C2, D, C4, D, D, D, A }}},
		{3, {.Bytes = { C3, D, D, C2, D, C3, D, D }}},
		{2, {.Bytes = { C4, D, D, D, C4, D, D, D }}},
		{6, {.Bytes = { A, C2, D, A, A, C2, D, A }}},
	};

	idx NumGlyphs = 0;
	for (idx I = 0;
	     I < BigSize;
	     ++I)
	{
		idx Idx = rand() % ARRAYCOUNT(Variants);
		NumGlyphs += Variants[Idx].NumGlyphs;
		BigBuf[I] = Variants[Idx].U64;
	}
	return NumGlyphs;
}

b32
UTF8SpeedTest(void)
{
	b32 Error = 0;
	udx Size = 32;
	struct
	{
		udx Bytes;
		udx Shift;
		char const *Name;
	} Sizes[] =
	{
		{32,  0, "bytes"},
		{128, 0, "bytes"},
		{512, 0, "bytes"},
		{1024, 10, "kilobytes"},
		{4096, 10, "kilobytes"},
		{8192, 10, "kilobytes"},
		{65536, 10, "kilobytes"},
		{1048576, 20, "megabytes"},
		{4194304, 20, "megabytes"},
		{8388608, 20, "megabytes"},
		{67108864, 20, "megabytes"},
		{1073741824, 30, "gigabytes"},
	};
	for (udx I = 0;
	     I < ARRAYCOUNT(Sizes);
	     ++I)
	{
		udx Size = Sizes[I].Bytes;
		udx Shift = Sizes[I].Shift;
		char *AsciiData = malloc(Size + 1);
		idx Expected = FillUTF8(Size, AsciiData);

		str_view View;
		View.Size = Size;
		View.Data = AsciiData;
		timing Time = TimeFunc(GlyphCount, View, Expected);
		timing TimeV = TimeFunc(GlyphCountValid, View, Expected);

		printf("Length: %zu %s\n"
		       "%32s  %lf +/- %lfsec %lf +/- %lfcycles\n"
		       "%32s  %lf +/- %lfsec %lf +/- %lfcycles\n",
		       Size >> Shift, Sizes[I].Name,
		       "GlyphCount",
		       Time.Mean, Time.Deviation,
		       Time.CycleMean, Time.CycleDeviation,
		       "GlyphCountValid",
		       TimeV.Mean, TimeV.Deviation,
		       TimeV.CycleMean, TimeV.CycleDeviation);

		free(AsciiData);
		Size = Size << 1;
	}

	return Error;
}

int
main(int, char **)
{
	b32 Error = 0;

	printf("Staring ValidUTF8 test ..."); fflush(stdout);
	Error |= ValidUTF8Test() << 0;
	puts(" Finished");

	printf("Staring InvalidUTF8 test ..."); fflush(stdout);
	Error |= InvalidUTF8Test() << 1;
	puts(" Finished");

	printf("Staring Ascii test ..."); fflush(stdout);
	Error |= AsciiTest() << 2;
	puts(" Finished");

	printf("Staring Ascii Speed test ...\n");
	Error |= AsciiSpeedTest() << 3;
	puts(" Finished");

	printf("Staring UTF8 Speed test ...\n");
	Error |= UTF8SpeedTest() << 4;
	puts(" Finished");

	return Error;
}

#pragma GCC optimize ("O2")

idx PUREFN
Strlen(str_view View)
{
	return strlen(View.Data);
}

#include "../src/def.c"
#include "../src/util/arena_allocator.c"
#include "../src/util/memory.c"
#include "../src/util/utf8.c"
#include "../src/util/string.c"
#include "../src/util/math.c"
#include "../src/util/vector.c"
