#define FormatOut(...)
#undef DEBUG

#include <def.h>
#include <util/memory.h>

#undef PRIVATE
#define PRIVATE

#include "../src/util/memory.c"

#include <stdio.h>
#include <memory.h>

PRIVATE b32
TestMemMove(void)
{
#define BUF_SIZE 256
#define ERRBYTE  0xEE
	b32 Error = 0;

	byte *Source = malloc(BUF_SIZE);
	byte *Dest   = malloc(BUF_SIZE);

	for (idx I = 0;
	     I < BUF_SIZE;
	     ++I)
	{
		Source[I] = (byte)I;
		Dest[I] = ERRBYTE;
	}

	puts("MemMove Test");
	printf("ErrorByte = %d\n", ERRBYTE);

	for (idx N = 1;
	     N <= BUF_SIZE;
	     N = N * 2)
	{
		MoveMemory(N, Source, Dest);
		for (idx I = 0;
		     I < N;
		     ++I)
		{
			if (Dest[I] != Source[I])
			{
				fprintf(stderr, "Error at N = %ld, I = %ld;\n"
					"Expected: %d; Got: %d\n",
					N, I, Source[I], Dest[I]);
				Error = 1;

				goto end;
			}
		}

		for (idx I = N;
		     I < BUF_SIZE;
		     ++I)
		{
			if (Dest[I] != ERRBYTE)
			{
				fprintf(stderr, "Error at N = %ld, I = %ld;\n"
					"Expected: %d; Got: %d\n",
					N, I, ERRBYTE, Dest[I]);
				Error = 2;

				goto end;
			}
		}

		memset(Dest, ERRBYTE, BUF_SIZE);
	}

	for (idx I = 0;
	     I < BUF_SIZE;
	     ++I)
	{
		if (Source[I] != I)
		{
			fprintf(stderr, "Source damaged at I = %ld;\n"
				"Expected: %d; Got: %d\n",
				I, (byte)I, Source[I]);
			Error = 3;
			goto end;
		}
	}

end:


#undef BUF_SIZE

	return Error;
}

int
main(int, char**)
{
	b32 Error = 0;

	Error = TestMemMove();

	return Error;
}
